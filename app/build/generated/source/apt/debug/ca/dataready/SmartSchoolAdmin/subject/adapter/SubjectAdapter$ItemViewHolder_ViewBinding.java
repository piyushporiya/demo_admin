// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.subject.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SubjectAdapter$ItemViewHolder_ViewBinding implements Unbinder {
  private SubjectAdapter.ItemViewHolder target;

  @UiThread
  public SubjectAdapter$ItemViewHolder_ViewBinding(SubjectAdapter.ItemViewHolder target,
      View source) {
    this.target = target;

    target.txtSubjectName = Utils.findRequiredViewAsType(source, R.id.txt_subject_name, "field 'txtSubjectName'", TextView.class);
    target.imgMore = Utils.findRequiredViewAsType(source, R.id.img_more, "field 'imgMore'", ImageView.class);
    target.txtGrade = Utils.findRequiredViewAsType(source, R.id.txt_grade, "field 'txtGrade'", TextView.class);
    target.txtSubjectType = Utils.findRequiredViewAsType(source, R.id.txt_subject_type, "field 'txtSubjectType'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SubjectAdapter.ItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtSubjectName = null;
    target.imgMore = null;
    target.txtGrade = null;
    target.txtSubjectType = null;
  }
}
