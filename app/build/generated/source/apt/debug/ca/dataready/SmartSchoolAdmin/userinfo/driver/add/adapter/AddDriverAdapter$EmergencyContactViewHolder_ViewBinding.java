// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.userinfo.driver.add.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddDriverAdapter$EmergencyContactViewHolder_ViewBinding implements Unbinder {
  private AddDriverAdapter.EmergencyContactViewHolder target;

  @UiThread
  public AddDriverAdapter$EmergencyContactViewHolder_ViewBinding(AddDriverAdapter.EmergencyContactViewHolder target,
      View source) {
    this.target = target;

    target.imgAddress = Utils.findRequiredViewAsType(source, R.id.img_address, "field 'imgAddress'", ImageView.class);
    target.layoutEmergencyContact = Utils.findRequiredViewAsType(source, R.id.layout_emergency_contact, "field 'layoutEmergencyContact'", CardView.class);
    target.btnPrevious = Utils.findRequiredViewAsType(source, R.id.btn_previous, "field 'btnPrevious'", Button.class);
    target.txtHeaderTitle = Utils.findRequiredViewAsType(source, R.id.txt_header_title, "field 'txtHeaderTitle'", TextView.class);
    target.btnStartAdminProcess = Utils.findRequiredViewAsType(source, R.id.btn_start_admin_process, "field 'btnStartAdminProcess'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AddDriverAdapter.EmergencyContactViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.imgAddress = null;
    target.layoutEmergencyContact = null;
    target.btnPrevious = null;
    target.txtHeaderTitle = null;
    target.btnStartAdminProcess = null;
  }
}
