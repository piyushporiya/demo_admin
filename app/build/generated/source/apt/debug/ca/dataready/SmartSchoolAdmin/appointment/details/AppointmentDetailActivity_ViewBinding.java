// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.appointment.details;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AppointmentDetailActivity_ViewBinding implements Unbinder {
  private AppointmentDetailActivity target;

  private View view2131230835;

  private View view2131230836;

  @UiThread
  public AppointmentDetailActivity_ViewBinding(AppointmentDetailActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AppointmentDetailActivity_ViewBinding(final AppointmentDetailActivity target,
      View source) {
    this.target = target;

    View view;
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.txtTitleValue = Utils.findRequiredViewAsType(source, R.id.txt_title_value, "field 'txtTitleValue'", TextView.class);
    target.txtDateValue = Utils.findRequiredViewAsType(source, R.id.txt_date_value, "field 'txtDateValue'", TextView.class);
    target.txtTimeValue = Utils.findRequiredViewAsType(source, R.id.txt_time_value, "field 'txtTimeValue'", TextView.class);
    target.txtConfirmValue = Utils.findRequiredViewAsType(source, R.id.txt_confirm_value, "field 'txtConfirmValue'", TextView.class);
    target.rvStudentsList = Utils.findRequiredViewAsType(source, R.id.rv_students, "field 'rvStudentsList'", RecyclerView.class);
    target.parentViewAnimator = Utils.findRequiredViewAsType(source, R.id.parent_viewAnimator, "field 'parentViewAnimator'", ViewAnimator.class);
    target.rvAttachments = Utils.findRequiredViewAsType(source, R.id.rvAttachments, "field 'rvAttachments'", RecyclerView.class);
    target.llAttachments = Utils.findRequiredViewAsType(source, R.id.llAttachments, "field 'llAttachments'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.btn_cancel, "field 'btnCancel' and method 'onViewClicked'");
    target.btnCancel = Utils.castView(view, R.id.btn_cancel, "field 'btnCancel'", Button.class);
    view2131230835 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_confirm, "field 'btnConfirm' and method 'onViewClicked'");
    target.btnConfirm = Utils.castView(view, R.id.btn_confirm, "field 'btnConfirm'", Button.class);
    view2131230836 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.llButtons = Utils.findRequiredViewAsType(source, R.id.llButtons, "field 'llButtons'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AppointmentDetailActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.txtTitleValue = null;
    target.txtDateValue = null;
    target.txtTimeValue = null;
    target.txtConfirmValue = null;
    target.rvStudentsList = null;
    target.parentViewAnimator = null;
    target.rvAttachments = null;
    target.llAttachments = null;
    target.btnCancel = null;
    target.btnConfirm = null;
    target.llButtons = null;

    view2131230835.setOnClickListener(null);
    view2131230835 = null;
    view2131230836.setOnClickListener(null);
    view2131230836 = null;
  }
}
