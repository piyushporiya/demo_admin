// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.userinfo.student.transportation_history.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class StudentTransportationAdapter$TripViewHolder_ViewBinding implements Unbinder {
  private StudentTransportationAdapter.TripViewHolder target;

  @UiThread
  public StudentTransportationAdapter$TripViewHolder_ViewBinding(StudentTransportationAdapter.TripViewHolder target,
      View source) {
    this.target = target;

    target.txtBoardingDate = Utils.findRequiredViewAsType(source, R.id.txt_boarding_date, "field 'txtBoardingDate'", TextView.class);
    target.txtScanTime = Utils.findRequiredViewAsType(source, R.id.txt_scan_time, "field 'txtScanTime'", TextView.class);
    target.txtOnBoard = Utils.findRequiredViewAsType(source, R.id.txt_on_board, "field 'txtOnBoard'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    StudentTransportationAdapter.TripViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtBoardingDate = null;
    target.txtScanTime = null;
    target.txtOnBoard = null;
  }
}
