// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.active_route;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ActiveRoutesFragment_ViewBinding implements Unbinder {
  private ActiveRoutesFragment target;

  @UiThread
  public ActiveRoutesFragment_ViewBinding(ActiveRoutesFragment target, View source) {
    this.target = target;

    target.viewAnimator = Utils.findRequiredViewAsType(source, R.id.viewAnimator, "field 'viewAnimator'", ViewAnimator.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ActiveRoutesFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.viewAnimator = null;
  }
}
