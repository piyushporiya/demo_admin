// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.userinfo.driver.add;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import com.hbb20.CountryCodePicker;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddDriverActivity_ViewBinding implements Unbinder {
  private AddDriverActivity target;

  private View view2131231039;

  private View view2131231075;

  private View view2131231062;

  private View view2131231286;

  private View view2131230841;

  private View view2131230850;

  private View view2131230859;

  private View view2131230870;

  private View view2131230910;

  private View view2131230900;

  private View view2131230801;

  private View view2131230909;

  private View view2131230856;

  private View view2131230855;

  private View view2131230904;

  private View view2131230846;

  private View view2131230845;

  private View view2131231637;

  @UiThread
  public AddDriverActivity_ViewBinding(AddDriverActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AddDriverActivity_ViewBinding(final AddDriverActivity target, View source) {
    this.target = target;

    View view;
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.etFirstName = Utils.findRequiredViewAsType(source, R.id.et_first_name, "field 'etFirstName'", EditText.class);
    target.etLastName = Utils.findRequiredViewAsType(source, R.id.et_last_name, "field 'etLastName'", EditText.class);
    view = Utils.findRequiredView(source, R.id.et_dob, "field 'etDob' and method 'onViewClicked'");
    target.etDob = Utils.castView(view, R.id.et_dob, "field 'etDob'", EditText.class);
    view2131231039 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.etLicenceNumber = Utils.findRequiredViewAsType(source, R.id.et_licence_number, "field 'etLicenceNumber'", EditText.class);
    target.etCountry = Utils.findRequiredViewAsType(source, R.id.et_country, "field 'etCountry'", EditText.class);
    view = Utils.findRequiredView(source, R.id.et_issued_date, "field 'etIssuedDate' and method 'onViewClicked'");
    target.etIssuedDate = Utils.castView(view, R.id.et_issued_date, "field 'etIssuedDate'", EditText.class);
    view2131231075 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.et_expiry_date, "field 'etExpiryDate' and method 'onViewClicked'");
    target.etExpiryDate = Utils.castView(view, R.id.et_expiry_date, "field 'etExpiryDate'", EditText.class);
    view2131231062 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.etPhone = Utils.findRequiredViewAsType(source, R.id.et_phone, "field 'etPhone'", EditText.class);
    target.etEmail = Utils.findRequiredViewAsType(source, R.id.et_Email, "field 'etEmail'", EditText.class);
    target.etPresentAddress = Utils.findRequiredViewAsType(source, R.id.et_present_address, "field 'etPresentAddress'", AutoCompleteTextView.class);
    target.imgPresentAddress = Utils.findRequiredViewAsType(source, R.id.img_present_address, "field 'imgPresentAddress'", ImageView.class);
    target.etPermanantAddress = Utils.findRequiredViewAsType(source, R.id.et_permanant_address, "field 'etPermanantAddress'", AutoCompleteTextView.class);
    target.imgPermenantAddress = Utils.findRequiredViewAsType(source, R.id.img_permenant_address, "field 'imgPermenantAddress'", ImageView.class);
    target.driverProfilePic = Utils.findRequiredViewAsType(source, R.id.driver_profile_pic, "field 'driverProfilePic'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.linaer_capture_image, "field 'linaerCaptureImage' and method 'onViewClicked'");
    target.linaerCaptureImage = Utils.castView(view, R.id.linaer_capture_image, "field 'linaerCaptureImage'", LinearLayout.class);
    view2131231286 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_edit, "field 'btnEdit' and method 'onViewClicked'");
    target.btnEdit = Utils.castView(view, R.id.btn_edit, "field 'btnEdit'", Button.class);
    view2131230841 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_next, "field 'btnNext' and method 'onViewClicked'");
    target.btnNext = Utils.castView(view, R.id.btn_next, "field 'btnNext'", Button.class);
    view2131230850 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutPtofileInfo = Utils.findRequiredViewAsType(source, R.id.layout_ptofile_info, "field 'layoutPtofileInfo'", CardView.class);
    target.imgAddress = Utils.findRequiredViewAsType(source, R.id.img_address, "field 'imgAddress'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.btn_previous, "field 'btnPrevious' and method 'onViewClicked'");
    target.btnPrevious = Utils.castView(view, R.id.btn_previous, "field 'btnPrevious'", Button.class);
    view2131230859 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_start_admin_process, "field 'btnStartAdminProcess' and method 'onViewClicked'");
    target.btnStartAdminProcess = Utils.castView(view, R.id.btn_start_admin_process, "field 'btnStartAdminProcess'", Button.class);
    view2131230870 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutEmergencyContact = Utils.findRequiredViewAsType(source, R.id.layout_emergency_contact, "field 'layoutEmergencyContact'", CardView.class);
    view = Utils.findRequiredView(source, R.id.card_profile_info, "field 'cardProfileInfo' and method 'onViewClicked'");
    target.cardProfileInfo = Utils.castView(view, R.id.card_profile_info, "field 'cardProfileInfo'", CardView.class);
    view2131230910 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.card_emergency_contact, "field 'cardEmergencyContact' and method 'onViewClicked'");
    target.cardEmergencyContact = Utils.castView(view, R.id.card_emergency_contact, "field 'cardEmergencyContact'", CardView.class);
    view2131230900 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtProfile = Utils.findRequiredViewAsType(source, R.id.txtProfile, "field 'txtProfile'", TextView.class);
    target.txtEmergecy = Utils.findRequiredViewAsType(source, R.id.txtEmergecy, "field 'txtEmergecy'", TextView.class);
    target.etEcFirstName = Utils.findRequiredViewAsType(source, R.id.et_ec_first_name, "field 'etEcFirstName'", EditText.class);
    target.etEcLastName = Utils.findRequiredViewAsType(source, R.id.et_ec_last_name, "field 'etEcLastName'", EditText.class);
    target.etEcRelation = Utils.findRequiredViewAsType(source, R.id.et_ec_relation, "field 'etEcRelation'", EditText.class);
    target.etEcPhone = Utils.findRequiredViewAsType(source, R.id.et_ec_phone, "field 'etEcPhone'", EditText.class);
    target.etEcEmail = Utils.findRequiredViewAsType(source, R.id.et_ec_email, "field 'etEcEmail'", EditText.class);
    target.etAddress = Utils.findRequiredViewAsType(source, R.id.et_address, "field 'etAddress'", AutoCompleteTextView.class);
    view = Utils.findRequiredView(source, R.id.add_driver_document, "field 'addDriverDocument' and method 'onViewClicked'");
    target.addDriverDocument = Utils.castView(view, R.id.add_driver_document, "field 'addDriverDocument'", TextView.class);
    view2131230801 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rvDriverAttachment = Utils.findRequiredViewAsType(source, R.id.rv_driver_attachment, "field 'rvDriverAttachment'", RecyclerView.class);
    target.linearDriverAttachments = Utils.findRequiredViewAsType(source, R.id.linear_driver_attachments, "field 'linearDriverAttachments'", LinearLayout.class);
    target.txtPhysician = Utils.findRequiredViewAsType(source, R.id.txtPhysician, "field 'txtPhysician'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_physician_contact, "field 'cardPhysicianContact' and method 'onViewClicked'");
    target.cardPhysicianContact = Utils.castView(view, R.id.card_physician_contact, "field 'cardPhysicianContact'", CardView.class);
    view2131230909 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.etPciFirstName = Utils.findRequiredViewAsType(source, R.id.et_pci_first_name, "field 'etPciFirstName'", EditText.class);
    target.etPciLastName = Utils.findRequiredViewAsType(source, R.id.et_pci_last_name, "field 'etPciLastName'", EditText.class);
    target.etPciPhoneNumber = Utils.findRequiredViewAsType(source, R.id.et_pci_phone_number, "field 'etPciPhoneNumber'", EditText.class);
    target.etPciFaxNumber = Utils.findRequiredViewAsType(source, R.id.et_pci_fax_number, "field 'etPciFaxNumber'", EditText.class);
    target.etPciAddress = Utils.findRequiredViewAsType(source, R.id.et_pci_address, "field 'etPciAddress'", AutoCompleteTextView.class);
    target.imgPciAddress = Utils.findRequiredViewAsType(source, R.id.img_pci_address, "field 'imgPciAddress'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.btn_pci_previous, "field 'btnPciPrevious' and method 'onViewClicked'");
    target.btnPciPrevious = Utils.castView(view, R.id.btn_pci_previous, "field 'btnPciPrevious'", Button.class);
    view2131230856 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_pci_next, "field 'btnPciNext' and method 'onViewClicked'");
    target.btnPciNext = Utils.castView(view, R.id.btn_pci_next, "field 'btnPciNext'", Button.class);
    view2131230855 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutPhysicianContact = Utils.findRequiredViewAsType(source, R.id.layout_physician_contact, "field 'layoutPhysicianContact'", CardView.class);
    target.scrollView = Utils.findRequiredViewAsType(source, R.id.scrollView, "field 'scrollView'", NestedScrollView.class);
    target.textFeedback = Utils.findRequiredViewAsType(source, R.id.textFeedback, "field 'textFeedback'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_feedback, "field 'cardFeedback' and method 'onViewClicked'");
    target.cardFeedback = Utils.castView(view, R.id.card_feedback, "field 'cardFeedback'", CardView.class);
    view2131230904 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutCardFeedback = Utils.findRequiredViewAsType(source, R.id.layout_card_feedback, "field 'layoutCardFeedback'", CardView.class);
    target.etFbTrasportationComment = Utils.findRequiredViewAsType(source, R.id.et_fb_trasportation_comment, "field 'etFbTrasportationComment'", EditText.class);
    target.etFbSchoolComment = Utils.findRequiredViewAsType(source, R.id.et_fb_school_comment, "field 'etFbSchoolComment'", EditText.class);
    view = Utils.findRequiredView(source, R.id.btn_fb_previous, "field 'btnFbPrevious' and method 'onViewClicked'");
    target.btnFbPrevious = Utils.castView(view, R.id.btn_fb_previous, "field 'btnFbPrevious'", Button.class);
    view2131230846 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_fb_next, "field 'btnFbNext' and method 'onViewClicked'");
    target.btnFbNext = Utils.castView(view, R.id.btn_fb_next, "field 'btnFbNext'", Button.class);
    view2131230845 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rvEcAttachment = Utils.findRequiredViewAsType(source, R.id.rv_ec_attachment, "field 'rvEcAttachment'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.txt_ec_attachment, "field 'txtEcAttachment' and method 'onViewClicked'");
    target.txtEcAttachment = Utils.castView(view, R.id.txt_ec_attachment, "field 'txtEcAttachment'", TextView.class);
    view2131231637 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.imgQrcode = Utils.findRequiredViewAsType(source, R.id.img_qrcode, "field 'imgQrcode'", ImageView.class);
    target.txtQrNotAvail = Utils.findRequiredViewAsType(source, R.id.txt_qr_not_avail, "field 'txtQrNotAvail'", TextView.class);
    target.qrFrame = Utils.findRequiredViewAsType(source, R.id.qr_frame, "field 'qrFrame'", FrameLayout.class);
    target.countryCode = Utils.findRequiredViewAsType(source, R.id.country_code, "field 'countryCode'", CountryCodePicker.class);
    target.countryCodePci = Utils.findRequiredViewAsType(source, R.id.country_code_pci, "field 'countryCodePci'", CountryCodePicker.class);
    target.countryCodeEc = Utils.findRequiredViewAsType(source, R.id.country_code_ec, "field 'countryCodeEc'", CountryCodePicker.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AddDriverActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.etFirstName = null;
    target.etLastName = null;
    target.etDob = null;
    target.etLicenceNumber = null;
    target.etCountry = null;
    target.etIssuedDate = null;
    target.etExpiryDate = null;
    target.etPhone = null;
    target.etEmail = null;
    target.etPresentAddress = null;
    target.imgPresentAddress = null;
    target.etPermanantAddress = null;
    target.imgPermenantAddress = null;
    target.driverProfilePic = null;
    target.linaerCaptureImage = null;
    target.btnEdit = null;
    target.btnNext = null;
    target.layoutPtofileInfo = null;
    target.imgAddress = null;
    target.btnPrevious = null;
    target.btnStartAdminProcess = null;
    target.layoutEmergencyContact = null;
    target.cardProfileInfo = null;
    target.cardEmergencyContact = null;
    target.txtProfile = null;
    target.txtEmergecy = null;
    target.etEcFirstName = null;
    target.etEcLastName = null;
    target.etEcRelation = null;
    target.etEcPhone = null;
    target.etEcEmail = null;
    target.etAddress = null;
    target.addDriverDocument = null;
    target.rvDriverAttachment = null;
    target.linearDriverAttachments = null;
    target.txtPhysician = null;
    target.cardPhysicianContact = null;
    target.etPciFirstName = null;
    target.etPciLastName = null;
    target.etPciPhoneNumber = null;
    target.etPciFaxNumber = null;
    target.etPciAddress = null;
    target.imgPciAddress = null;
    target.btnPciPrevious = null;
    target.btnPciNext = null;
    target.layoutPhysicianContact = null;
    target.scrollView = null;
    target.textFeedback = null;
    target.cardFeedback = null;
    target.layoutCardFeedback = null;
    target.etFbTrasportationComment = null;
    target.etFbSchoolComment = null;
    target.btnFbPrevious = null;
    target.btnFbNext = null;
    target.rvEcAttachment = null;
    target.txtEcAttachment = null;
    target.imgQrcode = null;
    target.txtQrNotAvail = null;
    target.qrFrame = null;
    target.countryCode = null;
    target.countryCodePci = null;
    target.countryCodeEc = null;

    view2131231039.setOnClickListener(null);
    view2131231039 = null;
    view2131231075.setOnClickListener(null);
    view2131231075 = null;
    view2131231062.setOnClickListener(null);
    view2131231062 = null;
    view2131231286.setOnClickListener(null);
    view2131231286 = null;
    view2131230841.setOnClickListener(null);
    view2131230841 = null;
    view2131230850.setOnClickListener(null);
    view2131230850 = null;
    view2131230859.setOnClickListener(null);
    view2131230859 = null;
    view2131230870.setOnClickListener(null);
    view2131230870 = null;
    view2131230910.setOnClickListener(null);
    view2131230910 = null;
    view2131230900.setOnClickListener(null);
    view2131230900 = null;
    view2131230801.setOnClickListener(null);
    view2131230801 = null;
    view2131230909.setOnClickListener(null);
    view2131230909 = null;
    view2131230856.setOnClickListener(null);
    view2131230856 = null;
    view2131230855.setOnClickListener(null);
    view2131230855 = null;
    view2131230904.setOnClickListener(null);
    view2131230904 = null;
    view2131230846.setOnClickListener(null);
    view2131230846 = null;
    view2131230845.setOnClickListener(null);
    view2131230845 = null;
    view2131231637.setOnClickListener(null);
    view2131231637 = null;
  }
}
