// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.grade.assign.teacher.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AssignedTeacherAdapter$ItemViewHolder_ViewBinding implements Unbinder {
  private AssignedTeacherAdapter.ItemViewHolder target;

  @UiThread
  public AssignedTeacherAdapter$ItemViewHolder_ViewBinding(AssignedTeacherAdapter.ItemViewHolder target,
      View source) {
    this.target = target;

    target.txtTeacherName = Utils.findRequiredViewAsType(source, R.id.txt_teacher_name, "field 'txtTeacherName'", TextView.class);
    target.imgMore = Utils.findRequiredViewAsType(source, R.id.img_more, "field 'imgMore'", ImageView.class);
    target.txtEmail = Utils.findRequiredViewAsType(source, R.id.txt_email, "field 'txtEmail'", TextView.class);
    target.txtSubject = Utils.findRequiredViewAsType(source, R.id.txt_subject, "field 'txtSubject'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AssignedTeacherAdapter.ItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtTeacherName = null;
    target.imgMore = null;
    target.txtEmail = null;
    target.txtSubject = null;
  }
}
