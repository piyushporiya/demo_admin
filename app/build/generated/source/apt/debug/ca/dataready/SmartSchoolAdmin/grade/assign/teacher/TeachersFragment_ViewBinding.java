// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.grade.assign.teacher;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class TeachersFragment_ViewBinding implements Unbinder {
  private TeachersFragment target;

  private View view2131231154;

  private View view2131231430;

  @UiThread
  public TeachersFragment_ViewBinding(final TeachersFragment target, View source) {
    this.target = target;

    View view;
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", SuperRecyclerView.class);
    view = Utils.findRequiredView(source, R.id.fab_create, "field 'fabCreate' and method 'onViewClicked'");
    target.fabCreate = Utils.castView(view, R.id.fab_create, "field 'fabCreate'", FloatingActionButton.class);
    view2131231154 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.etWeekDay = Utils.findRequiredViewAsType(source, R.id.et_week_day, "field 'etWeekDay'", TextView.class);
    view = Utils.findRequiredView(source, R.id.rl_week_day, "field 'rlWeekDay' and method 'onViewClicked'");
    target.rlWeekDay = Utils.castView(view, R.id.rl_week_day, "field 'rlWeekDay'", RelativeLayout.class);
    view2131231430 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    TeachersFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.recyclerView = null;
    target.fabCreate = null;
    target.etWeekDay = null;
    target.rlWeekDay = null;

    view2131231154.setOnClickListener(null);
    view2131231154 = null;
    view2131231430.setOnClickListener(null);
    view2131231430 = null;
  }
}
