// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.route.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RoutesAdapter$ItemViewHolder_ViewBinding implements Unbinder {
  private RoutesAdapter.ItemViewHolder target;

  @UiThread
  public RoutesAdapter$ItemViewHolder_ViewBinding(RoutesAdapter.ItemViewHolder target,
      View source) {
    this.target = target;

    target.txtDriverName = Utils.findRequiredViewAsType(source, R.id.txt_driver_name, "field 'txtDriverName'", TextView.class);
    target.imgMore = Utils.findRequiredViewAsType(source, R.id.img_more, "field 'imgMore'", ImageView.class);
    target.txtRouteName = Utils.findRequiredViewAsType(source, R.id.txt_route_name, "field 'txtRouteName'", TextView.class);
    target.txtRouteType = Utils.findRequiredViewAsType(source, R.id.txt_route_type, "field 'txtRouteType'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RoutesAdapter.ItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtDriverName = null;
    target.imgMore = null;
    target.txtRouteName = null;
    target.txtRouteType = null;
  }
}
