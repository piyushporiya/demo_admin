// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.route.history.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class TripsOfRouteAdapter$ItemViewHolder_ViewBinding implements Unbinder {
  private TripsOfRouteAdapter.ItemViewHolder target;

  @UiThread
  public TripsOfRouteAdapter$ItemViewHolder_ViewBinding(TripsOfRouteAdapter.ItemViewHolder target,
      View source) {
    this.target = target;

    target.txtTripName = Utils.findRequiredViewAsType(source, R.id.txt_trip_name, "field 'txtTripName'", TextView.class);
    target.rl = Utils.findRequiredViewAsType(source, R.id.rl, "field 'rl'", LinearLayout.class);
    target.txtStatus = Utils.findRequiredViewAsType(source, R.id.txt_status, "field 'txtStatus'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    TripsOfRouteAdapter.ItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtTripName = null;
    target.rl = null;
    target.txtStatus = null;
  }
}
