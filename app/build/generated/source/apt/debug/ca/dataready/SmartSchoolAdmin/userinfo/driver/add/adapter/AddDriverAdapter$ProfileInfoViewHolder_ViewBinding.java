// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.userinfo.driver.add.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddDriverAdapter$ProfileInfoViewHolder_ViewBinding implements Unbinder {
  private AddDriverAdapter.ProfileInfoViewHolder target;

  @UiThread
  public AddDriverAdapter$ProfileInfoViewHolder_ViewBinding(AddDriverAdapter.ProfileInfoViewHolder target,
      View source) {
    this.target = target;

    target.imgPresentAddress = Utils.findRequiredViewAsType(source, R.id.img_present_address, "field 'imgPresentAddress'", ImageView.class);
    target.imgPermenantAddress = Utils.findRequiredViewAsType(source, R.id.img_permenant_address, "field 'imgPermenantAddress'", ImageView.class);
    target.layoutPtofileInfo = Utils.findRequiredViewAsType(source, R.id.txtProfile, "field 'layoutPtofileInfo'", CardView.class);
    target.btnNext = Utils.findRequiredViewAsType(source, R.id.btn_next, "field 'btnNext'", Button.class);
    target.txtHeaderTitle = Utils.findRequiredViewAsType(source, R.id.txt_header_title, "field 'txtHeaderTitle'", TextView.class);
    target.etFirstName = Utils.findRequiredViewAsType(source, R.id.et_first_name, "field 'etFirstName'", EditText.class);
    target.etLastName = Utils.findRequiredViewAsType(source, R.id.et_last_name, "field 'etLastName'", EditText.class);
    target.etDob = Utils.findRequiredViewAsType(source, R.id.et_dob, "field 'etDob'", EditText.class);
    target.etLicenceNumber = Utils.findRequiredViewAsType(source, R.id.et_licence_number, "field 'etLicenceNumber'", EditText.class);
    target.etCountry = Utils.findRequiredViewAsType(source, R.id.et_country, "field 'etCountry'", EditText.class);
    target.etIssuedDate = Utils.findRequiredViewAsType(source, R.id.et_issued_date, "field 'etIssuedDate'", EditText.class);
    target.etExpiryDate = Utils.findRequiredViewAsType(source, R.id.et_expiry_date, "field 'etExpiryDate'", EditText.class);
    target.etPhone = Utils.findRequiredViewAsType(source, R.id.et_phone, "field 'etPhone'", EditText.class);
    target.etEmail = Utils.findRequiredViewAsType(source, R.id.et_Email, "field 'etEmail'", EditText.class);
    target.etPresentAddress = Utils.findRequiredViewAsType(source, R.id.et_present_address, "field 'etPresentAddress'", EditText.class);
    target.etPermanantAddress = Utils.findRequiredViewAsType(source, R.id.et_permanant_address, "field 'etPermanantAddress'", EditText.class);
    target.driverProfilePic = Utils.findRequiredViewAsType(source, R.id.driver_profile_pic, "field 'driverProfilePic'", ImageView.class);
    target.linaerCaptureImage = Utils.findRequiredViewAsType(source, R.id.linaer_capture_image, "field 'linaerCaptureImage'", LinearLayout.class);
    target.btnEdit = Utils.findRequiredViewAsType(source, R.id.btn_edit, "field 'btnEdit'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AddDriverAdapter.ProfileInfoViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.imgPresentAddress = null;
    target.imgPermenantAddress = null;
    target.layoutPtofileInfo = null;
    target.btnNext = null;
    target.txtHeaderTitle = null;
    target.etFirstName = null;
    target.etLastName = null;
    target.etDob = null;
    target.etLicenceNumber = null;
    target.etCountry = null;
    target.etIssuedDate = null;
    target.etExpiryDate = null;
    target.etPhone = null;
    target.etEmail = null;
    target.etPresentAddress = null;
    target.etPermanantAddress = null;
    target.driverProfilePic = null;
    target.linaerCaptureImage = null;
    target.btnEdit = null;
  }
}
