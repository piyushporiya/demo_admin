// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.classchannel.create;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CreateClassChannelActivity_ViewBinding implements Unbinder {
  private CreateClassChannelActivity target;

  private View view2131231243;

  private View view2131231198;

  private View view2131231245;

  private View view2131231486;

  private View view2131231291;

  private View view2131231292;

  @UiThread
  public CreateClassChannelActivity_ViewBinding(CreateClassChannelActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CreateClassChannelActivity_ViewBinding(final CreateClassChannelActivity target,
      View source) {
    this.target = target;

    View view;
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.llheader = Utils.findRequiredViewAsType(source, R.id.llheader, "field 'llheader'", LinearLayout.class);
    target.title = Utils.findRequiredViewAsType(source, R.id.title, "field 'title'", EditText.class);
    target.msg = Utils.findRequiredViewAsType(source, R.id.msg, "field 'msg'", EditText.class);
    view = Utils.findRequiredView(source, R.id.iv_SpeechToText, "field 'ivSpeechToText' and method 'onViewClicked'");
    target.ivSpeechToText = Utils.castView(view, R.id.iv_SpeechToText, "field 'ivSpeechToText'", ImageView.class);
    view2131231243 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.img_attachment, "field 'imgAttachment' and method 'onViewClicked'");
    target.imgAttachment = Utils.castView(view, R.id.img_attachment, "field 'imgAttachment'", ImageView.class);
    view2131231198 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.iv_camera_AttachFile, "field 'ivCameraAttachFile' and method 'onViewClicked'");
    target.ivCameraAttachFile = Utils.castView(view, R.id.iv_camera_AttachFile, "field 'ivCameraAttachFile'", ImageView.class);
    view2131231245 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.sendfeedback, "field 'sendfeedback' and method 'onViewClicked'");
    target.sendfeedback = Utils.castView(view, R.id.sendfeedback, "field 'sendfeedback'", Button.class);
    view2131231486 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtAttachment = Utils.findRequiredViewAsType(source, R.id.txt_attachment, "field 'txtAttachment'", TextView.class);
    target.rvAttachments = Utils.findRequiredViewAsType(source, R.id.rvAttachments, "field 'rvAttachments'", RecyclerView.class);
    target.llAttachments = Utils.findRequiredViewAsType(source, R.id.llAttachments, "field 'llAttachments'", LinearLayout.class);
    target.llTextArea = Utils.findRequiredViewAsType(source, R.id.llTextArea, "field 'llTextArea'", LinearLayout.class);
    target.parentViewAnimator = Utils.findRequiredViewAsType(source, R.id.parent_viewAnimator, "field 'parentViewAnimator'", ViewAnimator.class);
    target.txtGrade = Utils.findRequiredViewAsType(source, R.id.txt_Grade, "field 'txtGrade'", TextView.class);
    view = Utils.findRequiredView(source, R.id.linear_Grade, "field 'linearGrade' and method 'onViewClicked'");
    target.linearGrade = Utils.castView(view, R.id.linear_Grade, "field 'linearGrade'", RelativeLayout.class);
    view2131231291 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtClass = Utils.findRequiredViewAsType(source, R.id.txt_Class, "field 'txtClass'", TextView.class);
    view = Utils.findRequiredView(source, R.id.linear_Section, "field 'linearSection' and method 'onViewClicked'");
    target.linearSection = Utils.castView(view, R.id.linear_Section, "field 'linearSection'", RelativeLayout.class);
    view2131231292 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.main = Utils.findRequiredViewAsType(source, R.id.main, "field 'main'", RelativeLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CreateClassChannelActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.llheader = null;
    target.title = null;
    target.msg = null;
    target.ivSpeechToText = null;
    target.imgAttachment = null;
    target.ivCameraAttachFile = null;
    target.sendfeedback = null;
    target.txtAttachment = null;
    target.rvAttachments = null;
    target.llAttachments = null;
    target.llTextArea = null;
    target.parentViewAnimator = null;
    target.txtGrade = null;
    target.linearGrade = null;
    target.txtClass = null;
    target.linearSection = null;
    target.main = null;

    view2131231243.setOnClickListener(null);
    view2131231243 = null;
    view2131231198.setOnClickListener(null);
    view2131231198 = null;
    view2131231245.setOnClickListener(null);
    view2131231245 = null;
    view2131231486.setOnClickListener(null);
    view2131231486 = null;
    view2131231291.setOnClickListener(null);
    view2131231291 = null;
    view2131231292.setOnClickListener(null);
    view2131231292 = null;
  }
}
