// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.viewresponse.create;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PostMessageActivity_ViewBinding implements Unbinder {
  private PostMessageActivity target;

  private View view2131231291;

  private View view2131231292;

  private View view2131231486;

  private View view2131231638;

  private View view2131231243;

  private View view2131231198;

  private View view2131231245;

  @UiThread
  public PostMessageActivity_ViewBinding(PostMessageActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public PostMessageActivity_ViewBinding(final PostMessageActivity target, View source) {
    this.target = target;

    View view;
    target.txtGrade = Utils.findRequiredViewAsType(source, R.id.txt_Grade, "field 'txtGrade'", TextView.class);
    target.gradeImg = Utils.findRequiredViewAsType(source, R.id.grade_img, "field 'gradeImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.linear_Grade, "field 'linearGrade' and method 'onViewClicked'");
    target.linearGrade = Utils.castView(view, R.id.linear_Grade, "field 'linearGrade'", RelativeLayout.class);
    view2131231291 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtClass = Utils.findRequiredViewAsType(source, R.id.txt_Class, "field 'txtClass'", TextView.class);
    target.classImg = Utils.findRequiredViewAsType(source, R.id.class_img, "field 'classImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.linear_Section, "field 'linearSection' and method 'onViewClicked'");
    target.linearSection = Utils.castView(view, R.id.linear_Section, "field 'linearSection'", RelativeLayout.class);
    view2131231292 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rvStudents = Utils.findRequiredViewAsType(source, R.id.rv_Students, "field 'rvStudents'", RecyclerView.class);
    target.txtNoData = Utils.findRequiredViewAsType(source, R.id.txt_NoData, "field 'txtNoData'", TextView.class);
    target.studentListViewAnimator = Utils.findRequiredViewAsType(source, R.id.student_list_viewAnimator, "field 'studentListViewAnimator'", ViewAnimator.class);
    target.title = Utils.findRequiredViewAsType(source, R.id.title, "field 'title'", EditText.class);
    target.msg = Utils.findRequiredViewAsType(source, R.id.msg, "field 'msg'", EditText.class);
    view = Utils.findRequiredView(source, R.id.sendfeedback, "field 'sendfeedback' and method 'onViewClicked'");
    target.sendfeedback = Utils.castView(view, R.id.sendfeedback, "field 'sendfeedback'", Button.class);
    view2131231486 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.llheader = Utils.findRequiredViewAsType(source, R.id.llheader, "field 'llheader'", LinearLayout.class);
    target.llTextArea = Utils.findRequiredViewAsType(source, R.id.llTextArea, "field 'llTextArea'", LinearLayout.class);
    target.parentViewAnimator = Utils.findRequiredViewAsType(source, R.id.parent_viewAnimator, "field 'parentViewAnimator'", ViewAnimator.class);
    view = Utils.findRequiredView(source, R.id.txt_edit, "field 'txtEdit' and method 'onViewClicked'");
    target.txtEdit = Utils.castView(view, R.id.txt_edit, "field 'txtEdit'", TextView.class);
    view2131231638 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtAttachment = Utils.findRequiredViewAsType(source, R.id.txt_attachment, "field 'txtAttachment'", TextView.class);
    target.rvAttachments = Utils.findRequiredViewAsType(source, R.id.rvAttachments, "field 'rvAttachments'", RecyclerView.class);
    target.llAttachments = Utils.findRequiredViewAsType(source, R.id.llAttachments, "field 'llAttachments'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.iv_SpeechToText, "method 'onViewClicked'");
    view2131231243 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.img_attachment, "method 'onViewClicked'");
    view2131231198 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.iv_camera_AttachFile, "method 'onViewClicked'");
    view2131231245 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    PostMessageActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtGrade = null;
    target.gradeImg = null;
    target.linearGrade = null;
    target.txtClass = null;
    target.classImg = null;
    target.linearSection = null;
    target.rvStudents = null;
    target.txtNoData = null;
    target.studentListViewAnimator = null;
    target.title = null;
    target.msg = null;
    target.sendfeedback = null;
    target.toolbar = null;
    target.llheader = null;
    target.llTextArea = null;
    target.parentViewAnimator = null;
    target.txtEdit = null;
    target.txtAttachment = null;
    target.rvAttachments = null;
    target.llAttachments = null;

    view2131231291.setOnClickListener(null);
    view2131231291 = null;
    view2131231292.setOnClickListener(null);
    view2131231292 = null;
    view2131231486.setOnClickListener(null);
    view2131231486 = null;
    view2131231638.setOnClickListener(null);
    view2131231638 = null;
    view2131231243.setOnClickListener(null);
    view2131231243 = null;
    view2131231198.setOnClickListener(null);
    view2131231198 = null;
    view2131231245.setOnClickListener(null);
    view2131231245 = null;
  }
}
