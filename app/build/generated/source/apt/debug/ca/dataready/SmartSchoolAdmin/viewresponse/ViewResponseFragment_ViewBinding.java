// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.viewresponse;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ViewResponseFragment_ViewBinding implements Unbinder {
  private ViewResponseFragment target;

  private View view2131231153;

  @UiThread
  public ViewResponseFragment_ViewBinding(final ViewResponseFragment target, View source) {
    this.target = target;

    View view;
    target.rvCommunication = Utils.findRequiredViewAsType(source, R.id.rv_communication, "field 'rvCommunication'", RecyclerView.class);
    target.txtNoData = Utils.findRequiredViewAsType(source, R.id.txt_no_data, "field 'txtNoData'", TextView.class);
    target.viewAnimator = Utils.findRequiredViewAsType(source, R.id.viewAnimator, "field 'viewAnimator'", ViewAnimator.class);
    view = Utils.findRequiredView(source, R.id.fab_add_comm, "field 'fabAddComm' and method 'onViewClicked'");
    target.fabAddComm = Utils.castView(view, R.id.fab_add_comm, "field 'fabAddComm'", FloatingActionButton.class);
    view2131231153 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
    target.swipeRefreshLayout = Utils.findRequiredViewAsType(source, R.id.swipeRefreshLayout, "field 'swipeRefreshLayout'", SwipeRefreshLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ViewResponseFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rvCommunication = null;
    target.txtNoData = null;
    target.viewAnimator = null;
    target.fabAddComm = null;
    target.swipeRefreshLayout = null;

    view2131231153.setOnClickListener(null);
    view2131231153 = null;
  }
}
