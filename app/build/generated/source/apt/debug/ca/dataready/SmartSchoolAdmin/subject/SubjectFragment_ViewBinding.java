// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.subject;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SubjectFragment_ViewBinding implements Unbinder {
  private SubjectFragment target;

  private View view2131231154;

  @UiThread
  public SubjectFragment_ViewBinding(final SubjectFragment target, View source) {
    this.target = target;

    View view;
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", SuperRecyclerView.class);
    view = Utils.findRequiredView(source, R.id.fab_create, "field 'fabCreate' and method 'onViewClicked'");
    target.fabCreate = Utils.castView(view, R.id.fab_create, "field 'fabCreate'", FloatingActionButton.class);
    view2131231154 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SubjectFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.recyclerView = null;
    target.fabCreate = null;

    view2131231154.setOnClickListener(null);
    view2131231154 = null;
  }
}
