// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.grade.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ClassAdapter$AddMoreViewHolder_ViewBinding implements Unbinder {
  private ClassAdapter.AddMoreViewHolder target;

  @UiThread
  public ClassAdapter$AddMoreViewHolder_ViewBinding(ClassAdapter.AddMoreViewHolder target,
      View source) {
    this.target = target;

    target.txtClass = Utils.findRequiredViewAsType(source, R.id.txt_class, "field 'txtClass'", TextView.class);
    target.cardAddMore = Utils.findRequiredViewAsType(source, R.id.card_add_more, "field 'cardAddMore'", CardView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ClassAdapter.AddMoreViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtClass = null;
    target.cardAddMore = null;
  }
}
