// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.help;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class HelpFragment_ViewBinding implements Unbinder {
  private HelpFragment target;

  @UiThread
  public HelpFragment_ViewBinding(HelpFragment target, View source) {
    this.target = target;

    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", SuperRecyclerView.class);
    target.txtApp = Utils.findRequiredViewAsType(source, R.id.txt_app, "field 'txtApp'", TextView.class);
    target.txtApi = Utils.findRequiredViewAsType(source, R.id.txt_api, "field 'txtApi'", TextView.class);
    target.txtEnv = Utils.findRequiredViewAsType(source, R.id.txt_env, "field 'txtEnv'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    HelpFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.recyclerView = null;
    target.txtApp = null;
    target.txtApi = null;
    target.txtEnv = null;
  }
}
