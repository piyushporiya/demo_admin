// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.Utilities;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class KotlinActivity_ViewBinding implements Unbinder {
  private KotlinActivity target;

  @UiThread
  public KotlinActivity_ViewBinding(KotlinActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public KotlinActivity_ViewBinding(KotlinActivity target, View source) {
    this.target = target;

    target.etEmail = Utils.findRequiredViewAsType(source, R.id.et_email, "field 'etEmail'", EditText.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    KotlinActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.etEmail = null;
  }
}
