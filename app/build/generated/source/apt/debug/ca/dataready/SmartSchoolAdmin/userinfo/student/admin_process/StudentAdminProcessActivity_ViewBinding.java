// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.userinfo.student.admin_process;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import com.facebook.drawee.view.SimpleDraweeView;
import com.hbb20.CountryCodePicker;
import java.lang.IllegalStateException;
import java.lang.Override;

public class StudentAdminProcessActivity_ViewBinding implements Unbinder {
  private StudentAdminProcessActivity target;

  private View view2131230895;

  private View view2131231025;

  private View view2131231291;

  private View view2131231290;

  private View view2131230847;

  private View view2131230871;

  @UiThread
  public StudentAdminProcessActivity_ViewBinding(StudentAdminProcessActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public StudentAdminProcessActivity_ViewBinding(final StudentAdminProcessActivity target,
      View source) {
    this.target = target;

    View view;
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.txtAdminProcess = Utils.findRequiredViewAsType(source, R.id.txtAdminProcess, "field 'txtAdminProcess'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_admin_process, "field 'cardAdminProcess' and method 'onViewClicked'");
    target.cardAdminProcess = Utils.castView(view, R.id.card_admin_process, "field 'cardAdminProcess'", CardView.class);
    view2131230895 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.ivStudentImage = Utils.findRequiredViewAsType(source, R.id.iv_student_image, "field 'ivStudentImage'", SimpleDraweeView.class);
    target.txtStudentName = Utils.findRequiredViewAsType(source, R.id.txt_student_name, "field 'txtStudentName'", TextView.class);
    target.txtStudentId = Utils.findRequiredViewAsType(source, R.id.txt_student_id, "field 'txtStudentId'", TextView.class);
    view = Utils.findRequiredView(source, R.id.et_acedamic_year, "field 'etAcedamicYear' and method 'onViewClicked'");
    target.etAcedamicYear = Utils.castView(view, R.id.et_acedamic_year, "field 'etAcedamicYear'", EditText.class);
    view2131231025 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.etEmailId = Utils.findRequiredViewAsType(source, R.id.et_emailId, "field 'etEmailId'", EditText.class);
    target.etPhone = Utils.findRequiredViewAsType(source, R.id.et_phone, "field 'etPhone'", EditText.class);
    target.txtGrade = Utils.findRequiredViewAsType(source, R.id.txt_Grade, "field 'txtGrade'", TextView.class);
    target.gradeImg = Utils.findRequiredViewAsType(source, R.id.grade_img, "field 'gradeImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.linear_Grade, "field 'linearGrade' and method 'onViewClicked'");
    target.linearGrade = Utils.castView(view, R.id.linear_Grade, "field 'linearGrade'", RelativeLayout.class);
    view2131231291 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtClass = Utils.findRequiredViewAsType(source, R.id.txt_Class, "field 'txtClass'", TextView.class);
    target.classImg = Utils.findRequiredViewAsType(source, R.id.class_img, "field 'classImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.linear_Class, "field 'linearClass' and method 'onViewClicked'");
    target.linearClass = Utils.castView(view, R.id.linear_Class, "field 'linearClass'", RelativeLayout.class);
    view2131231290 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_fill_later, "field 'btnFillLater' and method 'onViewClicked'");
    target.btnFillLater = Utils.castView(view, R.id.btn_fill_later, "field 'btnFillLater'", Button.class);
    view2131230847 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_submit, "field 'btnSubmit' and method 'onViewClicked'");
    target.btnSubmit = Utils.castView(view, R.id.btn_submit, "field 'btnSubmit'", Button.class);
    view2131230871 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutAdminProcess = Utils.findRequiredViewAsType(source, R.id.layout_admin_process, "field 'layoutAdminProcess'", CardView.class);
    target.scrollView = Utils.findRequiredViewAsType(source, R.id.scrollView, "field 'scrollView'", NestedScrollView.class);
    target.txtError = Utils.findRequiredViewAsType(source, R.id.txt_error, "field 'txtError'", TextView.class);
    target.viewAnimator = Utils.findRequiredViewAsType(source, R.id.viewAnimator, "field 'viewAnimator'", ViewAnimator.class);
    target.countryCode = Utils.findRequiredViewAsType(source, R.id.country_code, "field 'countryCode'", CountryCodePicker.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    StudentAdminProcessActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.txtAdminProcess = null;
    target.cardAdminProcess = null;
    target.ivStudentImage = null;
    target.txtStudentName = null;
    target.txtStudentId = null;
    target.etAcedamicYear = null;
    target.etEmailId = null;
    target.etPhone = null;
    target.txtGrade = null;
    target.gradeImg = null;
    target.linearGrade = null;
    target.txtClass = null;
    target.classImg = null;
    target.linearClass = null;
    target.btnFillLater = null;
    target.btnSubmit = null;
    target.layoutAdminProcess = null;
    target.scrollView = null;
    target.txtError = null;
    target.viewAnimator = null;
    target.countryCode = null;

    view2131230895.setOnClickListener(null);
    view2131230895 = null;
    view2131231025.setOnClickListener(null);
    view2131231025 = null;
    view2131231291.setOnClickListener(null);
    view2131231291 = null;
    view2131231290.setOnClickListener(null);
    view2131231290 = null;
    view2131230847.setOnClickListener(null);
    view2131230847 = null;
    view2131230871.setOnClickListener(null);
    view2131230871 = null;
  }
}
