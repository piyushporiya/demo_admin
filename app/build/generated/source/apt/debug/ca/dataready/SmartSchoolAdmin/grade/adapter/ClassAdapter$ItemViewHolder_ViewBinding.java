// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.grade.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ClassAdapter$ItemViewHolder_ViewBinding implements Unbinder {
  private ClassAdapter.ItemViewHolder target;

  @UiThread
  public ClassAdapter$ItemViewHolder_ViewBinding(ClassAdapter.ItemViewHolder target, View source) {
    this.target = target;

    target.txtClass = Utils.findRequiredViewAsType(source, R.id.txt_class, "field 'txtClass'", TextView.class);
    target.imgEdit = Utils.findRequiredViewAsType(source, R.id.img_edit, "field 'imgEdit'", ImageView.class);
    target.imgDelete = Utils.findRequiredViewAsType(source, R.id.img_delete, "field 'imgDelete'", ImageView.class);
    target.cardGrade = Utils.findRequiredViewAsType(source, R.id.card_grade, "field 'cardGrade'", CardView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ClassAdapter.ItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtClass = null;
    target.imgEdit = null;
    target.imgDelete = null;
    target.cardGrade = null;
  }
}
