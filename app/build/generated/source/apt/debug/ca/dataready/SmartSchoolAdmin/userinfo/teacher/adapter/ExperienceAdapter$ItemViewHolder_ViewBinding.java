// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.userinfo.teacher.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ExperienceAdapter$ItemViewHolder_ViewBinding implements Unbinder {
  private ExperienceAdapter.ItemViewHolder target;

  @UiThread
  public ExperienceAdapter$ItemViewHolder_ViewBinding(ExperienceAdapter.ItemViewHolder target,
      View source) {
    this.target = target;

    target.etOrganizationName = Utils.findRequiredViewAsType(source, R.id.et_organization_name, "field 'etOrganizationName'", EditText.class);
    target.etValidFrom = Utils.findRequiredViewAsType(source, R.id.et_valid_from, "field 'etValidFrom'", EditText.class);
    target.etValidTill = Utils.findRequiredViewAsType(source, R.id.et_valid_till, "field 'etValidTill'", EditText.class);
    target.imgRemove = Utils.findRequiredViewAsType(source, R.id.img_remove, "field 'imgRemove'", ImageView.class);
    target.etAddress = Utils.findRequiredViewAsType(source, R.id.et_address, "field 'etAddress'", EditText.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ExperienceAdapter.ItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.etOrganizationName = null;
    target.etValidFrom = null;
    target.etValidTill = null;
    target.imgRemove = null;
    target.etAddress = null;
  }
}
