// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.appointment.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class UpcomingAdapter$DataViewHolder_ViewBinding implements Unbinder {
  private UpcomingAdapter.DataViewHolder target;

  @UiThread
  public UpcomingAdapter$DataViewHolder_ViewBinding(UpcomingAdapter.DataViewHolder target,
      View source) {
    this.target = target;

    target.txtShowDate = Utils.findRequiredViewAsType(source, R.id.txt_show_date, "field 'txtShowDate'", TextView.class);
    target.showMonth = Utils.findRequiredViewAsType(source, R.id.show_month, "field 'showMonth'", TextView.class);
    target.txtTitle = Utils.findRequiredViewAsType(source, R.id.txtTitle, "field 'txtTitle'", TextView.class);
    target.txtsubject = Utils.findRequiredViewAsType(source, R.id.txtsubject, "field 'txtsubject'", TextView.class);
    target.txtTeacher = Utils.findRequiredViewAsType(source, R.id.txtTeacher, "field 'txtTeacher'", TextView.class);
    target.llButtons = Utils.findRequiredViewAsType(source, R.id.llButtons, "field 'llButtons'", LinearLayout.class);
    target.btnConfirm = Utils.findRequiredViewAsType(source, R.id.btn_confirm, "field 'btnConfirm'", TextView.class);
    target.btnCancel = Utils.findRequiredViewAsType(source, R.id.btn_cancel, "field 'btnCancel'", TextView.class);
    target.txtStudent = Utils.findRequiredViewAsType(source, R.id.txtStudent, "field 'txtStudent'", TextView.class);
    target.txtFileName = Utils.findRequiredViewAsType(source, R.id.txt_file_name, "field 'txtFileName'", TextView.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    target.txtViewMore = Utils.findRequiredViewAsType(source, R.id.txt_view_more, "field 'txtViewMore'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    UpcomingAdapter.DataViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtShowDate = null;
    target.showMonth = null;
    target.txtTitle = null;
    target.txtsubject = null;
    target.txtTeacher = null;
    target.llButtons = null;
    target.btnConfirm = null;
    target.btnCancel = null;
    target.txtStudent = null;
    target.txtFileName = null;
    target.recyclerView = null;
    target.txtViewMore = null;
  }
}
