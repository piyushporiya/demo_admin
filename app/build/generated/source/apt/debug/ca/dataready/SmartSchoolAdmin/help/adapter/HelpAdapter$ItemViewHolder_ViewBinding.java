// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.help.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class HelpAdapter$ItemViewHolder_ViewBinding implements Unbinder {
  private HelpAdapter.ItemViewHolder target;

  @UiThread
  public HelpAdapter$ItemViewHolder_ViewBinding(HelpAdapter.ItemViewHolder target, View source) {
    this.target = target;

    target.txtQuestion = Utils.findRequiredViewAsType(source, R.id.txt_question, "field 'txtQuestion'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    HelpAdapter.ItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtQuestion = null;
  }
}
