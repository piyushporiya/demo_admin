// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.grade.assign.teacher.add;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AssignTeacherActivity_ViewBinding implements Unbinder {
  private AssignTeacherActivity target;

  private View view2131230871;

  private View view2131230897;

  private View view2131231134;

  private View view2131231133;

  private View view2131231142;

  private View view2131231137;

  @UiThread
  public AssignTeacherActivity_ViewBinding(AssignTeacherActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AssignTeacherActivity_ViewBinding(final AssignTeacherActivity target, View source) {
    this.target = target;

    View view;
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    view = Utils.findRequiredView(source, R.id.btn_submit, "field 'btnSubmit' and method 'onViewClicked'");
    target.btnSubmit = Utils.castView(view, R.id.btn_submit, "field 'btnSubmit'", Button.class);
    view2131230871 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtAssignTeacher = Utils.findRequiredViewAsType(source, R.id.txtAssignTeacher, "field 'txtAssignTeacher'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_assign_teacher, "field 'cardAssignTeacher' and method 'onViewClicked'");
    target.cardAssignTeacher = Utils.castView(view, R.id.card_assign_teacher, "field 'cardAssignTeacher'", CardView.class);
    view2131230897 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.et_teacher_name, "field 'etTeacherName' and method 'onViewClicked'");
    target.etTeacherName = Utils.castView(view, R.id.et_teacher_name, "field 'etTeacherName'", EditText.class);
    view2131231134 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.et_subject_name, "field 'etSubjectName' and method 'onViewClicked'");
    target.etSubjectName = Utils.castView(view, R.id.et_subject_name, "field 'etSubjectName'", EditText.class);
    view2131231133 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.et_week_day, "field 'etWeekDay' and method 'onViewClicked'");
    target.etWeekDay = Utils.castView(view, R.id.et_week_day, "field 'etWeekDay'", EditText.class);
    view2131231142 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.et_time, "field 'etTime' and method 'onViewClicked'");
    target.etTime = Utils.castView(view, R.id.et_time, "field 'etTime'", EditText.class);
    view2131231137 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutAssignTeacher = Utils.findRequiredViewAsType(source, R.id.layout_assign_teacher, "field 'layoutAssignTeacher'", CardView.class);
    target.viewAnimator = Utils.findRequiredViewAsType(source, R.id.viewAnimator, "field 'viewAnimator'", ViewAnimator.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AssignTeacherActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.btnSubmit = null;
    target.txtAssignTeacher = null;
    target.cardAssignTeacher = null;
    target.etTeacherName = null;
    target.etSubjectName = null;
    target.etWeekDay = null;
    target.etTime = null;
    target.layoutAssignTeacher = null;
    target.viewAnimator = null;

    view2131230871.setOnClickListener(null);
    view2131230871 = null;
    view2131230897.setOnClickListener(null);
    view2131230897 = null;
    view2131231134.setOnClickListener(null);
    view2131231134 = null;
    view2131231133.setOnClickListener(null);
    view2131231133 = null;
    view2131231142.setOnClickListener(null);
    view2131231142 = null;
    view2131231137.setOnClickListener(null);
    view2131231137 = null;
  }
}
