// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.appointment.upcoming;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class UpcomingAppointmentFragment_ViewBinding implements Unbinder {
  private UpcomingAppointmentFragment target;

  @UiThread
  public UpcomingAppointmentFragment_ViewBinding(UpcomingAppointmentFragment target, View source) {
    this.target = target;

    target.rvUpcoming = Utils.findRequiredViewAsType(source, R.id.rv_upcoming, "field 'rvUpcoming'", RecyclerView.class);
    target.txtNoUpcomingData = Utils.findRequiredViewAsType(source, R.id.txt_no_upcoming_data, "field 'txtNoUpcomingData'", TextView.class);
    target.upcomingViewAnimator = Utils.findRequiredViewAsType(source, R.id.upcoming_viewAnimator, "field 'upcomingViewAnimator'", ViewAnimator.class);
    target.swipeRefreshLayoutUpcoming = Utils.findRequiredViewAsType(source, R.id.swipeRefreshLayoutUpcoming, "field 'swipeRefreshLayoutUpcoming'", SwipeRefreshLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    UpcomingAppointmentFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rvUpcoming = null;
    target.txtNoUpcomingData = null;
    target.upcomingViewAnimator = null;
    target.swipeRefreshLayoutUpcoming = null;
  }
}
