// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.schoolchannel.read;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.Toolbar;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ReadSchoolChannelNotificationActivity_ViewBinding implements Unbinder {
  private ReadSchoolChannelNotificationActivity target;

  @UiThread
  public ReadSchoolChannelNotificationActivity_ViewBinding(ReadSchoolChannelNotificationActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ReadSchoolChannelNotificationActivity_ViewBinding(ReadSchoolChannelNotificationActivity target,
      View source) {
    this.target = target;

    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", SuperRecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ReadSchoolChannelNotificationActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.recyclerView = null;
  }
}
