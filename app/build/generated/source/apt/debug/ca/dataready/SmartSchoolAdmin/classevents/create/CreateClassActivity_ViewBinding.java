// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.classevents.create;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CreateClassActivity_ViewBinding implements Unbinder {
  private CreateClassActivity target;

  private View view2131231243;

  private View view2131231198;

  private View view2131231245;

  private View view2131231296;

  private View view2131231306;

  private View view2131231299;

  private View view2131231291;

  private View view2131231292;

  private View view2131231305;

  private View view2131231301;

  private View view2131231486;

  @UiThread
  public CreateClassActivity_ViewBinding(CreateClassActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CreateClassActivity_ViewBinding(final CreateClassActivity target, View source) {
    this.target = target;

    View view;
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    view = Utils.findRequiredView(source, R.id.iv_SpeechToText, "field 'ivSpeechToText' and method 'onViewClicked'");
    target.ivSpeechToText = Utils.castView(view, R.id.iv_SpeechToText, "field 'ivSpeechToText'", ImageView.class);
    view2131231243 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.img_attachment, "field 'imgAttachment' and method 'onViewClicked'");
    target.imgAttachment = Utils.castView(view, R.id.img_attachment, "field 'imgAttachment'", ImageView.class);
    view2131231198 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.iv_camera_AttachFile, "field 'ivCameraAttachFile' and method 'onViewClicked'");
    target.ivCameraAttachFile = Utils.castView(view, R.id.iv_camera_AttachFile, "field 'ivCameraAttachFile'", ImageView.class);
    view2131231245 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtAttachment = Utils.findRequiredViewAsType(source, R.id.txt_attachment, "field 'txtAttachment'", TextView.class);
    target.rvAttachments = Utils.findRequiredViewAsType(source, R.id.rvAttachments, "field 'rvAttachments'", RecyclerView.class);
    target.llAttachments = Utils.findRequiredViewAsType(source, R.id.llAttachments, "field 'llAttachments'", LinearLayout.class);
    target.title = Utils.findRequiredViewAsType(source, R.id.title, "field 'title'", EditText.class);
    target.msg = Utils.findRequiredViewAsType(source, R.id.msg, "field 'msg'", EditText.class);
    target.txtDate = Utils.findRequiredViewAsType(source, R.id.txt_Date, "field 'txtDate'", TextView.class);
    target.dateImg = Utils.findRequiredViewAsType(source, R.id.date_img, "field 'dateImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.linear_date, "field 'linearDate' and method 'onViewClicked'");
    target.linearDate = Utils.castView(view, R.id.linear_date, "field 'linearDate'", RelativeLayout.class);
    view2131231296 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtStart = Utils.findRequiredViewAsType(source, R.id.txt_start, "field 'txtStart'", TextView.class);
    target.startImg = Utils.findRequiredViewAsType(source, R.id.start_img, "field 'startImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.linear_start, "field 'linearStart' and method 'onViewClicked'");
    target.linearStart = Utils.castView(view, R.id.linear_start, "field 'linearStart'", RelativeLayout.class);
    view2131231306 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtEnd = Utils.findRequiredViewAsType(source, R.id.txt_end, "field 'txtEnd'", TextView.class);
    target.endImg = Utils.findRequiredViewAsType(source, R.id.end_img, "field 'endImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.linear_end, "field 'linearEnd' and method 'onViewClicked'");
    target.linearEnd = Utils.castView(view, R.id.linear_end, "field 'linearEnd'", RelativeLayout.class);
    view2131231299 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtGrade = Utils.findRequiredViewAsType(source, R.id.txt_Grade, "field 'txtGrade'", TextView.class);
    target.gradeImg = Utils.findRequiredViewAsType(source, R.id.grade_img, "field 'gradeImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.linear_Grade, "field 'linearGrade' and method 'onViewClicked'");
    target.linearGrade = Utils.castView(view, R.id.linear_Grade, "field 'linearGrade'", RelativeLayout.class);
    view2131231291 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtClass = Utils.findRequiredViewAsType(source, R.id.txt_Class, "field 'txtClass'", TextView.class);
    target.classImg = Utils.findRequiredViewAsType(source, R.id.class_img, "field 'classImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.linear_Section, "field 'linearSection' and method 'onViewClicked'");
    target.linearSection = Utils.castView(view, R.id.linear_Section, "field 'linearSection'", RelativeLayout.class);
    view2131231292 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.llheader = Utils.findRequiredViewAsType(source, R.id.llheader, "field 'llheader'", LinearLayout.class);
    target.txtRepeat = Utils.findRequiredViewAsType(source, R.id.txt_repeat, "field 'txtRepeat'", TextView.class);
    target.repeatImg = Utils.findRequiredViewAsType(source, R.id.repeat_img, "field 'repeatImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.linear_repeat, "field 'linearRepeat' and method 'onViewClicked'");
    target.linearRepeat = Utils.castView(view, R.id.linear_repeat, "field 'linearRepeat'", RelativeLayout.class);
    view2131231305 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtExpire = Utils.findRequiredViewAsType(source, R.id.txt_expire, "field 'txtExpire'", TextView.class);
    target.expireImg = Utils.findRequiredViewAsType(source, R.id.expire_img, "field 'expireImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.linear_expire, "field 'linearExpire' and method 'onViewClicked'");
    target.linearExpire = Utils.castView(view, R.id.linear_expire, "field 'linearExpire'", RelativeLayout.class);
    view2131231301 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.sendfeedback, "field 'sendfeedback' and method 'onViewClicked'");
    target.sendfeedback = Utils.castView(view, R.id.sendfeedback, "field 'sendfeedback'", Button.class);
    view2131231486 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.llTextArea = Utils.findRequiredViewAsType(source, R.id.llTextArea, "field 'llTextArea'", LinearLayout.class);
    target.parentViewAnimator = Utils.findRequiredViewAsType(source, R.id.parent_viewAnimator, "field 'parentViewAnimator'", ViewAnimator.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CreateClassActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.ivSpeechToText = null;
    target.imgAttachment = null;
    target.ivCameraAttachFile = null;
    target.txtAttachment = null;
    target.rvAttachments = null;
    target.llAttachments = null;
    target.title = null;
    target.msg = null;
    target.txtDate = null;
    target.dateImg = null;
    target.linearDate = null;
    target.txtStart = null;
    target.startImg = null;
    target.linearStart = null;
    target.txtEnd = null;
    target.endImg = null;
    target.linearEnd = null;
    target.txtGrade = null;
    target.gradeImg = null;
    target.linearGrade = null;
    target.txtClass = null;
    target.classImg = null;
    target.linearSection = null;
    target.llheader = null;
    target.txtRepeat = null;
    target.repeatImg = null;
    target.linearRepeat = null;
    target.txtExpire = null;
    target.expireImg = null;
    target.linearExpire = null;
    target.sendfeedback = null;
    target.llTextArea = null;
    target.parentViewAnimator = null;

    view2131231243.setOnClickListener(null);
    view2131231243 = null;
    view2131231198.setOnClickListener(null);
    view2131231198 = null;
    view2131231245.setOnClickListener(null);
    view2131231245 = null;
    view2131231296.setOnClickListener(null);
    view2131231296 = null;
    view2131231306.setOnClickListener(null);
    view2131231306 = null;
    view2131231299.setOnClickListener(null);
    view2131231299 = null;
    view2131231291.setOnClickListener(null);
    view2131231291 = null;
    view2131231292.setOnClickListener(null);
    view2131231292 = null;
    view2131231305.setOnClickListener(null);
    view2131231305 = null;
    view2131231301.setOnClickListener(null);
    view2131231301 = null;
    view2131231486.setOnClickListener(null);
    view2131231486 = null;
  }
}
