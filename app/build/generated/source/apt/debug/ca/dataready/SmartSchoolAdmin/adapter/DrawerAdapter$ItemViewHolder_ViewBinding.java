// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DrawerAdapter$ItemViewHolder_ViewBinding implements Unbinder {
  private DrawerAdapter.ItemViewHolder target;

  @UiThread
  public DrawerAdapter$ItemViewHolder_ViewBinding(DrawerAdapter.ItemViewHolder target,
      View source) {
    this.target = target;

    target.groupView = Utils.findRequiredViewAsType(source, R.id.group_view, "field 'groupView'", RelativeLayout.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    target.groupTitle = Utils.findRequiredViewAsType(source, R.id.group_title, "field 'groupTitle'", TextView.class);
    target.groupIcon = Utils.findRequiredViewAsType(source, R.id.group_icon, "field 'groupIcon'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DrawerAdapter.ItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.groupView = null;
    target.recyclerView = null;
    target.groupTitle = null;
    target.groupIcon = null;
  }
}
