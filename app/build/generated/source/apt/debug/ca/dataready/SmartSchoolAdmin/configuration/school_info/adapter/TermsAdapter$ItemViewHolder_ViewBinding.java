// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.configuration.school_info.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class TermsAdapter$ItemViewHolder_ViewBinding implements Unbinder {
  private TermsAdapter.ItemViewHolder target;

  @UiThread
  public TermsAdapter$ItemViewHolder_ViewBinding(TermsAdapter.ItemViewHolder target, View source) {
    this.target = target;

    target.etTermsName = Utils.findRequiredViewAsType(source, R.id.et_terms_name, "field 'etTermsName'", EditText.class);
    target.imgRemove = Utils.findRequiredViewAsType(source, R.id.img_remove, "field 'imgRemove'", ImageView.class);
    target.etTermsDesc = Utils.findRequiredViewAsType(source, R.id.et_terms_desc, "field 'etTermsDesc'", EditText.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    TermsAdapter.ItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.etTermsName = null;
    target.imgRemove = null;
    target.etTermsDesc = null;
  }
}
