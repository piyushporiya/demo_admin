// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.classchannel.details;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import com.facebook.drawee.view.SimpleDraweeView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ClassChannelDetailsActivity_ViewBinding implements Unbinder {
  private ClassChannelDetailsActivity target;

  @UiThread
  public ClassChannelDetailsActivity_ViewBinding(ClassChannelDetailsActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ClassChannelDetailsActivity_ViewBinding(ClassChannelDetailsActivity target, View source) {
    this.target = target;

    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.txtDateValue = Utils.findRequiredViewAsType(source, R.id.txt_date_value, "field 'txtDateValue'", TextView.class);
    target.txtTitleValue = Utils.findRequiredViewAsType(source, R.id.txt_title_value, "field 'txtTitleValue'", TextView.class);
    target.myImageView = Utils.findRequiredViewAsType(source, R.id.my_image_view, "field 'myImageView'", SimpleDraweeView.class);
    target.txtName = Utils.findRequiredViewAsType(source, R.id.txt_name, "field 'txtName'", TextView.class);
    target.txtEmail = Utils.findRequiredViewAsType(source, R.id.txt_email, "field 'txtEmail'", TextView.class);
    target.rlMainBox = Utils.findRequiredViewAsType(source, R.id.rlMainBox, "field 'rlMainBox'", RelativeLayout.class);
    target.txtMessageValue = Utils.findRequiredViewAsType(source, R.id.txt_message_value, "field 'txtMessageValue'", TextView.class);
    target.rvAttachments = Utils.findRequiredViewAsType(source, R.id.rvAttachments, "field 'rvAttachments'", RecyclerView.class);
    target.llAttachments = Utils.findRequiredViewAsType(source, R.id.llAttachments, "field 'llAttachments'", LinearLayout.class);
    target.txtEmpty = Utils.findRequiredViewAsType(source, R.id.txt_empty, "field 'txtEmpty'", TextView.class);
    target.viewAnimator = Utils.findRequiredViewAsType(source, R.id.viewAnimator, "field 'viewAnimator'", ViewAnimator.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ClassChannelDetailsActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.txtDateValue = null;
    target.txtTitleValue = null;
    target.myImageView = null;
    target.txtName = null;
    target.txtEmail = null;
    target.rlMainBox = null;
    target.txtMessageValue = null;
    target.rvAttachments = null;
    target.llAttachments = null;
    target.txtEmpty = null;
    target.viewAnimator = null;
  }
}
