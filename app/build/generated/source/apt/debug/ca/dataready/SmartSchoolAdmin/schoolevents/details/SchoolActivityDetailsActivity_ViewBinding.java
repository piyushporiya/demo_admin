// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.schoolevents.details;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SchoolActivityDetailsActivity_ViewBinding implements Unbinder {
  private SchoolActivityDetailsActivity target;

  @UiThread
  public SchoolActivityDetailsActivity_ViewBinding(SchoolActivityDetailsActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SchoolActivityDetailsActivity_ViewBinding(SchoolActivityDetailsActivity target,
      View source) {
    this.target = target;

    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.txtDateValue = Utils.findRequiredViewAsType(source, R.id.txt_date_value, "field 'txtDateValue'", TextView.class);
    target.txtStartTimeValue = Utils.findRequiredViewAsType(source, R.id.txt_start_time_value, "field 'txtStartTimeValue'", TextView.class);
    target.txtEndTimeValue = Utils.findRequiredViewAsType(source, R.id.txt_end_time_value, "field 'txtEndTimeValue'", TextView.class);
    target.txtRepeatValue = Utils.findRequiredViewAsType(source, R.id.txt_repeat_value, "field 'txtRepeatValue'", TextView.class);
    target.txtTitleValue = Utils.findRequiredViewAsType(source, R.id.txt_title_value, "field 'txtTitleValue'", TextView.class);
    target.txtDescValue = Utils.findRequiredViewAsType(source, R.id.txt_desc_value, "field 'txtDescValue'", TextView.class);
    target.rvAttachments = Utils.findRequiredViewAsType(source, R.id.rvAttachments, "field 'rvAttachments'", RecyclerView.class);
    target.llAttachments = Utils.findRequiredViewAsType(source, R.id.llAttachments, "field 'llAttachments'", LinearLayout.class);
    target.parentViewAnimator = Utils.findRequiredViewAsType(source, R.id.parent_viewAnimator, "field 'parentViewAnimator'", ViewAnimator.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SchoolActivityDetailsActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.txtDateValue = null;
    target.txtStartTimeValue = null;
    target.txtEndTimeValue = null;
    target.txtRepeatValue = null;
    target.txtTitleValue = null;
    target.txtDescValue = null;
    target.rvAttachments = null;
    target.llAttachments = null;
    target.parentViewAnimator = null;
  }
}
