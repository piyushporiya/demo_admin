// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.configuration.app_config;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AppLevelConfigsFragment_ViewBinding implements Unbinder {
  private AppLevelConfigsFragment target;

  private View view2131230894;

  private View view2131230850;

  private View view2131230915;

  private View view2131230874;

  private View view2131230873;

  private View view2131230907;

  private View view2131230854;

  private View view2131230853;

  private View view2131230899;

  private View view2131230838;

  private View view2131230837;

  private View view2131230896;

  private View view2131230877;

  private View view2131230832;

  @UiThread
  public AppLevelConfigsFragment_ViewBinding(final AppLevelConfigsFragment target, View source) {
    this.target = target;

    View view;
    target.txtAdminModule = Utils.findRequiredViewAsType(source, R.id.txtAdminModule, "field 'txtAdminModule'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_admin_module, "field 'cardAdminModule' and method 'onViewClicked'");
    target.cardAdminModule = Utils.castView(view, R.id.card_admin_module, "field 'cardAdminModule'", CardView.class);
    view2131230894 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.chkAppointment = Utils.findRequiredViewAsType(source, R.id.chk_appointment, "field 'chkAppointment'", CheckBox.class);
    target.chkChannel = Utils.findRequiredViewAsType(source, R.id.chk_channel, "field 'chkChannel'", CheckBox.class);
    target.chkCommunication = Utils.findRequiredViewAsType(source, R.id.chk_communication, "field 'chkCommunication'", CheckBox.class);
    target.chkDriver = Utils.findRequiredViewAsType(source, R.id.chk_driver, "field 'chkDriver'", CheckBox.class);
    target.chkEvents = Utils.findRequiredViewAsType(source, R.id.chk_events, "field 'chkEvents'", CheckBox.class);
    target.chkFee = Utils.findRequiredViewAsType(source, R.id.chk_fee, "field 'chkFee'", CheckBox.class);
    target.chkRoute = Utils.findRequiredViewAsType(source, R.id.chk_route, "field 'chkRoute'", CheckBox.class);
    target.chkStaff = Utils.findRequiredViewAsType(source, R.id.chk_staff, "field 'chkStaff'", CheckBox.class);
    target.chkStudent = Utils.findRequiredViewAsType(source, R.id.chk_student, "field 'chkStudent'", CheckBox.class);
    view = Utils.findRequiredView(source, R.id.btn_next, "field 'btnNext' and method 'onViewClicked'");
    target.btnNext = Utils.castView(view, R.id.btn_next, "field 'btnNext'", Button.class);
    view2131230850 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutAdminModule = Utils.findRequiredViewAsType(source, R.id.layout_admin_module, "field 'layoutAdminModule'", CardView.class);
    target.txtTeacherModule = Utils.findRequiredViewAsType(source, R.id.txtTeacherModule, "field 'txtTeacherModule'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_teacher_module, "field 'cardTeacherModule' and method 'onViewClicked'");
    target.cardTeacherModule = Utils.castView(view, R.id.card_teacher_module, "field 'cardTeacherModule'", CardView.class);
    view2131230915 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.chkTeacherAppointment = Utils.findRequiredViewAsType(source, R.id.chk_teacher_appointment, "field 'chkTeacherAppointment'", CheckBox.class);
    target.chkTeacherAttendance = Utils.findRequiredViewAsType(source, R.id.chk_teacher_attendance, "field 'chkTeacherAttendance'", CheckBox.class);
    target.chkTeacherCommunication = Utils.findRequiredViewAsType(source, R.id.chk_teacher_communication, "field 'chkTeacherCommunication'", CheckBox.class);
    target.chkTeacherT2t = Utils.findRequiredViewAsType(source, R.id.chk_teacher_t2t, "field 'chkTeacherT2t'", CheckBox.class);
    view = Utils.findRequiredView(source, R.id.btn_teacher_previous, "field 'btnTeacherPrevious' and method 'onViewClicked'");
    target.btnTeacherPrevious = Utils.castView(view, R.id.btn_teacher_previous, "field 'btnTeacherPrevious'", Button.class);
    view2131230874 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_teacher_next, "field 'btnTeacherNext' and method 'onViewClicked'");
    target.btnTeacherNext = Utils.castView(view, R.id.btn_teacher_next, "field 'btnTeacherNext'", Button.class);
    view2131230873 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutTeacherModule = Utils.findRequiredViewAsType(source, R.id.layout_teacher_module, "field 'layoutTeacherModule'", CardView.class);
    target.txtParentModule = Utils.findRequiredViewAsType(source, R.id.txtParentModule, "field 'txtParentModule'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_parent_module, "field 'cardParentModule' and method 'onViewClicked'");
    target.cardParentModule = Utils.castView(view, R.id.card_parent_module, "field 'cardParentModule'", CardView.class);
    view2131230907 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.chkParentAppointment = Utils.findRequiredViewAsType(source, R.id.chk_parent_appointment, "field 'chkParentAppointment'", CheckBox.class);
    target.chkParentAttendance = Utils.findRequiredViewAsType(source, R.id.chk_parent_attendance, "field 'chkParentAttendance'", CheckBox.class);
    target.chkParentCommunication = Utils.findRequiredViewAsType(source, R.id.chk_parent_communication, "field 'chkParentCommunication'", CheckBox.class);
    target.chkParentHomework = Utils.findRequiredViewAsType(source, R.id.chk_parent_homework, "field 'chkParentHomework'", CheckBox.class);
    target.chkParentChannel = Utils.findRequiredViewAsType(source, R.id.chk_parent_channel, "field 'chkParentChannel'", CheckBox.class);
    target.chkParentEvents = Utils.findRequiredViewAsType(source, R.id.chk_parent_events, "field 'chkParentEvents'", CheckBox.class);
    target.chkParentFee = Utils.findRequiredViewAsType(source, R.id.chk_parent_fee, "field 'chkParentFee'", CheckBox.class);
    target.chkParentP2p = Utils.findRequiredViewAsType(source, R.id.chk_parent_p2p, "field 'chkParentP2p'", CheckBox.class);
    target.chkParentRoute = Utils.findRequiredViewAsType(source, R.id.chk_parent_route, "field 'chkParentRoute'", CheckBox.class);
    target.etParentRefreshFrequncy = Utils.findRequiredViewAsType(source, R.id.et_parent_refresh_frequncy, "field 'etParentRefreshFrequncy'", EditText.class);
    view = Utils.findRequiredView(source, R.id.btn_parent_previous, "field 'btnParentPrevious' and method 'onViewClicked'");
    target.btnParentPrevious = Utils.castView(view, R.id.btn_parent_previous, "field 'btnParentPrevious'", Button.class);
    view2131230854 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_parent_next, "field 'btnParentNext' and method 'onViewClicked'");
    target.btnParentNext = Utils.castView(view, R.id.btn_parent_next, "field 'btnParentNext'", Button.class);
    view2131230853 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutParentModule = Utils.findRequiredViewAsType(source, R.id.layout_parent_module, "field 'layoutParentModule'", CardView.class);
    target.txtDriverModule = Utils.findRequiredViewAsType(source, R.id.txtDriverModule, "field 'txtDriverModule'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_driver_module, "field 'cardDriverModule' and method 'onViewClicked'");
    target.cardDriverModule = Utils.castView(view, R.id.card_driver_module, "field 'cardDriverModule'", CardView.class);
    view2131230899 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.chkDriverFileUpload = Utils.findRequiredViewAsType(source, R.id.chk_driver_file_upload, "field 'chkDriverFileUpload'", CheckBox.class);
    target.chkDriverPickupdropoff = Utils.findRequiredViewAsType(source, R.id.chk_driver_pickupdropoff, "field 'chkDriverPickupdropoff'", CheckBox.class);
    target.chkDriverQrScan = Utils.findRequiredViewAsType(source, R.id.chk_driver_qr_scan, "field 'chkDriverQrScan'", CheckBox.class);
    target.chkTeacherHomework = Utils.findRequiredViewAsType(source, R.id.chk_teacher_homework, "field 'chkTeacherHomework'", CheckBox.class);
    target.chkDriverVideo = Utils.findRequiredViewAsType(source, R.id.chk_driver_video, "field 'chkDriverVideo'", CheckBox.class);
    target.etDriverRefreshFrequncy = Utils.findRequiredViewAsType(source, R.id.et_driver_refresh_frequncy, "field 'etDriverRefreshFrequncy'", EditText.class);
    view = Utils.findRequiredView(source, R.id.btn_driver_previous, "field 'btnDriverPrevious' and method 'onViewClicked'");
    target.btnDriverPrevious = Utils.castView(view, R.id.btn_driver_previous, "field 'btnDriverPrevious'", Button.class);
    view2131230838 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_driver_next, "field 'btnDriverNext' and method 'onViewClicked'");
    target.btnDriverNext = Utils.castView(view, R.id.btn_driver_next, "field 'btnDriverNext'", Button.class);
    view2131230837 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutDriverModule = Utils.findRequiredViewAsType(source, R.id.layout_driver_module, "field 'layoutDriverModule'", CardView.class);
    target.txtAppModule = Utils.findRequiredViewAsType(source, R.id.txtAppModule, "field 'txtAppModule'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_app_module, "field 'cardAppModule' and method 'onViewClicked'");
    target.cardAppModule = Utils.castView(view, R.id.card_app_module, "field 'cardAppModule'", CardView.class);
    view2131230896 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.chkAppAdmin = Utils.findRequiredViewAsType(source, R.id.chk_app_admin, "field 'chkAppAdmin'", CheckBox.class);
    target.chkAppTeacher = Utils.findRequiredViewAsType(source, R.id.chk_app_teacher, "field 'chkAppTeacher'", CheckBox.class);
    target.chkAppParent = Utils.findRequiredViewAsType(source, R.id.chk_app_parent, "field 'chkAppParent'", CheckBox.class);
    target.chkAppDriver = Utils.findRequiredViewAsType(source, R.id.chk_app_driver, "field 'chkAppDriver'", CheckBox.class);
    view = Utils.findRequiredView(source, R.id.btn_update, "field 'btnUpdate' and method 'onViewClicked'");
    target.btnUpdate = Utils.castView(view, R.id.btn_update, "field 'btnUpdate'", Button.class);
    view2131230877 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutAppModule = Utils.findRequiredViewAsType(source, R.id.layout_app_module, "field 'layoutAppModule'", CardView.class);
    target.scrollView = Utils.findRequiredViewAsType(source, R.id.scrollView, "field 'scrollView'", NestedScrollView.class);
    target.txtError = Utils.findRequiredViewAsType(source, R.id.txt_error, "field 'txtError'", TextView.class);
    target.viewAnimator = Utils.findRequiredViewAsType(source, R.id.viewAnimator, "field 'viewAnimator'", ViewAnimator.class);
    view = Utils.findRequiredView(source, R.id.btn_app_previous, "method 'onViewClicked'");
    view2131230832 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    AppLevelConfigsFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtAdminModule = null;
    target.cardAdminModule = null;
    target.chkAppointment = null;
    target.chkChannel = null;
    target.chkCommunication = null;
    target.chkDriver = null;
    target.chkEvents = null;
    target.chkFee = null;
    target.chkRoute = null;
    target.chkStaff = null;
    target.chkStudent = null;
    target.btnNext = null;
    target.layoutAdminModule = null;
    target.txtTeacherModule = null;
    target.cardTeacherModule = null;
    target.chkTeacherAppointment = null;
    target.chkTeacherAttendance = null;
    target.chkTeacherCommunication = null;
    target.chkTeacherT2t = null;
    target.btnTeacherPrevious = null;
    target.btnTeacherNext = null;
    target.layoutTeacherModule = null;
    target.txtParentModule = null;
    target.cardParentModule = null;
    target.chkParentAppointment = null;
    target.chkParentAttendance = null;
    target.chkParentCommunication = null;
    target.chkParentHomework = null;
    target.chkParentChannel = null;
    target.chkParentEvents = null;
    target.chkParentFee = null;
    target.chkParentP2p = null;
    target.chkParentRoute = null;
    target.etParentRefreshFrequncy = null;
    target.btnParentPrevious = null;
    target.btnParentNext = null;
    target.layoutParentModule = null;
    target.txtDriverModule = null;
    target.cardDriverModule = null;
    target.chkDriverFileUpload = null;
    target.chkDriverPickupdropoff = null;
    target.chkDriverQrScan = null;
    target.chkTeacherHomework = null;
    target.chkDriverVideo = null;
    target.etDriverRefreshFrequncy = null;
    target.btnDriverPrevious = null;
    target.btnDriverNext = null;
    target.layoutDriverModule = null;
    target.txtAppModule = null;
    target.cardAppModule = null;
    target.chkAppAdmin = null;
    target.chkAppTeacher = null;
    target.chkAppParent = null;
    target.chkAppDriver = null;
    target.btnUpdate = null;
    target.layoutAppModule = null;
    target.scrollView = null;
    target.txtError = null;
    target.viewAnimator = null;

    view2131230894.setOnClickListener(null);
    view2131230894 = null;
    view2131230850.setOnClickListener(null);
    view2131230850 = null;
    view2131230915.setOnClickListener(null);
    view2131230915 = null;
    view2131230874.setOnClickListener(null);
    view2131230874 = null;
    view2131230873.setOnClickListener(null);
    view2131230873 = null;
    view2131230907.setOnClickListener(null);
    view2131230907 = null;
    view2131230854.setOnClickListener(null);
    view2131230854 = null;
    view2131230853.setOnClickListener(null);
    view2131230853 = null;
    view2131230899.setOnClickListener(null);
    view2131230899 = null;
    view2131230838.setOnClickListener(null);
    view2131230838 = null;
    view2131230837.setOnClickListener(null);
    view2131230837 = null;
    view2131230896.setOnClickListener(null);
    view2131230896 = null;
    view2131230877.setOnClickListener(null);
    view2131230877 = null;
    view2131230832.setOnClickListener(null);
    view2131230832 = null;
  }
}
