// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.chat.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ChatAdapter$ReceiveMessageViewHolder_ViewBinding implements Unbinder {
  private ChatAdapter.ReceiveMessageViewHolder target;

  @UiThread
  public ChatAdapter$ReceiveMessageViewHolder_ViewBinding(ChatAdapter.ReceiveMessageViewHolder target,
      View source) {
    this.target = target;

    target.messageTextView = Utils.findRequiredViewAsType(source, R.id.message_text_view, "field 'messageTextView'", TextView.class);
    target.timestampTextView = Utils.findRequiredViewAsType(source, R.id.timestamp_text_view, "field 'timestampTextView'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ChatAdapter.ReceiveMessageViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.messageTextView = null;
    target.timestampTextView = null;
  }
}
