// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.acadamic.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AcadamicYearAdapter$ItemViewHolder_ViewBinding implements Unbinder {
  private AcadamicYearAdapter.ItemViewHolder target;

  @UiThread
  public AcadamicYearAdapter$ItemViewHolder_ViewBinding(AcadamicYearAdapter.ItemViewHolder target,
      View source) {
    this.target = target;

    target.txtYear = Utils.findRequiredViewAsType(source, R.id.txt_year, "field 'txtYear'", TextView.class);
    target.txtYearStartDate = Utils.findRequiredViewAsType(source, R.id.txt_year_start_date, "field 'txtYearStartDate'", TextView.class);
    target.txtYearEndDate = Utils.findRequiredViewAsType(source, R.id.txt_year_end_date, "field 'txtYearEndDate'", TextView.class);
    target.imgMore = Utils.findRequiredViewAsType(source, R.id.img_more, "field 'imgMore'", ImageView.class);
    target.cardGrade = Utils.findRequiredViewAsType(source, R.id.card_grade, "field 'cardGrade'", CardView.class);
    target.status = Utils.findRequiredView(source, R.id.status, "field 'status'");
  }

  @Override
  @CallSuper
  public void unbind() {
    AcadamicYearAdapter.ItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtYear = null;
    target.txtYearStartDate = null;
    target.txtYearEndDate = null;
    target.imgMore = null;
    target.cardGrade = null;
    target.status = null;
  }
}
