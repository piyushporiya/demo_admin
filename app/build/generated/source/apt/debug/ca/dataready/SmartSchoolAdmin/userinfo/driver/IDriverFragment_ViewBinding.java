// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.userinfo.driver;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class IDriverFragment_ViewBinding implements Unbinder {
  private IDriverFragment target;

  private View view2131231154;

  private View view2131231477;

  @UiThread
  public IDriverFragment_ViewBinding(final IDriverFragment target, View source) {
    this.target = target;

    View view;
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", SuperRecyclerView.class);
    target.parentViewAnimator = Utils.findRequiredViewAsType(source, R.id.parent_viewAnimator, "field 'parentViewAnimator'", ViewAnimator.class);
    view = Utils.findRequiredView(source, R.id.fab_create, "field 'fabCreate' and method 'onViewClicked'");
    target.fabCreate = Utils.castView(view, R.id.fab_create, "field 'fabCreate'", FloatingActionButton.class);
    view2131231154 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.etSearch = Utils.findRequiredViewAsType(source, R.id.et_search, "field 'etSearch'", EditText.class);
    view = Utils.findRequiredView(source, R.id.search_img, "field 'searchImg' and method 'onViewClicked'");
    target.searchImg = Utils.castView(view, R.id.search_img, "field 'searchImg'", ImageView.class);
    view2131231477 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    IDriverFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.recyclerView = null;
    target.parentViewAnimator = null;
    target.fabCreate = null;
    target.etSearch = null;
    target.searchImg = null;

    view2131231154.setOnClickListener(null);
    view2131231154 = null;
    view2131231477.setOnClickListener(null);
    view2131231477 = null;
  }
}
