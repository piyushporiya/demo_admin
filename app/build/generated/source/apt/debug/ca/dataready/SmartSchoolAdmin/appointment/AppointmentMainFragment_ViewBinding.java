// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.appointment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.RadioButton;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import info.hoang8f.android.segmented.SegmentedGroup;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AppointmentMainFragment_ViewBinding implements Unbinder {
  private AppointmentMainFragment target;

  @UiThread
  public AppointmentMainFragment_ViewBinding(AppointmentMainFragment target, View source) {
    this.target = target;

    target.rbSchedule = Utils.findRequiredViewAsType(source, R.id.rb_schedule, "field 'rbSchedule'", RadioButton.class);
    target.rbUpcoming = Utils.findRequiredViewAsType(source, R.id.rb_upcoming, "field 'rbUpcoming'", RadioButton.class);
    target.rbPast = Utils.findRequiredViewAsType(source, R.id.rb_past, "field 'rbPast'", RadioButton.class);
    target.sgAppointment = Utils.findRequiredViewAsType(source, R.id.sgAppointment, "field 'sgAppointment'", SegmentedGroup.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AppointmentMainFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rbSchedule = null;
    target.rbUpcoming = null;
    target.rbPast = null;
    target.sgAppointment = null;
  }
}
