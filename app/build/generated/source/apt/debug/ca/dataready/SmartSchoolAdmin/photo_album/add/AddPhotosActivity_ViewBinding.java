// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.photo_album.add;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddPhotosActivity_ViewBinding implements Unbinder {
  private AddPhotosActivity target;

  private View view2131231304;

  private View view2131231290;

  private View view2131230867;

  private View view2131230878;

  @UiThread
  public AddPhotosActivity_ViewBinding(AddPhotosActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AddPhotosActivity_ViewBinding(final AddPhotosActivity target, View source) {
    this.target = target;

    View view;
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", SuperRecyclerView.class);
    target.txtUploadPhotos = Utils.findRequiredViewAsType(source, R.id.txtUploadPhotos, "field 'txtUploadPhotos'", TextView.class);
    target.cardUploadPhotos = Utils.findRequiredViewAsType(source, R.id.card_upload_photos, "field 'cardUploadPhotos'", CardView.class);
    target.txtGrade = Utils.findRequiredViewAsType(source, R.id.txt_grade, "field 'txtGrade'", TextView.class);
    target.gradeImg = Utils.findRequiredViewAsType(source, R.id.grade_img, "field 'gradeImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.linear_grade, "field 'linearGrade' and method 'onViewClicked'");
    target.linearGrade = Utils.castView(view, R.id.linear_grade, "field 'linearGrade'", RelativeLayout.class);
    view2131231304 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtClass = Utils.findRequiredViewAsType(source, R.id.txt_Class, "field 'txtClass'", TextView.class);
    target.classImg = Utils.findRequiredViewAsType(source, R.id.class_img, "field 'classImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.linear_Class, "field 'linearClass' and method 'onViewClicked'");
    target.linearClass = Utils.castView(view, R.id.linear_Class, "field 'linearClass'", RelativeLayout.class);
    view2131231290 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_select, "field 'btnSelect' and method 'onViewClicked'");
    target.btnSelect = Utils.castView(view, R.id.btn_select, "field 'btnSelect'", Button.class);
    view2131230867 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtEdit = Utils.findRequiredViewAsType(source, R.id.txt_edit, "field 'txtEdit'", TextView.class);
    view = Utils.findRequiredView(source, R.id.btn_upload, "field 'btnUpload' and method 'onViewClicked'");
    target.btnUpload = Utils.castView(view, R.id.btn_upload, "field 'btnUpload'", Button.class);
    view2131230878 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutUploadPhotos = Utils.findRequiredViewAsType(source, R.id.layout_upload_photos, "field 'layoutUploadPhotos'", CardView.class);
    target.scrollView = Utils.findRequiredViewAsType(source, R.id.scrollView, "field 'scrollView'", NestedScrollView.class);
    target.txtError = Utils.findRequiredViewAsType(source, R.id.txt_error, "field 'txtError'", TextView.class);
    target.viewAnimator = Utils.findRequiredViewAsType(source, R.id.viewAnimator, "field 'viewAnimator'", ViewAnimator.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AddPhotosActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.recyclerView = null;
    target.txtUploadPhotos = null;
    target.cardUploadPhotos = null;
    target.txtGrade = null;
    target.gradeImg = null;
    target.linearGrade = null;
    target.txtClass = null;
    target.classImg = null;
    target.linearClass = null;
    target.btnSelect = null;
    target.txtEdit = null;
    target.btnUpload = null;
    target.layoutUploadPhotos = null;
    target.scrollView = null;
    target.txtError = null;
    target.viewAnimator = null;

    view2131231304.setOnClickListener(null);
    view2131231304 = null;
    view2131231290.setOnClickListener(null);
    view2131231290 = null;
    view2131230867.setOnClickListener(null);
    view2131230867 = null;
    view2131230878.setOnClickListener(null);
    view2131230878 = null;
  }
}
