// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.appointment.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class UpcomingAdapter$HeaderViewHolder_ViewBinding implements Unbinder {
  private UpcomingAdapter.HeaderViewHolder target;

  @UiThread
  public UpcomingAdapter$HeaderViewHolder_ViewBinding(UpcomingAdapter.HeaderViewHolder target,
      View source) {
    this.target = target;

    target.txtMonth = Utils.findRequiredViewAsType(source, R.id.txt_month, "field 'txtMonth'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    UpcomingAdapter.HeaderViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtMonth = null;
  }
}
