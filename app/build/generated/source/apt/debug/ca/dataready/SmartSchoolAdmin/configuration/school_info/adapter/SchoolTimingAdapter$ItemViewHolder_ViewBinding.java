// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.configuration.school_info.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SchoolTimingAdapter$ItemViewHolder_ViewBinding implements Unbinder {
  private SchoolTimingAdapter.ItemViewHolder target;

  @UiThread
  public SchoolTimingAdapter$ItemViewHolder_ViewBinding(SchoolTimingAdapter.ItemViewHolder target,
      View source) {
    this.target = target;

    target.etName = Utils.findRequiredViewAsType(source, R.id.et_name, "field 'etName'", EditText.class);
    target.imgRemove = Utils.findRequiredViewAsType(source, R.id.img_remove, "field 'imgRemove'", ImageView.class);
    target.etStartDate = Utils.findRequiredViewAsType(source, R.id.et_start_date, "field 'etStartDate'", EditText.class);
    target.etEndDate = Utils.findRequiredViewAsType(source, R.id.et_end_date, "field 'etEndDate'", EditText.class);
    target.etStartTimeMon = Utils.findRequiredViewAsType(source, R.id.et_start_time_mon, "field 'etStartTimeMon'", EditText.class);
    target.etEndTimeMon = Utils.findRequiredViewAsType(source, R.id.et_end_time_mon, "field 'etEndTimeMon'", EditText.class);
    target.etIsHolidayMon = Utils.findRequiredViewAsType(source, R.id.et_is_holiday_mon, "field 'etIsHolidayMon'", EditText.class);
    target.etStartTimeTue = Utils.findRequiredViewAsType(source, R.id.et_start_time_tue, "field 'etStartTimeTue'", EditText.class);
    target.etEndTimeTue = Utils.findRequiredViewAsType(source, R.id.et_end_time_tue, "field 'etEndTimeTue'", EditText.class);
    target.etIsHolidayTue = Utils.findRequiredViewAsType(source, R.id.et_is_holiday_tue, "field 'etIsHolidayTue'", EditText.class);
    target.etStartTimeWed = Utils.findRequiredViewAsType(source, R.id.et_start_time_wed, "field 'etStartTimeWed'", EditText.class);
    target.etEndTimeWed = Utils.findRequiredViewAsType(source, R.id.et_end_time_wed, "field 'etEndTimeWed'", EditText.class);
    target.etIsHolidayWed = Utils.findRequiredViewAsType(source, R.id.et_is_holiday_wed, "field 'etIsHolidayWed'", EditText.class);
    target.etStartTimeThu = Utils.findRequiredViewAsType(source, R.id.et_start_time_thu, "field 'etStartTimeThu'", EditText.class);
    target.etEndTimeThu = Utils.findRequiredViewAsType(source, R.id.et_end_time_thu, "field 'etEndTimeThu'", EditText.class);
    target.etIsHolidayThu = Utils.findRequiredViewAsType(source, R.id.et_is_holiday_thu, "field 'etIsHolidayThu'", EditText.class);
    target.etStartTimeFri = Utils.findRequiredViewAsType(source, R.id.et_start_time_fri, "field 'etStartTimeFri'", EditText.class);
    target.etEndTimeFri = Utils.findRequiredViewAsType(source, R.id.et_end_time_fri, "field 'etEndTimeFri'", EditText.class);
    target.etIsHolidayFri = Utils.findRequiredViewAsType(source, R.id.et_is_holiday_fri, "field 'etIsHolidayFri'", EditText.class);
    target.etStartTimeSat = Utils.findRequiredViewAsType(source, R.id.et_start_time_sat, "field 'etStartTimeSat'", EditText.class);
    target.etEndTimeSat = Utils.findRequiredViewAsType(source, R.id.et_end_time_sat, "field 'etEndTimeSat'", EditText.class);
    target.etIsHolidaySat = Utils.findRequiredViewAsType(source, R.id.et_is_holiday_sat, "field 'etIsHolidaySat'", EditText.class);
    target.etStartTimeSun = Utils.findRequiredViewAsType(source, R.id.et_start_time_sun, "field 'etStartTimeSun'", EditText.class);
    target.etEndTimeSun = Utils.findRequiredViewAsType(source, R.id.et_end_time_sun, "field 'etEndTimeSun'", EditText.class);
    target.etIsHolidaySun = Utils.findRequiredViewAsType(source, R.id.et_is_holiday_sun, "field 'etIsHolidaySun'", EditText.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SchoolTimingAdapter.ItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.etName = null;
    target.imgRemove = null;
    target.etStartDate = null;
    target.etEndDate = null;
    target.etStartTimeMon = null;
    target.etEndTimeMon = null;
    target.etIsHolidayMon = null;
    target.etStartTimeTue = null;
    target.etEndTimeTue = null;
    target.etIsHolidayTue = null;
    target.etStartTimeWed = null;
    target.etEndTimeWed = null;
    target.etIsHolidayWed = null;
    target.etStartTimeThu = null;
    target.etEndTimeThu = null;
    target.etIsHolidayThu = null;
    target.etStartTimeFri = null;
    target.etEndTimeFri = null;
    target.etIsHolidayFri = null;
    target.etStartTimeSat = null;
    target.etEndTimeSat = null;
    target.etIsHolidaySat = null;
    target.etStartTimeSun = null;
    target.etEndTimeSun = null;
    target.etIsHolidaySun = null;
  }
}
