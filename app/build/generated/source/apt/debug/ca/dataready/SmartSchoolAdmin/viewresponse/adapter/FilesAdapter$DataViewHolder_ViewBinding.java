// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.viewresponse.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FilesAdapter$DataViewHolder_ViewBinding implements Unbinder {
  private FilesAdapter.DataViewHolder target;

  @UiThread
  public FilesAdapter$DataViewHolder_ViewBinding(FilesAdapter.DataViewHolder target, View source) {
    this.target = target;

    target.txtFileName = Utils.findRequiredViewAsType(source, R.id.txt_file_name, "field 'txtFileName'", TextView.class);
    target.btnCancel = Utils.findRequiredViewAsType(source, R.id.btn_cancel, "field 'btnCancel'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FilesAdapter.DataViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtFileName = null;
    target.btnCancel = null;
  }
}
