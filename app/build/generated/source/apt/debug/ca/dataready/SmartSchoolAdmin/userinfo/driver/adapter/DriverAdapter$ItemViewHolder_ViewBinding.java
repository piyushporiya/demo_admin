// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.userinfo.driver.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import com.mikhaellopez.circularimageview.CircularImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DriverAdapter$ItemViewHolder_ViewBinding implements Unbinder {
  private DriverAdapter.ItemViewHolder target;

  @UiThread
  public DriverAdapter$ItemViewHolder_ViewBinding(DriverAdapter.ItemViewHolder target,
      View source) {
    this.target = target;

    target.ivDriverImage = Utils.findRequiredViewAsType(source, R.id.iv_driver_image, "field 'ivDriverImage'", CircularImageView.class);
    target.txtDriverName = Utils.findRequiredViewAsType(source, R.id.txt_driver_name, "field 'txtDriverName'", TextView.class);
    target.imgMore = Utils.findRequiredViewAsType(source, R.id.img_more, "field 'imgMore'", ImageView.class);
    target.txtEmail = Utils.findRequiredViewAsType(source, R.id.txt_email, "field 'txtEmail'", TextView.class);
    target.txtCall = Utils.findRequiredViewAsType(source, R.id.txt_call, "field 'txtCall'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DriverAdapter.ItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivDriverImage = null;
    target.txtDriverName = null;
    target.imgMore = null;
    target.txtEmail = null;
    target.txtCall = null;
  }
}
