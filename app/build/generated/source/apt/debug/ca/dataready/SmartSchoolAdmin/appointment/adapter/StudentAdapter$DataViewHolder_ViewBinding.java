// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.appointment.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import com.facebook.drawee.view.SimpleDraweeView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class StudentAdapter$DataViewHolder_ViewBinding implements Unbinder {
  private StudentAdapter.DataViewHolder target;

  @UiThread
  public StudentAdapter$DataViewHolder_ViewBinding(StudentAdapter.DataViewHolder target,
      View source) {
    this.target = target;

    target.myImageView = Utils.findRequiredViewAsType(source, R.id.my_image_view, "field 'myImageView'", SimpleDraweeView.class);
    target.txtStudentName = Utils.findRequiredViewAsType(source, R.id.txt_StudentName, "field 'txtStudentName'", TextView.class);
    target.txtStudentId = Utils.findRequiredViewAsType(source, R.id.txt_StudentId, "field 'txtStudentId'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    StudentAdapter.DataViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.myImageView = null;
    target.txtStudentName = null;
    target.txtStudentId = null;
  }
}
