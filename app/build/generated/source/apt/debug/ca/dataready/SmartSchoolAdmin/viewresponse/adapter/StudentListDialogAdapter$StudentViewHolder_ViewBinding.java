// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.viewresponse.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import com.mikhaellopez.circularimageview.CircularImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class StudentListDialogAdapter$StudentViewHolder_ViewBinding implements Unbinder {
  private StudentListDialogAdapter.StudentViewHolder target;

  @UiThread
  public StudentListDialogAdapter$StudentViewHolder_ViewBinding(StudentListDialogAdapter.StudentViewHolder target,
      View source) {
    this.target = target;

    target.ivStudentImg = Utils.findRequiredViewAsType(source, R.id.my_image_view, "field 'ivStudentImg'", CircularImageView.class);
    target.ivSelection = Utils.findRequiredViewAsType(source, R.id.iv_Selection, "field 'ivSelection'", ImageView.class);
    target.txtStudentName = Utils.findRequiredViewAsType(source, R.id.txt_StudentName, "field 'txtStudentName'", TextView.class);
    target.mainLinear = Utils.findRequiredViewAsType(source, R.id.main_Linear, "field 'mainLinear'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    StudentListDialogAdapter.StudentViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivStudentImg = null;
    target.ivSelection = null;
    target.txtStudentName = null;
    target.mainLinear = null;
  }
}
