// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.chat.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AttachmentAdapter$ItemViewHolder_ViewBinding implements Unbinder {
  private AttachmentAdapter.ItemViewHolder target;

  @UiThread
  public AttachmentAdapter$ItemViewHolder_ViewBinding(AttachmentAdapter.ItemViewHolder target,
      View source) {
    this.target = target;

    target.messageTextView = Utils.findRequiredViewAsType(source, R.id.message_text_view, "field 'messageTextView'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AttachmentAdapter.ItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.messageTextView = null;
  }
}
