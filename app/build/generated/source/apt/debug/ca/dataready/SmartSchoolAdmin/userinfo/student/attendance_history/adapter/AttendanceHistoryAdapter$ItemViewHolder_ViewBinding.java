// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.userinfo.student.attendance_history.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AttendanceHistoryAdapter$ItemViewHolder_ViewBinding implements Unbinder {
  private AttendanceHistoryAdapter.ItemViewHolder target;

  @UiThread
  public AttendanceHistoryAdapter$ItemViewHolder_ViewBinding(AttendanceHistoryAdapter.ItemViewHolder target,
      View source) {
    this.target = target;

    target.txtTeacherName = Utils.findRequiredViewAsType(source, R.id.txt_teacher_name, "field 'txtTeacherName'", TextView.class);
    target.txtDate = Utils.findRequiredViewAsType(source, R.id.txt_date, "field 'txtDate'", TextView.class);
    target.txtSubject = Utils.findRequiredViewAsType(source, R.id.txt_subject, "field 'txtSubject'", TextView.class);
    target.txtStatus = Utils.findRequiredViewAsType(source, R.id.txt_status, "field 'txtStatus'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AttendanceHistoryAdapter.ItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtTeacherName = null;
    target.txtDate = null;
    target.txtSubject = null;
    target.txtStatus = null;
  }
}
