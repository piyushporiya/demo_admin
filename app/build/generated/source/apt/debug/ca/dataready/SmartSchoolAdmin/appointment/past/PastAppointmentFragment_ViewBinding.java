// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.appointment.past;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PastAppointmentFragment_ViewBinding implements Unbinder {
  private PastAppointmentFragment target;

  @UiThread
  public PastAppointmentFragment_ViewBinding(PastAppointmentFragment target, View source) {
    this.target = target;

    target.rvPast = Utils.findRequiredViewAsType(source, R.id.rv_past, "field 'rvPast'", RecyclerView.class);
    target.txtNoPastData = Utils.findRequiredViewAsType(source, R.id.txt_no_past_data, "field 'txtNoPastData'", TextView.class);
    target.pastViewAnimator = Utils.findRequiredViewAsType(source, R.id.past_viewAnimator, "field 'pastViewAnimator'", ViewAnimator.class);
    target.swipeRefreshLayoutPast = Utils.findRequiredViewAsType(source, R.id.swipeRefreshLayoutPast, "field 'swipeRefreshLayoutPast'", SwipeRefreshLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    PastAppointmentFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rvPast = null;
    target.txtNoPastData = null;
    target.pastViewAnimator = null;
    target.swipeRefreshLayoutPast = null;
  }
}
