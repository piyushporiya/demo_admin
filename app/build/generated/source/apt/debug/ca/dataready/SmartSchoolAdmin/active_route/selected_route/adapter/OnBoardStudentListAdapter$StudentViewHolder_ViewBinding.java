// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.active_route.selected_route.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import com.facebook.drawee.view.SimpleDraweeView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OnBoardStudentListAdapter$StudentViewHolder_ViewBinding implements Unbinder {
  private OnBoardStudentListAdapter.StudentViewHolder target;

  @UiThread
  public OnBoardStudentListAdapter$StudentViewHolder_ViewBinding(OnBoardStudentListAdapter.StudentViewHolder target,
      View source) {
    this.target = target;

    target.profilePic = Utils.findRequiredViewAsType(source, R.id.profile_pic, "field 'profilePic'", SimpleDraweeView.class);
    target.txtName = Utils.findRequiredViewAsType(source, R.id.txt_name, "field 'txtName'", TextView.class);
    target.txtStopId = Utils.findRequiredViewAsType(source, R.id.txt_stopId, "field 'txtStopId'", TextView.class);
    target.txtAddress = Utils.findRequiredViewAsType(source, R.id.txt_address, "field 'txtAddress'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    OnBoardStudentListAdapter.StudentViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.profilePic = null;
    target.txtName = null;
    target.txtStopId = null;
    target.txtAddress = null;
  }
}
