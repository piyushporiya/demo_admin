// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.subject.add;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import info.hoang8f.android.segmented.SegmentedGroup;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddSubjectActivity_ViewBinding implements Unbinder {
  private AddSubjectActivity target;

  private View view2131230892;

  private View view2131230871;

  @UiThread
  public AddSubjectActivity_ViewBinding(AddSubjectActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AddSubjectActivity_ViewBinding(final AddSubjectActivity target, View source) {
    this.target = target;

    View view;
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.txtAddSubject = Utils.findRequiredViewAsType(source, R.id.txtAddSubject, "field 'txtAddSubject'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_add_subject, "field 'cardAddSubject' and method 'onViewClicked'");
    target.cardAddSubject = Utils.castView(view, R.id.card_add_subject, "field 'cardAddSubject'", CardView.class);
    view2131230892 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.etSubjectName = Utils.findRequiredViewAsType(source, R.id.et_subject_name, "field 'etSubjectName'", EditText.class);
    target.etSubjectCode = Utils.findRequiredViewAsType(source, R.id.et_subject_code, "field 'etSubjectCode'", EditText.class);
    target.etPrerequisite = Utils.findRequiredViewAsType(source, R.id.et_prerequisite, "field 'etPrerequisite'", EditText.class);
    view = Utils.findRequiredView(source, R.id.btn_submit, "field 'btnSubmit' and method 'onViewClicked'");
    target.btnSubmit = Utils.castView(view, R.id.btn_submit, "field 'btnSubmit'", Button.class);
    view2131230871 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutAddSubject = Utils.findRequiredViewAsType(source, R.id.layout_add_subject, "field 'layoutAddSubject'", CardView.class);
    target.rbOptional = Utils.findRequiredViewAsType(source, R.id.rb_optional, "field 'rbOptional'", RadioButton.class);
    target.rbMandatory = Utils.findRequiredViewAsType(source, R.id.rb_mandatory, "field 'rbMandatory'", RadioButton.class);
    target.sgSubjectType = Utils.findRequiredViewAsType(source, R.id.sgSubjectType, "field 'sgSubjectType'", SegmentedGroup.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AddSubjectActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.txtAddSubject = null;
    target.cardAddSubject = null;
    target.etSubjectName = null;
    target.etSubjectCode = null;
    target.etPrerequisite = null;
    target.btnSubmit = null;
    target.layoutAddSubject = null;
    target.rbOptional = null;
    target.rbMandatory = null;
    target.sgSubjectType = null;

    view2131230892.setOnClickListener(null);
    view2131230892 = null;
    view2131230871.setOnClickListener(null);
    view2131230871 = null;
  }
}
