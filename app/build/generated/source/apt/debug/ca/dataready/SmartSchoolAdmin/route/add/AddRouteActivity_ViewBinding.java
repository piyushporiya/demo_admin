// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.route.add;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import info.hoang8f.android.segmented.SegmentedGroup;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddRouteActivity_ViewBinding implements Unbinder {
  private AddRouteActivity target;

  private View view2131231214;

  private View view2131231201;

  private View view2131231616;

  private View view2131230852;

  private View view2131230848;

  private View view2131231040;

  @UiThread
  public AddRouteActivity_ViewBinding(AddRouteActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AddRouteActivity_ViewBinding(final AddRouteActivity target, View source) {
    this.target = target;

    View view;
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.txtProfile = Utils.findRequiredViewAsType(source, R.id.txtProfile, "field 'txtProfile'", TextView.class);
    target.cardRouteInfo = Utils.findRequiredViewAsType(source, R.id.card_route_info, "field 'cardRouteInfo'", CardView.class);
    target.rbPickup = Utils.findRequiredViewAsType(source, R.id.rb_pickup, "field 'rbPickup'", RadioButton.class);
    target.rbDropoff = Utils.findRequiredViewAsType(source, R.id.rb_dropoff, "field 'rbDropoff'", RadioButton.class);
    target.sgRouteType = Utils.findRequiredViewAsType(source, R.id.sgRouteType, "field 'sgRouteType'", SegmentedGroup.class);
    target.etRouteId = Utils.findRequiredViewAsType(source, R.id.et_route_id, "field 'etRouteId'", EditText.class);
    target.etRouteName = Utils.findRequiredViewAsType(source, R.id.et_route_name, "field 'etRouteName'", EditText.class);
    target.layoutRouteInfo = Utils.findRequiredViewAsType(source, R.id.layout_route_info, "field 'layoutRouteInfo'", CardView.class);
    target.txtError = Utils.findRequiredViewAsType(source, R.id.txt_error, "field 'txtError'", TextView.class);
    target.viewAnimator = Utils.findRequiredViewAsType(source, R.id.viewAnimator, "field 'viewAnimator'", ViewAnimator.class);
    target.etSearchPlace = Utils.findRequiredViewAsType(source, R.id.et_search_place, "field 'etSearchPlace'", AutoCompleteTextView.class);
    view = Utils.findRequiredView(source, R.id.img_search, "field 'imgSearch' and method 'onViewClicked'");
    target.imgSearch = Utils.castView(view, R.id.img_search, "field 'imgSearch'", ImageView.class);
    view2131231214 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtAddress = Utils.findRequiredViewAsType(source, R.id.txt_address, "field 'txtAddress'", TextView.class);
    target.txtLatitude = Utils.findRequiredViewAsType(source, R.id.txt_latitude, "field 'txtLatitude'", TextView.class);
    target.txtLongitude = Utils.findRequiredViewAsType(source, R.id.txt_longitude, "field 'txtLongitude'", TextView.class);
    view = Utils.findRequiredView(source, R.id.img_cancel, "field 'imgCancel' and method 'onViewClicked'");
    target.imgCancel = Utils.castView(view, R.id.img_cancel, "field 'imgCancel'", ImageView.class);
    view2131231201 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.cardAddress = Utils.findRequiredViewAsType(source, R.id.card_address, "field 'cardAddress'", CardView.class);
    view = Utils.findRequiredView(source, R.id.txt_add_location, "field 'txtAddLocation' and method 'onViewClicked'");
    target.txtAddLocation = Utils.castView(view, R.id.txt_add_location, "field 'txtAddLocation'", TextView.class);
    view2131231616 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtStartLocation = Utils.findRequiredViewAsType(source, R.id.txt_start_location, "field 'txtStartLocation'", TextView.class);
    target.txtEndLocation = Utils.findRequiredViewAsType(source, R.id.txt_end_location, "field 'txtEndLocation'", TextView.class);
    view = Utils.findRequiredView(source, R.id.btn_optimize, "field 'btnOptimize' and method 'onViewClicked'");
    target.btnOptimize = Utils.castView(view, R.id.btn_optimize, "field 'btnOptimize'", Button.class);
    view2131230852 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.chkOrigin = Utils.findRequiredViewAsType(source, R.id.chk_origin, "field 'chkOrigin'", CheckBox.class);
    view = Utils.findRequiredView(source, R.id.btn_finalize, "field 'btnFinalize' and method 'onViewClicked'");
    target.btnFinalize = Utils.castView(view, R.id.btn_finalize, "field 'btnFinalize'", Button.class);
    view2131230848 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.et_driver, "field 'etDriver' and method 'onViewClicked'");
    target.etDriver = Utils.castView(view, R.id.et_driver, "field 'etDriver'", EditText.class);
    view2131231040 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    AddRouteActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.txtProfile = null;
    target.cardRouteInfo = null;
    target.rbPickup = null;
    target.rbDropoff = null;
    target.sgRouteType = null;
    target.etRouteId = null;
    target.etRouteName = null;
    target.layoutRouteInfo = null;
    target.txtError = null;
    target.viewAnimator = null;
    target.etSearchPlace = null;
    target.imgSearch = null;
    target.txtAddress = null;
    target.txtLatitude = null;
    target.txtLongitude = null;
    target.imgCancel = null;
    target.cardAddress = null;
    target.txtAddLocation = null;
    target.txtStartLocation = null;
    target.txtEndLocation = null;
    target.btnOptimize = null;
    target.chkOrigin = null;
    target.btnFinalize = null;
    target.etDriver = null;

    view2131231214.setOnClickListener(null);
    view2131231214 = null;
    view2131231201.setOnClickListener(null);
    view2131231201 = null;
    view2131231616.setOnClickListener(null);
    view2131231616 = null;
    view2131230852.setOnClickListener(null);
    view2131230852 = null;
    view2131230848.setOnClickListener(null);
    view2131230848 = null;
    view2131231040.setOnClickListener(null);
    view2131231040 = null;
  }
}
