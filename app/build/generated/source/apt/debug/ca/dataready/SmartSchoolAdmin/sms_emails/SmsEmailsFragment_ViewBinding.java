// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.sms_emails;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import info.hoang8f.android.segmented.SegmentedGroup;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SmsEmailsFragment_ViewBinding implements Unbinder {
  private SmsEmailsFragment target;

  private View view2131231291;

  private View view2131231290;

  private View view2131231638;

  @UiThread
  public SmsEmailsFragment_ViewBinding(final SmsEmailsFragment target, View source) {
    this.target = target;

    View view;
    target.txtEmergecyCommunicationTrip = Utils.findRequiredViewAsType(source, R.id.txtEmergecyCommunicationTrip, "field 'txtEmergecyCommunicationTrip'", TextView.class);
    target.cardEmergencyContactTrip = Utils.findRequiredViewAsType(source, R.id.card_emergency_contact_trip, "field 'cardEmergencyContactTrip'", CardView.class);
    target.rbGrade = Utils.findRequiredViewAsType(source, R.id.rb_grade, "field 'rbGrade'", RadioButton.class);
    target.rbClass = Utils.findRequiredViewAsType(source, R.id.rb_class, "field 'rbClass'", RadioButton.class);
    target.rbGroupOfStudent = Utils.findRequiredViewAsType(source, R.id.rb_group_of_student, "field 'rbGroupOfStudent'", RadioButton.class);
    target.sgSe = Utils.findRequiredViewAsType(source, R.id.sgSe, "field 'sgSe'", SegmentedGroup.class);
    target.txtGrade = Utils.findRequiredViewAsType(source, R.id.txt_Grade, "field 'txtGrade'", TextView.class);
    target.gradeImg = Utils.findRequiredViewAsType(source, R.id.grade_img, "field 'gradeImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.linear_Grade, "field 'linearGrade' and method 'onViewClicked'");
    target.linearGrade = Utils.castView(view, R.id.linear_Grade, "field 'linearGrade'", RelativeLayout.class);
    view2131231291 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtClass = Utils.findRequiredViewAsType(source, R.id.txt_Class, "field 'txtClass'", TextView.class);
    target.classImg = Utils.findRequiredViewAsType(source, R.id.class_img, "field 'classImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.linear_Class, "field 'linearClass' and method 'onViewClicked'");
    target.linearClass = Utils.castView(view, R.id.linear_Class, "field 'linearClass'", RelativeLayout.class);
    view2131231290 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.etDescription = Utils.findRequiredViewAsType(source, R.id.et_description, "field 'etDescription'", EditText.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.txt_edit, "field 'txtEdit' and method 'onViewClicked'");
    target.txtEdit = Utils.castView(view, R.id.txt_edit, "field 'txtEdit'", TextView.class);
    view2131231638 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.btnSubmit = Utils.findRequiredViewAsType(source, R.id.btn_submit, "field 'btnSubmit'", Button.class);
    target.layoutEmergencyTrip = Utils.findRequiredViewAsType(source, R.id.layout_emergency_trip, "field 'layoutEmergencyTrip'", CardView.class);
    target.txtError = Utils.findRequiredViewAsType(source, R.id.txt_error, "field 'txtError'", TextView.class);
    target.viewAnimator = Utils.findRequiredViewAsType(source, R.id.viewAnimator, "field 'viewAnimator'", ViewAnimator.class);
    target.scrollView = Utils.findRequiredViewAsType(source, R.id.scrollView, "field 'scrollView'", NestedScrollView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SmsEmailsFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtEmergecyCommunicationTrip = null;
    target.cardEmergencyContactTrip = null;
    target.rbGrade = null;
    target.rbClass = null;
    target.rbGroupOfStudent = null;
    target.sgSe = null;
    target.txtGrade = null;
    target.gradeImg = null;
    target.linearGrade = null;
    target.txtClass = null;
    target.classImg = null;
    target.linearClass = null;
    target.etDescription = null;
    target.recyclerView = null;
    target.txtEdit = null;
    target.btnSubmit = null;
    target.layoutEmergencyTrip = null;
    target.txtError = null;
    target.viewAnimator = null;
    target.scrollView = null;

    view2131231291.setOnClickListener(null);
    view2131231291 = null;
    view2131231290.setOnClickListener(null);
    view2131231290 = null;
    view2131231638.setOnClickListener(null);
    view2131231638 = null;
  }
}
