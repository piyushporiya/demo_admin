// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.configuration.school_info;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SchoolInfoFragment_ViewBinding implements Unbinder {
  private SchoolInfoFragment target;

  private View view2131230913;

  private View view2131230841;

  private View view2131230850;

  private View view2131230805;

  private View view2131230916;

  private View view2131230876;

  private View view2131230875;

  private View view2131230914;

  private View view2131230869;

  private View view2131230868;

  private View view2131230898;

  private View view2131230833;

  private View view2131230834;

  private View view2131230804;

  @UiThread
  public SchoolInfoFragment_ViewBinding(final SchoolInfoFragment target, View source) {
    this.target = target;

    View view;
    target.txtSchoolInfo = Utils.findRequiredViewAsType(source, R.id.txtSchoolInfo, "field 'txtSchoolInfo'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_school_info, "field 'cardSchoolInfo' and method 'onViewClicked'");
    target.cardSchoolInfo = Utils.castView(view, R.id.card_school_info, "field 'cardSchoolInfo'", CardView.class);
    view2131230913 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.etSchoolName = Utils.findRequiredViewAsType(source, R.id.et_school_name, "field 'etSchoolName'", EditText.class);
    target.etAddressLine1 = Utils.findRequiredViewAsType(source, R.id.et_address_line_1, "field 'etAddressLine1'", EditText.class);
    target.etAddressLine2 = Utils.findRequiredViewAsType(source, R.id.et_address_line_2, "field 'etAddressLine2'", EditText.class);
    target.etAddressLine3 = Utils.findRequiredViewAsType(source, R.id.et_address_line_3, "field 'etAddressLine3'", EditText.class);
    target.etCity = Utils.findRequiredViewAsType(source, R.id.et_city, "field 'etCity'", EditText.class);
    target.etState = Utils.findRequiredViewAsType(source, R.id.et_state, "field 'etState'", EditText.class);
    target.etPinCode = Utils.findRequiredViewAsType(source, R.id.et_pin_code, "field 'etPinCode'", EditText.class);
    target.schoolProfilePic = Utils.findRequiredViewAsType(source, R.id.school_profile_pic, "field 'schoolProfilePic'", ImageView.class);
    target.linaerCaptureImage = Utils.findRequiredViewAsType(source, R.id.linaer_capture_image, "field 'linaerCaptureImage'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.btn_edit, "field 'btnEdit' and method 'onViewClicked'");
    target.btnEdit = Utils.castView(view, R.id.btn_edit, "field 'btnEdit'", Button.class);
    view2131230841 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_next, "field 'btnNext' and method 'onViewClicked'");
    target.btnNext = Utils.castView(view, R.id.btn_next, "field 'btnNext'", Button.class);
    view2131230850 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutSchoolInfo = Utils.findRequiredViewAsType(source, R.id.layout_school_info, "field 'layoutSchoolInfo'", CardView.class);
    target.txtTerms = Utils.findRequiredViewAsType(source, R.id.txt_terms, "field 'txtTerms'", TextView.class);
    view = Utils.findRequiredView(source, R.id.add_more_terms, "field 'addMoreTerms' and method 'onViewClicked'");
    target.addMoreTerms = Utils.castView(view, R.id.add_more_terms, "field 'addMoreTerms'", ImageView.class);
    view2131230805 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.card_terms, "field 'cardTerms' and method 'onViewClicked'");
    target.cardTerms = Utils.castView(view, R.id.card_terms, "field 'cardTerms'", CardView.class);
    view2131230916 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rvTerms = Utils.findRequiredViewAsType(source, R.id.rv_terms, "field 'rvTerms'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.btn_tr_previous, "field 'btnTrPrevious' and method 'onViewClicked'");
    target.btnTrPrevious = Utils.castView(view, R.id.btn_tr_previous, "field 'btnTrPrevious'", Button.class);
    view2131230876 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_tr_next, "field 'btnTrNext' and method 'onViewClicked'");
    target.btnTrNext = Utils.castView(view, R.id.btn_tr_next, "field 'btnTrNext'", Button.class);
    view2131230875 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutTerms = Utils.findRequiredViewAsType(source, R.id.layout_terms, "field 'layoutTerms'", CardView.class);
    target.scrollView = Utils.findRequiredViewAsType(source, R.id.scrollView, "field 'scrollView'", NestedScrollView.class);
    target.txtError = Utils.findRequiredViewAsType(source, R.id.txt_error, "field 'txtError'", TextView.class);
    target.viewAnimator = Utils.findRequiredViewAsType(source, R.id.viewAnimator, "field 'viewAnimator'", ViewAnimator.class);
    target.txtSchoolTiming = Utils.findRequiredViewAsType(source, R.id.txtSchoolTiming, "field 'txtSchoolTiming'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_school_timing, "field 'cardSchoolTiming' and method 'onViewClicked'");
    target.cardSchoolTiming = Utils.castView(view, R.id.card_school_timing, "field 'cardSchoolTiming'", CardView.class);
    view2131230914 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_st_previous, "field 'btnStPrevious' and method 'onViewClicked'");
    target.btnStPrevious = Utils.castView(view, R.id.btn_st_previous, "field 'btnStPrevious'", Button.class);
    view2131230869 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_st_next, "field 'btnStNext' and method 'onViewClicked'");
    target.btnStNext = Utils.castView(view, R.id.btn_st_next, "field 'btnStNext'", Button.class);
    view2131230868 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutSchoolTiming = Utils.findRequiredViewAsType(source, R.id.layout_school_timing, "field 'layoutSchoolTiming'", CardView.class);
    target.txtBusInfo = Utils.findRequiredViewAsType(source, R.id.txtBusInfo, "field 'txtBusInfo'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_bus_info, "field 'cardBusInfo' and method 'onViewClicked'");
    target.cardBusInfo = Utils.castView(view, R.id.card_bus_info, "field 'cardBusInfo'", CardView.class);
    view2131230898 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.etBusStandAddress = Utils.findRequiredViewAsType(source, R.id.et_bus_stand_address, "field 'etBusStandAddress'", AutoCompleteTextView.class);
    view = Utils.findRequiredView(source, R.id.btn_bus_previous, "field 'btnBusPrevious' and method 'onViewClicked'");
    target.btnBusPrevious = Utils.castView(view, R.id.btn_bus_previous, "field 'btnBusPrevious'", Button.class);
    view2131230833 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_bus_update, "field 'btnBusUpdate' and method 'onViewClicked'");
    target.btnBusUpdate = Utils.castView(view, R.id.btn_bus_update, "field 'btnBusUpdate'", Button.class);
    view2131230834 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutBusInfo = Utils.findRequiredViewAsType(source, R.id.layout_bus_info, "field 'layoutBusInfo'", CardView.class);
    target.imgBusAddress = Utils.findRequiredViewAsType(source, R.id.img_bus_address, "field 'imgBusAddress'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.add_more_school_timing, "field 'addMoreSchoolTiming' and method 'onViewClicked'");
    target.addMoreSchoolTiming = Utils.castView(view, R.id.add_more_school_timing, "field 'addMoreSchoolTiming'", ImageView.class);
    view2131230804 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rvSchoolTiming = Utils.findRequiredViewAsType(source, R.id.rv_school_timing, "field 'rvSchoolTiming'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SchoolInfoFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtSchoolInfo = null;
    target.cardSchoolInfo = null;
    target.etSchoolName = null;
    target.etAddressLine1 = null;
    target.etAddressLine2 = null;
    target.etAddressLine3 = null;
    target.etCity = null;
    target.etState = null;
    target.etPinCode = null;
    target.schoolProfilePic = null;
    target.linaerCaptureImage = null;
    target.btnEdit = null;
    target.btnNext = null;
    target.layoutSchoolInfo = null;
    target.txtTerms = null;
    target.addMoreTerms = null;
    target.cardTerms = null;
    target.rvTerms = null;
    target.btnTrPrevious = null;
    target.btnTrNext = null;
    target.layoutTerms = null;
    target.scrollView = null;
    target.txtError = null;
    target.viewAnimator = null;
    target.txtSchoolTiming = null;
    target.cardSchoolTiming = null;
    target.btnStPrevious = null;
    target.btnStNext = null;
    target.layoutSchoolTiming = null;
    target.txtBusInfo = null;
    target.cardBusInfo = null;
    target.etBusStandAddress = null;
    target.btnBusPrevious = null;
    target.btnBusUpdate = null;
    target.layoutBusInfo = null;
    target.imgBusAddress = null;
    target.addMoreSchoolTiming = null;
    target.rvSchoolTiming = null;

    view2131230913.setOnClickListener(null);
    view2131230913 = null;
    view2131230841.setOnClickListener(null);
    view2131230841 = null;
    view2131230850.setOnClickListener(null);
    view2131230850 = null;
    view2131230805.setOnClickListener(null);
    view2131230805 = null;
    view2131230916.setOnClickListener(null);
    view2131230916 = null;
    view2131230876.setOnClickListener(null);
    view2131230876 = null;
    view2131230875.setOnClickListener(null);
    view2131230875 = null;
    view2131230914.setOnClickListener(null);
    view2131230914 = null;
    view2131230869.setOnClickListener(null);
    view2131230869 = null;
    view2131230868.setOnClickListener(null);
    view2131230868 = null;
    view2131230898.setOnClickListener(null);
    view2131230898 = null;
    view2131230833.setOnClickListener(null);
    view2131230833 = null;
    view2131230834.setOnClickListener(null);
    view2131230834 = null;
    view2131230804.setOnClickListener(null);
    view2131230804 = null;
  }
}
