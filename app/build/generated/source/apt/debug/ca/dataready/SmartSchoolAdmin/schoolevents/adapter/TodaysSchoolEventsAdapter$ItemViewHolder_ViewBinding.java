// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.schoolevents.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class TodaysSchoolEventsAdapter$ItemViewHolder_ViewBinding implements Unbinder {
  private TodaysSchoolEventsAdapter.ItemViewHolder target;

  @UiThread
  public TodaysSchoolEventsAdapter$ItemViewHolder_ViewBinding(TodaysSchoolEventsAdapter.ItemViewHolder target,
      View source) {
    this.target = target;

    target.txtLongMsg = Utils.findRequiredViewAsType(source, R.id.txt_long_msg, "field 'txtLongMsg'", TextView.class);
    target.txtDate = Utils.findRequiredViewAsType(source, R.id.txt_date, "field 'txtDate'", TextView.class);
    target.txtTime = Utils.findRequiredViewAsType(source, R.id.txt_time, "field 'txtTime'", TextView.class);
    target.img = Utils.findRequiredViewAsType(source, R.id.img, "field 'img'", ImageView.class);
    target.txtMsg = Utils.findRequiredViewAsType(source, R.id.txt_msg, "field 'txtMsg'", TextView.class);
    target.txtFileName = Utils.findRequiredViewAsType(source, R.id.txt_file_name, "field 'txtFileName'", TextView.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    target.txtViewMore = Utils.findRequiredViewAsType(source, R.id.txt_view_more, "field 'txtViewMore'", TextView.class);
    target.status = Utils.findRequiredView(source, R.id.status, "field 'status'");
    target.imgMore = Utils.findRequiredViewAsType(source, R.id.img_more, "field 'imgMore'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    TodaysSchoolEventsAdapter.ItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtLongMsg = null;
    target.txtDate = null;
    target.txtTime = null;
    target.img = null;
    target.txtMsg = null;
    target.txtFileName = null;
    target.recyclerView = null;
    target.txtViewMore = null;
    target.status = null;
    target.imgMore = null;
  }
}
