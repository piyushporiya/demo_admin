// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.login;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LoginActivity_ViewBinding implements Unbinder {
  private LoginActivity target;

  private View view2131230918;

  private View view2131231609;

  private View view2131231612;

  @UiThread
  public LoginActivity_ViewBinding(LoginActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public LoginActivity_ViewBinding(final LoginActivity target, View source) {
    this.target = target;

    View view;
    target.etEmail = Utils.findRequiredViewAsType(source, R.id.et_Email, "field 'etEmail'", EditText.class);
    target.etPassword = Utils.findRequiredViewAsType(source, R.id.et_Password, "field 'etPassword'", EditText.class);
    view = Utils.findRequiredView(source, R.id.cb_Remember, "field 'cbRemember' and method 'onViewClicked'");
    target.cbRemember = Utils.castView(view, R.id.cb_Remember, "field 'cbRemember'", CheckBox.class);
    view2131230918 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.txt_ForgotPass, "field 'txtForgotPass' and method 'onViewClicked'");
    target.txtForgotPass = Utils.castView(view, R.id.txt_ForgotPass, "field 'txtForgotPass'", TextView.class);
    view2131231609 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.txt_SignIn, "field 'txtSignIn' and method 'onViewClicked'");
    target.txtSignIn = Utils.castView(view, R.id.txt_SignIn, "field 'txtSignIn'", Button.class);
    view2131231612 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtVersion = Utils.findRequiredViewAsType(source, R.id.txt_version, "field 'txtVersion'", TextView.class);
    target.txtEnv = Utils.findRequiredViewAsType(source, R.id.txt_env, "field 'txtEnv'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    LoginActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.etEmail = null;
    target.etPassword = null;
    target.cbRemember = null;
    target.txtForgotPass = null;
    target.txtSignIn = null;
    target.txtVersion = null;
    target.txtEnv = null;

    view2131230918.setOnClickListener(null);
    view2131230918 = null;
    view2131231609.setOnClickListener(null);
    view2131231609 = null;
    view2131231612.setOnClickListener(null);
    view2131231612 = null;
  }
}
