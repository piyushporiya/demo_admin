// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.dashboard.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DashBoardAdapter$BoardViewHolder_ViewBinding implements Unbinder {
  private DashBoardAdapter.BoardViewHolder target;

  @UiThread
  public DashBoardAdapter$BoardViewHolder_ViewBinding(DashBoardAdapter.BoardViewHolder target,
      View source) {
    this.target = target;

    target.image = Utils.findRequiredViewAsType(source, R.id.image, "field 'image'", ImageView.class);
    target.name = Utils.findRequiredViewAsType(source, R.id.name, "field 'name'", TextView.class);
    target.counter = Utils.findRequiredViewAsType(source, R.id.counter, "field 'counter'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DashBoardAdapter.BoardViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.image = null;
    target.name = null;
    target.counter = null;
  }
}
