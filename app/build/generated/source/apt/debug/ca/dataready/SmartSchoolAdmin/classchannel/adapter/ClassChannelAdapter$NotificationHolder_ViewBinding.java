// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.classchannel.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ClassChannelAdapter$NotificationHolder_ViewBinding implements Unbinder {
  private ClassChannelAdapter.NotificationHolder target;

  @UiThread
  public ClassChannelAdapter$NotificationHolder_ViewBinding(ClassChannelAdapter.NotificationHolder target,
      View source) {
    this.target = target;

    target.txtSortMsg = Utils.findRequiredViewAsType(source, R.id.txt_sort_msg, "field 'txtSortMsg'", TextView.class);
    target.txtDate = Utils.findRequiredViewAsType(source, R.id.txt_date, "field 'txtDate'", TextView.class);
    target.txtLongMsg = Utils.findRequiredViewAsType(source, R.id.txt_long_msg, "field 'txtLongMsg'", TextView.class);
    target.img = Utils.findRequiredViewAsType(source, R.id.img, "field 'img'", ImageView.class);
    target.txtFileName = Utils.findRequiredViewAsType(source, R.id.txt_file_name, "field 'txtFileName'", TextView.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    target.txtViewMore = Utils.findRequiredViewAsType(source, R.id.txt_view_more, "field 'txtViewMore'", TextView.class);
    target.txtClassGrade = Utils.findRequiredViewAsType(source, R.id.txt_class_grade, "field 'txtClassGrade'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ClassChannelAdapter.NotificationHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtSortMsg = null;
    target.txtDate = null;
    target.txtLongMsg = null;
    target.img = null;
    target.txtFileName = null;
    target.recyclerView = null;
    target.txtViewMore = null;
    target.txtClassGrade = null;
  }
}
