// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DrawerSubItemAdapter$ItemViewHolder_ViewBinding implements Unbinder {
  private DrawerSubItemAdapter.ItemViewHolder target;

  @UiThread
  public DrawerSubItemAdapter$ItemViewHolder_ViewBinding(DrawerSubItemAdapter.ItemViewHolder target,
      View source) {
    this.target = target;

    target.subIcon = Utils.findRequiredViewAsType(source, R.id.sub_icon, "field 'subIcon'", ImageView.class);
    target.subTitle = Utils.findRequiredViewAsType(source, R.id.sub_title, "field 'subTitle'", TextView.class);
    target.subView = Utils.findRequiredViewAsType(source, R.id.sub_view, "field 'subView'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DrawerSubItemAdapter.ItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.subIcon = null;
    target.subTitle = null;
    target.subView = null;
  }
}
