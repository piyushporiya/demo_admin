// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.route.history;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import info.hoang8f.android.segmented.SegmentedGroup;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RouteHistoryActivity_ViewBinding implements Unbinder {
  private RouteHistoryActivity target;

  private View view2131231296;

  private View view2131231309;

  @UiThread
  public RouteHistoryActivity_ViewBinding(RouteHistoryActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public RouteHistoryActivity_ViewBinding(final RouteHistoryActivity target, View source) {
    this.target = target;

    View view;
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.rbPassangerInfo = Utils.findRequiredViewAsType(source, R.id.rb_passanger_info, "field 'rbPassangerInfo'", RadioButton.class);
    target.rbTripInfo = Utils.findRequiredViewAsType(source, R.id.rb_trip_info, "field 'rbTripInfo'", RadioButton.class);
    target.sgInfo = Utils.findRequiredViewAsType(source, R.id.sgInfo, "field 'sgInfo'", SegmentedGroup.class);
    target.txtError = Utils.findRequiredViewAsType(source, R.id.txt_error, "field 'txtError'", TextView.class);
    target.parentViewAnimator = Utils.findRequiredViewAsType(source, R.id.viewAnimator, "field 'parentViewAnimator'", ViewAnimator.class);
    target.piRecyclerView = Utils.findRequiredViewAsType(source, R.id.piRecyclerView, "field 'piRecyclerView'", SuperRecyclerView.class);
    target.infoviewAnimator = Utils.findRequiredViewAsType(source, R.id.infoviewAnimator, "field 'infoviewAnimator'", ViewAnimator.class);
    target.txtdate = Utils.findRequiredViewAsType(source, R.id.txtdate, "field 'txtdate'", TextView.class);
    view = Utils.findRequiredView(source, R.id.linear_date, "field 'linearDate' and method 'onViewClicked'");
    target.linearDate = Utils.castView(view, R.id.linear_date, "field 'linearDate'", RelativeLayout.class);
    view2131231296 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtTrip = Utils.findRequiredViewAsType(source, R.id.txt_trip, "field 'txtTrip'", TextView.class);
    view = Utils.findRequiredView(source, R.id.linear_trips, "field 'linearTrips' and method 'onViewClicked'");
    target.linearTrips = Utils.castView(view, R.id.linear_trips, "field 'linearTrips'", RelativeLayout.class);
    view2131231309 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    RouteHistoryActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.rbPassangerInfo = null;
    target.rbTripInfo = null;
    target.sgInfo = null;
    target.txtError = null;
    target.parentViewAnimator = null;
    target.piRecyclerView = null;
    target.infoviewAnimator = null;
    target.txtdate = null;
    target.linearDate = null;
    target.txtTrip = null;
    target.linearTrips = null;

    view2131231296.setOnClickListener(null);
    view2131231296 = null;
    view2131231309.setOnClickListener(null);
    view2131231309 = null;
  }
}
