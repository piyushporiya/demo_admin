// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.appointment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.calendarview.CalendarCustomView;
import com.alamkanak.weekview.WeekView;
import info.hoang8f.android.segmented.SegmentedGroup;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AppointmentFragment_ViewBinding implements Unbinder {
  private AppointmentFragment target;

  private View view2131231304;

  private View view2131231295;

  private View view2131231293;

  private View view2131230831;

  private View view2131230830;

  private View view2131231206;

  private View view2131231298;

  private View view2131231152;

  @UiThread
  public AppointmentFragment_ViewBinding(final AppointmentFragment target, View source) {
    this.target = target;

    View view;
    target.rbSchedule = Utils.findRequiredViewAsType(source, R.id.rb_schedule, "field 'rbSchedule'", RadioButton.class);
    target.rbUpcoming = Utils.findRequiredViewAsType(source, R.id.rb_upcoming, "field 'rbUpcoming'", RadioButton.class);
    target.rbPast = Utils.findRequiredViewAsType(source, R.id.rb_past, "field 'rbPast'", RadioButton.class);
    target.sgAppointment = Utils.findRequiredViewAsType(source, R.id.sgAppointment, "field 'sgAppointment'", SegmentedGroup.class);
    target.calenderView = Utils.findRequiredViewAsType(source, R.id.calenderView, "field 'calenderView'", CalendarCustomView.class);
    target.txtNoUpcomingData = Utils.findRequiredViewAsType(source, R.id.txt_no_upcoming_data, "field 'txtNoUpcomingData'", TextView.class);
    target.upcomingViewAnimator = Utils.findRequiredViewAsType(source, R.id.upcoming_viewAnimator, "field 'upcomingViewAnimator'", ViewAnimator.class);
    target.rvPast = Utils.findRequiredViewAsType(source, R.id.rv_past, "field 'rvPast'", RecyclerView.class);
    target.txtNoPastData = Utils.findRequiredViewAsType(source, R.id.txt_no_past_data, "field 'txtNoPastData'", TextView.class);
    target.pastViewAnimator = Utils.findRequiredViewAsType(source, R.id.past_viewAnimator, "field 'pastViewAnimator'", ViewAnimator.class);
    target.mainViewAnimator = Utils.findRequiredViewAsType(source, R.id.main_viewAnimator, "field 'mainViewAnimator'", ViewAnimator.class);
    target.parentViewAnimator = Utils.findRequiredViewAsType(source, R.id.parent_viewAnimator, "field 'parentViewAnimator'", ViewAnimator.class);
    target.swipeRefreshLayoutPast = Utils.findRequiredViewAsType(source, R.id.swipeRefreshLayoutPast, "field 'swipeRefreshLayoutPast'", SwipeRefreshLayout.class);
    target.swipeRefreshLayoutUpcoming = Utils.findRequiredViewAsType(source, R.id.swipeRefreshLayoutUpcoming, "field 'swipeRefreshLayoutUpcoming'", SwipeRefreshLayout.class);
    target.mWeekView = Utils.findRequiredViewAsType(source, R.id.weekView, "field 'mWeekView'", WeekView.class);
    target.edittext = Utils.findRequiredViewAsType(source, R.id.edittext, "field 'edittext'", EditText.class);
    target.txtGrade = Utils.findRequiredViewAsType(source, R.id.txt_grade, "field 'txtGrade'", TextView.class);
    target.gradeImg = Utils.findRequiredViewAsType(source, R.id.grade_img, "field 'gradeImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.linear_grade, "field 'linearGrade' and method 'onViewClicked'");
    target.linearGrade = Utils.castView(view, R.id.linear_grade, "field 'linearGrade'", RelativeLayout.class);
    view2131231304 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtClass = Utils.findRequiredViewAsType(source, R.id.txt_class, "field 'txtClass'", TextView.class);
    target.classImg = Utils.findRequiredViewAsType(source, R.id.class_img, "field 'classImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.linear_class, "field 'linearClass' and method 'onViewClicked'");
    target.linearClass = Utils.castView(view, R.id.linear_class, "field 'linearClass'", RelativeLayout.class);
    view2131231295 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtTime = Utils.findRequiredViewAsType(source, R.id.txt_time, "field 'txtTime'", TextView.class);
    target.timeImg = Utils.findRequiredViewAsType(source, R.id.time_img, "field 'timeImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.linear_Time, "field 'linearTime' and method 'onViewClicked'");
    target.linearTime = Utils.castView(view, R.id.linear_Time, "field 'linearTime'", RelativeLayout.class);
    view2131231293 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rvStudents = Utils.findRequiredViewAsType(source, R.id.rv_Students, "field 'rvStudents'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.btn_Cancel, "field 'btnCancel' and method 'onViewClicked'");
    target.btnCancel = Utils.castView(view, R.id.btn_Cancel, "field 'btnCancel'", Button.class);
    view2131230831 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_Assign, "field 'btnAssign' and method 'onViewClicked'");
    target.btnAssign = Utils.castView(view, R.id.btn_Assign, "field 'btnAssign'", Button.class);
    view2131230830 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rvUpcoming = Utils.findRequiredViewAsType(source, R.id.rv_upcoming, "field 'rvUpcoming'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.img_edit, "field 'imgEdit' and method 'onViewClicked'");
    target.imgEdit = Utils.castView(view, R.id.img_edit, "field 'imgEdit'", ImageView.class);
    view2131231206 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rlEdit = Utils.findRequiredViewAsType(source, R.id.rl_edit, "field 'rlEdit'", RelativeLayout.class);
    target.linearArea = Utils.findRequiredViewAsType(source, R.id.linear_area, "field 'linearArea'", LinearLayout.class);
    target.txtDuartion = Utils.findRequiredViewAsType(source, R.id.txt_duartion, "field 'txtDuartion'", TextView.class);
    target.durationImg = Utils.findRequiredViewAsType(source, R.id.duration_img, "field 'durationImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.linear_duartion, "field 'linearDuartion' and method 'onViewClicked'");
    target.linearDuartion = Utils.castView(view, R.id.linear_duartion, "field 'linearDuartion'", RelativeLayout.class);
    view2131231298 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.fab_add, "field 'fabAdd' and method 'onViewClicked'");
    target.fabAdd = Utils.castView(view, R.id.fab_add, "field 'fabAdd'", FloatingActionButton.class);
    view2131231152 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.scheduleViewAnimator = Utils.findRequiredViewAsType(source, R.id.schedule_viewAnimator, "field 'scheduleViewAnimator'", ViewAnimator.class);
    target.scheduleInnerViewAnimator = Utils.findRequiredViewAsType(source, R.id.schedule_inner_viewAnimator, "field 'scheduleInnerViewAnimator'", ViewAnimator.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AppointmentFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rbSchedule = null;
    target.rbUpcoming = null;
    target.rbPast = null;
    target.sgAppointment = null;
    target.calenderView = null;
    target.txtNoUpcomingData = null;
    target.upcomingViewAnimator = null;
    target.rvPast = null;
    target.txtNoPastData = null;
    target.pastViewAnimator = null;
    target.mainViewAnimator = null;
    target.parentViewAnimator = null;
    target.swipeRefreshLayoutPast = null;
    target.swipeRefreshLayoutUpcoming = null;
    target.mWeekView = null;
    target.edittext = null;
    target.txtGrade = null;
    target.gradeImg = null;
    target.linearGrade = null;
    target.txtClass = null;
    target.classImg = null;
    target.linearClass = null;
    target.txtTime = null;
    target.timeImg = null;
    target.linearTime = null;
    target.rvStudents = null;
    target.btnCancel = null;
    target.btnAssign = null;
    target.rvUpcoming = null;
    target.imgEdit = null;
    target.rlEdit = null;
    target.linearArea = null;
    target.txtDuartion = null;
    target.durationImg = null;
    target.linearDuartion = null;
    target.fabAdd = null;
    target.scheduleViewAnimator = null;
    target.scheduleInnerViewAnimator = null;

    view2131231304.setOnClickListener(null);
    view2131231304 = null;
    view2131231295.setOnClickListener(null);
    view2131231295 = null;
    view2131231293.setOnClickListener(null);
    view2131231293 = null;
    view2131230831.setOnClickListener(null);
    view2131230831 = null;
    view2131230830.setOnClickListener(null);
    view2131230830 = null;
    view2131231206.setOnClickListener(null);
    view2131231206 = null;
    view2131231298.setOnClickListener(null);
    view2131231298 = null;
    view2131231152.setOnClickListener(null);
    view2131231152 = null;
  }
}
