// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.route.emergency_notification.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CommunityPointsAdapter$PickUpStopViewHolder_ViewBinding implements Unbinder {
  private CommunityPointsAdapter.PickUpStopViewHolder target;

  @UiThread
  public CommunityPointsAdapter$PickUpStopViewHolder_ViewBinding(CommunityPointsAdapter.PickUpStopViewHolder target,
      View source) {
    this.target = target;

    target.txtAddress = Utils.findRequiredViewAsType(source, R.id.txt_address, "field 'txtAddress'", TextView.class);
    target.chk = Utils.findRequiredViewAsType(source, R.id.chk, "field 'chk'", CheckBox.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CommunityPointsAdapter.PickUpStopViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtAddress = null;
    target.chk = null;
  }
}
