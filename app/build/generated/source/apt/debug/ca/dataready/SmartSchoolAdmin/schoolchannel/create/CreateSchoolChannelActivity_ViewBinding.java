// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.schoolchannel.create;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CreateSchoolChannelActivity_ViewBinding implements Unbinder {
  private CreateSchoolChannelActivity target;

  private View view2131231243;

  private View view2131231198;

  private View view2131231245;

  private View view2131231486;

  @UiThread
  public CreateSchoolChannelActivity_ViewBinding(CreateSchoolChannelActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CreateSchoolChannelActivity_ViewBinding(final CreateSchoolChannelActivity target,
      View source) {
    this.target = target;

    View view;
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.txtClass = Utils.findRequiredViewAsType(source, R.id.txt_Class, "field 'txtClass'", TextView.class);
    target.llheader = Utils.findRequiredViewAsType(source, R.id.llheader, "field 'llheader'", LinearLayout.class);
    target.rvStudents = Utils.findRequiredViewAsType(source, R.id.rv_Students, "field 'rvStudents'", RecyclerView.class);
    target.txtEdit = Utils.findRequiredViewAsType(source, R.id.txt_edit, "field 'txtEdit'", TextView.class);
    target.txtNoData = Utils.findRequiredViewAsType(source, R.id.txt_NoData, "field 'txtNoData'", TextView.class);
    target.studentListViewAnimator = Utils.findRequiredViewAsType(source, R.id.student_list_viewAnimator, "field 'studentListViewAnimator'", ViewAnimator.class);
    target.title = Utils.findRequiredViewAsType(source, R.id.title, "field 'title'", EditText.class);
    target.msg = Utils.findRequiredViewAsType(source, R.id.msg, "field 'msg'", EditText.class);
    view = Utils.findRequiredView(source, R.id.iv_SpeechToText, "field 'ivSpeechToText' and method 'onViewClicked'");
    target.ivSpeechToText = Utils.castView(view, R.id.iv_SpeechToText, "field 'ivSpeechToText'", ImageView.class);
    view2131231243 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.img_attachment, "field 'imgAttachment' and method 'onViewClicked'");
    target.imgAttachment = Utils.castView(view, R.id.img_attachment, "field 'imgAttachment'", ImageView.class);
    view2131231198 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.iv_camera_AttachFile, "field 'ivCameraAttachFile' and method 'onViewClicked'");
    target.ivCameraAttachFile = Utils.castView(view, R.id.iv_camera_AttachFile, "field 'ivCameraAttachFile'", ImageView.class);
    view2131231245 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.sendfeedback, "field 'sendfeedback' and method 'onViewClicked'");
    target.sendfeedback = Utils.castView(view, R.id.sendfeedback, "field 'sendfeedback'", Button.class);
    view2131231486 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtAttachment = Utils.findRequiredViewAsType(source, R.id.txt_attachment, "field 'txtAttachment'", TextView.class);
    target.rvAttachments = Utils.findRequiredViewAsType(source, R.id.rvAttachments, "field 'rvAttachments'", RecyclerView.class);
    target.llAttachments = Utils.findRequiredViewAsType(source, R.id.llAttachments, "field 'llAttachments'", LinearLayout.class);
    target.llTextArea = Utils.findRequiredViewAsType(source, R.id.llTextArea, "field 'llTextArea'", LinearLayout.class);
    target.parentViewAnimator = Utils.findRequiredViewAsType(source, R.id.parent_viewAnimator, "field 'parentViewAnimator'", ViewAnimator.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CreateSchoolChannelActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.txtClass = null;
    target.llheader = null;
    target.rvStudents = null;
    target.txtEdit = null;
    target.txtNoData = null;
    target.studentListViewAnimator = null;
    target.title = null;
    target.msg = null;
    target.ivSpeechToText = null;
    target.imgAttachment = null;
    target.ivCameraAttachFile = null;
    target.sendfeedback = null;
    target.txtAttachment = null;
    target.rvAttachments = null;
    target.llAttachments = null;
    target.llTextArea = null;
    target.parentViewAnimator = null;

    view2131231243.setOnClickListener(null);
    view2131231243 = null;
    view2131231198.setOnClickListener(null);
    view2131231198 = null;
    view2131231245.setOnClickListener(null);
    view2131231245 = null;
    view2131231486.setOnClickListener(null);
    view2131231486 = null;
  }
}
