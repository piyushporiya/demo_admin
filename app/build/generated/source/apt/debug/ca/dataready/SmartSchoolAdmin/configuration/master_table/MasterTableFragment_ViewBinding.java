// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.configuration.master_table;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MasterTableFragment_ViewBinding implements Unbinder {
  private MasterTableFragment target;

  private View view2131231617;

  @UiThread
  public MasterTableFragment_ViewBinding(final MasterTableFragment target, View source) {
    this.target = target;

    View view;
    target.etParentRefreshFrequncy = Utils.findRequiredViewAsType(source, R.id.et_parent_refresh_frequncy, "field 'etParentRefreshFrequncy'", EditText.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    target.txtError = Utils.findRequiredViewAsType(source, R.id.txt_error, "field 'txtError'", TextView.class);
    target.viewAnimator = Utils.findRequiredViewAsType(source, R.id.viewAnimator, "field 'viewAnimator'", ViewAnimator.class);
    view = Utils.findRequiredView(source, R.id.txt_add_more, "field 'txtAddMore' and method 'onViewClicked'");
    target.txtAddMore = Utils.castView(view, R.id.txt_add_more, "field 'txtAddMore'", TextView.class);
    view2131231617 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    MasterTableFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.etParentRefreshFrequncy = null;
    target.recyclerView = null;
    target.txtError = null;
    target.viewAnimator = null;
    target.txtAddMore = null;

    view2131231617.setOnClickListener(null);
    view2131231617 = null;
  }
}
