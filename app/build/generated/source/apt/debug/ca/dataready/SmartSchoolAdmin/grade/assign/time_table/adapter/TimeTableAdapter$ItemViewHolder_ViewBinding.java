// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.grade.assign.time_table.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class TimeTableAdapter$ItemViewHolder_ViewBinding implements Unbinder {
  private TimeTableAdapter.ItemViewHolder target;

  @UiThread
  public TimeTableAdapter$ItemViewHolder_ViewBinding(TimeTableAdapter.ItemViewHolder target,
      View source) {
    this.target = target;

    target.txtTime = Utils.findRequiredViewAsType(source, R.id.txt_time, "field 'txtTime'", TextView.class);
    target.txtSlotOnMon = Utils.findRequiredViewAsType(source, R.id.txt_slot_on_mon, "field 'txtSlotOnMon'", TextView.class);
    target.txtSlotOnTue = Utils.findRequiredViewAsType(source, R.id.txt_slot_on_tue, "field 'txtSlotOnTue'", TextView.class);
    target.txtSlotOnWed = Utils.findRequiredViewAsType(source, R.id.txt_slot_on_wed, "field 'txtSlotOnWed'", TextView.class);
    target.txtSlotOnThu = Utils.findRequiredViewAsType(source, R.id.txt_slot_on_thu, "field 'txtSlotOnThu'", TextView.class);
    target.txtSlotOnFri = Utils.findRequiredViewAsType(source, R.id.txt_slot_on_fri, "field 'txtSlotOnFri'", TextView.class);
    target.txtSlotOnSat = Utils.findRequiredViewAsType(source, R.id.txt_slot_on_sat, "field 'txtSlotOnSat'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    TimeTableAdapter.ItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtTime = null;
    target.txtSlotOnMon = null;
    target.txtSlotOnTue = null;
    target.txtSlotOnWed = null;
    target.txtSlotOnThu = null;
    target.txtSlotOnFri = null;
    target.txtSlotOnSat = null;
  }
}
