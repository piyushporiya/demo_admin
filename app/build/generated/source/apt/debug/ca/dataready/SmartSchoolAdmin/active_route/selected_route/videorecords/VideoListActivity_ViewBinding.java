// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.active_route.selected_route.videorecords;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class VideoListActivity_ViewBinding implements Unbinder {
  private VideoListActivity target;

  @UiThread
  public VideoListActivity_ViewBinding(VideoListActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public VideoListActivity_ViewBinding(VideoListActivity target, View source) {
    this.target = target;

    target.viewAnimator = Utils.findRequiredViewAsType(source, R.id.viewAnimator, "field 'viewAnimator'", ViewAnimator.class);
    target.rvRoutes = Utils.findRequiredViewAsType(source, R.id.rvRoutes, "field 'rvRoutes'", RecyclerView.class);
    target.txtEmptyView = Utils.findRequiredViewAsType(source, R.id.txt_empty_view, "field 'txtEmptyView'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    VideoListActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.viewAnimator = null;
    target.rvRoutes = null;
    target.txtEmptyView = null;
  }
}
