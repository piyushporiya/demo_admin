// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.fileselectoer;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Views.CenterTitleToolbar;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FileSelectionActivity_ViewBinding implements Unbinder {
  private FileSelectionActivity target;

  @UiThread
  public FileSelectionActivity_ViewBinding(FileSelectionActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public FileSelectionActivity_ViewBinding(FileSelectionActivity target, View source) {
    this.target = target;

    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", CenterTitleToolbar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FileSelectionActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
  }
}
