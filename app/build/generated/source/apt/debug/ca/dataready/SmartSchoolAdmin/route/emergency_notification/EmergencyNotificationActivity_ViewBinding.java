// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.route.emergency_notification;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import info.hoang8f.android.segmented.SegmentedGroup;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EmergencyNotificationActivity_ViewBinding implements Unbinder {
  private EmergencyNotificationActivity target;

  private View view2131231308;

  private View view2131230871;

  private View view2131230902;

  private View view2131230901;

  private View view2131230872;

  private View view2131231638;

  @UiThread
  public EmergencyNotificationActivity_ViewBinding(EmergencyNotificationActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public EmergencyNotificationActivity_ViewBinding(final EmergencyNotificationActivity target,
      View source) {
    this.target = target;

    View view;
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.txtTrip = Utils.findRequiredViewAsType(source, R.id.txt_trip, "field 'txtTrip'", TextView.class);
    view = Utils.findRequiredView(source, R.id.linear_trip, "field 'linearTrip' and method 'onViewClicked'");
    target.linearTrip = Utils.castView(view, R.id.linear_trip, "field 'linearTrip'", RelativeLayout.class);
    view2131231308 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rbAll = Utils.findRequiredViewAsType(source, R.id.rb_all, "field 'rbAll'", RadioButton.class);
    target.rbCommunityPoint = Utils.findRequiredViewAsType(source, R.id.rb_community_point, "field 'rbCommunityPoint'", RadioButton.class);
    target.sgSe = Utils.findRequiredViewAsType(source, R.id.sgSe, "field 'sgSe'", SegmentedGroup.class);
    view = Utils.findRequiredView(source, R.id.btn_submit, "field 'btnSubmit' and method 'onViewClicked'");
    target.btnSubmit = Utils.castView(view, R.id.btn_submit, "field 'btnSubmit'", Button.class);
    view2131230871 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtError = Utils.findRequiredViewAsType(source, R.id.txt_error, "field 'txtError'", TextView.class);
    target.viewAnimator = Utils.findRequiredViewAsType(source, R.id.viewAnimator, "field 'viewAnimator'", ViewAnimator.class);
    target.rbGroupOfStudent = Utils.findRequiredViewAsType(source, R.id.rb_group_of_student, "field 'rbGroupOfStudent'", RadioButton.class);
    target.ssRecyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'ssRecyclerView'", RecyclerView.class);
    target.txtEmergecyCommunicationTrip = Utils.findRequiredViewAsType(source, R.id.txtEmergecyCommunicationTrip, "field 'txtEmergecyCommunicationTrip'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_emergency_contact_trip, "field 'cardEmergencyContactTrip' and method 'onViewClicked'");
    target.cardEmergencyContactTrip = Utils.castView(view, R.id.card_emergency_contact_trip, "field 'cardEmergencyContactTrip'", CardView.class);
    view2131230902 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.etTitle = Utils.findRequiredViewAsType(source, R.id.et_title, "field 'etTitle'", EditText.class);
    target.etDescription = Utils.findRequiredViewAsType(source, R.id.et_description, "field 'etDescription'", EditText.class);
    target.layoutEmergencyTrip = Utils.findRequiredViewAsType(source, R.id.layout_emergency_trip, "field 'layoutEmergencyTrip'", CardView.class);
    target.txtEmergecyCommunicationRoute = Utils.findRequiredViewAsType(source, R.id.txtEmergecyCommunicationRoute, "field 'txtEmergecyCommunicationRoute'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_emergency_contact_route, "field 'cardEmergencyContactRoute' and method 'onViewClicked'");
    target.cardEmergencyContactRoute = Utils.castView(view, R.id.card_emergency_contact_route, "field 'cardEmergencyContactRoute'", CardView.class);
    view2131230901 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.etRouteTitle = Utils.findRequiredViewAsType(source, R.id.et_route_title, "field 'etRouteTitle'", EditText.class);
    target.etRouteDescription = Utils.findRequiredViewAsType(source, R.id.et_route_description, "field 'etRouteDescription'", EditText.class);
    target.layoutEmergencyContactRoute = Utils.findRequiredViewAsType(source, R.id.layout_emergency_contact_route, "field 'layoutEmergencyContactRoute'", CardView.class);
    view = Utils.findRequiredView(source, R.id.btn_submit_route, "field 'btnSubmitRoute' and method 'onViewClicked'");
    target.btnSubmitRoute = Utils.castView(view, R.id.btn_submit_route, "field 'btnSubmitRoute'", Button.class);
    view2131230872 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.txt_edit, "field 'txtEdit' and method 'onViewClicked'");
    target.txtEdit = Utils.castView(view, R.id.txt_edit, "field 'txtEdit'", TextView.class);
    view2131231638 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    EmergencyNotificationActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.txtTrip = null;
    target.linearTrip = null;
    target.rbAll = null;
    target.rbCommunityPoint = null;
    target.sgSe = null;
    target.btnSubmit = null;
    target.txtError = null;
    target.viewAnimator = null;
    target.rbGroupOfStudent = null;
    target.ssRecyclerView = null;
    target.txtEmergecyCommunicationTrip = null;
    target.cardEmergencyContactTrip = null;
    target.etTitle = null;
    target.etDescription = null;
    target.layoutEmergencyTrip = null;
    target.txtEmergecyCommunicationRoute = null;
    target.cardEmergencyContactRoute = null;
    target.etRouteTitle = null;
    target.etRouteDescription = null;
    target.layoutEmergencyContactRoute = null;
    target.btnSubmitRoute = null;
    target.txtEdit = null;

    view2131231308.setOnClickListener(null);
    view2131231308 = null;
    view2131230871.setOnClickListener(null);
    view2131230871 = null;
    view2131230902.setOnClickListener(null);
    view2131230902 = null;
    view2131230901.setOnClickListener(null);
    view2131230901 = null;
    view2131230872.setOnClickListener(null);
    view2131230872 = null;
    view2131231638.setOnClickListener(null);
    view2131231638 = null;
  }
}
