// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.grade.assign.student.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import com.mikhaellopez.circularimageview.CircularImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AssignedStudentAdapter$ItemViewHolder_ViewBinding implements Unbinder {
  private AssignedStudentAdapter.ItemViewHolder target;

  @UiThread
  public AssignedStudentAdapter$ItemViewHolder_ViewBinding(AssignedStudentAdapter.ItemViewHolder target,
      View source) {
    this.target = target;

    target.ivStudentImage = Utils.findRequiredViewAsType(source, R.id.iv_driver_image, "field 'ivStudentImage'", CircularImageView.class);
    target.txtStudentName = Utils.findRequiredViewAsType(source, R.id.txt_student_name, "field 'txtStudentName'", TextView.class);
    target.imgMore = Utils.findRequiredViewAsType(source, R.id.img_more, "field 'imgMore'", ImageView.class);
    target.txtEmail = Utils.findRequiredViewAsType(source, R.id.txt_email, "field 'txtEmail'", TextView.class);
    target.txtCall = Utils.findRequiredViewAsType(source, R.id.txt_call, "field 'txtCall'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AssignedStudentAdapter.ItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivStudentImage = null;
    target.txtStudentName = null;
    target.imgMore = null;
    target.txtEmail = null;
    target.txtCall = null;
  }
}
