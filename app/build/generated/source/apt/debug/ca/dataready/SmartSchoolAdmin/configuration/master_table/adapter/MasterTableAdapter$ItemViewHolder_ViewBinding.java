// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.configuration.master_table.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MasterTableAdapter$ItemViewHolder_ViewBinding implements Unbinder {
  private MasterTableAdapter.ItemViewHolder target;

  @UiThread
  public MasterTableAdapter$ItemViewHolder_ViewBinding(MasterTableAdapter.ItemViewHolder target,
      View source) {
    this.target = target;

    target.txtId = Utils.findRequiredViewAsType(source, R.id.txt_id, "field 'txtId'", TextView.class);
    target.txtStartTime = Utils.findRequiredViewAsType(source, R.id.txt_start_time, "field 'txtStartTime'", TextView.class);
    target.txtEndTime = Utils.findRequiredViewAsType(source, R.id.txt_end_time, "field 'txtEndTime'", TextView.class);
    target.txtDuration = Utils.findRequiredViewAsType(source, R.id.txt_duration, "field 'txtDuration'", TextView.class);
    target.isAttendance = Utils.findRequiredViewAsType(source, R.id.isAttendance, "field 'isAttendance'", CheckBox.class);
    target.txtRemove = Utils.findRequiredViewAsType(source, R.id.txt_remove, "field 'txtRemove'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MasterTableAdapter.ItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtId = null;
    target.txtStartTime = null;
    target.txtEndTime = null;
    target.txtDuration = null;
    target.isAttendance = null;
    target.txtRemove = null;
  }
}
