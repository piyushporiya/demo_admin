// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.active_route.selected_route;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SelectedRouteActivity_ViewBinding implements Unbinder {
  private SelectedRouteActivity target;

  private View view2131231336;

  private View view2131230879;

  private View view2131230851;

  @UiThread
  public SelectedRouteActivity_ViewBinding(SelectedRouteActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SelectedRouteActivity_ViewBinding(final SelectedRouteActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.menu_item, "field 'menuItem' and method 'onViewClicked'");
    target.menuItem = Utils.castView(view, R.id.menu_item, "field 'menuItem'", ImageButton.class);
    view2131231336 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtSpeed = Utils.findRequiredViewAsType(source, R.id.txt_speed, "field 'txtSpeed'", TextView.class);
    target.txtPassengerCountLabel = Utils.findRequiredViewAsType(source, R.id.txt_passenger_count_label, "field 'txtPassengerCountLabel'", TextView.class);
    target.txtPassengerCount = Utils.findRequiredViewAsType(source, R.id.txt_passenger_count, "field 'txtPassengerCount'", TextView.class);
    target.txtTotalPassengerLabel = Utils.findRequiredViewAsType(source, R.id.txt_total_passenger_label, "field 'txtTotalPassengerLabel'", TextView.class);
    target.txtTotalPassenger = Utils.findRequiredViewAsType(source, R.id.txt_total_passenger, "field 'txtTotalPassenger'", TextView.class);
    target.llTotalpassanger = Utils.findRequiredViewAsType(source, R.id.llTotalpassanger, "field 'llTotalpassanger'", LinearLayout.class);
    target.viewTotalpassanger = Utils.findRequiredView(source, R.id.viewTotalpassanger, "field 'viewTotalpassanger'");
    target.txtTotalStation = Utils.findRequiredViewAsType(source, R.id.txt_total_station, "field 'txtTotalStation'", TextView.class);
    target.txtRemainigStudentsLabel = Utils.findRequiredViewAsType(source, R.id.txt_remainig_students_label, "field 'txtRemainigStudentsLabel'", TextView.class);
    target.txtRemainigStudents = Utils.findRequiredViewAsType(source, R.id.txt_remainig_students, "field 'txtRemainigStudents'", TextView.class);
    target.twe = Utils.findRequiredViewAsType(source, R.id.twe, "field 'twe'", LinearLayout.class);
    target.one = Utils.findRequiredViewAsType(source, R.id.one, "field 'one'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.btn_view_recording, "method 'onViewClicked'");
    view2131230879 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_on_board_student_list, "method 'onViewClicked'");
    view2131230851 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SelectedRouteActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.menuItem = null;
    target.txtSpeed = null;
    target.txtPassengerCountLabel = null;
    target.txtPassengerCount = null;
    target.txtTotalPassengerLabel = null;
    target.txtTotalPassenger = null;
    target.llTotalpassanger = null;
    target.viewTotalpassanger = null;
    target.txtTotalStation = null;
    target.txtRemainigStudentsLabel = null;
    target.txtRemainigStudents = null;
    target.twe = null;
    target.one = null;

    view2131231336.setOnClickListener(null);
    view2131231336 = null;
    view2131230879.setOnClickListener(null);
    view2131230879 = null;
    view2131230851.setOnClickListener(null);
    view2131230851 = null;
  }
}
