// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.userinfo.student.attendance_history;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import com.facebook.drawee.view.SimpleDraweeView;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AttendanceHistoryActivity_ViewBinding implements Unbinder {
  private AttendanceHistoryActivity target;

  private View view2131231199;

  private View view2131231631;

  @UiThread
  public AttendanceHistoryActivity_ViewBinding(AttendanceHistoryActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AttendanceHistoryActivity_ViewBinding(final AttendanceHistoryActivity target,
      View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.img_back_arrow, "field 'imgBackArrow' and method 'onViewClicked'");
    target.imgBackArrow = Utils.castView(view, R.id.img_back_arrow, "field 'imgBackArrow'", ImageView.class);
    view2131231199 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.profile = Utils.findRequiredViewAsType(source, R.id.profile, "field 'profile'", SimpleDraweeView.class);
    target.txtToolbarTitle = Utils.findRequiredViewAsType(source, R.id.txt_toolbar_title, "field 'txtToolbarTitle'", TextView.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", SuperRecyclerView.class);
    view = Utils.findRequiredView(source, R.id.txt_date, "field 'txtDate' and method 'onViewClicked'");
    target.txtDate = Utils.castView(view, R.id.txt_date, "field 'txtDate'", TextView.class);
    view2131231631 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.parentViewAnimator = Utils.findRequiredViewAsType(source, R.id.parent_viewAnimator, "field 'parentViewAnimator'", ViewAnimator.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AttendanceHistoryActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.imgBackArrow = null;
    target.profile = null;
    target.txtToolbarTitle = null;
    target.recyclerView = null;
    target.txtDate = null;
    target.parentViewAnimator = null;

    view2131231199.setOnClickListener(null);
    view2131231199 = null;
    view2131231631.setOnClickListener(null);
    view2131231631 = null;
  }
}
