// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.route.manage;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ManageRouteActivity_ViewBinding implements Unbinder {
  private ManageRouteActivity target;

  @UiThread
  public ManageRouteActivity_ViewBinding(ManageRouteActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ManageRouteActivity_ViewBinding(ManageRouteActivity target, View source) {
    this.target = target;

    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.parentViewAnimator = Utils.findRequiredViewAsType(source, R.id.parent_viewAnimator, "field 'parentViewAnimator'", ViewAnimator.class);
    target.txtPassengerCount = Utils.findRequiredViewAsType(source, R.id.txt_passenger_count, "field 'txtPassengerCount'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ManageRouteActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.parentViewAnimator = null;
    target.txtPassengerCount = null;
  }
}
