// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.classevents;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.RadioButton;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.calendarview.CalendarCustomView;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import info.hoang8f.android.segmented.SegmentedGroup;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ClassEventsFragment_ViewBinding implements Unbinder {
  private ClassEventsFragment target;

  private View view2131231157;

  @UiThread
  public ClassEventsFragment_ViewBinding(final ClassEventsFragment target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.fav_add, "field 'favAdd' and method 'onViewClicked'");
    target.favAdd = Utils.castView(view, R.id.fav_add, "field 'favAdd'", FloatingActionButton.class);
    view2131231157 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
    target.rbToday = Utils.findRequiredViewAsType(source, R.id.rb_today, "field 'rbToday'", RadioButton.class);
    target.rbUpcoming = Utils.findRequiredViewAsType(source, R.id.rb_upcoming, "field 'rbUpcoming'", RadioButton.class);
    target.sgClassEvents = Utils.findRequiredViewAsType(source, R.id.sgClassEvents, "field 'sgClassEvents'", SegmentedGroup.class);
    target.recyclerViewToday = Utils.findRequiredViewAsType(source, R.id.recyclerViewToday, "field 'recyclerViewToday'", SuperRecyclerView.class);
    target.recyclerViewUpcoming = Utils.findRequiredViewAsType(source, R.id.recyclerViewUpcoming, "field 'recyclerViewUpcoming'", SuperRecyclerView.class);
    target.viewAnimator = Utils.findRequiredViewAsType(source, R.id.viewAnimator, "field 'viewAnimator'", ViewAnimator.class);
    target.calenderView = Utils.findRequiredViewAsType(source, R.id.calenderView, "field 'calenderView'", CalendarCustomView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ClassEventsFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.favAdd = null;
    target.rbToday = null;
    target.rbUpcoming = null;
    target.sgClassEvents = null;
    target.recyclerViewToday = null;
    target.recyclerViewUpcoming = null;
    target.viewAnimator = null;
    target.calenderView = null;

    view2131231157.setOnClickListener(null);
    view2131231157 = null;
  }
}
