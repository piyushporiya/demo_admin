// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.viewresponse.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import com.facebook.drawee.view.SimpleDraweeView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ViewMessageAdapter$ViewMessageViewHolder_ViewBinding implements Unbinder {
  private ViewMessageAdapter.ViewMessageViewHolder target;

  @UiThread
  public ViewMessageAdapter$ViewMessageViewHolder_ViewBinding(ViewMessageAdapter.ViewMessageViewHolder target,
      View source) {
    this.target = target;

    target.ivStudentImg = Utils.findRequiredViewAsType(source, R.id.iv_StudentImg, "field 'ivStudentImg'", SimpleDraweeView.class);
    target.txtTitle = Utils.findRequiredViewAsType(source, R.id.txt_title, "field 'txtTitle'", TextView.class);
    target.txtDate = Utils.findRequiredViewAsType(source, R.id.txt_date, "field 'txtDate'", TextView.class);
    target.txtGrade = Utils.findRequiredViewAsType(source, R.id.txt_grade, "field 'txtGrade'", TextView.class);
    target.txtMsg = Utils.findRequiredViewAsType(source, R.id.txt_msg, "field 'txtMsg'", TextView.class);
    target.txtViewMore = Utils.findRequiredViewAsType(source, R.id.txt_view_more, "field 'txtViewMore'", TextView.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    target.txtFileName = Utils.findRequiredViewAsType(source, R.id.txt_file_name, "field 'txtFileName'", TextView.class);
    target.counter = Utils.findRequiredViewAsType(source, R.id.counter, "field 'counter'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ViewMessageAdapter.ViewMessageViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivStudentImg = null;
    target.txtTitle = null;
    target.txtDate = null;
    target.txtGrade = null;
    target.txtMsg = null;
    target.txtViewMore = null;
    target.recyclerView = null;
    target.txtFileName = null;
    target.counter = null;
  }
}
