// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.userinfo.student.add;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import com.hbb20.CountryCodePicker;
import info.hoang8f.android.segmented.SegmentedGroup;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddStudentActivity_ViewBinding implements Unbinder {
  private AddStudentActivity target;

  private View view2131230910;

  private View view2131231039;

  private View view2131231036;

  private View view2131231286;

  private View view2131230841;

  private View view2131230850;

  private View view2131230900;

  private View view2131230840;

  private View view2131230839;

  private View view2131230890;

  private View view2131230866;

  private View view2131230865;

  private View view2131230906;

  private View view2131231285;

  private View view2131230844;

  private View view2131231287;

  private View view2131230849;

  private View view2131230858;

  private View view2131230857;

  private View view2131231043;

  private View view2131230908;

  private View view2131230861;

  private View view2131230860;

  private View view2131230909;

  private View view2131230856;

  private View view2131230855;

  private View view2131230904;

  private View view2131230846;

  private View view2131230845;

  private View view2131231677;

  private View view2131231671;

  private View view2131231637;

  private View view2131231675;

  private View view2131231669;

  @UiThread
  public AddStudentActivity_ViewBinding(AddStudentActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AddStudentActivity_ViewBinding(final AddStudentActivity target, View source) {
    this.target = target;

    View view;
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.txtProfile = Utils.findRequiredViewAsType(source, R.id.txtProfile, "field 'txtProfile'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_profile_info, "field 'cardProfileInfo' and method 'onViewClicked'");
    target.cardProfileInfo = Utils.castView(view, R.id.card_profile_info, "field 'cardProfileInfo'", CardView.class);
    view2131230910 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.etFirstName = Utils.findRequiredViewAsType(source, R.id.et_first_name, "field 'etFirstName'", EditText.class);
    target.etLastName = Utils.findRequiredViewAsType(source, R.id.et_last_name, "field 'etLastName'", EditText.class);
    view = Utils.findRequiredView(source, R.id.et_dob, "field 'etDob' and method 'onViewClicked'");
    target.etDob = Utils.castView(view, R.id.et_dob, "field 'etDob'", EditText.class);
    view2131231039 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rbMale = Utils.findRequiredViewAsType(source, R.id.rb_male, "field 'rbMale'", RadioButton.class);
    target.rbFemale = Utils.findRequiredViewAsType(source, R.id.rb_female, "field 'rbFemale'", RadioButton.class);
    target.sgGender = Utils.findRequiredViewAsType(source, R.id.sgGender, "field 'sgGender'", SegmentedGroup.class);
    view = Utils.findRequiredView(source, R.id.et_date_of_joining, "field 'etJoiningDate' and method 'onViewClicked'");
    target.etJoiningDate = Utils.castView(view, R.id.et_date_of_joining, "field 'etJoiningDate'", EditText.class);
    view2131231036 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.etAddress = Utils.findRequiredViewAsType(source, R.id.et_address, "field 'etAddress'", AutoCompleteTextView.class);
    target.imgAddress = Utils.findRequiredViewAsType(source, R.id.img_address, "field 'imgAddress'", ImageView.class);
    target.studentProfilePic = Utils.findRequiredViewAsType(source, R.id.teacher_profile_pic, "field 'studentProfilePic'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.linaer_capture_image, "field 'linaerCaptureImage' and method 'onViewClicked'");
    target.linaerCaptureImage = Utils.castView(view, R.id.linaer_capture_image, "field 'linaerCaptureImage'", LinearLayout.class);
    view2131231286 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_edit, "field 'btnEdit' and method 'onViewClicked'");
    target.btnEdit = Utils.castView(view, R.id.btn_edit, "field 'btnEdit'", Button.class);
    view2131230841 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_next, "field 'btnNext' and method 'onViewClicked'");
    target.btnNext = Utils.castView(view, R.id.btn_next, "field 'btnNext'", Button.class);
    view2131230850 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutPtofileInfo = Utils.findRequiredViewAsType(source, R.id.layout_ptofile_info, "field 'layoutPtofileInfo'", CardView.class);
    target.txtEmergecy = Utils.findRequiredViewAsType(source, R.id.txtEmergecy, "field 'txtEmergecy'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_emergency_contact, "field 'cardEmergencyContact' and method 'onViewClicked'");
    target.cardEmergencyContact = Utils.castView(view, R.id.card_emergency_contact, "field 'cardEmergencyContact'", CardView.class);
    view2131230900 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.etEcFirstName = Utils.findRequiredViewAsType(source, R.id.et_ec_first_name, "field 'etEcFirstName'", EditText.class);
    target.etEcLastName = Utils.findRequiredViewAsType(source, R.id.et_ec_last_name, "field 'etEcLastName'", EditText.class);
    target.etEcRelation = Utils.findRequiredViewAsType(source, R.id.et_ec_relation, "field 'etEcRelation'", EditText.class);
    target.etEcPhone = Utils.findRequiredViewAsType(source, R.id.et_ec_phone, "field 'etEcPhone'", EditText.class);
    target.etEcEmail = Utils.findRequiredViewAsType(source, R.id.et_ec_email, "field 'etEcEmail'", EditText.class);
    target.etEcAddress = Utils.findRequiredViewAsType(source, R.id.et_ec_address, "field 'etEcAddress'", AutoCompleteTextView.class);
    target.imgEcAddress = Utils.findRequiredViewAsType(source, R.id.img_ec_address, "field 'imgEcAddress'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.btn_ec_previous, "field 'btnEcPrevious' and method 'onViewClicked'");
    target.btnEcPrevious = Utils.castView(view, R.id.btn_ec_previous, "field 'btnEcPrevious'", Button.class);
    view2131230840 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_ec_next, "field 'btnEcNext' and method 'onViewClicked'");
    target.btnEcNext = Utils.castView(view, R.id.btn_ec_next, "field 'btnEcNext'", Button.class);
    view2131230839 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutEmergencyContact = Utils.findRequiredViewAsType(source, R.id.layout_emergency_contact, "field 'layoutEmergencyContact'", CardView.class);
    target.scrollView = Utils.findRequiredViewAsType(source, R.id.scrollView, "field 'scrollView'", NestedScrollView.class);
    target.txtAchivements = Utils.findRequiredViewAsType(source, R.id.txt_achivements, "field 'txtAchivements'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_achivements, "field 'cardAchivements' and method 'onViewClicked'");
    target.cardAchivements = Utils.castView(view, R.id.card_achivements, "field 'cardAchivements'", CardView.class);
    view2131230890 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_sa_previous, "field 'btnSaPrevious' and method 'onViewClicked'");
    target.btnSaPrevious = Utils.castView(view, R.id.btn_sa_previous, "field 'btnSaPrevious'", Button.class);
    view2131230866 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_sa_next, "field 'btnSaNext' and method 'onViewClicked'");
    target.btnSaNext = Utils.castView(view, R.id.btn_sa_next, "field 'btnSaNext'", Button.class);
    view2131230865 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutAchivements = Utils.findRequiredViewAsType(source, R.id.layout_achivements, "field 'layoutAchivements'", CardView.class);
    target.txtError = Utils.findRequiredViewAsType(source, R.id.txt_error, "field 'txtError'", TextView.class);
    target.viewAnimator = Utils.findRequiredViewAsType(source, R.id.viewAnimator, "field 'viewAnimator'", ViewAnimator.class);
    target.etStudentId = Utils.findRequiredViewAsType(source, R.id.et_studentId, "field 'etStudentId'", EditText.class);
    target.etBirthPalace = Utils.findRequiredViewAsType(source, R.id.et_birth_palace, "field 'etBirthPalace'", EditText.class);
    target.etNationality = Utils.findRequiredViewAsType(source, R.id.et_nationality, "field 'etNationality'", EditText.class);
    target.etMotherTounge = Utils.findRequiredViewAsType(source, R.id.et_mother_tounge, "field 'etMotherTounge'", EditText.class);
    target.etReligion = Utils.findRequiredViewAsType(source, R.id.et_religion, "field 'etReligion'", EditText.class);
    target.txtParentInfo = Utils.findRequiredViewAsType(source, R.id.txtParentInfo, "field 'txtParentInfo'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_parent_info, "field 'cardParentInfo' and method 'onViewClicked'");
    target.cardParentInfo = Utils.castView(view, R.id.card_parent_info, "field 'cardParentInfo'", CardView.class);
    view2131230906 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.etPiFatherFirstName = Utils.findRequiredViewAsType(source, R.id.et_pi_father_first_name, "field 'etPiFatherFirstName'", EditText.class);
    target.etPiFatherLastName = Utils.findRequiredViewAsType(source, R.id.et_pi_father_last_name, "field 'etPiFatherLastName'", EditText.class);
    target.etPiFatherOccuption = Utils.findRequiredViewAsType(source, R.id.et_pi_father_occuption, "field 'etPiFatherOccuption'", EditText.class);
    target.etPiFatherPhone = Utils.findRequiredViewAsType(source, R.id.et_pi_father_phone, "field 'etPiFatherPhone'", EditText.class);
    target.etPiFatherEmailAddress = Utils.findRequiredViewAsType(source, R.id.et_pi_father_email_address, "field 'etPiFatherEmailAddress'", EditText.class);
    target.etPiMotherFirstName = Utils.findRequiredViewAsType(source, R.id.et_pi_mother_first_name, "field 'etPiMotherFirstName'", EditText.class);
    target.etPiMotherLastName = Utils.findRequiredViewAsType(source, R.id.et_pi_mother_last_name, "field 'etPiMotherLastName'", EditText.class);
    target.etPiMotherOccuption = Utils.findRequiredViewAsType(source, R.id.et_pi_mother_occuption, "field 'etPiMotherOccuption'", EditText.class);
    target.etPiMotherPhone = Utils.findRequiredViewAsType(source, R.id.et_pi_mother_phone, "field 'etPiMotherPhone'", EditText.class);
    target.etPiMotherEmailAddress = Utils.findRequiredViewAsType(source, R.id.et_pi_mother_email_address, "field 'etPiMotherEmailAddress'", EditText.class);
    target.fatherProfilePic = Utils.findRequiredViewAsType(source, R.id.father_profile_pic, "field 'fatherProfilePic'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.linaer_capture_father_image, "field 'linaerCaptureFatherImage' and method 'onViewClicked'");
    target.linaerCaptureFatherImage = Utils.castView(view, R.id.linaer_capture_father_image, "field 'linaerCaptureFatherImage'", LinearLayout.class);
    view2131231285 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_father_pic_edit, "field 'btnFatherPicEdit' and method 'onViewClicked'");
    target.btnFatherPicEdit = Utils.castView(view, R.id.btn_father_pic_edit, "field 'btnFatherPicEdit'", Button.class);
    view2131230844 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.motherProfilePic = Utils.findRequiredViewAsType(source, R.id.mother_profile_pic, "field 'motherProfilePic'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.linaer_capture_mother_image, "field 'linaerCaptureMotherImage' and method 'onViewClicked'");
    target.linaerCaptureMotherImage = Utils.castView(view, R.id.linaer_capture_mother_image, "field 'linaerCaptureMotherImage'", LinearLayout.class);
    view2131231287 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_mother_pic_edit, "field 'btnMotherPicEdit' and method 'onViewClicked'");
    target.btnMotherPicEdit = Utils.castView(view, R.id.btn_mother_pic_edit, "field 'btnMotherPicEdit'", Button.class);
    view2131230849 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_pi_previous, "field 'btnPiPrevious' and method 'onViewClicked'");
    target.btnPiPrevious = Utils.castView(view, R.id.btn_pi_previous, "field 'btnPiPrevious'", Button.class);
    view2131230858 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_pi_next, "field 'btnPiNext' and method 'onViewClicked'");
    target.btnPiNext = Utils.castView(view, R.id.btn_pi_next, "field 'btnPiNext'", Button.class);
    view2131230857 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutParentInfo = Utils.findRequiredViewAsType(source, R.id.layout_parent_info, "field 'layoutParentInfo'", CardView.class);
    view = Utils.findRequiredView(source, R.id.et_ec_dob, "field 'etEcDob' and method 'onViewClicked'");
    target.etEcDob = Utils.castView(view, R.id.et_ec_dob, "field 'etEcDob'", EditText.class);
    view2131231043 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.etEcEducation = Utils.findRequiredViewAsType(source, R.id.et_ec_education, "field 'etEcEducation'", EditText.class);
    target.etEcOccupation = Utils.findRequiredViewAsType(source, R.id.et_ec_occupation, "field 'etEcOccupation'", EditText.class);
    target.txtPastSchool = Utils.findRequiredViewAsType(source, R.id.txtPastSchool, "field 'txtPastSchool'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_past_info_contact, "field 'cardPastInfoContact' and method 'onViewClicked'");
    target.cardPastInfoContact = Utils.castView(view, R.id.card_past_info_contact, "field 'cardPastInfoContact'", CardView.class);
    view2131230908 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.etPsiSchoolName = Utils.findRequiredViewAsType(source, R.id.et_psi_school_name, "field 'etPsiSchoolName'", EditText.class);
    target.etPsiPassingYear = Utils.findRequiredViewAsType(source, R.id.et_psi_passing_year, "field 'etPsiPassingYear'", EditText.class);
    target.etPsiGrade = Utils.findRequiredViewAsType(source, R.id.et_psi_grade, "field 'etPsiGrade'", EditText.class);
    target.etPsiClass = Utils.findRequiredViewAsType(source, R.id.et_psi_class, "field 'etPsiClass'", EditText.class);
    target.etPsiAddress = Utils.findRequiredViewAsType(source, R.id.et_psi_address, "field 'etPsiAddress'", AutoCompleteTextView.class);
    target.imgPsiAddress = Utils.findRequiredViewAsType(source, R.id.img_psi_address, "field 'imgPsiAddress'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.btn_psi_previous, "field 'btnPsiPrevious' and method 'onViewClicked'");
    target.btnPsiPrevious = Utils.castView(view, R.id.btn_psi_previous, "field 'btnPsiPrevious'", Button.class);
    view2131230861 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_psi_next, "field 'btnPsiNext' and method 'onViewClicked'");
    target.btnPsiNext = Utils.castView(view, R.id.btn_psi_next, "field 'btnPsiNext'", Button.class);
    view2131230860 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutPastInfoContact = Utils.findRequiredViewAsType(source, R.id.layout_past_info_contact, "field 'layoutPastInfoContact'", CardView.class);
    target.txtPhysician = Utils.findRequiredViewAsType(source, R.id.txtPhysician, "field 'txtPhysician'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_physician_contact, "field 'cardPhysicianContact' and method 'onViewClicked'");
    target.cardPhysicianContact = Utils.castView(view, R.id.card_physician_contact, "field 'cardPhysicianContact'", CardView.class);
    view2131230909 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.etPciFirstName = Utils.findRequiredViewAsType(source, R.id.et_pci_first_name, "field 'etPciFirstName'", EditText.class);
    target.etPciLastName = Utils.findRequiredViewAsType(source, R.id.et_pci_last_name, "field 'etPciLastName'", EditText.class);
    target.etPciPhoneNumber = Utils.findRequiredViewAsType(source, R.id.et_pci_phone_number, "field 'etPciPhoneNumber'", EditText.class);
    target.etPciFaxNumber = Utils.findRequiredViewAsType(source, R.id.et_pci_fax_number, "field 'etPciFaxNumber'", EditText.class);
    view = Utils.findRequiredView(source, R.id.btn_pci_previous, "field 'btnPciPrevious' and method 'onViewClicked'");
    target.btnPciPrevious = Utils.castView(view, R.id.btn_pci_previous, "field 'btnPciPrevious'", Button.class);
    view2131230856 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_pci_next, "field 'btnPciNext' and method 'onViewClicked'");
    target.btnPciNext = Utils.castView(view, R.id.btn_pci_next, "field 'btnPciNext'", Button.class);
    view2131230855 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutPhysicianContact = Utils.findRequiredViewAsType(source, R.id.layout_physician_contact, "field 'layoutPhysicianContact'", CardView.class);
    target.imgPciAddress = Utils.findRequiredViewAsType(source, R.id.img_pci_address, "field 'imgPciAddress'", ImageView.class);
    target.chkTransportation = Utils.findRequiredViewAsType(source, R.id.chk_transportation, "field 'chkTransportation'", CheckBox.class);
    target.rbAtHome = Utils.findRequiredViewAsType(source, R.id.rb_at_home, "field 'rbAtHome'", RadioButton.class);
    target.rbCommunityPoint = Utils.findRequiredViewAsType(source, R.id.rb_community_point, "field 'rbCommunityPoint'", RadioButton.class);
    target.sgTransportation = Utils.findRequiredViewAsType(source, R.id.sgTransportation, "field 'sgTransportation'", SegmentedGroup.class);
    target.textFeedback = Utils.findRequiredViewAsType(source, R.id.textFeedback, "field 'textFeedback'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_feedback, "field 'cardFeedback' and method 'onViewClicked'");
    target.cardFeedback = Utils.castView(view, R.id.card_feedback, "field 'cardFeedback'", CardView.class);
    view2131230904 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.etFbTrasportationComment = Utils.findRequiredViewAsType(source, R.id.et_fb_trasportation_comment, "field 'etFbTrasportationComment'", EditText.class);
    target.etFbSchoolComment = Utils.findRequiredViewAsType(source, R.id.et_fb_school_comment, "field 'etFbSchoolComment'", EditText.class);
    target.layoutCardFeedback = Utils.findRequiredViewAsType(source, R.id.layout_card_feedback, "field 'layoutCardFeedback'", CardView.class);
    view = Utils.findRequiredView(source, R.id.btn_fb_previous, "field 'btnFbPrevious' and method 'onViewClicked'");
    target.btnFbPrevious = Utils.castView(view, R.id.btn_fb_previous, "field 'btnFbPrevious'", Button.class);
    view2131230846 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_fb_next, "field 'btnFbNext' and method 'onViewClicked'");
    target.btnFbNext = Utils.castView(view, R.id.btn_fb_next, "field 'btnFbNext'", Button.class);
    view2131230845 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.etPciAddress = Utils.findRequiredViewAsType(source, R.id.et_pci_address, "field 'etPciAddress'", AutoCompleteTextView.class);
    view = Utils.findRequiredView(source, R.id.txt_profile_attachment, "field 'txtProfileAttachment' and method 'onViewClicked'");
    target.txtProfileAttachment = Utils.castView(view, R.id.txt_profile_attachment, "field 'txtProfileAttachment'", TextView.class);
    view2131231677 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rvProfileAttachment = Utils.findRequiredViewAsType(source, R.id.rv_profile_attachment, "field 'rvProfileAttachment'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.txt_parent_attachment, "field 'txtParentAttachment' and method 'onViewClicked'");
    target.txtParentAttachment = Utils.castView(view, R.id.txt_parent_attachment, "field 'txtParentAttachment'", TextView.class);
    view2131231671 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rvParentAttachment = Utils.findRequiredViewAsType(source, R.id.rv_parent_attachment, "field 'rvParentAttachment'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.txt_ec_attachment, "field 'txtEcAttachment' and method 'onViewClicked'");
    target.txtEcAttachment = Utils.castView(view, R.id.txt_ec_attachment, "field 'txtEcAttachment'", TextView.class);
    view2131231637 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rvEcAttachment = Utils.findRequiredViewAsType(source, R.id.rv_ec_attachment, "field 'rvEcAttachment'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.txt_past_school_attachment, "field 'txtPastSchoolAttachment' and method 'onViewClicked'");
    target.txtPastSchoolAttachment = Utils.castView(view, R.id.txt_past_school_attachment, "field 'txtPastSchoolAttachment'", TextView.class);
    view2131231675 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rvPastSchoolAttachment = Utils.findRequiredViewAsType(source, R.id.rv_past_school_attachment, "field 'rvPastSchoolAttachment'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.txt_other_attachments, "field 'txtOtherAttachments' and method 'onViewClicked'");
    target.txtOtherAttachments = Utils.castView(view, R.id.txt_other_attachments, "field 'txtOtherAttachments'", TextView.class);
    view2131231669 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rvOtherAttachment = Utils.findRequiredViewAsType(source, R.id.rv_other_attachment, "field 'rvOtherAttachment'", RecyclerView.class);
    target.imgQrcode = Utils.findRequiredViewAsType(source, R.id.img_qrcode, "field 'imgQrcode'", ImageView.class);
    target.txtQrNotAvail = Utils.findRequiredViewAsType(source, R.id.txt_qr_not_avail, "field 'txtQrNotAvail'", TextView.class);
    target.qrFrame = Utils.findRequiredViewAsType(source, R.id.qr_frame, "field 'qrFrame'", FrameLayout.class);
    target.countryCodePiFather = Utils.findRequiredViewAsType(source, R.id.country_code_pi_father, "field 'countryCodePiFather'", CountryCodePicker.class);
    target.countryCodePiMother = Utils.findRequiredViewAsType(source, R.id.country_code_pi_mother, "field 'countryCodePiMother'", CountryCodePicker.class);
    target.chkSendInvitation = Utils.findRequiredViewAsType(source, R.id.chk_send_invitation, "field 'chkSendInvitation'", CheckBox.class);
    target.countryCodeEc = Utils.findRequiredViewAsType(source, R.id.country_code_ec, "field 'countryCodeEc'", CountryCodePicker.class);
    target.countryCodePci = Utils.findRequiredViewAsType(source, R.id.country_code_pci, "field 'countryCodePci'", CountryCodePicker.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AddStudentActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.txtProfile = null;
    target.cardProfileInfo = null;
    target.etFirstName = null;
    target.etLastName = null;
    target.etDob = null;
    target.rbMale = null;
    target.rbFemale = null;
    target.sgGender = null;
    target.etJoiningDate = null;
    target.etAddress = null;
    target.imgAddress = null;
    target.studentProfilePic = null;
    target.linaerCaptureImage = null;
    target.btnEdit = null;
    target.btnNext = null;
    target.layoutPtofileInfo = null;
    target.txtEmergecy = null;
    target.cardEmergencyContact = null;
    target.etEcFirstName = null;
    target.etEcLastName = null;
    target.etEcRelation = null;
    target.etEcPhone = null;
    target.etEcEmail = null;
    target.etEcAddress = null;
    target.imgEcAddress = null;
    target.btnEcPrevious = null;
    target.btnEcNext = null;
    target.layoutEmergencyContact = null;
    target.scrollView = null;
    target.txtAchivements = null;
    target.cardAchivements = null;
    target.btnSaPrevious = null;
    target.btnSaNext = null;
    target.layoutAchivements = null;
    target.txtError = null;
    target.viewAnimator = null;
    target.etStudentId = null;
    target.etBirthPalace = null;
    target.etNationality = null;
    target.etMotherTounge = null;
    target.etReligion = null;
    target.txtParentInfo = null;
    target.cardParentInfo = null;
    target.etPiFatherFirstName = null;
    target.etPiFatherLastName = null;
    target.etPiFatherOccuption = null;
    target.etPiFatherPhone = null;
    target.etPiFatherEmailAddress = null;
    target.etPiMotherFirstName = null;
    target.etPiMotherLastName = null;
    target.etPiMotherOccuption = null;
    target.etPiMotherPhone = null;
    target.etPiMotherEmailAddress = null;
    target.fatherProfilePic = null;
    target.linaerCaptureFatherImage = null;
    target.btnFatherPicEdit = null;
    target.motherProfilePic = null;
    target.linaerCaptureMotherImage = null;
    target.btnMotherPicEdit = null;
    target.btnPiPrevious = null;
    target.btnPiNext = null;
    target.layoutParentInfo = null;
    target.etEcDob = null;
    target.etEcEducation = null;
    target.etEcOccupation = null;
    target.txtPastSchool = null;
    target.cardPastInfoContact = null;
    target.etPsiSchoolName = null;
    target.etPsiPassingYear = null;
    target.etPsiGrade = null;
    target.etPsiClass = null;
    target.etPsiAddress = null;
    target.imgPsiAddress = null;
    target.btnPsiPrevious = null;
    target.btnPsiNext = null;
    target.layoutPastInfoContact = null;
    target.txtPhysician = null;
    target.cardPhysicianContact = null;
    target.etPciFirstName = null;
    target.etPciLastName = null;
    target.etPciPhoneNumber = null;
    target.etPciFaxNumber = null;
    target.btnPciPrevious = null;
    target.btnPciNext = null;
    target.layoutPhysicianContact = null;
    target.imgPciAddress = null;
    target.chkTransportation = null;
    target.rbAtHome = null;
    target.rbCommunityPoint = null;
    target.sgTransportation = null;
    target.textFeedback = null;
    target.cardFeedback = null;
    target.etFbTrasportationComment = null;
    target.etFbSchoolComment = null;
    target.layoutCardFeedback = null;
    target.btnFbPrevious = null;
    target.btnFbNext = null;
    target.etPciAddress = null;
    target.txtProfileAttachment = null;
    target.rvProfileAttachment = null;
    target.txtParentAttachment = null;
    target.rvParentAttachment = null;
    target.txtEcAttachment = null;
    target.rvEcAttachment = null;
    target.txtPastSchoolAttachment = null;
    target.rvPastSchoolAttachment = null;
    target.txtOtherAttachments = null;
    target.rvOtherAttachment = null;
    target.imgQrcode = null;
    target.txtQrNotAvail = null;
    target.qrFrame = null;
    target.countryCodePiFather = null;
    target.countryCodePiMother = null;
    target.chkSendInvitation = null;
    target.countryCodeEc = null;
    target.countryCodePci = null;

    view2131230910.setOnClickListener(null);
    view2131230910 = null;
    view2131231039.setOnClickListener(null);
    view2131231039 = null;
    view2131231036.setOnClickListener(null);
    view2131231036 = null;
    view2131231286.setOnClickListener(null);
    view2131231286 = null;
    view2131230841.setOnClickListener(null);
    view2131230841 = null;
    view2131230850.setOnClickListener(null);
    view2131230850 = null;
    view2131230900.setOnClickListener(null);
    view2131230900 = null;
    view2131230840.setOnClickListener(null);
    view2131230840 = null;
    view2131230839.setOnClickListener(null);
    view2131230839 = null;
    view2131230890.setOnClickListener(null);
    view2131230890 = null;
    view2131230866.setOnClickListener(null);
    view2131230866 = null;
    view2131230865.setOnClickListener(null);
    view2131230865 = null;
    view2131230906.setOnClickListener(null);
    view2131230906 = null;
    view2131231285.setOnClickListener(null);
    view2131231285 = null;
    view2131230844.setOnClickListener(null);
    view2131230844 = null;
    view2131231287.setOnClickListener(null);
    view2131231287 = null;
    view2131230849.setOnClickListener(null);
    view2131230849 = null;
    view2131230858.setOnClickListener(null);
    view2131230858 = null;
    view2131230857.setOnClickListener(null);
    view2131230857 = null;
    view2131231043.setOnClickListener(null);
    view2131231043 = null;
    view2131230908.setOnClickListener(null);
    view2131230908 = null;
    view2131230861.setOnClickListener(null);
    view2131230861 = null;
    view2131230860.setOnClickListener(null);
    view2131230860 = null;
    view2131230909.setOnClickListener(null);
    view2131230909 = null;
    view2131230856.setOnClickListener(null);
    view2131230856 = null;
    view2131230855.setOnClickListener(null);
    view2131230855 = null;
    view2131230904.setOnClickListener(null);
    view2131230904 = null;
    view2131230846.setOnClickListener(null);
    view2131230846 = null;
    view2131230845.setOnClickListener(null);
    view2131230845 = null;
    view2131231677.setOnClickListener(null);
    view2131231677 = null;
    view2131231671.setOnClickListener(null);
    view2131231671 = null;
    view2131231637.setOnClickListener(null);
    view2131231637 = null;
    view2131231675.setOnClickListener(null);
    view2131231675 = null;
    view2131231669.setOnClickListener(null);
    view2131231669 = null;
  }
}
