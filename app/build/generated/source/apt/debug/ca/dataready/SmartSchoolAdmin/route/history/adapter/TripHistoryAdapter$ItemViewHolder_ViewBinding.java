// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.route.history.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import com.mikhaellopez.circularimageview.CircularImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class TripHistoryAdapter$ItemViewHolder_ViewBinding implements Unbinder {
  private TripHistoryAdapter.ItemViewHolder target;

  @UiThread
  public TripHistoryAdapter$ItemViewHolder_ViewBinding(TripHistoryAdapter.ItemViewHolder target,
      View source) {
    this.target = target;

    target.ivDriverImage = Utils.findRequiredViewAsType(source, R.id.iv_driver_image, "field 'ivDriverImage'", CircularImageView.class);
    target.txtName = Utils.findRequiredViewAsType(source, R.id.txt_name, "field 'txtName'", TextView.class);
    target.txtInTime = Utils.findRequiredViewAsType(source, R.id.txt_in_time, "field 'txtInTime'", TextView.class);
    target.txtOutTime = Utils.findRequiredViewAsType(source, R.id.txt_out_time, "field 'txtOutTime'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    TripHistoryAdapter.ItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivDriverImage = null;
    target.txtName = null;
    target.txtInTime = null;
    target.txtOutTime = null;
  }
}
