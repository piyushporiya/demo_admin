// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.chat;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ChatActivity_ViewBinding implements Unbinder {
  private ChatActivity target;

  private View view2131231485;

  private View view2131231417;

  private View view2131231484;

  private View view2131230887;

  private View view2131230864;

  @UiThread
  public ChatActivity_ViewBinding(ChatActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ChatActivity_ViewBinding(final ChatActivity target, View source) {
    this.target = target;

    View view;
    target.recyclerview = Utils.findRequiredViewAsType(source, R.id.recyclerview, "field 'recyclerview'", RecyclerView.class);
    target.txtNoData = Utils.findRequiredViewAsType(source, R.id.txt_no_data, "field 'txtNoData'", TextView.class);
    target.parentViewAnimator = Utils.findRequiredViewAsType(source, R.id.parent_viewAnimator, "field 'parentViewAnimator'", ViewAnimator.class);
    target.editText = Utils.findRequiredViewAsType(source, R.id.editText, "field 'editText'", EditText.class);
    view = Utils.findRequiredView(source, R.id.send_btn, "field 'sendBtn' and method 'onViewClicked'");
    target.sendBtn = Utils.castView(view, R.id.send_btn, "field 'sendBtn'", ImageButton.class);
    view2131231485 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.progressBar = Utils.findRequiredViewAsType(source, R.id.progressBar, "field 'progressBar'", ProgressBar.class);
    view = Utils.findRequiredView(source, R.id.retry_btn, "field 'retryBtn' and method 'onViewClicked'");
    target.retryBtn = Utils.castView(view, R.id.retry_btn, "field 'retryBtn'", ImageButton.class);
    view2131231417 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.childviewAnimator = Utils.findRequiredViewAsType(source, R.id.childviewAnimator, "field 'childviewAnimator'", ViewAnimator.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.bottom = Utils.findRequiredViewAsType(source, R.id.bottom, "field 'bottom'", RelativeLayout.class);
    target.swipeRefreshLayout = Utils.findRequiredViewAsType(source, R.id.swipeRefreshLayout, "field 'swipeRefreshLayout'", SwipeRefreshLayout.class);
    view = Utils.findRequiredView(source, R.id.send_attach, "field 'sendAttach' and method 'onViewClicked'");
    target.sendAttach = Utils.castView(view, R.id.send_attach, "field 'sendAttach'", ImageButton.class);
    view2131231484 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.camera_attach, "field 'cameraAttach' and method 'onViewClicked'");
    target.cameraAttach = Utils.castView(view, R.id.camera_attach, "field 'cameraAttach'", ImageButton.class);
    view2131230887 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_refresh_chat, "method 'onViewClicked'");
    view2131230864 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ChatActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.recyclerview = null;
    target.txtNoData = null;
    target.parentViewAnimator = null;
    target.editText = null;
    target.sendBtn = null;
    target.progressBar = null;
    target.retryBtn = null;
    target.childviewAnimator = null;
    target.toolbar = null;
    target.bottom = null;
    target.swipeRefreshLayout = null;
    target.sendAttach = null;
    target.cameraAttach = null;

    view2131231485.setOnClickListener(null);
    view2131231485 = null;
    view2131231417.setOnClickListener(null);
    view2131231417 = null;
    view2131231484.setOnClickListener(null);
    view2131231484 = null;
    view2131230887.setOnClickListener(null);
    view2131230887 = null;
    view2131230864.setOnClickListener(null);
    view2131230864 = null;
  }
}
