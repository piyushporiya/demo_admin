// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.classchannel;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ClassChannelFragment_ViewBinding implements Unbinder {
  private ClassChannelFragment target;

  private View view2131231291;

  private View view2131231292;

  private View view2131231154;

  @UiThread
  public ClassChannelFragment_ViewBinding(final ClassChannelFragment target, View source) {
    this.target = target;

    View view;
    target.txtGrade = Utils.findRequiredViewAsType(source, R.id.txt_Grade, "field 'txtGrade'", TextView.class);
    target.gradeImg = Utils.findRequiredViewAsType(source, R.id.grade_img, "field 'gradeImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.linear_Grade, "field 'linearGrade' and method 'onViewClicked'");
    target.linearGrade = Utils.castView(view, R.id.linear_Grade, "field 'linearGrade'", RelativeLayout.class);
    view2131231291 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.txtClass = Utils.findRequiredViewAsType(source, R.id.txt_Class, "field 'txtClass'", TextView.class);
    target.classImg = Utils.findRequiredViewAsType(source, R.id.class_img, "field 'classImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.linear_Section, "field 'linearSection' and method 'onViewClicked'");
    target.linearSection = Utils.castView(view, R.id.linear_Section, "field 'linearSection'", RelativeLayout.class);
    view2131231292 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.llheader = Utils.findRequiredViewAsType(source, R.id.llheader, "field 'llheader'", LinearLayout.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", SuperRecyclerView.class);
    target.parentViewAnimator = Utils.findRequiredViewAsType(source, R.id.parent_viewAnimator, "field 'parentViewAnimator'", ViewAnimator.class);
    view = Utils.findRequiredView(source, R.id.fab_create, "field 'fabCreate' and method 'onViewClicked'");
    target.fabCreate = Utils.castView(view, R.id.fab_create, "field 'fabCreate'", FloatingActionButton.class);
    view2131231154 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ClassChannelFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtGrade = null;
    target.gradeImg = null;
    target.linearGrade = null;
    target.txtClass = null;
    target.classImg = null;
    target.linearSection = null;
    target.llheader = null;
    target.recyclerView = null;
    target.parentViewAnimator = null;
    target.fabCreate = null;

    view2131231291.setOnClickListener(null);
    view2131231291 = null;
    view2131231292.setOnClickListener(null);
    view2131231292 = null;
    view2131231154.setOnClickListener(null);
    view2131231154 = null;
  }
}
