// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.expired;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ExpiredActivity_ViewBinding implements Unbinder {
  private ExpiredActivity target;

  private View view2131230999;

  private View view2131231738;

  @UiThread
  public ExpiredActivity_ViewBinding(ExpiredActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ExpiredActivity_ViewBinding(final ExpiredActivity target, View source) {
    this.target = target;

    View view;
    target.viewAnimator = Utils.findRequiredViewAsType(source, R.id.viewAnimator, "field 'viewAnimator'", ViewAnimator.class);
    view = Utils.findRequiredView(source, R.id.dismiss, "method 'onClick'");
    view2131230999 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.update, "method 'onClick'");
    view2131231738 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ExpiredActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.viewAnimator = null;

    view2131230999.setOnClickListener(null);
    view2131230999 = null;
    view2131231738.setOnClickListener(null);
    view2131231738 = null;
  }
}
