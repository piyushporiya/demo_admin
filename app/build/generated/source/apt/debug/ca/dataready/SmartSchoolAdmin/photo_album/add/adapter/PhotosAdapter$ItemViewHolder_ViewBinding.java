// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.photo_album.add.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PhotosAdapter$ItemViewHolder_ViewBinding implements Unbinder {
  private PhotosAdapter.ItemViewHolder target;

  @UiThread
  public PhotosAdapter$ItemViewHolder_ViewBinding(PhotosAdapter.ItemViewHolder target,
      View source) {
    this.target = target;

    target.img = Utils.findRequiredViewAsType(source, R.id.img, "field 'img'", ImageView.class);
    target.imgCancel = Utils.findRequiredViewAsType(source, R.id.img_cancel, "field 'imgCancel'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    PhotosAdapter.ItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.img = null;
    target.imgCancel = null;
  }
}
