// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.userinfo.staff.add;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.ViewAnimator;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import com.hbb20.CountryCodePicker;
import info.hoang8f.android.segmented.SegmentedGroup;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddStaffActivity_ViewBinding implements Unbinder {
  private AddStaffActivity target;

  private View view2131230910;

  private View view2131231039;

  private View view2131231076;

  private View view2131231286;

  private View view2131230841;

  private View view2131230850;

  private View view2131230900;

  private View view2131230840;

  private View view2131230839;

  private View view2131230803;

  private View view2131230911;

  private View view2131230863;

  private View view2131230862;

  private View view2131230802;

  private View view2131230903;

  private View view2131230843;

  private View view2131230842;

  private View view2131230890;

  private View view2131230866;

  private View view2131230865;

  private View view2131230909;

  private View view2131230856;

  private View view2131230855;

  private View view2131230904;

  private View view2131230846;

  private View view2131230845;

  private View view2131231677;

  private View view2131231681;

  private View view2131231649;

  private View view2131231637;

  private View view2131231669;

  @UiThread
  public AddStaffActivity_ViewBinding(AddStaffActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AddStaffActivity_ViewBinding(final AddStaffActivity target, View source) {
    this.target = target;

    View view;
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.txtProfile = Utils.findRequiredViewAsType(source, R.id.txtProfile, "field 'txtProfile'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_profile_info, "field 'cardProfileInfo' and method 'onViewClicked'");
    target.cardProfileInfo = Utils.castView(view, R.id.card_profile_info, "field 'cardProfileInfo'", CardView.class);
    view2131230910 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.etFirstName = Utils.findRequiredViewAsType(source, R.id.et_first_name, "field 'etFirstName'", EditText.class);
    target.etLastName = Utils.findRequiredViewAsType(source, R.id.et_last_name, "field 'etLastName'", EditText.class);
    view = Utils.findRequiredView(source, R.id.et_dob, "field 'etDob' and method 'onViewClicked'");
    target.etDob = Utils.castView(view, R.id.et_dob, "field 'etDob'", EditText.class);
    view2131231039 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rbMale = Utils.findRequiredViewAsType(source, R.id.rb_male, "field 'rbMale'", RadioButton.class);
    target.rbFemale = Utils.findRequiredViewAsType(source, R.id.rb_female, "field 'rbFemale'", RadioButton.class);
    target.sgGender = Utils.findRequiredViewAsType(source, R.id.sgGender, "field 'sgGender'", SegmentedGroup.class);
    target.rbSingle = Utils.findRequiredViewAsType(source, R.id.rb_single, "field 'rbSingle'", RadioButton.class);
    target.rbMarried = Utils.findRequiredViewAsType(source, R.id.rb_married, "field 'rbMarried'", RadioButton.class);
    target.sgMartialStatus = Utils.findRequiredViewAsType(source, R.id.sgMartialStatus, "field 'sgMartialStatus'", SegmentedGroup.class);
    target.etPhone = Utils.findRequiredViewAsType(source, R.id.et_phone, "field 'etPhone'", EditText.class);
    target.etEmail = Utils.findRequiredViewAsType(source, R.id.et_Email, "field 'etEmail'", EditText.class);
    view = Utils.findRequiredView(source, R.id.et_joining_date, "field 'etJoiningDate' and method 'onViewClicked'");
    target.etJoiningDate = Utils.castView(view, R.id.et_joining_date, "field 'etJoiningDate'", EditText.class);
    view2131231076 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.etPosition = Utils.findRequiredViewAsType(source, R.id.et_position, "field 'etPosition'", EditText.class);
    target.etDepartment = Utils.findRequiredViewAsType(source, R.id.et_department, "field 'etDepartment'", EditText.class);
    target.etAddress = Utils.findRequiredViewAsType(source, R.id.et_address, "field 'etAddress'", AutoCompleteTextView.class);
    target.imgAddress = Utils.findRequiredViewAsType(source, R.id.img_address, "field 'imgAddress'", ImageView.class);
    target.staffProfilePic = Utils.findRequiredViewAsType(source, R.id.teacher_profile_pic, "field 'staffProfilePic'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.linaer_capture_image, "field 'linaerCaptureImage' and method 'onViewClicked'");
    target.linaerCaptureImage = Utils.castView(view, R.id.linaer_capture_image, "field 'linaerCaptureImage'", LinearLayout.class);
    view2131231286 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_edit, "field 'btnEdit' and method 'onViewClicked'");
    target.btnEdit = Utils.castView(view, R.id.btn_edit, "field 'btnEdit'", Button.class);
    view2131230841 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_next, "field 'btnNext' and method 'onViewClicked'");
    target.btnNext = Utils.castView(view, R.id.btn_next, "field 'btnNext'", Button.class);
    view2131230850 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutPtofileInfo = Utils.findRequiredViewAsType(source, R.id.layout_ptofile_info, "field 'layoutPtofileInfo'", CardView.class);
    target.txtEmergecy = Utils.findRequiredViewAsType(source, R.id.txtEmergecy, "field 'txtEmergecy'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_emergency_contact, "field 'cardEmergencyContact' and method 'onViewClicked'");
    target.cardEmergencyContact = Utils.castView(view, R.id.card_emergency_contact, "field 'cardEmergencyContact'", CardView.class);
    view2131230900 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.etEcFirstName = Utils.findRequiredViewAsType(source, R.id.et_ec_first_name, "field 'etEcFirstName'", EditText.class);
    target.etEcLastName = Utils.findRequiredViewAsType(source, R.id.et_ec_last_name, "field 'etEcLastName'", EditText.class);
    target.etEcRelation = Utils.findRequiredViewAsType(source, R.id.et_ec_relation, "field 'etEcRelation'", EditText.class);
    target.etEcPhone = Utils.findRequiredViewAsType(source, R.id.et_ec_phone, "field 'etEcPhone'", EditText.class);
    target.etEcEmail = Utils.findRequiredViewAsType(source, R.id.et_ec_email, "field 'etEcEmail'", EditText.class);
    target.etEcAddress = Utils.findRequiredViewAsType(source, R.id.et_ec_address, "field 'etEcAddress'", AutoCompleteTextView.class);
    target.imgEcAddress = Utils.findRequiredViewAsType(source, R.id.img_ec_address, "field 'imgEcAddress'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.btn_ec_previous, "field 'btnEcPrevious' and method 'onViewClicked'");
    target.btnEcPrevious = Utils.castView(view, R.id.btn_ec_previous, "field 'btnEcPrevious'", Button.class);
    view2131230840 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_ec_next, "field 'btnEcNext' and method 'onViewClicked'");
    target.btnEcNext = Utils.castView(view, R.id.btn_ec_next, "field 'btnEcNext'", Button.class);
    view2131230839 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutEmergencyContact = Utils.findRequiredViewAsType(source, R.id.layout_emergency_contact, "field 'layoutEmergencyContact'", CardView.class);
    target.scrollView = Utils.findRequiredViewAsType(source, R.id.scrollView, "field 'scrollView'", NestedScrollView.class);
    target.txtQualification = Utils.findRequiredViewAsType(source, R.id.txt_qualification, "field 'txtQualification'", TextView.class);
    view = Utils.findRequiredView(source, R.id.add_more_qualification, "field 'addMoreQualification' and method 'onViewClicked'");
    target.addMoreQualification = Utils.castView(view, R.id.add_more_qualification, "field 'addMoreQualification'", ImageView.class);
    view2131230803 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.card_qualification, "field 'cardQualification' and method 'onViewClicked'");
    target.cardQualification = Utils.castView(view, R.id.card_qualification, "field 'cardQualification'", CardView.class);
    view2131230911 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rvQualification = Utils.findRequiredViewAsType(source, R.id.rv_qualification, "field 'rvQualification'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.btn_ql_previous, "field 'btnQlPrevious' and method 'onViewClicked'");
    target.btnQlPrevious = Utils.castView(view, R.id.btn_ql_previous, "field 'btnQlPrevious'", Button.class);
    view2131230863 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_ql_next, "field 'btnQlNext' and method 'onViewClicked'");
    target.btnQlNext = Utils.castView(view, R.id.btn_ql_next, "field 'btnQlNext'", Button.class);
    view2131230862 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutQualification = Utils.findRequiredViewAsType(source, R.id.layout_qualification, "field 'layoutQualification'", CardView.class);
    target.txtExperience = Utils.findRequiredViewAsType(source, R.id.txt_experience, "field 'txtExperience'", TextView.class);
    view = Utils.findRequiredView(source, R.id.add_more_experience, "field 'addMoreExperience' and method 'onViewClicked'");
    target.addMoreExperience = Utils.castView(view, R.id.add_more_experience, "field 'addMoreExperience'", ImageView.class);
    view2131230802 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.card_experience, "field 'cardExperience' and method 'onViewClicked'");
    target.cardExperience = Utils.castView(view, R.id.card_experience, "field 'cardExperience'", CardView.class);
    view2131230903 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rvExperience = Utils.findRequiredViewAsType(source, R.id.rv_experience, "field 'rvExperience'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.btn_exp_previous, "field 'btnExpPrevious' and method 'onViewClicked'");
    target.btnExpPrevious = Utils.castView(view, R.id.btn_exp_previous, "field 'btnExpPrevious'", Button.class);
    view2131230843 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_exp_next, "field 'btnExpNext' and method 'onViewClicked'");
    target.btnExpNext = Utils.castView(view, R.id.btn_exp_next, "field 'btnExpNext'", Button.class);
    view2131230842 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutExperience = Utils.findRequiredViewAsType(source, R.id.layout_experience, "field 'layoutExperience'", CardView.class);
    target.txtAchivements = Utils.findRequiredViewAsType(source, R.id.txt_achivements, "field 'txtAchivements'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_achivements, "field 'cardAchivements' and method 'onViewClicked'");
    target.cardAchivements = Utils.castView(view, R.id.card_achivements, "field 'cardAchivements'", CardView.class);
    view2131230890 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_sa_previous, "field 'btnSaPrevious' and method 'onViewClicked'");
    target.btnSaPrevious = Utils.castView(view, R.id.btn_sa_previous, "field 'btnSaPrevious'", Button.class);
    view2131230866 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_sa_next, "field 'btnSaNext' and method 'onViewClicked'");
    target.btnSaNext = Utils.castView(view, R.id.btn_sa_next, "field 'btnSaNext'", Button.class);
    view2131230865 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutAchivements = Utils.findRequiredViewAsType(source, R.id.layout_achivements, "field 'layoutAchivements'", CardView.class);
    target.txtError = Utils.findRequiredViewAsType(source, R.id.txt_error, "field 'txtError'", TextView.class);
    target.viewAnimator = Utils.findRequiredViewAsType(source, R.id.viewAnimator, "field 'viewAnimator'", ViewAnimator.class);
    target.chkTransportation = Utils.findRequiredViewAsType(source, R.id.chk_transportation, "field 'chkTransportation'", CheckBox.class);
    target.rbAtHome = Utils.findRequiredViewAsType(source, R.id.rb_at_home, "field 'rbAtHome'", RadioButton.class);
    target.rbCommunityPoint = Utils.findRequiredViewAsType(source, R.id.rb_community_point, "field 'rbCommunityPoint'", RadioButton.class);
    target.sgTransportation = Utils.findRequiredViewAsType(source, R.id.sgTransportation, "field 'sgTransportation'", SegmentedGroup.class);
    target.txtPhysician = Utils.findRequiredViewAsType(source, R.id.txtPhysician, "field 'txtPhysician'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_physician_contact, "field 'cardPhysicianContact' and method 'onViewClicked'");
    target.cardPhysicianContact = Utils.castView(view, R.id.card_physician_contact, "field 'cardPhysicianContact'", CardView.class);
    view2131230909 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.etPciFirstName = Utils.findRequiredViewAsType(source, R.id.et_pci_first_name, "field 'etPciFirstName'", EditText.class);
    target.etPciLastName = Utils.findRequiredViewAsType(source, R.id.et_pci_last_name, "field 'etPciLastName'", EditText.class);
    target.etPciPhoneNumber = Utils.findRequiredViewAsType(source, R.id.et_pci_phone_number, "field 'etPciPhoneNumber'", EditText.class);
    target.etPciFaxNumber = Utils.findRequiredViewAsType(source, R.id.et_pci_fax_number, "field 'etPciFaxNumber'", EditText.class);
    target.etPciAddress = Utils.findRequiredViewAsType(source, R.id.et_pci_address, "field 'etPciAddress'", AutoCompleteTextView.class);
    target.imgPciAddress = Utils.findRequiredViewAsType(source, R.id.img_pci_address, "field 'imgPciAddress'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.btn_pci_previous, "field 'btnPciPrevious' and method 'onViewClicked'");
    target.btnPciPrevious = Utils.castView(view, R.id.btn_pci_previous, "field 'btnPciPrevious'", Button.class);
    view2131230856 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_pci_next, "field 'btnPciNext' and method 'onViewClicked'");
    target.btnPciNext = Utils.castView(view, R.id.btn_pci_next, "field 'btnPciNext'", Button.class);
    view2131230855 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutPhysicianContact = Utils.findRequiredViewAsType(source, R.id.layout_physician_contact, "field 'layoutPhysicianContact'", CardView.class);
    target.textFeedback = Utils.findRequiredViewAsType(source, R.id.textFeedback, "field 'textFeedback'", TextView.class);
    view = Utils.findRequiredView(source, R.id.card_feedback, "field 'cardFeedback' and method 'onViewClicked'");
    target.cardFeedback = Utils.castView(view, R.id.card_feedback, "field 'cardFeedback'", CardView.class);
    view2131230904 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.layoutCardFeedback = Utils.findRequiredViewAsType(source, R.id.layout_card_feedback, "field 'layoutCardFeedback'", CardView.class);
    target.etFbTrasportationComment = Utils.findRequiredViewAsType(source, R.id.et_fb_trasportation_comment, "field 'etFbTrasportationComment'", EditText.class);
    target.etFbSchoolComment = Utils.findRequiredViewAsType(source, R.id.et_fb_school_comment, "field 'etFbSchoolComment'", EditText.class);
    view = Utils.findRequiredView(source, R.id.btn_fb_previous, "field 'btnFbPrevious' and method 'onViewClicked'");
    target.btnFbPrevious = Utils.castView(view, R.id.btn_fb_previous, "field 'btnFbPrevious'", Button.class);
    view2131230846 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_fb_next, "field 'btnFbNext' and method 'onViewClicked'");
    target.btnFbNext = Utils.castView(view, R.id.btn_fb_next, "field 'btnFbNext'", Button.class);
    view2131230845 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.txt_profile_attachment, "field 'txtProfileAttachment' and method 'onViewClicked'");
    target.txtProfileAttachment = Utils.castView(view, R.id.txt_profile_attachment, "field 'txtProfileAttachment'", TextView.class);
    view2131231677 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rvProfileAttachment = Utils.findRequiredViewAsType(source, R.id.rv_profile_attachment, "field 'rvProfileAttachment'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.txt_qulification_attachment, "field 'txtQulificationAttachment' and method 'onViewClicked'");
    target.txtQulificationAttachment = Utils.castView(view, R.id.txt_qulification_attachment, "field 'txtQulificationAttachment'", TextView.class);
    view2131231681 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rvQualificationAttachment = Utils.findRequiredViewAsType(source, R.id.rv_qualification_attachment, "field 'rvQualificationAttachment'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.txt_exp_attachment, "field 'txtExpAttachment' and method 'onViewClicked'");
    target.txtExpAttachment = Utils.castView(view, R.id.txt_exp_attachment, "field 'txtExpAttachment'", TextView.class);
    view2131231649 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rvExpAttachment = Utils.findRequiredViewAsType(source, R.id.rv_exp_attachment, "field 'rvExpAttachment'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.txt_ec_attachment, "field 'txtEcAttachment' and method 'onViewClicked'");
    target.txtEcAttachment = Utils.castView(view, R.id.txt_ec_attachment, "field 'txtEcAttachment'", TextView.class);
    view2131231637 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rvEcAttachment = Utils.findRequiredViewAsType(source, R.id.rv_ec_attachment, "field 'rvEcAttachment'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.txt_other_attachments, "field 'txtOtherAttachments' and method 'onViewClicked'");
    target.txtOtherAttachments = Utils.castView(view, R.id.txt_other_attachments, "field 'txtOtherAttachments'", TextView.class);
    view2131231669 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rvOtherAttachment = Utils.findRequiredViewAsType(source, R.id.rv_other_attachment, "field 'rvOtherAttachment'", RecyclerView.class);
    target.imgQrcode = Utils.findRequiredViewAsType(source, R.id.img_qrcode, "field 'imgQrcode'", ImageView.class);
    target.txtQrNotAvail = Utils.findRequiredViewAsType(source, R.id.txt_qr_not_avail, "field 'txtQrNotAvail'", TextView.class);
    target.qrFrame = Utils.findRequiredViewAsType(source, R.id.qr_frame, "field 'qrFrame'", FrameLayout.class);
    target.countryCode = Utils.findRequiredViewAsType(source, R.id.country_code, "field 'countryCode'", CountryCodePicker.class);
    target.countryCodeEc = Utils.findRequiredViewAsType(source, R.id.country_code_ec, "field 'countryCodeEc'", CountryCodePicker.class);
    target.countryCodePci = Utils.findRequiredViewAsType(source, R.id.country_code_pci, "field 'countryCodePci'", CountryCodePicker.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AddStaffActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.txtProfile = null;
    target.cardProfileInfo = null;
    target.etFirstName = null;
    target.etLastName = null;
    target.etDob = null;
    target.rbMale = null;
    target.rbFemale = null;
    target.sgGender = null;
    target.rbSingle = null;
    target.rbMarried = null;
    target.sgMartialStatus = null;
    target.etPhone = null;
    target.etEmail = null;
    target.etJoiningDate = null;
    target.etPosition = null;
    target.etDepartment = null;
    target.etAddress = null;
    target.imgAddress = null;
    target.staffProfilePic = null;
    target.linaerCaptureImage = null;
    target.btnEdit = null;
    target.btnNext = null;
    target.layoutPtofileInfo = null;
    target.txtEmergecy = null;
    target.cardEmergencyContact = null;
    target.etEcFirstName = null;
    target.etEcLastName = null;
    target.etEcRelation = null;
    target.etEcPhone = null;
    target.etEcEmail = null;
    target.etEcAddress = null;
    target.imgEcAddress = null;
    target.btnEcPrevious = null;
    target.btnEcNext = null;
    target.layoutEmergencyContact = null;
    target.scrollView = null;
    target.txtQualification = null;
    target.addMoreQualification = null;
    target.cardQualification = null;
    target.rvQualification = null;
    target.btnQlPrevious = null;
    target.btnQlNext = null;
    target.layoutQualification = null;
    target.txtExperience = null;
    target.addMoreExperience = null;
    target.cardExperience = null;
    target.rvExperience = null;
    target.btnExpPrevious = null;
    target.btnExpNext = null;
    target.layoutExperience = null;
    target.txtAchivements = null;
    target.cardAchivements = null;
    target.btnSaPrevious = null;
    target.btnSaNext = null;
    target.layoutAchivements = null;
    target.txtError = null;
    target.viewAnimator = null;
    target.chkTransportation = null;
    target.rbAtHome = null;
    target.rbCommunityPoint = null;
    target.sgTransportation = null;
    target.txtPhysician = null;
    target.cardPhysicianContact = null;
    target.etPciFirstName = null;
    target.etPciLastName = null;
    target.etPciPhoneNumber = null;
    target.etPciFaxNumber = null;
    target.etPciAddress = null;
    target.imgPciAddress = null;
    target.btnPciPrevious = null;
    target.btnPciNext = null;
    target.layoutPhysicianContact = null;
    target.textFeedback = null;
    target.cardFeedback = null;
    target.layoutCardFeedback = null;
    target.etFbTrasportationComment = null;
    target.etFbSchoolComment = null;
    target.btnFbPrevious = null;
    target.btnFbNext = null;
    target.txtProfileAttachment = null;
    target.rvProfileAttachment = null;
    target.txtQulificationAttachment = null;
    target.rvQualificationAttachment = null;
    target.txtExpAttachment = null;
    target.rvExpAttachment = null;
    target.txtEcAttachment = null;
    target.rvEcAttachment = null;
    target.txtOtherAttachments = null;
    target.rvOtherAttachment = null;
    target.imgQrcode = null;
    target.txtQrNotAvail = null;
    target.qrFrame = null;
    target.countryCode = null;
    target.countryCodeEc = null;
    target.countryCodePci = null;

    view2131230910.setOnClickListener(null);
    view2131230910 = null;
    view2131231039.setOnClickListener(null);
    view2131231039 = null;
    view2131231076.setOnClickListener(null);
    view2131231076 = null;
    view2131231286.setOnClickListener(null);
    view2131231286 = null;
    view2131230841.setOnClickListener(null);
    view2131230841 = null;
    view2131230850.setOnClickListener(null);
    view2131230850 = null;
    view2131230900.setOnClickListener(null);
    view2131230900 = null;
    view2131230840.setOnClickListener(null);
    view2131230840 = null;
    view2131230839.setOnClickListener(null);
    view2131230839 = null;
    view2131230803.setOnClickListener(null);
    view2131230803 = null;
    view2131230911.setOnClickListener(null);
    view2131230911 = null;
    view2131230863.setOnClickListener(null);
    view2131230863 = null;
    view2131230862.setOnClickListener(null);
    view2131230862 = null;
    view2131230802.setOnClickListener(null);
    view2131230802 = null;
    view2131230903.setOnClickListener(null);
    view2131230903 = null;
    view2131230843.setOnClickListener(null);
    view2131230843 = null;
    view2131230842.setOnClickListener(null);
    view2131230842 = null;
    view2131230890.setOnClickListener(null);
    view2131230890 = null;
    view2131230866.setOnClickListener(null);
    view2131230866 = null;
    view2131230865.setOnClickListener(null);
    view2131230865 = null;
    view2131230909.setOnClickListener(null);
    view2131230909 = null;
    view2131230856.setOnClickListener(null);
    view2131230856 = null;
    view2131230855.setOnClickListener(null);
    view2131230855 = null;
    view2131230904.setOnClickListener(null);
    view2131230904 = null;
    view2131230846.setOnClickListener(null);
    view2131230846 = null;
    view2131230845.setOnClickListener(null);
    view2131230845 = null;
    view2131231677.setOnClickListener(null);
    view2131231677 = null;
    view2131231681.setOnClickListener(null);
    view2131231681 = null;
    view2131231649.setOnClickListener(null);
    view2131231649 = null;
    view2131231637.setOnClickListener(null);
    view2131231637 = null;
    view2131231669.setOnClickListener(null);
    view2131231669 = null;
  }
}
