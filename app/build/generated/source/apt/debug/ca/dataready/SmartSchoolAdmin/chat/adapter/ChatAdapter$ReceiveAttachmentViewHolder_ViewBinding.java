// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.chat.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ChatAdapter$ReceiveAttachmentViewHolder_ViewBinding implements Unbinder {
  private ChatAdapter.ReceiveAttachmentViewHolder target;

  @UiThread
  public ChatAdapter$ReceiveAttachmentViewHolder_ViewBinding(ChatAdapter.ReceiveAttachmentViewHolder target,
      View source) {
    this.target = target;

    target.recyclerview = Utils.findRequiredViewAsType(source, R.id.recyclerview, "field 'recyclerview'", RecyclerView.class);
    target.timestampTextView = Utils.findRequiredViewAsType(source, R.id.timestamp_text_view, "field 'timestampTextView'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ChatAdapter.ReceiveAttachmentViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.recyclerview = null;
    target.timestampTextView = null;
  }
}
