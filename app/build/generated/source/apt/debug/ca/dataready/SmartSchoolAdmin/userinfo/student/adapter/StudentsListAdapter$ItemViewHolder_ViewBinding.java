// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.userinfo.student.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import com.mikhaellopez.circularimageview.CircularImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class StudentsListAdapter$ItemViewHolder_ViewBinding implements Unbinder {
  private StudentsListAdapter.ItemViewHolder target;

  @UiThread
  public StudentsListAdapter$ItemViewHolder_ViewBinding(StudentsListAdapter.ItemViewHolder target,
      View source) {
    this.target = target;

    target.ivStudentImage = Utils.findRequiredViewAsType(source, R.id.iv_student_image, "field 'ivStudentImage'", CircularImageView.class);
    target.txtStudentName = Utils.findRequiredViewAsType(source, R.id.txt_student_name, "field 'txtStudentName'", TextView.class);
    target.imgMore = Utils.findRequiredViewAsType(source, R.id.img_more, "field 'imgMore'", ImageView.class);
    target.txtPending = Utils.findRequiredViewAsType(source, R.id.txt_pending, "field 'txtPending'", TextView.class);
    target.txtStudentId = Utils.findRequiredViewAsType(source, R.id.txt_student_id, "field 'txtStudentId'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    StudentsListAdapter.ItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivStudentImage = null;
    target.txtStudentName = null;
    target.imgMore = null;
    target.txtPending = null;
    target.txtStudentId = null;
  }
}
