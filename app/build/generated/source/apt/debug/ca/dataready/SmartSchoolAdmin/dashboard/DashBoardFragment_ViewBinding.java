// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.dashboard;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DashBoardFragment_ViewBinding implements Unbinder {
  private DashBoardFragment target;

  @UiThread
  public DashBoardFragment_ViewBinding(DashBoardFragment target, View source) {
    this.target = target;

    target.recycleview = Utils.findRequiredViewAsType(source, R.id.recycleview, "field 'recycleview'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DashBoardFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.recycleview = null;
  }
}
