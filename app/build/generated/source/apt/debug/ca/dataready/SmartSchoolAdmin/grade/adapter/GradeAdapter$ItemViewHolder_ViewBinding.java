// Generated code from Butter Knife. Do not modify!
package ca.dataready.SmartSchoolAdmin.grade.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import ca.dataready.SmartSchoolAdmin.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class GradeAdapter$ItemViewHolder_ViewBinding implements Unbinder {
  private GradeAdapter.ItemViewHolder target;

  @UiThread
  public GradeAdapter$ItemViewHolder_ViewBinding(GradeAdapter.ItemViewHolder target, View source) {
    this.target = target;

    target.txtGrade = Utils.findRequiredViewAsType(source, R.id.txt_grade, "field 'txtGrade'", TextView.class);
    target.cardGrade = Utils.findRequiredViewAsType(source, R.id.card_grade, "field 'cardGrade'", CardView.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    target.imgEdit = Utils.findRequiredViewAsType(source, R.id.img_edit, "field 'imgEdit'", ImageView.class);
    target.imgDelete = Utils.findRequiredViewAsType(source, R.id.img_delete, "field 'imgDelete'", ImageView.class);
    target.imgSubject = Utils.findRequiredViewAsType(source, R.id.img_subject, "field 'imgSubject'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    GradeAdapter.ItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtGrade = null;
    target.cardGrade = null;
    target.recyclerView = null;
    target.imgEdit = null;
    target.imgDelete = null;
    target.imgSubject = null;
  }
}
