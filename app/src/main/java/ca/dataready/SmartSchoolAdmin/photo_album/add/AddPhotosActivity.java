/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.photo_album.add;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.common.adapter.AttachmentAdapter;
import ca.dataready.SmartSchoolAdmin.photo_album.add.adapter.PhotosAdapter;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.MultipleImageUpload;
import ca.dataready.SmartSchoolAdmin.userinfo.teacher.add.AddTeacherActivity;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.FileModel;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.GradeList;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPhotosActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerView)
    SuperRecyclerView recyclerView;
    @BindView(R.id.txtUploadPhotos)
    TextView txtUploadPhotos;
    @BindView(R.id.card_upload_photos)
    CardView cardUploadPhotos;
    @BindView(R.id.txt_grade)
    TextView txtGrade;
    @BindView(R.id.grade_img)
    ImageView gradeImg;
    @BindView(R.id.linear_grade)
    RelativeLayout linearGrade;
    @BindView(R.id.txt_Class)
    TextView txtClass;
    @BindView(R.id.class_img)
    ImageView classImg;
    @BindView(R.id.linear_Class)
    RelativeLayout linearClass;
    @BindView(R.id.btn_select)
    Button btnSelect;
    @BindView(R.id.txt_edit)
    TextView txtEdit;
    @BindView(R.id.btn_upload)
    Button btnUpload;
    @BindView(R.id.layout_upload_photos)
    CardView layoutUploadPhotos;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.txt_error)
    TextView txtError;
    @BindView(R.id.viewAnimator)
    ViewAnimator viewAnimator;
    private String picturePath;
    private ArrayList<String> imagePathList = new ArrayList<>();
    PhotosAdapter adapter;
    private Call<GradeList> call;
    private ArrayList<GradeList.ResultBean.GradeBean> teacherBeans;
    private ListPopupWindow listPopupWindow;
    private List<String> gradeList = new ArrayList<>();
    private List<String> classList = new ArrayList<>();
    private String gradeId, classId;
    private Call<MultipleImageUpload> uploadPhotosCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_photos);
        ButterKnife.bind(this);
        setHeaderInfo();
        Init();
    }

    private void setHeaderInfo() {

        setSupportActionBar(toolbar);

        if (AdminApp.getInstance().getAdmin() != null)
            getSupportActionBar().setTitle(AdminApp.getInstance().getAdmin().getSchoolName() + " " + AdminApp.getInstance().getAdmin().getSchoolYear());
        else
            getSupportActionBar().setTitle(getString(R.string.add_photos));

        getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + getString(R.string.add_photos) + "</small>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void Init() {

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        adapter = new PhotosAdapter(AddPhotosActivity.this);
        recyclerView.setAdapter(adapter);
        TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
        emptyView.setText(getString(R.string.please_select_photo));

        ApiCall();
    }

    private void ApiCall() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();
        if (entity == null)
            return;

        call = AdminApp.getInstance().getApi().getClassSchedule(entity.getSchoolId(), entity.getSchoolYear());
        call.enqueue(new Callback<GradeList>() {
            @Override
            public void onResponse(Call<GradeList> call, Response<GradeList> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            teacherBeans = response.body().getResult().getGrade();
                        } else {
                            Utility.showSnackBar(viewAnimator, response.body().getMessage());
                        }
                    } else {

                        APIError error = APIError.parseError(response, AddPhotosActivity.this, App_Constants.API_TEACHER_CLASS_SCHEDULE);
                        Utility.error(AddPhotosActivity.this, error.message());

                    }
                } catch (Exception e) {
                    Utility.showSnackBar(viewAnimator, getString(R.string.somethingwrong));
                    e.printStackTrace();
                }

                viewAnimator.setDisplayedChild(1);

            }

            @Override
            public void onFailure(Call<GradeList> call, Throwable t) {

                if (!call.isCanceled()) {
                    System.out.println(t.getMessage());
                    Utility.showSnackBar(viewAnimator, App_Constants.NO_INTERNET);
                    viewAnimator.setDisplayedChild(1);
                }
            }
        });
    }

    private void show_drop_down_to_select_option(RelativeLayout layout, final String which) {

        if (teacherBeans != null && teacherBeans.size() > 0) {

            listPopupWindow = new ListPopupWindow(this);
            if (which.equals(App_Constants.GRADE)) {

                gradeList.clear();
                for (GradeList.ResultBean.GradeBean beans : teacherBeans) {
                    if (!gradeList.contains(beans.getGradeId()))
                        gradeList.add(beans.getGradeId());
                }

                listPopupWindow.setAdapter(new ArrayAdapter(this, R.layout.list_dropdown_item, gradeList));

            } else if (which.equals(App_Constants.CLASS)) {

                classList.clear();
                for (GradeList.ResultBean.GradeBean beans : teacherBeans) {

                    if (txtGrade.getText().toString().equals(beans.getGradeId())) {
                        if (!classList.contains(beans.getClassX()))
                            for (int i = 0; i < beans.getClassX().size(); i++)
                                classList.add(beans.getClassX().get(i).getClassId());
                    }
                }

                if (classList != null && classList.size() > 0) {
                    Collections.sort(classList, new Comparator<String>() {
                        public int compare(String obj1, String obj2) {
                            // ## Ascending order
                            return obj1.compareToIgnoreCase(obj2); // To compare string values
                        }
                    });
                }

                listPopupWindow.setAdapter(new ArrayAdapter(this, R.layout.list_dropdown_item, classList));
            }

            listPopupWindow.setAnchorView(layout);
            listPopupWindow.setWidth(ListPopupWindow.WRAP_CONTENT);
            listPopupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);

            listPopupWindow.setModal(true);
            listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    if (which.equals(App_Constants.GRADE)) {
                        txtGrade.setText(gradeList.get(i));
                        gradeId = gradeList.get(i);
                        txtClass.setText(getString(R.string.select_class_label));
                    } else if (which.equals(App_Constants.CLASS)) {
                        txtClass.setText(classList.get(i));
                        classId = classList.get(i);
                    }
                    listPopupWindow.dismiss();

                }
            });
            listPopupWindow.show();
        } else {
            Toast.makeText(this, getString(R.string.no_data), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @OnClick({R.id.btn_select, R.id.btn_upload, R.id.linear_grade, R.id.linear_Class})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_select:

                ImagePicker.create(this)
                        .folderMode(true) // folder mode (false by default)
                        .toolbarFolderTitle(getString(R.string.folders)) // folder selection title
                        .toolbarImageTitle(getString(R.string.select_images)) // image selection title
                        .toolbarArrowColor(Color.WHITE) // Toolbar 'up' arrow color
                        .multi() // multi mode (default mode)
                        .limit(10) // max images can be selected (99 by default)
                        .showCamera(false) // show camera or not (true by default)
                        .start(); // start image picker activity with request code

                break;
            case R.id.btn_upload:

                if (txtGrade.getText().toString().equalsIgnoreCase(getString(R.string.select_grade_label))) {
                    Toast.makeText(this, getString(R.string.please_select_grade), Toast.LENGTH_SHORT).show();
                } else if (txtClass.getText().toString().equalsIgnoreCase(getString(R.string.select_class_label))) {
                    Toast.makeText(this, getString(R.string.select_class_label), Toast.LENGTH_SHORT).show();
                } else if (imagePathList == null) {
                    Toast.makeText(this, getString(R.string.please_select_images), Toast.LENGTH_SHORT).show();
                } else if (imagePathList.size() == 0) {
                    Toast.makeText(this, getString(R.string.please_select_images), Toast.LENGTH_SHORT).show();
                } else {
                    UploadPhotos();
                }

                break;

            case R.id.linear_grade:
                show_drop_down_to_select_option(linearGrade, App_Constants.GRADE);
                break;
            case R.id.linear_Class:
                show_drop_down_to_select_option(linearClass, App_Constants.CLASS);
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // Get a list of picked images
            imagePathList.clear();
            List<Image> images = ImagePicker.getImages(data);
            if (images != null && images.size() > 0) {

                for (Image image : images) {
                    imagePathList.add(image.getPath());
                }
                adapter = new PhotosAdapter(AddPhotosActivity.this);
                recyclerView.setAdapter(adapter);
                adapter.addItems(images);

            } else {

                adapter = new PhotosAdapter(AddPhotosActivity.this);
                recyclerView.setAdapter(adapter);
                TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                emptyView.setText(getString(R.string.no_data));
            }

        }


        if (requestCode == APIError.UPDATE_TOKEN && resultCode == RESULT_OK) {

            if (data != null) {
                String apiName = data.getStringExtra("api");
                onRelaod(apiName);
            }

        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public void removeImage(String path) {

        if (imagePathList != null && imagePathList.size() > 0) {
            imagePathList.remove(path);
        }
    }

    private void UploadPhotos() {

        Utility.showProgress(AddPhotosActivity.this, getString(R.string.processing));
        List<MultipartBody.Part> parts = new ArrayList<>();

        for (String ss : imagePathList) {

            File file = new File(ss);
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part body = MultipartBody.Part.createFormData("files", file.getName(), requestFile);
            parts.add(body);
        }

        uploadPhotosCall = AdminApp.getInstance().getApi().uploadMultipleFile(parts);
        uploadPhotosCall.enqueue(new Callback<MultipleImageUpload>() {
            @Override
            public void onResponse(Call<MultipleImageUpload> call, Response<MultipleImageUpload> response) {

                try {
                    if (response.isSuccessful()) {
                        Utility.hideProgress();
                        if (response.body().isStatus()) {
                            if (response.body().getResult() != null) {
                                Toast.makeText(AddPhotosActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                Toast.makeText(AddPhotosActivity.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(AddPhotosActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        APIError error = APIError.parseError(response, AddPhotosActivity.this, App_Constants.API_MULTIPLE_FILE_UPLOAD);
                        Utility.error(AddPhotosActivity.this, error.message());
                    }
                } catch (Exception e) {
                    Utility.hideProgress();
                    e.printStackTrace();
                    Toast.makeText(AddPhotosActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<MultipleImageUpload> call, Throwable t) {
                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Log.e("ERROR UPLOAD", "" + t.getMessage());
                    Toast.makeText(AddPhotosActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void onRelaod(String apiName) {

        if (apiName != null) {
            if (apiName.equals(App_Constants.API_TEACHER_CLASS_SCHEDULE)) {
                ApiCall();
            } else if (apiName.equals(App_Constants.API_MULTIPLE_FILE_UPLOAD)) {
                UploadPhotos();
            }
        }
    }


    @Override
    protected void onStop() {
        super.onStop();

        if (call != null)
            call.cancel();

        if (uploadPhotosCall != null)
            uploadPhotosCall.cancel();
    }
}
