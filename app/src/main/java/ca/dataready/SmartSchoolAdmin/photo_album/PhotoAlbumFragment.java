/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.photo_album;


import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ca.dataready.SmartSchoolAdmin.GenericAdapter;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.Listeners.OnReloadListener;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.photo_album.adapter.PhotoAlbumAdapter;
import ca.dataready.SmartSchoolAdmin.photo_album.add.AddPhotosActivity;
import ca.dataready.SmartSchoolAdmin.server.GetAllDriverResponse;
import ca.dataready.SmartSchoolAdmin.userinfo.staff.add.AddStaffActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhotoAlbumFragment extends Fragment implements OnReloadListener {


    @BindView(R.id.recyclerView)
    SuperRecyclerView recyclerView;
    Unbinder unbinder;
    @BindView(R.id.fab_create)
    FloatingActionButton fabCreate;
    private GridLayoutManager gridLayoutManager;
    PhotoAlbumAdapter adapter;
    ArrayList<GetAllDriverResponse> data;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_photo_album, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) getActivity()).setSubTitle(getString(R.string.photo_album));
        ((HomeActivity) getActivity()).setOnReloadListener(this);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            gridLayoutManager = new GridLayoutManager(getActivity(), 3);
            recyclerView.setLayoutManager(gridLayoutManager);
        } else {
            gridLayoutManager = new GridLayoutManager(getActivity(), 2);
            recyclerView.setLayoutManager(gridLayoutManager);
        }

        Init();
    }

    private void Init() {

        adapter = new PhotoAlbumAdapter(getActivity());
        recyclerView.setAdapter(adapter);

      /*  GenericAdapter adapter = new GenericAdapter<GetAllDriverResponse>(getActivity(), data) {
            @Override
            public RecyclerView.ViewHolder setViewHolder(ViewGroup parent) {
                return null;
            }

            @Override
            public void onBindData(RecyclerView.ViewHolder holder, GetAllDriverResponse val) {

            }
        };
*/
    }

    @Override
    public void onReload(String apiName) {

        recyclerView.setRefreshing(false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.fab_create)
    public void onViewClicked() {

        startActivity(new Intent(getActivity(), AddPhotosActivity.class));
        if (getActivity() != null)
            getActivity().overridePendingTransition(R.anim.enter, R.anim.leave);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            gridLayoutManager = new GridLayoutManager(getContext(), 3);
        } else {
            gridLayoutManager = new GridLayoutManager(getContext(), 2);
        }
        recyclerView.setLayoutManager(gridLayoutManager);
    }
}
