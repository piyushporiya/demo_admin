/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.classevents.create;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ViewAnimator;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.DateFunction;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.fileselectoer.FileSelectionActivity;
import ca.dataready.SmartSchoolAdmin.schoolchannel.create.CreateSchoolChannelActivity;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.AddEventParams;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.ClassActivityResponse;
import ca.dataready.SmartSchoolAdmin.server.CreateClassActivityResponse;
import ca.dataready.SmartSchoolAdmin.viewresponse.adapter.FilesAdapter;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.FileModel;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.GradeList;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.ImageUpload;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateClassActivity extends BaseActivity {

    private static final int CAMERA_REQUEST_CODE = 101;
    private static final int REQUEST_CAMERA = 10;
    public static final String ACTION = "action";
    public static final String EDIT = "edit";
    public static final String OBJECT = "object";
    public static final String GRADE = "grade";
    public static final String CLASS = "class";
    public static final String REPEAT = "repeat";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.iv_SpeechToText)
    ImageView ivSpeechToText;
    @BindView(R.id.img_attachment)
    ImageView imgAttachment;
    @BindView(R.id.iv_camera_AttachFile)
    ImageView ivCameraAttachFile;
    List<FileModel> selectedFiles = new ArrayList<>();
    @BindView(R.id.txt_attachment)
    TextView txtAttachment;
    @BindView(R.id.rvAttachments)
    RecyclerView rvAttachments;
    @BindView(R.id.llAttachments)
    LinearLayout llAttachments;
    @BindView(R.id.title)
    EditText title;
    @BindView(R.id.msg)
    EditText msg;
    @BindView(R.id.txt_Date)
    TextView txtDate;
    @BindView(R.id.date_img)
    ImageView dateImg;
    @BindView(R.id.linear_date)
    RelativeLayout linearDate;
    @BindView(R.id.txt_start)
    TextView txtStart;
    @BindView(R.id.start_img)
    ImageView startImg;
    @BindView(R.id.linear_start)
    RelativeLayout linearStart;
    @BindView(R.id.txt_end)
    TextView txtEnd;
    @BindView(R.id.end_img)
    ImageView endImg;
    @BindView(R.id.linear_end)
    RelativeLayout linearEnd;
    @BindView(R.id.txt_Grade)
    TextView txtGrade;
    @BindView(R.id.grade_img)
    ImageView gradeImg;
    @BindView(R.id.linear_Grade)
    RelativeLayout linearGrade;
    @BindView(R.id.txt_Class)
    TextView txtClass;
    @BindView(R.id.class_img)
    ImageView classImg;
    @BindView(R.id.linear_Section)
    RelativeLayout linearSection;
    @BindView(R.id.llheader)
    LinearLayout llheader;
    @BindView(R.id.txt_repeat)
    TextView txtRepeat;
    @BindView(R.id.repeat_img)
    ImageView repeatImg;
    @BindView(R.id.linear_repeat)
    RelativeLayout linearRepeat;
    @BindView(R.id.txt_expire)
    TextView txtExpire;
    @BindView(R.id.expire_img)
    ImageView expireImg;
    @BindView(R.id.linear_expire)
    RelativeLayout linearExpire;
    @BindView(R.id.sendfeedback)
    Button sendfeedback;
    @BindView(R.id.llTextArea)
    LinearLayout llTextArea;
    @BindView(R.id.parent_viewAnimator)
    ViewAnimator parentViewAnimator;
    private FilesAdapter filesAdapter;
    private Calendar myCalendar = Calendar.getInstance();
    private ListPopupWindow listPopupWindow;
    ArrayList<String> gradeList = new ArrayList<>();
    ArrayList<String> classList = new ArrayList<>();
    ArrayList<String> repeatList = new ArrayList<>();
    private Call<GradeList> call;
    private List<GradeList.ResultBean.GradeBean> teacherBeans;
    private Call<CreateClassActivityResponse> createClassActivityCall;
    List<AddEventParams.FilesBean> files = new ArrayList<>();
    private Handler mUploadAttachmentshandler = new Handler();
    String type;
    ClassActivityResponse.ResultBean bean;
    private List<FileModel> updatedFiles = new ArrayList<>();
    private Call<CreateClassActivityResponse> updateNotificationCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_class_events);
        ButterKnife.bind(this);
        setHeaderInfo();
        Init();
    }

    private void Init() {

        ApiCall();

        rvAttachments.setLayoutManager(new LinearLayoutManager(CreateClassActivity.this));

        if (getIntent().getExtras() != null) {

            type = getIntent().getStringExtra(ACTION);
            bean = getIntent().getParcelableExtra(OBJECT);
            if (bean != null) {

                txtGrade.setText(bean.getGradeId());
                txtClass.setText(bean.getClassId());
                txtDate.setText(DateFunction.ConvertDate(bean.getNotificationDate(), "yyyy-MM-dd", "dd MMM yyyy"));
                txtStart.setText(bean.getNotificationStartTime());
                txtEnd.setText(bean.getNotificationEndTime());

                if (bean.getNotificationDate() != null && bean.getNotificationDate().contains("-")) {
                    myCalendar.set(Calendar.YEAR, Integer.parseInt(bean.getNotificationDate().split("-")[0]));
                    myCalendar.set(Calendar.MONTH, (Integer.parseInt(bean.getNotificationDate().split("-")[1]) - 1));
                    myCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(bean.getNotificationDate().split("-")[2]));
                }

                if (bean.getNotificationEndDate() != null) {

                    if (bean.getTimePeriod() != null && bean.getTimePeriod().isEmpty()) {
                        linearExpire.setVisibility(View.GONE);
                    } else {
                        linearExpire.setVisibility(View.VISIBLE);
                        txtRepeat.setText(bean.getTimePeriod());
                        txtExpire.setText(DateFunction.ConvertDate(bean.getNotificationEndDate(), "yyyy-MM-dd", "dd MMM yyyy"));
                    }
                }

                title.setText(bean.getNotificationShortMessage());
                msg.setText(bean.getNotificationLongMessage());

                sendfeedback.setText(getString(R.string.update));


                linearGrade.setEnabled(false);
                linearSection.setEnabled(false);

                linearGrade.setAlpha(0.5f);
                linearSection.setAlpha(0.5f);


                if (bean.getFiles() != null && bean.getFiles().size() > 0) {
                    filesAdapter = new FilesAdapter(CreateClassActivity.this);
                    rvAttachments.setAdapter(filesAdapter);
                    updatedFiles.clear();
                    for (ClassActivityResponse.ResultBean.FilesBean model : bean.getFiles()) {
                        if (model.getFilePath().contains("/")) {
                            updatedFiles.add(new FileModel(model.getFilePath().substring(model.getFilePath().lastIndexOf("/") + 1), model.getFilePath()));
                        }
                    }
                    filesAdapter.addItem(updatedFiles);

                    if (updatedFiles.size() > 0)
                        llAttachments.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private void setHeaderInfo() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(AdminApp.getInstance().getAdmin().getSchoolName() + " " + AdminApp.getInstance().getAdmin().getSchoolYear());
        getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + getString(R.string.create_class_events) + "</small>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }


    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        super.onBackPressed();
    }


    @OnClick({R.id.iv_SpeechToText, R.id.img_attachment, R.id.iv_camera_AttachFile, R.id.linear_date, R.id.linear_start, R.id.linear_end, R.id.linear_Grade, R.id.linear_Section, R.id.linear_repeat, R.id.linear_expire, R.id.sendfeedback})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.iv_SpeechToText:

                promptSpeechInput();

                break;

            case R.id.img_attachment:

                Intent intent = new Intent(CreateClassActivity.this, FileSelectionActivity.class);
                startActivityForResult(intent, 8);

                break;

            case R.id.iv_camera_AttachFile:

                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(CreateClassActivity.this, Manifest.permission.CAMERA)
                            == PackageManager.PERMISSION_DENIED) {

                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                CAMERA_REQUEST_CODE);
                    } else {

                        cameraIntent();
                    }
                } else {

                    cameraIntent();
                }

                break;

            case R.id.linear_date:

                setDate("d MMM yyyy", System.currentTimeMillis() - 1000);

                break;
            case R.id.linear_start:

                setStartDate();

                break;
            case R.id.linear_end:

                setEndTime();

                break;
            case R.id.linear_Grade:

                show_drop_down_to_select_option(linearGrade, GRADE);

                break;
            case R.id.linear_Section:

                show_drop_down_to_select_option(linearSection, CLASS);

                break;
            case R.id.linear_repeat:

                show_drop_down_to_select_option(linearRepeat, REPEAT);

                break;
            case R.id.linear_expire:

                setExpireDate("d MMM yyyy", System.currentTimeMillis() - 1000);

                break;
            case R.id.sendfeedback:

                if (txtDate.getText().toString().equalsIgnoreCase(getString(R.string.date_label))) {

                    Utility.showSnackBar(msg, getString(R.string.please_select_date));

                } else if (txtStart.getText().toString().equalsIgnoreCase(getString(R.string.select_time_from))) {

                    Utility.showSnackBar(msg, getString(R.string.please_select_time_from));

                } else if (txtEnd.getText().toString().equalsIgnoreCase(getString(R.string.select_time_to))) {

                    Utility.showSnackBar(msg, getString(R.string.please_select_time_to));

                } else if (txtGrade.getText().toString().equalsIgnoreCase(getString(R.string.select_grade_label))) {

                    Utility.showSnackBar(msg, getString(R.string.select_grade));

                } else if (txtClass.getText().toString().equalsIgnoreCase(getString(R.string.select_class_label))) {

                    Utility.showSnackBar(msg, getString(R.string.select_class));

                } /*else if (txtRepeat.getText().toString().equalsIgnoreCase(getString(R.string.repeat_label))) {

                    Utility.showSnackBar(msg, getString(R.string.please_select_repeat_duration));

                } */ else if (linearExpire.getVisibility() == View.VISIBLE && txtExpire.getText().toString().equalsIgnoreCase(getString(R.string.expiry_date_lable))) {

                    Utility.showSnackBar(msg, getString(R.string.please_select_expiry_date));

                } else if (title.getText().toString().isEmpty()) {

                    Utility.showSnackBar(msg, getString(R.string.enter_title));

                } else if (msg.getText().toString().isEmpty()) {

                    Utility.showSnackBar(msg, getString(R.string.enter_msg));

                } else {

                    if (type != null && type.equals(EDIT) && bean != null) {
                        if (selectedFiles != null && selectedFiles.size() > 0) {
                            files.clear();
                            for (int i = 0; i < selectedFiles.size(); i++) {
                                if (i == 0)
                                    UploadFile(true, selectedFiles.get(i).getPath());
                                else
                                    UploadFile(false, selectedFiles.get(i).getPath());
                            }
                            mUploadAttachmentshandler.postDelayed(runnable, 1000);
                        } else {
                            editClassActivity();
                        }

                    } else if (selectedFiles != null && selectedFiles.size() > 0) {
                        files.clear();
                        for (int i = 0; i < selectedFiles.size(); i++) {
                            if (i == 0)
                                UploadFile(true, selectedFiles.get(i).getPath());
                            else
                                UploadFile(false, selectedFiles.get(i).getPath());
                        }

                        mUploadAttachmentshandler.postDelayed(runnable, 1000);

                    } else {

                        createClassActivity();
                    }
                }

                break;

        }
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {

            if (selectedFiles != null && selectedFiles.size() > 0 && files.size() == selectedFiles.size()) {

                if (type != null && type.equals(EDIT))
                    editClassActivity();
                else
                    createClassActivity();

            } else {
                mUploadAttachmentshandler.postDelayed(runnable, 5000);
            }
        }
    };


    private void UploadFile(final boolean isFirstTime, String imageName) {

        if (isFirstTime)
            Utility.showProgress(CreateClassActivity.this, getString(R.string.processing));


        File file = new File(imageName);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        Call<ImageUpload> call = AdminApp.getInstance().getApi().upload(body);

        call.enqueue(new Callback<ImageUpload>() {
            @Override
            public void onResponse(Call<ImageUpload> call, Response<ImageUpload> response) {

                try {
                    if (response.isSuccessful()) {

                        if (response.body().isStatus()) {
                            if (response.body().getResult() != null) {
                                String image = response.body().getResult().getFilepath();
                                System.out.println(image);

                                AddEventParams.FilesBean bean = new AddEventParams.FilesBean();
                                bean.setFilePath(image);
                                files.add(bean);

                            } else {
                                Utility.hideProgress();
                                Toast.makeText(CreateClassActivity.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Utility.hideProgress();
                            Toast.makeText(CreateClassActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
                    Utility.hideProgress();
                    e.printStackTrace();
                    Toast.makeText(CreateClassActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<ImageUpload> call, Throwable t) {
                Utility.hideProgress();
                Log.e("ERROR UPLOAD", "" + t.getMessage());
                Toast.makeText(CreateClassActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void ApiCall() {

        parentViewAnimator.setDisplayedChild(0);

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();
        if (entity == null)
            return;

        call = AdminApp.getInstance().getApi().getClassSchedule(entity.getSchoolId(), entity.getSchoolYear());
        call.enqueue(new Callback<GradeList>() {
            @Override
            public void onResponse(Call<GradeList> call, Response<GradeList> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            teacherBeans = response.body().getResult().getGrade();
                        } else {
                            Utility.showSnackBar(msg, response.body().getMessage());
                        }

                    } else {

                        APIError error = APIError.parseError(response, CreateClassActivity.this, App_Constants.API_TEACHER_CLASS_SCHEDULE);
                        Utility.error(CreateClassActivity.this, error.message());
                        parentViewAnimator.setDisplayedChild(1);

                    }
                } catch (Exception e) {
                    Utility.showSnackBar(msg, getString(R.string.somethingwrong));
                    e.printStackTrace();
                }
                parentViewAnimator.setDisplayedChild(1);
            }

            @Override
            public void onFailure(Call<GradeList> call, Throwable t) {
                if (!call.isCanceled()) {
                    parentViewAnimator.setDisplayedChild(1);
                    System.out.println(t.getMessage());
                    Utility.showSnackBar(msg, App_Constants.NO_INTERNET);

                }
            }
        });

    }


    private void createClassActivity() {

        if (selectedFiles == null)
            Utility.showProgress(CreateClassActivity.this, getString(R.string.processing));

        AddEventParams params = new AddEventParams();
        params.setChannelType("class");
        params.setClassId(txtClass.getText().toString());
        params.setGradeId(txtGrade.getText().toString());
        params.setNotificationDate(DateFunction.ConvertDate(txtDate.getText().toString(), "dd MMM yyyy", "yyyy-MM-dd"));
        params.setNotificationStartTime(txtStart.getText().toString());
        params.setNotificationEndTime(txtEnd.getText().toString());
        params.setNotificationShortMessage(title.getText().toString());
        params.setNotificationLongMessage(msg.getText().toString());
        if (linearExpire.getVisibility() == View.VISIBLE) {
            params.setTimePeriod(txtRepeat.getText().toString().toLowerCase());
            params.setNotificationEndDate(DateFunction.ConvertDate(txtExpire.getText().toString(), "dd MMM yyyy", "yyyy-MM-dd"));
        } else {
            params.setNotificationEndDate(null);
        }
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
        params.setSchoolYear(AdminApp.getInstance().getAdmin().getSchoolYear());
        params.setStaffEmailId(AdminApp.getInstance().getAdmin().getEmailId());
        params.setStaffId(AdminApp.getInstance().getAdmin().getId());
        params.setStaffName(AdminApp.getInstance().getAdmin().getFirstName() + " " + AdminApp.getInstance().getAdmin().getLastName());
        params.setStaffProfilePic(AdminApp.getInstance().getAdmin().getProfilePic());
        if (selectedFiles != null && selectedFiles.size() > 0) {
            params.setFiles(files);
            for (AddEventParams.FilesBean s : files) {
                Log.e("========Files========", s.getFilePath());
            }
        }


        createClassActivityCall = AdminApp.getInstance().getApi().createClassActivities(params);
        createClassActivityCall.enqueue(new Callback<CreateClassActivityResponse>() {
            @Override
            public void onResponse(Call<CreateClassActivityResponse> call, Response<CreateClassActivityResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            setIntentData();
                            Toast.makeText(CreateClassActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        } else {

                            Toast.makeText(CreateClassActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    } else {

                        APIError error = APIError.parseError(response, CreateClassActivity.this, App_Constants.API_CREATE_CLASS_ACTIVITY);
                        Utility.error(CreateClassActivity.this, error.message());
                        parentViewAnimator.setDisplayedChild(1);

                    }
                } catch (Exception e) {

                    Utility.showSnackBar(msg, getString(R.string.somethingwrong));
                    e.printStackTrace();
                    parentViewAnimator.setDisplayedChild(1);
                }
            }

            @Override
            public void onFailure(Call<CreateClassActivityResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    System.out.println(t.getMessage());
                    Utility.showSnackBar(msg, App_Constants.NO_INTERNET);
                    parentViewAnimator.setDisplayedChild(1);
                }

            }
        });

    }

    private void editClassActivity() {

        if (selectedFiles != null && selectedFiles.size() == 0)
            Utility.showProgress(CreateClassActivity.this, getString(R.string.processing));

        AddEventParams params = new AddEventParams();
        params.setChannelType("class");
        params.setNotificationId(bean.getNotificationId());
        params.setClassId(bean.getClassId());
        params.setGradeId(bean.getGradeId());
        params.setNotificationDate(DateFunction.ConvertDate(txtDate.getText().toString(), "dd MMM yyyy", "yyyy-MM-dd"));
        params.setNotificationStartTime(txtStart.getText().toString());
        params.setNotificationEndTime(txtEnd.getText().toString());
        params.setNotificationShortMessage(title.getText().toString());
        params.setNotificationLongMessage(msg.getText().toString());
        if (linearExpire.getVisibility() == View.VISIBLE) {
            params.setTimePeriod(txtRepeat.getText().toString().toLowerCase());
            params.setNotificationEndDate(DateFunction.ConvertDate(txtExpire.getText().toString(), "dd MMM yyyy", "yyyy-MM-dd"));
        } else {
            params.setNotificationEndDate(null);
        }
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
        params.setSchoolYear(AdminApp.getInstance().getAdmin().getSchoolYear());
        params.setStaffEmailId(AdminApp.getInstance().getAdmin().getEmailId());
        params.setStaffId(AdminApp.getInstance().getAdmin().getId());
        params.setStaffName(AdminApp.getInstance().getAdmin().getFirstName() + " " + AdminApp.getInstance().getAdmin().getLastName());
        params.setStaffProfilePic(AdminApp.getInstance().getAdmin().getProfilePic());
        if (updatedFiles != null && updatedFiles.size() > 0) {

            for (FileModel model : updatedFiles) {
                AddEventParams.FilesBean bean = new AddEventParams.FilesBean();
                bean.setFilePath(model.getPath());
                files.add(bean);
            }

            for (AddEventParams.FilesBean s : files) {
                Log.e("========Files========", s.getFilePath());
            }
        }

        if (files != null && files.size() > 0) {
            params.setFiles(files);
        } else {
            List<AddEventParams.FilesBean> files = new ArrayList<>();
            params.setFiles(files);
        }

        updateNotificationCall = AdminApp.getInstance().getApi().updateClassActivities(params);
        updateNotificationCall.enqueue(new Callback<CreateClassActivityResponse>() {
            @Override
            public void onResponse(Call<CreateClassActivityResponse> call, Response<CreateClassActivityResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            Toast.makeText(CreateClassActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            setBackIntent();
                        } else {
                            Utility.showSnackBar(rvAttachments, response.body().getMessage());
                        }

                    } else {

                        APIError error = APIError.parseError(response, CreateClassActivity.this, App_Constants.API_UPDATE_EVENT_TO_CLASS);
                        Utility.error(CreateClassActivity.this, error.message());

                    }
                } catch (Exception e) {

                    Utility.showSnackBar(rvAttachments, getString(R.string.somethingwrong));
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<CreateClassActivityResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    System.out.println(t.getMessage());
                    Utility.showSnackBar(rvAttachments, App_Constants.NO_INTERNET);
                }
            }
        });

    }

    private void setBackIntent() {

        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    private void setIntentData() {
        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        finish();
    }


    public void setDate(final String myFormat, long maxDate) {

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                txtDate.setText(sdf.format(myCalendar.getTime()));
                txtExpire.setText(getString(R.string.select_expiry_date_label));

            }

        };

        DatePickerDialog dialog = new DatePickerDialog(CreateClassActivity.this, R.style.datepickerCustom, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        dialog.getDatePicker().setMinDate(maxDate);
        dialog.show();
    }

    public void setStartDate() {

        int hour = myCalendar.get(Calendar.HOUR_OF_DAY);
        int minute = myCalendar.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(CreateClassActivity.this, R.style.datepickerCustom, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                String AM_PM;
                if (selectedHour < 12) {
                    AM_PM = "AM";
                } else {
                    AM_PM = "PM";
                    selectedHour = selectedHour % 12;
                    if (selectedHour == 0) {
                        selectedHour = 12;
                    }
                }

                if (selectedHour < 10 && selectedMinute < 10) {
                    txtStart.setText(String.valueOf("0" + selectedHour) + ":" + String.valueOf("0" + selectedMinute) + " " + AM_PM);
                } else if (selectedHour < 10 || selectedMinute < 10) {
                    if (selectedHour < 10) {
                        txtStart.setText(String.valueOf("0" + selectedHour) + ":" + String.valueOf(selectedMinute) + " " + AM_PM);
                    } else {
                        txtStart.setText(String.valueOf(selectedHour) + ":" + String.valueOf("0" + selectedMinute) + " " + AM_PM);
                    }
                } else {
                    txtStart.setText(String.valueOf(selectedHour) + ":" + String.valueOf(selectedMinute) + " " + AM_PM);
                }
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle(getString(R.string.select_time_label));
        mTimePicker.show();
    }


    public void setEndTime() {

        int hour = myCalendar.get(Calendar.HOUR_OF_DAY);
        int minute = myCalendar.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(CreateClassActivity.this, R.style.datepickerCustom, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                String AM_PM;
                if (selectedHour < 12) {
                    AM_PM = "AM";
                } else {
                    AM_PM = "PM";
                    selectedHour = selectedHour % 12;
                    if (selectedHour == 0) {
                        selectedHour = 12;
                    }
                }

                if (selectedHour < 10 && selectedMinute < 10) {
                    txtEnd.setText(String.valueOf("0" + selectedHour) + ":" + String.valueOf("0" + selectedMinute) + " " + AM_PM);
                } else if (selectedHour < 10 || selectedMinute < 10) {
                    if (selectedHour < 10) {
                        txtEnd.setText(String.valueOf("0" + selectedHour) + ":" + String.valueOf(selectedMinute) + " " + AM_PM);
                    } else {
                        txtEnd.setText(String.valueOf(selectedHour) + ":" + String.valueOf("0" + selectedMinute) + " " + AM_PM);
                    }
                } else {
                    txtEnd.setText(String.valueOf(selectedHour) + ":" + String.valueOf(selectedMinute) + " " + AM_PM);
                }
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle(getString(R.string.select_time_label));
        mTimePicker.show();
    }

    public void setExpireDate(final String myFormat, long maxDate) {

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                txtExpire.setText(sdf.format(myCalendar.getTime()));

            }

        };

        DatePickerDialog dialog = new DatePickerDialog(CreateClassActivity.this, R.style.datepickerCustom, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        dialog.getDatePicker().setMinDate(myCalendar.getTimeInMillis());
        dialog.show();
    }

    private void show_drop_down_to_select_option(RelativeLayout layout, final String which) {

        if (teacherBeans != null && teacherBeans.size() > 0) {

           /* txtSubmitAttendance.setEnabled(false);
            txtSubmitAttendance.setAlpha(0.5f);*/

            listPopupWindow = new ListPopupWindow(CreateClassActivity.this);
            if (which.equals(GRADE)) {

                gradeList.clear();
                for (GradeList.ResultBean.GradeBean beans : teacherBeans) {
                    if (!gradeList.contains(beans.getGradeId()))
                        gradeList.add(beans.getGradeId());
                }

                listPopupWindow.setAdapter(new ArrayAdapter(CreateClassActivity.this, R.layout.list_dropdown_item, gradeList));

            } else if (which.equals(CLASS)) {

                classList.clear();
                for (GradeList.ResultBean.GradeBean beans : teacherBeans) {

                    if (txtGrade.getText().toString().equals(beans.getGradeId())) {
                        if (!classList.contains(beans.getClassX()))
                            for (int i = 0; i < beans.getClassX().size(); i++)
                                classList.add(beans.getClassX().get(i).getClassId());
                    }
                }

                if (classList != null && classList.size() > 0) {
                    Collections.sort(classList, new Comparator<String>() {
                        public int compare(String obj1, String obj2) {
                            // ## Ascending order
                            return obj1.compareToIgnoreCase(obj2); // To compare string values
                        }
                    });
                }
                listPopupWindow.setAdapter(new ArrayAdapter(CreateClassActivity.this, R.layout.list_dropdown_item, classList));

            } else if (which.equals(REPEAT)) {

                repeatList.clear();
                repeatList.add(getString(R.string.do_not_repeat));
                repeatList.add(getString(R.string.every_day));
                repeatList.add(getString(R.string.every_week));
                repeatList.add(getString(R.string.every_month));

                listPopupWindow.setAdapter(new ArrayAdapter(CreateClassActivity.this, R.layout.list_dropdown_item, repeatList));

            }

            listPopupWindow.setAnchorView(layout);
            listPopupWindow.setWidth(ListPopupWindow.WRAP_CONTENT);
            listPopupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);

            listPopupWindow.setModal(true);
            listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                    if (which.equals(GRADE)) {

                        txtGrade.setText(gradeList.get(i));
                        txtClass.setText(App_Constants.SELECT_CLASS);

                    } else if (which.equals(CLASS)) {

                        txtClass.setText(classList.get(i));

                    } else if (which.equals(REPEAT)) {

                        txtRepeat.setText(repeatList.get(i));

                        if (!repeatList.get(i).equalsIgnoreCase(getString(R.string.do_not_repeat))) {
                            linearExpire.setVisibility(View.VISIBLE);
                        } else {
                            linearExpire.setVisibility(View.GONE);
                        }

                    }
                    listPopupWindow.dismiss();

                }
            });
            listPopupWindow.show();
        }
    }


    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, App_Constants.REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Utility.showToast(CreateClassActivity.this, getString(R.string.speech_not_supported));
        }
    }

    private void cameraIntent() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);

    }

    @Override
    public void onActivityResult(final int requestCode, int resultCode, Intent data) {

        switch (requestCode) {

            case 8:

                if (resultCode == RESULT_OK && null != data) {


                    ArrayList<File> Files = (ArrayList<File>) data.getSerializableExtra(FileSelectionActivity.FILES_TO_UPLOAD);
                    //selectedFiles.clear();
                    for (File file : Files) {
                        String uri = file.getAbsolutePath();
                        Log.e("PATH========", uri);
                        if (file.toString().contains("/")) {
                            selectedFiles.add(new FileModel(file.toString().substring(file.toString().lastIndexOf("/") + 1), uri));
                        }
                    }

                    List<FileModel> finalList = new ArrayList<>();
                    if (updatedFiles != null && updatedFiles.size() > 0) {
                        finalList.addAll(updatedFiles);
                    }

                    finalList.addAll(selectedFiles);

                    if (selectedFiles != null && selectedFiles.size() > 0) {
                        llAttachments.setVisibility(View.VISIBLE);

                        rvAttachments.addItemDecoration(new DividerItemDecoration(CreateClassActivity.this, DividerItemDecoration.VERTICAL));
                        filesAdapter = new FilesAdapter(CreateClassActivity.this);
                        rvAttachments.setAdapter(filesAdapter);
                        filesAdapter.addItem(finalList);
                    }
                }

                break;

            case APIError.UPDATE_TOKEN:

                if (resultCode == RESULT_OK && null != data) {
                    String apiName = data.getStringExtra("api");
                    onReload(apiName);

                }

                break;

            case REQUEST_CAMERA:

                Bitmap imageBitmap = null;

                if (resultCode == RESULT_OK) {
                    Bundle extras = data.getExtras();
                    if (extras != null) {
                        imageBitmap = (Bitmap) extras.get("data");

                    }

                    Uri tempUri = getImageUri(CreateClassActivity.this, imageBitmap);
                    File finalFile = new File(getRealPathFromURI(tempUri));
                    String picturePath = finalFile.toString();
                    System.out.println(finalFile);


                    List<FileModel> finalList = new ArrayList<>();
                    if (updatedFiles != null && updatedFiles.size() > 0) {
                        finalList.addAll(updatedFiles);
                    }

                    selectedFiles.add(new FileModel(picturePath.substring(picturePath.lastIndexOf("/") + 1), picturePath));

                    finalList.addAll(selectedFiles);

                    llAttachments.setVisibility(View.VISIBLE);
                    rvAttachments.setLayoutManager(new LinearLayoutManager(CreateClassActivity.this));
                    rvAttachments.addItemDecoration(new DividerItemDecoration(CreateClassActivity.this, DividerItemDecoration.VERTICAL));
                    filesAdapter = new FilesAdapter(CreateClassActivity.this);
                    rvAttachments.setAdapter(filesAdapter);
                    filesAdapter.addItem(finalList);
                }

                break;

            case App_Constants.REQ_CODE_SPEECH_INPUT:

                if (resultCode == Activity.RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

                    if (title.hasFocus())
                        title.setText(result.get(0));
                    else
                        msg.setText(result.get(0));
                }

                break;
        }

    }

    public void onReload(String apiName) {

        switch (apiName) {
            case App_Constants.API_TEACHER_CLASS_SCHEDULE:
                ApiCall();
                break;
            case App_Constants.API_UPDATE_EVENT_TO_CLASS:
                sendfeedback.performClick();
            case App_Constants.API_CREATE_CLASS_ACTIVITY:
                sendfeedback.performClick();
                break;
        }

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public void removeUploadItem(FileModel file) {

        if (selectedFiles != null && selectedFiles.size() > 0) {
            selectedFiles.remove(file);
        }

        if (type != null && type.equals(EDIT)) {

            if (updatedFiles != null && updatedFiles.size() > 0) {
                updatedFiles.remove(file);
            }
        }

        if (filesAdapter != null && filesAdapter.getItemCount() == 0) {
            llAttachments.setVisibility(View.GONE);
        }
    }


}
