/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;

import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.counter.BadgeCountTracker;
import ca.dataready.SmartSchoolAdmin.login.LoginActivity;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;

import static ca.dataready.SmartSchoolAdmin.appointment.upcoming.UpcomingAppointmentFragment.isScheduleVisible;
import static ca.dataready.SmartSchoolAdmin.chat.ChatActivity.isChatScreenVisible;
import static ca.dataready.SmartSchoolAdmin.classevents.ClassEventsFragment.isClassEventsVisible;
import static ca.dataready.SmartSchoolAdmin.dashboard.DashBoardFragment.isDashboardVisible;
import static ca.dataready.SmartSchoolAdmin.schoolevents.SchoolEventsFragment.isSchoolEventVisible;
import static ca.dataready.SmartSchoolAdmin.viewresponse.ViewResponseFragment.isViewRespondVisible;


public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {

    Intent intent;

    public static final String SCHEDULE_PUSH_NOTIFICATION_BROADCAST = "schedule_push_notification_broadcast";
    public static final String VIEW_RESPOND_PUSH_NOTIFICATION_BROADCAST = "view_respond_push_notification_broadcast";
    public static final String UPDATE_DASHBOARD_BROADCAST = "update_dashboard_broadcast";
    public static final String VIEW_RESPOND_THREAD_PUSH_NOTIFICATION_BROADCAST = "view_respond_thread_push_notification_broadcast";
    public static final String SCHOOL_EVENT_PUSH_NOTIFICATION_BROADCAST = "school_event_push_notification_broadcast";
    public static final String CLASS_EVENT_PUSH_NOTIFICATION_BROADCAST = "class_event_push_notification_broadcast";

    public static final String CHANNEL_TYPE = "channelType";
    public static final String MESSAGE_TYPE = "messageType";

    public static final String CLASS = "class";
    public static final String APPOINTMENT = "appointment";
    public static final String COMMUNICATION = "communication";

    public static final String STUDENT_ID = "studentId";
    public static final String PROFILE_ID = "profileId";
    public static final String GRADE_ID = "gradeId";
    public static final String CLASS_ID = "classId";
    public static final String GROUP_ID = "groupId";

    public static final String SCHOOL_ID = "schoolId";
    public static final String MESSAGE_ID = "messageId";
    public static final String APPOINTMENT_ID = "appointmentId";
    public static final String NOTIFICATION_ID = "notificationId";


    String channelType, messageType, gradId, classId, studentId, profileId, schoolId, messageId, appointmentId, notificationId, groupId;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e("notification", remoteMessage.getNotification().getBody() + " N " + remoteMessage.getData());

        // intent = new Intent(getApplicationContext(), Splash.class);

        channelType = remoteMessage.getData().get(CHANNEL_TYPE);
        messageType = remoteMessage.getData().get(MESSAGE_TYPE);
        gradId = remoteMessage.getData().get(GRADE_ID);
        classId = remoteMessage.getData().get(CLASS_ID);
        studentId = remoteMessage.getData().get(STUDENT_ID);
        profileId = remoteMessage.getData().get(PROFILE_ID);
        schoolId = remoteMessage.getData().get(SCHOOL_ID);
        messageId = remoteMessage.getData().get(MESSAGE_ID);
        appointmentId = remoteMessage.getData().get(APPOINTMENT_ID);
        notificationId = remoteMessage.getData().get(NOTIFICATION_ID);
        groupId = remoteMessage.getData().get(GROUP_ID);

        if (AdminApp.getInstance().getCredential() != null)
            intent = new Intent(getApplicationContext(), HomeActivity.class);
        else
            intent = new Intent(getApplicationContext(), LoginActivity.class);

        intent.putExtra(CHANNEL_TYPE, remoteMessage.getData().get(CHANNEL_TYPE));
        intent.putExtra(MESSAGE_TYPE, remoteMessage.getData().get(MESSAGE_TYPE));
        intent.putExtra(GRADE_ID, remoteMessage.getData().get(GRADE_ID));
        intent.putExtra(CLASS_ID, remoteMessage.getData().get(CLASS_ID));
        intent.putExtra(STUDENT_ID, remoteMessage.getData().get(STUDENT_ID));
        intent.putExtra(PROFILE_ID, remoteMessage.getData().get(PROFILE_ID));
        intent.putExtra(SCHOOL_ID, remoteMessage.getData().get(SCHOOL_ID));
        intent.putExtra(MESSAGE_ID, remoteMessage.getData().get(MESSAGE_ID));
        intent.putExtra(APPOINTMENT_ID, remoteMessage.getData().get(APPOINTMENT_ID));
        intent.putExtra(NOTIFICATION_ID, remoteMessage.getData().get(NOTIFICATION_ID));
        intent.putExtra(GROUP_ID, remoteMessage.getData().get(GROUP_ID));
        // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        CREDENTIAL mainEntity = AdminApp.getInstance().getCredential();
        CREDENTIAL.ResultBean.SchoolpermissionsBean entity = null;
        CREDENTIAL.ResultBean.SchoolpermissionsBean.AdminmodulesBean adminPermissions = null;
        if (mainEntity != null && mainEntity.getResult() != null && mainEntity.getResult().size() > 0) {
            entity = mainEntity.getResult().get(0).getSchoolpermissions();
            adminPermissions = entity.getAdminmodules();
        }

        if (messageType != null) {

            if (messageType.equals(APPOINTMENT)) {

                if (adminPermissions != null) {
                    if (adminPermissions.isAppointment()) {
                        if (isScheduleVisible) {
                            Intent intent = new Intent(SCHEDULE_PUSH_NOTIFICATION_BROADCAST);
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                        } else {
                            showNotification(remoteMessage);
                        }
                    }
                } else {

                    if (isScheduleVisible) {
                        Intent intent = new Intent(SCHEDULE_PUSH_NOTIFICATION_BROADCAST);
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                    } else {
                        showNotification(remoteMessage);
                    }
                }

            }  else if (messageType.equals(COMMUNICATION)) {

                if (adminPermissions != null) {
                    if (adminPermissions.isCommunication()) {

                        if (isViewRespondVisible) {

                            int currentCounter = BadgeCountTracker.getViewandResposndSingleThreadBadgeCounter(this, messageId);
                            BadgeCountTracker.saveViewandResposndSingleThreadBadgeCounter(this, currentCounter + 1, messageId);

                            Intent intent = new Intent(VIEW_RESPOND_PUSH_NOTIFICATION_BROADCAST);
                            intent.putExtra(MESSAGE_ID, messageId);
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                        } else if (isChatScreenVisible) {

                            Intent intent = new Intent(VIEW_RESPOND_THREAD_PUSH_NOTIFICATION_BROADCAST);
                            intent.putExtra(MESSAGE_ID, messageId);
                            intent.putExtra(STUDENT_ID, studentId);
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                        }
                    }
                } else {

                    if (isViewRespondVisible) {

                        int currentCounter = BadgeCountTracker.getViewandResposndSingleThreadBadgeCounter(this, messageId);
                        BadgeCountTracker.saveViewandResposndSingleThreadBadgeCounter(this, currentCounter + 1, messageId);

                        Intent intent = new Intent(VIEW_RESPOND_PUSH_NOTIFICATION_BROADCAST);
                        intent.putExtra(MESSAGE_ID, messageId);
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                    } else if (isChatScreenVisible) {

                        Intent intent = new Intent(VIEW_RESPOND_THREAD_PUSH_NOTIFICATION_BROADCAST);
                        intent.putExtra(MESSAGE_ID, messageId);
                        intent.putExtra(STUDENT_ID, studentId);
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                    }

                }
            }
        } else {

            showNotification(remoteMessage);
        }


    }


    private void showNotification(RemoteMessage message) {

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setAutoCancel(true)
                .setContentTitle(message.getNotification().getTitle())
                .setContentText(message.getNotification().getBody())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setSound(uri)
                .setContentIntent(pendingIntent);

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        manager.notify(0, builder.build());


    }


    @Override
    public void handleIntent(Intent intent) {
        super.handleIntent(intent);

        Bundle extras = intent.getExtras();
        if (extras != null) {

            channelType = extras.getString(CHANNEL_TYPE);
            messageType = extras.getString(MESSAGE_TYPE);
            gradId = extras.getString(GRADE_ID);
            classId = extras.getString(CLASS_ID);
            studentId = extras.getString(STUDENT_ID);
            profileId = extras.getString(PROFILE_ID);
            schoolId = extras.getString(SCHOOL_ID);
            messageId = extras.getString(MESSAGE_ID);
            appointmentId = extras.getString(APPOINTMENT_ID);
            notificationId = extras.getString(NOTIFICATION_ID);
            groupId = extras.getString(GROUP_ID);

            Log.e("handleIntent", "found in handleIntent");
            Log.e("channelType", "" + channelType);
            Log.e("messageType", "" + messageType);
            Log.e("gradId", "" + gradId);
            Log.e("classId", "" + classId);
            Log.e("studentId", "" + studentId);
            Log.e("profileId", "" + profileId);
            Log.e("schoolId", "" + schoolId);
            Log.e("messageId", "" + messageId);
            Log.e("appointmentId", "" + appointmentId);
            Log.e("notificationId", "" + notificationId);
            Log.e("groupId", "" + groupId);

            CREDENTIAL mainEntity = AdminApp.getInstance().getCredential();
            CREDENTIAL.ResultBean.SchoolpermissionsBean entity = null;
            CREDENTIAL.ResultBean.SchoolpermissionsBean.AdminmodulesBean adminPermissions = null;
            if (mainEntity != null && mainEntity.getResult() != null && mainEntity.getResult().size() > 0) {
                entity = mainEntity.getResult().get(0).getSchoolpermissions();
                adminPermissions = entity.getAdminmodules();
            }

            if (messageType != null) {

                switch (messageType) {

                    case APPOINTMENT:

                        if (adminPermissions != null) {
                            if (adminPermissions.isAppointment()) {

                                if (!isScheduleVisible) {

                                    int currentCount = BadgeCountTracker.getAppointmentBadgeCounter(getApplicationContext());
                                    BadgeCountTracker.saveAppointmentBadgeCounter(getApplicationContext(), currentCount + 1);
                                }
                            }
                        } else {

                            if (!isScheduleVisible) {
                                int currentCount = BadgeCountTracker.getAppointmentBadgeCounter(getApplicationContext());
                                BadgeCountTracker.saveAppointmentBadgeCounter(getApplicationContext(), currentCount + 1);
                            }

                        }

                        break;

                    case COMMUNICATION:

                        if (adminPermissions != null) {
                            if (adminPermissions.isCommunication()) {

                                if (!isViewRespondVisible && !isChatScreenVisible) {
                                    int currentCount = BadgeCountTracker.getViewandResposndBadgeCounter(getApplicationContext());
                                    BadgeCountTracker.saveViewandResposndBadgeCounter(getApplicationContext(), currentCount + 1);

                                    int currentCounter = BadgeCountTracker.getViewandResposndSingleThreadBadgeCounter(this, messageId);
                                    BadgeCountTracker.saveViewandResposndSingleThreadBadgeCounter(this, currentCounter + 1, messageId);

                                }
                            }
                        } else {

                            if (!isViewRespondVisible && !isChatScreenVisible) {
                                int currentCount = BadgeCountTracker.getViewandResposndBadgeCounter(getApplicationContext());
                                BadgeCountTracker.saveViewandResposndBadgeCounter(getApplicationContext(), currentCount + 1);

                                int currentCounter = BadgeCountTracker.getViewandResposndSingleThreadBadgeCounter(this, messageId);
                                BadgeCountTracker.saveViewandResposndSingleThreadBadgeCounter(this, currentCounter + 1, messageId);

                            }
                        }
                        break;
                }


                if (isDashboardVisible) {
                    Intent i = new Intent(UPDATE_DASHBOARD_BROADCAST);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(i);
                }

            }

        } else {
            Log.e("handleIntent", "not found in handleIntent");
        }

    }

}
