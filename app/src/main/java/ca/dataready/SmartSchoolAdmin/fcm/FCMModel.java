/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.fcm;


import android.os.Parcel;
import android.os.Parcelable;

public class FCMModel implements Parcelable {

    private String channelType, messageType, gradId, classId, studentId, profileId, schoolId, messageId;



    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getGradId() {
        return gradId;
    }

    public void setGradId(String gradId) {
        this.gradId = gradId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.channelType);
        dest.writeString(this.messageType);
        dest.writeString(this.gradId);
        dest.writeString(this.classId);
        dest.writeString(this.studentId);
        dest.writeString(this.profileId);
        dest.writeString(this.schoolId);
        dest.writeString(this.messageId);
    }

    public FCMModel() {
    }

    protected FCMModel(Parcel in) {
        this.channelType = in.readString();
        this.messageType = in.readString();
        this.gradId = in.readString();
        this.classId = in.readString();
        this.studentId = in.readString();
        this.profileId = in.readString();
        this.schoolId = in.readString();
        this.messageId = in.readString();
    }

    public static final Creator<FCMModel> CREATOR = new Creator<FCMModel>() {
        @Override
        public FCMModel createFromParcel(Parcel source) {
            return new FCMModel(source);
        }

        @Override
        public FCMModel[] newArray(int size) {
            return new FCMModel[size];
        }
    };
}
