/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.configuration.master_table.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.configuration.master_table.MasterTableFragment;
import ca.dataready.SmartSchoolAdmin.server.MasterTableModel;
import ca.dataready.SmartSchoolAdmin.server.TimeTableModel;


/**
 * Created by social_jaydeep on 07/09/17.
 */

public class MasterTableAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private MasterTableFragment fragment;
    private List<MasterTableModel> rowItems;
    private LayoutInflater mInflater;

    public MasterTableAdapter(Context context, MasterTableFragment masterTableFragment) {
        this.context = context;
        fragment = masterTableFragment;
        this.rowItems = new ArrayList<MasterTableModel>();
        mInflater = LayoutInflater.from(context);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewGroup vVideos = (ViewGroup) mInflater.inflate(R.layout.raw_master_table, parent, false);
        return new ItemViewHolder(vVideos);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        final MasterTableModel bean = rowItems.get(position);

        if (holder instanceof ItemViewHolder) {

            ((ItemViewHolder) holder).txtId.setText(String.valueOf(bean.getId()));
            ((ItemViewHolder) holder).txtStartTime.setText(bean.getStartTime());
            ((ItemViewHolder) holder).txtEndTime.setText(bean.getEndTime());
            ((ItemViewHolder) holder).txtDuration.setText(String.valueOf(bean.getDuration()));
            ((ItemViewHolder) holder).isAttendance.setChecked(bean.isAttendance());

            ((ItemViewHolder) holder).txtRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    rowItems.remove(bean);
                    notifyDataSetChanged();

                    if(rowItems.size()==0){
                        fragment.setEmptyView();
                    }
                }
            });

        }

    }

    @Override
    public int getItemCount() {
        return rowItems == null ? 0 : rowItems.size();
    }


    public void addItem(List<MasterTableModel> productDetails) {

        rowItems.addAll(productDetails);
        notifyDataSetChanged();
    }

    public void addSingleItem(MasterTableModel model) {

        rowItems.add(model);
        notifyDataSetChanged();
    }

    public void clear() {
        rowItems.clear();
        notifyDataSetChanged();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_id)
        TextView txtId;
        @BindView(R.id.txt_start_time)
        TextView txtStartTime;
        @BindView(R.id.txt_end_time)
        TextView txtEndTime;
        @BindView(R.id.txt_duration)
        TextView txtDuration;
        @BindView(R.id.isAttendance)
        CheckBox isAttendance;
        @BindView(R.id.txt_remove)
        TextView txtRemove;

        //R.layout.raw_master_table
        ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}


