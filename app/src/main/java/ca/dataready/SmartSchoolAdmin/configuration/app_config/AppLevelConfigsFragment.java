/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.configuration.app_config;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ViewAnimator;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.dashboard.adapter.DashBoard;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;


public class AppLevelConfigsFragment extends Fragment {


    @BindView(R.id.txtAdminModule)
    TextView txtAdminModule;
    @BindView(R.id.card_admin_module)
    CardView cardAdminModule;
    @BindView(R.id.chk_appointment)
    CheckBox chkAppointment;
    @BindView(R.id.chk_channel)
    CheckBox chkChannel;
    @BindView(R.id.chk_communication)
    CheckBox chkCommunication;
    @BindView(R.id.chk_driver)
    CheckBox chkDriver;
    @BindView(R.id.chk_events)
    CheckBox chkEvents;
    @BindView(R.id.chk_fee)
    CheckBox chkFee;
    @BindView(R.id.chk_route)
    CheckBox chkRoute;
    @BindView(R.id.chk_staff)
    CheckBox chkStaff;
    @BindView(R.id.chk_student)
    CheckBox chkStudent;
    @BindView(R.id.btn_next)
    Button btnNext;
    @BindView(R.id.layout_admin_module)
    CardView layoutAdminModule;
    @BindView(R.id.txtTeacherModule)
    TextView txtTeacherModule;
    @BindView(R.id.card_teacher_module)
    CardView cardTeacherModule;
    @BindView(R.id.chk_teacher_appointment)
    CheckBox chkTeacherAppointment;
    @BindView(R.id.chk_teacher_attendance)
    CheckBox chkTeacherAttendance;
    @BindView(R.id.chk_teacher_communication)
    CheckBox chkTeacherCommunication;
    @BindView(R.id.chk_teacher_t2t)
    CheckBox chkTeacherT2t;
    @BindView(R.id.btn_teacher_previous)
    Button btnTeacherPrevious;
    @BindView(R.id.btn_teacher_next)
    Button btnTeacherNext;
    @BindView(R.id.layout_teacher_module)
    CardView layoutTeacherModule;
    @BindView(R.id.txtParentModule)
    TextView txtParentModule;
    @BindView(R.id.card_parent_module)
    CardView cardParentModule;
    @BindView(R.id.chk_parent_appointment)
    CheckBox chkParentAppointment;
    @BindView(R.id.chk_parent_attendance)
    CheckBox chkParentAttendance;
    @BindView(R.id.chk_parent_communication)
    CheckBox chkParentCommunication;
    @BindView(R.id.chk_parent_homework)
    CheckBox chkParentHomework;
    @BindView(R.id.chk_parent_channel)
    CheckBox chkParentChannel;
    @BindView(R.id.chk_parent_events)
    CheckBox chkParentEvents;
    @BindView(R.id.chk_parent_fee)
    CheckBox chkParentFee;
    @BindView(R.id.chk_parent_p2p)
    CheckBox chkParentP2p;
    @BindView(R.id.chk_parent_route)
    CheckBox chkParentRoute;
    @BindView(R.id.et_parent_refresh_frequncy)
    EditText etParentRefreshFrequncy;
    @BindView(R.id.btn_parent_previous)
    Button btnParentPrevious;
    @BindView(R.id.btn_parent_next)
    Button btnParentNext;
    @BindView(R.id.layout_parent_module)
    CardView layoutParentModule;
    @BindView(R.id.txtDriverModule)
    TextView txtDriverModule;
    @BindView(R.id.card_driver_module)
    CardView cardDriverModule;
    @BindView(R.id.chk_driver_file_upload)
    CheckBox chkDriverFileUpload;
    @BindView(R.id.chk_driver_pickupdropoff)
    CheckBox chkDriverPickupdropoff;
    @BindView(R.id.chk_driver_qr_scan)
    CheckBox chkDriverQrScan;
    @BindView(R.id.chk_teacher_homework)
    CheckBox chkTeacherHomework;
    @BindView(R.id.chk_driver_video)
    CheckBox chkDriverVideo;
    @BindView(R.id.et_driver_refresh_frequncy)
    EditText etDriverRefreshFrequncy;
    @BindView(R.id.btn_driver_previous)
    Button btnDriverPrevious;
    @BindView(R.id.btn_driver_next)
    Button btnDriverNext;
    @BindView(R.id.layout_driver_module)
    CardView layoutDriverModule;
    @BindView(R.id.txtAppModule)
    TextView txtAppModule;
    @BindView(R.id.card_app_module)
    CardView cardAppModule;
    @BindView(R.id.chk_app_admin)
    CheckBox chkAppAdmin;
    @BindView(R.id.chk_app_teacher)
    CheckBox chkAppTeacher;
    @BindView(R.id.chk_app_parent)
    CheckBox chkAppParent;
    @BindView(R.id.chk_app_driver)
    CheckBox chkAppDriver;
    @BindView(R.id.btn_update)
    Button btnUpdate;
    @BindView(R.id.layout_app_module)
    CardView layoutAppModule;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.txt_error)
    TextView txtError;
    @BindView(R.id.viewAnimator)
    ViewAnimator viewAnimator;
    Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_app_level_config, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setValues();
    }

    private void setValues() {

        CREDENTIAL mainEntity = AdminApp.getInstance().getCredential();
        CREDENTIAL.ResultBean.SchoolpermissionsBean schoolPermissions = null;
        if (mainEntity != null && mainEntity.getResult() != null && mainEntity.getResult().size() > 0) {
            schoolPermissions = mainEntity.getResult().get(0).getSchoolpermissions();
        }

        if (schoolPermissions != null) {

            if (schoolPermissions.getAdminmodules() != null) {

                CREDENTIAL.ResultBean.SchoolpermissionsBean.AdminmodulesBean adminBean = schoolPermissions.getAdminmodules();
                chkAppointment.setChecked(adminBean.isAppointment());
                chkChannel.setChecked(adminBean.isChannel());
                chkCommunication.setChecked(adminBean.isCommunication());
                chkDriver.setChecked(adminBean.isDriver());
                chkEvents.setChecked(adminBean.isEvents());
                chkFee.setChecked(adminBean.isFee());
                chkRoute.setChecked(adminBean.isRoute());
                chkStaff.setChecked(adminBean.isStaff());
                chkStudent.setChecked(adminBean.isStudent());
            }

            if (schoolPermissions.getTeachermodules() != null) {

                CREDENTIAL.ResultBean.SchoolpermissionsBean.TeachermodulesBean teacherBean = schoolPermissions.getTeachermodules();
                chkTeacherAppointment.setChecked(teacherBean.isAppointment());
                chkTeacherAttendance.setChecked(teacherBean.isAttendance());
                chkTeacherCommunication.setChecked(teacherBean.isCommunication());
                chkTeacherHomework.setChecked(teacherBean.isHomework());
                chkTeacherT2t.setChecked(teacherBean.isT2t());
            }

            if (schoolPermissions.getParentmodules() != null) {

                CREDENTIAL.ResultBean.SchoolpermissionsBean.ParentmodulesBean parentBean = schoolPermissions.getParentmodules();
                chkParentAppointment.setChecked(parentBean.isAppointment());
                chkParentAttendance.setChecked(parentBean.isAttendance());
                chkParentCommunication.setChecked(parentBean.isCommunication());
                chkParentHomework.setChecked(parentBean.isHomework());
                chkParentChannel.setChecked(parentBean.isChannel());
                chkParentEvents.setChecked(parentBean.isEvents());
                chkParentFee.setChecked(parentBean.isFee());
                chkParentRoute.setChecked(parentBean.isRoute());
                chkParentP2p.setChecked(parentBean.isP2p());
                etParentRefreshFrequncy.setText(parentBean.getRefreshFrequency());
            }

            if (schoolPermissions.getDrivermodules() != null) {

                CREDENTIAL.ResultBean.SchoolpermissionsBean.DrivermodulesBean driverBean = schoolPermissions.getDrivermodules();
                chkDriverFileUpload.setChecked(driverBean.isFileupload());
                chkDriverPickupdropoff.setChecked(driverBean.isPickupdropoff());
                chkDriverQrScan.setChecked(driverBean.isQrscan());
                chkDriverVideo.setChecked(driverBean.isVideo());
                etDriverRefreshFrequncy.setText(driverBean.getRefreshFrequency());
            }

            if (schoolPermissions.getApps() != null) {

                CREDENTIAL.ResultBean.SchoolpermissionsBean.AppsBean appBean = schoolPermissions.getApps();
                chkAppDriver.setChecked(appBean.isDriver());
                chkAppParent.setChecked(appBean.isParent());
                chkAppTeacher.setChecked(appBean.isTeacher());
                chkAppAdmin.setChecked(appBean.isAdmin());
            }
        }

        viewAnimator.setDisplayedChild(1);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.card_admin_module, R.id.btn_next, R.id.card_teacher_module, R.id.btn_teacher_previous, R.id.btn_teacher_next
            , R.id.card_parent_module, R.id.btn_parent_previous, R.id.btn_parent_next, R.id.card_driver_module
            , R.id.btn_driver_previous, R.id.btn_driver_next, R.id.card_app_module, R.id.btn_update, R.id.btn_app_previous})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.card_admin_module:

                layoutAdminModule.setVisibility(layoutAdminModule.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                if (layoutTeacherModule.getVisibility() == View.VISIBLE) {
                    layoutTeacherModule.setVisibility(View.GONE);
                    txtTeacherModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutParentModule.getVisibility() == View.VISIBLE) {
                    layoutParentModule.setVisibility(View.GONE);
                    txtParentModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutDriverModule.getVisibility() == View.VISIBLE) {
                    layoutDriverModule.setVisibility(View.GONE);
                    txtDriverModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutAppModule.getVisibility() == View.VISIBLE) {
                    layoutAppModule.setVisibility(View.GONE);
                    txtAppModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutAdminModule.getVisibility() == View.VISIBLE)
                    txtAdminModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_up), null);
                else
                    txtAdminModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);

                break;
            case R.id.card_teacher_module:

                layoutTeacherModule.setVisibility(layoutTeacherModule.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                if (layoutAdminModule.getVisibility() == View.VISIBLE) {
                    layoutAdminModule.setVisibility(View.GONE);
                    txtAdminModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutParentModule.getVisibility() == View.VISIBLE) {
                    layoutParentModule.setVisibility(View.GONE);
                    txtParentModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutDriverModule.getVisibility() == View.VISIBLE) {
                    layoutDriverModule.setVisibility(View.GONE);
                    txtDriverModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutAppModule.getVisibility() == View.VISIBLE) {
                    layoutAppModule.setVisibility(View.GONE);
                    txtAppModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutTeacherModule.getVisibility() == View.VISIBLE)
                    txtTeacherModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_up), null);
                else
                    txtTeacherModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);

                break;
            case R.id.card_parent_module:

                layoutParentModule.setVisibility(layoutParentModule.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                if (layoutAdminModule.getVisibility() == View.VISIBLE) {
                    layoutAdminModule.setVisibility(View.GONE);
                    txtAdminModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutTeacherModule.getVisibility() == View.VISIBLE) {
                    layoutTeacherModule.setVisibility(View.GONE);
                    txtTeacherModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutDriverModule.getVisibility() == View.VISIBLE) {
                    layoutDriverModule.setVisibility(View.GONE);
                    txtDriverModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutAppModule.getVisibility() == View.VISIBLE) {
                    layoutAppModule.setVisibility(View.GONE);
                    txtAppModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutParentModule.getVisibility() == View.VISIBLE)
                    txtParentModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_up), null);
                else
                    txtParentModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);

                break;
            case R.id.card_driver_module:

                layoutDriverModule.setVisibility(layoutDriverModule.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                if (layoutAdminModule.getVisibility() == View.VISIBLE) {
                    layoutAdminModule.setVisibility(View.GONE);
                    txtAdminModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutTeacherModule.getVisibility() == View.VISIBLE) {
                    layoutTeacherModule.setVisibility(View.GONE);
                    txtTeacherModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutParentModule.getVisibility() == View.VISIBLE) {
                    layoutParentModule.setVisibility(View.GONE);
                    txtParentModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutAppModule.getVisibility() == View.VISIBLE) {
                    layoutAppModule.setVisibility(View.GONE);
                    txtAppModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutDriverModule.getVisibility() == View.VISIBLE)
                    txtDriverModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_up), null);
                else
                    txtDriverModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);

                break;
            case R.id.card_app_module:

                layoutAppModule.setVisibility(layoutAppModule.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                if (layoutAdminModule.getVisibility() == View.VISIBLE) {
                    layoutAdminModule.setVisibility(View.GONE);
                    txtAdminModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutTeacherModule.getVisibility() == View.VISIBLE) {
                    layoutTeacherModule.setVisibility(View.GONE);
                    txtTeacherModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutParentModule.getVisibility() == View.VISIBLE) {
                    layoutParentModule.setVisibility(View.GONE);
                    txtParentModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (txtDriverModule.getVisibility() == View.VISIBLE) {
                    layoutDriverModule.setVisibility(View.GONE);
                    txtDriverModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutAppModule.getVisibility() == View.VISIBLE)
                    txtAppModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_up), null);
                else
                    txtAppModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);

                break;
            case R.id.btn_next:
                setUpViews(false, true, false, false, false);
                break;
            case R.id.btn_teacher_previous:
                setUpViews(true, false, false, false, false);
                break;
            case R.id.btn_teacher_next:
                setUpViews(false, false, true, false, false);
                break;
            case R.id.btn_parent_previous:
                setUpViews(false, true, false, false, false);
                break;
            case R.id.btn_parent_next:
                setUpViews(false, false, false, true, false);
                break;
            case R.id.btn_driver_previous:
                setUpViews(false, false, true, false, false);
                break;
            case R.id.btn_driver_next:
                setUpViews(false, false, false, false, true);
                break;
            case R.id.btn_app_previous:
                setUpViews(false, false, false, true, false);
                break;
            case R.id.btn_update:
                break;
        }
    }


    private void setUpViews(boolean isAdminModuleVisible, boolean isTeacherModuleVisible, boolean isParentModuleVisible, boolean isDriverModuleVisible, boolean isAppModuleVisible) {


        layoutAdminModule.setVisibility(isAdminModuleVisible ? View.VISIBLE : View.GONE);
        layoutTeacherModule.setVisibility(isTeacherModuleVisible ? View.VISIBLE : View.GONE);
        layoutParentModule.setVisibility(isParentModuleVisible ? View.VISIBLE : View.GONE);
        layoutDriverModule.setVisibility(isDriverModuleVisible ? View.VISIBLE : View.GONE);
        layoutAppModule.setVisibility(isAppModuleVisible ? View.VISIBLE : View.GONE);


        if (isAdminModuleVisible) {
            txtAdminModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_up), null);
        } else {
            txtAdminModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
        }

        if (isTeacherModuleVisible) {
            txtTeacherModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_up), null);
        } else {
            txtTeacherModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
        }

        if (isParentModuleVisible) {
            txtParentModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_up), null);
        } else {
            txtParentModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
        }

        if (isDriverModuleVisible) {
            txtDriverModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_up), null);
        } else {
            txtDriverModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
        }

        if (isAppModuleVisible) {
            txtAppModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_up), null);
        } else {
            txtAppModule.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
        }
    }
}
