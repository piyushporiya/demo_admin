/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.configuration.school_info.adapter;


import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.MonthYearPicker;
import ca.dataready.SmartSchoolAdmin.classevents.create.CreateClassActivity;
import ca.dataready.SmartSchoolAdmin.server.SchoolTimingModel;
import ca.dataready.SmartSchoolAdmin.userinfo.teacher.add.AddTeacherActivity;

public class SchoolTimingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<SchoolTimingModel> beans;
    private MonthYearPicker myp;
    private Calendar myCalendar = Calendar.getInstance();
    private String[] items = {"Yes", "No"};


    public SchoolTimingAdapter(Context context) {
        this.context = context;
        beans = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_school_time, parent, false));
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        final SchoolTimingModel bean = beans.get(position);

        if (holder instanceof ItemViewHolder) {


            ((ItemViewHolder) holder).etName.setText(bean.getName());
            ((ItemViewHolder) holder).etStartDate.setText(bean.getStartDate());
            ((ItemViewHolder) holder).etEndDate.setText(bean.getEndDate());


            /*((ItemViewHolder) holder).etTermsName.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    bean.setTermName(charSequence.toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });


            ((ItemViewHolder) holder).etTermsDesc.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    bean.setTermDesc(charSequence.toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

*/

            ((ItemViewHolder) holder).etStartDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setDate(((ItemViewHolder) holder).etStartDate);
                }
            });

            ((ItemViewHolder) holder).etEndDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setDate(((ItemViewHolder) holder).etEndDate);
                }
            });

            //mon
            ((ItemViewHolder) holder).etStartTimeMon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setTime(((ItemViewHolder) holder).etStartTimeMon);
                }
            });

            ((ItemViewHolder) holder).etEndTimeMon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setTime(((ItemViewHolder) holder).etEndTimeMon);
                }
            });

            ((ItemViewHolder) holder).etIsHolidayMon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setHoliday(((ItemViewHolder) holder).etIsHolidayMon);
                }
            });

            //tue
            ((ItemViewHolder) holder).etStartTimeTue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setTime(((ItemViewHolder) holder).etStartTimeTue);
                }
            });

            ((ItemViewHolder) holder).etEndTimeTue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setTime(((ItemViewHolder) holder).etEndTimeTue);
                }
            });

            ((ItemViewHolder) holder).etIsHolidayTue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setHoliday(((ItemViewHolder) holder).etIsHolidayTue);
                }
            });

            //wed
            ((ItemViewHolder) holder).etStartTimeWed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setTime(((ItemViewHolder) holder).etStartTimeWed);
                }
            });

            ((ItemViewHolder) holder).etEndTimeWed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setTime(((ItemViewHolder) holder).etEndTimeWed);
                }
            });

            ((ItemViewHolder) holder).etIsHolidayWed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setHoliday(((ItemViewHolder) holder).etIsHolidayWed);
                }
            });


            //thu
            ((ItemViewHolder) holder).etStartTimeThu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setTime(((ItemViewHolder) holder).etStartTimeThu);
                }
            });

            ((ItemViewHolder) holder).etEndTimeThu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setTime(((ItemViewHolder) holder).etEndTimeThu);
                }
            });

            ((ItemViewHolder) holder).etIsHolidayThu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setHoliday(((ItemViewHolder) holder).etIsHolidayThu);
                }
            });

            //fri
            ((ItemViewHolder) holder).etStartTimeFri.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setTime(((ItemViewHolder) holder).etStartTimeFri);
                }
            });

            ((ItemViewHolder) holder).etEndTimeFri.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setTime(((ItemViewHolder) holder).etEndTimeFri);
                }
            });

            ((ItemViewHolder) holder).etIsHolidayFri.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setHoliday(((ItemViewHolder) holder).etIsHolidayFri);
                }
            });

            //sat
            ((ItemViewHolder) holder).etStartTimeSat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setTime(((ItemViewHolder) holder).etStartTimeSat);
                }
            });

            ((ItemViewHolder) holder).etEndTimeSat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setTime(((ItemViewHolder) holder).etEndTimeSat);
                }
            });

            ((ItemViewHolder) holder).etIsHolidaySat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setHoliday(((ItemViewHolder) holder).etIsHolidaySat);
                }
            });

            //sun
            ((ItemViewHolder) holder).etStartTimeSun.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setTime(((ItemViewHolder) holder).etStartTimeSun);
                }
            });

            ((ItemViewHolder) holder).etEndTimeSun.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setTime(((ItemViewHolder) holder).etEndTimeSun);
                }
            });

            ((ItemViewHolder) holder).etIsHolidaySun.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setHoliday(((ItemViewHolder) holder).etIsHolidaySun);
                }
            });


            ((ItemViewHolder) holder).imgRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    beans.remove(bean);
                    notifyDataSetChanged();
                }
            });

        }

    }

    public void setTime(final EditText editText) {

        int hour = myCalendar.get(Calendar.HOUR_OF_DAY);
        int minute = myCalendar.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(context, R.style.datepickerCustom, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                String AM_PM;
                if (selectedHour < 12) {
                    AM_PM = "AM";
                } else {
                    AM_PM = "PM";
                    selectedHour = selectedHour % 12;
                    if (selectedHour == 0) {
                        selectedHour = 12;
                    }
                }

                if (selectedHour < 10 && selectedMinute < 10) {
                    editText.setText(String.valueOf("0" + selectedHour) + ":" + String.valueOf("0" + selectedMinute) + " " + AM_PM);
                } else if (selectedHour < 10 || selectedMinute < 10) {
                    if (selectedHour < 10) {
                        editText.setText(String.valueOf("0" + selectedHour) + ":" + String.valueOf(selectedMinute) + " " + AM_PM);
                    } else {
                        editText.setText(String.valueOf(selectedHour) + ":" + String.valueOf("0" + selectedMinute) + " " + AM_PM);
                    }
                } else {
                    editText.setText(String.valueOf(selectedHour) + ":" + String.valueOf(selectedMinute) + " " + AM_PM);
                }
            }
        }, hour, minute, true);//Yes 24 hour time
        //  mTimePicker.setTitle(getString(R.string.select_time_label));
        mTimePicker.show();
    }

    public void setDate(final EditText editText) {

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.US);
                editText.setText(sdf.format(myCalendar.getTime()));

            }

        };

        DatePickerDialog dialog = new DatePickerDialog(context, R.style.datepickerCustom, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        dialog.show();
    }

    private void setHoliday(final EditText editText) {

        new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.is_holiday))
                .setSingleChoiceItems(items, 0, null)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                        editText.setText(items[selectedPosition]);
                    }
                })
                .show();
    }

    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }

    public void addItem(ArrayList<SchoolTimingModel> results) {

        beans.addAll(results);

        notifyDataSetChanged();
    }

    private void setMonthYear(final EditText editText) {


        myp = new MonthYearPicker(((AddTeacherActivity) context));
        myp.build(new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                editText.setText(myp.getSelectedMonthName() + " " + myp.getSelectedYear());

            }
        }, null);
        myp.show();

    }

    public ArrayList<SchoolTimingModel> getTermsData() {

        return beans;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.et_name)
        EditText etName;
        @BindView(R.id.img_remove)
        ImageView imgRemove;
        @BindView(R.id.et_start_date)
        EditText etStartDate;
        @BindView(R.id.et_end_date)
        EditText etEndDate;
        @BindView(R.id.et_start_time_mon)
        EditText etStartTimeMon;
        @BindView(R.id.et_end_time_mon)
        EditText etEndTimeMon;
        @BindView(R.id.et_is_holiday_mon)
        EditText etIsHolidayMon;
        @BindView(R.id.et_start_time_tue)
        EditText etStartTimeTue;
        @BindView(R.id.et_end_time_tue)
        EditText etEndTimeTue;
        @BindView(R.id.et_is_holiday_tue)
        EditText etIsHolidayTue;
        @BindView(R.id.et_start_time_wed)
        EditText etStartTimeWed;
        @BindView(R.id.et_end_time_wed)
        EditText etEndTimeWed;
        @BindView(R.id.et_is_holiday_wed)
        EditText etIsHolidayWed;
        @BindView(R.id.et_start_time_thu)
        EditText etStartTimeThu;
        @BindView(R.id.et_end_time_thu)
        EditText etEndTimeThu;
        @BindView(R.id.et_is_holiday_thu)
        EditText etIsHolidayThu;
        @BindView(R.id.et_start_time_fri)
        EditText etStartTimeFri;
        @BindView(R.id.et_end_time_fri)
        EditText etEndTimeFri;
        @BindView(R.id.et_is_holiday_fri)
        EditText etIsHolidayFri;
        @BindView(R.id.et_start_time_sat)
        EditText etStartTimeSat;
        @BindView(R.id.et_end_time_sat)
        EditText etEndTimeSat;
        @BindView(R.id.et_is_holiday_sat)
        EditText etIsHolidaySat;
        @BindView(R.id.et_start_time_sun)
        EditText etStartTimeSun;
        @BindView(R.id.et_end_time_sun)
        EditText etEndTimeSun;
        @BindView(R.id.et_is_holiday_sun)
        EditText etIsHolidaySun;
       // R.layout.raw_school_time

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
