/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.configuration.school_info;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.LatLng;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.Utilities.place.PlaceAPIAdapter;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.configuration.school_info.adapter.SchoolTimingAdapter;
import ca.dataready.SmartSchoolAdmin.configuration.school_info.adapter.TermsAdapter;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.AppApi;
import ca.dataready.SmartSchoolAdmin.server.CommonResponse;
import ca.dataready.SmartSchoolAdmin.server.GetSchoolInfo;
import ca.dataready.SmartSchoolAdmin.server.SchoolTimingModel;
import ca.dataready.SmartSchoolAdmin.server.UpdateSchoolInfoParams;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.ImageUpload;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


public class SchoolInfoFragment extends Fragment {


    Unbinder unbinder;
    @BindView(R.id.txtSchoolInfo)
    TextView txtSchoolInfo;
    @BindView(R.id.card_school_info)
    CardView cardSchoolInfo;
    @BindView(R.id.et_school_name)
    EditText etSchoolName;
    @BindView(R.id.et_address_line_1)
    EditText etAddressLine1;
    @BindView(R.id.et_address_line_2)
    EditText etAddressLine2;
    @BindView(R.id.et_address_line_3)
    EditText etAddressLine3;
    @BindView(R.id.et_city)
    EditText etCity;
    @BindView(R.id.et_state)
    EditText etState;
    @BindView(R.id.et_pin_code)
    EditText etPinCode;
    @BindView(R.id.school_profile_pic)
    ImageView schoolProfilePic;
    @BindView(R.id.linaer_capture_image)
    LinearLayout linaerCaptureImage;
    @BindView(R.id.btn_edit)
    Button btnEdit;
    @BindView(R.id.btn_next)
    Button btnNext;
    @BindView(R.id.layout_school_info)
    CardView layoutSchoolInfo;
    @BindView(R.id.txt_terms)
    TextView txtTerms;
    @BindView(R.id.add_more_terms)
    ImageView addMoreTerms;
    @BindView(R.id.card_terms)
    CardView cardTerms;
    @BindView(R.id.rv_terms)
    RecyclerView rvTerms;
    @BindView(R.id.btn_tr_previous)
    Button btnTrPrevious;
    @BindView(R.id.btn_tr_next)
    Button btnTrNext;
    @BindView(R.id.layout_terms)
    CardView layoutTerms;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.txt_error)
    TextView txtError;
    @BindView(R.id.viewAnimator)
    ViewAnimator viewAnimator;
    @BindView(R.id.txtSchoolTiming)
    TextView txtSchoolTiming;
    @BindView(R.id.card_school_timing)
    CardView cardSchoolTiming;
    /*   @BindView(R.id.et_st_start_time)
       EditText etStStartTime;
       @BindView(R.id.et_st_end_time)
       EditText etStEndTime;
       @BindView(R.id.et_period_frequncy)
       EditText etPeriodFrequncy;*/
    @BindView(R.id.btn_st_previous)
    Button btnStPrevious;
    @BindView(R.id.btn_st_next)
    Button btnStNext;
    @BindView(R.id.layout_school_timing)
    CardView layoutSchoolTiming;
    @BindView(R.id.txtBusInfo)
    TextView txtBusInfo;
    @BindView(R.id.card_bus_info)
    CardView cardBusInfo;
    @BindView(R.id.et_bus_stand_address)
    AutoCompleteTextView etBusStandAddress;
    @BindView(R.id.btn_bus_previous)
    Button btnBusPrevious;
    @BindView(R.id.btn_bus_update)
    Button btnBusUpdate;
    @BindView(R.id.layout_bus_info)
    CardView layoutBusInfo;
    TermsAdapter termsAdapter;
    SchoolTimingAdapter schoolTimingAdapter;
    @BindView(R.id.img_bus_address)
    ImageView imgBusAddress;
    @BindView(R.id.add_more_school_timing)
    ImageView addMoreSchoolTiming;
    @BindView(R.id.rv_school_timing)
    RecyclerView rvSchoolTiming;
    private ArrayList<UpdateSchoolInfoParams.TermDetailsBean> termsList;
    private Call<GetSchoolInfo> call;
    private Calendar myCalendar = Calendar.getInstance();
    private String picturePath;
    private String schoolPicURL;
    private Call<CommonResponse> updateCall;
    private GetSchoolInfo.ResultBean result;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_school_info, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Init();
    }

    private void Init() {

        rvTerms.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvTerms.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));

        rvSchoolTiming.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvSchoolTiming.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));

        termsAdapter = new TermsAdapter(getActivity());
        rvTerms.setAdapter(termsAdapter);

        schoolTimingAdapter = new SchoolTimingAdapter(getActivity());
        rvSchoolTiming.setAdapter(schoolTimingAdapter);

        etBusStandAddress.setAdapter(new PlaceAPIAdapter(getActivity(), R.layout.autocomplete_list_item));
        etBusStandAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String address = (String) parent.getItemAtPosition(position);
                try {
                    getLocationFromAddress(imgBusAddress, address);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


       /* etBusStandAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().length() != 0 && editable.toString().length() % 6 == 0) {

                    getLocationFromAddress(imgBusAddress, editable.toString());

                }
            }
        });*/

        getSchoolInfo();
    }


    private void getSchoolInfo() {

        call = AdminApp.getInstance().getApi().getSchoolInfo(AdminApp.getInstance().getAdmin().getSchoolId());
        call.enqueue(new Callback<GetSchoolInfo>() {
            @Override
            public void onResponse(Call<GetSchoolInfo> call, Response<GetSchoolInfo> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            result = response.body().getResult();
                            if (result != null) {
                                setValues(result);
                            } else {
                                viewAnimator.setDisplayedChild(2);
                                txtError.setText(response.body().getMessage());
                            }
                        } else {

                            viewAnimator.setDisplayedChild(2);
                            txtError.setText(response.body().getMessage());
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_GET_SCHOOL_INFO);
                        viewAnimator.setDisplayedChild(2);
                        txtError.setText(error.message());
                    }
                } catch (Exception e) {

                    e.printStackTrace();
                    viewAnimator.setDisplayedChild(2);
                    txtError.setText(getString(R.string.somethingwrong));

                }
            }

            @Override
            public void onFailure(Call<GetSchoolInfo> call, Throwable t) {

                if (!call.isCanceled()) {

                    viewAnimator.setDisplayedChild(2);
                    txtError.setText(App_Constants.NO_INTERNET);
                }
            }
        });
    }

    private void setValues(GetSchoolInfo.ResultBean bean) {

        etSchoolName.setText(bean.getSchoolName());
        linaerCaptureImage.setVisibility(View.GONE);
        Glide.with(getActivity())
                .load(AppApi.BASE_URL + bean.getProfilePic())
                .into(schoolProfilePic);
        etAddressLine1.setText(bean.getAddrLine1());
        etAddressLine2.setText(bean.getAddrLine2());
        etAddressLine3.setText(bean.getAddrLine3());
        etCity.setText(bean.getCity());
        etState.setText(bean.getState());
        etPinCode.setText(bean.getPin());

        if (bean.getTermDetails() != null) {

            List<GetSchoolInfo.ResultBean.TermDetailsBean> termDetails = bean.getTermDetails();

            if (termDetails != null && termDetails.size() > 0) {

                ArrayList<UpdateSchoolInfoParams.TermDetailsBean> models = new ArrayList<>();

                for (GetSchoolInfo.ResultBean.TermDetailsBean termsBean : termDetails) {

                    UpdateSchoolInfoParams.TermDetailsBean model = new UpdateSchoolInfoParams.TermDetailsBean();
                    model.setTermName(termsBean.getTermName());
                    model.setTermDesc(termsBean.getTermDesc());
                    model.setTermId(termsBean.getTermId());
                    models.add(model);
                }

                if (models.size() > 0) {
                    termsAdapter.addItem(models);
                }
            }
        }

       /* if (bean.getSchoolTiming() != null) {
            etStStartTime.setText(bean.getSchoolTiming().getStartTime());
            etStEndTime.setText(bean.getSchoolTiming().getEndTime());
            etPeriodFrequncy.setText(bean.getSchoolTiming().getPeriodFrequency());
        }*/

        if (bean.getBusStandInfo() != null) {
            etBusStandAddress.setText(bean.getBusStandInfo().getAddress());

            if (bean.getBusStandInfo().getGeopoint() != null && !bean.getBusStandInfo().getGeopoint().isEmpty()) {

                if (bean.getBusStandInfo().getGeopoint().contains(",")) {

                    double latitude = Double.parseDouble(bean.getBusStandInfo().getGeopoint().split(",")[0].trim());
                    double longitude = Double.parseDouble(bean.getBusStandInfo().getGeopoint().split(",")[1].trim());
                    Glide.with(getActivity())
                            .load(MapImageUrl(600, 150, latitude, longitude))
                            .into(imgBusAddress);
                }
            }
        }


        viewAnimator.setDisplayedChild(1);
    }


    @OnClick({R.id.card_school_info, R.id.card_terms, R.id.card_school_timing, R.id.card_bus_info,
            R.id.btn_edit, R.id.btn_next, R.id.add_more_terms, R.id.add_more_school_timing, R.id.btn_tr_previous, R.id.btn_tr_next
            , R.id.btn_st_previous, R.id.btn_st_next, R.id.btn_bus_previous, R.id.btn_bus_update/*, R.id.et_st_start_time, R.id.et_st_end_time*/})
    public void onViewClicked(View view) {
        switch (view.getId()) {

         /*   case R.id.et_st_start_time:

                setTime(etStStartTime);

                break;

            case R.id.et_st_end_time:

                setTime(etStEndTime);

                break;*/

            case R.id.card_school_info:

                layoutSchoolInfo.setVisibility(layoutSchoolInfo.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                if (layoutTerms.getVisibility() == View.VISIBLE) {
                    layoutTerms.setVisibility(View.GONE);
                    txtTerms.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutSchoolTiming.getVisibility() == View.VISIBLE) {
                    layoutSchoolTiming.setVisibility(View.GONE);
                    txtSchoolTiming.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutBusInfo.getVisibility() == View.VISIBLE) {
                    layoutBusInfo.setVisibility(View.GONE);
                    txtBusInfo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutSchoolInfo.getVisibility() == View.VISIBLE)
                    txtSchoolInfo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_up), null);
                else
                    txtSchoolInfo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);

                addMoreTerms.setVisibility(layoutTerms.getVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);
                addMoreSchoolTiming.setVisibility(layoutSchoolTiming.getVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);

                break;

            case R.id.card_terms:

                layoutTerms.setVisibility(layoutTerms.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                if (layoutSchoolInfo.getVisibility() == View.VISIBLE) {
                    layoutSchoolInfo.setVisibility(View.GONE);
                    txtSchoolInfo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutSchoolTiming.getVisibility() == View.VISIBLE) {
                    layoutSchoolTiming.setVisibility(View.GONE);
                    txtSchoolTiming.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutBusInfo.getVisibility() == View.VISIBLE) {
                    layoutBusInfo.setVisibility(View.GONE);
                    txtBusInfo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutTerms.getVisibility() == View.VISIBLE)
                    txtTerms.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_up), null);
                else
                    txtTerms.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);

                addMoreTerms.setVisibility(layoutTerms.getVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);
                addMoreSchoolTiming.setVisibility(layoutSchoolTiming.getVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);

                break;

            case R.id.card_school_timing:

                layoutSchoolTiming.setVisibility(layoutSchoolTiming.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                if (layoutSchoolInfo.getVisibility() == View.VISIBLE) {
                    layoutSchoolInfo.setVisibility(View.GONE);
                    txtSchoolInfo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutTerms.getVisibility() == View.VISIBLE) {
                    layoutTerms.setVisibility(View.GONE);
                    txtTerms.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutBusInfo.getVisibility() == View.VISIBLE) {
                    layoutBusInfo.setVisibility(View.GONE);
                    txtBusInfo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutSchoolTiming.getVisibility() == View.VISIBLE)
                    txtSchoolTiming.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_up), null);
                else
                    txtSchoolTiming.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);

                addMoreTerms.setVisibility(layoutTerms.getVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);
                addMoreSchoolTiming.setVisibility(layoutSchoolTiming.getVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);

                break;
            case R.id.card_bus_info:

                layoutBusInfo.setVisibility(layoutBusInfo.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                if (layoutSchoolInfo.getVisibility() == View.VISIBLE) {
                    layoutSchoolInfo.setVisibility(View.GONE);
                    txtSchoolInfo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutTerms.getVisibility() == View.VISIBLE) {
                    layoutTerms.setVisibility(View.GONE);
                    txtTerms.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutSchoolTiming.getVisibility() == View.VISIBLE) {
                    layoutSchoolTiming.setVisibility(View.GONE);
                    txtSchoolTiming.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
                }

                if (layoutBusInfo.getVisibility() == View.VISIBLE)
                    txtBusInfo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_up), null);
                else
                    txtBusInfo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);

                addMoreTerms.setVisibility(layoutTerms.getVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);
                addMoreSchoolTiming.setVisibility(layoutSchoolTiming.getVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);

                break;
            case R.id.btn_next:

                setUpViews(false, true, false, false);

                break;

            case R.id.btn_tr_next:

                ArrayList<UpdateSchoolInfoParams.TermDetailsBean> results = termsAdapter.getTermsData();
                termsList = new ArrayList<>();
                if (results != null && results.size() > 0) {

                    for (UpdateSchoolInfoParams.TermDetailsBean bean : results) {

                        Log.e("DATA", "TermName " + bean.getTermName()
                                + " ,TermDesc " + bean.getTermDesc() + " - " + bean.getTermId());

                        if (bean.getTermName().isEmpty()) {
                            Toast.makeText(getActivity(), getString(R.string.please_enter_term_name), Toast.LENGTH_SHORT).show();
                            break;
                        }

                        if (bean.getTermDesc().isEmpty()) {
                            Toast.makeText(getActivity(), getString(R.string.please_enter_term_desc), Toast.LENGTH_SHORT).show();
                            break;
                        }

                        termsList.add(bean);
                    }

                    if (termsList != null && termsList.size() > 0) {

                        setUpViews(false, false, true, false);
                    }

                } else {

                    setUpViews(false, false, true, false);
                }

                break;
            case R.id.btn_tr_previous:

                setUpViews(true, false, false, false);

                break;

            case R.id.btn_st_next:

                setUpViews(false, false, false, true);

                break;

            case R.id.btn_st_previous:

                setUpViews(false, true, false, false);

                break;

            case R.id.btn_bus_update:

                validateData();

                break;

            case R.id.btn_bus_previous:

                setUpViews(false, false, true, false);

                break;

            case R.id.btn_edit:

                selectPhoto();

                break;

            case R.id.linaer_capture_image:

                selectPhoto();

                break;

            case R.id.add_more_terms:

                ArrayList<UpdateSchoolInfoParams.TermDetailsBean> termsBeans = new ArrayList<>();

                UpdateSchoolInfoParams.TermDetailsBean termBean = new UpdateSchoolInfoParams.TermDetailsBean();
                termBean.setTermName(App_Constants.EMPTY);
                termBean.setTermDesc(App_Constants.EMPTY);
                termBean.setTermId(App_Constants.EMPTY);

                termsBeans.add(termBean);

                termsAdapter.addItem(termsBeans);


                break;

            case R.id.add_more_school_timing:

                ArrayList<SchoolTimingModel> expBeans = new ArrayList<>();

                SchoolTimingModel schoolTimingBean = new SchoolTimingModel();
                schoolTimingBean.setName(App_Constants.EMPTY);
                schoolTimingBean.setStartDate(App_Constants.EMPTY);
                schoolTimingBean.setEndDate(App_Constants.EMPTY);

                SchoolTimingModel.TimeTableBean ttbean = new SchoolTimingModel.TimeTableBean();
                ttbean.setStartTime(App_Constants.EMPTY);
                ttbean.setEndTime(App_Constants.EMPTY);
                ttbean.setIsHoliday(App_Constants.EMPTY);
                schoolTimingBean.setTimeTableBean(ttbean);

                expBeans.add(schoolTimingBean);

                schoolTimingAdapter.addItem(expBeans);


                break;
        }
    }

    private void validateData() {

        //updateSchoolInfo();

    }

    private void updateSchoolInfo() {

        Utility.showProgress(getActivity(), getString(R.string.processing));

        UpdateSchoolInfoParams params = new UpdateSchoolInfoParams();
        params.setCityId(result.getCityId());
        params.setCountryId(result.getCountryId());
        params.setEmailId(result.getEmailID());
        params.setGeopoint(result.getGeopoint());
        params.setPhoneNo(result.getPhoneNo());
        params.setProfilePic(schoolPicURL != null ? schoolPicURL : result.getProfilePic());
        params.setRegionId(result.getRegionId());
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
        params.setSchoolName(AdminApp.getInstance().getAdmin().getSchoolName());
        params.setStateId(result.getStateId());

       /* UpdateSchoolInfoParams.SchoolTimingBean schoolTimingBean = new UpdateSchoolInfoParams.SchoolTimingBean();
        schoolTimingBean.setStartTime(etStStartTime.getText().toString());
        schoolTimingBean.setEndTime(etStEndTime.getText().toString());
        schoolTimingBean.setPeriodFrequency(etPeriodFrequncy.getText().toString());
        params.setSchoolTiming(schoolTimingBean);*/

        UpdateSchoolInfoParams.BusStandInfoBean busStandInfoBean = new UpdateSchoolInfoParams.BusStandInfoBean();
        busStandInfoBean.setAddress(etBusStandAddress.getText().toString());
        LatLng location = getLatLng(etBusStandAddress.getText().toString());
        busStandInfoBean.setGeopoint(location.latitude + "," + location.longitude);
        params.setBusStandInfo(busStandInfoBean);

        UpdateSchoolInfoParams.SchoolAddressBean schoolAddressBean = new UpdateSchoolInfoParams.SchoolAddressBean();
        schoolAddressBean.setAddrLine1(etAddressLine1.getText().toString());
        schoolAddressBean.setAddrLine2(etAddressLine2.getText().toString());
        schoolAddressBean.setAddrLine3(etAddressLine3.getText().toString());
        schoolAddressBean.setCity(etCity.getText().toString());
        schoolAddressBean.setPin(etPinCode.getText().toString());
        schoolAddressBean.setState(etState.getText().toString());
        params.setSchoolAddress(schoolAddressBean);
        params.setTermDetails(termsList);

        updateCall = AdminApp.getInstance().getApi().updateSchoolInfo(params);
        updateCall.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_UPDATE_SCHOOL_INFO);
                        Toast.makeText(getActivity(), error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(getActivity(), getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(getActivity(), App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    private void selectPhoto() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_image_picker_dialog);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setLayout(getScreenWidth(getActivity()) / 2, LinearLayoutCompat.LayoutParams.WRAP_CONTENT);
        }
        dialog.setCancelable(true);
        dialog.show();

        Button gallery = (Button) dialog.findViewById(R.id.bGallery);
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                galleryIntent();

            }
        });

        Button camera = (Button) dialog.findViewById(R.id.bCamera);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                cameraIntent();

            }
        });

    }

    public int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    }

    private void galleryIntent() {

        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, 1);
    }

    public void cameraIntent() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 0);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == APIError.UPDATE_TOKEN && resultCode == RESULT_OK) {

            if (data != null) {
                String apiName = data.getStringExtra("api");
                onRelaod(apiName);
            }

        } else if (requestCode == 0 && resultCode == RESULT_OK) {

            Bitmap imageBitmap = null;
            Bundle extras = data.getExtras();
            if (extras != null) {
                imageBitmap = (Bitmap) extras.get("data");
            }

            Uri tempUri = getImageUri(getActivity(), imageBitmap);
            File finalFile = new File(getRealPathFromURI(tempUri));
            picturePath = finalFile.toString();

            linaerCaptureImage.setVisibility(View.GONE);
            schoolProfilePic.setImageURI(Uri.fromFile(finalFile));

            System.out.println(finalFile);

            UploadPic();

        } else if (requestCode == 1 && resultCode == RESULT_OK) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getActivity().getContentResolver()
                    .query(selectedImage, filePathColumn, null, null,
                            null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);

            linaerCaptureImage.setVisibility(View.GONE);
            schoolProfilePic.setImageURI(selectedImage);

            cursor.close();

            System.out.println(selectedImage);

            UploadPic();

        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void UploadPic() {

        Utility.showProgress(getActivity(), getString(R.string.processing));

        File file = new File(picturePath);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        Call<ImageUpload> call = AdminApp.getInstance().getApi().upload(body);

        call.enqueue(new Callback<ImageUpload>() {
            @Override
            public void onResponse(Call<ImageUpload> call, Response<ImageUpload> response) {

                try {
                    if (response.isSuccessful()) {

                        if (response.body().isStatus()) {
                            if (response.body().getResult() != null) {
                                schoolPicURL = response.body().getResult().getFilepath();
                                System.out.println(schoolPicURL);
                                Utility.hideProgress();
                            } else {
                                Utility.hideProgress();
                                Toast.makeText(getActivity(), getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Utility.hideProgress();
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_UPLOAD);
                        Utility.error(getActivity(), error.message());
                    }
                } catch (Exception e) {
                    Utility.hideProgress();
                    e.printStackTrace();
                    Toast.makeText(getActivity(), getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<ImageUpload> call, Throwable t) {
                Utility.hideProgress();
                Log.e("ERROR UPLOAD", "" + t.getMessage());
                Toast.makeText(getActivity(), App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
            }
        });

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }


    private void setUpViews(boolean isSchoolInfoVisible, boolean isTermsViewVisible, boolean isSchoolTimingVisible, boolean isBusInfoVisible) {


        layoutSchoolInfo.setVisibility(isSchoolInfoVisible ? View.VISIBLE : View.GONE);
        layoutTerms.setVisibility(isTermsViewVisible ? View.VISIBLE : View.GONE);
        layoutSchoolTiming.setVisibility(isSchoolTimingVisible ? View.VISIBLE : View.GONE);
        layoutBusInfo.setVisibility(isBusInfoVisible ? View.VISIBLE : View.GONE);


        if (isSchoolInfoVisible) {
            txtSchoolInfo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_up), null);
        } else {
            txtSchoolInfo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
        }

        if (isTermsViewVisible) {
            txtTerms.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_up), null);
        } else {
            txtTerms.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
        }

        if (isSchoolTimingVisible) {
            txtSchoolTiming.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_up), null);
        } else {
            txtSchoolTiming.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
        }

        if (isBusInfoVisible) {
            txtBusInfo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_up), null);
        } else {
            txtBusInfo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down), null);
        }

        addMoreTerms.setVisibility(layoutTerms.getVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);
        addMoreSchoolTiming.setVisibility(layoutSchoolTiming.getVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);

    }


    public void setTime(final EditText editText) {

        int hour = myCalendar.get(Calendar.HOUR_OF_DAY);
        int minute = myCalendar.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getActivity(), R.style.datepickerCustom, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                String AM_PM;
                if (selectedHour < 12) {
                    AM_PM = "AM";
                } else {
                    AM_PM = "PM";
                    selectedHour = selectedHour % 12;
                    if (selectedHour == 0) {
                        selectedHour = 12;
                    }
                }

                if (selectedHour < 10 && selectedMinute < 10) {
                    editText.setText(String.valueOf("0" + selectedHour) + ":" + String.valueOf("0" + selectedMinute) + " " + AM_PM);
                } else if (selectedHour < 10 || selectedMinute < 10) {
                    if (selectedHour < 10) {
                        editText.setText(String.valueOf("0" + selectedHour) + ":" + String.valueOf(selectedMinute) + " " + AM_PM);
                    } else {
                        editText.setText(String.valueOf(selectedHour) + ":" + String.valueOf("0" + selectedMinute) + " " + AM_PM);
                    }
                } else {
                    editText.setText(String.valueOf(selectedHour) + ":" + String.valueOf(selectedMinute) + " " + AM_PM);
                }
            }
        }, hour, minute, true);//Yes 24 hour time
        //  mTimePicker.setTitle(getString(R.string.select_time_label));
        mTimePicker.show();
    }

    public String MapImageUrl(int X, int Y, double lat, double lon) {
        String url = "https://maps.googleapis.com/maps/api/staticmap?zoom=15&size=" + X + "x" + Y + "&maptype=roadmap&markers=color:red%7Clabel:R%7C" + lat + "," + lon + "&sensor=false";
        Log.e("MAP URL ", "" + url);
        return url;
    }

    public void getLocationFromAddress(final ImageView imageView, String strAddress) {

        Geocoder coder = new Geocoder(getActivity());
        List<Address> addresses;

        try {
            addresses = coder.getFromLocationName(strAddress, 5);
            if (addresses == null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), "address not found", Toast.LENGTH_SHORT).show();
                    }
                });

            }

            if (addresses.size() == 0) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), "address not found", Toast.LENGTH_SHORT).show();
                    }
                });

            }

            Address location = addresses.get(0);
            if (location != null) {
                Glide.with(getActivity())
                        .load(MapImageUrl(600, 150, location.getLatitude(), location.getLongitude()))
                        .into(imageView);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    class GetLocationFromAddressAsyncTask extends AsyncTask<Void, Void, Address> {

        private final ImageView imageView;
        private final String strAddress;

        public GetLocationFromAddressAsyncTask(ImageView imageView, String strAddress) {

            this.imageView = imageView;
            this.strAddress = strAddress;
        }

        @Override
        protected Address doInBackground(Void... voids) {

            Geocoder coder = new Geocoder(getActivity());
            List<Address> addresses;

            try {
                addresses = coder.getFromLocationName(strAddress, 5);
                if (addresses == null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity(), "address not found", Toast.LENGTH_SHORT).show();
                        }
                    });
                    return null;
                }

                if (addresses.size() == 0) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity(), "address not found", Toast.LENGTH_SHORT).show();
                        }
                    });
                    return null;
                }

                return addresses.get(0);
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Address location) {
            super.onPostExecute(location);

            if (location != null) {
                Glide.with(getActivity())
                        .load(MapImageUrl(600, 150, location.getLatitude(), location.getLongitude()))
                        .into(imageView);
            }
        }
    }

    public LatLng getLatLng(String strAddress) {

        Geocoder coder = new Geocoder(getActivity());
        List<Address> addresses;

        try {
            addresses = coder.getFromLocationName(strAddress, 5);
            if (addresses == null) {
                return new LatLng(0.0, 0.0);
            }

            if (addresses.size() == 0) {
                return new LatLng(0.0, 0.0);
            }

            Address location = addresses.get(0);
            if (location != null) {
                return new LatLng(location.getLatitude(), location.getLongitude());
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            return new LatLng(0.0, 0.0);
        }

        return new LatLng(0.0, 0.0);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (updateCall != null)
            updateCall.cancel();

        if (call != null)
            call.cancel();
    }

    private void onRelaod(String apiName) {

        if (apiName != null) {

            if (apiName.equals(App_Constants.API_UPDATE_SCHOOL_INFO)) {

                validateData();

            } else if (apiName.equals(App_Constants.API_GET_SCHOOL_INFO)) {

                getSchoolInfo();

            }
        }
    }

}
