/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.configuration.master_table;

import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.ViewAnimator;

import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.LastItemDecoration;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.configuration.master_table.adapter.MasterTableAdapter;
import ca.dataready.SmartSchoolAdmin.grade.assign.time_table.adapter.TimeTableAdapter;
import ca.dataready.SmartSchoolAdmin.server.GetAssignedTeacherList;
import ca.dataready.SmartSchoolAdmin.server.GradeListResponse;
import ca.dataready.SmartSchoolAdmin.server.MasterTableModel;
import ca.dataready.SmartSchoolAdmin.server.TimeTableModel;
import retrofit2.Call;


public class MasterTableFragment extends Fragment {

    Unbinder unbinder;
    List<TimeTableModel> list;
    @BindView(R.id.et_parent_refresh_frequncy)
    EditText etParentRefreshFrequncy;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.txt_error)
    TextView txtError;
    @BindView(R.id.viewAnimator)
    ViewAnimator viewAnimator;
    @BindView(R.id.txt_add_more)
    TextView txtAddMore;
    private MasterTableAdapter adapter;
    private Call<GetAssignedTeacherList> appApi;
    private List<GetAssignedTeacherList.ResultBean> results;
    private Calendar myCalendar = Calendar.getInstance();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_master_table, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewAnimator.setDisplayedChild(2);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new MasterTableAdapter(getActivity(),MasterTableFragment.this);
        recyclerView.setAdapter(adapter);

    }

    @OnClick(R.id.txt_add_more)
    public void onViewClicked() {


        addPeriodDialog();

    }

    private void addPeriodDialog() {

        final View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_add_period, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.add_period);
        builder.setView(view);
        builder.setCancelable(true);

        final EditText etStartTime = (EditText) view.findViewById(R.id.et_start_time);
        final EditText etEndTime = (EditText) view.findViewById(R.id.et_end_time);
        final CheckBox checkBox = (CheckBox) view.findViewById(R.id.chk);

        etStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTime(etStartTime);
            }
        });

        etEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTime(etEndTime);

            }
        });

        builder.setPositiveButton(R.string.submit, null);

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialogInterface) {

                Button button = ((AlertDialog) alertDialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {

                        if(etStartTime.getText().toString().isEmpty()){
                            etStartTime.setError(getString(R.string.field_required));
                            return;
                        }

                        if(etEndTime.getText().toString().isEmpty()){
                            etEndTime.setError(getString(R.string.field_required));
                            return;
                        }

                        try{

                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
                            Date date1 = simpleDateFormat.parse(etStartTime.getText().toString());
                            Date date2 = simpleDateFormat.parse(etEndTime.getText().toString());
                            list = new ArrayList<>();

                            long difference = (date2.getTime() - date1.getTime()) / 1000;
                            long hours = difference % (24 * 3600) / 3600;
                            long minute = difference % 3600 / 60;
                            long min = minute + (hours * 60);

                            MasterTableModel model = new MasterTableModel();
                            model.setId(adapter.getItemCount() + 1);
                            model.setStartTime(etStartTime.getText().toString());
                            model.setEndTime(etEndTime.getText().toString());
                            model.setDuration((int) min);
                            model.setAttendance(checkBox.isChecked());
                            adapter.addSingleItem(model);
                            viewAnimator.setDisplayedChild(1);


                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        dialogInterface.dismiss();
                    }
                });
            }
        });
        alertDialog.show();
    }

    public void setTime(final EditText editText) {

        int hour = myCalendar.get(Calendar.HOUR_OF_DAY);
        int minute = myCalendar.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getActivity(), R.style.datepickerCustom, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                String AM_PM;
                if (selectedHour < 12) {
                    AM_PM = "AM";
                } else {
                    AM_PM = "PM";
                    selectedHour = selectedHour % 12;
                    if (selectedHour == 0) {
                        selectedHour = 12;
                    }
                }

                if (selectedHour < 10 && selectedMinute < 10) {
                    editText.setText(String.valueOf("0" + selectedHour) + ":" + String.valueOf("0" + selectedMinute) + " " + AM_PM);
                } else if (selectedHour < 10 || selectedMinute < 10) {
                    if (selectedHour < 10) {
                        editText.setText(String.valueOf("0" + selectedHour) + ":" + String.valueOf(selectedMinute) + " " + AM_PM);
                    } else {
                        editText.setText(String.valueOf(selectedHour) + ":" + String.valueOf("0" + selectedMinute) + " " + AM_PM);
                    }
                } else {
                    editText.setText(String.valueOf(selectedHour) + ":" + String.valueOf(selectedMinute) + " " + AM_PM);
                }

           /*     try{
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
                    Date sTime = simpleDateFormat.parse(etStartTime.getText().toString());
                    Date eTime = simpleDateFormat.parse(etEndTime.getText().toString());

                    if(eTime.before(sTime)){
                        etEndTime.getText().clear();
                        etEndTime.setError(getString(R.string.select_time_after_start_time));
                    }

                }catch (Exception e){

                    e.printStackTrace();
                }*/

            }
        }, hour, minute, true);//Yes 24 hour time
        //  mTimePicker.setTitle(getString(R.string.select_time_label));
        mTimePicker.show();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public void setEmptyView() {

        viewAnimator.setDisplayedChild(2);
    }
}
