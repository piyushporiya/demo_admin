/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.configuration;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.Listeners.OnReloadListener;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.configuration.app_config.AppLevelConfigsFragment;
import ca.dataready.SmartSchoolAdmin.configuration.master_table.MasterTableFragment;
import ca.dataready.SmartSchoolAdmin.configuration.school_info.SchoolInfoFragment;
import ca.dataready.SmartSchoolAdmin.grade.assign.time_table.TimeTableFragment;
import ca.dataready.SmartSchoolAdmin.configuration.user_config.UserLevelConfigsFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConfigurationFragment extends Fragment implements OnReloadListener {


    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_configuration, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) getActivity()).setSubTitle(getString(R.string.configuration));
        ((HomeActivity) getActivity()).setOnReloadListener(this);

        initViewPagerAndTabs();
    }

    private void initViewPagerAndTabs() {

        PagerAdapters pagerAdapter = new PagerAdapters(getChildFragmentManager());

        SchoolInfoFragment schoolInfoFragment = new SchoolInfoFragment();
        pagerAdapter.addFragment(schoolInfoFragment, getString(R.string.school_info));

        MasterTableFragment masterTableFragment = new MasterTableFragment();
        pagerAdapter.addFragment(masterTableFragment, getString(R.string.master_table));

        AppLevelConfigsFragment appLevelConfigsFragment = new AppLevelConfigsFragment();
        pagerAdapter.addFragment(appLevelConfigsFragment, getString(R.string.app_level_configs));

        UserLevelConfigsFragment userLevelConfigsFragment = new UserLevelConfigsFragment();
        pagerAdapter.addFragment(userLevelConfigsFragment, getString(R.string.user_level_configs));

        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

    }

    class PagerAdapters extends FragmentPagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public PagerAdapters(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }


    @Override
    public void onReload(String apiName) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
