/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.help.adapter;

/**
 * Created by pankaj on 01/09/16.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.server.AppApi;
import ca.dataready.SmartSchoolAdmin.server.HelpResponse;
import ca.dataready.SmartSchoolAdmin.video.VideoViewActivity;


public class HelpAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public List<HelpResponse.ResultBean> mItemList;
    private LayoutInflater mInflater;
    private Context context;

    public HelpAdapter(Context context) {
        mItemList = new ArrayList<>();
        this.context =context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ViewGroup vVideos = (ViewGroup) LayoutInflater.from(context).inflate(R.layout.raw_help, parent, false);
        return new ItemViewHolder(vVideos);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        final HelpResponse.ResultBean entity = mItemList.get(position);

        if (viewHolder instanceof ItemViewHolder) {

            ((ItemViewHolder) viewHolder).txtQuestion.setText(entity.getQestionText());

            ((ItemViewHolder) viewHolder).view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    context.startActivity(new Intent(context, VideoViewActivity.class)
                            .putExtra(VideoViewActivity.VIDEO_PATH, AppApi.BASE_URL+entity.getVideoUrl())
                            .putExtra(VideoViewActivity.FILE_NAME, entity.getVideoName()));
                }
            });
        }

    }


    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }


    public void addItems(List<HelpResponse.ResultBean> productDetails) {

        mItemList.addAll(productDetails);
        notifyDataSetChanged();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.txt_question)
        TextView txtQuestion;
        View view;
        //R.layout.raw_help

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }
}
