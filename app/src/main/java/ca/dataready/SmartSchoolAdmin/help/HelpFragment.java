/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.help;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.malinskiy.superrecyclerview.SuperRecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.Listeners.OnReloadListener;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Views.PaddingItemDecoration;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.help.adapter.HelpAdapter;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.HelpResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HelpFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, OnReloadListener {


    Unbinder unbinder;
    @BindView(R.id.recyclerView)
    SuperRecyclerView recyclerView;
    @BindView(R.id.txt_app)
    TextView txtApp;
    @BindView(R.id.txt_api)
    TextView txtApi;
    @BindView(R.id.txt_env)
    TextView txtEnv;
    private HelpAdapter adapter;
    private Call<HelpResponse> appApi;


    public HelpFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_help, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((HomeActivity) getActivity()).setSubTitle(getString(R.string.help));
        ((HomeActivity) getActivity()).setOnReloadListener(this);
        Init();

        if (AdminApp.getInstance().getConfigs() == null)
            return;

        if (AdminApp.getInstance().getConfigs().getResult() == null)
            return;

        txtApp.setText(AdminApp.getInstance().getConfigs().getResult().getAppversion() + "(" + AdminApp.getInstance().getConfigs().getResult().getAppbuildnumber() + ")");
        txtApi.setText(AdminApp.getInstance().getConfigs().getResult().getApiversion());
        txtEnv.setText(AdminApp.getInstance().getConfigs().getResult().getEnv());
    }


    private void Init() {

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new PaddingItemDecoration(10));
        recyclerView.setRefreshListener(HelpFragment.this);
        recyclerView.setRefreshingColorResources(R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary);

        ApiCall();
    }

    private void ApiCall() {

        appApi = AdminApp.getInstance().getApi().getQuestions("admin");
        appApi.enqueue(new Callback<HelpResponse>() {
            @Override
            public void onResponse(Call<HelpResponse> call, Response<HelpResponse> response) {
                try {

                    if (isVisible() && isAdded() && response.isSuccessful()) {

                        if (response.body().isStatus()) {

                            if (response.body().getResult() != null && response.body().getResult().size() > 0) {

                                adapter = new HelpAdapter(getActivity());
                                recyclerView.setAdapter(adapter);
                                adapter.addItems(response.body().getResult());

                            } else {

                                adapter = new HelpAdapter(getActivity());
                                recyclerView.setAdapter(adapter);
                                TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                                emptyView.setText(getString(R.string.no_data));
                            }

                        } else {

                            adapter = new HelpAdapter(getActivity());
                            recyclerView.setAdapter(adapter);
                            TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                            emptyView.setText(response.body().getMessage());
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_HELP);
                        adapter = new HelpAdapter(getActivity());
                        recyclerView.setAdapter(adapter);
                        TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                        emptyView.setText(error.message());

                    }
                } catch (Exception e) {

                    e.printStackTrace();
                    adapter = new HelpAdapter(getActivity());
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(getString(R.string.somethingwrong));
                }
            }

            @Override
            public void onFailure(Call<HelpResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    adapter = new HelpAdapter(getActivity());
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(App_Constants.NO_INTERNET);
                }
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onRefresh() {

        if (isVisible())
            ApiCall();
    }

    @Override
    public void onReload(String apiName) {

        if (apiName != null) {
            if (apiName.equals(App_Constants.API_HELP))
                ApiCall();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if(appApi!=null)
            appApi.cancel();
    }
}
