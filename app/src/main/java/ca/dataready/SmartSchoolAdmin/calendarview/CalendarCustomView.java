/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.calendarview;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.calendarview.adapter.GridAdapter;
import ca.dataready.SmartSchoolAdmin.calendarview.callbacks.CompactCalendarViewListener;
import ca.dataready.SmartSchoolAdmin.calendarview.callbacks.OnDayClickListener;
import ca.dataready.SmartSchoolAdmin.calendarview.callbacks.OnMonthChangeListener;
import ca.dataready.SmartSchoolAdmin.server.ClassActivityResponse;
import ca.dataready.SmartSchoolAdmin.server.SchoolActivityResponse;
import ca.dataready.SmartSchoolAdmin.server.TeacherAppointmentModel;


/**
 * Created by c161 on 09/06/17.
 */

public class CalendarCustomView extends LinearLayout {
    private static final String TAG = CalendarCustomView.class.getSimpleName();
    private ImageView previousButton, nextButton;
    private TextView currentDate;
    private ExpandableHeightGridView calendarGridView;
    private Button addEventButton;
    private static final int MAX_CALENDAR_COLUMN = 42;
    private int month, year;
    private SimpleDateFormat formatter = new SimpleDateFormat("MMMM , yyyy", Locale.ENGLISH);
    private Calendar cal = Calendar.getInstance(Locale.ENGLISH);
    private int todayMonth = Calendar.getInstance(Locale.ENGLISH).get(Calendar.MONTH);
    private int todayDay = Calendar.getInstance(Locale.ENGLISH).get(Calendar.DAY_OF_MONTH);
    private int todayYear = Calendar.getInstance(Locale.ENGLISH).get(Calendar.YEAR);
    private Context context;
    private GridAdapter mAdapter;
    private CompactCalendarViewListener compactCalendarViewListener;
    private OnMonthChangeListener onMonthChangeListener;
    private List<TeacherAppointmentModel.ResultBean> mEvents = new ArrayList<>();
    private List<SchoolActivityResponse.ResultBean> sEvents = new ArrayList<>();
    private List<ClassActivityResponse.ResultBean> cEvents = new ArrayList<>();

    //private DatabaseQuery mQuery;
    public CalendarCustomView(Context context) {
        super(context);
    }

    public CalendarCustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initializeUILayout();
        setUpCalendarAdapter();
        setPreviousButtonClickEvent();
        setNextButtonClickEvent();
        setGridCellClickEvents();
        Log.d(TAG, "I need to call this method");
    }

    public CalendarCustomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void initializeUILayout() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.calendar_view_schedule, this);
        previousButton = (ImageView) view.findViewById(R.id.bt_left_arrow);
        nextButton = (ImageView) view.findViewById(R.id.bt_right_arrow);
        currentDate = (TextView) view.findViewById(R.id.display_current_date);
        calendarGridView = (ExpandableHeightGridView) view.findViewById(R.id.calendar_grid);
        calendarGridView.setExpanded(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            calendarGridView.setNestedScrollingEnabled(false);
        }
    }

    private void setPreviousButtonClickEvent() {
        previousButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                cal.add(Calendar.MONTH, -1);
                if (onMonthChangeListener != null)
                    onMonthChangeListener.onMonthChanged(cal.get(Calendar.MONTH) + 1, cal.get(Calendar.YEAR));
                setUpCalendarAdapter();
            }
        });
    }

    private void setNextButtonClickEvent() {
        nextButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                cal.add(Calendar.MONTH, 1);
                if (onMonthChangeListener != null)
                    onMonthChangeListener.onMonthChanged(cal.get(Calendar.MONTH) + 1, cal.get(Calendar.YEAR));
                setUpCalendarAdapter();
            }
        });
    }

    private void setGridCellClickEvents() {
        calendarGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
    }

    private void setUpCalendarAdapter() {
        List<Date> dayValueInCells = new ArrayList<Date>();

        //List<EventInfo.EventBean> mEvents = Temp.getAllEvent();

        Calendar mCal = (Calendar) cal.clone();
        mCal.set(Calendar.DAY_OF_MONTH, 1);
        int firstDayOfTheMonth = mCal.get(Calendar.DAY_OF_WEEK) - 1;
        mCal.add(Calendar.DAY_OF_MONTH, -firstDayOfTheMonth);
        while (dayValueInCells.size() < MAX_CALENDAR_COLUMN) {
            dayValueInCells.add(mCal.getTime());
            mCal.add(Calendar.DAY_OF_MONTH, 1);
        }
        Log.d(TAG, "Number of date " + dayValueInCells.size());
        String sDate = formatter.format(cal.getTime());
        currentDate.setText(sDate);
        //mAdapter = new GridAdapter(context, dayValueInCells,cal);
        mAdapter = new GridAdapter(context, dayValueInCells, cal, mEvents, sEvents, cEvents, todayMonth + 1, todayYear, todayDay, new OnDayClickListener() {
            @Override
            public void setDay(Date day) {

                Calendar dateCal = Calendar.getInstance();
                dateCal.setTime(day);
                if (compactCalendarViewListener != null) {
                    compactCalendarViewListener.onDayClick(dateCal.get(Calendar.DAY_OF_MONTH), dateCal.get(Calendar.MONTH) + 1, dateCal.get(Calendar.YEAR));
                }
            }

            @Override
            public void setSchoolEvents(List<SchoolActivityResponse.ResultBean> sEvents, Date day) {

                Calendar dateCal = Calendar.getInstance();
                dateCal.setTime(day);

                if (compactCalendarViewListener != null) {
                    compactCalendarViewListener.getSchoolEvents(sEvents, dateCal.get(Calendar.DAY_OF_MONTH), dateCal.get(Calendar.MONTH) + 1, dateCal.get(Calendar.YEAR));
                }


            }

            @Override
            public void setClassEvents(List<ClassActivityResponse.ResultBean> cEvents, Date day) {

                Calendar dateCal = Calendar.getInstance();
                dateCal.setTime(day);

                if (compactCalendarViewListener != null) {
                    compactCalendarViewListener.getClassEvents(cEvents, dateCal.get(Calendar.DAY_OF_MONTH), dateCal.get(Calendar.MONTH) + 1, dateCal.get(Calendar.YEAR));
                }
            }
        });
        calendarGridView.setAdapter(mAdapter);
    }

   /* public interface OnDayClickListener {
        public void setDay(Date day);

        public void setSchoolEvents(List<SchoolActivityResponse.ResultBean> sEvents, Date day);

        public void setClassEvents(List<ClassActivityResponse.ResultBean> sEvents, Date day);
    }*/

  /*  public interface CompactCalendarViewListener {
        public void onDayClick(int date, int month, int year);

        public void getSchoolEvents(List<SchoolActivityResponse.ResultBean> sEvents, int date, int month, int year);

        public void getClassEvents(List<ClassActivityResponse.ResultBean> cEvents, int date, int month, int year);
    }*/

    public void setOnMonthChangeListener(OnMonthChangeListener onMonthChangeListener) {
        this.onMonthChangeListener = onMonthChangeListener;
    }

    public void setCurrentDate() {
        setUpCalendarAdapter();
    }

    public void setListener(CompactCalendarViewListener listener) {

        this.compactCalendarViewListener = listener;
    }

    public void setEventList(List<TeacherAppointmentModel.ResultBean> mEvents) {
        this.mEvents = mEvents;
        mAdapter.addEventList(this.mEvents);
    }

    public void setSchoolEventsList(List<SchoolActivityResponse.ResultBean> sEvents) {
        this.sEvents = sEvents;
        mAdapter.addSchoolEventList(this.sEvents);
    }

    public void setClassEventsList(List<ClassActivityResponse.ResultBean> cEvents) {
        this.cEvents = cEvents;
        mAdapter.addSchoolEventList(this.sEvents);
    }

}
