/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.calendarview.callbacks;

import java.util.List;

import ca.dataready.SmartSchoolAdmin.server.ClassActivityResponse;
import ca.dataready.SmartSchoolAdmin.server.SchoolActivityResponse;

public interface CompactCalendarViewListener {


    public void onDayClick(int date, int month, int year);

    public void getSchoolEvents(List<SchoolActivityResponse.ResultBean> sEvents, int date, int month, int year);

    public void getClassEvents(List<ClassActivityResponse.ResultBean> cEvents, int date, int month, int year);

}
