/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.calendarview.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.calendarview.CalendarCustomView;
import ca.dataready.SmartSchoolAdmin.calendarview.callbacks.OnDayClickListener;
import ca.dataready.SmartSchoolAdmin.server.ClassActivityResponse;
import ca.dataready.SmartSchoolAdmin.server.SchoolActivityResponse;
import ca.dataready.SmartSchoolAdmin.server.TeacherAppointmentModel;


/**
 * Created by c161 on 09/06/17.
 */

public class GridAdapter extends ArrayAdapter {
    private static final String TAG = GridAdapter.class.getSimpleName();
    private LayoutInflater mInflater;
    private List<Date> monthlyDates;
    private Calendar currentDate;
    private List<TeacherAppointmentModel.ResultBean> allEvents;
    private List<SchoolActivityResponse.ResultBean> sEvents;
    private List<ClassActivityResponse.ResultBean> cEvents;
    private Context context;
    private int last_index;
    private TextView last_textView;
    private int todayMonth;
    private int todayDay;
    private int todayYear;
    private TextView last_event_indicator;
    private OnDayClickListener onDayClickListener;

    public GridAdapter(Context context, List<Date> monthlyDates, Calendar currentDate, List<TeacherAppointmentModel.ResultBean> allEvents,List<SchoolActivityResponse.ResultBean> sEvents
            ,List<ClassActivityResponse.ResultBean> cEvents,int todayMonth, int todayYear, int todayDay, OnDayClickListener onDayClickListener) {
        super(context, R.layout.single_cell_layout);
        this.monthlyDates = monthlyDates;
        this.currentDate = currentDate;
        this.allEvents = allEvents;
        this.sEvents = sEvents;
        this.cEvents = cEvents;
        this.context = context;
        this.todayMonth = todayMonth;
        this.todayYear = todayYear;
        this.todayDay = todayDay;
        mInflater = LayoutInflater.from(context);
        this.onDayClickListener = onDayClickListener;
    }

    public GridAdapter(Context context, List<Date> monthlyDates, Calendar currentDate) {
        super(context, R.layout.single_cell_layout);
        this.monthlyDates = monthlyDates;
        this.currentDate = currentDate;
        mInflater = LayoutInflater.from(context);

    }

    @NonNull
    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        Date mDate = monthlyDates.get(position);
        Calendar dateCal = Calendar.getInstance();
        dateCal.setTime(mDate);
        int dayValue = dateCal.get(Calendar.DAY_OF_MONTH);
        int displayMonth = dateCal.get(Calendar.MONTH) + 1;
        int displayYear = dateCal.get(Calendar.YEAR);
        int currentMonth = currentDate.get(Calendar.MONTH) + 1;
        int currentYear = currentDate.get(Calendar.YEAR);
        View view = convertView;
        if (view == null) {
            view = mInflater.inflate(R.layout.single_cell_layout, parent, false);
        }
        //Add day to calendar
        final TextView cellNumber = (TextView) view.findViewById(R.id.calendar_date_id);
        final TextView eventIndicator = (TextView) view.findViewById(R.id.event_id);

        cellNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (last_textView != null && onDayClickListener != null) {

                    last_textView.setTextColor(ContextCompat.getColor(context, R.color.roboto_calendar_day_of_the_week_font));
                    last_textView.setBackgroundResource(0);
                    cellNumber.setTextColor(ContextCompat.getColor(context, android.R.color.white));
                    cellNumber.setBackgroundResource(R.drawable.round_textview);
                    last_index = position;
                    last_textView = cellNumber;
                    last_event_indicator = eventIndicator;
                    onDayClickListener.setDay(monthlyDates.get(position));
                    onDayClickListener.setSchoolEvents(sEvents,monthlyDates.get(position));
                    onDayClickListener.setClassEvents(cEvents,monthlyDates.get(position));
                }
            }
        });

        cellNumber.setText(String.valueOf(dayValue));

        if (displayMonth == currentMonth && displayYear == currentYear) {
            cellNumber.setVisibility(View.VISIBLE);
            eventIndicator.setVisibility(View.VISIBLE);
            if (currentMonth == todayMonth && dayValue == todayDay && currentYear == todayYear && last_index == 0) {
                last_index = position;
                last_textView = cellNumber;
                last_event_indicator = eventIndicator;
                cellNumber.setTextColor(ContextCompat.getColor(context, android.R.color.white));
                cellNumber.setBackgroundResource(R.drawable.round_textview);
                onDayClickListener.setDay(monthlyDates.get(position));
            }
        } else {
            cellNumber.setVisibility(View.INVISIBLE);
            eventIndicator.setVisibility(View.INVISIBLE);
        }

        if (last_textView == null) {
            last_textView = cellNumber;

        }
        if (last_event_indicator == null) {
            last_event_indicator = eventIndicator;
        }


        //Add events to the calendar

        Calendar eventCalendar = Calendar.getInstance();
        if(allEvents!=null && allEvents.size()>0) {
            for (int i = 0; i < allEvents.size(); i++) {

                try {
                    if (allEvents.get(i).getAppointmentDate() != null)
                        eventCalendar.setTime(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(allEvents.get(i).getAppointmentDate()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (dayValue == eventCalendar.get(Calendar.DAY_OF_MONTH) && displayMonth == eventCalendar.get(Calendar.MONTH) + 1
                        && displayYear == eventCalendar.get(Calendar.YEAR)) {

                    eventIndicator.setBackgroundColor(Color.parseColor("#3D6CB2"));
                }
            }
        }

        if(sEvents!=null && sEvents.size()>0) {
            for (int i = 0; i < sEvents.size(); i++) {

                try {
                    if (sEvents.get(i).getNotificationDate() != null)
                        eventCalendar.setTime(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(sEvents.get(i).getNotificationDate()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (dayValue == eventCalendar.get(Calendar.DAY_OF_MONTH) && displayMonth == eventCalendar.get(Calendar.MONTH) + 1
                        && displayYear == eventCalendar.get(Calendar.YEAR)) {

                    eventIndicator.setBackgroundColor(Color.parseColor("#3D6CB2"));
                }
            }
        }

        if(cEvents!=null && cEvents.size()>0) {
            for (int i = 0; i < cEvents.size(); i++) {

                try {
                    if (cEvents.get(i).getNotificationDate() != null)
                        eventCalendar.setTime(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(cEvents.get(i).getNotificationDate()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (dayValue == eventCalendar.get(Calendar.DAY_OF_MONTH) && displayMonth == eventCalendar.get(Calendar.MONTH) + 1
                        && displayYear == eventCalendar.get(Calendar.YEAR)) {

                    eventIndicator.setBackgroundColor(Color.parseColor("#3D6CB2"));
                }
            }
        }


        return view;
    }

    @Override
    public int getCount() {
        return monthlyDates.size();
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return monthlyDates.get(position);
    }

    @Override
    public int getPosition(Object item) {
        return monthlyDates.indexOf(item);
    }

    public void addEventList(List<TeacherAppointmentModel.ResultBean> mEvents) {
        this.allEvents = mEvents;

        if (last_index != 0 && last_textView != null && last_event_indicator != null && onDayClickListener != null) {
            last_event_indicator.setBackgroundColor(0x00000000);
            onDayClickListener.setDay(monthlyDates.get(last_index));
        }
        notifyDataSetChanged();

    }

    public void addSchoolEventList(List<SchoolActivityResponse.ResultBean> mEvents) {
        this.sEvents = mEvents;

        if (last_index != 0 && last_textView != null && last_event_indicator != null && onDayClickListener != null) {
            last_event_indicator.setBackgroundColor(0x00000000);
            onDayClickListener.setDay(monthlyDates.get(last_index));
        }
        notifyDataSetChanged();

    }

    public void addClassEventList(List<ClassActivityResponse.ResultBean> mEvents) {
        this.cEvents = mEvents;

        if (last_index != 0 && last_textView != null && last_event_indicator != null && onDayClickListener != null) {
            last_event_indicator.setBackgroundColor(0x00000000);
            onDayClickListener.setDay(monthlyDates.get(last_index));
        }
        notifyDataSetChanged();

    }



}
