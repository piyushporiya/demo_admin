/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.active_route.selected_route.videorecords;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.ArrayList;

import ca.dataready.SmartSchoolAdmin.R;

/**
 * Created by social_jaydeep on 29/07/17.
 */

public class RouteListAdapter extends RecyclerView.Adapter<RouteListAdapter.VideoViewHolder> {


    private Context context;
    private ArrayList<String> beans;

    public RouteListAdapter(Context context, ArrayList<String> recordingList) {
        this.context = context;
        beans = recordingList;
    }

    @Override
    public RouteListAdapter.VideoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RouteListAdapter.VideoViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_route_list, parent, false));
    }

    @Override
    public void onBindViewHolder(RouteListAdapter.VideoViewHolder holder, int position) {

        final String bean = beans.get(position);

        holder.txtName.setText(bean);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((VideoListActivity) context).ShowAllVideos(bean);
            }
        });

    }

    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }

    class VideoViewHolder extends RecyclerView.ViewHolder {

        TextView txtName;
        View mView;

        VideoViewHolder(View itemView) {
            super(itemView);
            mView = itemView;

            txtName = (TextView) itemView.findViewById(R.id.txt_name);
        }

    }
}

