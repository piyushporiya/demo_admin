/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.active_route.selected_route.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.server.AppApi;
import ca.dataready.SmartSchoolAdmin.server.GetTotalBoardingResponse;


/**
 * Created by social_jaydeep on 13/09/17.
 */

public class OnBoardStudentListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<GetTotalBoardingResponse.ResultBean> beans;

    public OnBoardStudentListAdapter(Context context) {
        this.context = context;
        beans = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new StudentViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_dialog_on_board_student, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        GetTotalBoardingResponse.ResultBean bean = beans.get(position);
        if (holder instanceof StudentViewHolder) {

            ((StudentViewHolder) holder).txtName.setText(bean.getFirstName() + " " + bean.getLastName());
            ((StudentViewHolder) holder).profilePic.setImageURI(AppApi.BASE_URL + bean.getProfilePic());
            ((StudentViewHolder) holder).txtStopId.setText("Stop Id : " + bean.getStopId());

        }
    }

    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }

    public void addItem(List<GetTotalBoardingResponse.ResultBean> results) {

        Collections.sort(results, new Comparator<GetTotalBoardingResponse.ResultBean>() {
            public int compare(GetTotalBoardingResponse.ResultBean obj1, GetTotalBoardingResponse.ResultBean obj2) {
                return obj1.getStopId().compareToIgnoreCase(obj2.getStopId());

            }
        });

        beans.addAll(results);
        notifyDataSetChanged();
    }

    class StudentViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.profile_pic)
        SimpleDraweeView profilePic;
        @BindView(R.id.txt_name)
        TextView txtName;
        @BindView(R.id.txt_stopId)
        TextView txtStopId;
        @BindView(R.id.txt_address)
        TextView txtAddress;
        // R.layout.raw_dialog_on_board_student

        public StudentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
