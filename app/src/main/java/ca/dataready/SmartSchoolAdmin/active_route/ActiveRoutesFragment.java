/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.active_route;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.Listeners.OnReloadListener;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.active_route.selected_route.SelectedRouteActivity;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.ActiveRouteInfoResponse;
import ca.dataready.SmartSchoolAdmin.server.MapModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ActiveRoutesFragment extends Fragment implements OnReloadListener, OnMapReadyCallback {


    SupportMapFragment mapFragment;
    private static View view;
    @BindView(R.id.viewAnimator)
    ViewAnimator viewAnimator;
    private Call<ActiveRouteInfoResponse> activeRouteApi;
    ArrayList<MapModel> latlongModel = new ArrayList<>();
    ArrayList<LatLng> markerPoints = new ArrayList<>();
    private GoogleMap mMap;
    private List<ActiveRouteInfoResponse.ResultBean> results;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_active_routes, container, false);
            mapFragment = (SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.map);

        } catch (InflateException e) {
           /* map is already there, just return view as it is */
        }

        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) getActivity()).setSubTitle(getString(R.string.active_route));
        ((HomeActivity) getActivity()).setOnReloadListener(this);
        Init();
    }

    private void Init() {

        getActiveRoutes();
    }

    private void getActiveRoutes() {

        viewAnimator.setDisplayedChild(0);

        activeRouteApi = AdminApp.getInstance().getApi().getActiveRoutes(AdminApp.getInstance().getAdmin().getSchoolId(), AdminApp.getInstance().currentDate());
        activeRouteApi.enqueue(new Callback<ActiveRouteInfoResponse>() {
            @Override
            public void onResponse(Call<ActiveRouteInfoResponse> call, Response<ActiveRouteInfoResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            results = response.body().getResult();
                            if (results != null && results.size() > 0) {

                                for (ActiveRouteInfoResponse.ResultBean bean : results) {

                                    if (bean.getPositionDetails() != null && bean.getPositionDetails().getLongitude() != null && bean.getPositionDetails().getLatitude() != null) {
                                        latlongModel.add(new MapModel(Double.parseDouble(bean.getPositionDetails().getLatitude())
                                                , Double.parseDouble(bean.getPositionDetails().getLongitude()), bean.getName() + "-" + bean.getTripType()));
                                    }
                                }
                                mapFragment.getMapAsync(ActiveRoutesFragment.this);
                                viewAnimator.setDisplayedChild(1);

                            } else {

                                Toast.makeText(getActivity(), getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                                viewAnimator.setDisplayedChild(1);
                            }

                        } else {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            viewAnimator.setDisplayedChild(1);
                        }
                    } else {
                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_GET_ACTIVE_ROUTES);
                        Toast.makeText(getActivity(), error.message(), Toast.LENGTH_SHORT).show();
                        viewAnimator.setDisplayedChild(1);
                    }
                } catch (Exception e) {

                    Toast.makeText(getActivity(), getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                    viewAnimator.setDisplayedChild(1);
                }
            }

            @Override
            public void onFailure(Call<ActiveRouteInfoResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(getActivity(), App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                    viewAnimator.setDisplayedChild(1);
                }
            }
        });
    }

    @Override
    public void onReload(String apiName) {

        if (apiName != null) {

            if (apiName.equals(App_Constants.API_GET_ACTIVE_ROUTES)) {

                getActiveRoutes();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if (activeRouteApi != null)
            activeRouteApi.cancel();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        if (latlongModel.size() > 0) {

            for (int i = 0; i < latlongModel.size(); i++) {

                LatLng latLng = new LatLng(latlongModel.get(i).getLat(), latlongModel.get(i).getLon());
                markerPoints.add(latLng);

                Marker marker = mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title(latlongModel.get(i).getTitle())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_directions_bus_black_24dp)));
                marker.showInfoWindow();
                builder.include(latLng);
            }
        }

        final LatLngBounds bounds = builder.build();

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {

                float[] results = new float[1];
                Location.distanceBetween(bounds.northeast.latitude, bounds.northeast.longitude,
                        bounds.southwest.latitude, bounds.southwest.longitude, results);

                CameraUpdate cu = null;
                if (results[0] < 1000) { // distance is less than 1 km -> set to zoom level 15
                    cu = CameraUpdateFactory.newLatLngZoom(bounds.getCenter(), 14);

                } else {
                    int padding = 40; // offset from edges of the map in pixels
                    cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                }
                if (cu != null) {
                    mMap.animateCamera(cu);
                }


            }
        });

        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {

                if (results != null && results.size() > 0) {

                    for (ActiveRouteInfoResponse.ResultBean bean : results) {

                        if (bean.getPositionDetails() != null && bean.getPositionDetails().getLongitude() != null && bean.getPositionDetails().getLatitude() != null
                                && Double.parseDouble(bean.getPositionDetails().getLatitude()) == marker.getPosition().latitude
                                && Double.parseDouble(bean.getPositionDetails().getLongitude()) == marker.getPosition().longitude) {

                            startActivityForResult(new Intent(getActivity(), SelectedRouteActivity.class)
                                    .putExtra(App_Constants.OBJECT, bean), App_Constants.UPDATE_LISTING);
                            if (getActivity() != null)
                                getActivity().overridePendingTransition(R.anim.enter, R.anim.leave);

                            break;

                        }
                    }
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
