/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.active_route.selected_route;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.map.HttpConnection;
import ca.dataready.SmartSchoolAdmin.Utilities.map.PathJSONParser;
import ca.dataready.SmartSchoolAdmin.active_route.selected_route.adapter.OnBoardStudentListAdapter;
import ca.dataready.SmartSchoolAdmin.active_route.selected_route.videorecords.VideoListActivity;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.ActiveRouteInfoResponse;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.MapModel;
import ca.dataready.SmartSchoolAdmin.server.GetTotalBoardingResponse;
import ca.dataready.SmartSchoolAdmin.server.TripInfoResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectedRouteActivity extends BaseActivity implements OnMapReadyCallback {

    SupportMapFragment mapFragment;
    @BindView(R.id.menu_item)
    ImageButton menuItem;
    @BindView(R.id.txt_speed)
    TextView txtSpeed;
    @BindView(R.id.txt_passenger_count_label)
    TextView txtPassengerCountLabel;
    @BindView(R.id.txt_passenger_count)
    TextView txtPassengerCount;
    @BindView(R.id.txt_total_passenger_label)
    TextView txtTotalPassengerLabel;
    @BindView(R.id.txt_total_passenger)
    TextView txtTotalPassenger;
    @BindView(R.id.llTotalpassanger)
    LinearLayout llTotalpassanger;
    @BindView(R.id.viewTotalpassanger)
    View viewTotalpassanger;
    @BindView(R.id.txt_total_station)
    TextView txtTotalStation;
    @BindView(R.id.txt_remainig_students_label)
    TextView txtRemainigStudentsLabel;
    @BindView(R.id.txt_remainig_students)
    TextView txtRemainigStudents;
    @BindView(R.id.twe)
    LinearLayout twe;
    @BindView(R.id.one)
    LinearLayout one;

    private Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {

            getSelectedTripInfo();
            handler.postDelayed(this, App_Constants.SCHEDULED_TIME);
        }
    };
    private boolean isPathDrawn;
    private ProgressDialog progressDialog;
    private Call<TripInfoResponse> selectedRouteappApi;
    private TripInfoResponse.ResultBean selctedResultBean;
    private TripInfoResponse.ResultBean.CurrentPositionBean currentBean;
    private int onBoardCount;
    private String tripId, tripType;

    ArrayList<MapModel> latlongModel = new ArrayList<>();
    ArrayList<LatLng> markerPoints = new ArrayList<>();
    private Marker currentMarker;
    private GoogleMap mMap;
    PolylineOptions lineOptions;
    private Call<GetTotalBoardingResponse> apiStudentList;
    private int remainingStudent;
    ActiveRouteInfoResponse.ResultBean bean;
    private String routeId;
    private String uniqueId;
    private ViewAnimator viewAnimatorStudentList;
    private TextView txtEmpty;
    private OnBoardStudentListAdapter adapter;
    private AlertDialog studentBoardingAlertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_route);
        ButterKnife.bind(this);
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

      //  Init();
    }

    private void Init() {

        bean = getIntent().getParcelableExtra(App_Constants.OBJECT);
        if (bean != null) {
            routeId = bean.getRouteId();
            uniqueId = bean.getUniqueId();
            handler.postDelayed(runnable, 200);
        }
    }

    private void getSelectedTripInfo() {

        if (!isPathDrawn)
            showDialog(getString(R.string.processing));

        selectedRouteappApi = AdminApp.getInstance().getApi().getTripInfo(routeId, uniqueId, AdminApp.getInstance().currentDate());
        selectedRouteappApi.enqueue(new Callback<TripInfoResponse>() {
            @Override
            public void onResponse(Call<TripInfoResponse> call, Response<TripInfoResponse> response) {
                hideDialog();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            selctedResultBean = response.body().getResult();

                            if (selctedResultBean != null) {

                                currentBean = selctedResultBean.getCurrentPosition();
                                onBoardCount = Integer.parseInt(selctedResultBean.getOnboardCount());

                                tripId = selctedResultBean.getTripId();
                                tripType = selctedResultBean.getTripType();
                                if (!isPathDrawn) {
                                    if (tripType != null && tripType.equalsIgnoreCase(App_Constants.DROP_OFF))
                                        setLabels(true);
                                    else
                                        setLabels(false);
                                }
                                txtPassengerCount.setText("" + selctedResultBean.getOnboardCount());
                                getRemainigStudentPickUpCount();
                            }

                            List<TripInfoResponse.ResultBean.StopageBean> stopageList = response.body().getResult().getStopage();
                            if (stopageList != null && stopageList.size() > 0) {

                                txtTotalStation.setText(String.valueOf(stopageList.size() - 1));
                                if (!isPathDrawn) {

                                    for (TripInfoResponse.ResultBean.StopageBean bean : stopageList) { //add all lat, lon and title in latlongModel list
                                        latlongModel.add(new MapModel(Double.parseDouble(bean.getLatitude()), Double.parseDouble(bean.getLongitude()), bean.getAddress()));
                                    }
                                    mapFragment.getMapAsync(SelectedRouteActivity.this);
                                }

                                if (currentBean != null) {
                                    updateView(); //update the views
                                }

                                if (!isPathDrawn)
                                    hideDialog();

                            } else {
                                removeCallbacks();
                                Toast.makeText(SelectedRouteActivity.this, getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                            }

                        } else {

                            if (!isPathDrawn)
                                hideDialog();
                            removeCallbacks();
                            clearMap();
                            resetLabels();
                            Toast.makeText(SelectedRouteActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        APIError error = APIError.parseError(response, SelectedRouteActivity.this, App_Constants.API_ADMIN_SELECTED_ROUTE_INFO);
                        Toast.makeText(SelectedRouteActivity.this, "" + error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {

                    removeCallbacks();
                    if (!isPathDrawn)
                        hideDialog();
                    e.printStackTrace();
                    Toast.makeText(SelectedRouteActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TripInfoResponse> call, Throwable t) {

                if (!call.isCanceled()) {

                    if (!isPathDrawn)
                        hideDialog();

                    if (t.getCause() instanceof SocketTimeoutException)
                        Toast.makeText(SelectedRouteActivity.this, getString(R.string.time_out), Toast.LENGTH_SHORT).show();
                    else if (t.getCause() instanceof IllegalStateException)
                        Toast.makeText(SelectedRouteActivity.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(SelectedRouteActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();

                    System.out.println("ERROR " + t.getCause());
                }
            }
        });
    }

    private void getRemainigStudentPickUpCount() {

        CREDENTIAL.ResultBean bean = AdminApp.getInstance().getAdmin();
        apiStudentList = AdminApp.getInstance().getApi().getTotalBoarding(bean.getSchoolId(), bean.getSchoolYear(), routeId, tripId, tripType);
        apiStudentList.enqueue(new Callback<GetTotalBoardingResponse>() {
            @Override
            public void onResponse(Call<GetTotalBoardingResponse> call, Response<GetTotalBoardingResponse> response) {

                try {
                    if (response.isSuccessful()) {

                        if (response.body().isStatus()) {

                            if (response.body().getResult() != null && response.body().getResult().size() > 0) {

                                txtTotalPassenger.setText(String.valueOf(response.body().getResult().size()));

                                if (tripType != null && tripType.equalsIgnoreCase(App_Constants.PICK_UP)) {
                                    List<GetTotalBoardingResponse.ResultBean> beans = new ArrayList<GetTotalBoardingResponse.ResultBean>();
                                    beans.clear();
                                    for (GetTotalBoardingResponse.ResultBean bean : response.body().getResult()) {
                                        if (bean.getOuttime() == null) {
                                            if (bean.getIntime() == null) {
                                                beans.add(bean);
                                            }
                                        }
                                    }
                                    txtRemainigStudents.setText(String.valueOf(beans.size()));
                                } else {
                                    List<GetTotalBoardingResponse.ResultBean> droppedStudents = new ArrayList<GetTotalBoardingResponse.ResultBean>();
                                    droppedStudents.clear();
                                    for (GetTotalBoardingResponse.ResultBean bean : response.body().getResult()) {
                                        if (bean.getOuttime() != null) {
                                            droppedStudents.add(bean);
                                        }
                                    }
                                    txtRemainigStudents.setText(String.valueOf(droppedStudents.size()));
                                }

                            } else {

                                remainingStudent = 0;
                                txtRemainigStudents.setText(String.valueOf(remainingStudent));
                            }
                        }
                    } else {

                        APIError error = APIError.parseError(response, SelectedRouteActivity.this, App_Constants.API_ADMIN_PICK_UP_STUDENTS_COUNT);
                        Toast.makeText(SelectedRouteActivity.this, "" + error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    remainingStudent = 0;
                    txtRemainigStudents.setText(String.valueOf(remainingStudent));

                }
            }

            @Override
            public void onFailure(Call<GetTotalBoardingResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    remainingStudent = 0;
                    txtRemainigStudents.setText(String.valueOf(remainingStudent));
                    Toast.makeText(SelectedRouteActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    private void clearMap() {

        isPathDrawn = false;
        if (mMap != null)
            mMap.clear(); //clear map
        markerPoints.clear(); //clear markerpoints list
        latlongModel.clear(); //clear latlongModel list
        lineOptions = null;
    }

    private void resetLabels() {

        txtPassengerCount.setText("0");
        txtRemainigStudents.setText("0");
        txtSpeed.setText("0.00 km/h");
        txtTotalPassenger.setText("0");
        txtTotalStation.setText("0");
    }

    private void setLabels(boolean isDropOffTrip) {


        if (isDropOffTrip) {
            txtPassengerCountLabel.setText("Passengers On Board");
            llTotalpassanger.setVisibility(View.VISIBLE);
            viewTotalpassanger.setVisibility(View.VISIBLE);
            txtRemainigStudentsLabel.setText("Dropped Passengers");
        } else {
            txtPassengerCountLabel.setText("Passengers Count");
            llTotalpassanger.setVisibility(View.GONE);
            viewTotalpassanger.setVisibility(View.GONE);
            txtRemainigStudentsLabel.setText("Remaining Passenger");
        }
    }

    private void updateView() {

        txtSpeed.setText(String.format("%.2f", Double.parseDouble(currentBean.getSpeed())) + " km/h"); // set speed.
        if (mMap != null) {

            if (currentMarker != null)
                currentMarker.remove();

            currentMarker = mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_directions_bus_black_24dp))
                    .position(new LatLng(Double.parseDouble(currentBean.getLatitude()), Double.parseDouble(currentBean.getLongitude()))).title("Bus's Position"));

            //centerMapOnMyLocation();
        }
    }

    private void centerMapOnMyLocation() {

        LatLng myLocation = new LatLng(Double.parseDouble(currentBean.getLatitude()),
                Double.parseDouble(currentBean.getLongitude()));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation,
                15));

    }

    private void removeCallbacks() {

        if (handler != null)
            handler.removeCallbacks(runnable);
    }

    private void showDialog(String message) {

        progressDialog = new ProgressDialog(SelectedRouteActivity.this, R.style.ProgressDialogTheme);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    private void hideDialog() {

        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @OnClick({R.id.menu_item, R.id.btn_view_recording, R.id.btn_on_board_student_list})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.menu_item:
                finish();
                break;
            case R.id.btn_view_recording:

                startActivity(new Intent(SelectedRouteActivity.this, VideoListActivity.class)
                        .putExtra(VideoListActivity.WHICH, VideoListActivity.ADMIN)
                        .putExtra(VideoListActivity.SCHOOL_ID, AdminApp.getInstance().getAdmin().getSchoolId()));
                break;
            case R.id.btn_on_board_student_list:
                getOnBoardStudentListDialog();
                break;
        }
    }


    private void getOnBoardStudentListDialog() {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.on_board_student_list_dialog, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);


        viewAnimatorStudentList = (ViewAnimator) dialogView.findViewById(R.id.viewAnimatorStudentList);
        txtEmpty = (TextView) dialogView.findViewById(R.id.txt_empty);
        ImageButton btnCancel = (ImageButton) dialogView.findViewById(R.id.btnCancel);

        RecyclerView recylerview = (RecyclerView) dialogView.findViewById(R.id.recyclerview);

        recylerview.setLayoutManager(new LinearLayoutManager(SelectedRouteActivity.this));
        recylerview.addItemDecoration(new DividerItemDecoration(SelectedRouteActivity.this, DividerItemDecoration.VERTICAL));

        adapter = new OnBoardStudentListAdapter(SelectedRouteActivity.this);
        recylerview.setAdapter(adapter);


        CREDENTIAL.ResultBean bean = AdminApp.getInstance().getAdmin();
        apiStudentList = AdminApp.getInstance().getApi().getTotalBoarding(bean.getSchoolId(), bean.getSchoolYear(), routeId, tripId, tripType);
        apiStudentList.enqueue(new Callback<GetTotalBoardingResponse>() {
            @Override
            public void onResponse(Call<GetTotalBoardingResponse> call, Response<GetTotalBoardingResponse> response) {

                try {
                    if (response.isSuccessful()) {

                        if (response.body().isStatus()) {

                            if (response.body().getResult() != null && response.body().getResult().size() > 0) {
                                List<GetTotalBoardingResponse.ResultBean> beans = new ArrayList<>();
                                for (GetTotalBoardingResponse.ResultBean bean : response.body().getResult()) {
                                    if (bean.getOuttime() == null) {
                                        if (bean.getIntime() != null) {

                                            beans.add(bean);
                                        }
                                    }
                                }

                                if (beans.size() > 0) {

                                    adapter.addItem(beans);
                                    viewAnimatorStudentList.setDisplayedChild(1);

                                } else {

                                    viewAnimatorStudentList.setDisplayedChild(2);
                                    txtEmpty.setText(getString(R.string.no_data));
                                }

                            } else {

                                viewAnimatorStudentList.setDisplayedChild(2);
                                txtEmpty.setText(getString(R.string.no_data));
                            }

                        } else {

                            viewAnimatorStudentList.setDisplayedChild(2);
                            txtEmpty.setText(response.body().getMessage());
                        }
                    } else {

                        viewAnimatorStudentList.setDisplayedChild(2);

                        txtEmpty.setText(getString(R.string.somethingwrong));
                    }
                } catch (Exception e) {

                    viewAnimatorStudentList.setDisplayedChild(2);
                    txtEmpty.setText(getString(R.string.somethingwrong));
                }
            }

            @Override
            public void onFailure(Call<GetTotalBoardingResponse> call, Throwable t) {

                if (!call.isCanceled()) {

                    viewAnimatorStudentList.setDisplayedChild(2);
                    txtEmpty.setText(App_Constants.NO_INTERNET);
                }
            }
        });


        studentBoardingAlertDialog = dialogBuilder.create();
        studentBoardingAlertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dismiss),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        studentBoardingAlertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                studentBoardingAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(SelectedRouteActivity.this, R.color.colorPrimaryDark));
            }
        });
        studentBoardingAlertDialog.show();


    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        if (latlongModel.size() > 0) {

            for (int i = 0; i < latlongModel.size(); i++) {
                MarkerOptions markerOptions = new MarkerOptions();
                LatLng latLng = new LatLng(latlongModel.get(i).getLat(), latlongModel.get(i).getLon());
                markerPoints.add(latLng);
                if (i == 0) {
                    if (tripType != null && tripType.equalsIgnoreCase(App_Constants.PICK_UP))
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                    else if (tripType != null && tripType.equalsIgnoreCase(App_Constants.DROP_OFF))
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                    else
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                } else if (i == latlongModel.size() - 1) {
                    if (tripType != null && tripType.equalsIgnoreCase(App_Constants.PICK_UP))
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_school_icon));
                    else if (tripType != null && tripType.equalsIgnoreCase(App_Constants.DROP_OFF))
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_school_green));
                    else
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_school_icon));
                } else {
                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                }
                markerOptions.position(latLng);
                markerOptions.title(latlongModel.get(i).getTitle());
                //markerOptions.snippet("place");
                mMap.addMarker(markerOptions);
                builder.include(latLng);
            }


        }

        final LatLngBounds bounds = builder.build();


        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {

                float[] results = new float[1];
                Location.distanceBetween(bounds.northeast.latitude, bounds.northeast.longitude,
                        bounds.southwest.latitude, bounds.southwest.longitude, results);

                CameraUpdate cu = null;
                if (results[0] < 1000) { // distance is less than 1 km -> set to zoom level 15
                    cu = CameraUpdateFactory.newLatLngZoom(bounds.getCenter(), 14);

                } else {
                    int padding = 40; // offset from edges of the map in pixels
                    cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                }
                if (cu != null) {
                    mMap.animateCamera(cu);
                }
            }
        });

        if (lineOptions != null) {

            drawRoute();

        } else {
            if (latlongModel.size() > 0) {
                String url = getMapsApiDirectionsUrl();
                ReadTask downloadTask = new ReadTask();
                downloadTask.execute(url);
            }
        }
    }

    private String getMapsApiDirectionsUrl() {

        // Origin of route
        String str_origin = "origin=" + latlongModel.get(0).getLat() + "," + latlongModel.get(0).getLon();

        // Destination of route
        String str_dest = "destination=" + latlongModel.get(latlongModel.size() - 1).getLat() + "," + latlongModel.get(latlongModel.size() - 1).getLon();

        // Sensor enabled
        String sensor = "sensor=false";

        // Waypoints
        String waypoints = "";
        for (int i = 0; i < markerPoints.size(); i++) {
            LatLng point = (LatLng) markerPoints.get(i);
            if (i == 0)
                waypoints = "waypoints=";
            waypoints += point.latitude + "," + point.longitude + "|";
        }

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + waypoints;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?key=AIzaSyBaK8M0QdcKkYYid-DH0iRcpFLd_IGws40&" + parameters;

        Log.e("URL", url);

        return url;

    }


    private class ReadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                HttpConnection http = new HttpConnection();
                data = http.readUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                new ParserTask().execute(result);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }

        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {

            ArrayList<LatLng> polylinePoints;

            if (result != null && result.size() > 0) {
                // Traversing through all the routes
                for (int i = 0; i < result.size(); i++) {
                    polylinePoints = new ArrayList<LatLng>();
                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);

                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        polylinePoints.add(position);
                    }

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(polylinePoints);
                    lineOptions.width(10);
                    lineOptions.color(Color.RED);
                }
            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                mMap.addPolyline(lineOptions);
                isPathDrawn = true;
            } else {
                //Toast.makeText(AdminActivity.this, getString(R.string.no_path), Toast.LENGTH_SHORT).show();
                //clearMap();
            }

        }

    }


    private void drawRoute() {

        if (lineOptions != null) {
            mMap.addPolyline(lineOptions);
            isPathDrawn = true;
        } else {
            clearMap();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        Init();
    }

    @Override
    protected void onStop() {
        super.onStop();
        removeCallbacks();

        if (selectedRouteappApi != null)
            selectedRouteappApi.cancel();

        if (apiStudentList != null)
            apiStudentList.cancel();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == APIError.UPDATE_TOKEN && resultCode == RESULT_OK) {

            if (data != null) {
                String apiName = data.getStringExtra("api");
                onReload(apiName);
            }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public void onReload(String apiName) {

        if (apiName != null) {
            if (apiName.equals(App_Constants.API_ADMIN_SELECTED_ROUTE_INFO)) {
                getSelectedTripInfo();
            } else if (apiName.equals(App_Constants.API_ADMIN_PICK_UP_STUDENTS_COUNT)) {
                getRemainigStudentPickUpCount();
            }

        }
    }
}
