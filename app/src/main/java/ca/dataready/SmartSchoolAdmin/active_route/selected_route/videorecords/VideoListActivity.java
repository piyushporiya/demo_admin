/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.active_route.selected_route.videorecords;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.ViewAnimator;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;


import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.video.VideoViewActivity;


public class VideoListActivity extends BaseActivity {

    ArrayList<VideoRecord> recordingList = new ArrayList<>();
    ArrayList<String> routesList = new ArrayList<>();
    RecyclerView rvVideo;
    Toolbar toolbar;
    ProgressDialog progressDialog;
    File uploadToS3;
    RouteListAdapter routeListAdapter;

    public static final String ADMIN = "admin";
    public static final String DRIVER = "driver";
    public static final String WHICH = "which";
    public static final String SCHOOL_ID = "school_id";
    public static final String EXTENSION = ".mp4";
    public static final String DRIVER_UPLOAD_TRIP = "driver_upload";

    AmazonS3 s3Client;

   /* //for stg
    String bucket = "smartdriverappvideos";
    String poolId = "ap-south-1:8f1223d1-395f-40b9-b9aa-9a9b1c6e1b4a";*/

    //for dev
   /* String bucket = "devsmartdrivervideos";
    String poolId = "ap-south-1:e44a48f1-901c-4dc6-81ab-a5467539ae7d";*/

    //public static final String PATH = "https://s3.ap-south-1.amazonaws.com/";

//    ===========New credentials=============

   /* //alpha
    String bucket = "devsmartdrivervideo";
    String poolId = "us-east-2:b2dbcc46-de9b-4180-b396-82404b1f3fdf";*/

    //beta & production
    String bucket = "stgsmartdrivervideos";
    String poolId = "us-east-2:e1be9d62-50fb-450b-8ea4-9079192d285f";

    public static final String PATH = "https://s3.us-east-2.amazonaws.com/";

    TransferUtility transferUtility;
    String from;
    @BindView(R.id.viewAnimator)
    ViewAnimator viewAnimator;
    @BindView(R.id.rvRoutes)
    RecyclerView rvRoutes;
    String schoolId;
    @BindView(R.id.txt_empty_view)
    TextView txtEmptyView;
    Handler mainHandler = new Handler();
    private ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_list);
        ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        rvVideo = (RecyclerView) findViewById(R.id.rvVideo);
        rvVideo.setLayoutManager(new LinearLayoutManager(VideoListActivity.this));
        rvVideo.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        rvRoutes.setLayoutManager(new LinearLayoutManager(VideoListActivity.this));
        rvRoutes.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        // callback method to call credentialsProvider method.
        s3credentialsProvider();

        // callback method to call the setTransferUtility method
        setTransferUtility();

        if (savedInstanceState != null) {

            from = savedInstanceState.getString(WHICH);
            schoolId = savedInstanceState.getString(SCHOOL_ID);
        }

        if (getIntent().getExtras() != null) {

            from = getIntent().getStringExtra(WHICH);
            schoolId = getIntent().getStringExtra(SCHOOL_ID);
        }


        if (from != null) {

            if (from.equals(ADMIN)) {
                getSupportActionBar().setTitle(getString(R.string.route));
                viewAnimator.setDisplayedChild(0);
                mainHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fetchFileFromS3();
                    }
                }, 200);

            }
        }
    }


    public void s3credentialsProvider() {

        // Initialize the AWS Credential
        CognitoCachingCredentialsProvider cognitoCachingCredentialsProvider =
                new CognitoCachingCredentialsProvider(
                        getApplicationContext(),
                        poolId, // Identity Pool ID
                        Regions.US_EAST_2 // Region
                );
        createAmazonS3Client(cognitoCachingCredentialsProvider);
    }

    /**
     * Create a AmazonS3Client constructor and pass the credentialsProvider.
     *
     * @param credentialsProvider
     */
    public void createAmazonS3Client(CognitoCachingCredentialsProvider
                                             credentialsProvider) {
/*
        s3Client = new AmazonS3Client(
                new BasicAWSCredentials("AKIAIAVSXM6UWSNOAKDQ", "WiP0k2Zj0qBUU030xjlpmbqNIDDwUgncy0CFX1nr"));
        s3Client.setRegion(Region.getRegion(Regions.AP_SOUTH_1));*/


        // Create an S3 client
        s3Client = new AmazonS3Client(credentialsProvider);
        // Set the region of your S3 bucket
        s3Client.setRegion(Region.getRegion(Regions.US_EAST_2));

        //      s3Client.setEndpoint( "https://s3.ap-south-1.amazonaws.com" );
    }

    public void setTransferUtility() {

        transferUtility = new TransferUtility(s3Client, getApplicationContext());
    }


    public String getFileSize(long size) {

        String hrSize = "";
        double k = size / 1024.0;
        double m = k / 1024.0;
        double g = m / 1024.0;
        double t = g / 1024.0;
        DecimalFormat dec = new DecimalFormat("0.00");

        if (t > 1) {
            hrSize = dec.format(t).concat(" TB");
        } else if (g > 1) {
            hrSize = dec.format(g).concat(" GB");
        } else if (m > 1) {
            hrSize = dec.format(m).concat(" MB");
        } else if (k > 1) {
            hrSize = dec.format(k).concat(" KB");
        } else {
            hrSize = dec.format(size).concat(" Bytes");
        }
        return hrSize;
    }

    public void openVideo(String videoPath) {

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(videoPath));
        intent.setDataAndType(Uri.parse(videoPath), "video/*");
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {

            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if (from != null) {
            switch (from) {
                case ADMIN:
                    if (viewAnimator.getDisplayedChild() == 1) {
                        getSupportActionBar().setTitle(getString(R.string.route));
                        viewAnimator.setDisplayedChild(2);
                    } else {
                        super.onBackPressed();
                    }
                    break;
                default:
                    super.onBackPressed();
                    break;
            }
        } else {
            super.onBackPressed();
        }

    }


    public void fetchFileFromS3() {

        try {

            FetchFileFromS3AsyncTask task = new FetchFileFromS3AsyncTask();
            task.execute();

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("tag", "Exception found while listing " + e);
            viewAnimator.setDisplayedChild(3);
            txtEmptyView.setText(App_Constants.NO_INTERNET);
        }

    }

    private class FetchFileFromS3AsyncTask extends AsyncTask<Object, Object, List<S3ObjectSummary>> {

        @Override
        protected void onPreExecute() {
            viewAnimator.setDisplayedChild(0);
        }

        @Override
        protected List<S3ObjectSummary> doInBackground(Object... params) {

            ObjectListing images = s3Client.listObjects(bucket);
            List<S3ObjectSummary> list = images.getObjectSummaries();
            return list;
        }

        @Override
        protected void onPostExecute(List<S3ObjectSummary> result) {

            for (S3ObjectSummary image : result) {
                recordingList.add(new VideoRecord(image.getKey(), "", "", getFileSize(image.getSize())));
                if (image.getKey().contains(schoolId)) {
                    if (image.getKey().contains("_")) {
                        String[] sp = image.getKey().split("_");
                        for (int i = 0; i < sp.length; i++) {
                            if (i == 0) {
                                if (!routesList.contains(sp[i])) {
                                    routesList.add(sp[i]);
                                }
                            }
                        }

                    }
                }
            }
            if (routesList != null && routesList.size() > 0) {
                routeListAdapter = new RouteListAdapter(VideoListActivity.this, routesList);
                rvRoutes.setAdapter(routeListAdapter);
                viewAnimator.setDisplayedChild(2);
            } else {
                viewAnimator.setDisplayedChild(3);
            }
        }

    }


    public void ViewVideo(String realName, String name) {

        String path = PATH + bucket + "/" + name;

        startActivity(new Intent(VideoListActivity.this, VideoViewActivity.class)
                .putExtra(VideoViewActivity.VIDEO_PATH, path)
                .putExtra(VideoViewActivity.FILE_NAME, name));

    }

    @Override
    protected void onDestroy() {
        if (mainHandler != null)
            mainHandler.removeCallbacksAndMessages(null);

        super.onDestroy();
    }

    public void ShowAllVideos(String bean) {

        getSupportActionBar().setTitle(getString(R.string.video_list));

        if (recordingList != null && recordingList.size() > 0) {
            int counter = 0;
            ArrayList<VideoRecord> results = new ArrayList<>();
            for (VideoRecord beans : recordingList) {
                String[] aa = beans.getMvideoName().split("_");
                if (aa.length >= 1) {
                    if (aa[0].equals(bean)) {
                        if (beans.getMvideoName().contains(schoolId)) {
                        /*counter = counter + 1;
                        String[] sp = beans.getMvideoName().split("_");
                        String date = sp[0].concat("-").concat(sp[1]).concat("-").concat(sp[2]).concat(" ").concat(sp[3].concat(":").concat(sp[4]).concat(":").concat(sp[5]));
                        VideoRecord v = new VideoRecord("Recording_" + counter, "", date, beans.getMvideoSize(), beans.getMvideoName());*/
                            results.add(beans);
                        }
                    }
                }
            }
            rvVideo.setAdapter(new VideoAdminAdapter(VideoListActivity.this, results));
            viewAnimator.setDisplayedChild(1);
        } else {
            viewAnimator.setDisplayedChild(3);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(WHICH, from);
        outState.putString(SCHOOL_ID, schoolId);
        super.onSaveInstanceState(outState);
    }

}
