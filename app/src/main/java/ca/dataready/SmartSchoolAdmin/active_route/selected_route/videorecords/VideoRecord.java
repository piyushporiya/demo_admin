/*
 * Copyright (c) 2017.                                                       
 *                                                                           
 * ***********************************************************************  
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                           
 * ________________________________________________________________________  
 *                                                                           
 *    [2010] - [2018] SripathiSolutions Corp.                                
 *    [2010] - [2018] DataReady Technology Corp.                             
 *   ________________________________________________________________________
 *                                                                           
 *    All Rights Reserved.                                                   
 *                                                                           
 *   NOTICE:  All information contained herein is, and remains               
 *   the property of DataReady Technology  and its suppliers,                
 *   if any.  The intellectual and technical concepts contained              
 *   herein are proprietary to DataReady Technology Incorporated             
 *   and its suppliers and may be covered by U.S. and Foreign Patents,       
 *   patents in process, and are protected by trade secret or copyright law. 
 *   Dissemination of this information or reproduction of this material      
 *   is strictly forbidden unless prior written permission is obtained       
 *   from DataReady Technology Incorporated.                                 
 *                                                                           
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                            
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.active_route.selected_route.videorecords;

/**
 * Created by social_jaydeep on 25/07/17.
 */

class VideoRecord {

    private String mvideoName;
    private String mvideoPath;
    private String mvideoDuration;
    private String mvideoSize;
    private String realVideoName;

    public VideoRecord(String mvideoName, String mvideoPath, String mvideoDuration, String mvideoSize) {
        this.mvideoName = mvideoName;
        this.mvideoPath = mvideoPath;
        this.mvideoDuration = mvideoDuration;
        this.mvideoSize = mvideoSize;
    }

    public VideoRecord(String mvideoName, String mvideoPath, String mvideoDuration, String mvideoSize,String realVideoName) {
        this.mvideoName = mvideoName;
        this.mvideoPath = mvideoPath;
        this.mvideoDuration = mvideoDuration;
        this.mvideoSize = mvideoSize;
        this.realVideoName = realVideoName;
    }

    public String getRealVideoName() {
        return realVideoName;
    }

    public void setRealVideoName(String realVideoName) {
        this.realVideoName = realVideoName;
    }

    public String getMvideoName() {
        return mvideoName;
    }

    public void setMvideoName(String mvideoName) {
        this.mvideoName = mvideoName;
    }

    public String getMvideoPath() {
        return mvideoPath;
    }

    public void setMvideoPath(String mvideoPath) {
        this.mvideoPath = mvideoPath;
    }

    public String getMvideoDuration() {
        return mvideoDuration;
    }

    public void setMvideoDuration(String mvideoDuration) {
        this.mvideoDuration = mvideoDuration;
    }

    public String getMvideoSize() {
        return mvideoSize;
    }

    public void setMvideoSize(String mvideoSize) {
        this.mvideoSize = mvideoSize;
    }
}
