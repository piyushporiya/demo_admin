/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.web;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Display;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ViewAnimator;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Views.CenterTitleToolbar;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;


public class WebActivity extends BaseActivity {

    @BindView(R.id.web)
    WebView web;
    public static final String URL = "url";
    String filPath;
    @BindView(R.id.toolbar)
    CenterTitleToolbar toolbar;
    @BindView(R.id.viewAnimator)
    ViewAnimator viewAnimator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        ButterKnife.bind(this);
        setHeaderInfo();
        if (getIntent().getExtras() != null) {
            filPath = getIntent().getStringExtra(URL);
            startWebView(filPath);
        }

        Log.e("FILEPATH", "" +filPath);

    }

    private void setHeaderInfo() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getString(R.string.attachments));
        actionBar.setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    private void startWebView(String filPath) {

        web.getSettings().setJavaScriptEnabled(true);
        web.getSettings().setLoadWithOverviewMode(true);
        web.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        web.getSettings().setUseWideViewPort(true);
        web.getSettings().setBuiltInZoomControls(true);
        web.getSettings().setDisplayZoomControls(true);
        //web.loadUrl(filPath);
        if (filPath.contains(".png") || filPath.contains(".jpg") || filPath.contains(".jpeg")
                || filPath.contains(".PNG") || filPath.contains(".JPG") || filPath.contains(".JPEG")
                || filPath.contains(".bmp") || filPath.contains(".BMP")
                || filPath.contains(".gif") || filPath.contains(".GIF")) {

            Display display = getWindowManager().getDefaultDisplay();
            int width = display.getWidth();
            String html = "<html><meta name=\"viewport\"\"content=\"width=" + width + ", initial-scale=0.5 \"/>";
            html += "<body><img width=\"" + width + "\" src=\"" + filPath + "\" /></body></html>";


            String htmlString = "<!DOCTYPE html><html><body style = \"text-align:center\"><img src=" + filPath + " alt=\"pageNo\" width=\"100%\" height=\"100%\"></body></html>";
            web.loadDataWithBaseURL(null, html, "text/html", "UTF-8", null);
            // web.loadUrl(filPath);
        } else {
            web.loadUrl("http://docs.google.com/gview?embedded=true&url=" + filPath);
        }

        web.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {

                //Toast.makeText(WebActivity.this, "page Finished loading", Toast.LENGTH_SHORT).show();
                viewAnimator.setDisplayedChild(1);
            }
        });
    }
}
