/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.adapter;


import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.server.DrawerModel;

public class DrawerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private DrawerSubItemAdapter adapter;
    private Context context;
    private List<DrawerModel> beans;

    public DrawerAdapter(Context context) {
        this.context = context;
        beans = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_drawer_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        final DrawerModel bean = beans.get(position);

        if (holder instanceof ItemViewHolder) {

            ((ItemViewHolder) holder).groupTitle.setText(bean.getGroupTitle());

            adapter = new DrawerSubItemAdapter(context);
            ((ItemViewHolder) holder).recyclerView.setAdapter(adapter);

            if (bean.hasMoreItems()) {
                ((ItemViewHolder) holder).groupIcon.setVisibility(View.VISIBLE);
                adapter.addItems(bean.getSubList());
            } else {
                ((ItemViewHolder) holder).groupIcon.setVisibility(View.GONE);
            }

            if (bean.isExpanded()) {
                ((ItemViewHolder) holder).groupIcon.setImageResource(R.drawable.ic_remove);
                ((ItemViewHolder) holder).recyclerView.setVisibility(View.VISIBLE);
            } else {
                ((ItemViewHolder) holder).groupIcon.setImageResource(R.drawable.ic_add);
                ((ItemViewHolder) holder).recyclerView.setVisibility(View.GONE);
            }

            ((ItemViewHolder) holder).groupView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (bean.hasMoreItems()) {

                        bean.setExpanded(!bean.isExpanded());

                        if (bean.isExpanded()) {
                            ((ItemViewHolder) holder).groupIcon.setImageResource(R.drawable.ic_remove);
                            ((ItemViewHolder) holder).recyclerView.setVisibility(View.VISIBLE);
                        } else {
                            ((ItemViewHolder) holder).groupIcon.setImageResource(R.drawable.ic_add);
                            ((ItemViewHolder) holder).recyclerView.setVisibility(View.GONE);
                        }

                    } else {

                        ((HomeActivity) context).closeDrawer();
                        ((HomeActivity) context).LogoutDialog();
                    }

                    makeOtherCollapsed(holder.getAdapterPosition());
                }
            });
        }
    }

    private void expandAnimation(View view, View groupView) {

        TranslateAnimation animate = new TranslateAnimation(0,0,0,groupView.getHeight());
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.VISIBLE);

    }


    public void addItems(List<DrawerModel> models) {

        beans.addAll(models);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }

    private void makeOtherCollapsed(int adapterPosition) {

        for (int i = 0; i < beans.size(); i++) {

            if (i != adapterPosition) {
                beans.get(i).setExpanded(false);
            }
        }

        notifyDataSetChanged();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.group_view)
        RelativeLayout groupView;
        @BindView(R.id.recyclerView)
        RecyclerView recyclerView;
        @BindView(R.id.group_title)
        TextView groupTitle;
        @BindView(R.id.group_icon)
        ImageView groupIcon;
        //R.layout.raw_drawer_layout

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        }
    }
}
