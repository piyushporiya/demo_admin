/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.dashboard.adapter.DashBoardAdapter;
import ca.dataready.SmartSchoolAdmin.server.DrawerModel;

public class DrawerSubItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<DrawerModel.SubItem> beans;

    public DrawerSubItemAdapter(Context context) {
        this.context = context;
        beans = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_drawer_sub_item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final DrawerModel.SubItem bean = beans.get(position);

        if (holder instanceof ItemViewHolder) {

            ((ItemViewHolder) holder).subTitle.setText(bean.getSubItemTitle());
            ((ItemViewHolder) holder).subIcon.setImageResource(bean.getSubItemIcon());
            ((ItemViewHolder) holder).mView.setEnabled(bean.isEnabled());
            ((ItemViewHolder) holder).mView.setAlpha(bean.isEnabled() ? 1.0f : 0.5f);

            ((ItemViewHolder) holder).mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ((HomeActivity) context).closeDrawer();
                    ((HomeActivity) context).Loadfragment(bean.getFragment());

                }
            });
        }
    }

    public void addItems(List<DrawerModel.SubItem> models) {

        beans.addAll(models);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }


    class ItemViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.sub_icon)
        ImageView subIcon;
        @BindView(R.id.sub_title)
        TextView subTitle;
        @BindView(R.id.sub_view)
        LinearLayout subView;
        View mView;
        // R.layout.raw_drawer_sub_item_layout

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mView = itemView;
        }
    }
}

