/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.sms_emails;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.Listeners.OnReloadListener;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.appointment.adapter.ScheduledAdapter;
import ca.dataready.SmartSchoolAdmin.route.emergency_notification.EmergencyNotificationActivity;
import ca.dataready.SmartSchoolAdmin.route.emergency_notification.adapter.GroupOfStudentsAdapter;
import ca.dataready.SmartSchoolAdmin.route.emergency_notification.adapter.SelectedGroupOfStudentsAdapter;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.GetTotalBoardingResponse;
import ca.dataready.SmartSchoolAdmin.sms_emails.adapter.GroupOfStudentsForSmsAdapter;
import ca.dataready.SmartSchoolAdmin.sms_emails.adapter.SelectedGroupOfStudentsForSmsAdapter;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.GradeList;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.StudentListModel;
import info.hoang8f.android.segmented.SegmentedGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SmsEmailsFragment extends Fragment implements OnReloadListener {


    @BindView(R.id.txtEmergecyCommunicationTrip)
    TextView txtEmergecyCommunicationTrip;
    @BindView(R.id.card_emergency_contact_trip)
    CardView cardEmergencyContactTrip;
    @BindView(R.id.rb_grade)
    RadioButton rbGrade;
    @BindView(R.id.rb_class)
    RadioButton rbClass;
    @BindView(R.id.rb_group_of_student)
    RadioButton rbGroupOfStudent;
    @BindView(R.id.sgSe)
    SegmentedGroup sgSe;
    @BindView(R.id.txt_Grade)
    TextView txtGrade;
    @BindView(R.id.grade_img)
    ImageView gradeImg;
    @BindView(R.id.linear_Grade)
    RelativeLayout linearGrade;
    @BindView(R.id.txt_Class)
    TextView txtClass;
    @BindView(R.id.class_img)
    ImageView classImg;
    @BindView(R.id.linear_Class)
    RelativeLayout linearClass;
    @BindView(R.id.et_description)
    EditText etDescription;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.txt_edit)
    TextView txtEdit;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.layout_emergency_trip)
    CardView layoutEmergencyTrip;
    @BindView(R.id.txt_error)
    TextView txtError;
    @BindView(R.id.viewAnimator)
    ViewAnimator viewAnimator;
    Unbinder unbinder;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    private Call<GradeList> call;
    private List<String> gradeList = new ArrayList<>();
    private List<String> classList = new ArrayList<>();
    private ArrayList<GradeList.ResultBean.GradeBean> teacherBeans;
    private ListPopupWindow listPopupWindow;
    private String gradeId, classId;
    private AlertDialog alertDialog;
    private Call<StudentListModel> apiStudentList;
    private GroupOfStudentsForSmsAdapter adapterStudentList;
    private SelectedGroupOfStudentsForSmsAdapter adapterSelectedStudents;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sms_emails, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) getActivity()).setSubTitle(getString(R.string.send_sms_emails));
        ((HomeActivity) getActivity()).setOnReloadListener(this);

        Init();
    }

    private void Init() {

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        sgSe.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.rb_grade) {

                    linearClass.setVisibility(View.GONE);
                    linearGrade.setVisibility(View.VISIBLE);

                    adapterSelectedStudents = new SelectedGroupOfStudentsForSmsAdapter(getActivity());
                    recyclerView.setAdapter(adapterSelectedStudents);

                    txtEdit.setVisibility(View.GONE);

                    txtGrade.setText(getString(R.string.select_grade_label));
                    txtClass.setText(getString(R.string.select_class_label));

                } else if (checkedId == R.id.rb_class) {

                    linearClass.setVisibility(View.VISIBLE);
                    linearGrade.setVisibility(View.VISIBLE);

                    adapterSelectedStudents = new SelectedGroupOfStudentsForSmsAdapter(getActivity());
                    recyclerView.setAdapter(adapterSelectedStudents);

                    txtEdit.setVisibility(View.GONE);

                    txtGrade.setText(getString(R.string.select_grade_label));
                    txtClass.setText(getString(R.string.select_class_label));

                } else if (checkedId == R.id.rb_group_of_student) {

                    linearClass.setVisibility(View.VISIBLE);
                    linearGrade.setVisibility(View.VISIBLE);

                    txtGrade.setText(getString(R.string.select_grade_label));
                    txtClass.setText(getString(R.string.select_class_label));
                }
            }
        });

        ApiCall();
    }

    private void ApiCall() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();
        if (entity == null)
            return;

        call = AdminApp.getInstance().getApi().getClassSchedule(entity.getSchoolId(), entity.getSchoolYear());
        call.enqueue(new Callback<GradeList>() {
            @Override
            public void onResponse(Call<GradeList> call, Response<GradeList> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            teacherBeans = response.body().getResult().getGrade();
                        } else {
                            Utility.showSnackBar(viewAnimator, response.body().getMessage());
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_TEACHER_CLASS_SCHEDULE);
                        Utility.error(getActivity(), error.message());

                    }
                } catch (Exception e) {
                    if (getActivity() != null)
                        Utility.showSnackBar(viewAnimator, getString(R.string.somethingwrong));
                    e.printStackTrace();
                }

                viewAnimator.setDisplayedChild(1);

            }

            @Override
            public void onFailure(Call<GradeList> call, Throwable t) {

                if (!call.isCanceled()) {
                    System.out.println(t.getMessage());
                    Utility.showSnackBar(viewAnimator, App_Constants.NO_INTERNET);
                    viewAnimator.setDisplayedChild(1);
                }
            }
        });
    }

    private void getStudentList() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        getActivity().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        builder.setCancelable(true);
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.raw_group_of_student_dialog, null);
        builder.setView(view);

        final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        final ViewAnimator viewAnimator = (ViewAnimator) view.findViewById(R.id.viewAnimator);
        final TextView txtNoData = (TextView) view.findViewById(R.id.txt_no_data);

        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 5));

        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                adapterSelectedStudents = new SelectedGroupOfStudentsForSmsAdapter(getActivity());
                SmsEmailsFragment.this.recyclerView.setAdapter(adapterSelectedStudents);

                if (adapterStudentList != null && adapterStudentList.getSelectedStudent().size() > 0) {
                    adapterSelectedStudents.addItem(adapterStudentList.getSelectedStudent());
                    txtEdit.setVisibility(View.VISIBLE);
                } else {
                    txtEdit.setVisibility(View.GONE);
                    rbGrade.setChecked(true);
                }


                dialogInterface.dismiss();
            }
        });

        builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                rbGrade.setChecked(true);
                dialogInterface.dismiss();
            }
        });

        alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {

                CREDENTIAL.ResultBean bean = AdminApp.getInstance().getAdmin();
                apiStudentList = AdminApp.getInstance().getApi().getStudentList(bean.getSchoolId(), gradeId, classId, bean.getSchoolYear());
                apiStudentList.enqueue(new Callback<StudentListModel>() {
                    @Override
                    public void onResponse(Call<StudentListModel> call, Response<StudentListModel> response) {

                        try {
                            if (response.isSuccessful()) {

                                if (response.body().isStatus()) {

                                    adapterStudentList = new GroupOfStudentsForSmsAdapter(getActivity());
                                    recyclerView.setAdapter(adapterStudentList);
                                    adapterStudentList.setSingleChoiceMode(false);

                                    if (response.body().getResult() != null && response.body().getResult().size() > 0) {

                                        if (adapterSelectedStudents != null && adapterSelectedStudents.getItemCount() > 0) {

                                            for (StudentListModel.ResultBean bean : response.body().getResult()) {

                                                for (StudentListModel.ResultBean model : adapterSelectedStudents.getVisibleStudent()) {

                                                    if (bean.getEmailId() != null && model.getEmailId() != null && bean.getEmailId().equals(model.getEmailId())) {

                                                        bean.setSelected(true);
                                                    }
                                                }
                                            }
                                        }

                                        adapterStudentList.addItem(response.body().getResult());
                                        viewAnimator.setDisplayedChild(1);
                                    } else {

                                        viewAnimator.setDisplayedChild(2);
                                        txtNoData.setText(getString(R.string.no_data));
                                    }
                                } else {

                                    viewAnimator.setDisplayedChild(2);
                                    txtNoData.setText(getString(R.string.no_data));
                                }
                            } else {

                                if (alertDialog != null && alertDialog.isShowing())
                                    alertDialog.dismiss();

                                APIError error = APIError.parseError(response, getActivity(), App_Constants.API_STUDENT_LIST);
                                viewAnimator.setDisplayedChild(2);
                                txtNoData.setText(error.message());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            viewAnimator.setDisplayedChild(2);
                            txtNoData.setText(getString(R.string.somethingwrong));

                        }
                    }

                    @Override
                    public void onFailure(Call<StudentListModel> call, Throwable t) {

                        if (!call.isCanceled()) {
                            viewAnimator.setDisplayedChild(2);
                            txtNoData.setText(App_Constants.NO_INTERNET);
                        }
                    }
                });

            }
        });


        alertDialog.show();


        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.90);

        if (alertDialog != null && alertDialog.getWindow() != null) {
            alertDialog.getWindow().setLayout(screenWidth, ViewGroup.LayoutParams.WRAP_CONTENT); //set below the setContentview
        }
    }


    private void show_drop_down_to_select_option(RelativeLayout layout, final String which) {

        if (teacherBeans != null && teacherBeans.size() > 0) {

            listPopupWindow = new ListPopupWindow(getActivity());
            if (which.equals(App_Constants.GRADE)) {

                gradeList.clear();
                for (GradeList.ResultBean.GradeBean beans : teacherBeans) {
                    if (!gradeList.contains(beans.getGradeId()))
                        gradeList.add(beans.getGradeId());
                }

                listPopupWindow.setAdapter(new ArrayAdapter(getActivity(), R.layout.list_dropdown_item, gradeList));

            } else if (which.equals(App_Constants.CLASS)) {

                classList.clear();
                for (GradeList.ResultBean.GradeBean beans : teacherBeans) {

                    if (txtGrade.getText().toString().equals(beans.getGradeId())) {
                        if (!classList.contains(beans.getClassX()))
                            for (int i = 0; i < beans.getClassX().size(); i++)
                                classList.add(beans.getClassX().get(i).getClassId());
                    }
                }

                if (classList != null && classList.size() > 0) {
                    Collections.sort(classList, new Comparator<String>() {
                        public int compare(String obj1, String obj2) {
                            // ## Ascending order
                            return obj1.compareToIgnoreCase(obj2); // To compare string values
                        }
                    });
                }

                listPopupWindow.setAdapter(new ArrayAdapter(getActivity(), R.layout.list_dropdown_item, classList));
            }

            listPopupWindow.setAnchorView(layout);
            listPopupWindow.setWidth(ListPopupWindow.WRAP_CONTENT);
            listPopupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);

            listPopupWindow.setModal(true);
            listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    if (which.equals(App_Constants.GRADE)) {
                        txtGrade.setText(gradeList.get(i));
                        gradeId = gradeList.get(i);
                        txtClass.setText(getString(R.string.select_class_label));
                    } else if (which.equals(App_Constants.CLASS)) {
                        txtClass.setText(classList.get(i));
                        classId = classList.get(i);

                        if (sgSe.getCheckedRadioButtonId() == R.id.rb_group_of_student)
                            getStudentList();
                    }
                    listPopupWindow.dismiss();

                }
            });
            listPopupWindow.show();
        } else {
            Toast.makeText(getActivity(), getString(R.string.no_data), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onReload(String apiName) {

        if (apiName != null && isVisible()) {
            if (apiName.equalsIgnoreCase(App_Constants.API_TEACHER_CLASS_SCHEDULE)) {
                ApiCall();
            }
        }
    }

    @OnClick({R.id.txt_edit, R.id.linear_Grade, R.id.linear_Class})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txt_edit:
                getStudentList();
                break;
            case R.id.linear_Grade:
                show_drop_down_to_select_option(linearGrade, App_Constants.GRADE);
                break;
            case R.id.linear_Class:
                show_drop_down_to_select_option(linearClass, App_Constants.CLASS);
                break;
        }
    }
}
