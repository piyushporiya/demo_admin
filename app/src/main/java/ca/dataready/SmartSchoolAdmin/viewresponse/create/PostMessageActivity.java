/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.viewresponse.create;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ca.dataready.SmartSchoolAdmin.Listeners.OnStudentSelectedListener;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.Views.CustomLinearLayoutManager;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.classevents.create.CreateClassActivity;
import ca.dataready.SmartSchoolAdmin.fileselectoer.FileSelectionActivity;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.viewresponse.adapter.FilesAdapter;
import ca.dataready.SmartSchoolAdmin.viewresponse.adapter.StudentListAdapter;
import ca.dataready.SmartSchoolAdmin.viewresponse.adapter.StudentListDialogAdapter;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.FileModel;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.ImageUpload;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.SendCommunicationMessage;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.StudentListModel;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.GradeList;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostMessageActivity extends BaseActivity implements OnStudentSelectedListener {

    private static final int REQUEST_CAMERA = 111;
    private static final int CAMERA_REQUEST_CODE = 501;
    @BindView(R.id.txt_Grade)
    TextView txtGrade;
    @BindView(R.id.grade_img)
    ImageView gradeImg;
    @BindView(R.id.linear_Grade)
    RelativeLayout linearGrade;
    @BindView(R.id.txt_Class)
    TextView txtClass;
    @BindView(R.id.class_img)
    ImageView classImg;
    @BindView(R.id.linear_Section)
    RelativeLayout linearSection;
    @BindView(R.id.rv_Students)
    RecyclerView rvStudents;
    @BindView(R.id.txt_NoData)
    TextView txtNoData;
    @BindView(R.id.student_list_viewAnimator)
    ViewAnimator studentListViewAnimator;
    @BindView(R.id.title)
    EditText title;
    @BindView(R.id.msg)
    EditText msg;
    @BindView(R.id.sendfeedback)
    Button sendfeedback;
    ProgressDialog progressDialog;

    public static final String GRADE = "grade";
    public static final String CLASS = "class";
    public static final String TIME = "time";

    CREDENTIAL.ResultBean entity;
    Call<GradeList> call;
    Call<StudentListModel> studentListCall;
    Call<SendCommunicationMessage> sendCommMsgCall;
    List<GradeList.ResultBean.GradeBean> teacherBeans;
    ArrayList<String> gradeList = new ArrayList<>();
    ArrayList<String> classList = new ArrayList<>();
    ArrayList<String> timeList = new ArrayList<>();
    ListPopupWindow listPopupWindow;
    StudentListAdapter studentListAdapter;
    List<String> studentIdList = new ArrayList<>();
    boolean isVisible = false;

    List<StudentListModel.ResultBean> studentResults;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.llheader)
    LinearLayout llheader;
    @BindView(R.id.llTextArea)
    LinearLayout llTextArea;
    @BindView(R.id.parent_viewAnimator)
    ViewAnimator parentViewAnimator;
    boolean isSendMessageReload = false;

    CustomLinearLayoutManager linearLayoutManager;
    public static final int UPDATE_MESSAGES_BROADCAST = 909;
    @BindView(R.id.txt_edit)
    TextView txtEdit;
    @BindView(R.id.txt_attachment)
    TextView txtAttachment;
    @BindView(R.id.rvAttachments)
    RecyclerView rvAttachments;
    @BindView(R.id.llAttachments)
    LinearLayout llAttachments;
    private StudentListDialogAdapter studentListDialogAdapter;
    private List<StudentListModel.ResultBean> studentListBeans;
    private AlertDialog alertDialog;
    private FilesAdapter filesAdapter;
    private List<FileModel> selectedFiles = new ArrayList<>();
    List<SendMessageParams.FilesBean> files = new ArrayList<>();
    private Handler mSendMessagehandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_message);
        ButterKnife.bind(this);
        setHeaderInfo();
        Init();
    }

    private void setHeaderInfo() {

        setSupportActionBar(toolbar);

        if (AdminApp.getInstance().getAdmin() != null)
            getSupportActionBar().setTitle(AdminApp.getInstance().getAdmin().getSchoolName()+ " " + AdminApp.getInstance().getAdmin().getSchoolYear());
        else
            getSupportActionBar().setTitle(getString(R.string.app_name));

        getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + getString(R.string.post_messsage_view_response) + "</small>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }


    private void Init() {

        SpannableString styledString = new SpannableString(getString(R.string.view_edit));
        styledString.setSpan(new UnderlineSpan(), 0, styledString.length(), 0);
        txtEdit.setText(styledString);

        rvAttachments.setLayoutManager(new LinearLayoutManager(PostMessageActivity.this));

        linearLayoutManager = new CustomLinearLayoutManager(PostMessageActivity.this, LinearLayoutManager.HORIZONTAL, false);
        linearLayoutManager.setScrollEnabled(false);
        rvStudents.setLayoutManager(linearLayoutManager);
        //rvStudents.setNestedScrollingEnabled(false);
        entity = AdminApp.getInstance().getAdmin();
        if (entity != null) {
            ApiCall();
        }
    }

    private void ApiCall() {

        studentListViewAnimator.setDisplayedChild(2);
        txtNoData.setText(getString(R.string.select_grade_class_load_students));

        parentViewAnimator.setDisplayedChild(0);
        call = AdminApp.getInstance().getApi().getClassSchedule(entity.getSchoolId(), entity.getSchoolYear());
        call.enqueue(new Callback<GradeList>() {
            @Override
            public void onResponse(Call<GradeList> call, Response<GradeList> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            teacherBeans = response.body().getResult().getGrade();
                            parentViewAnimator.setDisplayedChild(1);
                            Log.d("List", "onResponse: " + response.body().getResult().getGrade());
                            //LoadStudentOnStartUp();

                        } else {

                            parentViewAnimator.setDisplayedChild(1);
                            Utility.showSnackBar(rvStudents, response.body().getMessage());

                        }

                    } else {

                        APIError error = APIError.parseError(response, PostMessageActivity.this, App_Constants.API_TEACHER_CLASS_SCHEDULE);
                        Utility.error(PostMessageActivity.this, error.message());
                        parentViewAnimator.setDisplayedChild(1);

                    }
                } catch (Exception e) {

                    Utility.showSnackBar(rvStudents, getString(R.string.somethingwrong));
                    e.printStackTrace();
                    parentViewAnimator.setDisplayedChild(1);
                }


            }

            @Override
            public void onFailure(Call<GradeList> call, Throwable t) {
                if (!call.isCanceled()) {

                    System.out.println(t.getMessage());
                    Utility.showSnackBar(rvStudents, App_Constants.NO_INTERNET);
                    parentViewAnimator.setDisplayedChild(1);
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }


    @OnClick({R.id.linear_Grade, R.id.linear_Section, R.id.txt_edit, R.id.iv_SpeechToText, R.id.img_attachment, R.id.iv_camera_AttachFile, R.id.sendfeedback})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linear_Grade:
                show_drop_down_to_select_option(linearGrade, GRADE);
                break;
            case R.id.linear_Section:
                show_drop_down_to_select_option(linearSection, CLASS);
                break;
            case R.id.txt_edit:
                getStudentListDialog();
                break;
            case R.id.iv_SpeechToText:
                promptSpeechInput();
                break;
            case R.id.img_attachment:
                Intent intent = new Intent(PostMessageActivity.this, FileSelectionActivity.class);
                startActivityForResult(intent, App_Constants.FILE_SELECT);
                break;
            case R.id.iv_camera_AttachFile:
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(PostMessageActivity.this, Manifest.permission.CAMERA)
                            == PackageManager.PERMISSION_DENIED) {

                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                CAMERA_REQUEST_CODE);
                    } else {

                        cameraIntent();
                    }
                } else {

                    cameraIntent();
                }
                break;
            case R.id.sendfeedback:

                if (title.getText().toString().isEmpty()) {
                    title.setError(getString(R.string.enter_title));
                } else if (msg.getText().toString().isEmpty()) {
                    msg.setError(getString(R.string.enter_msg));
                } else if (studentIdList.size() == 0) {
                    Utility.showSnackBar(sendfeedback, getString(R.string.select_student));
                } else {
                    if (selectedFiles != null && selectedFiles.size() > 0) {
                        files.clear();
                        for (int i = 0; i < selectedFiles.size(); i++) {
                            if (i == 0)
                                UploadFile(true, selectedFiles.get(i).getPath());
                            else
                                UploadFile(false, selectedFiles.get(i).getPath());
                        }

                        mSendMessagehandler.postDelayed(runnable, 1000);

                    } else {
                        sendMessage();
                    }
                }

                break;
        }
    }

    private void cameraIntent() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);

    }

    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, App_Constants.REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Utility.showToast(PostMessageActivity.this, getString(R.string.speech_not_supported));
        }
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {

            if (selectedFiles != null && selectedFiles.size() > 0 && files.size() == selectedFiles.size()) {
                sendMessage();
            } else {
                mSendMessagehandler.postDelayed(runnable, 5000);
            }
        }
    };


    private void UploadFile(final boolean isFirstTime, String imageName) {

        if (isFirstTime)
            Utility.showProgress(PostMessageActivity.this, getString(R.string.processing));


        File file = new File(imageName);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        Call<ImageUpload> call = AdminApp.getInstance().getApi().upload(body);

        call.enqueue(new Callback<ImageUpload>() {
            @Override
            public void onResponse(Call<ImageUpload> call, Response<ImageUpload> response) {

                try {
                    if (response.isSuccessful()) {

                        if (response.body().isStatus()) {
                            if (response.body().getResult() != null) {
                                String image = response.body().getResult().getFilepath();
                                System.out.println(image);

                                SendMessageParams.FilesBean bean = new SendMessageParams.FilesBean();
                                bean.setFilePath(image);
                                files.add(bean);

                            } else {
                                Utility.hideProgress();
                                Toast.makeText(PostMessageActivity.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Utility.hideProgress();
                            Toast.makeText(PostMessageActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
                    Utility.hideProgress();
                    e.printStackTrace();
                    Toast.makeText(PostMessageActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ImageUpload> call, Throwable t) {
                Utility.hideProgress();
                Log.e("ERROR UPLOAD", "" + t.getMessage());
                Toast.makeText(PostMessageActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void show_drop_down_to_select_option(RelativeLayout layout, final String which) {

        if (teacherBeans != null && teacherBeans.size() > 0) {

            listPopupWindow = new ListPopupWindow(PostMessageActivity.this);
            if (which.equals(GRADE)) {

                gradeList.clear();
                for (GradeList.ResultBean.GradeBean beans : teacherBeans) {
                    if (!gradeList.contains(beans.getGradeId()))
                        gradeList.add(beans.getGradeId());
                }

                listPopupWindow.setAdapter(new ArrayAdapter(PostMessageActivity.this, R.layout.list_dropdown_item, gradeList));

            } else if (which.equals(CLASS)) {

                classList.clear();
                for (GradeList.ResultBean.GradeBean beans : teacherBeans) {


                    if (txtGrade.getText().toString().equals(beans.getGradeId())) {
                        if (!classList.contains(beans.getClassX()))
                            for (int i = 0; i < beans.getClassX().size(); i++)
                                classList.add(beans.getClassX().get(i).getClassId());
                    }
                }

                if (classList != null && classList.size() > 0) {
                    Collections.sort(classList, new Comparator<String>() {
                        public int compare(String obj1, String obj2) {
                            // ## Ascending order
                            return obj1.compareToIgnoreCase(obj2); // To compare string values
                        }
                    });
                }

                listPopupWindow.setAdapter(new ArrayAdapter(PostMessageActivity.this, R.layout.list_dropdown_item, classList));
            }

            listPopupWindow.setAnchorView(layout);
            listPopupWindow.setWidth(ListPopupWindow.WRAP_CONTENT);
            listPopupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);

            listPopupWindow.setModal(true);
            listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    if (which.equals(GRADE)) {
                        txtGrade.setText(gradeList.get(i));
                        txtClass.setText(getString(R.string.select_class_label));
                    } else if (which.equals(CLASS)) {
                        txtClass.setText(classList.get(i));
                        //getStudentList();
                        getStudentListDialog();
                    }
                    listPopupWindow.dismiss();

                }
            });
            listPopupWindow.show();
        }
    }

    private void getStudentList() {

        studentListViewAnimator.setVisibility(View.VISIBLE);
        studentListViewAnimator.setDisplayedChild(0);

        studentListCall = AdminApp.getInstance().getApi().getStudentList(entity.getSchoolId(), txtGrade.getText().toString(),
                txtClass.getText().toString(), AdminApp.getInstance().getAdmin().getSchoolYear());
        studentListCall.enqueue(new Callback<StudentListModel>() {
            @Override
            public void onResponse(Call<StudentListModel> call, Response<StudentListModel> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            studentListAdapter = new StudentListAdapter(PostMessageActivity.this);
                            rvStudents.setAdapter(studentListAdapter);
                            studentListAdapter.setOnStudentSelectedListener(PostMessageActivity.this);

                            studentResults = response.body().getResult();

                            if (studentResults != null && studentResults.size() > 0) {

                                studentListViewAnimator.setDisplayedChild(1);
                                studentListAdapter.addItem(studentResults);

                            } else {

                                studentListViewAnimator.setDisplayedChild(2);
                                txtNoData.setText(getString(R.string.no_data));
                            }

                        } else {

                            studentListViewAnimator.setDisplayedChild(2);
                            txtNoData.setText(response.body().getMessage());
                        }

                    } else {

                        APIError error = APIError.parseError(response, PostMessageActivity.this, App_Constants.API_STUDENT_LIST);
                        studentListViewAnimator.setDisplayedChild(2);
                        txtNoData.setText(error.message());
                    }
                } catch (Exception e) {

                    studentListViewAnimator.setDisplayedChild(2);
                    txtNoData.setText(getString(R.string.somethingwrong));
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<StudentListModel> call, Throwable t) {
                if (!call.isCanceled()) {
                    studentListViewAnimator.setDisplayedChild(2);
                    txtNoData.setText(App_Constants.NO_INTERNET);
                    System.out.println(t.getMessage());
                }
            }
        });
    }


    private void getStudentListDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(PostMessageActivity.this);

        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        builder.setCancelable(false);
        View view = LayoutInflater.from(PostMessageActivity.this).inflate(R.layout.raw_hw_student_list_dialog, null);
        builder.setView(view);

        final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        final ViewAnimator viewAnimator = (ViewAnimator) view.findViewById(R.id.viewAnimator);
        final TextView txtNoData = (TextView) view.findViewById(R.id.txt_no_data);

        recyclerView.setLayoutManager(new GridLayoutManager(PostMessageActivity.this, 5));

        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();

                studentListAdapter = new StudentListAdapter(PostMessageActivity.this);
                rvStudents.setAdapter(studentListAdapter);

                if (studentListDialogAdapter != null) {

                    studentListViewAnimator.setVisibility(View.VISIBLE);
                    txtEdit.setVisibility(View.VISIBLE);
                    studentListAdapter.addItem(studentListDialogAdapter.getSelectedStudent());
                    studentListViewAnimator.setDisplayedChild(1);
                }

            }
        });

        builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();
            }
        });

        alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {

                studentListCall = AdminApp.getInstance().getApi().getStudentList(entity.getSchoolId(), txtGrade.getText().toString(),
                        txtClass.getText().toString(), AdminApp.getInstance().getAdmin().getSchoolYear());
                studentListCall.enqueue(new Callback<StudentListModel>() {
                    @Override
                    public void onResponse(Call<StudentListModel> call, Response<StudentListModel> response) {

                        try {
                            if (response.isSuccessful()) {
                                if (response.body().isStatus()) {

                                    studentListDialogAdapter = new StudentListDialogAdapter(PostMessageActivity.this);
                                    recyclerView.setAdapter(studentListDialogAdapter);
                                    studentListDialogAdapter.setSingleChoiceMode(true);
                                    studentListDialogAdapter.setOnStudentSelectedListener(PostMessageActivity.this);

                                    studentListBeans = response.body().getResult();

                                    if (studentListBeans != null && studentListBeans.size() > 0) {

                                        if (studentListAdapter != null && studentListAdapter.getItemCount() > 0) {

                                            for (StudentListModel.ResultBean bean : studentListBeans) {

                                                for (StudentListModel.ResultBean model : studentListAdapter.getVisibleStudent()) {

                                                    if (bean.getStudentId().equals(model.getStudentId())) {

                                                        bean.setSelected(true);
                                                    }
                                                }
                                            }

                                            studentListDialogAdapter.addItem(studentListBeans);

                                        } else {

                                            studentListDialogAdapter.addItem(response.body().getResult());
                                        }
                                        viewAnimator.setDisplayedChild(1); // recyclerview visible

                                    } else {

                                        viewAnimator.setDisplayedChild(2);  // empty view visible
                                        txtNoData.setText(getString(R.string.no_data));
                                    }

                                } else {

                                    viewAnimator.setDisplayedChild(2); // empty view visible
                                    txtNoData.setText(response.body().getMessage());
                                }

                            } else {

                                APIError error = APIError.parseError(response, PostMessageActivity.this, App_Constants.API_STUDENT_LIST);
                                viewAnimator.setDisplayedChild(2); // empty view visible
                                txtNoData.setText(error.message());

                                if (alertDialog != null && alertDialog.isShowing())
                                    alertDialog.dismiss();
                            }
                        } catch (Exception e) {

                            viewAnimator.setDisplayedChild(2); // empty view visible
                            txtNoData.setText(getString(R.string.somethingwrong));
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<StudentListModel> call, Throwable t) {
                        if (!call.isCanceled()) {
                            viewAnimator.setDisplayedChild(2); // empty view visible
                            txtNoData.setText(App_Constants.NO_INTERNET);
                            System.out.println(t.getMessage());
                        }
                    }
                });

            }
        });


        alertDialog.show();


        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.90);

        if (alertDialog != null && alertDialog.getWindow() != null) {
            alertDialog.getWindow().setLayout(screenWidth, ViewGroup.LayoutParams.WRAP_CONTENT); //set below the setContentview
        }

    }


    private void sendMessage() {

        if (!(selectedFiles != null && selectedFiles.size() > 0))
            Utility.showProgress(PostMessageActivity.this, "Processing...");

        SendMessageParams params = new SendMessageParams();
        params.setClassId(txtClass.getText().toString());
        params.setGradeId(txtGrade.getText().toString());
        params.setMessage(msg.getText().toString());
        params.setSchoolYear(AdminApp.getInstance().getAdmin().getSchoolYear());
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
        params.setStudentId(studentIdList.get(0));
        params.setCommType("school");
        params.setTeacherId(AdminApp.getInstance().getAdmin().getId());
        params.setTitle(title.getText().toString());
        if (selectedFiles != null && selectedFiles.size() > 0) {
            params.setFiles(files);
            for (SendMessageParams.FilesBean s : files) {
                Log.e("========Files========", s.getFilePath());
            }
        }


        sendCommMsgCall = AdminApp.getInstance().getApi().sendMessage(params);
        sendCommMsgCall.enqueue(new Callback<SendCommunicationMessage>() {
            @Override
            public void onResponse(Call<SendCommunicationMessage> call, Response<SendCommunicationMessage> response) {

                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            Utility.showToast(PostMessageActivity.this, response.body().getMessage());
                            setIntentData();

                        } else {
                            Utility.showSnackBar(rvStudents, response.body().getMessage());
                        }

                    } else {

                        APIError error = APIError.parseError(response, PostMessageActivity.this, App_Constants.API_SEND_COMMUNICATION_MESSAGE);
                        Utility.error(PostMessageActivity.this, error.message());

                    }
                } catch (Exception e) {

                    Utility.showSnackBar(rvStudents, getString(R.string.somethingwrong));
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<SendCommunicationMessage> call, Throwable t) {
                if (!call.isCanceled()) {

                    Utility.hideProgress();
                    System.out.println(t.getMessage());
                    Utility.showSnackBar(rvStudents, App_Constants.NO_INTERNET);
                }
            }
        });
    }

    private void setIntentData() {

        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        onBackPressed();
    }


    private void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    private void showProgress() {
        progressDialog = new ProgressDialog(PostMessageActivity.this);
        progressDialog.setMessage(getString(R.string.processing));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == APIError.UPDATE_TOKEN && resultCode == RESULT_OK) {
            if (data != null) {
                String apiName = data.getStringExtra("api");
                onReload(apiName);
            }
        } else if (requestCode == App_Constants.FILE_SELECT && resultCode == RESULT_OK) {

            ArrayList<File> Files = (ArrayList<File>) data.getSerializableExtra(FileSelectionActivity.FILES_TO_UPLOAD);
            for (File file : Files) {
                String uri = file.getAbsolutePath();
                Log.e("PATH========", uri);
                if (file.toString().contains("/")) {
                    selectedFiles.add(new FileModel(file.toString().substring(file.toString().lastIndexOf("/") + 1), uri));
                }

            }
            // txtFileName.setVisibility(View.VISIBLE);
            // txtFileName.setText(Files.size() + " attachments");

            llAttachments.setVisibility(View.VISIBLE);
            rvAttachments.addItemDecoration(new DividerItemDecoration(PostMessageActivity.this, DividerItemDecoration.VERTICAL));
            filesAdapter = new FilesAdapter(PostMessageActivity.this);
            rvAttachments.setAdapter(filesAdapter);
            filesAdapter.addItem(selectedFiles);

        } else if (requestCode == App_Constants.REQ_CODE_SPEECH_INPUT && resultCode == RESULT_OK) {

            if (null != data) {

                ArrayList<String> result = data
                        .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                msg.setText(result.get(0));
            }
        } else if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {

            Bitmap imageBitmap = null;
            Bundle extras = data.getExtras();
            if (extras != null) {
                imageBitmap = (Bitmap) extras.get("data");

            }

            Uri tempUri = getImageUri(PostMessageActivity.this, imageBitmap);
            File finalFile = new File(getRealPathFromURI(tempUri));
            String picturePath = finalFile.toString();
            System.out.println(finalFile);

            selectedFiles.add(new FileModel(picturePath.substring(picturePath.lastIndexOf("/") + 1), picturePath));

            llAttachments.setVisibility(View.VISIBLE);
            rvAttachments.addItemDecoration(new DividerItemDecoration(PostMessageActivity.this, DividerItemDecoration.VERTICAL));
            filesAdapter = new FilesAdapter(PostMessageActivity.this);
            rvAttachments.setAdapter(filesAdapter);
            filesAdapter.addItem(selectedFiles);

        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public void onReload(String apiName) {

        Log.e("OnReload", "Chat Screen");

        switch (apiName) {
            case App_Constants.API_TEACHER_CLASS_SCHEDULE:
                ApiCall();
                break;
            case App_Constants.API_STUDENT_LIST:
                getStudentListDialog();
                break;
            case App_Constants.API_SEND_COMMUNICATION_MESSAGE:
                sendfeedback.performClick();
                break;
        }

    }


    @Override
    protected void onResume() {
        isVisible = true;
        super.onResume();
    }

    @Override
    protected void onPause() {
        isVisible = false;
        super.onPause();
    }

    @Override
    public void onStudentSelected(boolean makeEnabled, List<String> studentList) {

        if (isVisible) {
            Log.e("StUdentList", studentList.toString());
            studentIdList.clear();
            studentIdList.addAll(studentList);
            //  studentListAdapter.makeSingleChoice();
            // studentListDialogAdapter.makeSingleChoice();
        }
    }


    public void removeUploadItem(FileModel file) {

        if (selectedFiles != null && selectedFiles.size() > 0) {
            selectedFiles.remove(file);
        }

        if (filesAdapter != null && filesAdapter.getItemCount() == 0) {
            llAttachments.setVisibility(View.GONE);
        }
    }

    public static class SendMessageParams {


        /**
         * classId : string
         * files : [{"filePath":"string"}]
         * gradeId : string
         * message : string
         * messageId : string
         * schoolId : string
         * schoolYear : YYYY-YYYY
         * studentId : string
         * teacherId : string
         * title : string
         */

        private String classId;
        private String gradeId;
        private String message;
        private String messageId;
        private String schoolId;
        private String schoolYear;
        private String studentId;
        private String teacherId;
        private String commType;
        private String title;
        private List<FilesBean> files;

        public String getCommType() {
            return commType;
        }

        public void setCommType(String commType) {
            this.commType = commType;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getGradeId() {
            return gradeId;
        }

        public void setGradeId(String gradeId) {
            this.gradeId = gradeId;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getMessageId() {
            return messageId;
        }

        public void setMessageId(String messageId) {
            this.messageId = messageId;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getSchoolYear() {
            return schoolYear;
        }

        public void setSchoolYear(String schoolYear) {
            this.schoolYear = schoolYear;
        }

        public String getStudentId() {
            return studentId;
        }

        public void setStudentId(String studentId) {
            this.studentId = studentId;
        }

        public String getTeacherId() {
            return teacherId;
        }

        public void setTeacherId(String teacherId) {
            this.teacherId = teacherId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public List<FilesBean> getFiles() {
            return files;
        }

        public void setFiles(List<FilesBean> files) {
            this.files = files;
        }

        public static class FilesBean {
            /**
             * filePath : string
             */

            private String filePath;

            public String getFilePath() {
                return filePath;
            }

            public void setFilePath(String filePath) {
                this.filePath = filePath;
            }
        }
    }

    public int getScreenWidth() {

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.widthPixels;

    }

    public int getScreenHeight() {

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.heightPixels;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == CAMERA_REQUEST_CODE) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(PostMessageActivity.this, "camera permission granted", Toast.LENGTH_LONG).show();
                cameraIntent();

            } else {
                Toast.makeText(PostMessageActivity.this, "Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();

            }

        }
    }
}
