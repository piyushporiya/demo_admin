/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.viewresponse.model;

import java.util.List;

/**
 * Created by social_jaydeep on 03/10/17.
 */

public class ThreadCommunicationModel {


    /**
     * message : Message informations are below
     * status : true
     * authtoken : null
     * result : [{"studentLastName":"Singh","gradeId":"3","staffLastName":"One","messageId":"1499241270500","message":"ok read","title":"test","sequenceId":2,"studentProfilePic":"/schoolapp/images/s08.jpg","studentId":"1214","classId":"A","createdDate":"2017-07-05","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/sample.jpg","schoolId":"129","schoolYear":"2017","from":"teacherone@dataready.in","studentFirstName":"Rahul"},{"studentLastName":"Singh","gradeId":"3","staffLastName":"One","messageId":"1499241270500","message":"test","title":"test","sequenceId":1,"studentProfilePic":"/schoolapp/images/s08.jpg","studentId":"1214","classId":"A","createdDate":"2017-07-05","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/sample.jpg","schoolId":"129","schoolYear":"2017","from":"teacherone@dataready.in","studentFirstName":"Rahul"}]
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private Object authtoken;
    private Object authtokenexpires;
    private List<ResultBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Object getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(Object authtoken) {
        this.authtoken = authtoken;
    }

    public Object getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(Object authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * studentLastName : Singh
         * gradeId : 3
         * staffLastName : One
         * messageId : 1499241270500
         * message : ok read
         * title : test
         * sequenceId : 2
         * studentProfilePic : /schoolapp/images/s08.jpg
         * studentId : 1214
         * classId : A
         * createdDate : 2017-07-05
         * teacherId : teacherone@dataready.in
         * staffFirstName : Teacher
         * staffProfilePic : /schoolapp/images/sample.jpg
         * schoolId : 129
         * schoolYear : 2017
         * from : teacherone@dataready.in
         * studentFirstName : Rahul
         */

        private String studentLastName;
        private String gradeId;
        private String staffLastName;
        private String messageId;
        private String message;
        private String title;
        private int sequenceId;
        private String studentProfilePic;
        private String studentId;
        private String classId;
        private String createdDate;
        private String teacherId;
        private String staffFirstName;
        private String staffProfilePic;
        private String schoolId;
        private String schoolYear;
        private String from;
        private String studentFirstName;
        private List<FilesBean> files;
        private boolean isAttachment=false;

        public boolean isAttachment() {
            return isAttachment;
        }

        public void setIsAttachment(boolean attachment) {
            isAttachment = attachment;
        }

        public String getStudentLastName() {
            return studentLastName;
        }

        public void setStudentLastName(String studentLastName) {
            this.studentLastName = studentLastName;
        }

        public String getGradeId() {
            return gradeId;
        }

        public void setGradeId(String gradeId) {
            this.gradeId = gradeId;
        }

        public String getStaffLastName() {
            return staffLastName;
        }

        public void setStaffLastName(String staffLastName) {
            this.staffLastName = staffLastName;
        }

        public String getMessageId() {
            return messageId;
        }

        public void setMessageId(String messageId) {
            this.messageId = messageId;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getSequenceId() {
            return sequenceId;
        }

        public void setSequenceId(int sequenceId) {
            this.sequenceId = sequenceId;
        }

        public String getStudentProfilePic() {
            return studentProfilePic;
        }

        public void setStudentProfilePic(String studentProfilePic) {
            this.studentProfilePic = studentProfilePic;
        }

        public String getStudentId() {
            return studentId;
        }

        public void setStudentId(String studentId) {
            this.studentId = studentId;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

        public String getTeacherId() {
            return teacherId;
        }

        public void setTeacherId(String teacherId) {
            this.teacherId = teacherId;
        }

        public String getStaffFirstName() {
            return staffFirstName;
        }

        public void setStaffFirstName(String staffFirstName) {
            this.staffFirstName = staffFirstName;
        }

        public String getStaffProfilePic() {
            return staffProfilePic;
        }

        public void setStaffProfilePic(String staffProfilePic) {
            this.staffProfilePic = staffProfilePic;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getSchoolYear() {
            return schoolYear;
        }

        public void setSchoolYear(String schoolYear) {
            this.schoolYear = schoolYear;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getStudentFirstName() {
            return studentFirstName;
        }

        public void setStudentFirstName(String studentFirstName) {
            this.studentFirstName = studentFirstName;
        }

        public List<FilesBean> getFiles() {
            return files;
        }

        public void setFiles(List<FilesBean> files) {
            this.files = files;
        }

        public static class FilesBean {
            /**
             * filePath : /schoolapp/images/354fa4c9-129e-4720-b29c-cee8ffb410a6.pdf
             */

            private String filePath;

            public String getFilePath() {
                return filePath;
            }

            public void setFilePath(String filePath) {
                this.filePath = filePath;
            }
        }
    }
}
