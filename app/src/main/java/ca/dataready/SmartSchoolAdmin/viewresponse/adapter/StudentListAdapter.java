/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.viewresponse.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.Listeners.OnStudentSelectedListener;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.server.AppApi;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.StudentListModel;


/**
 * Created by social_jaydeep on 27/09/17.
 */

public class StudentListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<StudentListModel.ResultBean> beans;
    private List<String> studentIdList = new ArrayList<>();
    private OnStudentSelectedListener onStudentSelectedListener;
    private String studentId;
    private int width, height;

    public StudentListAdapter(Context context) {
        this.context = context;
        beans = new ArrayList<>();
      /*  width = screenWidth;
        height = screenHeight;*/
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(displaymetrics);
        width = displaymetrics.widthPixels;
        height = displaymetrics.heightPixels;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new StudentViewHolder(LayoutInflater.from(context).inflate(R.layout.row_students_for_attendance, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        final StudentListModel.ResultBean bean = beans.get(position);

        if (holder instanceof StudentViewHolder) {

            ((StudentViewHolder) holder).txtStudentName.setText(bean.getFirstName() + " " + bean.getLastName());
            ((StudentViewHolder) holder).ivStudentImg.setImageURI(Uri.parse(AppApi.BASE_URL + bean.getProfilePic()));

            if (bean.isSelected()) {
                ((StudentViewHolder) holder).ivSelection.setImageResource(R.drawable.ic_present);
            } else {
                ((StudentViewHolder) holder).ivSelection.setImageResource(R.drawable.ic_normal);
            }

            if (bean.isEnabled()) {
                ((StudentViewHolder) holder).ivSelection.setEnabled(true);
            } else {
                ((StudentViewHolder) holder).ivSelection.setEnabled(false);
            }

            ((StudentViewHolder) holder).ivSelection.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    bean.setSelected(!bean.isSelected());

                    if (bean.isSelected()) {
                        ((StudentViewHolder) holder).ivSelection.setImageResource(R.drawable.ic_present);
                        studentIdList.add(bean.getStudentId());
                        setSubmitEnable();
                    } else {
                        ((StudentViewHolder) holder).ivSelection.setImageResource(R.drawable.ic_normal);
                        studentIdList.remove(bean.getStudentId());
                        setSubmitEnable();
                    }


                }
            });

        }
    }

    public void addItem(List<StudentListModel.ResultBean> results) {

        Collections.sort(results, new Comparator<StudentListModel.ResultBean>() {
            public int compare(StudentListModel.ResultBean obj1, StudentListModel.ResultBean obj2) {
                return obj1.getFirstName().compareToIgnoreCase(obj2.getFirstName()); // To compare string values
            }
        });

        beans.addAll(results);
        notifyDataSetChanged();

    }

    public void setOnStudentSelectedListener(OnStudentSelectedListener onStudentSelectedListener) {
        this.onStudentSelectedListener = onStudentSelectedListener;
    }

    private void setSubmitEnable() {

        if (studentIdList != null && studentIdList.size() > 0) {
            if (onStudentSelectedListener != null)
                onStudentSelectedListener.onStudentSelected(true, studentIdList);
        } else {
            if (onStudentSelectedListener != null)
                onStudentSelectedListener.onStudentSelected(false, studentIdList);
        }
    }

    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }

    public void fallBack() {

        for (StudentListModel.ResultBean bean : beans) {
            bean.setSelected(false);
        }
        studentIdList.clear();
        notifyDataSetChanged();
    }

    public void makeSingleChoice() {

        if (studentIdList != null && studentIdList.size() > 1) {

            for (int i = 0; i < beans.size(); i++) {

                if (beans.get(i).getStudentId().equals(studentIdList.get(0))) {
                    beans.get(i).setSelected(false);
                    studentId = beans.get(i).getStudentId();
                }
            }
            if (studentId != null)
                studentIdList.remove(studentId);
            notifyDataSetChanged();
        }
    }

    public void makeAllSelected() {


        for (StudentListModel.ResultBean bean : beans) {
            bean.setSelected(true);
            studentIdList.add(bean.getStudentId());
        }

        notifyDataSetChanged();

        setSubmitEnable();
    }

   /* public List<AttendanceFragment.PostAttandanceParams.StudentInfoLIstBean> getAllStudentListInfo() {

        List<AttendanceFragment.PostAttandanceParams.StudentInfoLIstBean> allStudentList = new ArrayList<>();

        for (StudentListModel.ResultBean bean : beans) {

            AttendanceFragment.PostAttandanceParams.StudentInfoLIstBean model = new AttendanceFragment.PostAttandanceParams.StudentInfoLIstBean();
            model.setProfilePic(bean.getProfilePic());
            model.setStudentName(bean.getFirstName() + " " + bean.getLastName());
            model.setStudentId(bean.getStudentId());
            model.setSubjectAttendance(bean.isSelected());
            allStudentList.add(model);
        }

        return allStudentList;

    }*/

    public List<StudentListModel.ResultBean> getVisibleStudent() {

        List<StudentListModel.ResultBean> visibleStudentsList = new ArrayList<>();
        for (StudentListModel.ResultBean bean : beans) {
            visibleStudentsList.add(bean);
        }

        return visibleStudentsList;
    }

    public List<String> getSelectedStudents() {

        List<String> selectedStudentsList = new ArrayList<>();
        for (StudentListModel.ResultBean bean : beans) {
            if (bean.isSelected())
                selectedStudentsList.add(bean.getStudentId());
        }

        return selectedStudentsList;
    }


    class StudentViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.my_image_view)
        SimpleDraweeView ivStudentImg;
        @BindView(R.id.iv_Selection)
        ImageView ivSelection;
        @BindView(R.id.txt_StudentName)
        TextView txtStudentName;
        @BindView(R.id.main_Linear)
        LinearLayout mainLinear;


        //R.layout.row_students_for_attendance

        public StudentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(width / 7, LinearLayout.LayoutParams.WRAP_CONTENT);
            mainLinear.setLayoutParams(lp);
        }
    }
}
