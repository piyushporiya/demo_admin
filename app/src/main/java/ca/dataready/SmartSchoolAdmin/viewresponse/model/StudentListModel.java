/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.viewresponse.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by social_jaydeep on 27/09/17.
 */

public class StudentListModel {


    /**
     * message : Student informations are below
     * status : true
     * authtoken : null
     * result : [{"lastName":"Dargan","gradeId":"3","profilePic":"/schoolapp/images/1291217.jpg","emailId":"ajaydargan@hotmail.com","parentId":null,"phoneNo":"2147483647","studentId":"1217","firstName":"Ajay","classId":"A","dob":"1987-02-01","schoolId":"129","schoolYear":"2017","routeDetails":{"name":"SP9","stopId":"1","id":"9","uniqueId":"694092"}},{"lastName":"Agarwal","gradeId":"3","profilePic":"/schoolapp/images/1291218.jpg","emailId":"rajeshagarwal@hotmail.com","parentId":null,"phoneNo":"2147483647","studentId":"1218","firstName":"Rajesh","classId":"A","dob":"1987-02-01","schoolId":"129","schoolYear":"2017","routeDetails":{"name":"SP9","stopId":"1","id":"9","uniqueId":"694092"}},{"lastName":"Choubey","gradeId":"3","profilePic":"/schoolapp/images/1291214.jpg","emailId":"ajeetchoubey@hotmail.com","parentId":null,"phoneNo":"2147483647","studentId":"1214","firstName":"Ajeet","classId":"A","dob":"1987-02-01","schoolId":"129","schoolYear":"2017","routeDetails":{"name":"SP9","stopId":"1","id":"9","uniqueId":"694092"}},{"lastName":"Badola","gradeId":"3","profilePic":"/schoolapp/images/1291215.jpg","emailId":"himanshubadola@hotmail.com","parentId":null,"phoneNo":"2147483647","studentId":"1215","firstName":"Himanshu","classId":"A","dob":"1987-02-01","schoolId":"129","schoolYear":"2017","routeDetails":{"name":"SP9","stopId":"1","id":"9","uniqueId":"694092"}},{"lastName":"Singh","gradeId":"3","profilePic":"/schoolapp/images/1291221.jpg","emailId":"sahilsingh@hotmail.com","parentId":null,"phoneNo":"2147483647","studentId":"1221","firstName":"Sahil","classId":"A","dob":"1987-02-01","schoolId":"129","schoolYear":"2017","routeDetails":{"name":"SP9","stopId":"1","id":"9","uniqueId":"694092"}},{"lastName":"Depuria","gradeId":"3","profilePic":"/schoolapp/images/1291222.jpg","emailId":"anoopdepuria@hotmail.com","parentId":null,"phoneNo":"2147483647","studentId":"1222","firstName":"Anoop","classId":"A","dob":"1987-02-01","schoolId":"129","schoolYear":"2017","routeDetails":{"name":"SP9","stopId":"1","id":"9","uniqueId":"694092"}},{"lastName":"Tiwari","gradeId":"3","profilePic":"/schoolapp/images/1291219.jpg","emailId":"vijaytiwari@hotmail.com","parentId":null,"phoneNo":"2147483647","studentId":"1219","firstName":"Vijay","classId":"A","dob":"1987-02-01","schoolId":"129","schoolYear":"2017","routeDetails":{"name":"SP9","stopId":"1","id":"9","uniqueId":"694092"}},{"lastName":"Singh","gradeId":"3","profilePic":"/schoolapp/images/1291220.jpg","emailId":"rahulsingh@hotmail.com","parentId":null,"phoneNo":"2147483647","studentId":"1220","firstName":"Rahul","classId":"A","dob":"1987-02-01","schoolId":"129","schoolYear":"2017","routeDetails":{"name":"SP9","stopId":"1","id":"9","uniqueId":"694092"}},{"lastName":"Verma","gradeId":"3","profilePic":"/schoolapp/images/1291223.jpg","emailId":"brijeshverma@hotmail.com","parentId":null,"phoneNo":"2147483647","studentId":"1223","firstName":"Brijesh","classId":"A","dob":"1987-02-01","schoolId":"129","schoolYear":"2017","routeDetails":{"name":"SP9","stopId":"1","id":"9","uniqueId":"694092"}},{"lastName":"srdrpk","gradeId":"3","profilePic":"/schoolapp/images/1291216.jpg","emailId":"davidsrdrpk@hotmail.com","parentId":null,"phoneNo":"2147483647","studentId":"1216","firstName":"David","classId":"A","dob":"1987-02-01","schoolId":"129","schoolYear":"2017","routeDetails":{"name":"SP9","stopId":"1","id":"9","uniqueId":"694092"}}]
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private String authtokenexpires;
    private List<ResultBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean implements Parcelable {
        /**
         * lastName : Dargan
         * gradeId : 3
         * profilePic : /schoolapp/images/1291217.jpg
         * emailId : ajaydargan@hotmail.com
         * parentId : null
         * phoneNo : 2147483647
         * studentId : 1217
         * firstName : Ajay
         * classId : A
         * dob : 1987-02-01
         * schoolId : 129
         * schoolYear : 2017
         * routeDetails : {"name":"SP9","stopId":"1","id":"9","uniqueId":"694092"}
         */

        private String lastName;
        private String gradeId;
        private String profilePic;
        private String emailId;
        private String parentId;
        private String phoneNo;
        private String studentId;
        private String firstName;
        private String classId;
        private String dob;
        private String id;
        private String schoolId;
        private String schoolYear;
        private RouteDetailsBean routeDetails;
        private boolean isSelected = false;
        private boolean isEnabled = true;
        private boolean isPresent = false;
        private boolean isLate = false;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public boolean isLate() {
            return isLate;
        }

        public void setLate(boolean late) {
            isLate = late;
        }

        public boolean isPresent() {
            return isPresent;
        }

        public void setPresent(boolean present) {
            isPresent = present;
        }

        public boolean isEnabled() {
            return isEnabled;
        }

        public void setEnabled(boolean enabled) {
            isEnabled = enabled;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }


        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getGradeId() {
            return gradeId;
        }

        public void setGradeId(String gradeId) {
            this.gradeId = gradeId;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        public String getEmailId() {
            return emailId;
        }

        public void setEmailId(String emailId) {
            this.emailId = emailId;
        }

        public String getParentId() {
            return parentId;
        }

        public void setParentId(String parentId) {
            this.parentId = parentId;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public String getStudentId() {
            return studentId;
        }

        public void setStudentId(String studentId) {
            this.studentId = studentId;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getSchoolYear() {
            return schoolYear;
        }

        public void setSchoolYear(String schoolYear) {
            this.schoolYear = schoolYear;
        }

        public RouteDetailsBean getRouteDetails() {
            return routeDetails;
        }

        public void setRouteDetails(RouteDetailsBean routeDetails) {
            this.routeDetails = routeDetails;
        }

        public static class RouteDetailsBean implements Parcelable {
            /**
             * name : SP9
             * stopId : 1
             * id : 9
             * uniqueId : 694092
             */

            private String name;
            private String stopId;
            private String id;
            private String uniqueId;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getStopId() {
                return stopId;
            }

            public void setStopId(String stopId) {
                this.stopId = stopId;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getUniqueId() {
                return uniqueId;
            }

            public void setUniqueId(String uniqueId) {
                this.uniqueId = uniqueId;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.name);
                dest.writeString(this.stopId);
                dest.writeString(this.id);
                dest.writeString(this.uniqueId);
            }

            public RouteDetailsBean() {
            }

            protected RouteDetailsBean(Parcel in) {
                this.name = in.readString();
                this.stopId = in.readString();
                this.id = in.readString();
                this.uniqueId = in.readString();
            }

            public static final Creator<RouteDetailsBean> CREATOR = new Creator<RouteDetailsBean>() {
                @Override
                public RouteDetailsBean createFromParcel(Parcel source) {
                    return new RouteDetailsBean(source);
                }

                @Override
                public RouteDetailsBean[] newArray(int size) {
                    return new RouteDetailsBean[size];
                }
            };
        }

        public ResultBean() {
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.lastName);
            dest.writeString(this.gradeId);
            dest.writeString(this.profilePic);
            dest.writeString(this.emailId);
            dest.writeString(this.parentId);
            dest.writeString(this.phoneNo);
            dest.writeString(this.studentId);
            dest.writeString(this.firstName);
            dest.writeString(this.classId);
            dest.writeString(this.dob);
            dest.writeString(this.id);
            dest.writeString(this.schoolId);
            dest.writeString(this.schoolYear);
            dest.writeParcelable(this.routeDetails, flags);
            dest.writeByte(this.isSelected ? (byte) 1 : (byte) 0);
            dest.writeByte(this.isEnabled ? (byte) 1 : (byte) 0);
            dest.writeByte(this.isPresent ? (byte) 1 : (byte) 0);
            dest.writeByte(this.isLate ? (byte) 1 : (byte) 0);
        }

        protected ResultBean(Parcel in) {
            this.lastName = in.readString();
            this.gradeId = in.readString();
            this.profilePic = in.readString();
            this.emailId = in.readString();
            this.parentId = in.readString();
            this.phoneNo = in.readString();
            this.studentId = in.readString();
            this.firstName = in.readString();
            this.classId = in.readString();
            this.dob = in.readString();
            this.id = in.readString();
            this.schoolId = in.readString();
            this.schoolYear = in.readString();
            this.routeDetails = in.readParcelable(RouteDetailsBean.class.getClassLoader());
            this.isSelected = in.readByte() != 0;
            this.isEnabled = in.readByte() != 0;
            this.isPresent = in.readByte() != 0;
            this.isLate = in.readByte() != 0;
        }

        public static final Creator<ResultBean> CREATOR = new Creator<ResultBean>() {
            @Override
            public ResultBean createFromParcel(Parcel source) {
                return new ResultBean(source);
            }

            @Override
            public ResultBean[] newArray(int size) {
                return new ResultBean[size];
            }
        };
    }
}
