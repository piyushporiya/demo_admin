/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.viewresponse.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by social_jaydeep on 04/10/17.
 */

public class TeacherAppointmentModel implements Parcelable {


    /**
     * message : Appointment details are below
     * status : true
     * authtoken : null
     * result : [{"teacherMobileNo":"9898989898","gradeId":"3","teacherName":"Teacher three","studentList":["1225"],"creationDate":"2017-11-24 06:17:00","classId":"B","teacherId":"teacherthree@dataready.in","appointmentTime":"12:00 - 13:00","appointmentId":"1291511504185745","teacherEmailId":"teacherthree@dataready.in","appointmentTitle":"sure","schoolId":"129","schoolYear":"2017","from":"teacherthree@dataready.in","id":"1291511504185745","appointmentDate":"2017-11-26","subjectName":"History","status":"confirmed"},{"teacherMobileNo":"9898989898","gradeId":"3","teacherName":"Teacher three","studentList":["1225"],"creationDate":"2017-11-24 06:27:01","classId":"B","teacherId":"teacherthree@dataready.in","appointmentTime":"12:00 - 13:00","appointmentId":"1291511504821184","teacherEmailId":"teacherthree@dataready.in","appointmentTitle":"zmsmsm","schoolId":"129","schoolYear":"2017","from":"teacherthree@dataready.in","id":"1291511504821184","appointmentDate":"2017-11-26","subjectName":"History","status":"initiated"},{"teacherMobileNo":"9898989898","gradeId":"3","teacherName":"Teacher three","studentList":["1226"],"creationDate":"2017-11-24 06:15:07","classId":"B","teacherId":"teacherthree@dataready.in","appointmentTime":"12:00 - 13:00","appointmentId":"1291511504107538","teacherEmailId":"teacherthree@dataready.in","appointmentTitle":"thjknmdkfkf","schoolId":"129","schoolYear":"2017","from":"teacherthree@dataready.in","id":"1291511504107538","appointmentDate":"2017-11-26","subjectName":"History","status":"initiated"},{"teacherMobileNo":"9898989898","gradeId":"3","teacherName":"Teacher three","studentList":["1226"],"creationDate":"2017-11-24 12:49:20","classId":"B","teacherId":"teacherthree@dataready.in","appointmentTime":"12:00 - 13:00","appointmentId":"1291511527760387","teacherEmailId":"teacherthree@dataready.in","appointmentTitle":"dddfdd","schoolId":"129","schoolYear":"2017","from":"teacherthree@dataready.in","id":"1291511527760387","appointmentDate":"2017-11-27","subjectName":"History","status":"initiated"},{"teacherMobileNo":"9898989898","gradeId":"3","teacherName":"Teacher three","studentList":["1225"],"creationDate":"2017-11-27 08:06:35","classId":"B","teacherId":"teacherthree@dataready.in","appointmentTime":"03:00 PM - 03:15 PM","appointmentId":"1291511769995563","teacherEmailId":"teacherthree@dataready.in","appointmentTitle":"lets schedule for Lokesh\nhow\ndhgg\nfjcjdk\ncnckdkd\njcjxkd\nxnckkdd\nxkfkdkd\ndkdkdk\n","schoolId":"129","schoolYear":"2017","from":"teacherthree@dataready.in","id":"1291511769995563","appointmentDate":"2017-11-28","subjectName":"History","status":"initiated"},{"teacherMobileNo":"9898989898","gradeId":"3","teacherName":"Teacher three","studentList":["1233"],"creationDate":"2017-11-27 11:12:40","classId":"B","teacherId":"teacherthree@dataready.in","appointmentTime":"03:00 PM - 03:15 PM","appointmentId":"1291511781160484","teacherEmailId":"teacherthree@dataready.in","appointmentTitle":"hjdjdgsjhdvxjcbxb","schoolId":"129","schoolYear":"2017","from":"teacherthree@dataready.in","id":"1291511781160484","appointmentDate":"2017-11-28","subjectName":"History","status":"initiated"},{"teacherMobileNo":"9898989898","gradeId":"3","teacherName":"Teacher three","studentList":["1225"],"creationDate":"2017-11-27 11:12:04","classId":"B","teacherId":"teacherthree@dataready.in","appointmentTime":"03:00 PM - 03:15 PM","appointmentId":"1291511781124546","teacherEmailId":"teacherthree@dataready.in","appointmentTitle":"gsggsgd sjjjdjudgsy bshhshshhshsb hshjjshsgs bnxhgssjs jjdjjsgsg\nnjsjsksk\nhsjjskisois\njzjisiisis\n","schoolId":"129","schoolYear":"2017","from":"teacherthree@dataready.in","id":"1291511781124546","appointmentDate":"2017-11-28","subjectName":"History","status":"initiated"},{"teacherMobileNo":"9898989898","gradeId":"3","teacherName":"Teacher three","studentList":["1225"],"creationDate":"2017-11-29 10:59:02","classId":"B","teacherId":"teacherth 11-30 16:08:12.711 4868-10950/ca.dataready.smartteacher D/OkHttp: ree@dataready.in","appointmentTime":"03:00 PM - 03:15 PM","appointmentId":"1291511781210634","teacherEmailId":"teacherthree@dataready.in","appointmentTitle":"gxxhxhdg bxhhxhhd bxbdh bxhd\ngfjdkdkf\nfkfkfkfkf\ndmdkfllflf\n1234567890098765432112348569606006","schoolId":"129","schoolYear":"2017","from":"teacherthree@dataready.in","id":"1291511781210634","appointmentDate":"2017-11-29","subjectName":"History","status":"initiated"},{"teacherMobileNo":"9898989898","gradeId":"3","teacherName":"Teacher three","studentList":["1225"],"creationDate":"2017-11-30 04:37:44","classId":"B","teacherId":"teacherthree@dataready.in","appointmentTime":"03:00 PM - 03:15 PM","appointmentId":"1291512016664776","teacherEmailId":"teacherthree@dataready.in","appointmentTitle":"jfjfjfjfjfjfjj","schoolId":"129","schoolYear":"2017","from":"teacherthree@dataready.in","id":"1291512016664776","appointmentDate":"2017-11-30","subjectName":"History","status":"initiated"},{"teacherMobileNo":"9898989898","gradeId":"3","teacherName":"Teacher three","studentList":["1225"],"creationDate":"2017-11-29 11:19:36","classId":"B","teacherId":"teacherthree@dataready.in","appointmentTime":"03:00 PM - 03:15 PM","appointmentId":"1291511954376993","teacherEmailId":"teacherthree@dataready.in","appointmentTitle":"kjkjkjkjkj\nsmdmmdkd\nxmxmmxmxmd\nmxmxmmmd\nmdmdmxkxkmd","schoolId":"129","schoolYear":"2017","from":"teacherthree@dataready.in","id":"1291511954376993","appointmentDate":"2017-11-30","subjectName":"History","status":"initiated"},{"teacherMobileNo":"9898989898","gradeId":"3","teacherName":"Teacher three","studentList":["1238"],"creationDate":"2017-11-30 10:36:46","classId":"C","teacherId":"teacherthree@dataready.in","appointmentTime":"03:00 PM - 03:15 PM","appointmentId":"1291512038206230","teacherEmailId":"teacherthree@dataready.in","appointmentTitle":"hello Curtis","schoolId":"129","schoolYear":"2017","files":[{"filePath":"/schoolapp/images/585f70a5-f49b-4542-a294-e2a04e3c6a11.png"}],"from":"teacherthree@dataready.in","id":"1291512038206230","appointmentDate":"2017-12-09","subjectName":"History","status":"initiated"}]
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private String authtokenexpires;
    private List<ResultBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean implements Parcelable {
        /**
         * teacherMobileNo : 9898989898
         * gradeId : 3
         * teacherName : Teacher three
         * studentList : ["1225"]
         * creationDate : 2017-11-24 06:17:00
         * classId : B
         * teacherId : teacherthree@dataready.in
         * appointmentTime : 12:00 - 13:00
         * appointmentId : 1291511504185745
         * teacherEmailId : teacherthree@dataready.in
         * appointmentTitle : sure
         * schoolId : 129
         * schoolYear : 2017
         * from : teacherthree@dataready.in
         * id : 1291511504185745
         * appointmentDate : 2017-11-26
         * subjectName : History
         * status : confirmed
         * files : [{"filePath":"/schoolapp/images/585f70a5-f49b-4542-a294-e2a04e3c6a11.png"}]
         */

        private String teacherMobileNo;
        private String gradeId;
        private String teacherName;
        private String creationDate;
        private String classId;
        private String teacherId;
        private String appointmentTime;
        private String appointmentId;
        private String teacherEmailId;
        private String appointmentTitle;
        private String schoolId;
        private String schoolYear;
        private String from;
        private String id;
        private String appointmentDate;
        private String subjectName;
        private String status;
        private List<String> studentList;
        private List<FilesBean> files;
        private  boolean isExpanded = false;
        private String month;
        private int type = 0;
        private boolean isEditable = false;


        public boolean isEditable() {
            return isEditable;
        }

        public void setEditable(boolean editable) {
            isEditable = editable;
        }

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public boolean isExpanded() {
            return isExpanded;
        }

        public void setExpanded(boolean expanded) {
            isExpanded = expanded;
        }

        public static Creator<ResultBean> getCREATOR() {
            return CREATOR;
        }

        public String getTeacherMobileNo() {
            return teacherMobileNo;
        }

        public void setTeacherMobileNo(String teacherMobileNo) {
            this.teacherMobileNo = teacherMobileNo;
        }

        public String getGradeId() {
            return gradeId;
        }

        public void setGradeId(String gradeId) {
            this.gradeId = gradeId;
        }

        public String getTeacherName() {
            return teacherName;
        }

        public void setTeacherName(String teacherName) {
            this.teacherName = teacherName;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getTeacherId() {
            return teacherId;
        }

        public void setTeacherId(String teacherId) {
            this.teacherId = teacherId;
        }

        public String getAppointmentTime() {
            return appointmentTime;
        }

        public void setAppointmentTime(String appointmentTime) {
            this.appointmentTime = appointmentTime;
        }

        public String getAppointmentId() {
            return appointmentId;
        }

        public void setAppointmentId(String appointmentId) {
            this.appointmentId = appointmentId;
        }

        public String getTeacherEmailId() {
            return teacherEmailId;
        }

        public void setTeacherEmailId(String teacherEmailId) {
            this.teacherEmailId = teacherEmailId;
        }

        public String getAppointmentTitle() {
            return appointmentTitle;
        }

        public void setAppointmentTitle(String appointmentTitle) {
            this.appointmentTitle = appointmentTitle;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getSchoolYear() {
            return schoolYear;
        }

        public void setSchoolYear(String schoolYear) {
            this.schoolYear = schoolYear;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getAppointmentDate() {
            return appointmentDate;
        }

        public void setAppointmentDate(String appointmentDate) {
            this.appointmentDate = appointmentDate;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<String> getStudentList() {
            return studentList;
        }

        public void setStudentList(List<String> studentList) {
            this.studentList = studentList;
        }

        public List<FilesBean> getFiles() {
            return files;
        }

        public void setFiles(List<FilesBean> files) {
            this.files = files;
        }

        public static class FilesBean implements Parcelable {
            /**
             * filePath : /schoolapp/images/585f70a5-f49b-4542-a294-e2a04e3c6a11.png
             */

            private String filePath;

            public String getFilePath() {
                return filePath;
            }

            public void setFilePath(String filePath) {
                this.filePath = filePath;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.filePath);
            }

            public FilesBean() {
            }

            protected FilesBean(Parcel in) {
                this.filePath = in.readString();
            }

            public static final Creator<FilesBean> CREATOR = new Creator<FilesBean>() {
                @Override
                public FilesBean createFromParcel(Parcel source) {
                    return new FilesBean(source);
                }

                @Override
                public FilesBean[] newArray(int size) {
                    return new FilesBean[size];
                }
            };
        }

        public ResultBean() {
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.teacherMobileNo);
            dest.writeString(this.gradeId);
            dest.writeString(this.teacherName);
            dest.writeString(this.creationDate);
            dest.writeString(this.classId);
            dest.writeString(this.teacherId);
            dest.writeString(this.appointmentTime);
            dest.writeString(this.appointmentId);
            dest.writeString(this.teacherEmailId);
            dest.writeString(this.appointmentTitle);
            dest.writeString(this.schoolId);
            dest.writeString(this.schoolYear);
            dest.writeString(this.from);
            dest.writeString(this.id);
            dest.writeString(this.appointmentDate);
            dest.writeString(this.subjectName);
            dest.writeString(this.status);
            dest.writeStringList(this.studentList);
            dest.writeTypedList(this.files);
            dest.writeByte(this.isExpanded ? (byte) 1 : (byte) 0);
            dest.writeString(this.month);
            dest.writeInt(this.type);
        }

        protected ResultBean(Parcel in) {
            this.teacherMobileNo = in.readString();
            this.gradeId = in.readString();
            this.teacherName = in.readString();
            this.creationDate = in.readString();
            this.classId = in.readString();
            this.teacherId = in.readString();
            this.appointmentTime = in.readString();
            this.appointmentId = in.readString();
            this.teacherEmailId = in.readString();
            this.appointmentTitle = in.readString();
            this.schoolId = in.readString();
            this.schoolYear = in.readString();
            this.from = in.readString();
            this.id = in.readString();
            this.appointmentDate = in.readString();
            this.subjectName = in.readString();
            this.status = in.readString();
            this.studentList = in.createStringArrayList();
            this.files = in.createTypedArrayList(FilesBean.CREATOR);
            this.isExpanded = in.readByte() != 0;
            this.month = in.readString();
            this.type = in.readInt();
        }

        public static final Creator<ResultBean> CREATOR = new Creator<ResultBean>() {
            @Override
            public ResultBean createFromParcel(Parcel source) {
                return new ResultBean(source);
            }

            @Override
            public ResultBean[] newArray(int size) {
                return new ResultBean[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.message);
        dest.writeByte(this.status ? (byte) 1 : (byte) 0);
        dest.writeString(this.authtoken);
        dest.writeString(this.authtokenexpires);
        dest.writeList(this.result);
    }

    public TeacherAppointmentModel() {
    }

    protected TeacherAppointmentModel(Parcel in) {
        this.message = in.readString();
        this.status = in.readByte() != 0;
        this.authtoken = in.readString();
        this.authtokenexpires = in.readString();
        this.result = new ArrayList<ResultBean>();
        in.readList(this.result, ResultBean.class.getClassLoader());
    }

    public static final Creator<TeacherAppointmentModel> CREATOR = new Creator<TeacherAppointmentModel>() {
        @Override
        public TeacherAppointmentModel createFromParcel(Parcel source) {
            return new TeacherAppointmentModel(source);
        }

        @Override
        public TeacherAppointmentModel[] newArray(int size) {
            return new TeacherAppointmentModel[size];
        }
    };
}
