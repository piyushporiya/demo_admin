/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.viewresponse.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.DateFunction;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.chat.ChatActivity;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.server.AppApi;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.FileModel;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.SchoolCommunication;
import ca.dataready.SmartSchoolAdmin.web.WebActivity;


/**
 * Created by social_jaydeep on 02/10/17.
 */

public class ViewMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<SchoolCommunication.ResultBean> beans;
    private FilesAdapter filesAdapter;
    private List<FileModel> selectedFiles = new ArrayList<>();
    private static final int TYPE_VIEW = 0;
    private static final int TYPE_HEADER = 1;

    public ViewMessageAdapter(Context context) {
        this.context = context;
        beans = new ArrayList<>();
    }

    @Override
    public int getItemViewType(int position) {
        if (beans.get(position).getType() == 1) {
            return TYPE_HEADER;
        }
        return TYPE_VIEW;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_VIEW) {

            return new ViewMessageViewHolder(LayoutInflater.from(context).inflate(R.layout.row_view_respond, parent, false));

        } else if (viewType == TYPE_HEADER) {

            return new HeaderViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_header, parent, false));

        } else {

            return null;
        }

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        final SchoolCommunication.ResultBean bean = beans.get(position);

        if (holder instanceof ViewMessageViewHolder) {

            ((ViewMessageViewHolder) holder).counter.setVisibility(bean.getCounter() == 0 ? View.GONE : View.VISIBLE);
            ((ViewMessageViewHolder) holder).counter.setText(String.valueOf(bean.getCounter()));

            ((ViewMessageViewHolder) holder).ivStudentImg.setImageURI(Uri.parse(AppApi.BASE_URL + bean.getStudentProfilePic()));
            ((ViewMessageViewHolder) holder).txtTitle.setText(bean.getStudentFirstName() + " " + bean.getStudentLastName());
            String formattedDate = DateFunction.ConvertDate(bean.getCreatedDate(), "yyyy-MM-dd", "dd MMM yyyy");
            ((ViewMessageViewHolder) holder).txtGrade.setText("Grade " + bean.getGradeId() + " - " + "Class " + bean.getClassId());
            ((ViewMessageViewHolder) holder).txtMsg.setText(bean.getTitle());
            ((ViewMessageViewHolder) holder).mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    context.startActivity(new Intent(context, ChatActivity.class).putExtra(ChatActivity.OBJECT, bean));
                    ((HomeActivity) context).overridePendingTransition(R.anim.enter, R.anim.leave);

                }
            });

            ((ViewMessageViewHolder) holder).txtDate.setText(formattedDate);
            if (bean.getFiles() != null && bean.getFiles().size() > 0) {

                ((ViewMessageViewHolder) holder).recyclerView.setLayoutManager(new LinearLayoutManager(context));
                ((ViewMessageViewHolder) holder).recyclerView.setNestedScrollingEnabled(false);
                filesAdapter = new FilesAdapter(context, true);
                ((ViewMessageViewHolder) holder).recyclerView.setAdapter(filesAdapter);
                selectedFiles.clear();
                if (bean.getFiles() != null && bean.getFiles().size() > 0) {
                    for (SchoolCommunication.ResultBean.FilesBean model : bean.getFiles()) {
                        if (model.getFilePath().contains("/")) {
                            selectedFiles.add(new FileModel(model.getFilePath().substring(model.getFilePath().lastIndexOf("/") + 1), model.getFilePath()));
                        }
                    }
                }
                filesAdapter.addItem(selectedFiles);

            } else {
                ((ViewMessageViewHolder) holder).txtViewMore.setVisibility(View.GONE);
            }

            ((ViewMessageViewHolder) holder).txtMsg.setMaxLines(Integer.MAX_VALUE);

            if (bean.isExpanded()) {
                ((ViewMessageViewHolder) holder).txtViewMore.setVisibility(View.VISIBLE);
                ((ViewMessageViewHolder) holder).txtViewMore.setText(App_Constants.VIEW_LESS);
                ((ViewMessageViewHolder) holder).txtMsg.setMaxLines(Integer.MAX_VALUE);
                ((ViewMessageViewHolder) holder).recyclerView.setVisibility(View.VISIBLE);
                ((ViewMessageViewHolder) holder).txtFileName.setVisibility(View.GONE);

            } else {

                if (bean.getFiles() != null && bean.getFiles().size() > 1) {


                    ((ViewMessageViewHolder) holder).txtMsg.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                        @Override
                        public boolean onPreDraw() {
                            ((ViewMessageViewHolder) holder).txtMsg.getViewTreeObserver().removeOnPreDrawListener(this);
                            int lineCount = ((ViewMessageViewHolder) holder).txtMsg.getLineCount();
                            if (lineCount > 1) {
                                ((ViewMessageViewHolder) holder).txtMsg.setMaxLines(1);
                            }

                            return false;
                        }
                    });

                    ((ViewMessageViewHolder) holder).txtViewMore.setVisibility(View.VISIBLE);
                    ((ViewMessageViewHolder) holder).recyclerView.setVisibility(View.GONE);
                    ((ViewMessageViewHolder) holder).txtFileName.setVisibility(View.VISIBLE);
                    ((ViewMessageViewHolder) holder).txtFileName.setText(selectedFiles.get(0).getName());

                } else if (bean.getFiles() != null && bean.getFiles().size() > 0) {

                    ((ViewMessageViewHolder) holder).txtViewMore.setVisibility(View.GONE);
                    ((ViewMessageViewHolder) holder).recyclerView.setVisibility(View.GONE);
                    ((ViewMessageViewHolder) holder).txtFileName.setVisibility(View.VISIBLE);
                    ((ViewMessageViewHolder) holder).txtFileName.setText(selectedFiles.get(0).getName());

                    ((ViewMessageViewHolder) holder).txtMsg.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                        @Override
                        public boolean onPreDraw() {
                            ((ViewMessageViewHolder) holder).txtMsg.getViewTreeObserver().removeOnPreDrawListener(this);
                            int lineCount = ((ViewMessageViewHolder) holder).txtMsg.getLineCount();
                            if (lineCount > 1) {
                                ((ViewMessageViewHolder) holder).txtViewMore.setVisibility(View.VISIBLE);
                                ((ViewMessageViewHolder) holder).txtMsg.setMaxLines(1);
                            } else {
                                ((ViewMessageViewHolder) holder).txtViewMore.setVisibility(View.GONE);
                            }

                            return false;
                        }
                    });

                } else {

                    ((ViewMessageViewHolder) holder).txtViewMore.setVisibility(View.GONE);
                    ((ViewMessageViewHolder) holder).recyclerView.setVisibility(View.GONE);
                    ((ViewMessageViewHolder) holder).txtFileName.setVisibility(View.GONE);


                    ((ViewMessageViewHolder) holder).txtMsg.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                        @Override
                        public boolean onPreDraw() {
                            ((ViewMessageViewHolder) holder).txtMsg.getViewTreeObserver().removeOnPreDrawListener(this);
                            int lineCount = ((ViewMessageViewHolder) holder).txtMsg.getLineCount();
                            if (lineCount > 1) {
                                ((ViewMessageViewHolder) holder).txtViewMore.setVisibility(View.VISIBLE);
                                ((ViewMessageViewHolder) holder).txtMsg.setMaxLines(1);
                            } else {
                                ((ViewMessageViewHolder) holder).txtViewMore.setVisibility(View.GONE);
                            }

                            return false;
                        }
                    });

                }


                ((ViewMessageViewHolder) holder).txtViewMore.setText(App_Constants.VIEW_MORE);
                //  ((ViewMessageViewHolder) holder).txtMsg.setMaxLines(1);

            }

            ((ViewMessageViewHolder) holder).txtFileName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (bean.getFiles() != null && bean.getFiles().size() > 0) {
                        context.startActivity(new Intent(context, WebActivity.class)
                                .putExtra(WebActivity.URL, AppApi.BASE_URL + bean.getFiles().get(0).getFilePath()));
                    }

                }
            });


            ((ViewMessageViewHolder) holder).txtViewMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    bean.setExpanded(!bean.isExpanded());
                    makeOtherCollapsed(holder.getAdapterPosition());

                }
            });

        } else if (holder instanceof HeaderViewHolder) {

            ((HeaderViewHolder) holder).txtMonth.setText(bean.getMonth());
        }
    }

    public void addItem(List<SchoolCommunication.ResultBean> results) {

        Collections.sort(results, new Comparator<SchoolCommunication.ResultBean>() {
            public int compare(SchoolCommunication.ResultBean obj1, SchoolCommunication.ResultBean obj2) {
                // ## Ascending order
                return obj2.getCreatedDate().compareToIgnoreCase(obj1.getCreatedDate()); // To compare string values
            }
        });

        beans.addAll(results);
        notifyDataSetChanged();

    }

    private void makeOtherCollapsed(int position) {

        for (int i = 0; i < beans.size(); i++) {

            if (i != position) {
                beans.get(i).setExpanded(false);
            }

        }

        notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }


    class ViewMessageViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.iv_StudentImg)
        SimpleDraweeView ivStudentImg;
        @BindView(R.id.txt_title)
        TextView txtTitle;
        @BindView(R.id.txt_date)
        TextView txtDate;
        @BindView(R.id.txt_grade)
        TextView txtGrade;
        @BindView(R.id.txt_msg)
        TextView txtMsg;
        @BindView(R.id.txt_view_more)
        TextView txtViewMore;
        @BindView(R.id.recyclerView)
        RecyclerView recyclerView;
        @BindView(R.id.txt_file_name)
        TextView txtFileName;
        View mView;
        @BindView(R.id.counter)
        TextView counter;
        //R.layout.row_view_respond

        public ViewMessageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mView = itemView;
        }
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_month)
        TextView txtMonth;

        HeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

