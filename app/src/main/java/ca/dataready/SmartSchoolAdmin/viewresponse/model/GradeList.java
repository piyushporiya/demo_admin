/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.viewresponse.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by social_jaydeep on 19/09/17.
 */

public class GradeList {


    /**
     * message : Informations are below
     * status : true
     * authtoken : null
     * result : {"schoolId":"129","grade":[{"gradeName":"Grade 1","gradeId":"1","class":[{"classId":"A","className":"Class A"},{"classId":"B","className":"Class B"},{"classId":"C","className":"Class C"}]},{"gradeName":"Grade 2","gradeId":"2","class":[{"classId":"A","className":"Class A"},{"classId":"B","className":"Class B"},{"classId":"C","className":"Class C"}]},{"gradeName":"Grade 3","gradeId":"3","class":[{"classId":"A","className":"Class A"},{"classId":"B","className":"Class B"},{"classId":"C","className":"Class C"}]},{"gradeName":"Grade 4","gradeId":"4","class":[{"classId":"A","className":"Class A"},{"classId":"B","className":"Class B"},{"classId":"C","className":"Class C"}]}],"schoolYear":"2017-2018"}
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private Object authtoken;
    private ResultBean result;
    private Object authtokenexpires;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Object getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(Object authtoken) {
        this.authtoken = authtoken;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public Object getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(Object authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public static class ResultBean implements Parcelable {
        /**
         * schoolId : 129
         * grade : [{"gradeName":"Grade 1","gradeId":"1","class":[{"classId":"A","className":"Class A"},{"classId":"B","className":"Class B"},{"classId":"C","className":"Class C"}]},{"gradeName":"Grade 2","gradeId":"2","class":[{"classId":"A","className":"Class A"},{"classId":"B","className":"Class B"},{"classId":"C","className":"Class C"}]},{"gradeName":"Grade 3","gradeId":"3","class":[{"classId":"A","className":"Class A"},{"classId":"B","className":"Class B"},{"classId":"C","className":"Class C"}]},{"gradeName":"Grade 4","gradeId":"4","class":[{"classId":"A","className":"Class A"},{"classId":"B","className":"Class B"},{"classId":"C","className":"Class C"}]}]
         * schoolYear : 2017-2018
         */

        private String schoolId;
        private String schoolYear;
        private ArrayList<GradeBean> grade;

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getSchoolYear() {
            return schoolYear;
        }

        public void setSchoolYear(String schoolYear) {
            this.schoolYear = schoolYear;
        }

        public ArrayList<GradeBean> getGrade() {
            return grade;
        }

        public void setGrade(ArrayList<GradeBean> grade) {
            this.grade = grade;
        }

        public static class GradeBean implements Parcelable {
            /**
             * gradeName : Grade 1
             * gradeId : 1
             * class : [{"classId":"A","className":"Class A"},{"classId":"B","className":"Class B"},{"classId":"C","className":"Class C"}]
             */

            private String gradeName;
            private String gradeId;
            @SerializedName("class")
            private List<ClassBean> classX;

            public String getGradeName() {
                return gradeName;
            }

            public void setGradeName(String gradeName) {
                this.gradeName = gradeName;
            }

            public String getGradeId() {
                return gradeId;
            }

            public void setGradeId(String gradeId) {
                this.gradeId = gradeId;
            }

            public List<ClassBean> getClassX() {
                return classX;
            }

            public void setClassX(List<ClassBean> classX) {
                this.classX = classX;
            }

            public static class ClassBean implements Parcelable {
                /**
                 * classId : A
                 * className : Class A
                 */

                private String classId;
                private String className;

                public String getClassId() {
                    return classId;
                }

                public void setClassId(String classId) {
                    this.classId = classId;
                }

                public String getClassName() {
                    return className;
                }

                public void setClassName(String className) {
                    this.className = className;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(this.classId);
                    dest.writeString(this.className);
                }

                public ClassBean() {
                }

                protected ClassBean(Parcel in) {
                    this.classId = in.readString();
                    this.className = in.readString();
                }

                public static final Creator<ClassBean> CREATOR = new Creator<ClassBean>() {
                    @Override
                    public ClassBean createFromParcel(Parcel source) {
                        return new ClassBean(source);
                    }

                    @Override
                    public ClassBean[] newArray(int size) {
                        return new ClassBean[size];
                    }
                };
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.gradeName);
                dest.writeString(this.gradeId);
                dest.writeList(this.classX);
            }

            public GradeBean() {
            }

            protected GradeBean(Parcel in) {
                this.gradeName = in.readString();
                this.gradeId = in.readString();
                this.classX = new ArrayList<ClassBean>();
                in.readList(this.classX, ClassBean.class.getClassLoader());
            }

            public static final Creator<GradeBean> CREATOR = new Creator<GradeBean>() {
                @Override
                public GradeBean createFromParcel(Parcel source) {
                    return new GradeBean(source);
                }

                @Override
                public GradeBean[] newArray(int size) {
                    return new GradeBean[size];
                }
            };
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.schoolId);
            dest.writeString(this.schoolYear);
            dest.writeList(this.grade);
        }

        public ResultBean() {
        }

        protected ResultBean(Parcel in) {
            this.schoolId = in.readString();
            this.schoolYear = in.readString();
            this.grade = new ArrayList<GradeBean>();
            in.readList(this.grade, GradeBean.class.getClassLoader());
        }

        public static final Parcelable.Creator<ResultBean> CREATOR = new Parcelable.Creator<ResultBean>() {
            @Override
            public ResultBean createFromParcel(Parcel source) {
                return new ResultBean(source);
            }

            @Override
            public ResultBean[] newArray(int size) {
                return new ResultBean[size];
            }
        };
    }
}
