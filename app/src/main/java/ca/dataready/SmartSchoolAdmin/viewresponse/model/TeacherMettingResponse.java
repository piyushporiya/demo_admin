/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.viewresponse.model;


import java.util.List;

public class TeacherMettingResponse {


    /**
     * message : Appointment details are below
     * status : true
     * authtoken : null
     * result : ["03:00 PM - 03:15 PM","03:15 PM - 03:30 PM","03:30 PM - 03:45 PM","03:45 PM - 04:00 PM","04:00 PM - 04:15 PM","04:15 PM - 04:30 PM","04:30 PM - 04:45 PM","04:45 PM - 05:00 PM"]
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private Object authtoken;
    private Object authtokenexpires;
    private List<String> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Object getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(Object authtoken) {
        this.authtoken = authtoken;
    }

    public Object getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(Object authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public List<String> getResult() {
        return result;
    }

    public void setResult(List<String> result) {
        this.result = result;
    }
}
