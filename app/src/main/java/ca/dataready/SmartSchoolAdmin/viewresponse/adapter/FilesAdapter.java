/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.viewresponse.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.server.AppApi;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.FileModel;
import ca.dataready.SmartSchoolAdmin.web.WebActivity;


public class FilesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    private boolean showAttachmentFromServer = false;
    private AlertDialog alertDialog;
    //private HomeWorkFragment homeWorkFragment;
    List<FileModel> selectedFiles;
    private boolean isShowOneline = false;

    /*public FilesAdapter(Context context, HomeWorkFragment homeWorkFragment) {
        this.context = context;
        this.homeWorkFragment = homeWorkFragment;
        selectedFiles = new ArrayList<>();
    }*/

    public FilesAdapter(Context context, boolean showAttachmentFromServer, AlertDialog alertDialog) {
        this.context = context;
        this.showAttachmentFromServer = showAttachmentFromServer;
        this.alertDialog = alertDialog;
        selectedFiles = new ArrayList<>();
    }

    public FilesAdapter(Context context, boolean showAttachmentFromServer) {
        this.context = context;
        this.showAttachmentFromServer = showAttachmentFromServer;
        selectedFiles = new ArrayList<>();
    }

    public FilesAdapter(Context context) {
        this.context = context;
        selectedFiles = new ArrayList<>();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DataViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_files, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        final FileModel file = selectedFiles.get(position);

        if (holder instanceof DataViewHolder) {

            if (showAttachmentFromServer) {

                SpannableString styledString = new SpannableString(file.getName());
                styledString.setSpan(new UnderlineSpan(), 0, styledString.length(), 0);
                ((DataViewHolder) holder).txtFileName.setText(styledString);
                ((DataViewHolder) holder).txtFileName.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));

            } else {

                ((DataViewHolder) holder).txtFileName.setText(file.getName());
            }

            ((DataViewHolder) holder).btnCancel.setVisibility(showAttachmentFromServer ? View.GONE : View.VISIBLE);

            ((DataViewHolder) holder).btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    selectedFiles.remove(holder.getAdapterPosition());

                    /*if (homeWorkFragment != null)
                        homeWorkFragment.removeUploadItem(file);
                    else if (context instanceof AddClassChanelActivity)
                        ((AddClassChanelActivity) context).removeUploadItem(file);
                    else if (context instanceof AddScheduleActivity)
                        ((AddScheduleActivity) context).removeUploadItem(file);
                    else if (context instanceof ParentTeacherCommActivity)
                        ((ParentTeacherCommActivity) context).removeUploadItem(file);
                    else if (context instanceof HomeWorkDetailsActivity)
                        ((HomeWorkDetailsActivity) context).removeUploadItem(file);
                    else if (context instanceof CreateClassActivity)
                        ((CreateClassActivity) context).removeUploadItem(file);*/


                    notifyItemRemoved(holder.getAdapterPosition());
                    notifyItemRangeChanged(holder.getAdapterPosition(), selectedFiles.size());
                }
            });

            ((DataViewHolder) holder).txtFileName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (showAttachmentFromServer) {

                        if (alertDialog != null && alertDialog.isShowing())
                            alertDialog.dismiss();

                        context.startActivity(new Intent(context, WebActivity.class)
                                .putExtra(WebActivity.URL, AppApi.BASE_URL + file.getPath()));
                    }
                }
            });
        }

    }



    @Override
    public int getItemCount() {

        return (selectedFiles == null ? 0 : selectedFiles.size());
    }

    public List<FileModel> getItem() {

        return selectedFiles;
    }

    public void addItem(List<FileModel> files) {

        this.selectedFiles = files;
        notifyDataSetChanged();

    }

    public void showOneLine(boolean isShowOneLine) {

        this.isShowOneline = isShowOneLine;
        notifyDataSetChanged();
    }


    class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_file_name)
        TextView txtFileName;
        @BindView(R.id.btn_cancel)
        ImageView btnCancel;
        //R.layout.raw_files

        public DataViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
