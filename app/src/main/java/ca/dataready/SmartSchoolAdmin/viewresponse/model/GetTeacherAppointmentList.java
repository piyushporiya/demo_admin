/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.viewresponse.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by c161 on 13/06/17.
 */

public class GetTeacherAppointmentList {


    /**
     * message : Appointment details are below
     * status : true
     * authtoken :
     * result : [{"teacherMobileNo":"9898989898","gradeId":"2","teacherName":"Three Teacher","studentList":["1235"],"classId":"C","teacherId":"teacherthree@dataready.in","appointmentTime":"9:00","appointmentId":"1291498448479136","teacherEmailId":"teacherthree@dataready.in","appointmentTitle":"appointment for result update","schoolId":"129","schoolYear":"2017","from":"teacherthree@dataready.in","appointmentDate":"2017-06-28","subjectName":"history","status":"initiated"},{"teacherMobileNo":"9999999999","gradeId":"2","teacherName":"Teacher Three","studentList":["1234","1235","1236"],"classId":"C","teacherId":"teacherthree@dataready.in","appointmentTime":"10:00","appointmentId":"1291498399063145","teacherEmailId":"teacherthree@dataready.in","appointmentTitle":"Lets have discussion for child result","schoolId":"129","schoolYear":"2017","from":"teacherthree@dataready.in","appointmentDate":"2017-06-25","subjectName":"history","status":"cancelled"}]
     * authtokenexpires :
     */

    private String message;
    private String status;
    private String authtoken;
    private String authtokenexpires;
    private List<GetTeacherAppointmentBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public List<GetTeacherAppointmentBean> getResult() {
        return result;
    }

    public void setResult(List<GetTeacherAppointmentBean> result) {
        this.result = result;
    }

    public static class GetTeacherAppointmentBean implements Parcelable {
        /**
         * teacherMobileNo : 9898989898
         * gradeId : 2
         * teacherName : Three Teacher
         * studentList : ["1235"]
         * classId : C
         * teacherId : teacherthree@dataready.in
         * appointmentTime : 9:00
         * appointmentId : 1291498448479136
         * teacherEmailId : teacherthree@dataready.in
         * appointmentTitle : appointment for result update
         * schoolId : 129
         * schoolYear : 2017
         * from : teacherthree@dataready.in
         * appointmentDate : 2017-06-28
         * subjectName : history
         * status : initiated
         */

        private String teacherMobileNo;
        private String gradeId;
        private String teacherName;
        private String classId;
        private String teacherId;
        private String appointmentTime;
        private String appointmentId;
        private String teacherEmailId;
        private String appointmentTitle;
        private String schoolId;
        private String schoolYear;
        private String from;
        private String appointmentDate;
        private String subjectName;
        private String status;
        private List<String> studentList;

        public String getTeacherMobileNo() {
            return teacherMobileNo;
        }

        public void setTeacherMobileNo(String teacherMobileNo) {
            this.teacherMobileNo = teacherMobileNo;
        }

        public String getGradeId() {
            return gradeId;
        }

        public void setGradeId(String gradeId) {
            this.gradeId = gradeId;
        }

        public String getTeacherName() {
            return teacherName;
        }

        public void setTeacherName(String teacherName) {
            this.teacherName = teacherName;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getTeacherId() {
            return teacherId;
        }

        public void setTeacherId(String teacherId) {
            this.teacherId = teacherId;
        }

        public String getAppointmentTime() {
            return appointmentTime;
        }

        public void setAppointmentTime(String appointmentTime) {
            this.appointmentTime = appointmentTime;
        }

        public String getAppointmentId() {
            return appointmentId;
        }

        public void setAppointmentId(String appointmentId) {
            this.appointmentId = appointmentId;
        }

        public String getTeacherEmailId() {
            return teacherEmailId;
        }

        public void setTeacherEmailId(String teacherEmailId) {
            this.teacherEmailId = teacherEmailId;
        }

        public String getAppointmentTitle() {
            return appointmentTitle;
        }

        public void setAppointmentTitle(String appointmentTitle) {
            this.appointmentTitle = appointmentTitle;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getSchoolYear() {
            return schoolYear;
        }

        public void setSchoolYear(String schoolYear) {
            this.schoolYear = schoolYear;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getAppointmentDate() {
            return appointmentDate;
        }

        public void setAppointmentDate(String appointmentDate) {
            this.appointmentDate = appointmentDate;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<String> getStudentList() {
            return studentList;
        }

        public void setStudentList(List<String> studentList) {
            this.studentList = studentList;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.teacherMobileNo);
            dest.writeString(this.gradeId);
            dest.writeString(this.teacherName);
            dest.writeString(this.classId);
            dest.writeString(this.teacherId);
            dest.writeString(this.appointmentTime);
            dest.writeString(this.appointmentId);
            dest.writeString(this.teacherEmailId);
            dest.writeString(this.appointmentTitle);
            dest.writeString(this.schoolId);
            dest.writeString(this.schoolYear);
            dest.writeString(this.from);
            dest.writeString(this.appointmentDate);
            dest.writeString(this.subjectName);
            dest.writeString(this.status);
            dest.writeStringList(this.studentList);
        }

        public GetTeacherAppointmentBean() {
        }

        protected GetTeacherAppointmentBean(Parcel in) {
            this.teacherMobileNo = in.readString();
            this.gradeId = in.readString();
            this.teacherName = in.readString();
            this.classId = in.readString();
            this.teacherId = in.readString();
            this.appointmentTime = in.readString();
            this.appointmentId = in.readString();
            this.teacherEmailId = in.readString();
            this.appointmentTitle = in.readString();
            this.schoolId = in.readString();
            this.schoolYear = in.readString();
            this.from = in.readString();
            this.appointmentDate = in.readString();
            this.subjectName = in.readString();
            this.status = in.readString();
            this.studentList = in.createStringArrayList();
        }

        public static final Creator<GetTeacherAppointmentBean> CREATOR = new Creator<GetTeacherAppointmentBean>() {
            @Override
            public GetTeacherAppointmentBean createFromParcel(Parcel source) {
                return new GetTeacherAppointmentBean(source);
            }

            @Override
            public GetTeacherAppointmentBean[] newArray(int size) {
                return new GetTeacherAppointmentBean[size];
            }
        };
    }
}
