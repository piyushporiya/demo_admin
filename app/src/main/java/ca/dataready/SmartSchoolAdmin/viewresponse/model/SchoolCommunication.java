/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.viewresponse.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by socialinfotech12 on 3/5/18.
 */

public class SchoolCommunication implements Parcelable {

    /**
     * message : Message informations are below
     * status : true
     * authtoken : null
     * result : [{"studentLastName":"Choubey","gradeId":"3","staffLastName":"One","messageId":"1519901094014","message":"Thanks ","title":"Ajeet push test","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291214.jpg","studentId":"1214","classId":"B","createdDate":"2018-03-01 10:44:54","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/67e878b1-026b-4244-85e4-7b7113a527d3.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Ajeet"},{"studentLastName":"Tiwari","gradeId":"3","staffLastName":"three","messageId":"1519631027364","message":"You","title":"uuu","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291219.jpg","studentId":"1219","classId":"A","createdDate":"2018-02-26 07:43:47","teacherId":"teacherthree@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/de6e3b40-492f-42f5-9850-19573bb71a52.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","files":[{"filePath":"/schoolapp/images/d8a6f15e-e4d4-4bc6-96b5-d356cb77032a.JPG"}],"from":"teacherthree@dataready.in","studentFirstName":"Vijay"},{"studentLastName":"Mendapara","gradeId":"3","staffLastName":"two","messageId":"1519210707751","message":"The new update has been fixed but it is still very fun and I am ","title":"I don\u2019t think you ","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291234.jpg","studentId":"1234","classId":"C","createdDate":"2018-02-21 10:58:27","teacherId":"teachertwo@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/1e947096-1b1e-4487-b5fd-8097171801f9.jpg","commType":"school","schoolId":"129","schoolYear":"2017-2018","files":[{"filePath":"/schoolapp/images/c22c534d-e9d0-40d2-99e5-ba832fecf5d2.PNG"},{"filePath":"/schoolapp/images/1aaf20b3-e532-4944-9dd9-214e777c37be.pdf"}],"from":"teachertwo@dataready.in","studentFirstName":"Darshit"},{"studentLastName":"Mendapara","gradeId":"3","staffLastName":"","messageId":"1519123344084","message":"Eettetettetettetettetet e they\u2019re terry teeter retreated eye the road to a nearby town of a home from a new one in a new way for a while and then the other one is on a bus to a hotel room for the first three hours and the second time of a year and two years later in a day that was the only way for the ","title":"DDD","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291234.jpg","studentId":"1234","classId":"C","createdDate":"2018-02-20 10:42:24","teacherId":"","staffFirstName":"","staffProfilePic":"","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"1234","studentFirstName":"Darshit"},{"studentLastName":"Mendapara","gradeId":"3","staffLastName":"","messageId":"1519123076120","message":"Comm","title":"school ","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291234.jpg","studentId":"1234","classId":"C","createdDate":"2018-02-20 10:37:56","teacherId":"","staffFirstName":"","staffProfilePic":"","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"1234","studentFirstName":"Darshit"},{"studentLastName":"Poriya","gradeId":"3","staffLastName":"One","messageId":"1519118285722","message":"How\u2019s things coming on","title":"Hi Piyush","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291231.jpg","studentId":"1231","classId":"B","createdDate":"2018-02-20 09:18:05","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Piyush"},{"studentLastName":"Poriya","gradeId":"3","staffLastName":"","messageId":"1518850755283","message":"I have to check some detail ","title":"hey principle ","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291231.jpg","studentId":"1231","classId":"B","createdDate":"2018-02-17 06:59:15","teacherId":"","staffFirstName":"","staffProfilePic":"","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"1231","studentFirstName":"Piyush"},{"studentLastName":"Choubey","gradeId":"3","staffLastName":"","messageId":"1518271593758","message":"testing","title":"D new ","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291214.jpg","studentId":"1214","classId":"B","createdDate":"2018-02-10 14:06:33","teacherId":"","staffFirstName":"","staffProfilePic":"","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"1214","studentFirstName":"Ajeet"},{"studentLastName":"Dargan","gradeId":"3","staffLastName":"One","messageId":"1518242729584","message":"You look you can also speak something and are they enter the data","title":"I have to talk regarding Homework ","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291217.jpg","studentId":"1217","classId":"A","createdDate":"2018-02-10 06:05:29","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","files":[{"filePath":"/schoolapp/images/0ba20866-a5b5-4068-aadb-ccabee006c39.odt"},{"filePath":"/schoolapp/images/f67b4606-f622-4f43-b6eb-5a0da5ae331a.JPG"}],"from":"teacherone@dataready.in","studentFirstName":"Ajay"},{"studentLastName":"Choubey","gradeId":"3","staffLastName":"One","messageId":"1518162635423","message":"Hi ajeet","title":"TestwitAjeet","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291214.jpg","studentId":"1214","classId":"B","createdDate":"2018-02-09 07:50:35","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Ajeet"},{"studentLastName":"Badola","gradeId":"3","staffLastName":"One","messageId":"1517206489695","message":"1","title":"hi","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291215.jpg","studentId":"1215","classId":"A","createdDate":"2018-01-29 06:14:49","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Himanshu"},{"studentLastName":"Tiwari","gradeId":"3","staffLastName":"","messageId":"1516872588065","message":"Cgghhj\nDfhh\nDGH","title":"gjgefygg dffgv ","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291225.jpg","studentId":"1225","classId":"B","createdDate":"2018-01-25 09:29:48","teacherId":"","staffFirstName":"","staffProfilePic":"","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"1225","studentFirstName":"Lokesh"},{"studentLastName":"Tiwari","gradeId":"3","staffLastName":"","messageId":"1516872484364","message":"Vhgdggv\nChi","title":"cherry","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291225.jpg","studentId":"1225","classId":"B","createdDate":"2018-01-25 09:28:04","teacherId":"","staffFirstName":"","staffProfilePic":"","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"1225","studentFirstName":"Lokesh"},{"studentLastName":"Mishra","gradeId":"3","staffLastName":"","messageId":"1516871091938","message":"gzdhfug cjfhfjfk\nx b fjfj cjvkg i gv vjcvkgkbl\ncjgigkblmxjv j","title":"hdhdhfjc dkhdhffjgvk c kvkhcghxbc fffg df h fjg fjgjg","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291226.jpg","studentId":"1226","classId":"B","createdDate":"2018-01-25 09:04:51","teacherId":"","staffFirstName":"","staffProfilePic":"","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"1226","studentFirstName":"Pankaj"},{"studentLastName":"Srivastava","gradeId":"3","staffLastName":"One","messageId":"1516717990688","message":"Hi tushar","title":"hi tushar","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291227.jpg","studentId":"1227","classId":"B","createdDate":"2018-01-23 14:33:10","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Tushar"},{"studentLastName":"Mishra","gradeId":"3","staffLastName":"One","messageId":"1516717914214","message":"Hmooi7,kn","title":"dgfghfjhjk","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291226.jpg","studentId":"1226","classId":"B","createdDate":"2018-01-23 14:31:54","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Pankaj"},{"studentLastName":"Mishra","gradeId":"3","staffLastName":"One","messageId":"1516717727163","message":"Sbko,k","title":"dguljk,","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291226.jpg","studentId":"1226","classId":"B","createdDate":"2018-01-23 14:28:47","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Pankaj"},{"studentLastName":"Mishra","gradeId":"3","staffLastName":"One","messageId":"1516717402805","message":"Hows jky ..?","title":"hey jitu","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291226.jpg","studentId":"1226","classId":"B","createdDate":"2018-01-23 14:23:22","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Pankaj"},{"studentLastName":"","gradeId":"3","staffLastName":"One","messageId":"1516701742276","message":"KL","title":"kL","sequenceId":1,"studentProfilePic":"","studentId":"","classId":"A","createdDate":"2018-01-23 10:02:22","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":""},{"studentLastName":"Mishra","gradeId":"3","staffLastName":"One","messageId":"1516608664993","message":"Rjsh","title":"hi pankaj ","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291226.jpg","studentId":"1226","classId":"B","createdDate":"2018-01-22 08:11:04","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Pankaj"},{"studentLastName":"Dargan","gradeId":"3","staffLastName":"One","messageId":"1516435951399","message":"Hello hello","title":"gvggg","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291217.jpg","studentId":"1217","classId":"A","createdDate":"2018-01-20 08:12:31","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","files":[{"filePath":"/schoolapp/images/8f661a6a-67be-4050-95eb-fd000ccd619b.png"},{"filePath":"/schoolapp/images/38324184-f979-4f4c-bd2c-c7643b4f7992.PNG"}],"from":"teacherone@dataready.in","studentFirstName":"Ajay"},{"studentLastName":"srdrpk","gradeId":"3","staffLastName":"One","messageId":"1516435476338","message":"Lomh","title":"ui","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291216.jpg","studentId":"1216","classId":"A","createdDate":"2018-01-20 08:04:36","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","files":[{"filePath":"/schoolapp/images/2571426e-1e5c-452e-8caf-c394adf3f9c9.png"},{"filePath":"/schoolapp/images/4a342fa8-307c-411f-bc07-e90ed0b8accd.JPG"}],"from":"teacherone@dataready.in","studentFirstName":"David"},{"studentLastName":"Srivastava","gradeId":"3","staffLastName":"","messageId":"1516434625414","message":"i would like to discuss","title":"bi principal","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291227.jpg","studentId":"1227","classId":"B","createdDate":"2018-01-20 07:50:25","teacherId":"","staffFirstName":"","staffProfilePic":"","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"1227","studentFirstName":"Tushar"},{"studentLastName":"Tiwari","gradeId":"3","staffLastName":"One","messageId":"1516361071848","message":"L","title":"l","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291225.jpg","studentId":"1225","classId":"B","createdDate":"2018-01-19 11:24:31","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Lokesh"},{"studentLastName":"Mishra","gradeId":"3","staffLastName":"One","messageId":"1516361003664","message":"P","title":"p","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291226.jpg","studentId":"1226","classId":"B","createdDate":"2018-01-19 11:23:23","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Pankaj"},{"studentLastName":"Tiwari","gradeId":"3","staffLastName":"One","messageId":"1516360822881","message":"Vhjvjb","title":"gujvcjk","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291225.jpg","studentId":"1225","classId":"B","createdDate":"2018-01-19 11:20:22","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Lokesh"},{"studentLastName":"Tiwari","gradeId":"3","staffLastName":"One","messageId":"1516360790817","message":"Mdkd","title":"kdkdkkd","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291225.jpg","studentId":"1225","classId":"B","createdDate":"2018-01-19 11:19:50","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Lokesh"},{"studentLastName":"Tiwari","gradeId":"3","staffLastName":"One","messageId":"1516360749649","message":"Lokesh","title":"hi lokesh","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291225.jpg","studentId":"1225","classId":"B","createdDate":"2018-01-19 11:19:09","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Lokesh"},{"studentLastName":"Dargan","gradeId":"3","staffLastName":"One","messageId":"1516348010956","message":"Yes hello hi","title":"hey ajay","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291217.jpg","studentId":"1217","classId":"A","createdDate":"2018-01-19 07:46:50","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Ajay"},{"studentLastName":"Dargan","gradeId":"3","staffLastName":"","messageId":"1516271501564","message":"Hi we","title":"etle","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291217.jpg","studentId":"1217","classId":"A","createdDate":"2018-01-18 10:31:41","teacherId":"","staffFirstName":"","staffProfilePic":"","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"1217","studentFirstName":"Ajay"},{"studentLastName":"Choubey","gradeId":"3","staffLastName":"One","messageId":"1516165671595","message":"Hi","title":"bhj","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291214.jpg","studentId":"1214","classId":"B","createdDate":"2018-01-17 05:07:51","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Ajeet"},{"studentLastName":"Mishra","gradeId":"3","staffLastName":"One","messageId":"1516165318764","message":"Hi","title":"hihi","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291226.jpg","studentId":"1226","classId":"B","createdDate":"2018-01-17 05:01:58","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Pankaj"},{"studentLastName":"Mishra","gradeId":"3","staffLastName":"One","messageId":"1516165102007","message":"This one is new message for Pankaj ","title":"Hi Pankaj","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291226.jpg","studentId":"1226","classId":"B","createdDate":"2018-01-17 04:58:22","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Pankaj"},{"studentLastName":"Mishra","gradeId":"3","staffLastName":"One","messageId":"1516164919259","message":"This is communication with Pankaj","title":"Hi Jitendra ","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291226.jpg","studentId":"1226","classId":"B","createdDate":"2018-01-17 04:55:19","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Pankaj"},{"studentLastName":"Haden","gradeId":"3","staffLastName":"","messageId":"1516164393130","message":"gr","title":"gr","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291237.jpg","studentId":"1237","classId":"C","createdDate":"2018-01-17 04:46:33","teacherId":"","staffFirstName":"","staffProfilePic":"","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"1237","studentFirstName":"Alex"},{"studentLastName":"Haden","gradeId":"3","staffLastName":"One","messageId":"1516164341643","message":"Alex \n","title":"hi","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291237.jpg","studentId":"1237","classId":"C","createdDate":"2018-01-17 04:45:41","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Alex"},{"studentLastName":"Jaswal","gradeId":"3","staffLastName":"One","messageId":"1516163788573","message":"Krishna","title":"hi","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291233.jpg","studentId":"1233","classId":"A","createdDate":"2018-01-17 04:36:28","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Krishan"},{"studentLastName":"Mishra","gradeId":"3","staffLastName":"One","messageId":"1516077809500","message":"Djdkkdkdodoorkrkr","title":"jejejdjrjr","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291226.jpg","studentId":"1226","classId":"B","createdDate":"2018-01-16 04:43:29","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Pankaj"},{"studentLastName":"Mishra","gradeId":"3","staffLastName":"One","messageId":"1516036341800","message":"Whats going on man","title":"hey pamkaj mishra","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291226.jpg","studentId":"1226","classId":"B","createdDate":"2018-01-15 17:12:21","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Pankaj"},{"studentLastName":"Choubey","gradeId":"3","staffLastName":"One","messageId":"1515680527613","message":"The new version is not ","title":"the new ","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291214.jpg","studentId":"1214","classId":"A","createdDate":"2018-01-11 14:22:07","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Ajeet"},{"studentLastName":"Choubey","gradeId":"3","staffLastName":"One","messageId":"1515680273148","message":"Good to chat","title":"Master","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291229.jpg","studentId":"1229","classId":"B","createdDate":"2018-01-11 14:17:53","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","files":[{"filePath":"/schoolapp/images/fddf6cdf-79cb-4b0c-9e18-fc584f0bb8fd.JPG"}],"from":"teacherone@dataready.in","studentFirstName":"Ramdhari"},{"studentLastName":"Choubey","gradeId":"3","staffLastName":"One","messageId":"1515671497246","message":"New Long","title":"Pot","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291214.jpg","studentId":"1214","classId":"A","createdDate":"2018-01-11 11:51:37","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Ajeet"},{"studentLastName":"Jaswal","gradeId":"3","staffLastName":"One","messageId":"1515662855408","message":"K","title":"hi","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291233.jpg","studentId":"1233","classId":"A","createdDate":"2018-01-11 09:27:35","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"1233","studentFirstName":"Krishan"},{"studentLastName":"Jaswal","gradeId":"3","staffLastName":"One","messageId":"1515662825378","message":"K","title":"hi","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291233.jpg","studentId":"1233","classId":"A","createdDate":"2018-01-11 09:27:05","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"1233","studentFirstName":"Krishan"},{"studentLastName":"Jaswal","gradeId":"3","staffLastName":"One","messageId":"1515662795202","message":"K","title":"hi","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291233.jpg","studentId":"1233","classId":"A","createdDate":"2018-01-11 09:26:35","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Krishan"},{"studentLastName":"Jaswal","gradeId":"3","staffLastName":"One","messageId":"1515658086636","message":"2","title":"hi","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291233.jpg","studentId":"1233","classId":"A","createdDate":"2018-01-11 08:08:06","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Krishan"},{"studentLastName":"Jaswal","gradeId":"3","staffLastName":"One","messageId":"1515658065598","message":"1","title":"hi","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291233.jpg","studentId":"1233","classId":"A","createdDate":"2018-01-11 08:07:45","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Krishan"},{"studentLastName":"Jaswal","gradeId":"3","staffLastName":"One","messageId":"1515655952259","message":"Msg","title":"hi","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291233.jpg","studentId":"1233","classId":"A","createdDate":"2018-01-11 07:32:32","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Krishan"},{"studentLastName":"Poriya","gradeId":"3","staffLastName":"One","messageId":"1515652872126","message":"1","title":"hi","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291231.jpg","studentId":"1231","classId":"B","createdDate":"2018-01-11 06:41:12","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Piyush"},{"studentLastName":"Jaswal","gradeId":"3","staffLastName":"One","messageId":"1515652836071","message":"Krishna","title":"hi","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291233.jpg","studentId":"1233","classId":"A","createdDate":"2018-01-11 06:40:36","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Krishan"},{"studentLastName":"Choubey","gradeId":"3","staffLastName":"One","messageId":"1515590652038","message":"Ajeet","title":"hi","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291214.jpg","studentId":"1214","classId":"A","createdDate":"2018-01-10 13:24:12","teacherId":"teacherone@dataready.in","staffFirstName":"Teacher","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"teacherone@dataready.in","studentFirstName":"Ajeet"},{"studentLastName":"Jaswal","gradeId":"3","staffLastName":"","messageId":"1515590549364","message":"This is to test a school communication by D","title":"create by D","sequenceId":1,"studentProfilePic":"/schoolapp/images/1291233.jpg","studentId":"1233","classId":"A","createdDate":"2018-01-10 13:22:29","teacherId":"","staffFirstName":"","staffProfilePic":"","commType":"school","schoolId":"129","schoolYear":"2017-2018","from":"1233","studentFirstName":"Krishan"}]
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private Object authtoken;
    private Object authtokenexpires;
    private ArrayList<ResultBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Object getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(Object authtoken) {
        this.authtoken = authtoken;
    }

    public Object getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(Object authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public ArrayList<ResultBean> getResult() {
        return result;
    }

    public void setResult(ArrayList<ResultBean> result) {
        this.result = result;
    }


    public static class ResultBean implements Parcelable {
        /**
         * studentLastName : Choubey
         * gradeId : 3
         * staffLastName : One
         * messageId : 1519901094014
         * message : Thanks
         * title : Ajeet push test
         * sequenceId : 1
         * studentProfilePic : /schoolapp/images/1291214.jpg
         * studentId : 1214
         * classId : B
         * createdDate : 2018-03-01 10:44:54
         * teacherId : teacherone@dataready.in
         * staffFirstName : Teacher
         * staffProfilePic : /schoolapp/images/67e878b1-026b-4244-85e4-7b7113a527d3.png
         * commType : school
         * schoolId : 129
         * schoolYear : 2017-2018
         * from : teacherone@dataready.in
         * studentFirstName : Ajeet
         * files : [{"filePath":"/schoolapp/images/d8a6f15e-e4d4-4bc6-96b5-d356cb77032a.JPG"}]
         */

        private String studentLastName;
        private String gradeId;
        private String staffLastName;
        private String messageId;
        private String message;
        private String title;
        private int sequenceId;
        private String studentProfilePic;
        private String studentId;
        private String classId;
        private String createdDate;
        private String teacherId;
        private String staffFirstName;
        private String staffProfilePic;
        private String commType;
        private String schoolId;
        private String schoolYear;
        private String from;
        private String studentFirstName;
        private List<FilesBean> files;
        private boolean isExpanded = false;
        private int type = 0;
        private String month;
        private int counter = 0;

        public int getCounter() {
            return counter;
        }

        public void setCounter(int counter) {
            this.counter = counter;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public boolean isExpanded() {
            return isExpanded;
        }

        public void setExpanded(boolean expanded) {
            isExpanded = expanded;
        }

        public String getStudentLastName() {
            return studentLastName;
        }

        public void setStudentLastName(String studentLastName) {
            this.studentLastName = studentLastName;
        }

        public String getGradeId() {
            return gradeId;
        }

        public void setGradeId(String gradeId) {
            this.gradeId = gradeId;
        }

        public String getStaffLastName() {
            return staffLastName;
        }

        public void setStaffLastName(String staffLastName) {
            this.staffLastName = staffLastName;
        }

        public String getMessageId() {
            return messageId;
        }

        public void setMessageId(String messageId) {
            this.messageId = messageId;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getSequenceId() {
            return sequenceId;
        }

        public void setSequenceId(int sequenceId) {
            this.sequenceId = sequenceId;
        }

        public String getStudentProfilePic() {
            return studentProfilePic;
        }

        public void setStudentProfilePic(String studentProfilePic) {
            this.studentProfilePic = studentProfilePic;
        }

        public String getStudentId() {
            return studentId;
        }

        public void setStudentId(String studentId) {
            this.studentId = studentId;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

        public String getTeacherId() {
            return teacherId;
        }

        public void setTeacherId(String teacherId) {
            this.teacherId = teacherId;
        }

        public String getStaffFirstName() {
            return staffFirstName;
        }

        public void setStaffFirstName(String staffFirstName) {
            this.staffFirstName = staffFirstName;
        }

        public String getStaffProfilePic() {
            return staffProfilePic;
        }

        public void setStaffProfilePic(String staffProfilePic) {
            this.staffProfilePic = staffProfilePic;
        }

        public String getCommType() {
            return commType;
        }

        public void setCommType(String commType) {
            this.commType = commType;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getSchoolYear() {
            return schoolYear;
        }

        public void setSchoolYear(String schoolYear) {
            this.schoolYear = schoolYear;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getStudentFirstName() {
            return studentFirstName;
        }

        public void setStudentFirstName(String studentFirstName) {
            this.studentFirstName = studentFirstName;
        }

        public List<FilesBean> getFiles() {
            return files;
        }

        public void setFiles(List<FilesBean> files) {
            this.files = files;
        }

        public static class FilesBean implements Parcelable {
            /**
             * filePath : /schoolapp/images/d8a6f15e-e4d4-4bc6-96b5-d356cb77032a.JPG
             */

            private String filePath;

            public String getFilePath() {
                return filePath;
            }

            public void setFilePath(String filePath) {
                this.filePath = filePath;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.filePath);
            }

            public FilesBean() {
            }

            protected FilesBean(Parcel in) {
                this.filePath = in.readString();
            }

            public static final Creator<FilesBean> CREATOR = new Creator<FilesBean>() {
                @Override
                public FilesBean createFromParcel(Parcel source) {
                    return new FilesBean(source);
                }

                @Override
                public FilesBean[] newArray(int size) {
                    return new FilesBean[size];
                }
            };
        }

        public ResultBean() {
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.studentLastName);
            dest.writeString(this.gradeId);
            dest.writeString(this.staffLastName);
            dest.writeString(this.messageId);
            dest.writeString(this.message);
            dest.writeString(this.title);
            dest.writeInt(this.sequenceId);
            dest.writeString(this.studentProfilePic);
            dest.writeString(this.studentId);
            dest.writeString(this.classId);
            dest.writeString(this.createdDate);
            dest.writeString(this.teacherId);
            dest.writeString(this.staffFirstName);
            dest.writeString(this.staffProfilePic);
            dest.writeString(this.commType);
            dest.writeString(this.schoolId);
            dest.writeString(this.schoolYear);
            dest.writeString(this.from);
            dest.writeString(this.studentFirstName);
            dest.writeList(this.files);
            dest.writeByte(this.isExpanded ? (byte) 1 : (byte) 0);
            dest.writeInt(this.type);
            dest.writeString(this.month);
            dest.writeInt(this.counter);
        }

        protected ResultBean(Parcel in) {
            this.studentLastName = in.readString();
            this.gradeId = in.readString();
            this.staffLastName = in.readString();
            this.messageId = in.readString();
            this.message = in.readString();
            this.title = in.readString();
            this.sequenceId = in.readInt();
            this.studentProfilePic = in.readString();
            this.studentId = in.readString();
            this.classId = in.readString();
            this.createdDate = in.readString();
            this.teacherId = in.readString();
            this.staffFirstName = in.readString();
            this.staffProfilePic = in.readString();
            this.commType = in.readString();
            this.schoolId = in.readString();
            this.schoolYear = in.readString();
            this.from = in.readString();
            this.studentFirstName = in.readString();
            this.files = new ArrayList<FilesBean>();
            in.readList(this.files, FilesBean.class.getClassLoader());
            this.isExpanded = in.readByte() != 0;
            this.type = in.readInt();
            this.month = in.readString();
            this.counter = in.readInt();
        }

        public static final Creator<ResultBean> CREATOR = new Creator<ResultBean>() {
            @Override
            public ResultBean createFromParcel(Parcel source) {
                return new ResultBean(source);
            }

            @Override
            public ResultBean[] newArray(int size) {
                return new ResultBean[size];
            }
        };
    }

    public SchoolCommunication() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.message);
        dest.writeByte(this.status ? (byte) 1 : (byte) 0);
        dest.writeTypedList(this.result);
    }

    protected SchoolCommunication(Parcel in) {
        this.message = in.readString();
        this.status = in.readByte() != 0;
        this.authtoken = in.readParcelable(Object.class.getClassLoader());
        this.authtokenexpires = in.readParcelable(Object.class.getClassLoader());
        this.result = in.createTypedArrayList(ResultBean.CREATOR);
    }

    public static final Creator<SchoolCommunication> CREATOR = new Creator<SchoolCommunication>() {
        @Override
        public SchoolCommunication createFromParcel(Parcel source) {
            return new SchoolCommunication(source);
        }

        @Override
        public SchoolCommunication[] newArray(int size) {
            return new SchoolCommunication[size];
        }
    };
}
