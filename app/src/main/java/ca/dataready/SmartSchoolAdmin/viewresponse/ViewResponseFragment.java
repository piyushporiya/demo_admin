/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.viewresponse;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ViewAnimator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ca.dataready.SmartSchoolAdmin.Listeners.OnReloadListener;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.DateFunction;
import ca.dataready.SmartSchoolAdmin.Views.LastItemDecoration;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.counter.BadgeCountTracker;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.offline.view_message.ViewMessageDataHolder;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.viewresponse.adapter.ViewMessageAdapter;
import ca.dataready.SmartSchoolAdmin.viewresponse.create.PostMessageActivity;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.SchoolCommunication;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.VIEW_RESPOND_PUSH_NOTIFICATION_BROADCAST;


public class ViewResponseFragment extends Fragment implements OnReloadListener, SwipeRefreshLayout.OnRefreshListener {

    public static final String OBJECT = "object";
    @BindView(R.id.rv_communication)
    RecyclerView rvCommunication;
    @BindView(R.id.txt_no_data)
    TextView txtNoData;
    CREDENTIAL.ResultBean entity;
    @BindView(R.id.viewAnimator)
    ViewAnimator viewAnimator;
    ViewMessageAdapter viewMessageAdapter;

    Call<SchoolCommunication> call;
    @BindView(R.id.fab_add_comm)
    FloatingActionButton fabAddComm;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    public static boolean isViewRespondVisible = false;
    String messageId;
    Unbinder unbinder;
    private ArrayList<SchoolCommunication.ResultBean> teacherCommBeans;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_view_response, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) getActivity()).setOnReloadListener(this);
        ((HomeActivity) getActivity()).setSubTitle(getString(R.string.view_and_respond));
        BadgeCountTracker.saveViewandResposndBadgeCounter(getActivity(), 0);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mViewRespondPushNotificationReceiver,
                new IntentFilter(VIEW_RESPOND_PUSH_NOTIFICATION_BROADCAST));
        swipeRefreshLayout.setOnRefreshListener(this);
        rvCommunication.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvCommunication.addItemDecoration(new LastItemDecoration());

    }

    private void Init() {

        entity = AdminApp.getInstance().getAdmin();
        if (entity != null) {
            viewAnimator.setDisplayedChild(0);
            ApiCall();
        } else {
            viewAnimator.setDisplayedChild(2);
        }
    }


    private void ApiCall() {

        call = AdminApp.getInstance().getApi().getSchoolCommunications(entity.getSchoolId(), entity.getSchoolYear());
        call.enqueue(new Callback<SchoolCommunication>() {
            @Override
            public void onResponse(Call<SchoolCommunication> call, Response<SchoolCommunication> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            viewMessageAdapter = new ViewMessageAdapter(getActivity());
                            rvCommunication.setAdapter(viewMessageAdapter);

                            teacherCommBeans = response.body().getResult();

                            if (teacherCommBeans != null && teacherCommBeans.size() > 0) {

                                List<String> headers = new ArrayList<>();
                                for (int i = 0; i < teacherCommBeans.size(); i++) {

                                    if (teacherCommBeans.get(i).getCreatedDate() != null && teacherCommBeans.get(i).getCreatedDate().contains(" ")) {

                                        String formattedDate = DateFunction.ConvertDate(teacherCommBeans.get(i).getCreatedDate(), "yyyy-MM-dd hh:mm:ss", "dd MMMM yyyy");

                                        String[] pDates = formattedDate.split(" ");
                                        if (pDates.length > 2) {
                                            String month = pDates[1];
                                            String year = pDates[2];
                                            if (!headers.contains(month)) {
                                                headers.add(month);
                                                SchoolCommunication.ResultBean bean = new SchoolCommunication.ResultBean();
                                                bean.setCreatedDate(teacherCommBeans.get(i).getCreatedDate());
                                                bean.setType(1);
                                                bean.setMonth(month + " " + year);
                                                teacherCommBeans.add(i, bean);
                                            }

                                        }
                                    }
                                }

                                for (SchoolCommunication.ResultBean bean : teacherCommBeans) {
                                    bean.setCounter(BadgeCountTracker.getViewandResposndSingleThreadBadgeCounter(getActivity(), bean.getMessageId()));
                                }

                                viewMessageAdapter.addItem(teacherCommBeans);
                                viewAnimator.setDisplayedChild(1);
                                addDataForOfflineUse(teacherCommBeans);

                            } else {

                                viewAnimator.setDisplayedChild(2);
                                txtNoData.setText(getString(R.string.no_data));
                            }

                        } else {

                            viewAnimator.setDisplayedChild(2);
                            txtNoData.setText(response.body().getMessage());
                        }

                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_TEACHER_COMMUNICATION);
                        viewAnimator.setDisplayedChild(2);
                        txtNoData.setText(error.message());

                    }
                } catch (Exception e) {

                    viewAnimator.setDisplayedChild(2);
                    txtNoData.setText(getString(R.string.somethingwrong));
                    e.printStackTrace();
                }

                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<SchoolCommunication> call, Throwable t) {
                if (!call.isCanceled()) {
                    viewAnimator.setDisplayedChild(2);
                    swipeRefreshLayout.setRefreshing(false);
                    txtNoData.setText(App_Constants.NO_INTERNET);
                    System.out.println(t.getMessage());
                    getDataForOfflineUse();
                }
            }
        });
    }

    private void addDataForOfflineUse(List<SchoolCommunication.ResultBean> results) {

        ViewMessageDataHolder.saveMessageList(getActivity(), results);
    }

    private void getDataForOfflineUse() {

        List<SchoolCommunication.ResultBean> model = ViewMessageDataHolder.getMessageList(getActivity());

        if (model != null && model.size() > 0) {
            viewMessageAdapter = new ViewMessageAdapter(getActivity());
            rvCommunication.setAdapter(viewMessageAdapter);
            viewMessageAdapter.addItem(model);
            viewAnimator.setDisplayedChild(1);
        }

    }


    @OnClick(R.id.fab_add_comm)
    public void onViewClicked() {

        startActivityForResult(new Intent(getActivity(), PostMessageActivity.class), PostMessageActivity.UPDATE_MESSAGES_BROADCAST);
        if (getActivity() != null)
            getActivity().overridePendingTransition(R.anim.enter, R.anim.leave);
    }


    @Override
    public void onReload(String apiName) {

        if (apiName.equals(App_Constants.API_TEACHER_COMMUNICATION)) {
            if (isVisible()) {
                Init();
            }
        }
    }

    @Override
    public void onRefresh() {

        if (isVisible()) {
            ApiCall();
        }
    }

    private BroadcastReceiver mViewRespondPushNotificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            onRefresh();

        }
    };


    @Override
    public void onPause() {
        super.onPause();
        isViewRespondVisible = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        Init();
        isViewRespondVisible = true;
    }


    @Override
    public void onStop() {
        if (call != null)
            call.cancel();
        super.onStop();
    }


    @Override
    public void onActivityResult(final int requestCode, int resultCode, Intent data) {

        switch (requestCode) {

            case PostMessageActivity.UPDATE_MESSAGES_BROADCAST:

                if (resultCode == RESULT_OK && null != data) {

                    if (isVisible())
                        Init();

                }

                break;

            case APIError.UPDATE_TOKEN:

                if (resultCode == RESULT_OK && null != data) {
                    String apiName = data.getStringExtra("api");
                    onReload(apiName);
                }

                break;

        }
    }

}

