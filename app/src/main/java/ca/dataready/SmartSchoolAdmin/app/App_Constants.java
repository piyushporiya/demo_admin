/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.app;



public class App_Constants {

    private static final int REFRESH_TIME = 10;
    public static final int SCHEDULED_TIME = REFRESH_TIME * 1000;

    public static final String SELECTED_DATE = "selected_date";
    public static final String PREFS_USER_ID = "id";
    public static final String PREFS_TEACHER_DETAIL = "teacher_detail";
    public static final String SELECT_GRADE = "Select Grade";
    public static final String SELECT_CLASS = "Select Class";
    public static final String SELECT_TIME = "Select Time";
    public static final String EDIT = "Edit";
    public static final String UPDATE = "Update";
    public static final String DATE_FORMAT_DD_MM_YYYY_HYPHEN = "dd-MM-yyyy";
    public static final String DATE_FORMAT_YYYY_MM_DD_HYPHEN = "yyyy-MM-dd";
    public static final String DATE_FORMAT_D_MMM_YYYY_SPACE = "d MMM yyyy";
    public static final int FILE_SELECT = 677;
    public static final String OBJECT = "object";
    public static final String NOTIFICATION_ID = "notification_id";
    public static final String SUBTITLE = "sub_title";
    public static final String ORIENTATION = "orientation";
    public static final int UPDATE_LISTING = 2022;

    public static final String GRADE = "grade";
    public static final String CLASS = "class";
    public static final String TIME = "time";
    public static final String TEACHER_CLASS_SCHEDULE_OBJECT = "teacher_class_schedule_object";
    public static final String CLASS_ID = "class_id";
    public static final String GRADE_ID = "grade_id";
    public static final String PAST = "past";
    public static final String UPCOMING = "upcoming";
    public static final String APPOINTMENT_ID = "appointment_id";
    public static final String PAST_APPOINTMENT_LIST = "past_appointment_list";
    public static final String UPCOMING_APPOINTMENT_LIST = "upcoming_appointment_list";
    public static final String CHECKED_ITEM = "checked_item";
    public static final String ALL_APPOINTMENT_LIST = "all_appointment_list";
    public static final String PROFILE_VIEW = "profile_view";
    public static final String EMERGENCY_CONTACT = "emergency_contact";
    public static final String API_REGISTER_DRIVERS = "register_driver";
    public static final String API_UPDATE_DRIVERS = "update_driver";

    public static final String API_ADMIN_PICK_UP_STUDENTS_COUNT = "api_admin_pickup_student_count";

    public static final String PROFILE_ATTACHMENT = "profile_attachment";
    public static final String EMERGENCY_ATTACHMENT = "emergency_attachment";
    public static final String QUALIFICATION_ATTACHMENT = "qualification_attachment";
    public static final String EXPERIENCE_ATTCHEMTENT = "experience_attachment";
    public static final String OTHER_ATTACHMENT = "other_attachment";
    public static final String PARENT_ATTACHMENT = "parent_attachment";
    public static final String PAST_SCHOOL_ATTACHMENT = "past_school_attachment";

    public static final String API_ADMIN_SELECTED_ROUTE_INFO = "api_admin_selected_route_info";
    public static final String TEACHER = "teacher";
    public static final String MALE = "male";
    public static final String FEMALE = "female";
    public static final String SINGLE = "single";
    public static final String MARRIED = "married";
    public static final String EMPTY = "";
    public static final String API_REGISTER_TEACHER = "register_teacher";
    public static final String API_GET_TEACHER_INFO = "get_teacher_info";
    public static final String API_UPDATE_TEACHER = "update_teacher";
    public static final String STAFF = "staff";
    public static final String API_PENDING_STUDENTS = "pending_students_list";
    public static final String PENDING = "pending";
    public static final String CONFIRM = "confirm";
    public static final String STUDENT_LIST_TYPE = "student_list_type";
    public static final String API_CONFIRM_STUDENTS = "confirm_students_list";
    public static final String API_SEARCH_STUDENT = "search_student";
    public static final String DURATION = "duration";
    public static final String PROFILE_IMAGE = "profile_image";
    public static final String FATHER_PROFILE_IMAGE = "father_profile_image";
    public static final String MOTHER_PROFILE_IMAGE = "mother_profile_image";

    public static final String API_REGISTER_STUDENT = "register_student";
    public static final String API_UPDATE_STUDENT = "update_student";
    public static final String API_GET_STUDENT_INFO = "get_student_info";
    public static final String API_GET_YEAR_LIST = "get_year_list";
    public static final String API_UPDATE_STUDENT_ADMIN_PROCESS = "update_student_admin_process";
    public static final String API_REGISTER_STUDENT_ADMIN_PROCESS = "register_student_admin_process";
    public static final String API_UPLOAD = "upload";
    public static final String API_ATTENDANCE_DETAILS = "api_attendance_details";
    public static final String API_TRANSPORTATION_DETAILS = "api_transportation_details";
    public static final String API_GET_GRADE_LIST = "api_grade_list";
    public static final String API_ADD_GRADE_LIST = "api_add_grade_list";
    public static final String ADD_GRADE = "add_grade";
    public static final String UPDATE_GRADE = "upgrade_grade";
    public static final String API_UPDATE_GRADE_LIST = "api_update_grade_list";
    public static final String ADD_CLASS = "add_class";
    public static final String UPDATE_CLASS = "update_class";
    public static final String API_ADD_CLASS_LIST = "api_add_class_list";
    public static final String API_UPDATE_CLASS_LIST = "api_update_class_list";
    public static final String API_DELETE_CLASS_LIST = "api_delete_class_list";
    public static final String API_DELETE_GRADE_LIST = "api_delete_grade_list";
    public static final String API_GET_SUBJECT_LIST = "api_get_subject_list";
    public static final String API_DELETE_SUBJECT = "api_delete_subject";
    public static final String API_UPDATE_SUBJECT = "api_update_subject";
    public static final String API_ADD_SUBJECT = "api_add_subject";
    public static final String YEAR_ACTIVE = "active";
    public static final String API_SET_ACTIVE_YEAR = "api_set_active_year";
    public static final String ADD_ACADAMIC_YEAR = "add_acadamic_year";
    public static final String UPDATE_ACADAMIC_YEAR = "update_acadamic_year";
    public static final String API_ADD_ACADEMIC_YEAR = "api_add_academic_year";
    public static final String API_UPDATE_ACADEMIC_YEAR = "api_update_academic_year";
    public static final String TEACHER_NAME = "teacher_name";
    public static final String SUBJECT_NAME = "subject_name";
    public static final String WEEK_DAY = "week_day";
    public static final String TIME_SLOT = "time_slot";
    public static final String API_END_YEAR = "api_end_year";
    public static final String YEAR_COMPLETED = "completed";

    public static final String NO_INTERNET = "Not Connected! Please check your internet connection.";
    public static final String API_ASSIGNED_TEACHER_LIST = "api_assigned_teacher_list";
    public static final String API_DELETE_ASSIGNED_TEACHER = "api_delete_assigned_teacher";
    public static final int CAMERA_ATTACHMENT_RESULT_CODE = 110;
    public static final String API_MULTIPLE_FILE_UPLOAD = "api_multiple_file_upload";
    public static final String API_GET_ALL_ROUTES = "api_get_all_routes";
    public static final String API_SEARCH_ROUTE = "api_search_route";
    public static final String API_ASSIGN_STUDENTS = "api_assign_students";
    public static final String PICK_UP = "PICKUP";
    public static final String DROP_OFF = "DROPOFF";
    public static final String CHANGE_ASSIGNMENT = "change_assignment";
    public static final String API_GET_ACTIVE_ROUTES = "api_get_active_routes";
    public static final String API_HELP = "api_help";
    public static final String API_GET_QR = "api_get_qr";
    public static final long LOCATION_REFRESH_TIME = 0;
    public static final float LOCATION_REFRESH_DISTANCE = 0;
    public static final String API_CREATE_ROUTE = "api_create_route";
    public static final String API_GET_TRIPS_OF_ROUTES = "api_get_trips_of_route";
    public static final String API_GET_TRIP_HISTORY = "api_get_trip_history";
    public static final String API_SEND_EMERGENCY_COMMUNICATION_TO_ROUTE = "api_send_emergency_communication_to_route";
    public static final String API_SEND_EMERGENCY_COMMUNICATION_TO_ACTIVE_TRIP = "api_send_emergency_communication_to_active_trip";
    public static final String API_GET_SCHOOL_INFO = "api_get_school_info";
    public static final String API_UPDATE_ROUTE = "api_update_route";
    public static final String API_ASSIGN_PASSANGER = "api_assign_passanger";
    public static final String API_UNASSIGN_PASSANGER = "api_unassign_passanger";
    public static final String TRIP_IDS = "trip_ids";
    public static final String STUDENT = "student";
    public static final String API_ASSIGN_DRIVER = "assign_driver_to_route";
    public static final String API_GET_FULL_ROUTE_INFO = "api_get_full_route_info";
    public static final String API_UN_ASSIGN_DRIVER = "api_un_assign_driver_from_route";
    public static final double CIRCLE_RADIUS = 150;
    public static final String APPOINTMENT = "Appointment";
    public static final String API_GET_TOTAL_BOARDING = "api_total_boarding";
    public static final String API_DELETE_EVENTS = "api_delete_events";
    public static final String API_UPDATE_SCHOOL_INFO = "api_update_school_info";
    public static final int PHOTOS_SELECT = 1588;

    public static String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    public static int REQ_LOCATION = 153;
    public static final int REQ_PICK_FILE_FOR_ASSIGNMENT = 7;
    public static final int REQ_CODE_SPEECH_INPUT = 369;

    public static final String VIEW_LESS = "View Less";
    public static final String VIEW_MORE = "View More";


    public static final String THUMBNAIL_PATH = "FilePick";

    public static final String TOKEN_EXPIRED_MESSAGE = "Auth token is expired";

    public static final String UPDATE_FRAGEMENT_BROADCAST = "update_fragment_broadcast";
    public static final String SCHOOL_CHANNEL = "school_channel";
    public static final String TYPE = "type";

    public static final String API_LOGIN = "login";
    public static final String API_FORGOT_PASS = "login";
    public static final String API_TEACHER_COMMUNICATION = "teacher_communication";
    public static final String API_CREATE_SCHOOL_CHANNEL_MESSAGE = "create_school_channel_message";
    public static final String API_CREATE_CLASS_CHANNEL_MESSAGE = "create_class_channel_message";

    public static final String API_STUDENT_LIST = "student_list";
    public static final String API_GET_ALL_DRIVERS = "get_all_drivers";
    public static final String API_GET_ALL_TEACHERS = "get_all_teachers";

    public static final String API_ASSIGN_TEACHER = "api_assign_student";
    public static final String API_UPDATE_ASSIGNED_TEACHER = "api_update_assigned_student";

    public static final String API_PREVIOUS_ATTENDANCE = "previous_attendance";
    public static final String API_UPDATE_ATTENDANCE = "update_attendance";
    public static final String API_SEND_COMMUNICATION_MESSAGE = "send_comm_message";
    public static final String API_TEACHER_CLASS_SCHEDULE = "teacher_class_schedule";
    public static final String API_SUBMIT_ATTENDANCE = "submit_attendance";
    public static final String API_SUBMIT_HOMEWORK = "submit_homework";
    public static final String API_PREVIOUS_HOMEWORK = "previous_homework";
    public static final String API_COMMUNICATION_THREAD = "communication_thread";
    public static final String API_REPLY_COMMUNICATION = "reply_communication_thread";
    public static final String API_TEACHER_APPOINTMENT = "teacher_appointment";
    public static final String API_UPCOMING_APPOINMENT = "upcoming_appointment";
    public static final String API_PAST_APPOINMENT = "past_appointment";
    public static final String API_CANCEL_APPOINTMENT = "cancel_appointment";
    public static final String API_CONFIRM_APPOINMENT = "confirm_appointment";
    public static final String API_ADD_EVENT = "add_event";
    public static final String API_UPDATE_EVENT = "update_event";
    public static final String API_SEND_NOTIFICATION_TO_CLASS = "send_notification_to_class";
    public static final String API_CLASS_CHANNEL_DATA = "class_channel_data";
    public static final String API_SCHOOL_CHANNEL_DATA = "school_channel_data";

    public static final String CONFIRMED = "confirmed";
    public static final String CANCELLED = "cancelled";


    public static final String API_GET_COMMUNICATION_LIST = "teacher_communication_list";
    public static final String API_ACCEPT_INVITATION = "accept_invitation";
    public static final String API_DECLINE_INVITATION = "decline_invitation";
    public static final String API_TEACHER_LIST = "teacher_list";
    public static final String API_SEND_INVITATION = "send_invitation_to_teacher";
    public static final String API_ADD_TEACHER_TO_GROUP = "add_teacher_to_group";
    public static final String API_SEARCH_TEACHER = "search_teacher";
    public static final String API_SEARCH_DRIVER= "search_driver";
    public static final String API_CLASS_ACTIVITY_DATA = "class_Activity";
    public static final String API_CREATE_CLASS_ACTIVITY = "create_class_activity";
    public static final String API_SCHOOL_ACTIVITY_DATA = "school_Activity";
    public static final String API_APPOINTMENT_BY_ID = "appointment_by_id";
    public static final String API_UPDATE_EVENT_TO_CLASS = "update_event_class";
    public static final String API_FEEDBACK_LIST = "feedback_list";
    public static final String API_ADD_FEEDBACK = "add_feedback";
    public static final String API_FEEDBACK_LIST_STUDENT_WISE = "feedback_list_student_wise";


}
