/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.app;

import android.app.Activity;
import android.app.Application;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Point;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.CalendarContract;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import ca.dataready.SmartSchoolAdmin.BuildConfig;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.server.AppApi;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.ConfigResponse;
import ca.dataready.SmartSchoolAdmin.server.GetSchoolInfo;
import ca.dataready.SmartSchoolAdmin.server.TeacherAppointmentModel;
import io.fabric.sdk.android.Fabric;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


public class AdminApp extends Application {

    private static final String CONSTANT_CONFIG = "constant_config";
    private static final String CONSTANT_API_URL = "constant_api_url";
    private static final String CONSTANT_API_VERSION = "constant_api_version";
    private static final String APP_LEVEL_PREFS = "app_level_prefs";
    private static final String CONSTANT_SCHOOL_INFO = "constant_school_info";
    private static AdminApp mInstance;
    private static Retrofit retrofit;
    private static SharedPreferences sharedPreferences;

    private static final String CONSTANT_CREDENTIAL = "credential";
    private static final String CONSTANT_PASSWORD = "password";
    private static final String CONSTANT_LOGINTYPE = "logintype";
    private static final String CONSTANT_TOKEN = "authToken";


    @Override
    public void onCreate() {
        super.onCreate();
        sharedPreferences = getSharedPreferences(APP_LEVEL_PREFS, Context.MODE_PRIVATE);
        Fresco.initialize(this);
        // if (!BuildConfig.DEBUG)
        Fabric.with(this, new Crashlytics());
        MultiDex.install(this);
        this.mInstance = this;
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("AVENIRLTSTD-REGULAR.OTF")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        Crashlytics.log("Hey I've created custom log! YAY :)");
        if (getCredential() != null && getAdmin() != null)
            Crashlytics.setString("emailID", getAdmin().getEmailId());
        if (getConfigs() != null) {
            if (getConfigs().getResult() != null) {
                Crashlytics.setString("appVersion", getConfigs().getResult().getAppversion() + " (" + getConfigs().getResult().getAppbuildnumber() + ")");
                Crashlytics.setString("environment", getConfigs().getResult().getEnv());
                Crashlytics.setString("region", getConfigs().getResult().getRegion());
            }
        }
    }

    public static synchronized AdminApp getInstance() {
        return mInstance;
    }

    public synchronized void ToastSomethingWrong() {

        Toast.makeText(this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
    }

    public synchronized String weekDay() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        switch (day) {
            case Calendar.SUNDAY:
                day = 7;
                break;

            case Calendar.MONDAY:
                day = 1;
                break;

            case Calendar.TUESDAY:
                day = 2;
                break;

            case Calendar.WEDNESDAY:
                day = 3;
                break;

            case Calendar.THURSDAY:
                day = 4;
                break;

            case Calendar.FRIDAY:
                day = 5;
                break;

            case Calendar.SATURDAY:
                day = 6;
                break;


        }

        return day + "";
    }

    public synchronized String weekDayInNumbers(String weekDay) {

        String weekday = "";

        if (weekDay.equals(getString(R.string.monday))) {
            weekday = "1";
        } else if (weekDay.equals(getString(R.string.tuesday))) {
            weekday = "2";
        } else if (weekDay.equals(getString(R.string.wednesday))) {
            weekday = "3";
        } else if (weekDay.equals(getString(R.string.thursday))) {
            weekday = "4";
        } else if (weekDay.equals(getString(R.string.friday))) {
            weekday = "5";
        } else if (weekDay.equals(getString(R.string.saturday))) {
            weekday = "6";
        }

        return weekday;
    }

    public synchronized String weekDayInLatters(String weekDay) {

        String weekday = "";

        if (weekDay.equals("1")) {
            weekday = getString(R.string.monday);
        } else if (weekDay.equals("2")) {
            weekday = getString(R.string.tuesday);
        } else if (weekDay.equals("3")) {
            weekday = getString(R.string.wednesday);
        } else if (weekDay.equals("4")) {
            weekday = getString(R.string.thursday);
        } else if (weekDay.equals("5")) {
            weekday = getString(R.string.friday);
        } else if (weekDay.equals("6")) {
            weekday = getString(R.string.saturday);
        }

        return weekday;
    }

    public synchronized void addEventToDeviceCalander(TeacherAppointmentModel.ResultBean entity) {

        try {
            if (entity == null || entity.getAppointmentDate() == null || entity.getAppointmentTime() == null)
                return;

            if (entity.getAppointmentDate().contains("-") && entity.getAppointmentTime().contains("-")) {
                String[] timeArray = entity.getAppointmentTime().split("-");

                Calendar cal = Calendar.getInstance();

                SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
                SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
                Date sTime = parseFormat.parse(timeArray[0]);
                Date eTime = parseFormat.parse(timeArray[1]);

                String Stime = entity.getAppointmentDate() + " " + displayFormat.format(sTime);
                String Etime = entity.getAppointmentDate() + " " + displayFormat.format(eTime);

                Log.e("Stime", "" + Stime);
                Log.e("Etime", "" + Etime);

                Date sdt = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(Stime);
                Date edt = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(Etime);

                Calendar beginTime = Calendar.getInstance();
                cal.setTime(sdt);

                // beginTime.set(year, month, day, hour, minute);
                beginTime.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
                        cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY),
                        cal.get(Calendar.MINUTE));

                Calendar endTime = Calendar.getInstance();
                cal.setTime(edt);

                // endTime.set(year, month, day, hour, minute);
                endTime.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
                        cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY),
                        cal.get(Calendar.MINUTE));


                ContentResolver cr = getContentResolver();
                ContentValues values = new ContentValues();

                values.put(CalendarContract.Events.DTSTART, beginTime.getTimeInMillis());
                values.put(CalendarContract.Events.DTEND, endTime.getTimeInMillis());

                values.put(CalendarContract.Events.TITLE, entity.getAppointmentTitle());
                values.put(CalendarContract.Events.DESCRIPTION, entity.getAppointmentTitle());

                TimeZone timeZone = TimeZone.getDefault();
                values.put(CalendarContract.Events.EVENT_TIMEZONE, timeZone.getID());

                // Default calendar
                values.put(CalendarContract.Events.CALENDAR_ID, 1);
                values.put(CalendarContract.Events.HAS_ALARM, 1);

                // Insert event to calendar
                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);
                long eventID = Long.parseLong(uri.getLastPathSegment());
                Log.e("EventID", "" + eventID);
                setReminder(cr, eventID, 30);

            } else {
                Toast.makeText(this, getString(R.string.event_not_added_to_calender), Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, getString(R.string.event_not_added_to_calender), Toast.LENGTH_SHORT).show();
        }
    }


    public synchronized void setReminder(ContentResolver cr, long eventID, int timeBefore) {
        try {
            ContentValues values = new ContentValues();
            values.put(CalendarContract.Reminders.MINUTES, timeBefore);
            values.put(CalendarContract.Reminders.EVENT_ID, eventID);
            values.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            Uri uri = cr.insert(CalendarContract.Reminders.CONTENT_URI, values);
            Cursor c = CalendarContract.Reminders.query(cr, eventID,
                    new String[]{CalendarContract.Reminders.MINUTES});
            if (c.moveToFirst()) {
                Log.e("Reminder Uri", uri.toString());
                Log.e("", "calendar"
                        + c.getInt(c.getColumnIndex(CalendarContract.Reminders.MINUTES)));
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public synchronized AppApi getApi() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        //log level
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);


        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                okhttp3.Request original = chain.request();

                // Request customization: add request headers
                okhttp3.Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("Content-Type", "application/json");
                if (!AuthToken().equals("")) {
                    requestBuilder.addHeader("AuthToken", AuthToken());
                }
                requestBuilder.addHeader("language", Locale.getDefault().getDisplayLanguage());
                okhttp3.Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });


        httpClient.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                okhttp3.Request request = chain.request();
                Response response = chain.proceed(request);
                response.code();
                return response;
            }
        });
        OkHttpClient client = httpClient.addInterceptor(interceptor).build();
        retrofit = new Retrofit.Builder()
                .baseUrl(AppApi.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .build();
        return retrofit.create(AppApi.class);
    }

    public synchronized AppApi getConfigApi() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        //log level
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);


        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                okhttp3.Request original = chain.request();

                // Request customization: add request headers
                okhttp3.Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("Content-Type", "application/json");
                if (!AuthToken().equals("")) {
                    requestBuilder.addHeader("AuthToken", AuthToken());
                }
                okhttp3.Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });


        httpClient.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                okhttp3.Request request = chain.request();
                Response response = chain.proceed(request);
                response.code();
                return response;
            }
        });
        OkHttpClient client = httpClient.addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.dataready.us/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .build();
        AppApi github = retrofit.create(AppApi.class);
        return github;
    }


    public synchronized AppApi getLocationAPI() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        //log level
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);


        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                okhttp3.Request original = chain.request();

                // Request customization: add request headers
                okhttp3.Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("Content-Type", "application/json");
                okhttp3.Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });


        httpClient.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                okhttp3.Request request = chain.request();
                Response response = chain.proceed(request);
                response.code();
                return response;
            }
        });
        OkHttpClient client = httpClient.addInterceptor(interceptor).build();
        retrofit = new Retrofit.Builder()

                .baseUrl(AppApi.GEOCODING_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .build();
        AppApi github = retrofit.create(AppApi.class);
        return github;
    }


    public int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    }


    public synchronized AppApi getLoginApi() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        //log level
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);


        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                okhttp3.Request original = chain.request();

                // Request customization: add request headers
                okhttp3.Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("Content-Type", "application/json");
                String token = FirebaseInstanceId.getInstance().getToken();
                if (token != null)
                    requestBuilder.addHeader("deviceTokenId", token);
                else
                    requestBuilder.addHeader("deviceTokenId", "SmartSchoolAdmin");
                requestBuilder.addHeader("language", Locale.getDefault().getDisplayLanguage());
                okhttp3.Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });


        httpClient.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                okhttp3.Request request = chain.request();
                Response response = chain.proceed(request);
                response.code();
                return response;
            }
        });
        OkHttpClient client = httpClient.addInterceptor(interceptor).build();
        retrofit = new Retrofit.Builder()
                .baseUrl(AppApi.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .build();
        AppApi github = retrofit.create(AppApi.class);
        return github;
    }

    public synchronized Retrofit getRetrofit() {
        return retrofit;
    }


    public synchronized void saveCredential(CREDENTIAL loginModels) {
        if (loginModels != null) {
            Gson gson = new Gson();
            String json = gson.toJson(loginModels);
            sharedPreferences.edit().putString(CONSTANT_CREDENTIAL, json).apply();
            sharedPreferences.edit().putString(CONSTANT_TOKEN, loginModels.getAuthtoken()).apply();
        }

    }

    public synchronized CREDENTIAL getCredential() {
        CREDENTIAL loginModel = null;
        try {
            Gson gson = new Gson();
            String json = sharedPreferences.getString(CONSTANT_CREDENTIAL, null);
            loginModel = gson.fromJson(json, CREDENTIAL.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loginModel;
    }

    public synchronized void saveSchoolInfo(GetSchoolInfo getSchoolInfo) {

        if (getSchoolInfo != null) {
            Gson gson = new Gson();
            String json = gson.toJson(getSchoolInfo);
            sharedPreferences.edit().putString(CONSTANT_SCHOOL_INFO, json).apply();
        }
    }

    public synchronized GetSchoolInfo getSchoolInfo() {
        GetSchoolInfo getSchoolInfo = null;
        try {
            Gson gson = new Gson();
            String json = sharedPreferences.getString(CONSTANT_SCHOOL_INFO, null);
            getSchoolInfo = gson.fromJson(json, GetSchoolInfo.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return getSchoolInfo;
    }

    public synchronized CREDENTIAL.ResultBean getAdmin() {

        CREDENTIAL.ResultBean loginModel = null;
        try {
            Gson gson = new Gson();
            String json = sharedPreferences.getString(CONSTANT_CREDENTIAL, null);
            CREDENTIAL model = gson.fromJson(json, CREDENTIAL.class);
            if (model != null && model.getResult() != null && model.getResult().size() > 0) {
                loginModel = model.getResult().get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return loginModel;
    }

    public synchronized ConfigResponse getConfigs() {
        ConfigResponse configResponse = null;
        try {
            Gson gson = new Gson();
            String json = sharedPreferences.getString(CONSTANT_CONFIG, null);
            configResponse = gson.fromJson(json, ConfigResponse.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return configResponse;
    }

    public synchronized void saveConfigs(ConfigResponse configResponse) {

        if (configResponse != null) {
            Gson gson = new Gson();
            String json = gson.toJson(configResponse);
            sharedPreferences.edit().putString(CONSTANT_CONFIG, json).commit();
            sharedPreferences.edit().putString(CONSTANT_API_URL, configResponse.getResult().getApiurl()).commit();
            sharedPreferences.edit().putString(CONSTANT_API_VERSION, configResponse.getResult().getApiversion()).commit();
        }
    }

    public synchronized String getApiVersion() {
        return sharedPreferences.getString(CONSTANT_API_VERSION, "1.2.0");
    }

    public synchronized String getApiUrl() {
        String json = sharedPreferences.getString(CONSTANT_API_URL, "dev.api.dataready.us");
        return json;
    }


    public synchronized void logout() {
        sharedPreferences.edit().remove(CONSTANT_CREDENTIAL).apply();
        SharedPreferences preferences = getSharedPreferences("counter_preference", Context.MODE_PRIVATE);
        preferences.edit().clear().apply();
    }

    public synchronized String AuthToken() {
        String json = sharedPreferences.getString(CONSTANT_TOKEN, "");
        return json;
    }

    public synchronized String currentDate() {
        Calendar c = Calendar.getInstance();
        //System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public synchronized int currentyear() {
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.YEAR);
    }
}
