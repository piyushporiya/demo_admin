/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.subject.add;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.AddSubjectParams;
import ca.dataready.SmartSchoolAdmin.server.AddUpdateSubjectResponse;
import ca.dataready.SmartSchoolAdmin.server.SubjectListResponse;
import info.hoang8f.android.segmented.SegmentedGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddSubjectActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txtAddSubject)
    TextView txtAddSubject;
    @BindView(R.id.card_add_subject)
    CardView cardAddSubject;
    @BindView(R.id.et_subject_name)
    EditText etSubjectName;
    @BindView(R.id.et_subject_code)
    EditText etSubjectCode;
    @BindView(R.id.et_prerequisite)
    EditText etPrerequisite;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.layout_add_subject)
    CardView layoutAddSubject;
    @BindView(R.id.rb_optional)
    RadioButton rbOptional;
    @BindView(R.id.rb_mandatory)
    RadioButton rbMandatory;
    @BindView(R.id.sgSubjectType)
    SegmentedGroup sgSubjectType;
    private String gradeId;
    private Call<AddUpdateSubjectResponse> addCall;
    private String subjectType;
    private SubjectListResponse.ResultBean bean;
    private String type;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_subject);
        ButterKnife.bind(this);
        setHeaderInfo();
        Init();
    }

    private void setHeaderInfo() {

        setSupportActionBar(toolbar);

        if (AdminApp.getInstance().getAdmin() != null)
            getSupportActionBar().setTitle(AdminApp.getInstance().getAdmin().getSchoolName() + " " + AdminApp.getInstance().getAdmin().getSchoolYear());
        else
            getSupportActionBar().setTitle(getString(R.string.app_name));

        getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + getString(R.string.add_subject) + "</small>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void Init() {

        if (getIntent().getExtras() != null) {

            type = getIntent().getStringExtra(App_Constants.TYPE);

            if (type != null && type.equals(App_Constants.UPDATE)) {
                bean = getIntent().getParcelableExtra(App_Constants.OBJECT);

                if (bean != null) {

                    btnSubmit.setText(getString(R.string.update));
                    etSubjectName.setText(bean.getSubjectName());
                    etSubjectCode.setText(bean.getSubjectCode());
                    etPrerequisite.setText(bean.getPrerequisite());
                    if (bean.getSubjectType() != null && bean.getSubjectType().equals(rbMandatory.getText().toString()))
                        rbMandatory.setChecked(true);
                    else
                        rbOptional.setChecked(true);
                }

            } else {
                gradeId = getIntent().getStringExtra(App_Constants.GRADE_ID);
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);

    }

    @OnClick({R.id.btn_submit, R.id.card_add_subject})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_submit:
                validateData();
                break;
            case R.id.card_add_subject:
                layoutAddSubject.setVisibility(layoutAddSubject.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                break;
        }
    }

    private void validateData() {

        if (etSubjectName.getText().toString().isEmpty())
            etSubjectName.setError(getString(R.string.field_required));
        else if (etSubjectCode.getText().toString().isEmpty())
            etSubjectCode.setError(getString(R.string.field_required));
        else {
            if (btnSubmit.getText().toString().equals(getString(R.string.submit)))
                addSubject();
            else
                updateSubject();
        }
    }


    private void addSubject() {

        if (sgSubjectType.getCheckedRadioButtonId() == R.id.rb_optional)
            subjectType = getString(R.string.optional);
        else
            subjectType = getString(R.string.mandatory);

        AddSubjectParams params = new AddSubjectParams();
        params.setGradeId(gradeId);
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
        params.setSubjectName(etSubjectName.getText().toString());
        params.setSubjectCode(etSubjectCode.getText().toString());
        params.setPrerequisite(etPrerequisite.getText().toString());
        params.setSubjectType(subjectType);

        Utility.showProgress(AddSubjectActivity.this, getString(R.string.processing));

        addCall = AdminApp.getInstance().getApi().addSubject(params);
        addCall.enqueue(new Callback<AddUpdateSubjectResponse>() {
            @Override
            public void onResponse(Call<AddUpdateSubjectResponse> call, Response<AddUpdateSubjectResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            Toast.makeText(AddSubjectActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            setBackIntent();
                        } else {
                            Toast.makeText(AddSubjectActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        APIError error = APIError.parseError(response, AddSubjectActivity.this, App_Constants.API_ADD_SUBJECT);
                        Toast.makeText(AddSubjectActivity.this, error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(AddSubjectActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddUpdateSubjectResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(AddSubjectActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void updateSubject() {

        if (sgSubjectType.getCheckedRadioButtonId() == R.id.rb_optional)
            subjectType = getString(R.string.optional);
        else
            subjectType = getString(R.string.mandatory);

        AddSubjectParams params = new AddSubjectParams();
        params.setId(bean.getId());
        params.setGradeId(bean.getGradeId());
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
        params.setSubjectName(etSubjectName.getText().toString());
        params.setSubjectCode(etSubjectCode.getText().toString());
        params.setPrerequisite(etPrerequisite.getText().toString());
        params.setSubjectType(subjectType);

        Utility.showProgress(AddSubjectActivity.this, getString(R.string.processing));

        addCall = AdminApp.getInstance().getApi().updateSubject(params);
        addCall.enqueue(new Callback<AddUpdateSubjectResponse>() {
            @Override
            public void onResponse(Call<AddUpdateSubjectResponse> call, Response<AddUpdateSubjectResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            Toast.makeText(AddSubjectActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            setBackIntent();
                        } else {
                            Toast.makeText(AddSubjectActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        APIError error = APIError.parseError(response, AddSubjectActivity.this, App_Constants.API_UPDATE_SUBJECT);
                        Toast.makeText(AddSubjectActivity.this, error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(AddSubjectActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddUpdateSubjectResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(AddSubjectActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == APIError.UPDATE_TOKEN && resultCode == RESULT_OK) {

            if (data != null) {
                String apiName = data.getStringExtra("api");
                onRelaod(apiName);
            }

        }
    }


    private void onRelaod(String apiName) {

        if (apiName != null) {
            if (apiName.equals(App_Constants.API_ADD_SUBJECT)) {
                addSubject();
            } else if (apiName.equals(App_Constants.API_UPDATE_SUBJECT)) {
                updateSubject();
            }
        }
    }


    private void setBackIntent() {

        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        onBackPressed();
    }
}
