/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.subject;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.Listeners.OnReloadListener;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.DeleteSubjectResponse;
import ca.dataready.SmartSchoolAdmin.server.SubjectListResponse;
import ca.dataready.SmartSchoolAdmin.subject.adapter.SubjectAdapter;
import ca.dataready.SmartSchoolAdmin.subject.add.AddSubjectActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class SubjectFragment extends Fragment implements OnReloadListener, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.recyclerView)
    SuperRecyclerView recyclerView;
    @BindView(R.id.fab_create)
    FloatingActionButton fabCreate;
    Unbinder unbinder;
    private GridLayoutManager gridLayoutManager;
    private String gradeId;
    private Call<SubjectListResponse> appApi;
    private List<SubjectListResponse.ResultBean> results;
    private SubjectAdapter adapter;
    private String subjectId;
    private Call<DeleteSubjectResponse> deleteCall;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_subject, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) getActivity()).setSubTitle(getString(R.string.subject));
        ((HomeActivity) getActivity()).setOnReloadListener(this);
        Init();
    }

    private void Init() {

        Bundle bundle = this.getArguments();

        if (bundle != null) {

            gradeId = bundle.getString(App_Constants.GRADE_ID);
        }

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            gridLayoutManager = new GridLayoutManager(getContext(), 3);
        } else {
            gridLayoutManager = new GridLayoutManager(getContext(), 2);
        }

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setRefreshListener(SubjectFragment.this);

        getSubjectList();

    }


    private void getSubjectList() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();

        appApi = AdminApp.getInstance().getApi().getSubjectList(entity.getSchoolId(), gradeId);
        appApi.enqueue(new Callback<SubjectListResponse>() {
            @Override
            public void onResponse(Call<SubjectListResponse> call, Response<SubjectListResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            results = response.body().getResult();
                            if (results != null && results.size() > 0) {

                                setAdapter();

                            } else {

                                results = null;
                                adapter = new SubjectAdapter(getActivity(), SubjectFragment.this);
                                recyclerView.setAdapter(adapter);
                                TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                                emptyView.setText(getString(R.string.no_data));
                            }

                        } else {

                            results = null;
                            adapter = new SubjectAdapter(getActivity(), SubjectFragment.this);
                            recyclerView.setAdapter(adapter);
                            TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                            emptyView.setText(getString(R.string.no_data));
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_GET_GRADE_LIST);
                        adapter = new SubjectAdapter(getActivity(), SubjectFragment.this);
                        recyclerView.setAdapter(adapter);
                        TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                        emptyView.setText(error.message());

                    }
                } catch (Exception e) {

                    adapter = new SubjectAdapter(getActivity(), SubjectFragment.this);
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(getString(R.string.somethingwrong));
                }

            }

            @Override
            public void onFailure(Call<SubjectListResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    adapter = new SubjectAdapter(getActivity(), SubjectFragment.this);
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(App_Constants.NO_INTERNET);
                }
            }
        });
    }

    private void setAdapter() {

        adapter = new SubjectAdapter(getActivity(), SubjectFragment.this);
        recyclerView.setAdapter(adapter);
        adapter.addItems(results);

    }


    @OnClick({R.id.fab_create})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.fab_create:

                startActivityForResult(new Intent(getActivity(), AddSubjectActivity.class)
                        .putExtra(App_Constants.GRADE_ID, gradeId), App_Constants.UPDATE_LISTING);
                if (getActivity() != null)
                    getActivity().overridePendingTransition(R.anim.enter, R.anim.leave);


                break;
        }
    }

    public void showPopUp(ImageView imgMore, int position, SubjectListResponse.ResultBean bean) {

        PopupMenu popup = new PopupMenu(imgMore.getContext(), imgMore);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.edit_subject_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new CardMenuItemClickListener(position, bean));
        popup.show();
    }

    private class CardMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        private int position;
        private SubjectListResponse.ResultBean bean;

        CardMenuItemClickListener(int positon, SubjectListResponse.ResultBean bean) {
            this.position = positon;
            this.bean = bean;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {

                case R.id.item_edit:

                    startActivityForResult(new Intent(getActivity(), AddSubjectActivity.class)
                            .putExtra(App_Constants.OBJECT, bean)
                            .putExtra(App_Constants.TYPE, App_Constants.UPDATE), App_Constants.UPDATE_LISTING);
                    if (getActivity() != null)
                        getActivity().overridePendingTransition(R.anim.enter, R.anim.leave);

                    break;

                case R.id.item_delete:

                    subjectId = bean.getId();

                    deleteSubjectDialog();

                    break;

                default:

                    break;
            }
            return false;
        }
    }

    private void deleteSubjectDialog() {


        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle(getString(R.string.alert));
        alertDialog.setMessage(getString(R.string.delete_subject_msg));

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                alertDialog.dismiss();
                deleteSubject();
            }
        });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                alertDialog.dismiss();

            }
        });

        alertDialog.show();

    }

    private void deleteSubject() {

        Utility.showProgress(getActivity(), getString(R.string.processing));

        deleteCall = AdminApp.getInstance().getApi().deleteSubject(AdminApp.getInstance().getAdmin().getSchoolId(), subjectId);
        deleteCall.enqueue(new Callback<DeleteSubjectResponse>() {
            @Override
            public void onResponse(Call<DeleteSubjectResponse> call, Response<DeleteSubjectResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            getSubjectList();
                        } else {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_DELETE_SUBJECT);
                        Toast.makeText(getActivity(), error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(getActivity(), getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DeleteSubjectResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    Log.e("ERROR", "" + t.getMessage());
                    Utility.hideProgress();
                    Toast.makeText(getActivity(), App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public void onStop() {
        super.onStop();
        if (appApi != null)
            appApi.cancel();

        if (deleteCall != null)
            deleteCall.cancel();
    }

    @Override
    public void onReload(String apiName) {

        if (apiName != null && isVisible()) {
            if (apiName.equals(App_Constants.API_GET_SUBJECT_LIST)) {
                getSubjectList();
            } else if (apiName.equals(App_Constants.API_DELETE_SUBJECT)) {
                deleteSubject();
            }
        }
    }

    @Override
    public void onRefresh() {

        if (isVisible())
            getSubjectList();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == APIError.UPDATE_TOKEN && resultCode == RESULT_OK) {

            if (data != null) {
                String apiName = data.getStringExtra("api");
                onReload(apiName);
            }

        } else if (requestCode == App_Constants.UPDATE_LISTING && resultCode == RESULT_OK) {

            getSubjectList();

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            gridLayoutManager = new GridLayoutManager(getContext(), 3);
        } else {
            gridLayoutManager = new GridLayoutManager(getContext(), 2);
        }

        recyclerView.setLayoutManager(gridLayoutManager);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
