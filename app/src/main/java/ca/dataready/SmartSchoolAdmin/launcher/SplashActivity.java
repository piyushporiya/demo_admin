/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.launcher;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.chat.ChatActivity;
import ca.dataready.SmartSchoolAdmin.login.LoginActivity;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.ConfigParams;
import ca.dataready.SmartSchoolAdmin.server.ConfigResponse;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.ReplyCommunicationModel;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.ThreadCommunicationModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.APPOINTMENT_ID;
import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.CHANNEL_TYPE;
import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.CLASS_ID;
import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.GRADE_ID;
import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.GROUP_ID;
import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.MESSAGE_ID;
import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.MESSAGE_TYPE;
import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.NOTIFICATION_ID;
import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.PROFILE_ID;
import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.SCHOOL_ID;
import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.STUDENT_ID;

public class SplashActivity extends BaseActivity {

    private static final int REQUEST_PERMISSION_CODE = 101;
    private Call<ConfigResponse> call;
    private String version;
    private String versionName;
    String channelType, messageType;
    private String gradId, classId, studentId, profileId, schoolId, messageId, appointmentId, notificationId, groupId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(CHANNEL_TYPE) || extras.containsKey(MESSAGE_TYPE)) {

                channelType = extras.getString(CHANNEL_TYPE);
                messageType = extras.getString(MESSAGE_TYPE);
                gradId = extras.getString(GRADE_ID);
                classId = extras.getString(CLASS_ID);
                studentId = extras.getString(STUDENT_ID);
                profileId = extras.getString(PROFILE_ID);
                schoolId = extras.getString(SCHOOL_ID);
                messageId = extras.getString(MESSAGE_ID);
                appointmentId = extras.getString(APPOINTMENT_ID);
                notificationId = extras.getString(NOTIFICATION_ID);
                groupId = extras.getString(GROUP_ID);

                Log.e("message", "found in splash");
                Log.e("channelType", "" + extras.getString(CHANNEL_TYPE));
                Log.e("messageType", "" + extras.getString(MESSAGE_TYPE));
                Log.e("gradId", "" + extras.getString(GRADE_ID));
                Log.e("classId", "" + extras.getString(CLASS_ID));
                Log.e("studentId", "" + extras.getString(STUDENT_ID));
                Log.e("profileId", "" + extras.getString(PROFILE_ID));
                Log.e("schoolId", "" + extras.getString(SCHOOL_ID));
                Log.e("messageId", "" + extras.getString(MESSAGE_ID));
                Log.e("notificationId", "" + extras.getString(APPOINTMENT_ID));
                Log.e("groupId", "" + extras.getString(GROUP_ID));

            } else {

                Log.e("message", "not found");
            }
        }

        Init();
    }

    private void Init() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_PERMISSION_CODE);
            } else {
                getConfig();
            }
        } else {
            getConfig();
        }
    }

    private void getConfig() {

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = String.valueOf(pInfo.versionCode);
            versionName = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String model = Build.BRAND;
        String language = Locale.getDefault().getDisplayLanguage();
        String osVersion = String.valueOf(Build.VERSION.SDK);

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        String deviceID = telephonyManager.getDeviceId();
        if (deviceID == null)
            deviceID = String.valueOf(new Random().nextInt(100000));
        String region = Locale.getDefault().getCountry();

        ConfigParams params = new ConfigParams();
        params.setApiurl("dev.api.dataready.us");
        params.setApiversion("1.2.0");
        params.setAppversion(version);
        params.setAppbuildnumber(versionName);
        params.setApptype("admin");
        params.setDeviceid(deviceID);
        params.setDevicevendor(model);
        params.setEnv("dev");
        params.setLanguage(language);
        params.setOstype("Android");
        params.setOsversion(osVersion);
        params.setRegion(region);


        call = AdminApp.getInstance().getConfigApi().getConfig(params);
        call.enqueue(new Callback<ConfigResponse>() {
            @Override
            public void onResponse(Call<ConfigResponse> call, Response<ConfigResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            LoadNextScreen(response.body());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ConfigResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    System.out.println(t.getMessage());
                    AlertDialog alertDialog = new AlertDialog.Builder(SplashActivity.this).create();
                    alertDialog.setTitle(getString(R.string.alert));
                    alertDialog.setCancelable(false);
                    alertDialog.setMessage(App_Constants.NO_INTERNET);
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.try_again),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    Init();
                                }
                            });
                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.cancel),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    finish();
                                }
                            });
                    alertDialog.show();
                }
            }
        });

    }

    private void LoadNextScreen(final ConfigResponse body) {

        if (AdminApp.getInstance().getConfigs() != null) {

            if (AdminApp.getInstance().getConfigs().getResult().getEnv().equals(body.getResult().getEnv())) {

                AdminApp.getInstance().saveConfigs(body);
                if (AdminApp.getInstance().getCredential() != null) {


                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                    if (channelType != null)
                        intent.putExtra(CHANNEL_TYPE, channelType);
                    if (messageType != null)
                        intent.putExtra(MESSAGE_TYPE, messageType);
                    if (gradId != null)
                        intent.putExtra(GRADE_ID, gradId);
                    if (classId != null)
                        intent.putExtra(CLASS_ID, classId);
                    if (studentId != null)
                        intent.putExtra(STUDENT_ID, studentId);
                    if (profileId != null)
                        intent.putExtra(PROFILE_ID, profileId);
                    if (schoolId != null)
                        intent.putExtra(SCHOOL_ID, schoolId);
                    if (messageId != null)
                        intent.putExtra(MESSAGE_ID, messageId);
                    if (appointmentId != null)
                        intent.putExtra(APPOINTMENT_ID, appointmentId);
                    if (notificationId != null)
                        intent.putExtra(NOTIFICATION_ID, notificationId);
                    if (groupId != null)
                        intent.putExtra(GROUP_ID, groupId);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.leave);
                    finish();

                } else {

                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    if (channelType != null)
                        intent.putExtra(CHANNEL_TYPE, channelType);
                    if (messageType != null)
                        intent.putExtra(MESSAGE_TYPE, messageType);
                    if (gradId != null)
                        intent.putExtra(GRADE_ID, gradId);
                    if (classId != null)
                        intent.putExtra(CLASS_ID, classId);
                    if (studentId != null)
                        intent.putExtra(STUDENT_ID, studentId);
                    if (profileId != null)
                        intent.putExtra(PROFILE_ID, profileId);
                    if (schoolId != null)
                        intent.putExtra(SCHOOL_ID, schoolId);
                    if (messageId != null)
                        intent.putExtra(MESSAGE_ID, messageId);
                    if (appointmentId != null)
                        intent.putExtra(APPOINTMENT_ID, appointmentId);
                    if (notificationId != null)
                        intent.putExtra(NOTIFICATION_ID, notificationId);
                    if (groupId != null)
                        intent.putExtra(GROUP_ID, groupId);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.leave);
                    finish();

                }
            } else {

                AdminApp.getInstance().logout();
                AdminApp.getInstance().saveConfigs(body);

                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                if (channelType != null)
                    intent.putExtra(CHANNEL_TYPE, channelType);
                if (messageType != null)
                    intent.putExtra(MESSAGE_TYPE, messageType);
                if (gradId != null)
                    intent.putExtra(GRADE_ID, gradId);
                if (classId != null)
                    intent.putExtra(CLASS_ID, classId);
                if (studentId != null)
                    intent.putExtra(STUDENT_ID, studentId);
                if (profileId != null)
                    intent.putExtra(PROFILE_ID, profileId);
                if (schoolId != null)
                    intent.putExtra(SCHOOL_ID, schoolId);
                if (messageId != null)
                    intent.putExtra(MESSAGE_ID, messageId);
                if (appointmentId != null)
                    intent.putExtra(APPOINTMENT_ID, appointmentId);
                if (notificationId != null)
                    intent.putExtra(NOTIFICATION_ID, notificationId);
                if (groupId != null)
                    intent.putExtra(GROUP_ID, groupId);
                startActivity(intent);

                overridePendingTransition(R.anim.enter, R.anim.leave);
                finish();
            }

        } else {

            AdminApp.getInstance().saveConfigs(body);
            if (AdminApp.getInstance().getCredential() != null) {

                Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                if (channelType != null)
                    intent.putExtra(CHANNEL_TYPE, channelType);
                if (messageType != null)
                    intent.putExtra(MESSAGE_TYPE, messageType);
                if (gradId != null)
                    intent.putExtra(GRADE_ID, gradId);
                if (classId != null)
                    intent.putExtra(CLASS_ID, classId);
                if (studentId != null)
                    intent.putExtra(STUDENT_ID, studentId);
                if (profileId != null)
                    intent.putExtra(PROFILE_ID, profileId);
                if (schoolId != null)
                    intent.putExtra(SCHOOL_ID, schoolId);
                if (messageId != null)
                    intent.putExtra(MESSAGE_ID, messageId);
                if (appointmentId != null)
                    intent.putExtra(APPOINTMENT_ID, appointmentId);
                if (notificationId != null)
                    intent.putExtra(NOTIFICATION_ID, notificationId);
                if (groupId != null)
                    intent.putExtra(GROUP_ID, groupId);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.leave);
                finish();

            } else {

                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                if (channelType != null)
                    intent.putExtra(CHANNEL_TYPE, channelType);
                if (messageType != null)
                    intent.putExtra(MESSAGE_TYPE, messageType);
                if (gradId != null)
                    intent.putExtra(GRADE_ID, gradId);
                if (classId != null)
                    intent.putExtra(CLASS_ID, classId);
                if (studentId != null)
                    intent.putExtra(STUDENT_ID, studentId);
                if (profileId != null)
                    intent.putExtra(PROFILE_ID, profileId);
                if (schoolId != null)
                    intent.putExtra(SCHOOL_ID, schoolId);
                if (messageId != null)
                    intent.putExtra(MESSAGE_ID, messageId);
                if (appointmentId != null)
                    intent.putExtra(APPOINTMENT_ID, appointmentId);
                if (notificationId != null)
                    intent.putExtra(NOTIFICATION_ID, notificationId);
                if (groupId != null)
                    intent.putExtra(GROUP_ID, groupId);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.leave);
                finish();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getConfig();
                } else {
                    AlertDialog alertDialog = new AlertDialog.Builder(SplashActivity.this).create();
                    alertDialog.setTitle(getString(R.string.alert));
                    alertDialog.setMessage(getString(R.string.please_grant_read_phone_permission));
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.go_settings),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    Intent intent = new Intent();
                                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                                    intent.setData(uri);
                                    startActivityForResult(intent, 1022);
                                }
                            });
                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.cancel),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1022) {
            Init();
        }
    }

    @Override
    protected void onStop() {
        if (call != null)
            call.cancel();
        super.onStop();
    }
}
