/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.grade;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.Listeners.OnReloadListener;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.grade.adapter.GradeAdapter;
import ca.dataready.SmartSchoolAdmin.grade.assign.AssignClassNGradeActivity;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.AddGrade;
import ca.dataready.SmartSchoolAdmin.server.AddGradeResponse;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.DeleteGrade;
import ca.dataready.SmartSchoolAdmin.server.GradeListResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GradeFragment extends Fragment implements OnReloadListener, SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.fab_create)
    FloatingActionButton fabCreate;
    private int GRID_COUNT = 3;
    @BindView(R.id.recyclerView)
    SuperRecyclerView recyclerView;
    Unbinder unbinder;
    GradeAdapter adapter;
    private Call<GradeListResponse> appApi;
    private GradeListResponse.ResultBean results;
    private Call<AddGradeResponse> addGradeCall;
    private String gradeName;
    private String className;
    private String gradeId;
    private String classId;
    private GradeListResponse.ResultBean.GradeBean gradeEntity;
    private GradeListResponse.ResultBean.GradeBean.ClassBean classEntity;
    private int exandedPosition;
    private GradeListResponse.ResultBean.GradeBean deletGradeBean;
    private GradeListResponse.ResultBean.GradeBean.ClassBean deleteClassBean;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_grade, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) getActivity()).setSubTitle(getString(R.string.grade));
        ((HomeActivity) getActivity()).setOnReloadListener(this);
        Init();
    }

    private void Init() {

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            GRID_COUNT = 4;
        } else {
            GRID_COUNT = 3;
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setRefreshListener(GradeFragment.this);

        getGradeList();
    }

    private void getGradeList() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();

        appApi = AdminApp.getInstance().getApi().getGradeList(entity.getSchoolId(), entity.getSchoolYear());
        appApi.enqueue(new Callback<GradeListResponse>() {
            @Override
            public void onResponse(Call<GradeListResponse> call, Response<GradeListResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            results = response.body().getResult();
                            if (results != null) {

                                setAdapter();

                            } else {

                                results = null;
                                adapter = new GradeAdapter(getActivity(), GRID_COUNT, GradeFragment.this);
                                recyclerView.setAdapter(adapter);
                                TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                                emptyView.setText(getString(R.string.no_data));
                            }

                        } else {

                            results = null;
                            adapter = new GradeAdapter(getActivity(), GRID_COUNT, GradeFragment.this);
                            recyclerView.setAdapter(adapter);
                            TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                            emptyView.setText(getString(R.string.no_data));
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_GET_GRADE_LIST);
                        adapter = new GradeAdapter(getActivity(), GRID_COUNT, GradeFragment.this);
                        recyclerView.setAdapter(adapter);
                        TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                        emptyView.setText(error.message());

                    }
                } catch (Exception e) {

                    adapter = new GradeAdapter(getActivity(), GRID_COUNT, GradeFragment.this);
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(getString(R.string.somethingwrong));
                }

            }

            @Override
            public void onFailure(Call<GradeListResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    adapter = new GradeAdapter(getActivity(), GRID_COUNT, GradeFragment.this);
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(App_Constants.NO_INTERNET);
                }
            }
        });
    }

    private void setAdapter() {

        List<GradeListResponse.ResultBean.GradeBean> gradeList = new ArrayList<>();

        for (int i = 0; i < results.getGrade().size(); i++) {

            GradeListResponse.ResultBean.GradeBean gradeBean = results.getGrade().get(i);

            GradeListResponse.ResultBean.GradeBean.ClassBean classBean = new GradeListResponse.ResultBean.GradeBean.ClassBean();
            classBean.setAddMore(true);

            gradeBean.getClassX().add(gradeBean.getClassX().size(), classBean);

            if (i == exandedPosition) {
                gradeBean.setExpanded(true);
                //  exandedPosition = -1;
            }
            gradeList.add(gradeBean);
        }


        adapter = new GradeAdapter(getActivity(), GRID_COUNT, GradeFragment.this);
        recyclerView.setAdapter(adapter);
        adapter.addItems(gradeList);

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onReload(String apiName) {

        if (apiName != null && isVisible()) {
            if (apiName.equals(App_Constants.API_GET_GRADE_LIST)) {
                getGradeList();
            } else if (apiName.equals(App_Constants.API_ADD_GRADE_LIST)) {
                addGrade();
            } else if (apiName.equals(App_Constants.API_UPDATE_GRADE_LIST)) {
                updateGrade();
            } else if (apiName.equals(App_Constants.API_ADD_CLASS_LIST)) {
                addClass();
            } else if (apiName.equals(App_Constants.API_UPDATE_CLASS_LIST)) {
                updateClass();
            } else if (apiName.equals(App_Constants.API_DELETE_GRADE_LIST)) {
                deleteGrade();
            } else if (apiName.equals(App_Constants.API_DELETE_CLASS_LIST)) {
                deleteClass();
            }
        }
    }

    @Override
    public void onRefresh() {

        if (isVisible())
            getGradeList();
    }

    @Override
    public void onStop() {
        super.onStop();

        if (appApi != null)
            appApi.cancel();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            GRID_COUNT = 4;
        } else {
            GRID_COUNT = 3;
        }
        if (adapter != null)
            adapter.setGridCount(GRID_COUNT);
    }


    @OnClick({R.id.fab_create})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.fab_create:

                if (results != null) {
                    showAddGradeDialog(App_Constants.ADD_GRADE, null, results.getGrade());
                } else {
                    showAddGradeDialog(App_Constants.ADD_GRADE, null, null);
                }

                break;
        }
    }


    public void showAddGradeDialog(final String type, final GradeListResponse.ResultBean.GradeBean bean, final List<GradeListResponse.ResultBean.GradeBean> beans) {

        final View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_add_grade, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if (type.equals(App_Constants.ADD_GRADE))
            builder.setTitle(R.string.add_grade);
        else
            builder.setTitle(R.string.update_grade);
        builder.setView(view);
        builder.setCancelable(true);

        final EditText etGradeID = (EditText) view.findViewById(R.id.et_grade_id);
        final EditText etGradeName = (EditText) view.findViewById(R.id.et_grade_name);

        if (!type.equals(App_Constants.ADD_GRADE)) {

            etGradeID.setText(bean.getGradeId());
            etGradeID.setSelection(bean.getGradeId().length());
            etGradeName.setText(bean.getGradeName());
            etGradeName.setSelection(bean.getGradeName().length());

            etGradeID.setEnabled(false);
            etGradeID.setAlpha(0.5f);
        }

        builder.setPositiveButton(R.string.submit, null);

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {

                Button button = ((AlertDialog) alertDialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {

                        List<String> gradIds = new ArrayList<>();

                        if (beans != null && beans.size() > 0) {
                            for (GradeListResponse.ResultBean.GradeBean gradeBean : beans) {
                                gradIds.add(gradeBean.getGradeId());
                            }
                        }

                        if (etGradeID.getText().toString().isEmpty()) {
                            etGradeID.setError(getString(R.string.field_required));
                            return;
                        }

                        if (etGradeName.getText().toString().isEmpty()) {
                            etGradeName.setError(getString(R.string.field_required));
                            return;
                        }

                        if (gradIds.size() > 0) {
                            if (type.equals(App_Constants.ADD_GRADE) && gradIds.contains(etGradeID.getText().toString())) {
                                etGradeID.setError(getString(R.string.enter_diffrent_grade_id));
                                return;
                            }
                        }

                        alertDialog.dismiss();
                        gradeId = etGradeID.getText().toString();
                        gradeName = etGradeName.getText().toString();

                        if (type.equals(App_Constants.ADD_GRADE))
                            addGrade();
                        else
                            updateGrade();

                    }
                });
            }
        });
        alertDialog.show();
    }

    public void showAddClassDialog(final String type, final GradeListResponse.ResultBean.GradeBean gradeBean
            , final GradeListResponse.ResultBean.GradeBean.ClassBean classBean, final List<GradeListResponse.ResultBean.GradeBean.ClassBean> beans) {

        final View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_add_class, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if (type.equals(App_Constants.ADD_CLASS))
            builder.setTitle(R.string.add_class);
        else
            builder.setTitle(R.string.update_class);
        builder.setView(view);
        builder.setCancelable(true);

        final EditText etClassID = (EditText) view.findViewById(R.id.et_class_id);
        final EditText etClassName = (EditText) view.findViewById(R.id.et_class_name);

        if (!type.equals(App_Constants.ADD_CLASS)) {

            etClassID.setText(classBean.getClassId());
            etClassID.setSelection(classBean.getClassId().length());
            etClassName.setText(classBean.getClassName());
            etClassName.setSelection(classBean.getClassName().length());

            etClassID.setEnabled(false);
            etClassID.setAlpha(0.5f);
        }

        builder.setPositiveButton(R.string.submit, null);

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {

                Button button = ((AlertDialog) alertDialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {

                        List<String> gradIds = new ArrayList<>();

                        for (GradeListResponse.ResultBean.GradeBean.ClassBean gradeBean : beans) {
                            gradIds.add(gradeBean.getClassId());
                        }

                        if (etClassID.getText().toString().isEmpty()) {
                            etClassID.setError(getString(R.string.field_required));
                            return;
                        }

                        if (etClassName.getText().toString().isEmpty()) {
                            etClassName.setError(getString(R.string.field_required));
                            return;
                        }

                        if (type.equals(App_Constants.ADD_CLASS) && gradIds.contains(etClassID.getText().toString())) {
                            etClassID.setError(getString(R.string.enter_diffrent_class_id));
                            return;
                        }

                        alertDialog.dismiss();
                        classId = etClassID.getText().toString();
                        className = etClassName.getText().toString();
                        gradeEntity = gradeBean;
                        classEntity = classBean;

                        if (type.equals(App_Constants.ADD_CLASS))
                            addClass();
                        else
                            updateClass();

                    }
                });
            }
        });
        alertDialog.show();
    }

    private void addGrade() {

        AddGrade params = new AddGrade();
        params.setGradeId(gradeId);
        params.setGradeName(gradeName);
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
        params.setSchoolYear(AdminApp.getInstance().getAdmin().getSchoolYear());

        Utility.showProgress(getActivity(), getString(R.string.processing));

        addGradeCall = AdminApp.getInstance().getApi().addGrade(params);
        addGradeCall.enqueue(new Callback<AddGradeResponse>() {
            @Override
            public void onResponse(Call<AddGradeResponse> call, Response<AddGradeResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {

                        if (response.body().isStatus()) {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            if (adapter != null) {
                                exandedPosition = adapter.getExpandedItemPosition();
                            }
                            getGradeList();
                        } else {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_ADD_GRADE_LIST);
                        Toast.makeText(getActivity(), error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddGradeResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(getActivity(), App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void updateGrade() {

        AddGrade params = new AddGrade();
        params.setGradeId(gradeId);
        params.setGradeName(gradeName);
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
        params.setSchoolYear(AdminApp.getInstance().getAdmin().getSchoolYear());

        Utility.showProgress(getActivity(), getString(R.string.processing));

        addGradeCall = AdminApp.getInstance().getApi().addGrade(params);
        addGradeCall.enqueue(new Callback<AddGradeResponse>() {
            @Override
            public void onResponse(Call<AddGradeResponse> call, Response<AddGradeResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {

                        if (response.body().isStatus()) {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            if (adapter != null) {
                                exandedPosition = adapter.getExpandedItemPosition();
                            }
                            getGradeList();
                        } else {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_UPDATE_GRADE_LIST);
                        Toast.makeText(getActivity(), error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddGradeResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(getActivity(), App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    public void addClass() {

        AddGrade params = new AddGrade();
        params.setGradeName(gradeEntity.getGradeName());
        params.setGradeId(gradeEntity.getGradeId());
        params.setClassName(className);
        params.setClassId(classId);
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
        params.setSchoolYear(AdminApp.getInstance().getAdmin().getSchoolYear());

        Utility.showProgress(getActivity(), getString(R.string.processing));

        addGradeCall = AdminApp.getInstance().getApi().addGrade(params);
        addGradeCall.enqueue(new Callback<AddGradeResponse>() {
            @Override
            public void onResponse(Call<AddGradeResponse> call, Response<AddGradeResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {

                        if (response.body().isStatus()) {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            if (adapter != null) {
                                exandedPosition = adapter.getExpandedItemPosition();
                            }
                            getGradeList();
                        } else {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_ADD_CLASS_LIST);
                        Toast.makeText(getActivity(), error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddGradeResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(getActivity(), App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void updateClass() {

        AddGrade params = new AddGrade();
        params.setGradeName(gradeEntity.getGradeName());
        params.setGradeId(gradeEntity.getGradeId());
        params.setClassName(className);
        params.setClassId(classId);
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
        params.setSchoolYear(AdminApp.getInstance().getAdmin().getSchoolYear());

        Utility.showProgress(getActivity(), getString(R.string.processing));

        addGradeCall = AdminApp.getInstance().getApi().addGrade(params);
        addGradeCall.enqueue(new Callback<AddGradeResponse>() {
            @Override
            public void onResponse(Call<AddGradeResponse> call, Response<AddGradeResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {

                        if (response.body().isStatus()) {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            if (adapter != null) {
                                exandedPosition = adapter.getExpandedItemPosition();
                            }
                            getGradeList();
                        } else {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_UPDATE_CLASS_LIST);
                        Toast.makeText(getActivity(), error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddGradeResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(getActivity(), App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void deleteClassDialog(final GradeListResponse.ResultBean.GradeBean gradeBean, final GradeListResponse.ResultBean.GradeBean.ClassBean bean) {

        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle(getString(R.string.alert));
        alertDialog.setMessage(getString(R.string.delete_class_msg));

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                alertDialog.dismiss();
                deletGradeBean = gradeBean;
                deleteClassBean = bean;
                deleteClass();
            }
        });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                alertDialog.dismiss();
            }
        });

        alertDialog.show();

    }


    public void deleteClass() {

        DeleteGrade params = new DeleteGrade();
        params.setGradeId(deletGradeBean.getGradeId());
        params.setClassId(deleteClassBean.getClassId());
        params.setRemoveClass(true);
        params.setRemoveGrade(false);
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
        params.setSchoolYear(AdminApp.getInstance().getAdmin().getSchoolYear());

        Utility.showProgress(getActivity(), getString(R.string.processing));

        addGradeCall = AdminApp.getInstance().getApi().deleteGradeORClass(params);
        addGradeCall.enqueue(new Callback<AddGradeResponse>() {
            @Override
            public void onResponse(Call<AddGradeResponse> call, Response<AddGradeResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {

                        if (response.body().isStatus()) {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            if (adapter != null) {
                                exandedPosition = adapter.getExpandedItemPosition();
                            }
                            getGradeList();
                        } else {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_DELETE_CLASS_LIST);
                        Toast.makeText(getActivity(), error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddGradeResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(getActivity(), App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void deleteGradeDialog(final GradeListResponse.ResultBean.GradeBean gradeBean) {

        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle(getString(R.string.alert));
        alertDialog.setMessage(getString(R.string.delete_grade_msg));

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                alertDialog.dismiss();
                deletGradeBean = gradeBean;
                deleteGrade();
            }
        });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                alertDialog.dismiss();

            }
        });

        alertDialog.show();

    }

    public void deleteGrade() {

        DeleteGrade params = new DeleteGrade();
        params.setGradeId(deletGradeBean.getGradeId());
        params.setRemoveClass(true);
        params.setRemoveGrade(true);
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
        params.setSchoolYear(AdminApp.getInstance().getAdmin().getSchoolYear());

        Utility.showProgress(getActivity(), getString(R.string.processing));

        addGradeCall = AdminApp.getInstance().getApi().deleteGradeORClass(params);
        addGradeCall.enqueue(new Callback<AddGradeResponse>() {
            @Override
            public void onResponse(Call<AddGradeResponse> call, Response<AddGradeResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {

                        if (response.body().isStatus()) {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            getGradeList();
                        } else {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_DELETE_GRADE_LIST);
                        Toast.makeText(getActivity(), error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddGradeResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(getActivity(), App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void startAssignActivity(GradeListResponse.ResultBean.GradeBean gradeBean, GradeListResponse.ResultBean.GradeBean.ClassBean classBean) {

        startActivity(new Intent(getActivity(), AssignClassNGradeActivity.class)
                .putExtra(App_Constants.GRADE_ID, gradeBean.getGradeId())
                .putExtra(App_Constants.CLASS_ID, classBean.getClassId()));
        if (getActivity() != null)
            getActivity().overridePendingTransition(R.anim.enter, R.anim.leave);
    }
}
