/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.grade.assign.teacher.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.grade.assign.teacher.TeachersFragment;
import ca.dataready.SmartSchoolAdmin.server.GetAssignedTeacherList;

public class AssignedTeacherAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private TeachersFragment fragment;
    private List<GetAssignedTeacherList.ResultBean> beans;

    public AssignedTeacherAdapter(Context context, TeachersFragment fragment) {
        this.context = context;
        this.fragment = fragment;
        beans = new ArrayList<>();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_assigned_teachers, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {

        final GetAssignedTeacherList.ResultBean bean = beans.get(position);

        if (holder instanceof ItemViewHolder) {

            ((ItemViewHolder) holder).txtTeacherName.setText(bean.getTeacherName());
            ((ItemViewHolder) holder).txtEmail.setText(bean.getTeacherEmailId());
            ((ItemViewHolder) holder).txtSubject.setText(bean.getSubjectName() + " (" + bean.getSubjectStartTime() + " - " + bean.getSubjectEndTime() + ")");

            ((ItemViewHolder) holder).imgMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    fragment.showPopUp(((ItemViewHolder) holder).imgMore, holder.getAdapterPosition(), bean);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }

    public void addItems(List<GetAssignedTeacherList.ResultBean> results) {

        beans.addAll(results);
        notifyDataSetChanged();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.txt_teacher_name)
        TextView txtTeacherName;
        @BindView(R.id.img_more)
        ImageView imgMore;
        @BindView(R.id.txt_email)
        TextView txtEmail;
        @BindView(R.id.txt_subject)
        TextView txtSubject;
        View mView;
        // R.layout.raw_assigned_teachers

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mView = itemView;
        }
    }
}
