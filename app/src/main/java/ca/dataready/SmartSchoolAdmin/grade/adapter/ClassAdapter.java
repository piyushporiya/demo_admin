/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.grade.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.grade.GradeFragment;
import ca.dataready.SmartSchoolAdmin.server.GradeListResponse;

public class ClassAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private GradeFragment fragment;
    private GradeListResponse.ResultBean.GradeBean gradeBean;
    private List<GradeListResponse.ResultBean.GradeBean.ClassBean> beans;
    private static final int VIEW_ITEM = 0;
    private static final int VIEW_ADD_MORE = 1;

    public ClassAdapter(Context context, GradeFragment fragment, GradeListResponse.ResultBean.GradeBean bean) {
        this.context = context;
        this.fragment = fragment;
        gradeBean = bean;
        beans = new ArrayList<>();
    }

    @Override
    public int getItemViewType(int position) {
        if (beans.get(position).isAddMore())
            return VIEW_ADD_MORE;

        return VIEW_ITEM;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == VIEW_ITEM) {
            return new ItemViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_class, parent, false));
        } else if (viewType == VIEW_ADD_MORE) {
            return new AddMoreViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_add_more, parent, false));
        } else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        final GradeListResponse.ResultBean.GradeBean.ClassBean bean = beans.get(position);

        if (holder instanceof ItemViewHolder) {

            ((ItemViewHolder) holder).txtClass.setText(bean.getClassName());

            ((ItemViewHolder) holder).imgEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    fragment.showAddClassDialog(App_Constants.UPDATE_CLASS, gradeBean, bean,beans);
                }
            });

            ((ItemViewHolder) holder).imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    fragment.deleteClassDialog(gradeBean,bean);
                }
            });

            ((ItemViewHolder) holder).mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    fragment.startAssignActivity(gradeBean,bean);
                }
            });

        } else if (holder instanceof AddMoreViewHolder) {

            ((AddMoreViewHolder) holder).cardAddMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    fragment.showAddClassDialog(App_Constants.ADD_CLASS, gradeBean, null, beans);

                }
            });

        }

    }


    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }

    public void addItems(List<GradeListResponse.ResultBean.GradeBean.ClassBean> data) {

        beans.addAll(data);
        notifyDataSetChanged();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.txt_class)
        TextView txtClass;
        @BindView(R.id.img_edit)
        ImageView imgEdit;
        @BindView(R.id.img_delete)
        ImageView imgDelete;
        @BindView(R.id.card_grade)
        CardView cardGrade;
        View mView;
        //R.layout.raw_class

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mView = itemView;
        }
    }

    class AddMoreViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_class)
        TextView txtClass;
        @BindView(R.id.card_add_more)
        CardView cardAddMore;
        // R.layout.raw_add_more

        public AddMoreViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
