/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.grade.assign.student;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ca.dataready.SmartSchoolAdmin.Listeners.OnReloadListener;
import ca.dataready.SmartSchoolAdmin.Listeners.OnStudentSelectedListener;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.LastItemDecoration;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.grade.assign.AssignClassNGradeActivity;
import ca.dataready.SmartSchoolAdmin.grade.assign.student.adapter.AssignedStudentAdapter;
import ca.dataready.SmartSchoolAdmin.grade.assign.student.adapter.UnassignedStudentsAdapter;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.AssignStudentParams;
import ca.dataready.SmartSchoolAdmin.server.AssignStudentResponse;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.GetStudentsList;
import ca.dataready.SmartSchoolAdmin.userinfo.student.IStudentFragment;
import ca.dataready.SmartSchoolAdmin.userinfo.student.add.AddStudentActivity;
import ca.dataready.SmartSchoolAdmin.userinfo.student.admin_process.StudentAdminProcessActivity;
import ca.dataready.SmartSchoolAdmin.userinfo.student.attendance_history.AttendanceHistoryActivity;
import ca.dataready.SmartSchoolAdmin.userinfo.student.transportation_history.StudentTransportationActivity;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.StudentListModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


public class StudentsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, OnReloadListener, OnStudentSelectedListener {

    @BindView(R.id.recyclerView)
    SuperRecyclerView recyclerView;
    @BindView(R.id.fab_create)
    FloatingActionButton fabCreate;
    Unbinder unbinder;
    private AssignedStudentAdapter adapter;
    private GridLayoutManager gridLayoutManager;
    private String gradeId, classId;
    private List<StudentListModel.ResultBean> results;
    private Call<StudentListModel> appApi;
    private AlertDialog alertDialog;
    private UnassignedStudentsAdapter unassignedStudentsAdapter;
    private Call<GetStudentsList> pendingStudentsApi;
    private Call<AssignStudentResponse> assignApi;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_students, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((AssignClassNGradeActivity) getActivity()).setOnReloadListener(this);
        Init();
    }

    private void Init() {

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            gradeId = bundle.getString(App_Constants.GRADE_ID);
            classId = bundle.getString(App_Constants.CLASS_ID);
        }

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            gridLayoutManager = new GridLayoutManager(getContext(), 3);
        } else {
            gridLayoutManager = new GridLayoutManager(getContext(), 2);
        }

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.addItemDecoration(new LastItemDecoration());
        recyclerView.setRefreshListener(StudentsFragment.this);

        getStudentList();
    }

    private void getStudentList() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();

        appApi = AdminApp.getInstance().getApi().getStudentList(entity.getSchoolId(), gradeId, classId, entity.getSchoolYear());
        appApi.enqueue(new Callback<StudentListModel>() {
            @Override
            public void onResponse(Call<StudentListModel> call, Response<StudentListModel> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            results = response.body().getResult();
                            if (results != null && results.size() > 0) {

                                setAdapter();

                            } else {
                                adapter = new AssignedStudentAdapter(getActivity(),StudentsFragment.this);
                                recyclerView.setAdapter(adapter);
                                TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                                emptyView.setText(getString(R.string.no_data));
                            }

                        } else {
                            adapter = new AssignedStudentAdapter(getActivity(), StudentsFragment.this);
                            recyclerView.setAdapter(adapter);
                            TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                            emptyView.setText(getString(R.string.no_data));
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_STUDENT_LIST);
                        adapter = new AssignedStudentAdapter(getActivity(), StudentsFragment.this);
                        recyclerView.setAdapter(adapter);
                        TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                        emptyView.setText(error.message());

                    }
                } catch (Exception e) {

                    adapter = new AssignedStudentAdapter(getActivity(), StudentsFragment.this);
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(getString(R.string.somethingwrong));
                }

            }

            @Override
            public void onFailure(Call<StudentListModel> call, Throwable t) {
                if (!call.isCanceled()) {
                    adapter = new AssignedStudentAdapter(getActivity(), StudentsFragment.this);
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(App_Constants.NO_INTERNET);
                }
            }
        });
    }

    private void setAdapter() {

        adapter = new AssignedStudentAdapter(getActivity(), StudentsFragment.this);
        recyclerView.setAdapter(adapter);
        adapter.addItems(results);
    }

    public void showPopUp(ImageView imgMore, int position, StudentListModel.ResultBean bean) {

        PopupMenu popup = new PopupMenu(imgMore.getContext(), imgMore);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.edit_assigned_student_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new CardMenuItemClickListener(position, bean));
        popup.show();
    }

    private class CardMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        private int position;
        private StudentListModel.ResultBean bean;

        CardMenuItemClickListener(int positon, StudentListModel.ResultBean bean) {
            this.position = positon;
            this.bean = bean;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {

                case R.id.item_edit_student_data:

                    startActivityForResult(new Intent(getActivity(), StudentAdminProcessActivity.class)
                            .putExtra(App_Constants.OBJECT, bean)
                            .putExtra(App_Constants.TYPE, App_Constants.CHANGE_ASSIGNMENT), App_Constants.UPDATE_LISTING);
                    if(getActivity()!=null)
                        getActivity().overridePendingTransition(R.anim.enter, R.anim.leave);

                    break;

                default:

                    break;
            }
            return false;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == APIError.UPDATE_TOKEN && resultCode == RESULT_OK) {

            if (data != null) {
                String apiName = data.getStringExtra("api");
                onReload(apiName);
            }

        } else if (requestCode == App_Constants.UPDATE_LISTING && resultCode == RESULT_OK) {

            getStudentList();

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.fab_create})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.fab_create:
                getStudentListDialog();
                break;
        }
    }

    private void getStudentListDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        getActivity().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        builder.setCancelable(false);
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.raw_hw_student_list_dialog, null);
        builder.setView(view);

        final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        final ViewAnimator viewAnimator = (ViewAnimator) view.findViewById(R.id.viewAnimator);
        final TextView txtNoData = (TextView) view.findViewById(R.id.txt_no_data);

        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 5));

        builder.setPositiveButton(getString(R.string.done), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

                if (unassignedStudentsAdapter != null && unassignedStudentsAdapter.getSelectedStudent().size() >= 0) {
                    assignStudent();
                }else {
                    Toast.makeText(getActivity(), getString(R.string.please_select_student), Toast.LENGTH_SHORT).show();
                }


            }
        });

        builder.setNegativeButton(getString(R.string.close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {


                CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();
                pendingStudentsApi = AdminApp.getInstance().getApi().getPendingStudents(entity.getSchoolId());
                pendingStudentsApi.enqueue(new Callback<GetStudentsList>() {
                    @Override
                    public void onResponse(Call<GetStudentsList> call, Response<GetStudentsList> response) {
                        try {
                            if (response.isSuccessful()) {
                                if (response.body().isStatus()) {

                                    unassignedStudentsAdapter = new UnassignedStudentsAdapter(getActivity());
                                    recyclerView.setAdapter(unassignedStudentsAdapter);
                                    unassignedStudentsAdapter.setSingleChoiceMode(false);
                                    unassignedStudentsAdapter.setOnStudentSelectedListener(StudentsFragment.this);

                                    List<GetStudentsList.ResultBean> studentListBeans = response.body().getResult();

                                    if (studentListBeans != null && studentListBeans.size() > 0) {

                                        unassignedStudentsAdapter.addItem(studentListBeans);
                                        viewAnimator.setDisplayedChild(1); // recyclerview visible

                                    } else {

                                        viewAnimator.setDisplayedChild(2);  // empty view visible
                                        txtNoData.setText(getString(R.string.no_data));
                                    }

                                } else {

                                    viewAnimator.setDisplayedChild(2); // empty view visible
                                    txtNoData.setText(response.body().getMessage());
                                }

                            } else {

                                APIError error = APIError.parseError(response, getActivity(), App_Constants.API_PENDING_STUDENTS);
                                viewAnimator.setDisplayedChild(2); // empty view visible
                                txtNoData.setText(error.message());

                                if (alertDialog != null && alertDialog.isShowing())
                                    alertDialog.dismiss();
                            }
                        } catch (Exception e) {

                            viewAnimator.setDisplayedChild(2); // empty view visible
                            txtNoData.setText(getString(R.string.somethingwrong));
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<GetStudentsList> call, Throwable t) {
                        if (!call.isCanceled()) {
                            viewAnimator.setDisplayedChild(2); // empty view visible
                            txtNoData.setText(App_Constants.NO_INTERNET);
                            System.out.println(t.getMessage());
                        }
                    }
                });

            }
        });


        alertDialog.show();


        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.90);

        if (alertDialog != null && alertDialog.getWindow() != null) {
            alertDialog.getWindow().setLayout(screenWidth, ViewGroup.LayoutParams.WRAP_CONTENT); //set below the setContentview
        }

    }

    private void assignStudent() {

        List<AssignStudentParams> paramsList = new ArrayList<>();

        if (unassignedStudentsAdapter != null && unassignedStudentsAdapter.getSelectedStudent().size() > 0) {

            for (GetStudentsList.ResultBean bean : unassignedStudentsAdapter.getSelectedStudent()) {

                AssignStudentParams params = new AssignStudentParams();
                params.setClassId(classId);
                params.setDob(bean.getDob());
                params.setEmailId(bean.getEmailId());
                params.setFirstName(bean.getFirstName());
                params.setGradeId(gradeId);
                params.setLastName(bean.getLastName());
                params.setPhoneNo(bean.getPhoneNo());
                params.setProfilePic(bean.getProfilePic());
                params.setSchoolId(bean.getSchoolId());
                params.setSchoolYear(AdminApp.getInstance().getAdmin().getSchoolYear());
                params.setStudentId(bean.getStudentId());
                paramsList.add(params);

            }
        }

        Utility.showProgress(getActivity(),getString(R.string.processing));

        assignApi = AdminApp.getInstance().getApi().assignStudents(paramsList);
        assignApi.enqueue(new Callback<AssignStudentResponse>() {
            @Override
            public void onResponse(Call<AssignStudentResponse> call, Response<AssignStudentResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            getStudentList();
                        } else {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_STUDENT_LIST);
                        Toast.makeText(getActivity(), error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {

                    Toast.makeText(getActivity(), getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AssignStudentResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(getActivity(), App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public void onRefresh() {

        if (isVisible())
            getStudentList();
    }

    @Override
    public void onReload(String apiName) {

        if (apiName != null && isVisible()) {
            if (apiName.equals(App_Constants.API_STUDENT_LIST)) {
                getStudentList();
            } else if (apiName.equals(App_Constants.API_PENDING_STUDENTS)) {
                getStudentListDialog();
            }else if (apiName.equals(App_Constants.API_ASSIGN_STUDENTS)) {
                assignStudent();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if (appApi != null)
            appApi.cancel();

        if (assignApi != null)
            assignApi.cancel();

        if (pendingStudentsApi != null)
            pendingStudentsApi.cancel();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            gridLayoutManager = new GridLayoutManager(getContext(), 3);
        } else {
            gridLayoutManager = new GridLayoutManager(getContext(), 2);
        }
        recyclerView.setLayoutManager(gridLayoutManager);
    }


    @Override
    public void onStudentSelected(boolean makeEnabled, List<String> studentIdList) {

    }

}
