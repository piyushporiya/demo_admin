/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.grade.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.grade.GradeFragment;
import ca.dataready.SmartSchoolAdmin.server.GradeListResponse;
import ca.dataready.SmartSchoolAdmin.subject.SubjectFragment;

public class GradeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private int gridCount;
    private GradeFragment fragment;
    private List<GradeListResponse.ResultBean.GradeBean> beans;
    private ClassAdapter adapter;

    public GradeAdapter(Context context, int gridCount, GradeFragment fragment) {
        this.context = context;
        this.gridCount = gridCount;
        this.fragment = fragment;
        beans = new ArrayList<>();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new ItemViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_grade, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {

        final GradeListResponse.ResultBean.GradeBean bean = beans.get(position);

        if (holder instanceof ItemViewHolder) {

            ((ItemViewHolder) holder).txtGrade.setText(bean.getGradeName());

            ((ItemViewHolder) holder).recyclerView.setLayoutManager(new GridLayoutManager(context, gridCount));
            adapter = new ClassAdapter(context, fragment, bean);
            ((ItemViewHolder) holder).recyclerView.setAdapter(adapter);
            adapter.addItems(bean.getClassX());

            if (bean.isExpanded()) {
                ((ItemViewHolder) holder).txtGrade.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(context
                        , R.drawable.ic_arrow_down), null);
                ((ItemViewHolder) holder).recyclerView.setVisibility(View.VISIBLE);
            } else {
                ((ItemViewHolder) holder).txtGrade.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(context
                        , R.drawable.ic_arrow_up), null);
                ((ItemViewHolder) holder).recyclerView.setVisibility(View.GONE);
            }

            ((ItemViewHolder) holder).cardGrade.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    bean.setExpanded(!bean.isExpanded());

                    if (bean.isExpanded()) {
                        ((ItemViewHolder) holder).txtGrade.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(context
                                , R.drawable.ic_arrow_down), null);
                        ((ItemViewHolder) holder).recyclerView.setVisibility(View.VISIBLE);
                    } else {
                        ((ItemViewHolder) holder).txtGrade.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(context
                                , R.drawable.ic_arrow_up), null);
                        ((ItemViewHolder) holder).recyclerView.setVisibility(View.VISIBLE);
                    }


                    makeOtherCollapsed(holder.getAdapterPosition());
                }
            });

            ((ItemViewHolder) holder).imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    fragment.deleteGradeDialog(bean);
                }
            });

            ((ItemViewHolder) holder).imgEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    fragment.showAddGradeDialog(App_Constants.UPDATE_GRADE, bean,beans);
                }
            });

            ((ItemViewHolder) holder).imgSubject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ((HomeActivity)context).LoadFragmentWithPayLoad(new SubjectFragment(),bean.getGradeId());
                }
            });

        }

    }

    private void makeOtherCollapsed(int adapterPosition) {

        for (int i = 0; i < beans.size(); i++) {

            if (i != adapterPosition) {
                beans.get(i).setExpanded(false);
            }
        }

        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }

    public void addItems(List<GradeListResponse.ResultBean.GradeBean> data) {

        beans.addAll(data);
        notifyDataSetChanged();
    }

    public void setGridCount(int grid_count) {

        gridCount = grid_count;
        notifyDataSetChanged();
    }

    public int getExpandedItemPosition() {

        for (int i = 0; i < beans.size(); i++) {

            if (beans.get(i).isExpanded())
                return i;
        }

        return 0;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.txt_grade)
        TextView txtGrade;
        @BindView(R.id.card_grade)
        CardView cardGrade;
        @BindView(R.id.recyclerView)
        RecyclerView recyclerView;
        @BindView(R.id.img_edit)
        ImageView imgEdit;
        @BindView(R.id.img_delete)
        ImageView imgDelete;
        @BindView(R.id.img_subject)
        ImageView imgSubject;
        //R.layout.raw_grade

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
