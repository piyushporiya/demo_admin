/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.grade.model;


import java.util.List;

public class GradeModel {


    private String groupTitle;
    private List<GradeModel.SubItem> subList;
    private boolean isExpanded = false;
    private boolean hasMoreItems = true;

    public GradeModel() {

    }

    public GradeModel(String groupTitle, List<GradeModel.SubItem> subList) {
        this.groupTitle = groupTitle;
        this.subList = subList;
    }

    public boolean hasMoreItems() {
        return hasMoreItems;
    }

    public void setHasMoreItems(boolean hasMoreItems) {
        this.hasMoreItems = hasMoreItems;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }

    public List<GradeModel.SubItem> getSubList() {
        return subList;
    }

    public void setSubList(List<GradeModel.SubItem> subList) {
        this.subList = subList;
    }

    public String getGroupTitle() {

        return groupTitle;
    }

    public void setGroupTitle(String groupTitle) {
        this.groupTitle = groupTitle;
    }

    public static class SubItem {

        private String subItemTitle;
        private boolean isAddMore;


        public SubItem() {

        }

        public SubItem(String subItemTitle, boolean isAddMore) {
            this.subItemTitle = subItemTitle;
            this.isAddMore = isAddMore;
        }

        public boolean isAddMore() {
            return isAddMore;
        }

        public void setAddMore(boolean addMore) {
            isAddMore = addMore;
        }

        public String getSubItemTitle() {
            return subItemTitle;
        }

        public void setSubItemTitle(String subItemTitle) {
            this.subItemTitle = subItemTitle;
        }

    }
}
