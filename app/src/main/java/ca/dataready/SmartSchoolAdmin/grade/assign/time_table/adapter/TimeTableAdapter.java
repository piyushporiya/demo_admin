/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.grade.assign.time_table.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.server.TimeTableModel;


/**
 * Created by social_jaydeep on 07/09/17.
 */

public class TimeTableAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<TimeTableModel> rowItems;
    private LayoutInflater mInflater;

    public TimeTableAdapter(Context context) {
        this.context = context;
        this.rowItems = new ArrayList<TimeTableModel>();
        mInflater = LayoutInflater.from(context);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewGroup vVideos = (ViewGroup) mInflater.inflate(R.layout.raw_time_table, parent, false);
        return new ItemViewHolder(vVideos);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final TimeTableModel bean = rowItems.get(position);

        if (holder instanceof ItemViewHolder) {
            ((ItemViewHolder) holder).txtTime.setText(bean.getTimePeriod());
            ((ItemViewHolder) holder).txtSlotOnMon.setText(bean.getSlotBean().getSlotOnMon());
            ((ItemViewHolder) holder).txtSlotOnTue.setText(bean.getSlotBean().getSlotOnTue());
            ((ItemViewHolder) holder).txtSlotOnWed.setText(bean.getSlotBean().getSlotOnWed());
            ((ItemViewHolder) holder).txtSlotOnThu.setText(bean.getSlotBean().getSlotOnThu());
            ((ItemViewHolder) holder).txtSlotOnFri.setText(bean.getSlotBean().getSlotOnFri());
            ((ItemViewHolder) holder).txtSlotOnSat.setText(bean.getSlotBean().getSlotOnSat());


            ((ItemViewHolder) holder).txtSlotOnMon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Toast.makeText(context, "Mon - " + bean.getTimePeriod() +" - "+bean.getSlotBean().getSlotOnMon(), Toast.LENGTH_SHORT).show();
                }
            });

            ((ItemViewHolder) holder).txtSlotOnTue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Toast.makeText(context, "Tue - " + bean.getTimePeriod(), Toast.LENGTH_SHORT).show();
                }
            });

            ((ItemViewHolder) holder).txtSlotOnWed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Toast.makeText(context, "Wed - " + bean.getTimePeriod(), Toast.LENGTH_SHORT).show();
                }
            });

            ((ItemViewHolder) holder).txtSlotOnThu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Toast.makeText(context, "Thu - " + bean.getTimePeriod(), Toast.LENGTH_SHORT).show();
                }
            });

            ((ItemViewHolder) holder).txtSlotOnFri.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Toast.makeText(context, "Fri - " + bean.getTimePeriod(), Toast.LENGTH_SHORT).show();
                }
            });

            ((ItemViewHolder) holder).txtSlotOnSat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Toast.makeText(context, "Sat - " + bean.getTimePeriod(), Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return rowItems == null ? 0 : rowItems.size();
    }


    public void addItem(List<TimeTableModel> productDetails) {

        rowItems.addAll(productDetails);
        notifyDataSetChanged();
    }

    public void clear() {
        rowItems.clear();
        notifyDataSetChanged();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_time)
        TextView txtTime;
        @BindView(R.id.txt_slot_on_mon)
        TextView txtSlotOnMon;
        @BindView(R.id.txt_slot_on_tue)
        TextView txtSlotOnTue;
        @BindView(R.id.txt_slot_on_wed)
        TextView txtSlotOnWed;
        @BindView(R.id.txt_slot_on_thu)
        TextView txtSlotOnThu;
        @BindView(R.id.txt_slot_on_fri)
        TextView txtSlotOnFri;
        @BindView(R.id.txt_slot_on_sat)
        TextView txtSlotOnSat;

        //R.layout.raw_time_table
        ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}


