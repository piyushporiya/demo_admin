/*
 * Copyright (c) 2017.                                                       
 *                                                                           
 * ***********************************************************************  
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                           
 * ________________________________________________________________________  
 *                                                                           
 *    [2010] - [2018] SripathiSolutions Corp.                                
 *    [2010] - [2018] DataReady Technology Corp.                             
 *   ________________________________________________________________________
 *                                                                           
 *    All Rights Reserved.                                                   
 *                                                                           
 *   NOTICE:  All information contained herein is, and remains               
 *   the property of DataReady Technology  and its suppliers,                
 *   if any.  The intellectual and technical concepts contained              
 *   herein are proprietary to DataReady Technology Incorporated             
 *   and its suppliers and may be covered by U.S. and Foreign Patents,       
 *   patents in process, and are protected by trade secret or copyright law. 
 *   Dissemination of this information or reproduction of this material      
 *   is strictly forbidden unless prior written permission is obtained       
 *   from DataReady Technology Incorporated.                                 
 *                                                                           
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                            
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.grade.assign.student.adapter;


import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.drawee.view.SimpleDraweeView;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.Listeners.OnStudentSelectedListener;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.server.AppApi;
import ca.dataready.SmartSchoolAdmin.server.GetStudentsList;
import ca.dataready.SmartSchoolAdmin.userinfo.student.adapter.StudentsListAdapter;


public class UnassignedStudentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<GetStudentsList.ResultBean> beans;
    private List<String> studentIdList = new ArrayList<>();
    private OnStudentSelectedListener onStudentSelectedListener;
    private boolean isSingleChoice = false;

    public UnassignedStudentsAdapter(Context context) {
        this.context = context;
        beans = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new StudentViewHolder(LayoutInflater.from(context).inflate(R.layout.row_unassigned_students, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        final GetStudentsList.ResultBean bean = beans.get(position);

        if (holder instanceof StudentViewHolder) {

            ((StudentViewHolder) holder).txtStudentName.setText(bean.getFirstName() + " " + bean.getLastName());

            Glide.with(context)
                    .load(AppApi.BASE_URL + bean.getProfilePic())
                    .into(((StudentViewHolder) holder).ivStudentImg);

            if (bean.isSelected()) {
                ((StudentViewHolder) holder).ivSelection.setImageResource(R.drawable.ic_present);
            } else {
                ((StudentViewHolder) holder).ivSelection.setImageResource(R.drawable.ic_normal);
            }


            ((StudentViewHolder) holder).ivSelection.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (isSingleChoice) {

                        bean.setSelected(!bean.isSelected());
                        if (bean.isSelected()) {
                            ((StudentViewHolder) holder).ivSelection.setImageResource(R.drawable.ic_present);
                            studentIdList.add(bean.getStudentId());
                        } else {
                            ((StudentViewHolder) holder).ivSelection.setImageResource(R.drawable.ic_normal);
                            studentIdList.remove(bean.getStudentId());
                        }

                        makeOtherDisabled(holder.getAdapterPosition());


                    } else {

                        bean.setSelected(!bean.isSelected());

                        if (bean.isSelected()) {
                            ((StudentViewHolder) holder).ivSelection.setImageResource(R.drawable.ic_present);
                            studentIdList.add(bean.getStudentId());
                            setSubmitEnable();
                        } else {
                            ((StudentViewHolder) holder).ivSelection.setImageResource(R.drawable.ic_normal);
                            studentIdList.remove(bean.getStudentId());
                            setSubmitEnable();
                        }
                    }


                }
            });

        }
    }

    private void makeOtherDisabled(int pos) {

        for (int i = 0; i < beans.size(); i++) {

            if (i != pos) {
                beans.get(i).setSelected(false);
                studentIdList.remove(beans.get(i).getStudentId());
            }
        }

        setSubmitEnable();
        notifyDataSetChanged();
    }

    public void setSingleChoiceMode(boolean value) {

        isSingleChoice = value;
    }

    public void addItem(List<GetStudentsList.ResultBean> results) {

        Collections.sort(results, new Comparator<GetStudentsList.ResultBean>() {
            public int compare(GetStudentsList.ResultBean obj1, GetStudentsList.ResultBean obj2) {
                return obj1.getFirstName().compareToIgnoreCase(obj2.getFirstName()); // To compare string values
            }
        });

        beans.addAll(results);
        notifyDataSetChanged();

    }

    public void setOnStudentSelectedListener(OnStudentSelectedListener onStudentSelectedListener) {
        this.onStudentSelectedListener = onStudentSelectedListener;
    }

    private void setSubmitEnable() {

        if (studentIdList != null && studentIdList.size() > 0) {
            if (onStudentSelectedListener != null)
                onStudentSelectedListener.onStudentSelected(true, studentIdList);
        } else {
            if (onStudentSelectedListener != null)
                onStudentSelectedListener.onStudentSelected(false, studentIdList);
        }
    }

    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }

    public List<GetStudentsList.ResultBean> getSelectedStudent() {

        List<GetStudentsList.ResultBean> selectedStudentsList = new ArrayList<>();
        for (GetStudentsList.ResultBean bean : beans) {
            if (bean.isSelected()) {
                selectedStudentsList.add(bean);
            }
        }
        return selectedStudentsList;
    }

    public List<String> getSelectedStudentsIds() {

        List<String> selectedStudentsList = new ArrayList<>();
        for (GetStudentsList.ResultBean bean : beans) {
            if (bean.isSelected())
                selectedStudentsList.add(bean.getStudentId());
        }

        return selectedStudentsList;
    }


    class StudentViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.my_image_view)
        CircularImageView ivStudentImg;
        @BindView(R.id.iv_Selection)
        ImageView ivSelection;
        @BindView(R.id.txt_StudentName)
        TextView txtStudentName;
        @BindView(R.id.main_Linear)
        LinearLayout mainLinear;


        //R.layout.row_students_for_attendance

        public StudentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}

