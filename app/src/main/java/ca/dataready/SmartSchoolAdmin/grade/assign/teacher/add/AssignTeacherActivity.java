/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.grade.assign.teacher.add;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.AssignUpdateTeacherParams;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.CommonResponse;
import ca.dataready.SmartSchoolAdmin.server.GetAllTeacherResponse;
import ca.dataready.SmartSchoolAdmin.server.GetAssignedTeacherList;
import ca.dataready.SmartSchoolAdmin.server.SubjectListResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AssignTeacherActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.txtAssignTeacher)
    TextView txtAssignTeacher;
    @BindView(R.id.card_assign_teacher)
    CardView cardAssignTeacher;
    @BindView(R.id.et_teacher_name)
    EditText etTeacherName;
    @BindView(R.id.et_subject_name)
    EditText etSubjectName;
    @BindView(R.id.et_week_day)
    EditText etWeekDay;
    @BindView(R.id.et_time)
    EditText etTime;
    @BindView(R.id.layout_assign_teacher)
    CardView layoutAssignTeacher;
    @BindView(R.id.viewAnimator)
    ViewAnimator viewAnimator;
    private String gradeId, classId;
    private Call<CommonResponse> assignCall;
    private String subjectType;
    private GetAssignedTeacherList.ResultBean bean;
    private String type;
    private ListPopupWindow listPopupWindow;
    private List<String> teacherIdList = new ArrayList<>();
    private List<String> teacherNameList = new ArrayList<>();
    private List<String> subjectNameList = new ArrayList<>();
    private List<String> weekDayList = new ArrayList<>();
    private List<String> timeSlotList = new ArrayList<>();
    private ArrayList<GetAllTeacherResponse.ResultBean> teacherBeans;
    private Call<GetAllTeacherResponse> appApi;
    private Call<SubjectListResponse> subjectApi;
    private List<SubjectListResponse.ResultBean> subjectBeans;
    private GetAllTeacherResponse.ResultBean teacherSingleBean;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_teacher);
        ButterKnife.bind(this);
        setHeaderInfo();
        Init();
    }

    private void setHeaderInfo() {

        setSupportActionBar(toolbar);

        if (AdminApp.getInstance().getAdmin() != null)
            getSupportActionBar().setTitle(AdminApp.getInstance().getAdmin().getSchoolName() + " " + AdminApp.getInstance().getAdmin().getSchoolYear());
        else
            getSupportActionBar().setTitle(getString(R.string.app_name));

        getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + getString(R.string.assing_teacher) + "</small>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void Init() {

        if (getIntent().getExtras() != null) {

            type = getIntent().getStringExtra(App_Constants.TYPE);

            if (type != null && type.equals(App_Constants.UPDATE)) {
                bean = getIntent().getParcelableExtra(App_Constants.OBJECT);

                if (bean != null) {
                    btnSubmit.setText(getString(R.string.update));
                    etTeacherName.setEnabled(false);
                    etTeacherName.setAlpha(0.5f);
                    gradeId = bean.getGradeId();
                    etTeacherName.setText(bean.getTeacherName());
                    etTime.setText(bean.getSubjectStartTime() + " - " + bean.getSubjectEndTime());
                    etSubjectName.setText(bean.getSubjectName());
                    etWeekDay.setText(AdminApp.getInstance().weekDayInLatters(bean.getWeekDay()));
                }

            } else {

                gradeId = getIntent().getStringExtra(App_Constants.GRADE_ID);
                classId = getIntent().getStringExtra(App_Constants.CLASS_ID);
            }
        }

        getAllTeacher();
    }

    private void getAllTeacher() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();

        appApi = AdminApp.getInstance().getApi().getAllTeachers(entity.getSchoolId());
        appApi.enqueue(new Callback<GetAllTeacherResponse>() {
            @Override
            public void onResponse(Call<GetAllTeacherResponse> call, Response<GetAllTeacherResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            teacherBeans = response.body().getResult();
                        }
                        getAllSubject();
                    } else {
                        APIError error = APIError.parseError(response, AssignTeacherActivity.this, App_Constants.API_GET_ALL_TEACHERS);
                        Toast.makeText(AssignTeacherActivity.this, "" + error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    getAllSubject();
                }


            }

            @Override
            public void onFailure(Call<GetAllTeacherResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    Toast.makeText(AssignTeacherActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                    getAllSubject();
                }
            }
        });

    }

    private void getAllSubject() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();

        subjectApi = AdminApp.getInstance().getApi().getSubjectList(entity.getSchoolId(), gradeId);
        subjectApi.enqueue(new Callback<SubjectListResponse>() {
            @Override
            public void onResponse(Call<SubjectListResponse> call, Response<SubjectListResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            subjectBeans = response.body().getResult();
                        }
                    } else {
                        APIError error = APIError.parseError(response, AssignTeacherActivity.this, App_Constants.API_GET_SUBJECT_LIST);
                        Toast.makeText(AssignTeacherActivity.this, "" + error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                viewAnimator.setDisplayedChild(1);
            }

            @Override
            public void onFailure(Call<SubjectListResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    Toast.makeText(AssignTeacherActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                    viewAnimator.setDisplayedChild(1);
                }
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);

    }

    @OnClick({R.id.btn_submit, R.id.card_assign_teacher, R.id.et_teacher_name, R.id.et_subject_name, R.id.et_week_day, R.id.et_time})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_submit:
                validateData();
                break;
            case R.id.card_assign_teacher:
                layoutAssignTeacher.setVisibility(layoutAssignTeacher.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                break;
            case R.id.et_teacher_name:
                show_drop_down_to_select_option(etTeacherName, App_Constants.TEACHER_NAME);
                break;
            case R.id.et_subject_name:
                show_drop_down_to_select_option(etSubjectName, App_Constants.SUBJECT_NAME);
                break;
            case R.id.et_week_day:
                show_drop_down_to_select_option(etWeekDay, App_Constants.WEEK_DAY);
                break;
            case R.id.et_time:
                show_drop_down_to_select_option(etTime, App_Constants.TIME_SLOT);
                break;
        }
    }

    private void show_drop_down_to_select_option(EditText layout, final String which) {

        listPopupWindow = new ListPopupWindow(AssignTeacherActivity.this);
        if (which.equals(App_Constants.TEACHER_NAME)) {

            if (teacherBeans != null && teacherBeans.size() > 0) {
                teacherNameList.clear();
                for (GetAllTeacherResponse.ResultBean beans : teacherBeans) {
                    if (!teacherNameList.contains(beans.getFirstName() + " " + beans.getLastName())) {
                        teacherNameList.add(beans.getFirstName() + " " + beans.getLastName());
                        teacherIdList.add(beans.getId());
                    }
                }

                listPopupWindow.setAdapter(new ArrayAdapter(AssignTeacherActivity.this, R.layout.list_dropdown_item, teacherNameList));

            } else {

                Toast.makeText(this, getString(R.string.no_teacher_avail), Toast.LENGTH_SHORT).show();
            }

        } else if (which.equals(App_Constants.SUBJECT_NAME)) {

            if (subjectBeans != null && subjectBeans.size() > 0) {

                subjectNameList.clear();
                for (SubjectListResponse.ResultBean beans : subjectBeans) {
                    if (!subjectNameList.contains(beans.getSubjectName()))
                        subjectNameList.add(beans.getSubjectName());
                }

                listPopupWindow.setAdapter(new ArrayAdapter(AssignTeacherActivity.this, R.layout.list_dropdown_item, subjectNameList));

            } else {

                Toast.makeText(this, getString(R.string.no_subject_avail), Toast.LENGTH_SHORT).show();
            }

        } else if (which.equals(App_Constants.WEEK_DAY)) {

            weekDayList.clear();
            weekDayList.add(getString(R.string.monday));
            weekDayList.add(getString(R.string.tuesday));
            weekDayList.add(getString(R.string.wednesday));
            weekDayList.add(getString(R.string.thursday));
            weekDayList.add(getString(R.string.friday));
            weekDayList.add(getString(R.string.saturday));

            listPopupWindow.setAdapter(new ArrayAdapter(AssignTeacherActivity.this, R.layout.list_dropdown_item, weekDayList));

        } else if (which.equals(App_Constants.TIME_SLOT)) {

            timeSlotList.clear();
            timeSlotList.add(getString(R.string.time_one));
            timeSlotList.add(getString(R.string.time_two));
            timeSlotList.add(getString(R.string.time_three));
            timeSlotList.add(getString(R.string.time_four));
            timeSlotList.add(getString(R.string.time_five));
            timeSlotList.add(getString(R.string.time_six));

            listPopupWindow.setAdapter(new ArrayAdapter(AssignTeacherActivity.this, R.layout.list_dropdown_item, timeSlotList));
        }

        listPopupWindow.setAnchorView(layout);
        listPopupWindow.setWidth(ListPopupWindow.WRAP_CONTENT);
        listPopupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);

        listPopupWindow.setModal(true);
        listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if (which.equals(App_Constants.TEACHER_NAME)) {

                    etTeacherName.setText(teacherNameList.get(i));

                    if (teacherBeans != null && teacherBeans.size() > 0) {

                        for (GetAllTeacherResponse.ResultBean bean : teacherBeans) {

                            if (bean.getId().equals(teacherIdList.get(i))) {

                                teacherSingleBean = bean;

                                break;
                            }
                        }
                    }

                } else if (which.equals(App_Constants.SUBJECT_NAME)) {

                    etSubjectName.setText(subjectNameList.get(i));

                } else if (which.equals(App_Constants.WEEK_DAY)) {

                    etWeekDay.setText(weekDayList.get(i));

                } else if (which.equals(App_Constants.TIME_SLOT)) {

                    etTime.setText(timeSlotList.get(i));
                }
                listPopupWindow.dismiss();

            }
        });
        listPopupWindow.show();
    }


    private void validateData() {

        if (etTeacherName.getText().toString().isEmpty())
            etTeacherName.setError(getString(R.string.field_required));
        else if (etSubjectName.getText().toString().isEmpty())
            etSubjectName.setError(getString(R.string.field_required));
        else if (etWeekDay.getText().toString().isEmpty())
            etWeekDay.setError(getString(R.string.field_required));
        else if (etTime.getText().toString().isEmpty())
            etTime.setError(getString(R.string.field_required));
        else {

            if (btnSubmit.getText().toString().equals(getString(R.string.submit)))
                assignTeacher();
            else
                updateAssignedTeacher();
        }
    }


    private void assignTeacher() {


        AssignUpdateTeacherParams params = new AssignUpdateTeacherParams();
        params.setClassId(classId);
        params.setGradeId(gradeId);
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
        params.setSchoolYear(AdminApp.getInstance().getAdmin().getSchoolYear());
        params.setSubjectStartTime(etTime.getText().toString().split("-")[0].trim());
        params.setSubjectEndTime(etTime.getText().toString().split("-")[1].trim());
        params.setSubjectName(etSubjectName.getText().toString());
        params.setTeacherEmailId(teacherSingleBean.getEmailId());
        params.setTeacherId(teacherSingleBean.getId());
        params.setTeacherMobileNo(teacherSingleBean.getPhoneNo());
        params.setTeacherName(teacherSingleBean.getFirstName() + " " + teacherSingleBean.getLastName());
        params.setWeekDay(AdminApp.getInstance().weekDayInNumbers(etWeekDay.getText().toString()));

        Utility.showProgress(AssignTeacherActivity.this, getString(R.string.processing));

        assignCall = AdminApp.getInstance().getApi().assignTeacher(params);
        assignCall.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            Toast.makeText(AssignTeacherActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            setBackIntent();
                        } else {
                            Toast.makeText(AssignTeacherActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        APIError error = APIError.parseError(response, AssignTeacherActivity.this, App_Constants.API_ASSIGN_TEACHER);
                        Toast.makeText(AssignTeacherActivity.this, error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(AssignTeacherActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(AssignTeacherActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void updateAssignedTeacher() {

        AssignUpdateTeacherParams params = new AssignUpdateTeacherParams();
        params.setId(bean.getId());
        params.setClassId(bean.getClassId());
        params.setGradeId(bean.getGradeId());
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
        params.setSchoolYear(AdminApp.getInstance().getAdmin().getSchoolYear());
        params.setSubjectStartTime(etTime.getText().toString().split("-")[0].trim());
        params.setSubjectEndTime(etTime.getText().toString().split("-")[1].trim());
        params.setSubjectName(etSubjectName.getText().toString());
        params.setTeacherEmailId(bean.getTeacherEmailId());
        params.setTeacherId(bean.getId());
        params.setTeacherMobileNo(bean.getTeacherMobileNo());
        params.setTeacherName(bean.getTeacherName());
        params.setWeekDay(AdminApp.getInstance().weekDayInNumbers(etWeekDay.getText().toString()));

        Utility.showProgress(AssignTeacherActivity.this, getString(R.string.processing));

        assignCall = AdminApp.getInstance().getApi().updateAssignedTeacher(params);
        assignCall.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            Toast.makeText(AssignTeacherActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            setBackIntent();
                        } else {
                            Toast.makeText(AssignTeacherActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        APIError error = APIError.parseError(response, AssignTeacherActivity.this, App_Constants.API_UPDATE_ASSIGNED_TEACHER);
                        Toast.makeText(AssignTeacherActivity.this, error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(AssignTeacherActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(AssignTeacherActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == APIError.UPDATE_TOKEN && resultCode == RESULT_OK) {

            if (data != null) {
                String apiName = data.getStringExtra("api");
                onRelaod(apiName);
            }

        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (assignCall != null)
            assignCall.cancel();
    }

    private void onRelaod(String apiName) {

        if (apiName != null) {
            if (apiName.equals(App_Constants.API_GET_ALL_TEACHERS)) {
                getAllTeacher();
            } else if (apiName.equals(App_Constants.API_GET_SUBJECT_LIST)) {
                getAllSubject();
            } else if (apiName.equals(App_Constants.API_ASSIGN_TEACHER)) {
                assignTeacher();
            } else if (apiName.equals(App_Constants.API_UPDATE_ASSIGNED_TEACHER)) {
                updateAssignedTeacher();
            }
        }
    }


    private void setBackIntent() {

        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        onBackPressed();
    }
}
