/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.grade.assign.time_table;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.malinskiy.superrecyclerview.SuperRecyclerView;

import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.LastItemDecoration;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.grade.assign.time_table.adapter.TimeTableAdapter;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.GetAssignedTeacherList;
import ca.dataready.SmartSchoolAdmin.server.TimeTableModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TimeTableFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.recyclerView)
    SuperRecyclerView recyclerView;
    Unbinder unbinder;
    List<TimeTableModel> list;
    private TimeTableAdapter adapter;
    private Call<GetAssignedTeacherList> appApi;
    private List<GetAssignedTeacherList.ResultBean> results;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_time_table, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getTeacherList();
    }

    private void getTeacherList() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();

        appApi = AdminApp.getInstance().getApi().getAssignedTeacherList(entity.getSchoolId(), entity.getSchoolYear(), "3", "A", "1");
        appApi.enqueue(new Callback<GetAssignedTeacherList>() {
            @Override
            public void onResponse(Call<GetAssignedTeacherList> call, Response<GetAssignedTeacherList> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            results = response.body().getResult();
                            if (results != null && results.size() > 0) {

                                setAdapter();

                            } else {

                                adapter = new TimeTableAdapter(getActivity());
                                recyclerView.setAdapter(adapter);
                                TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                                emptyView.setText(getString(R.string.no_data));
                            }

                        } else {

                            adapter = new TimeTableAdapter(getActivity());
                            recyclerView.setAdapter(adapter);
                            TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                            emptyView.setText(getString(R.string.no_data));
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_ASSIGNED_TEACHER_LIST);
                        adapter = new TimeTableAdapter(getActivity());
                        recyclerView.setAdapter(adapter);
                        TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                        emptyView.setText(error.message());

                    }
                } catch (Exception e) {

                    adapter = new TimeTableAdapter(getActivity());
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(getString(R.string.somethingwrong));
                }

            }

            @Override
            public void onFailure(Call<GetAssignedTeacherList> call, Throwable t) {

                if (!call.isCanceled()) {

                    adapter = new TimeTableAdapter(getActivity());
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(App_Constants.NO_INTERNET);

                }
            }
        });
    }

    private void setAdapter() {

        long min = 0;
        long difference;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
            Date date1 = simpleDateFormat.parse("07:00");
            Date date2 = simpleDateFormat.parse("14:00");
            list = new ArrayList<>();

            difference = (date2.getTime() - date1.getTime()) / 1000;
            long hours = difference % (24 * 3600) / 3600;
            long minute = difference % 3600 / 60;
            min = minute + (hours * 60);
            int frequecy = 60;
            int totalIterration = (int) (min / frequecy);

            Log.e("MIN", "" + min);
            Log.e("hours", "" + hours);
            Log.e("totalIterration", "" + totalIterration);

            String startTime = "07:00";

            for (int i = 0; i < totalIterration; i++) {

                DateTimeFormatter formatter = DateTimeFormat.forPattern("HH:mm");
                LocalTime time = formatter.parseLocalTime(startTime);
                time = time.plusMinutes(Integer.parseInt(String.valueOf(frequecy)));
                String newTime = formatter.print(time);

                TimeTableModel model = new TimeTableModel();
                model.setTimePeriod(startTime + " - " + newTime.toUpperCase());

                TimeTableModel.SlotBean slotModel = new TimeTableModel.SlotBean();

                for (GetAssignedTeacherList.ResultBean bean : results) {

                    if ((bean.getSubjectStartTime() + " - " + bean.getSubjectEndTime()).equalsIgnoreCase(startTime + " - " + newTime.toUpperCase())){

                        slotModel.setSlotOnMon(bean.getSubjectName());
                        break;
                    }
                }

                slotModel.setSlotOnTue("");
                slotModel.setSlotOnWed("");
                slotModel.setSlotOnThu("");
                slotModel.setSlotOnFri("");
                slotModel.setSlotOnSat("");
                model.setSlotBean(slotModel);

                list.add(model);
                startTime = newTime.toUpperCase();

            }

        } catch (Throwable e) {
            e.printStackTrace();
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new LastItemDecoration());
        recyclerView.setRefreshListener(this);

        adapter = new TimeTableAdapter(getActivity());
        recyclerView.setAdapter(adapter);
        adapter.addItem(list);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onRefresh() {
        recyclerView.setRefreshing(false);
    }
}
