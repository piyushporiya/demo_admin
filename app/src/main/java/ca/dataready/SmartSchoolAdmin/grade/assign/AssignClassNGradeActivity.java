/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.grade.assign;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.Html;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.Listeners.OnReloadListener;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.grade.assign.student.StudentsFragment;
import ca.dataready.SmartSchoolAdmin.grade.assign.teacher.TeachersFragment;
import ca.dataready.SmartSchoolAdmin.grade.assign.time_table.TimeTableFragment;
import ca.dataready.SmartSchoolAdmin.server.APIError;

public class AssignClassNGradeActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    private String gradeId, classId;
    OnReloadListener onReloadListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_class_ngrade);
        ButterKnife.bind(this);
        setHeaderInfo();
        Init();
    }

    private void setHeaderInfo() {

        setSupportActionBar(toolbar);

        if (AdminApp.getInstance().getAdmin() != null)
            getSupportActionBar().setTitle(AdminApp.getInstance().getAdmin().getSchoolName() + " " + AdminApp.getInstance().getAdmin().getSchoolYear());
        else
            getSupportActionBar().setTitle(getString(R.string.app_name));

        getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + getString(R.string.assign_class_grade) + "</small>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void Init() {

        gradeId = getIntent().getStringExtra(App_Constants.GRADE_ID);
        classId = getIntent().getStringExtra(App_Constants.CLASS_ID);

        initViewPagerAndTabs();
    }

    private void initViewPagerAndTabs() {

        PagerAdapters pagerAdapter = new PagerAdapters(getSupportFragmentManager());

        TeachersFragment teachersFragment = new TeachersFragment();
        Bundle tBundle = new Bundle();
        tBundle.putString(App_Constants.GRADE_ID, gradeId);
        tBundle.putString(App_Constants.CLASS_ID, classId);
        teachersFragment.setArguments(tBundle);

        pagerAdapter.addFragment(teachersFragment, getString(R.string.teachers));

        StudentsFragment studentsFragment = new StudentsFragment();
        Bundle sBundle = new Bundle();
        sBundle.putString(App_Constants.GRADE_ID, gradeId);
        sBundle.putString(App_Constants.CLASS_ID, classId);
        studentsFragment.setArguments(sBundle);

        pagerAdapter.addFragment(studentsFragment, getString(R.string.students));

        TimeTableFragment timeTableFragment = new TimeTableFragment();
        Bundle ttBundle = new Bundle();
        ttBundle.putString(App_Constants.GRADE_ID, gradeId);
        ttBundle.putString(App_Constants.CLASS_ID, classId);
        timeTableFragment.setArguments(ttBundle);

        pagerAdapter.addFragment(timeTableFragment, getString(R.string.time_table));


        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

    }

    class PagerAdapters extends FragmentPagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public PagerAdapters(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }

    public void setOnReloadListener(OnReloadListener onReloadListener) {
        this.onReloadListener = onReloadListener;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == APIError.UPDATE_TOKEN && resultCode == RESULT_OK) {

            if (data != null) {
                String apiName = data.getStringExtra("api");
                if (onReloadListener != null)
                    onReloadListener.onReload(apiName);
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

}
