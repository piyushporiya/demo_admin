/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.grade.assign.teacher;


import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ca.dataready.SmartSchoolAdmin.Listeners.OnReloadListener;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.LastItemDecoration;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.grade.assign.AssignClassNGradeActivity;
import ca.dataready.SmartSchoolAdmin.grade.assign.teacher.adapter.AssignedTeacherAdapter;
import ca.dataready.SmartSchoolAdmin.grade.assign.teacher.add.AssignTeacherActivity;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.CommonResponse;
import ca.dataready.SmartSchoolAdmin.server.GetAssignedTeacherList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class TeachersFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, OnReloadListener {


    @BindView(R.id.recyclerView)
    SuperRecyclerView recyclerView;
    @BindView(R.id.fab_create)
    FloatingActionButton fabCreate;
    Unbinder unbinder;
    @BindView(R.id.et_week_day)
    TextView etWeekDay;
    @BindView(R.id.rl_week_day)
    RelativeLayout rlWeekDay;
    private GridLayoutManager gridLayoutManager;
    private AssignedTeacherAdapter adapter;
    private String gradeId, classId;
    private String weekday;
    private Call<GetAssignedTeacherList> appApi;
    private List<GetAssignedTeacherList.ResultBean> results;
    private ListPopupWindow listPopupWindow;
    private ArrayList<String> weekDayList = new ArrayList<>();
    private String idToDelete;
    private Call<CommonResponse> deleteApi;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_teachers, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((AssignClassNGradeActivity)getActivity()).setOnReloadListener(this);
        Init();
    }

    private void Init() {

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            gradeId = bundle.getString(App_Constants.GRADE_ID);
            classId = bundle.getString(App_Constants.CLASS_ID);
        }

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            gridLayoutManager = new GridLayoutManager(getContext(), 3);
        } else {
            gridLayoutManager = new GridLayoutManager(getContext(), 2);
        }

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.addItemDecoration(new LastItemDecoration());
        recyclerView.setRefreshListener(TeachersFragment.this);

        weekday = AdminApp.getInstance().weekDay();
        etWeekDay.setText(AdminApp.getInstance().weekDayInLatters(weekday));

        getTeacherList();
    }

    private void getTeacherList() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();

        appApi = AdminApp.getInstance().getApi().getAssignedTeacherList(entity.getSchoolId(), entity.getSchoolYear(), gradeId, classId, weekday);
        appApi.enqueue(new Callback<GetAssignedTeacherList>() {
            @Override
            public void onResponse(Call<GetAssignedTeacherList> call, Response<GetAssignedTeacherList> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            results = response.body().getResult();
                            if (results != null && results.size() > 0) {

                                setAdapter();

                            } else {
                                adapter = new AssignedTeacherAdapter(getActivity(), TeachersFragment.this);
                                recyclerView.setAdapter(adapter);
                                TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                                emptyView.setText(getString(R.string.no_data));
                            }

                        } else {
                            adapter = new AssignedTeacherAdapter(getActivity(), TeachersFragment.this);
                            recyclerView.setAdapter(adapter);
                            TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                            emptyView.setText(getString(R.string.no_data));
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_ASSIGNED_TEACHER_LIST);
                        adapter = new AssignedTeacherAdapter(getActivity(), TeachersFragment.this);
                        recyclerView.setAdapter(adapter);
                        TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                        emptyView.setText(error.message());

                    }
                } catch (Exception e) {

                    adapter = new AssignedTeacherAdapter(getActivity(), TeachersFragment.this);
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(getString(R.string.somethingwrong));
                }

            }

            @Override
            public void onFailure(Call<GetAssignedTeacherList> call, Throwable t) {
                if (!call.isCanceled()) {
                    adapter = new AssignedTeacherAdapter(getActivity(), TeachersFragment.this);
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(App_Constants.NO_INTERNET);
                }
            }
        });
    }

    private void setAdapter() {

        adapter = new AssignedTeacherAdapter(getActivity(), TeachersFragment.this);
        recyclerView.setAdapter(adapter);
        adapter.addItems(results);
    }

    public void showPopUp(ImageView imgMore, int position, GetAssignedTeacherList.ResultBean bean) {

        PopupMenu popup = new PopupMenu(imgMore.getContext(), imgMore);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.edit_assign_teacher_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new CardMenuItemClickListener(position, bean));
        popup.show();
    }

    private class CardMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        private int position;
        private GetAssignedTeacherList.ResultBean bean;

        CardMenuItemClickListener(int positon, GetAssignedTeacherList.ResultBean bean) {
            this.position = positon;
            this.bean = bean;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {

                case R.id.item_edit:

                    startActivityForResult(new Intent(getActivity(), AssignTeacherActivity.class)
                            .putExtra(App_Constants.OBJECT, bean)
                            .putExtra(App_Constants.TYPE,App_Constants.UPDATE), App_Constants.UPDATE_LISTING);
                    if (getActivity() != null)
                        getActivity().overridePendingTransition(R.anim.enter, R.anim.leave);

                    break;

                case R.id.item_remove:

                    idToDelete = bean.getId();
                    deleteAssignedTeacher();

                    break;

                default:
            }
            return false;
        }
    }

    private void deleteAssignedTeacher() {

        Utility.showProgress(getActivity(), getString(R.string.processing));

        deleteApi = AdminApp.getInstance().getApi().deleteAssignedTeacher(idToDelete);
        deleteApi.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            getTeacherList();
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_DELETE_ASSIGNED_TEACHER);
                        Toast.makeText(getActivity(), "" + error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    Toast.makeText(getActivity(), App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                    Utility.hideProgress();
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStop() {
        super.onStop();

        if (deleteApi != null)
            deleteApi.cancel();

        if (appApi != null)
            appApi.cancel();
    }

    @OnClick({R.id.fab_create, R.id.rl_week_day})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.fab_create:

                startActivityForResult(new Intent(getActivity(), AssignTeacherActivity.class)
                        .putExtra(App_Constants.GRADE_ID, gradeId)
                        .putExtra(App_Constants.CLASS_ID, classId), App_Constants.UPDATE_LISTING);
                if (getActivity() != null)
                    getActivity().overridePendingTransition(R.anim.enter, R.anim.leave);

                break;

            case R.id.rl_week_day:

                show_drop_down_to_select_option(rlWeekDay, App_Constants.WEEK_DAY);

                break;
        }
    }

    private void show_drop_down_to_select_option(RelativeLayout layout, final String which) {

        listPopupWindow = new ListPopupWindow(getActivity());
        if (which.equals(App_Constants.WEEK_DAY)) {

            weekDayList.clear();
            weekDayList.add(getString(R.string.monday));
            weekDayList.add(getString(R.string.tuesday));
            weekDayList.add(getString(R.string.wednesday));
            weekDayList.add(getString(R.string.thursday));
            weekDayList.add(getString(R.string.friday));
            weekDayList.add(getString(R.string.saturday));

            listPopupWindow.setAdapter(new ArrayAdapter(getActivity(), R.layout.list_dropdown_item, weekDayList));

        }

        listPopupWindow.setAnchorView(layout);
        listPopupWindow.setWidth(ListPopupWindow.WRAP_CONTENT);
        listPopupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);

        listPopupWindow.setModal(true);
        listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if (which.equals(App_Constants.WEEK_DAY)) {

                    etWeekDay.setText(weekDayList.get(i));

                    weekday = AdminApp.getInstance().weekDayInNumbers(weekDayList.get(i));
                    recyclerView.showProgress();
                    getTeacherList();

                }
                listPopupWindow.dismiss();

            }
        });
        listPopupWindow.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == APIError.UPDATE_TOKEN && resultCode == RESULT_OK) {

            if (data != null) {
                String apiName = data.getStringExtra("api");
                onReload(apiName);
            }

        } else if (requestCode == App_Constants.UPDATE_LISTING && resultCode == RESULT_OK) {

            getTeacherList();

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRefresh() {

        if (isVisible())
            getTeacherList();
    }

    @Override
    public void onReload(String apiName) {

        if (apiName != null) {

            if (isVisible()) {
                if (apiName.equals(App_Constants.API_ASSIGNED_TEACHER_LIST)) {
                    getTeacherList();
                }else if (apiName.equals(App_Constants.API_DELETE_ASSIGNED_TEACHER)) {
                    deleteAssignedTeacher();
                }
            }
        }
    }




    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            gridLayoutManager = new GridLayoutManager(getContext(), 3);
        } else {
            gridLayoutManager = new GridLayoutManager(getContext(), 2);
        }
        recyclerView.setLayoutManager(gridLayoutManager);
    }

}
