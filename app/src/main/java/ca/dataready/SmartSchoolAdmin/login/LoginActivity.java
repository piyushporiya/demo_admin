/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.login;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.ForgotPassResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.et_Email)
    EditText etEmail;
    @BindView(R.id.et_Password)
    EditText etPassword;
    @BindView(R.id.cb_Remember)
    CheckBox cbRemember;
    @BindView(R.id.txt_ForgotPass)
    TextView txtForgotPass;
    @BindView(R.id.txt_SignIn)
    Button txtSignIn;
    int width, height;
    @BindView(R.id.txt_version)
    TextView txtVersion;
    @BindView(R.id.txt_env)
    TextView txtEnv;
    private int currentYear;
    SharedPreferences preferences;
    Call<CREDENTIAL> call;
    Call<ForgotPassResponse> forgotpasCall;
    private static final String REMEMBER_ME = "remember_me";
    private static final int PERMISSION_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);
        ButterKnife.bind(this);

        if (!checkPermission()) {
            requestPermission();
        }
        preferences = getSharedPreferences(REMEMBER_ME, MODE_PRIVATE);
        // getSupportActionBar().hide();
        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setupControls();
    }

    private void setupControls() {

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        width = displaymetrics.widthPixels;
        height = displaymetrics.heightPixels;
        etEmail.setText(preferences.getString("email", ""));
        etPassword.setText(preferences.getString("password", ""));
        cbRemember.setChecked(preferences.getBoolean("isChecked", false));

        if (AdminApp.getInstance().getConfigs() == null)
            return;

        if (AdminApp.getInstance().getConfigs().getResult() == null)
            return;

        txtVersion.setVisibility(View.VISIBLE);
        txtVersion.setText(getString(R.string.app) + " : " + AdminApp.getInstance().getConfigs().getResult().getAppversion() + "(" + AdminApp.getInstance().getConfigs().getResult().getAppbuildnumber() + ")");

        if (!AdminApp.getInstance().getConfigs().getResult().getEnv().equalsIgnoreCase("prod")) {
            txtEnv.setVisibility(View.VISIBLE);
            txtEnv.setText(getString(R.string.env) + " : " + AdminApp.getInstance().getConfigs().getResult().getEnv());
        }

    }

    @OnClick({R.id.cb_Remember, R.id.txt_ForgotPass, R.id.txt_SignIn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cb_Remember:
                break;
            case R.id.txt_ForgotPass:
                showPopup_for_Forgot_Password(LoginActivity.this);
                break;
            case R.id.txt_SignIn:
                /*Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(intent);*/
                checkAllValidations();
                break;
        }
    }

    private void checkAllValidations() {

        if (etEmail.getText().toString().trim().equals("")) {
            etEmail.setError(getString(R.string.enter_email));
        } else if (etPassword.getText().toString().trim().equals("")) {
            etPassword.setError(getString(R.string.enter_password));
        } else if (!(etEmail.getText().toString().trim().matches(App_Constants.emailPattern))) {
            etEmail.setError(getString(R.string.not_valid_email));
        } else {
            if (cbRemember.isChecked()) {
                preferences.edit().putString("email", etEmail.getText().toString()).apply();
                preferences.edit().putString("password", etPassword.getText().toString()).apply();
                preferences.edit().putBoolean("isChecked", true).apply();
            } else {
                preferences.edit().clear().apply();
            }
            performLogIn();
        }
    }

    private void performLogIn() {

        Utility.showProgress(LoginActivity.this, getString(R.string.processing));

        LoginParams loginParams = new LoginParams();
        loginParams.setEmailId(etEmail.getText().toString().trim());
        loginParams.setPassword(etPassword.getText().toString().trim());

        call = AdminApp.getInstance().getLoginApi().login(loginParams);
        call.enqueue(new Callback<CREDENTIAL>() {
            @Override
            public void onResponse(Call<CREDENTIAL> call, Response<CREDENTIAL> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            if (response.body().getResult() != null && response.body().getResult().size() > 0) {

                                if (response.body().getResult().get(0).getUserpermissions() != null) {

                                    if (response.body().getResult().get(0).getUserpermissions().getAdminmodules() != null) {

                                        if (response.body().getResult().get(0).getUserpermissions().getAdminmodules().isLogin()) {

                                            Crashlytics.log("Hey I've created custom log! YAY :)");
                                            Crashlytics.setString("emailID",response.body().getResult().get(0).getEmailId());
                                            if (AdminApp.getInstance().getConfigs() != null) {
                                                if (AdminApp.getInstance().getConfigs().getResult() != null) {
                                                    Crashlytics.setString("appVersion", AdminApp.getInstance().getConfigs().getResult().getAppversion() + " (" + AdminApp.getInstance().getConfigs().getResult().getAppbuildnumber() + ")");
                                                    Crashlytics.setString("environment", AdminApp.getInstance().getConfigs().getResult().getEnv());
                                                    Crashlytics.setString("region", AdminApp.getInstance().getConfigs().getResult().getRegion());
                                                }
                                            }
                                            startNextActivity(response.body());

                                        } else {

                                            Toast.makeText(LoginActivity.this, getString(R.string.not_authorized), Toast.LENGTH_SHORT).show();
                                        }
                                    } else {

                                        startNextActivity(response.body());
                                    }
                                } else {

                                    startNextActivity(response.body());

                                }
                            } else {

                                Toast.makeText(LoginActivity.this, getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                            }

                        } else {

                            Utility.showSnackBar(txtSignIn, response.body().getMessage());
                        }

                    } else {

                        APIError error = APIError.parseError(response, LoginActivity.this, App_Constants.API_LOGIN);
                        Utility.showSnackBar(txtSignIn, error.message());
                    }

                } catch (Exception e) {
                    Utility.showSnackBar(txtSignIn, getString(R.string.somethingwrong));
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<CREDENTIAL> call, Throwable t) {
                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    System.out.println(t.getMessage());
                    Utility.showSnackBar(txtSignIn, App_Constants.NO_INTERNET);
                }
            }
        });
    }

    private void startNextActivity(CREDENTIAL body) {

        AdminApp.getInstance().saveCredential(body);
        Utility.launchEntryIntent(LoginActivity.this, HomeActivity.class);
        LoginActivity.this.finish();
    }


    private void showPopup_for_Forgot_Password(final Activity context) {

        final View view = LayoutInflater.from(LoginActivity.this).inflate(R.layout.forgot_password, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle(R.string.forgot_password);
        builder.setView(view);
        builder.setCancelable(true);

        final EditText etEmail = (EditText) view.findViewById(R.id.et_Email);

        builder.setPositiveButton(R.string.submit, null);
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {

                Button button = ((AlertDialog) alertDialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // TODO Do something

                        if (etEmail.getText().toString().trim().equals("") || !(etEmail.getText().toString().trim().matches(App_Constants.emailPattern))) {
                            if (etEmail.getText().toString().trim().equals("")) {
                                etEmail.setError(getString(R.string.enter_email));
                            } else {
                                etEmail.setError(getString(R.string.not_valid_email));
                            }
                        } else {
                            Utility.hideKeyboard(LoginActivity.this);
                            alertDialog.dismiss();
                            perform_forgot_password(etEmail.getText().toString().trim());
                        }
                    }
                });
            }
        });
        alertDialog.show();
    }

    public class ForgotPassParams {


        /**
         * emailId : string
         * newPassword : string
         * password : string
         */

        private String emailId;
        private String newPassword;
        private String password;

        public String getEmailId() {
            return emailId;
        }

        public void setEmailId(String emailId) {
            this.emailId = emailId;
        }

        public String getNewPassword() {
            return newPassword;
        }

        public void setNewPassword(String newPassword) {
            this.newPassword = newPassword;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

    private void perform_forgot_password(String email) {

        Utility.showProgress(LoginActivity.this, "Processing...");

        ForgotPassParams frogotPassParams = new ForgotPassParams();
        frogotPassParams.setEmailId(email);

        forgotpasCall = AdminApp.getInstance().getLoginApi().forgotPassword(frogotPassParams);
        forgotpasCall.enqueue(new Callback<ForgotPassResponse>() {
            @Override
            public void onResponse(Call<ForgotPassResponse> call, Response<ForgotPassResponse> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            Utility.showSnackBar(txtSignIn, response.body().getMessage());
                        } else {
                            Utility.showSnackBar(txtSignIn, response.body().getMessage());
                        }

                    } else {
                        APIError error = APIError.parseError(response, LoginActivity.this, App_Constants.API_FORGOT_PASS);
                        Utility.showSnackBar(txtSignIn, error.message());
                    }
                } catch (Exception e) {
                    Utility.showSnackBar(txtSignIn, getString(R.string.somethingwrong));
                    e.printStackTrace();
                }
                Utility.hideProgress();
            }

            @Override
            public void onFailure(Call<ForgotPassResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    System.out.println(t.getMessage());
                    Utility.showSnackBar(txtSignIn, App_Constants.NO_INTERNET);
                }
            }
        });

    }

    public class LoginParams {

        /**
         * emailId : string
         * facebookId : string
         * googleId : string
         * loginType : string
         * newPassword : string
         * password : string
         */

        private String emailId;
        private String facebookId;
        private String googleId;
        private String loginType;
        private String newPassword;
        private String password;

        public String getEmailId() {
            return emailId;
        }

        public void setEmailId(String emailId) {
            this.emailId = emailId;
        }

        public String getFacebookId() {
            return facebookId;
        }

        public void setFacebookId(String facebookId) {
            this.facebookId = facebookId;
        }

        public String getGoogleId() {
            return googleId;
        }

        public void setGoogleId(String googleId) {
            this.googleId = googleId;
        }

        public String getLoginType() {
            return loginType;
        }

        public void setLoginType(String loginType) {
            this.loginType = loginType;
        }

        public String getNewPassword() {
            return newPassword;
        }

        public void setNewPassword(String newPassword) {
            this.newPassword = newPassword;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

    private boolean checkPermission() {

        int wifiState = ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.ACCESS_WIFI_STATE);
        int networkstate = ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.ACCESS_NETWORK_STATE);
        int read = ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int write = ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int camera = ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.CAMERA);
        int readCal = ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.READ_CALENDAR);
        int writeCal = ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.WRITE_CALENDAR);
        if (wifiState == PackageManager.PERMISSION_GRANTED && networkstate == PackageManager.PERMISSION_GRANTED
                && read == PackageManager.PERMISSION_GRANTED && write == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED
                && readCal == PackageManager.PERMISSION_GRANTED
                && writeCal == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }

    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this, Manifest.permission.ACCESS_WIFI_STATE)
                || ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this, Manifest.permission.ACCESS_NETWORK_STATE)
                || ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                || ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                || ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this, Manifest.permission.CAMERA)
                || ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this, Manifest.permission.READ_CALENDAR)
                || ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this, Manifest.permission.WRITE_CALENDAR)) {

            Toast.makeText(LoginActivity.this, R.string.allow_app_setting, Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(LoginActivity.this,
                    new String[]{Manifest.permission.ACCESS_WIFI_STATE, Manifest.permission.ACCESS_NETWORK_STATE
                            , Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA
                            , Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR}, PERMISSION_REQUEST_CODE);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED
                        && grantResults[2] == PackageManager.PERMISSION_GRANTED
                        && grantResults[3] == PackageManager.PERMISSION_GRANTED
                        && grantResults[4] == PackageManager.PERMISSION_GRANTED
                        && grantResults[5] == PackageManager.PERMISSION_GRANTED
                        && grantResults[6] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(LoginActivity.this, R.string.permission_granted, Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
}
