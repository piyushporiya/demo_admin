/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.dashboard.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ca.dataready.SmartSchoolAdmin.R;

/**
 * Created by socialinfotech12 on 2/28/18.
 */

public class ExpandableListData {

    public static HashMap<String, List<ListModel>> getData() {
        HashMap<String, List<ListModel>> expandableListDetail = new HashMap<String, List<ListModel>>();

        List<ListModel> transportation = new ArrayList<>();
        transportation.add(new ListModel(R.drawable.ic_route, "Route"));
        expandableListDetail.put("Transportation", transportation);

        List<ListModel> classschedule = new ArrayList<>();
        classschedule.add(new ListModel(R.drawable.ic_grade, "Grade"));
        classschedule.add(new ListModel(R.drawable.ic_events, "Class Events"));
        classschedule.add(new ListModel(R.drawable.ic_events, "School Events"));
        expandableListDetail.put("Class Schedules info", classschedule);

        List<ListModel> admission = new ArrayList<>();
        admission.add(new ListModel(R.drawable.ic_student, "Student"));
        admission.add(new ListModel(R.drawable.ic_teacher, "Teacher"));
        admission.add(new ListModel(R.drawable.ic_teacher, "Staff"));
        admission.add(new ListModel(R.drawable.ic_driver, "Driver"));
        expandableListDetail.put("Admission/ On Board", admission);


        List<ListModel> userinfo = new ArrayList<>();
        userinfo.add(new ListModel(R.drawable.ic_student, "Student"));
        userinfo.add(new ListModel(R.drawable.ic_teacher, "Teacher"));
        userinfo.add(new ListModel(R.drawable.ic_teacher, "Staff"));
        userinfo.add(new ListModel(R.drawable.ic_driver, "Driver"));
        expandableListDetail.put("Users info", userinfo);


        List<ListModel> communication = new ArrayList<>();
        communication.add(new ListModel(R.drawable.ic_view_respond, "View & Respond"));
        communication.add(new ListModel(R.drawable.ic_school, "School Channel"));
        communication.add(new ListModel(R.drawable.ic_class_channel, "Class Channel"));
        communication.add(new ListModel(R.drawable.ic_appoinment, "Appoinment"));
        expandableListDetail.put("Communication", communication);

        //expandableListDetail.put("Logout", null);

        return expandableListDetail;
    }
}
