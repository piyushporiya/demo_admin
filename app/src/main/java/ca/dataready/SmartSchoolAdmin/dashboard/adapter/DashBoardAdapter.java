/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.dashboard.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.dashboard.DashBoardFragment;
import ca.dataready.SmartSchoolAdmin.HomeActivity;

/**
 * Created by socialinfotech12 on 2/28/18.
 */

public class DashBoardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<DashBoard> mItemList;
    private Context context;

    public DashBoardAdapter(Context context, ArrayList<DashBoard> boardArrayList) {
        this.mItemList = boardArrayList;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater mInflater = LayoutInflater.from(parent.getContext());
        ViewGroup view = (ViewGroup) mInflater.inflate(R.layout.board_item, parent, false);
        return new BoardViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        final DashBoard entity = mItemList.get(position);

        ((BoardViewHolder) viewHolder).view.setEnabled(entity.isEnabled());
        ((BoardViewHolder) viewHolder).image.setAlpha(entity.isEnabled() ? 1.0f : 0.5f);
        ((BoardViewHolder) viewHolder).name.setAlpha(entity.isEnabled() ? 1.0f : 0.5f);

        if (entity.isNeedBadge()) {

            if (entity.getNotificationCount() == 0) {

                ((BoardViewHolder) viewHolder).counter.setVisibility(View.GONE);

            } else {

                ((BoardViewHolder) viewHolder).counter.setVisibility(View.VISIBLE);
                if (entity.getNotificationCount() > 99) {
                    ((BoardViewHolder) viewHolder).counter.setText("99+");
                } else {
                    ((BoardViewHolder) viewHolder).counter.setText(String.valueOf(entity.getNotificationCount()));
                }
            }
        } else {
            ((BoardViewHolder) viewHolder).counter.setVisibility(View.GONE);
        }

        ((BoardViewHolder) viewHolder).image.setImageResource(entity.getImage());
        ((BoardViewHolder) viewHolder).name.setText(entity.getName());
        ((BoardViewHolder) viewHolder).view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (entity.getFragment() instanceof DashBoardFragment) {
                    ((HomeActivity) context).LogoutDialog();
                } else {
                    ((HomeActivity) context).Loadfragment(entity.getFragment());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    public void addItem(List<DashBoard> productDetails) {
        mItemList.addAll(productDetails);
        notifyDataSetChanged();
    }


    class BoardViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.counter)
        TextView counter;
        View view;

        public BoardViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            ButterKnife.bind(this, itemView);
        }
    }

}