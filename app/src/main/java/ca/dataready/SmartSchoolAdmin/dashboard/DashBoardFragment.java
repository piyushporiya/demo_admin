/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.dashboard;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.Listeners.OnReloadListener;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.acadamic.AcadamicYearFragment;
import ca.dataready.SmartSchoolAdmin.active_route.ActiveRoutesFragment;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.appointment.AppointmentMainFragment;
import ca.dataready.SmartSchoolAdmin.classchannel.ClassChannelFragment;
import ca.dataready.SmartSchoolAdmin.configuration.ConfigurationFragment;
import ca.dataready.SmartSchoolAdmin.counter.BadgeCountTracker;
import ca.dataready.SmartSchoolAdmin.dashboard.adapter.DashBoard;
import ca.dataready.SmartSchoolAdmin.dashboard.adapter.DashBoardAdapter;
import ca.dataready.SmartSchoolAdmin.grade.GradeFragment;
import ca.dataready.SmartSchoolAdmin.help.HelpFragment;
import ca.dataready.SmartSchoolAdmin.photo_album.PhotoAlbumFragment;
import ca.dataready.SmartSchoolAdmin.route.RouteFragment;
import ca.dataready.SmartSchoolAdmin.schoolchannel.SchoolChannelFragment;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.AddUpdateSubjectResponse;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.GetSchoolInfo;
import ca.dataready.SmartSchoolAdmin.sms_emails.SmsEmailsFragment;
import ca.dataready.SmartSchoolAdmin.subject.add.AddSubjectActivity;
import ca.dataready.SmartSchoolAdmin.userinfo.driver.IDriverFragment;
import ca.dataready.SmartSchoolAdmin.userinfo.staff.IStaffFragment;
import ca.dataready.SmartSchoolAdmin.userinfo.student.IStudentFragment;
import ca.dataready.SmartSchoolAdmin.userinfo.teacher.ITeacherFragment;
import ca.dataready.SmartSchoolAdmin.viewresponse.ViewResponseFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.UPDATE_DASHBOARD_BROADCAST;


public class DashBoardFragment extends Fragment implements OnReloadListener {


    @BindView(R.id.recycleview)
    RecyclerView recycleview;
    Unbinder unbinder;

    DashBoardAdapter adapter;
    GridLayoutManager gridLayoutManager;
    private Call<GetSchoolInfo> call;
    public static boolean isDashboardVisible = false;
    private int viewNResponseNotificationCount, appointmentNotificationCount;
    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dash_board, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) getActivity()).setSubTitle(getString(R.string.dashboard));
        ((HomeActivity) getActivity()).setOnReloadListener(this);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            gridLayoutManager = new GridLayoutManager(getContext(), 5);
        } else {
            gridLayoutManager = new GridLayoutManager(getContext(), 3);
        }

        recycleview.setLayoutManager(gridLayoutManager);

        getSchoolInfo();

        setBadgeCounterAndFillAdapter();
    }

    private void getSchoolInfo() {

        call = AdminApp.getInstance().getApi().getSchoolInfo(AdminApp.getInstance().getAdmin().getSchoolId());
        call.enqueue(new Callback<GetSchoolInfo>() {
            @Override
            public void onResponse(Call<GetSchoolInfo> call, Response<GetSchoolInfo> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            AdminApp.getInstance().saveSchoolInfo(response.body());
                        }
                    } else {
                        APIError.parseError(response, getActivity(), App_Constants.API_GET_SCHOOL_INFO);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GetSchoolInfo> call, Throwable t) {

                if (!call.isCanceled()) {

                }
            }
        });
    }

    private void setBadgeCounterAndFillAdapter() {

        viewNResponseNotificationCount = BadgeCountTracker.getViewandResposndBadgeCounter(getActivity());
        appointmentNotificationCount = BadgeCountTracker.getAppointmentBadgeCounter(getActivity());
        setAdapter();
    }

    private void setAdapter() {

        CREDENTIAL mainEntity = AdminApp.getInstance().getCredential();
        CREDENTIAL.ResultBean.SchoolpermissionsBean schoolPermissions = null;
        CREDENTIAL.ResultBean.UserpermissionsBean.AdminmodulesBean userEntity = null;
        ArrayList<DashBoard> dashBoardsList = new ArrayList<>();

        if (mainEntity != null && mainEntity.getResult() != null && mainEntity.getResult().size() > 0) {
            schoolPermissions = mainEntity.getResult().get(0).getSchoolpermissions();
            CREDENTIAL.ResultBean.UserpermissionsBean ue = mainEntity.getResult().get(0).getUserpermissions();
            if (ue != null) {
                userEntity = ue.getAdminmodules();
            }
        }

        if (schoolPermissions != null) {

            if (schoolPermissions.getAdminmodules().isCommunication()) {

                if (userEntity != null)
                    dashBoardsList.add(new DashBoard(R.drawable.ic_view_respond, getString(R.string.view_and_respond), new ViewResponseFragment(), true, viewNResponseNotificationCount, userEntity.isCommunication()));
                else
                    dashBoardsList.add(new DashBoard(R.drawable.ic_view_respond, getString(R.string.view_and_respond), new ViewResponseFragment(), true, viewNResponseNotificationCount, true));
            }

            if (schoolPermissions.getAdminmodules().isChannel()) {
                if (userEntity != null) {
                    dashBoardsList.add(new DashBoard(R.drawable.ic_school, getString(R.string.school_channel), new SchoolChannelFragment(), false, 0, userEntity.isChannel()));
                    dashBoardsList.add(new DashBoard(R.drawable.ic_class_channel, getString(R.string.class_channel), new ClassChannelFragment(), false, 0, userEntity.isChannel()));
                } else {
                    dashBoardsList.add(new DashBoard(R.drawable.ic_school, getString(R.string.school_channel), new SchoolChannelFragment(), false, 0, true));
                    dashBoardsList.add(new DashBoard(R.drawable.ic_class_channel, getString(R.string.class_channel), new ClassChannelFragment(), false, 0, true));
                }
            }

            if (schoolPermissions.getAdminmodules().isAppointment()) {
                if (userEntity != null)
                    dashBoardsList.add(new DashBoard(R.drawable.ic_appoinment, getString(R.string.appointment), new AppointmentMainFragment(), true, appointmentNotificationCount, userEntity.isAppointment()));
                else
                    dashBoardsList.add(new DashBoard(R.drawable.ic_appoinment, getString(R.string.appointment), new AppointmentMainFragment(), true, appointmentNotificationCount, true));
            }

            dashBoardsList.add(new DashBoard(R.drawable.ic_grade, getString(R.string.grade), new GradeFragment(), false, 0, true));
            dashBoardsList.add(new DashBoard(R.drawable.ic_acedamic_year, getString(R.string.acedmic_year), new AcadamicYearFragment(), false, 0, true));

            if (schoolPermissions.getAdminmodules().isStudent()) {
                if (userEntity != null)
                    dashBoardsList.add(new DashBoard(R.drawable.ic_student, getString(R.string.student), new IStudentFragment(), false, 0, userEntity.isStudent()));
                else
                    dashBoardsList.add(new DashBoard(R.drawable.ic_student, getString(R.string.student), new IStudentFragment(), false, 0, true));
            }

            dashBoardsList.add(new DashBoard(R.drawable.ic_teacher, getString(R.string.teacher), new ITeacherFragment(), false, 0, true));

            if (schoolPermissions.getAdminmodules().isStaff()) {
                if (userEntity != null)
                    dashBoardsList.add(new DashBoard(R.drawable.ic_staff, getString(R.string.staff), new IStaffFragment(), false, 0, userEntity.isStaff()));
                else
                    dashBoardsList.add(new DashBoard(R.drawable.ic_staff, getString(R.string.staff), new IStaffFragment(), false, 0, true));
            }

            if (schoolPermissions.getAdminmodules().isDriver()) {
                if (userEntity != null)
                    dashBoardsList.add(new DashBoard(R.drawable.ic_driver, getString(R.string.driver), new IDriverFragment(), false, 0, userEntity.isDriver()));
                else
                    dashBoardsList.add(new DashBoard(R.drawable.ic_driver, getString(R.string.driver), new IDriverFragment(), false, 0, true));
            }

            if (schoolPermissions.getAdminmodules().isRoute()) {

                if (userEntity != null) {
                    dashBoardsList.add(new DashBoard(R.drawable.ic_route, getString(R.string.route), new RouteFragment(), false, 0, userEntity.isRoute()));
                    dashBoardsList.add(new DashBoard(R.drawable.ic_active_route, getString(R.string.active_route), new ActiveRoutesFragment(), false, 0, userEntity.isRoute()));
                } else {
                    dashBoardsList.add(new DashBoard(R.drawable.ic_route, getString(R.string.route), new RouteFragment(), false, 0, true));
                    dashBoardsList.add(new DashBoard(R.drawable.ic_active_route, getString(R.string.active_route), new ActiveRoutesFragment(), false, 0, true));
                }
            }

            dashBoardsList.add(new DashBoard(R.drawable.ic_configs, getString(R.string.send_sms_emails), new SmsEmailsFragment(), false, 0, true));
            dashBoardsList.add(new DashBoard(R.drawable.ic_photo_album, getString(R.string.photo_album), new PhotoAlbumFragment(), false, 0, true));
            dashBoardsList.add(new DashBoard(R.drawable.ic_configs, getString(R.string.configuration), new ConfigurationFragment(), false, 0, true));
            dashBoardsList.add(new DashBoard(R.drawable.ic_help, getString(R.string.help), new HelpFragment(), false, 0, true));
            dashBoardsList.add(new DashBoard(R.drawable.ic_logout_48dp, getString(R.string.logout), new DashBoardFragment(), false, 0, true));

        } else {

            if (userEntity != null) {

                dashBoardsList.add(new DashBoard(R.drawable.ic_view_respond, getString(R.string.view_and_respond), new ViewResponseFragment(), true, viewNResponseNotificationCount, userEntity.isCommunication()));
                dashBoardsList.add(new DashBoard(R.drawable.ic_school, getString(R.string.school_channel), new SchoolChannelFragment(), false, 0, userEntity.isChannel()));
                dashBoardsList.add(new DashBoard(R.drawable.ic_class_channel, getString(R.string.class_channel), new ClassChannelFragment(), false, 0, userEntity.isChannel()));
                dashBoardsList.add(new DashBoard(R.drawable.ic_appoinment, getString(R.string.appointment), new AppointmentMainFragment(), true, appointmentNotificationCount, userEntity.isAppointment()));
                dashBoardsList.add(new DashBoard(R.drawable.ic_grade, getString(R.string.grade), new GradeFragment(), false, 0, true));
                dashBoardsList.add(new DashBoard(R.drawable.ic_acedamic_year, getString(R.string.acedmic_year), new AcadamicYearFragment(), false, 0, true));
                dashBoardsList.add(new DashBoard(R.drawable.ic_student, getString(R.string.student), new IStudentFragment(), false, 0, userEntity.isStudent()));
                dashBoardsList.add(new DashBoard(R.drawable.ic_teacher, getString(R.string.teacher), new ITeacherFragment(), false, 0, true));
                dashBoardsList.add(new DashBoard(R.drawable.ic_staff, getString(R.string.staff), new IStaffFragment(), false, 0, userEntity.isStaff()));
                dashBoardsList.add(new DashBoard(R.drawable.ic_driver, getString(R.string.driver), new IDriverFragment(), false, 0, userEntity.isDriver()));
                dashBoardsList.add(new DashBoard(R.drawable.ic_route, getString(R.string.route), new RouteFragment(), false, 0, userEntity.isRoute()));
                dashBoardsList.add(new DashBoard(R.drawable.ic_active_route, getString(R.string.active_route), new ActiveRoutesFragment(), false, 0, userEntity.isRoute()));
                dashBoardsList.add(new DashBoard(R.drawable.ic_configs, getString(R.string.send_sms_emails), new SmsEmailsFragment(), false, 0, true));
                dashBoardsList.add(new DashBoard(R.drawable.ic_photo_album, getString(R.string.photo_album), new PhotoAlbumFragment(), false, 0, true));
                dashBoardsList.add(new DashBoard(R.drawable.ic_configs, getString(R.string.configuration), new ConfigurationFragment(), false, 0, true));
                dashBoardsList.add(new DashBoard(R.drawable.ic_help, getString(R.string.help), new HelpFragment(), false, 0, true));
                dashBoardsList.add(new DashBoard(R.drawable.ic_logout_48dp, getString(R.string.logout), new DashBoardFragment(), false, 0, true));

            } else {

                dashBoardsList.add(new DashBoard(R.drawable.ic_view_respond, getString(R.string.view_and_respond), new ViewResponseFragment(), true, viewNResponseNotificationCount, true));
                dashBoardsList.add(new DashBoard(R.drawable.ic_school, getString(R.string.school_channel), new SchoolChannelFragment(), false, 0, true));
                dashBoardsList.add(new DashBoard(R.drawable.ic_class_channel, getString(R.string.class_channel), new ClassChannelFragment(), false, 0, true));
                dashBoardsList.add(new DashBoard(R.drawable.ic_appoinment, getString(R.string.appointment), new AppointmentMainFragment(), true, appointmentNotificationCount, true));
                dashBoardsList.add(new DashBoard(R.drawable.ic_grade, getString(R.string.grade), new GradeFragment(), false, 0, true));
                dashBoardsList.add(new DashBoard(R.drawable.ic_acedamic_year, getString(R.string.acedmic_year), new AcadamicYearFragment(), false, 0, true));
                dashBoardsList.add(new DashBoard(R.drawable.ic_student, getString(R.string.student), new IStudentFragment(), false, 0, true));
                dashBoardsList.add(new DashBoard(R.drawable.ic_teacher, getString(R.string.teacher), new ITeacherFragment(), false, 0, true));
                dashBoardsList.add(new DashBoard(R.drawable.ic_staff, getString(R.string.staff), new IStaffFragment(), false, 0, true));
                dashBoardsList.add(new DashBoard(R.drawable.ic_driver, getString(R.string.driver), new IDriverFragment(), false, 0, true));
                dashBoardsList.add(new DashBoard(R.drawable.ic_route, getString(R.string.route), new RouteFragment(), false, 0, true));
                dashBoardsList.add(new DashBoard(R.drawable.ic_active_route, getString(R.string.active_route), new ActiveRoutesFragment(), false, 0, true));
                dashBoardsList.add(new DashBoard(R.drawable.ic_configs, getString(R.string.send_sms_emails), new SmsEmailsFragment(), false, 0, true));
                dashBoardsList.add(new DashBoard(R.drawable.ic_photo_album, getString(R.string.photo_album), new PhotoAlbumFragment(), false, 0, true));
                dashBoardsList.add(new DashBoard(R.drawable.ic_configs, getString(R.string.configuration), new ConfigurationFragment(), false, 0, true));
                dashBoardsList.add(new DashBoard(R.drawable.ic_help, getString(R.string.help), new HelpFragment(), false, 0, true));
                dashBoardsList.add(new DashBoard(R.drawable.ic_logout_48dp, getString(R.string.logout), new DashBoardFragment(), false, 0, true));
            }
        }

        adapter = new DashBoardAdapter(getContext(), dashBoardsList);
        recycleview.setAdapter(adapter);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            gridLayoutManager = new GridLayoutManager(getContext(), 5);
        } else {
            gridLayoutManager = new GridLayoutManager(getContext(), 3);
        }
        recycleview.setLayoutManager(gridLayoutManager);
    }

    @Override
    public void onReload(String apiName) {

        if (apiName != null && isVisible()) {

            if (apiName.equalsIgnoreCase(App_Constants.API_GET_SCHOOL_INFO)) {

                getSchoolInfo();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        isDashboardVisible = false;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mUpdateDashboardReceiver,
                new IntentFilter(UPDATE_DASHBOARD_BROADCAST));
    }

    @Override
    public void onDetach() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mUpdateDashboardReceiver);
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (context != null)
            setBadgeCounterAndFillAdapter();
        isDashboardVisible = true;
    }


    private BroadcastReceiver mUpdateDashboardReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (isAdded() && isVisible())
                setBadgeCounterAndFillAdapter();

        }
    };
}
