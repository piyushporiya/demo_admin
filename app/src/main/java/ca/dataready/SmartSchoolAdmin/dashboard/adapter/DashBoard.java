/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.dashboard.adapter;

import android.support.v4.app.Fragment;

import ca.dataready.SmartSchoolAdmin.viewresponse.ViewResponseFragment;

/**
 * Created by socialinfotech12 on 2/28/18.
 */

public class DashBoard {

    private int image;
    private String name;
    private Fragment fragment;
    private boolean isNeedBadge =false;
    private int notificationCount = 0;
    private boolean isEnabled;



    public DashBoard(int image, String name, Fragment fragment, boolean isNeedBadge, int notificationCount, boolean isEnabled) {

        this.image = image;
        this.name = name;
        this.fragment = fragment;
        this.isNeedBadge = isNeedBadge;
        this.notificationCount = notificationCount;
        this.isEnabled = isEnabled;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public boolean isNeedBadge() {
        return isNeedBadge;
    }

    public void setNeedBadge(boolean needBadge) {
        isNeedBadge = needBadge;
    }

    public int getNotificationCount() {
        return notificationCount;
    }

    public void setNotificationCount(int notificationCount) {
        this.notificationCount = notificationCount;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
