/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.appointment.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.DateFunction;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.appointment.details.AppointmentDetailActivity;
import ca.dataready.SmartSchoolAdmin.server.AppApi;
import ca.dataready.SmartSchoolAdmin.server.TeacherAppointmentModel;
import ca.dataready.SmartSchoolAdmin.viewresponse.adapter.FilesAdapter;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.FileModel;
import ca.dataready.SmartSchoolAdmin.web.WebActivity;


/**
 * Created by social_jaydeep on 09/08/17.
 */

public class ScheduledAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public List<TeacherAppointmentModel.ResultBean> mItemList;
    private Context context;
    private int lastPosition = -1;
    private LayoutInflater mInflater;
    private static final int TYPE_VIEW = 0;
    private static final int TYPE_HEADER = 1;
    private FilesAdapter filesAdapter;
    private List<FileModel> selectedFiles = new ArrayList<>();


    public ScheduledAdapter(Context context) {
        mItemList = new ArrayList<>();
        this.context = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getItemViewType(int position) {
        if (mItemList.get(position).getType() == 1) {
            return TYPE_HEADER;
        }
        return TYPE_VIEW;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_VIEW) {

            return new DataViewHolder(LayoutInflater.from(context).inflate(R.layout.scheduled_list_item, parent, false));

        } else if (viewType == TYPE_HEADER) {

            return new HeaderViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_header, parent, false));

        } else {

            return null;
        }

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, int position) {
        final TeacherAppointmentModel.ResultBean entity = mItemList.get(position);


        if (viewHolder instanceof DataViewHolder) {
            ((DataViewHolder) viewHolder).txtTitle.setText(entity.getAppointmentTitle());
            ((DataViewHolder) viewHolder).txtTeacher.setText(entity.getTeacherName());
            ((DataViewHolder) viewHolder).txtsubject.setText(entity.getTeacherMobileNo());
            ((DataViewHolder) viewHolder).txtShowDate.setText(entity.getAppointmentTime());
            String formattedDate = DateFunction.ConvertDate(entity.getAppointmentDate(), "yyyy-MM-dd", "dd MMM yyyy");
            ((DataViewHolder) viewHolder).showMonth.setText(formattedDate);
            if (entity.getStudentList() != null && entity.getStudentList().size() > 0)
                ((DataViewHolder) viewHolder).txtStudent.setText(entity.getStudentList().get(0));

            if (entity.getFiles() != null && entity.getFiles().size() > 0) {

                ((DataViewHolder) viewHolder).recyclerView.setLayoutManager(new LinearLayoutManager(context));
                filesAdapter = new FilesAdapter(context, true);
                ((DataViewHolder) viewHolder).recyclerView.setAdapter(filesAdapter);
                selectedFiles.clear();
                if (entity.getFiles() != null && entity.getFiles().size() > 0) {
                    for (TeacherAppointmentModel.ResultBean.FilesBean model : entity.getFiles()) {
                        if (model.getFilePath().contains("/")) {
                            selectedFiles.add(new FileModel(model.getFilePath().substring(model.getFilePath().lastIndexOf("/") + 1), model.getFilePath()));
                        }
                    }
                }
                filesAdapter.addItem(selectedFiles);

            } else {
                ((DataViewHolder) viewHolder).txtViewMore.setVisibility(View.GONE);
            }

            ((DataViewHolder) viewHolder).txtTitle.setMaxLines(Integer.MAX_VALUE);

            if (entity.isExpanded()) {

                ((DataViewHolder) viewHolder).txtViewMore.setText(App_Constants.VIEW_LESS);
                ((DataViewHolder) viewHolder).txtTitle.setMaxLines(Integer.MAX_VALUE);
                ((DataViewHolder) viewHolder).recyclerView.setVisibility(View.VISIBLE);
                ((DataViewHolder) viewHolder).txtFileName.setVisibility(View.GONE);

            } else {

                if (entity.getFiles() != null && entity.getFiles().size() > 1) {

                    ((DataViewHolder) viewHolder).txtTitle.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                        @Override
                        public boolean onPreDraw() {
                            ((DataViewHolder) viewHolder).txtTitle.getViewTreeObserver().removeOnPreDrawListener(this);
                            int lineCount = ((DataViewHolder) viewHolder).txtTitle.getLineCount();
                            Log.e("TreeObserver", "onBind: " + lineCount);
                            if (lineCount > 1) {
                                ((DataViewHolder) viewHolder).txtTitle.setMaxLines(1);
                            }

                            return false;
                        }
                    });

                    ((DataViewHolder) viewHolder).txtViewMore.setVisibility(View.VISIBLE);
                    ((DataViewHolder) viewHolder).recyclerView.setVisibility(View.GONE);
                    ((DataViewHolder) viewHolder).txtFileName.setVisibility(View.VISIBLE);
                    ((DataViewHolder) viewHolder).txtFileName.setText(selectedFiles.get(0).getName());

                } else if (entity.getFiles() != null && entity.getFiles().size() > 0) {

                    ((DataViewHolder) viewHolder).txtViewMore.setVisibility(View.GONE);
                    ((DataViewHolder) viewHolder).recyclerView.setVisibility(View.GONE);
                    ((DataViewHolder) viewHolder).txtFileName.setVisibility(View.VISIBLE);
                    ((DataViewHolder) viewHolder).txtFileName.setText(selectedFiles.get(0).getName());

                    ((DataViewHolder) viewHolder).txtTitle.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                        @Override
                        public boolean onPreDraw() {
                            ((DataViewHolder) viewHolder).txtTitle.getViewTreeObserver().removeOnPreDrawListener(this);
                            int lineCount = ((DataViewHolder) viewHolder).txtTitle.getLineCount();
                            Log.e("TreeObserver", "onBind: " + lineCount);
                            if (lineCount > 1) {
                                ((DataViewHolder) viewHolder).txtViewMore.setVisibility(View.VISIBLE);
                                ((DataViewHolder) viewHolder).txtTitle.setMaxLines(1);
                            } else {
                                ((DataViewHolder) viewHolder).txtViewMore.setVisibility(View.GONE);
                            }

                            return false;
                        }
                    });

                } else {

                    ((DataViewHolder) viewHolder).txtViewMore.setVisibility(View.GONE);
                    ((DataViewHolder) viewHolder).recyclerView.setVisibility(View.GONE);
                    ((DataViewHolder) viewHolder).txtFileName.setVisibility(View.GONE);

                    ((DataViewHolder) viewHolder).txtTitle.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                        @Override
                        public boolean onPreDraw() {
                            ((DataViewHolder) viewHolder).txtTitle.getViewTreeObserver().removeOnPreDrawListener(this);
                            int lineCount = ((DataViewHolder) viewHolder).txtTitle.getLineCount();
                            Log.e("TreeObserver", "onBind: " + lineCount);
                            if (lineCount > 1) {
                                ((DataViewHolder) viewHolder).txtViewMore.setVisibility(View.VISIBLE);
                                ((DataViewHolder) viewHolder).txtTitle.setMaxLines(1);
                            } else {
                                ((DataViewHolder) viewHolder).txtViewMore.setVisibility(View.GONE);
                            }

                            return false;
                        }
                    });

                }


                ((DataViewHolder) viewHolder).txtViewMore.setText(App_Constants.VIEW_MORE);
                //   ((DataViewHolder) viewHolder).txtLongMsg.setMaxLines(1);

            }

            ((DataViewHolder) viewHolder).txtFileName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (entity.getFiles() != null && entity.getFiles().size() > 0) {
                        context.startActivity(new Intent(context, WebActivity.class)
                                .putExtra(WebActivity.URL, AppApi.BASE_URL + entity.getFiles().get(0).getFilePath()));
                    }


                }
            });


            ((DataViewHolder) viewHolder).txtViewMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    entity.setExpanded(!entity.isExpanded());
                    makeOtherCollapsed(viewHolder.getAdapterPosition());

                }
            });

            ((DataViewHolder) viewHolder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, AppointmentDetailActivity.class);
                    intent.putExtra(AppointmentDetailActivity.TYPE, AppointmentDetailActivity.PAST);
                    intent.putExtra(AppointmentDetailActivity.SCHEDULED_OBJECT, entity);
                    context.startActivity(intent);

                }
            });
        }else if (viewHolder instanceof HeaderViewHolder) {

            ((HeaderViewHolder) viewHolder).txtMonth.setText(entity.getMonth());
        }

    }


    private void makeOtherCollapsed(int position) {

        for (int i = 0; i < mItemList.size(); i++) {

            if (i != position) {
                mItemList.get(i).setExpanded(false);
            }
        }
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }


    public void addItem(List<TeacherAppointmentModel.ResultBean> productDetails) {

        mItemList.addAll(productDetails);
        notifyDataSetChanged();
    }


    public void addSingleItem(TeacherAppointmentModel.ResultBean productDetails) {
        mItemList.add(productDetails);
        notifyDataSetChanged();
    }

    public void clear() {
        mItemList.clear();
        notifyDataSetChanged();
    }


    class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_show_date)
        TextView txtShowDate;
        @BindView(R.id.show_month)
        TextView showMonth;
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.txtsubject)
        TextView txtsubject;
        @BindView(R.id.txtTeacher)
        TextView txtTeacher;
        @BindView(R.id.txtStudent)
        TextView txtStudent;
        View itemView;
        @BindView(R.id.txt_file_name)
        TextView txtFileName;
        @BindView(R.id.recyclerView)
        RecyclerView recyclerView;
        @BindView(R.id.txt_view_more)
        TextView txtViewMore;

        // R.layout.scheduled_list_item

        DataViewHolder(View parent) {
            super(parent);
            ButterKnife.bind(this, parent);
            itemView = parent;
        }

    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_month)
        TextView txtMonth;

        HeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}

