/*
 * Copyright (c) 2017.                                                       
 *                                                                           
 * ***********************************************************************  
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                           
 * ________________________________________________________________________  
 *                                                                           
 *    [2010] - [2018] SripathiSolutions Corp.                                
 *    [2010] - [2018] DataReady Technology Corp.                             
 *   ________________________________________________________________________
 *                                                                           
 *    All Rights Reserved.                                                   
 *                                                                           
 *   NOTICE:  All information contained herein is, and remains               
 *   the property of DataReady Technology  and its suppliers,                
 *   if any.  The intellectual and technical concepts contained              
 *   herein are proprietary to DataReady Technology Incorporated             
 *   and its suppliers and may be covered by U.S. and Foreign Patents,       
 *   patents in process, and are protected by trade secret or copyright law. 
 *   Dissemination of this information or reproduction of this material      
 *   is strictly forbidden unless prior written permission is obtained       
 *   from DataReady Technology Incorporated.                                 
 *                                                                           
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                            
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.appointment.details;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.DateFunction;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.appointment.adapter.StudentAdapter;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.AppointmentByIdResponse;
import ca.dataready.SmartSchoolAdmin.server.AppointmentConfirmResponse;
import ca.dataready.SmartSchoolAdmin.server.TeacherAppointmentModel;
import ca.dataready.SmartSchoolAdmin.viewresponse.adapter.FilesAdapter;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.FileModel;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.StudentListModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppointmentDetailActivity extends BaseActivity {

    public static final String SCHEDULED_DATE = "date";
    public static final String APPOINMENT_ID = "appointment_id";
    public static final String UPCOMING = "upcoming";
    public static final String PAST = "past";
    public static final String TYPE = "type";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txt_title_value)
    TextView txtTitleValue;
    @BindView(R.id.txt_date_value)
    TextView txtDateValue;
    @BindView(R.id.txt_time_value)
    TextView txtTimeValue;
    @BindView(R.id.txt_confirm_value)
    TextView txtConfirmValue;
    public static final String SCHEDULED_OBJECT = "object";
    TeacherAppointmentModel.ResultBean entity;
    StudentAdapter studentListForHWAdapter;
    @BindView(R.id.rv_students)
    RecyclerView rvStudentsList;
    @BindView(R.id.parent_viewAnimator)
    ViewAnimator parentViewAnimator;
    /* @BindView(R.id.toolbar)
     CenterTitleToolbar toolbar;*/
    @BindView(R.id.rvAttachments)
    RecyclerView rvAttachments;
    @BindView(R.id.llAttachments)
    LinearLayout llAttachments;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;
    @BindView(R.id.llButtons)
    LinearLayout llButtons;
    private FilesAdapter filesAdapter;
    private List<FileModel> selectedFiles = new ArrayList<>();
    private ProgressDialog progressDialog;
    private String type, appointmentId;
    private Call<AppointmentByIdResponse> appointmentByIdcall;
    private AppointmentByIdResponse.ResultBean model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_detail);
        ButterKnife.bind(this);
        setHeaderInfo();

        if (getIntent().getExtras() != null) {

            entity = getIntent().getParcelableExtra(SCHEDULED_OBJECT);
            type = getIntent().getStringExtra(TYPE);
            appointmentId = getIntent().getStringExtra(APPOINMENT_ID);

            if (appointmentId != null) {
                getAppointmentById();
            } else if (entity != null) {
                appointmentId = entity.getAppointmentId();
                setValues(entity);
            }
        }
    }

    private void setHeaderInfo() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setElevation(5);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + getString(R.string.appointment_details) + "</small>"));
        getSupportActionBar().setTitle(AdminApp.getInstance().getAdmin().getSchoolName()+ " " + AdminApp.getInstance().getAdmin().getSchoolYear());

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    private void setValues(final TeacherAppointmentModel.ResultBean entity) {

        txtTitleValue.setText(entity.getAppointmentTitle());
        Log.e("DATA", entity.getAppointmentDate());

        llButtons.setVisibility(type != null && type.equalsIgnoreCase(UPCOMING) && entity.getStatus().equals("initiated") && entity.getFrom() != null
                && !entity.getFrom().equals(AdminApp.getInstance().getAdmin().getEmailId()) ? View.VISIBLE : View.GONE);

        txtTimeValue.setText(entity.getAppointmentTime());
        txtConfirmValue.setText(entity.getStatus());
        String formattedDate = DateFunction.ConvertDate(entity.getAppointmentDate(), "yyyy-MM-dd", "dd MMM yyyy");
        txtDateValue.setText(formattedDate);
        rvStudentsList.setLayoutManager(new LinearLayoutManager(AppointmentDetailActivity.this));


        if (entity.getFiles() != null && entity.getFiles().size() > 0) {

            rvAttachments.setLayoutManager(new LinearLayoutManager(AppointmentDetailActivity.this));
            filesAdapter = new FilesAdapter(AppointmentDetailActivity.this, true);
            rvAttachments.setAdapter(filesAdapter);
            selectedFiles.clear();
            if (entity.getFiles() != null && entity.getFiles().size() > 0) {
                for (TeacherAppointmentModel.ResultBean.FilesBean model : entity.getFiles()) {
                    if (model.getFilePath().contains("/")) {
                        selectedFiles.add(new FileModel(model.getFilePath().substring(model.getFilePath().lastIndexOf("/") + 1), model.getFilePath()));
                    }
                }
            }
            filesAdapter.addItem(selectedFiles);

        } else {

            llAttachments.setVisibility(View.GONE);
        }


        Call<StudentListModel> studentList = AdminApp.getInstance().getApi().getStudentList(AdminApp.getInstance().getAdmin().getSchoolId(), entity.getGradeId(),
                entity.getClassId(), AdminApp.getInstance().getAdmin().getSchoolYear());
        studentList.enqueue(new Callback<StudentListModel>() {
            @Override
            public void onResponse(Call<StudentListModel> call, Response<StudentListModel> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            studentListForHWAdapter = new StudentAdapter(AppointmentDetailActivity.this);
                            rvStudentsList.setAdapter(studentListForHWAdapter);

                            if (response.body().getResult() != null && response.body().getResult().size() > 0) {

                                List<StudentListModel.ResultBean> filteredStudentList = new ArrayList<StudentListModel.ResultBean>();
                                for (StudentListModel.ResultBean bean : response.body().getResult()) {

                                    if (entity.getStudentList().contains(bean.getStudentId())) {

                                        filteredStudentList.add(bean);
                                    }
                                }

                                studentListForHWAdapter.addItem(filteredStudentList);
                                parentViewAnimator.setDisplayedChild(1);

                            } else {
                                parentViewAnimator.setDisplayedChild(1);
                            }

                        } else {
                            parentViewAnimator.setDisplayedChild(1);
                        }

                    } else {
                        parentViewAnimator.setDisplayedChild(1);
                        APIError error = APIError.parseError(response, AppointmentDetailActivity.this, App_Constants.API_STUDENT_LIST);
                        Toast.makeText(AppointmentDetailActivity.this, error.message(), Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception e) {
                    parentViewAnimator.setDisplayedChild(1);
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<StudentListModel> call, Throwable t) {
                if (!call.isCanceled()) {
                    parentViewAnimator.setDisplayedChild(1);
                    // viewAnimator.setDisplayedChild(2);
                    // txtNoDataFound.setText(App_Constants.NO_INTERNET);
                    System.out.println(t.getMessage());
                }
            }
        });

    }


    @Override
    public void onActivityResult(final int requestCode, int resultCode, Intent data) {
        if (requestCode == APIError.UPDATE_TOKEN) {
            if (resultCode == RESULT_OK) {

                if (data != null) {
                    String apiName = data.getStringExtra("api");
                    onReload(apiName);
                }
            }

        }

    }

    private void onReload(String apiName) {

        Log.e("OnReload", "Add Schedule");

        switch (apiName) {

            case App_Constants.API_STUDENT_LIST:
                setValues(entity);
                break;

        }
    }

    @OnClick({R.id.btn_cancel, R.id.btn_confirm})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_cancel:
                cancelAppointment();
                break;
            case R.id.btn_confirm:
                confirmAppointment();
                break;
        }
    }

    private void confirmAppointment() {

        showProgress();

        Call<AppointmentConfirmResponse> cancelCall = AdminApp.getInstance().getApi().confirmAppointment(appointmentId);

        cancelCall.enqueue(new Callback<AppointmentConfirmResponse>() {
            @Override
            public void onResponse(Call<AppointmentConfirmResponse> call, Response<AppointmentConfirmResponse> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            AdminApp.getInstance().addEventToDeviceCalander(entity);
                           // entity.setStatus("confirmed");
                            llButtons.setVisibility(View.GONE);
                            txtConfirmValue.setText(getString(R.string.confirmed));
                            Utility.showSnackBar(parentViewAnimator, response.body().getMessage());


                        } else {

                            Utility.showSnackBar(parentViewAnimator, response.body().getMessage());
                        }
                    } else {

                        APIError error = APIError.parseError(response, AppointmentDetailActivity.this, App_Constants.API_CANCEL_APPOINTMENT);
                        Utility.showSnackBar(parentViewAnimator, error.message());
                    }
                } catch (Exception e) {

                    Utility.showSnackBar(parentViewAnimator, getString(R.string.somethingwrong));
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<AppointmentConfirmResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    hideProgress();
                    Utility.showSnackBar(parentViewAnimator, App_Constants.NO_INTERNET);
                }
            }
        });
    }


    private void cancelAppointment() {

        showProgress();

        Call<AppointmentConfirmResponse> cancelCall = AdminApp.getInstance().getApi().cancelAppointment(appointmentId);

        cancelCall.enqueue(new Callback<AppointmentConfirmResponse>() {
            @Override
            public void onResponse(Call<AppointmentConfirmResponse> call, Response<AppointmentConfirmResponse> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                           // entity.setStatus("cancelled");
                            txtConfirmValue.setText(getString(R.string.cancelled));
                            llButtons.setVisibility(View.GONE);
                            Utility.showSnackBar(parentViewAnimator, response.body().getMessage());

                        } else {

                            Utility.showSnackBar(parentViewAnimator, response.body().getMessage());
                        }
                    } else {

                        APIError error = APIError.parseError(response, AppointmentDetailActivity.this, App_Constants.API_CANCEL_APPOINTMENT);
                        Utility.showSnackBar(parentViewAnimator, error.message());
                    }
                } catch (Exception e) {

                    Utility.showSnackBar(parentViewAnimator, getString(R.string.somethingwrong));
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<AppointmentConfirmResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    hideProgress();
                    Utility.showSnackBar(parentViewAnimator, App_Constants.NO_INTERNET);
                }
            }
        });
    }


    private void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    private void showProgress() {
        progressDialog = new ProgressDialog(AppointmentDetailActivity.this);
        progressDialog.setMessage(getString(R.string.processing));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }


    private void getAppointmentById() {

        appointmentByIdcall = AdminApp.getInstance().getApi().getAppointmentById(appointmentId);

        appointmentByIdcall.enqueue(new Callback<AppointmentByIdResponse>() {
            @Override
            public void onResponse(Call<AppointmentByIdResponse> call, Response<AppointmentByIdResponse> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            model = response.body().getResult();
                            setValuesFromAppointmentId(model);
                        } else {
                            Toast.makeText(AppointmentDetailActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        APIError error = APIError.parseError(response, AppointmentDetailActivity.this, App_Constants.API_APPOINTMENT_BY_ID);
                        Toast.makeText(AppointmentDetailActivity.this, "" + error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {

                    Toast.makeText(AppointmentDetailActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<AppointmentByIdResponse> call, Throwable t) {

                if (!call.isCanceled()) {

                    Toast.makeText(AppointmentDetailActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void setValuesFromAppointmentId(final AppointmentByIdResponse.ResultBean entity) {


        txtTitleValue.setText(entity.getAppointmentTitle());
        Log.e("DATA", entity.getAppointmentDate());

        llButtons.setVisibility(type != null && type.equalsIgnoreCase(UPCOMING) && entity.getStatus().equals("initiated") && entity.getFrom() != null
                && !entity.getFrom().equals(AdminApp.getInstance().getAdmin().getEmailId()) ? View.VISIBLE : View.GONE);

        txtTimeValue.setText(entity.getAppointmentTime());
        txtConfirmValue.setText(entity.getStatus());
        String formattedDate = DateFunction.ConvertDate(entity.getAppointmentDate(), "yyyy-MM-dd", "dd MMM yyyy");
        txtDateValue.setText(formattedDate);
        rvStudentsList.setLayoutManager(new LinearLayoutManager(AppointmentDetailActivity.this));


        if (entity.getFiles() != null && entity.getFiles().size() > 0) {

            rvAttachments.setLayoutManager(new LinearLayoutManager(AppointmentDetailActivity.this));
            filesAdapter = new FilesAdapter(AppointmentDetailActivity.this, true);
            rvAttachments.setAdapter(filesAdapter);
            selectedFiles.clear();
            if (entity.getFiles() != null && entity.getFiles().size() > 0) {
                for (AppointmentByIdResponse.ResultBean.FilesBean model : entity.getFiles()) {
                    if (model.getFilePath().contains("/")) {
                        selectedFiles.add(new FileModel(model.getFilePath().substring(model.getFilePath().lastIndexOf("/") + 1), model.getFilePath()));
                    }
                }
            }
            filesAdapter.addItem(selectedFiles);

        } else {

            llAttachments.setVisibility(View.GONE);
        }


        Call<StudentListModel> studentList = AdminApp.getInstance().getApi().getStudentList(AdminApp.getInstance().getAdmin().getSchoolId(), entity.getGradeId(),
                entity.getClassId(), AdminApp.getInstance().getAdmin().getSchoolYear());
        studentList.enqueue(new Callback<StudentListModel>() {
            @Override
            public void onResponse(Call<StudentListModel> call, Response<StudentListModel> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            studentListForHWAdapter = new StudentAdapter(AppointmentDetailActivity.this);
                            rvStudentsList.setAdapter(studentListForHWAdapter);

                            if (response.body().getResult() != null && response.body().getResult().size() > 0) {

                                List<StudentListModel.ResultBean> filteredStudentList = new ArrayList<StudentListModel.ResultBean>();
                                for (StudentListModel.ResultBean bean : response.body().getResult()) {

                                    if (entity.getStudentList().contains(bean.getStudentId())) {

                                        filteredStudentList.add(bean);
                                    }
                                }

                                studentListForHWAdapter.addItem(filteredStudentList);
                                parentViewAnimator.setDisplayedChild(1);

                            } else {
                                parentViewAnimator.setDisplayedChild(1);
                            }

                        } else {
                            parentViewAnimator.setDisplayedChild(1);
                        }

                    } else {
                        parentViewAnimator.setDisplayedChild(1);
                        APIError error = APIError.parseError(response, AppointmentDetailActivity.this, App_Constants.API_STUDENT_LIST);
                        Toast.makeText(AppointmentDetailActivity.this, error.message(), Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception e) {
                    parentViewAnimator.setDisplayedChild(1);
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<StudentListModel> call, Throwable t) {
                if (!call.isCanceled()) {
                    parentViewAnimator.setDisplayedChild(1);
                    // viewAnimator.setDisplayedChild(2);
                    // txtNoDataFound.setText(App_Constants.NO_INTERNET);
                    System.out.println(t.getMessage());
                }
            }
        });


    }

    @Override
    public void onBackPressed() {

        if (appointmentId != null) {
            Intent intent = new Intent(App_Constants.UPDATE_FRAGEMENT_BROADCAST);
            intent.putExtra(App_Constants.TYPE, App_Constants.APPOINTMENT);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }
        super.onBackPressed();

    }
}
