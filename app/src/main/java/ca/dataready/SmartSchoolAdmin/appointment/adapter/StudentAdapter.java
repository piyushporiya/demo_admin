/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.appointment.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.server.AppApi;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.StudentListModel;


/**
 * Created by social_jaydeep on 16/10/17.
 */

public class StudentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<StudentListModel.ResultBean> beans;

    public StudentAdapter(Context context) {
        this.context = context;
        beans = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DataViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_student_list, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        StudentListModel.ResultBean bean = beans.get(position);

        if (holder instanceof DataViewHolder) {

            ((DataViewHolder) holder).myImageView.setImageURI(Uri.parse(AppApi.BASE_URL + bean.getProfilePic()));
            ((DataViewHolder) holder).txtStudentId.setText(bean.getStudentId());
            ((DataViewHolder) holder).txtStudentName.setText(bean.getFirstName() + " " + bean.getLastName());

        }

    }

    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }

    public void addItem(List<StudentListModel.ResultBean> result) {

        beans.addAll(result);
        notifyDataSetChanged();
    }

    class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.my_image_view)
        SimpleDraweeView myImageView;
        @BindView(R.id.txt_StudentName)
        TextView txtStudentName;
        @BindView(R.id.txt_StudentId)
        TextView txtStudentId;
        //R.layout.raw_hw_student_list

        public DataViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
