/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.appointment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.alamkanak.weekview.DateTimeInterpreter;
import com.alamkanak.weekview.MonthLoader;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.joda.time.Minutes;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.Listeners.OnAppointmentConfirmationListener;
import ca.dataready.SmartSchoolAdmin.Listeners.OnEventActionListener;
import ca.dataready.SmartSchoolAdmin.Listeners.OnReloadListener;
import ca.dataready.SmartSchoolAdmin.Listeners.OnStudentSelectedListener;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.DateFunction;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.appointment.adapter.ScheduledAdapter;
import ca.dataready.SmartSchoolAdmin.appointment.adapter.UpcomingAdapter;
import ca.dataready.SmartSchoolAdmin.appointment.details.AppointmentDetailActivity;
import ca.dataready.SmartSchoolAdmin.calendarview.CalendarCustomView;
import ca.dataready.SmartSchoolAdmin.calendarview.callbacks.CompactCalendarViewListener;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.AddEventModel;
import ca.dataready.SmartSchoolAdmin.server.AppointmentByIdResponse;
import ca.dataready.SmartSchoolAdmin.server.AppointmentConfirmResponse;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.ClassActivityResponse;
import ca.dataready.SmartSchoolAdmin.server.SchoolActivityResponse;
import ca.dataready.SmartSchoolAdmin.server.TeacherAppointmentModel;
import ca.dataready.SmartSchoolAdmin.viewresponse.adapter.StudentListAdapter;
import ca.dataready.SmartSchoolAdmin.viewresponse.adapter.StudentListDialogAdapter;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.GradeList;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.StudentListModel;
import info.hoang8f.android.segmented.SegmentedGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AppointmentFragment extends Fragment implements OnReloadListener, CompactCalendarViewListener
        , OnAppointmentConfirmationListener, OnEventActionListener, MonthLoader.MonthChangeListener, OnStudentSelectedListener
        , WeekView.EventLongPressListener, WeekView.EmptyViewLongPressListener {

    @BindView(R.id.rb_schedule)
    RadioButton rbSchedule;
    @BindView(R.id.rb_upcoming)
    RadioButton rbUpcoming;
    @BindView(R.id.rb_past)
    RadioButton rbPast;
    @BindView(R.id.sgAppointment)
    SegmentedGroup sgAppointment;
    @BindView(R.id.calenderView)
    CalendarCustomView calenderView;
    /* @BindView(R.id.txt_Schedule)
     TextView txtSchedule;*/
   /* @BindView(R.id.rv_EventsForDay)
    RecyclerView rvEventsForDay;*/
   /* @BindView(R.id.txt_NoData)
    TextView txtNoData;*/

    @BindView(R.id.txt_no_upcoming_data)
    TextView txtNoUpcomingData;
    @BindView(R.id.upcoming_viewAnimator)
    ViewAnimator upcomingViewAnimator;
    @BindView(R.id.rv_past)
    RecyclerView rvPast;
    @BindView(R.id.txt_no_past_data)
    TextView txtNoPastData;
    @BindView(R.id.past_viewAnimator)
    ViewAnimator pastViewAnimator;
    @BindView(R.id.main_viewAnimator)
    ViewAnimator mainViewAnimator;
    @BindView(R.id.parent_viewAnimator)
    ViewAnimator parentViewAnimator;
    CREDENTIAL.ResultBean entity;
    Call<TeacherAppointmentModel> upcomingCall;
    Call<TeacherAppointmentModel> pastCall;
    Call<TeacherAppointmentModel> allCall;
    Call<AppointmentConfirmResponse> confirmCall;
    Call<AppointmentConfirmResponse> cancelCall;
    ScheduledAdapter scheduledAdapter;
    UpcomingAdapter upcomingAdapter;
    String time, currentTime;
    ArrayList<TeacherAppointmentModel.ResultBean> results;
    String selectedDate;
    boolean isActionButtonsVisible;
    ProgressDialog progressDialog;
    @BindView(R.id.swipeRefreshLayoutPast)
    SwipeRefreshLayout swipeRefreshLayoutPast;
    @BindView(R.id.swipeRefreshLayoutUpcoming)
    SwipeRefreshLayout swipeRefreshLayoutUpcoming;
    @BindView(R.id.weekView)
    WeekView mWeekView;
    @BindView(R.id.edittext)
    EditText edittext;
    @BindView(R.id.txt_grade)
    TextView txtGrade;
    @BindView(R.id.grade_img)
    ImageView gradeImg;
    @BindView(R.id.linear_grade)
    RelativeLayout linearGrade;
    @BindView(R.id.txt_class)
    TextView txtClass;
    @BindView(R.id.class_img)
    ImageView classImg;
    @BindView(R.id.linear_class)
    RelativeLayout linearClass;
    @BindView(R.id.txt_time)
    TextView txtTime;
    @BindView(R.id.time_img)
    ImageView timeImg;
    @BindView(R.id.linear_Time)
    RelativeLayout linearTime;
    @BindView(R.id.rv_Students)
    RecyclerView rvStudents;
    @BindView(R.id.btn_Cancel)
    Button btnCancel;
    @BindView(R.id.btn_Assign)
    Button btnAssign;
    @BindView(R.id.rv_upcoming)
    RecyclerView rvUpcoming;
    @BindView(R.id.img_edit)
    ImageView imgEdit;
    @BindView(R.id.rl_edit)
    RelativeLayout rlEdit;
    @BindView(R.id.linear_area)
    LinearLayout linearArea;
    @BindView(R.id.txt_duartion)
    TextView txtDuartion;
    @BindView(R.id.duration_img)
    ImageView durationImg;
    @BindView(R.id.linear_duartion)
    RelativeLayout linearDuartion;
    @BindView(R.id.fab_add)
    FloatingActionButton fabAdd;
    @BindView(R.id.schedule_viewAnimator)
    ViewAnimator scheduleViewAnimator;
    @BindView(R.id.schedule_inner_viewAnimator)
    ViewAnimator scheduleInnerViewAnimator;
    TeacherAppointmentModel.ResultBean Appointmententity;
    String appointmentId;
    int adapterPos;
    List<TeacherAppointmentModel.ResultBean> offlineModel;
    private List<String> headers = new ArrayList<>();
    ArrayList<TeacherAppointmentModel.ResultBean> allUpcomingResult = new ArrayList<>();

    String aId;
    private Call<GradeList> call;
    private ArrayList<GradeList.ResultBean.GradeBean> teacherBeans;
    public static final String GRADE = "grade";
    public static final String CLASS = "class";
    public static final String TIME = "time";
    public static final String DURATION = "duration";
    ArrayList<String> gradeList = new ArrayList<>();
    ArrayList<String> classList = new ArrayList<>();
    ArrayList<String> timeList = new ArrayList<>();
    ArrayList<String> durationList = new ArrayList<>();
    ArrayList<String> subjectList = new ArrayList<>();
    private ListPopupWindow listPopupWindow;
    private List<String> timeSlotList;
    private AlertDialog alertDialog;
    private Call<StudentListModel> studentListCall;
    private StudentListDialogAdapter studentListDialogAdapter;
    List<String> studentIdList = new ArrayList<>();
    private StudentListAdapter studentListAdapter;
    private List<StudentListModel.ResultBean> studentListBeans;
    private String displayTime;
    private Call<AddEventModel> AddEventCall;
    private String selectedTime;
    private Call<AppointmentByIdResponse> appointmentByIdcall;
    private String appoId;
    private AppointmentByIdResponse.ResultBean appointmentByIdModel;
    private Call<AddEventModel> updateEventCall;
    private String endTime;
    private ArrayList<TeacherAppointmentModel.ResultBean> pastResults;
    private ArrayList<TeacherAppointmentModel.ResultBean> upcomingResults;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_appointment2, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) getActivity()).setSubTitle(getString(R.string.appointment));
        ((HomeActivity) getActivity()).setOnReloadListener(this);

        rvStudents.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvUpcoming.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvPast.setLayoutManager(new LinearLayoutManager(getActivity()));

        calenderView.setListener(this);

        entity = AdminApp.getInstance().getAdmin();
        Init();


        if (savedInstanceState != null) {

            pastResults = savedInstanceState.getParcelableArrayList(App_Constants.PAST_APPOINTMENT_LIST);
            upcomingResults = savedInstanceState.getParcelableArrayList(App_Constants.UPCOMING_APPOINTMENT_LIST);
            results = savedInstanceState.getParcelableArrayList(App_Constants.ALL_APPOINTMENT_LIST);
            int checkedItem = savedInstanceState.getInt(App_Constants.CHECKED_ITEM);

            if (checkedItem == R.id.rb_upcoming) {
                rbUpcoming.setChecked(true);
            } else if (checkedItem == R.id.rb_past) {
                rbPast.setChecked(true);
            }

            if (sgAppointment.getCheckedRadioButtonId() == R.id.rb_schedule) {

                parentViewAnimator.setDisplayedChild(1);
                mainViewAnimator.setDisplayedChild(0);
                scheduleViewAnimator.setDisplayedChild(1);

                if (results != null && results.size() > 0) {

                    List<TeacherAppointmentModel.ResultBean> finalResults = new ArrayList<>();
                    for (TeacherAppointmentModel.ResultBean info : results) {
                        if (info.getAppointmentTitle() != null && !info.getAppointmentTitle().isEmpty() && !info.getStatus().equals("cancelled")
                                && info.getFrom() != null) {
                            finalResults.add(info);
                        }
                    }
                    if (calenderView != null) {
                        calenderView.setEventList(finalResults);
                    }

                    String date = AdminApp.getInstance().currentDate();
                    String[] dateArr = date.split("-");
                    int day = Integer.parseInt(dateArr[2]);
                    int month = Integer.parseInt(dateArr[1]);
                    int year = Integer.parseInt(dateArr[0]);

                    onMonthChange(year, month);
                }


            } else if (sgAppointment.getCheckedRadioButtonId() == R.id.rb_upcoming) {

                parentViewAnimator.setDisplayedChild(1);
                mainViewAnimator.setDisplayedChild(1);

                upcomingAdapter = new UpcomingAdapter(getActivity());
                rvUpcoming.setAdapter(upcomingAdapter);
                upcomingAdapter.setOnAppointmentConfirmationListener(AppointmentFragment.this);

                if (upcomingResults != null && upcomingResults.size() > 0) {
                    upcomingViewAnimator.setDisplayedChild(1);
                    upcomingAdapter.addItem(AppointmentFragment.this.upcomingResults);
                } else {
                    upcomingViewAnimator.setDisplayedChild(2);
                }

            } else if (sgAppointment.getCheckedRadioButtonId() == R.id.rb_past) {

                parentViewAnimator.setDisplayedChild(1);
                mainViewAnimator.setDisplayedChild(2);

                scheduledAdapter = new ScheduledAdapter(getActivity());
                rvPast.setAdapter(scheduledAdapter);

                if (pastResults != null && pastResults.size() > 0) {
                    pastViewAnimator.setDisplayedChild(1);
                    scheduledAdapter.addItem(AppointmentFragment.this.pastResults);
                } else {
                    pastViewAnimator.setDisplayedChild(2);
                }
            }

        } else {

            Bundle bundle = this.getArguments();
            if (bundle != null) {
                aId = bundle.getString(App_Constants.APPOINTMENT_ID);
                rbUpcoming.performClick();
            } else {
                getTeacherClassSchedule();

            }
        }
    }

    private void Init() {


        mWeekView.setNumberOfVisibleDays(1);
        // Lets change some dimensions to best fit the view.
        mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
        mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
        mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
        mWeekView.setHourHeight(600);


        swipeRefreshLayoutUpcoming.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (isVisible()) {

                    if (entity != null) {
                        getUpCommingAppointment();
                    } else {
                        upcomingViewAnimator.setDisplayedChild(2);
                    }

                    swipeRefreshLayoutUpcoming.setRefreshing(false);
                }

            }
        });

        swipeRefreshLayoutPast.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (isVisible()) {
                    if (entity != null) {
                        getPastAppointment();
                    } else {
                        pastViewAnimator.setDisplayedChild(2);
                    }
                }

                swipeRefreshLayoutPast.setRefreshing(false);
            }
        });


       /* sgAppointment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {

                if (i == R.id.rb_schedule) {

                    InitScheduleAppointent();

                } else if (i == R.id.rb_upcoming) {

                    InitUpcomingAppointment();

                } else if (i == R.id.rb_past) {

                    InitPastAppointment();
                }
            }

        });*/

        rbSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (rbSchedule.isChecked())
                    InitScheduleAppointent();
            }
        });

        rbUpcoming.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (rbUpcoming.isChecked())
                    InitUpcomingAppointment();
            }
        });

        rbPast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (rbPast.isChecked())
                    InitPastAppointment();
            }
        });


        mWeekView.setMonthChangeListener(this);
        //  mWeekView.setStartNEndTime(0, 5);

        // Set long press listener for events.
        mWeekView.setEventLongPressListener(this);

        // Set long press listener for empty view
        mWeekView.setEmptyViewLongPressListener(this);

        setupDateTimeInterpreter(false);
    }

    private void getTeacherClassSchedule() {

        call = AdminApp.getInstance().getApi().getClassSchedule(entity.getSchoolId(), entity.getSchoolYear());
        call.enqueue(new Callback<GradeList>() {
            @Override
            public void onResponse(Call<GradeList> call, Response<GradeList> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            teacherBeans = response.body().getResult().getGrade();

                        } else {

                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }

                        //getTeacherMeetingTimeSlots(null);
                        InitScheduleAppointent();

                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_TEACHER_CLASS_SCHEDULE);
                        Toast.makeText(getActivity(), error.message(), Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception e) {

                    Toast.makeText(getActivity(), getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<GradeList> call, Throwable t) {
                if (!call.isCanceled()) {
                    Toast.makeText(getActivity(), App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                    scheduleViewAnimator.setDisplayedChild(1);
                }
            }
        });

    }


    @OnClick({R.id.linear_grade, R.id.linear_class, R.id.linear_Time, R.id.btn_Assign, R.id.img_edit, R.id.btn_Cancel, R.id.fab_add, R.id.linear_duartion})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linear_grade:

                show_drop_down_to_select_option(linearGrade, GRADE);

                break;
            case R.id.linear_class:

                show_drop_down_to_select_option(linearClass, CLASS);

                break;
            case R.id.linear_Time:

                //show_drop_down_to_select_option(linearTime, TIME);
                setTime();

                break;

            case R.id.linear_duartion:

                if (txtTime.getText().toString().equalsIgnoreCase(getString(R.string.select_time_label))) {
                    Utility.showSnackBar(rvStudents, getString(R.string.please_select_time));
                    return;
                }
                show_drop_down_to_select_option(linearDuartion, DURATION);

                break;

            case R.id.fab_add:

                if (selectedDate != null) {
                    int compareValue = DateFunction.compareDateWithTodayDate(selectedDate);
                    if (compareValue >= 0) {

                        fabAdd.setVisibility(fabAdd.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                        linearArea.setVisibility(linearArea.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                    } else {

                        Toast.makeText(getActivity(), getString(R.string.select_future_date), Toast.LENGTH_SHORT).show();
                    }
                }


                break;

            case R.id.btn_Assign:

                if (txtGrade.getText().toString().equalsIgnoreCase(getString(R.string.select_grade_label))) {
                    Utility.showSnackBar(rvStudents, getString(R.string.please_select_grade));
                } else if (txtClass.getText().toString().equalsIgnoreCase(getString(R.string.select_class_label))) {
                    Utility.showSnackBar(rvStudents, getString(R.string.please_select_class));
                } else if (txtTime.getText().toString().equalsIgnoreCase(getString(R.string.select_time_label))) {
                    Utility.showSnackBar(rvStudents, getString(R.string.please_select_time));
                } else if (txtDuartion.getText().toString().equalsIgnoreCase(getString(R.string.select_duration_label))) {
                    Utility.showSnackBar(rvStudents, getString(R.string.please_select_duration));
                } else if (edittext.getText().toString().trim().equals("")) {
                    edittext.setError(getString(R.string.enter_title));
                } else if (studentIdList.size() == 0) {
                    Utility.showSnackBar(rvStudents, getString(R.string.select_student));
                } else {

                    if (btnAssign.getText().toString().equalsIgnoreCase(getString(R.string.update)))
                        UpdateEvent();
                    else
                        AddEvent();
                }

                break;

            case R.id.img_edit:

                getStudentListDialog();

                break;

            case R.id.btn_Cancel:

                clearSelections();

                break;

        }
    }


    public void setTime() {

        Calendar myCalendar = Calendar.getInstance();
        int hour = myCalendar.get(Calendar.HOUR_OF_DAY);
        int minute = myCalendar.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getActivity(), R.style.datepickerCustom, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                String AM_PM;
                if (selectedHour < 12) {
                    AM_PM = "AM";
                    selectedHour = selectedHour % 12;
                    if (selectedHour == 0) {
                        selectedHour = 12;
                    }
                } else {
                    AM_PM = "PM";
                    selectedHour = selectedHour % 12;
                    if (selectedHour == 0) {
                        selectedHour = 12;
                    }
                }

                if (selectedHour < 10 && selectedMinute < 10) {
                    txtTime.setText(String.valueOf("0" + selectedHour) + ":" + String.valueOf("0" + selectedMinute) + " " + AM_PM);
                } else if (selectedHour < 10 || selectedMinute < 10) {
                    if (selectedHour < 10) {
                        txtTime.setText(String.valueOf("0" + selectedHour) + ":" + String.valueOf(selectedMinute) + " " + AM_PM);
                    } else {
                        txtTime.setText(String.valueOf(selectedHour) + ":" + String.valueOf("0" + selectedMinute) + " " + AM_PM);
                    }
                } else {
                    txtTime.setText(String.valueOf(selectedHour) + ":" + String.valueOf(selectedMinute) + " " + AM_PM);
                }

                if (!txtDuartion.getText().toString().equalsIgnoreCase(getString(R.string.select_duration_label))) {

                    DateTimeFormatter formatter = DateTimeFormat.forPattern("hh:mm a");
                    LocalTime time = formatter.parseLocalTime(txtTime.getText().toString());
                    time = time.plusMinutes(Integer.parseInt(txtDuartion.getText().toString().split(" ")[0].trim()));
                    System.out.println(formatter.print(time));
                    endTime = formatter.print(time);
                }

            }
        }, hour, minute, false);//24 hour time
        mTimePicker.setTitle(getString(R.string.select_time_label));
        mTimePicker.show();
    }

    private void AddEvent() {

        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTime SelectedDate = fmt.parseDateTime(selectedDate);
        DateTime CurrentDate = fmt.parseDateTime(AdminApp.getInstance().currentDate());

        if (SelectedDate.isBefore(CurrentDate)) {

            Utility.showSnackBar(rvStudents, getString(R.string.select_future_date));
            return;
        }

        String startTime = txtTime.getText().toString();

        String sTime = "08:00 AM";
        String eTime = "05:00 PM";


        DateTimeFormatter dtf = DateTimeFormat.forPattern("hh:mm a");
        DateTimeFormatter locale = dtf.withLocale(Locale.getDefault());

        LocalTime start = LocalTime.parse(sTime, locale);
        LocalTime stop = LocalTime.parse(eTime, locale);
        LocalTime target = LocalTime.parse(startTime, locale);

        Boolean isTargetAfterStartAndBeforeStop = ((target.isAfter(start) && target.isBefore(stop))
                || target.isEqual(start) || target.isEqual(stop));

        if (isTargetAfterStartAndBeforeStop) {

            SimpleDateFormat sdf4 = new SimpleDateFormat("hh:mm a");
            String currentDateandTime = sdf4.format(new Date());

            LocalTime currenTime = LocalTime.parse(currentDateandTime, locale);
            LocalTime select = LocalTime.parse(startTime, locale);

            if (SelectedDate.isEqual(CurrentDate)) {

                if (currenTime.isAfter(select)) {
                    Toast.makeText(getActivity(), getString(R.string.select_future_time), Toast.LENGTH_SHORT).show();
                    return;
                }
            }


            if (results != null) {

                List<String> startTimeSlots = new ArrayList<>();

                for (TeacherAppointmentModel.ResultBean bean : results) {

                    if (bean.getAppointmentDate() != null && bean.getAppointmentDate().equalsIgnoreCase(selectedDate)
                            && !bean.getStatus().equalsIgnoreCase(App_Constants.CANCELLED)) {
                        startTimeSlots.add(bean.getAppointmentTime());
                    }
                }

                for (String st : startTimeSlots) {

                    Log.e("------------", "" + st);

                    LocalTime first = LocalTime.parse(st.split("-")[0].trim(), locale);
                    LocalTime last = LocalTime.parse(st.split("-")[1].trim(), locale);

                    LocalTime startDestination = LocalTime.parse(startTime, locale);
                    LocalTime endDestination = LocalTime.parse(endTime, locale);

                    Log.e("-------first-----", "" + first.toString());
                    Log.e("-------last-----", "" + last.toString());

                    Log.e("--startDestination--", "" + startDestination.toString());
                    Log.e("--endDestination---", "" + endDestination.toString());

                    if (startDestination.isAfter(first) && startDestination.isBefore(last)) {
                        Utility.showSnackBar(rvStudents, getString(R.string.select_diffrent_time));
                        return;
                    }

                    if (endDestination.isAfter(first) && endDestination.isBefore(last)
                            || endDestination.isEqual(first) || endDestination.isEqual(last)) {
                        Utility.showSnackBar(rvStudents, getString(R.string.select_diffrent_duration));
                        return;
                    }
                }
            }

        } else {

            Utility.showSnackBar(rvStudents, getString(R.string.select_time_btw_8_to_5));
            return;
        }

        scheduleInnerViewAnimator.setDisplayedChild(1);
        // if (selectedFiles == null)
        //  Utility.showProgress(getActivity(), getString(R.string.processing));

        AddEventParams params = new AddEventParams();
        params.setAppointmentDate(selectedDate);
        params.setAppointmentTime(txtTime.getText().toString() + " - " + endTime);
        params.setGradeId(txtGrade.getText().toString());
        params.setClassId(txtClass.getText().toString());
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
        params.setSchoolYear(AdminApp.getInstance().getAdmin().getSchoolYear());
        List<String> sIds = new ArrayList<>();
        sIds.add(studentIdList.get(studentIdList.size() - 1));
        params.setStudentList(sIds);
        params.setFrom(AdminApp.getInstance().getAdmin().getId());
        params.setSubjectName("History");
        params.setTeacherEmailId(AdminApp.getInstance().getAdmin().getEmailId());
        params.setTeacherId(AdminApp.getInstance().getAdmin().getId());
        params.setTeacherMobileNo(AdminApp.getInstance().getAdmin().getPhoneNo());
        params.setTeacherName(AdminApp.getInstance().getAdmin().getFirstName() + " " + AdminApp.getInstance().getAdmin().getLastName());
        params.setAppointmentTitle(edittext.getText().toString());


        AddEventCall = AdminApp.getInstance().getApi().postEvent(params);
        AddEventCall.enqueue(new Callback<AddEventModel>() {
            @Override
            public void onResponse(Call<AddEventModel> call, Response<AddEventModel> response) {

                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            Utility.showToast(getActivity(), response.body().getMessage());
                            clearSelections();
                            if (sgAppointment.getCheckedRadioButtonId() == R.id.rb_schedule) {
                                if (entity != null) {
                                    getTeachersAllAppointment();
                                    scheduleInnerViewAnimator.setDisplayedChild(0);
                                } else {
                                    parentViewAnimator.setDisplayedChild(1);
                                    mainViewAnimator.setDisplayedChild(0);
                                }
                            }
                        } else {
                            Utility.showSnackBar(rvStudents, response.body().getMessage());
                        }

                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_ADD_EVENT);
                        Utility.error(getActivity(), error.message());

                    }
                } catch (Exception e) {

                    Utility.showSnackBar(rvStudents, getString(R.string.somethingwrong));
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<AddEventModel> call, Throwable t) {
                if (!call.isCanceled()) {
                    System.out.println(t.getMessage());
                    Utility.showSnackBar(rvStudents, App_Constants.NO_INTERNET);
                    Utility.hideProgress();
                }
            }
        });
    }

    private void clearSelections() {

        classList.clear();
        gradeList.clear();
        timeList.clear();
        subjectList.clear();
        durationList.clear();
        edittext.getText().clear();

        txtGrade.setText(getString(R.string.select_grade_label));
        txtTime.setText(getString(R.string.select_time_label));
        txtClass.setText(getString(R.string.select_class_label));
        txtDuartion.setText(getString(R.string.select_duration_label));
        studentListAdapter = null;
        rvStudents.setAdapter(new StudentListAdapter(getActivity()));

        btnAssign.setText(getString(R.string.create));


        fabAdd.setVisibility(fabAdd.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
        linearArea.setVisibility(linearArea.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

        rlEdit.setVisibility(View.GONE);

        linearGrade.setAlpha(1.0f);
        linearGrade.setEnabled(true);

        linearClass.setAlpha(1.0f);
        linearClass.setEnabled(true);

        linearTime.setAlpha(1.0f);
        linearTime.setEnabled(true);

        linearDuartion.setAlpha(1.0f);
        linearDuartion.setEnabled(true);

    }

    private boolean checkTime(String time, String endtime) {

        try {

            DateTimeFormatter fmt = DateTimeFormat.forPattern("hh:mm a");
            DateTime dateTime1 = fmt.parseDateTime(time);
            DateTime dateTime2 = fmt.parseDateTime(endtime);

            if (dateTime1.isBefore(dateTime2)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {

            e.printStackTrace();
        }


        return false;
    }

    private void show_drop_down_to_select_option(RelativeLayout layout, final String which) {

        if (teacherBeans != null && teacherBeans.size() > 0) {

            listPopupWindow = new ListPopupWindow(getActivity());
            if (which.equals(App_Constants.GRADE)) {

                gradeList.clear();
                for (GradeList.ResultBean.GradeBean beans : teacherBeans) {
                    if (!gradeList.contains(beans.getGradeId()))
                        gradeList.add(beans.getGradeId());
                }

                listPopupWindow.setAdapter(new ArrayAdapter(getActivity(), R.layout.list_dropdown_item, gradeList));

            } else if (which.equals(App_Constants.CLASS)) {

                classList.clear();
                for (GradeList.ResultBean.GradeBean beans : teacherBeans) {

                    if (txtGrade.getText().toString().equals(beans.getGradeId())) {
                        if (!classList.contains(beans.getClassX()))
                            for (int i = 0; i < beans.getClassX().size(); i++)
                                classList.add(beans.getClassX().get(i).getClassId());
                    }
                }

                if (classList != null && classList.size() > 0) {
                    Collections.sort(classList, new Comparator<String>() {
                        public int compare(String obj1, String obj2) {
                            // ## Ascending order
                            return obj1.compareToIgnoreCase(obj2); // To compare string values
                        }
                    });
                }

                listPopupWindow.setAdapter(new ArrayAdapter(getActivity(), R.layout.list_dropdown_item, classList));

            } else if (which.equals(DURATION)) {

                durationList.clear();
                durationList.add(getString(R.string.minute_5));
                durationList.add(getString(R.string.minute_10));
                durationList.add(getString(R.string.minute_15));
                durationList.add(getString(R.string.minute_20));
                durationList.add(getString(R.string.minute_25));
                durationList.add(getString(R.string.minute_30));
                durationList.add(getString(R.string.minute_35));
                durationList.add(getString(R.string.minute_40));
                durationList.add(getString(R.string.minute_45));
                durationList.add(getString(R.string.minute_50));
                durationList.add(getString(R.string.minute_55));
                durationList.add(getString(R.string.minute_60));

                listPopupWindow.setAdapter(new ArrayAdapter(getActivity(), R.layout.list_dropdown_item, durationList));
            }

            listPopupWindow.setAnchorView(layout);
            listPopupWindow.setWidth(ListPopupWindow.WRAP_CONTENT);
            listPopupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);

            listPopupWindow.setModal(true);
            listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    if (which.equals(App_Constants.GRADE)) {
                        txtGrade.setText(gradeList.get(i));
                        txtClass.setText(getString(R.string.select_class_label));
                    } else if (which.equals(App_Constants.CLASS)) {
                        txtClass.setText(classList.get(i));
                        getStudentListDialog();
                    } else if (which.equals(DURATION)) {

                        txtDuartion.setText(durationList.get(i));

                        DateTimeFormatter formatter = DateTimeFormat.forPattern("hh:mm a");
                        DateTimeFormatter locale = formatter.withLocale(Locale.getDefault());
                        LocalTime time = formatter.parseLocalTime(txtTime.getText().toString());
                        time = time.plusMinutes(Integer.parseInt(txtDuartion.getText().toString().split(" ")[0].trim()));
                        System.out.println(formatter.print(time));
                        endTime = formatter.print(time);
                    }
                    listPopupWindow.dismiss();

                }
            });
            listPopupWindow.show();
        }

    }


    private void getStudentListDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.raw_hw_student_list_dialog, null);
        builder.setView(view);
        final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        final ViewAnimator viewAnimator = (ViewAnimator) view.findViewById(R.id.viewAnimator);
        final TextView txtNoData = (TextView) view.findViewById(R.id.txt_no_data);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 7));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 4));
        }

        builder.setPositiveButton(getString(R.string.done), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();

                studentListAdapter = new StudentListAdapter(getActivity());
                rvStudents.setAdapter(studentListAdapter);

                if (studentListDialogAdapter != null) {

                    rlEdit.setVisibility(View.VISIBLE);
                    studentListAdapter.addItem(studentListDialogAdapter.getSelectedStudent());
                    //studentlistViewAnimator.setDisplayedChild(1);
                }

            }
        });

        builder.setNegativeButton(getString(R.string.close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();
            }
        });

        alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {

                studentListCall = AdminApp.getInstance().getApi().getStudentList(entity.getSchoolId(), txtGrade.getText().toString(),
                        txtClass.getText().toString().trim(), AdminApp.getInstance().getAdmin().getSchoolYear());
                studentListCall.enqueue(new Callback<StudentListModel>() {
                    @Override
                    public void onResponse(Call<StudentListModel> call, Response<StudentListModel> response) {

                        try {
                            if (response.isSuccessful()) {
                                if (response.body().isStatus()) {

                                    studentListDialogAdapter = new StudentListDialogAdapter(getActivity());
                                    recyclerView.setAdapter(studentListDialogAdapter);
                                    studentListDialogAdapter.setSingleChoiceMode(true);
                                    studentListDialogAdapter.setOnStudentSelectedListener(AppointmentFragment.this);

                                    studentListBeans = response.body().getResult();

                                    if (studentListBeans != null && studentListBeans.size() > 0) {

                                        if (studentListAdapter != null && studentListAdapter.getItemCount() > 0) {

                                            for (StudentListModel.ResultBean bean : studentListBeans) {

                                                for (StudentListModel.ResultBean model : studentListAdapter.getVisibleStudent()) {

                                                    if (bean.getStudentId().equals(model.getStudentId())) {
                                                        bean.setSelected(true);
                                                    }
                                                }
                                            }

                                            studentListDialogAdapter.addItem(studentListBeans);

                                        } else {

                                            studentListDialogAdapter.addItem(response.body().getResult());
                                        }
                                        viewAnimator.setDisplayedChild(1); // recyclerview visible

                                    } else {

                                        viewAnimator.setDisplayedChild(2);  // empty view visible
                                        txtNoData.setText(getString(R.string.no_data));
                                    }

                                } else {

                                    viewAnimator.setDisplayedChild(2); // empty view visible
                                    txtNoData.setText(response.body().getMessage());
                                }

                            } else {

                                APIError error = APIError.parseError(response, getActivity(), App_Constants.API_STUDENT_LIST);
                                viewAnimator.setDisplayedChild(2); // empty view visible
                                txtNoData.setText(error.message());

                                if (alertDialog != null && alertDialog.isShowing())
                                    alertDialog.dismiss();
                            }
                        } catch (Exception e) {

                            viewAnimator.setDisplayedChild(2); // empty view visible
                            txtNoData.setText(getString(R.string.somethingwrong));
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<StudentListModel> call, Throwable t) {
                        if (!call.isCanceled()) {
                            viewAnimator.setDisplayedChild(2); // empty view visible
                            txtNoData.setText(App_Constants.NO_INTERNET);
                            System.out.println(t.getMessage());
                        }
                    }
                });

            }
        });


        alertDialog.show();
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.90);

        if (alertDialog != null && alertDialog.getWindow() != null) {
            alertDialog.getWindow().setLayout(screenWidth, ViewGroup.LayoutParams.WRAP_CONTENT); //set below the setContentview
        }
    }

    private void InitScheduleAppointent() {

        parentViewAnimator.setDisplayedChild(1);
        mainViewAnimator.setDisplayedChild(0);

        if (entity != null) {
            getTeachersAllAppointment();
        } else {
            mainViewAnimator.setDisplayedChild(0);
        }

    }

    private void InitUpcomingAppointment() {

        parentViewAnimator.setDisplayedChild(1);
        mainViewAnimator.setDisplayedChild(1);

        if (entity != null) {
            getUpCommingAppointment();
        } else {
            upcomingViewAnimator.setDisplayedChild(2);
        }
    }

    private void InitPastAppointment() {

        parentViewAnimator.setDisplayedChild(1);
        mainViewAnimator.setDisplayedChild(2);

        if (entity != null) {
            getPastAppointment();
        } else {
            pastViewAnimator.setDisplayedChild(2);
        }
    }

    private void getTeachersAllAppointment() {

        scheduleViewAnimator.setDisplayedChild(0);

        allCall = AdminApp.getInstance().getApi().getTeacherAppoinments(entity.getSchoolId(), entity.getSchoolYear(), entity.getId());
        allCall.enqueue(new Callback<TeacherAppointmentModel>() {
            @Override
            public void onResponse(Call<TeacherAppointmentModel> call, Response<TeacherAppointmentModel> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            results = response.body().getResult();

                            List<TeacherAppointmentModel.ResultBean> finalResults = new ArrayList<>();
                            for (TeacherAppointmentModel.ResultBean info : results) {
                                if (info.getAppointmentTitle() != null && !info.getAppointmentTitle().isEmpty() && !info.getStatus().equals("cancelled")
                                        && info.getFrom() != null) {
                                    finalResults.add(info);
                                }
                            }
                            if (calenderView != null) {
                                calenderView.setEventList(finalResults);
                            }


                            String date = AdminApp.getInstance().currentDate();
                            String[] dateArr = date.split("-");
                            int day = Integer.parseInt(dateArr[2]);
                            int month = Integer.parseInt(dateArr[1]);
                            int year = Integer.parseInt(dateArr[0]);

                            onMonthChange(year, month);
                            // onDayClick(day, month, year);

                            parentViewAnimator.setDisplayedChild(1);
                            mainViewAnimator.setDisplayedChild(0);

                        } else {

                            parentViewAnimator.setDisplayedChild(1);
                            mainViewAnimator.setDisplayedChild(0);
                            Utility.showSnackBar(parentViewAnimator, response.body().getMessage());
                        }

                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_TEACHER_APPOINTMENT);
                        parentViewAnimator.setDisplayedChild(1);
                        mainViewAnimator.setDisplayedChild(0);
                        Utility.showSnackBar(parentViewAnimator, error.message());
                    }
                } catch (Exception e) {

                    parentViewAnimator.setDisplayedChild(1);
                    mainViewAnimator.setDisplayedChild(0);
                    Utility.showSnackBar(parentViewAnimator, AdminApp.getInstance().getString(R.string.somethingwrong));
                    e.printStackTrace();
                }

                scheduleViewAnimator.setDisplayedChild(1);
            }

            @Override
            public void onFailure(Call<TeacherAppointmentModel> call, Throwable t) {
                if (!call.isCanceled()) {
                    System.out.println(t.getMessage());
                    parentViewAnimator.setDisplayedChild(1);
                    scheduleViewAnimator.setDisplayedChild(1);
                    mainViewAnimator.setDisplayedChild(0);
                    Utility.showSnackBar(parentViewAnimator, App_Constants.NO_INTERNET);

                }
            }
        });
    }


    private void getUpCommingAppointment() {

        upcomingViewAnimator.setDisplayedChild(0);
        upcomingCall = AdminApp.getInstance().getApi().getTeacherAppoinments(entity.getSchoolId(), entity.getSchoolYear(), entity.getId());
        upcomingCall.enqueue(new Callback<TeacherAppointmentModel>() {
            @Override
            public void onResponse(Call<TeacherAppointmentModel> call, Response<TeacherAppointmentModel> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            upcomingResults = new ArrayList<TeacherAppointmentModel.ResultBean>();
                            List<TeacherAppointmentModel.ResultBean> upcomingResults = response.body().getResult();
                            headers.clear();

                            if (upcomingResults != null && upcomingResults.size() > 0) {

                                Collections.sort(upcomingResults, new Comparator<TeacherAppointmentModel.ResultBean>() {
                                    public int compare(TeacherAppointmentModel.ResultBean o1, TeacherAppointmentModel.ResultBean o2) {

                                        if (o2.getAppointmentDate() == null || o1.getAppointmentDate() == null)
                                            return 0;

                                        return o2.getAppointmentDate().compareTo(o1.getAppointmentDate());
                                    }
                                });

                                upcomingAdapter = new UpcomingAdapter(getActivity());
                                rvUpcoming.setAdapter(upcomingAdapter);
                                upcomingAdapter.setOnAppointmentConfirmationListener(AppointmentFragment.this);

                                Log.e("Appointment Size ", "" + upcomingResults.size());
                                for (int i = 0; i < upcomingResults.size(); i++) {

                                    TeacherAppointmentModel.ResultBean bean = upcomingResults.get(i);

                                    if (bean.getAppointmentDate() != null) {

                                        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
                                        DateTime serviceDate = fmt.parseDateTime(bean.getAppointmentDate());
                                        DateTime CurrentDate = fmt.parseDateTime(AdminApp.getInstance().currentDate());

                                        if (serviceDate.isAfter(CurrentDate) || serviceDate.isEqual(CurrentDate)) {
                                            if (bean.getAppointmentTime().contains("-")) {
                                                String[] aa = bean.getAppointmentTime().split("-");
                                                time = aa[0].trim();
                                                Log.e("TIME", time);

                                                Calendar cal = Calendar.getInstance(TimeZone.getDefault());
                                                Date currentLocalTime = cal.getTime();
                                                DateFormat date = new SimpleDateFormat("hh:mm a");
                                                date.setTimeZone(TimeZone.getDefault());

                                                currentTime = date.format(currentLocalTime);

                                            } else {

                                                time = bean.getAppointmentTime();
                                                Calendar cal = Calendar.getInstance(TimeZone.getDefault());
                                                Date currentLocalTime = cal.getTime();
                                                DateFormat date = new SimpleDateFormat("hh:mm a");
                                                date.setTimeZone(TimeZone.getDefault());

                                                currentTime = date.format(currentLocalTime);


                                            }

                                            String finalDate = DateFunction.ConvertDate(bean.getAppointmentDate(), "yyyy-MM-dd", "dd MMMM yyyy");

                                            if (finalDate.contains(" ")) {
                                                String[] pDates = finalDate.split(" ");
                                                if (pDates.length > 2) {
                                                    String month = pDates[1];
                                                    String year = pDates[2];
                                                    if (!headers.contains(month)) {
                                                        if (time != null && currentTime != null) {
                                                            if (serviceDate.isEqual(CurrentDate)) {
                                                                if (checkTimeForUpcoming(time, currentTime)) {
                                                                    headers.add(month);
                                                                    TeacherAppointmentModel.ResultBean entity = new TeacherAppointmentModel.ResultBean();
                                                                    entity.setAppointmentDate(bean.getAppointmentDate());
                                                                    entity.setType(1);
                                                                    entity.setMonth(month + " " + year);
                                                                    AppointmentFragment.this.upcomingResults.add(entity);
                                                                    AppointmentFragment.this.upcomingResults.add(bean);
                                                                }
                                                            } else {
                                                                headers.add(month);
                                                                TeacherAppointmentModel.ResultBean entity = new TeacherAppointmentModel.ResultBean();
                                                                entity.setAppointmentDate(bean.getAppointmentDate());
                                                                entity.setType(1);
                                                                entity.setMonth(month + " " + year);
                                                                AppointmentFragment.this.upcomingResults.add(entity);
                                                                AppointmentFragment.this.upcomingResults.add(bean);
                                                            }
                                                        }
                                                    } else {
                                                        if (time != null && currentTime != null) {
                                                            if (serviceDate.isEqual(CurrentDate)) {
                                                                if (checkTimeForUpcoming(time, currentTime))
                                                                    AppointmentFragment.this.upcomingResults.add(bean);
                                                            } else {
                                                                AppointmentFragment.this.upcomingResults.add(bean);
                                                            }
                                                        }
                                                    }
                                                }
                                            }


                                        }
                                    }
                                }

                                if (aId != null) {

                                    for (TeacherAppointmentModel.ResultBean bean : AppointmentFragment.this.upcomingResults) {
                                        if (bean.getAppointmentId() != null && bean.getAppointmentId().equals(aId)) {
                                            Intent intent = new Intent(getActivity(), AppointmentDetailActivity.class);
                                            intent.putExtra(AppointmentDetailActivity.SCHEDULED_OBJECT, bean);
                                            intent.putExtra(AppointmentDetailActivity.TYPE, AppointmentDetailActivity.UPCOMING);
                                            intent.putExtra(AppointmentDetailActivity.SCHEDULED_DATE, bean.getAppointmentDate());
                                            startActivity(intent);
                                            aId = null;
                                            break;
                                        }
                                    }


                                } else {

                                    if (AppointmentFragment.this.upcomingResults.size() > 0) {
                                        upcomingViewAnimator.setDisplayedChild(1);
                                        upcomingAdapter.addItem(AppointmentFragment.this.upcomingResults);
                                    } else {
                                        upcomingViewAnimator.setDisplayedChild(2);
                                    }
                                }

                            } else {

                                upcomingViewAnimator.setDisplayedChild(2);
                            }


                        } else {

                            upcomingViewAnimator.setDisplayedChild(2);
                            txtNoUpcomingData.setText(response.body().getMessage());
                        }

                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_UPCOMING_APPOINMENT);
                        upcomingViewAnimator.setDisplayedChild(2);
                        txtNoUpcomingData.setText(error.message());
                    }
                } catch (Exception e) {

                    upcomingViewAnimator.setDisplayedChild(2);
                    AdminApp.getInstance().ToastSomethingWrong();
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<TeacherAppointmentModel> call, Throwable t) {
                if (!call.isCanceled()) {
                    System.out.println(t.getMessage());
                    upcomingViewAnimator.setDisplayedChild(2);
                    txtNoUpcomingData.setText(App_Constants.NO_INTERNET);

                }
            }
        });
    }


    private void getPastAppointment() {

        pastViewAnimator.setDisplayedChild(0);
        pastCall = AdminApp.getInstance().getApi().getTeacherAppoinments(entity.getSchoolId(), entity.getSchoolYear(), entity.getId());
        pastCall.enqueue(new Callback<TeacherAppointmentModel>() {
            @Override
            public void onResponse(Call<TeacherAppointmentModel> call, Response<TeacherAppointmentModel> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            pastResults = new ArrayList<TeacherAppointmentModel.ResultBean>();

                            List<TeacherAppointmentModel.ResultBean> pastResults = response.body().getResult();
                            headers.clear();

                            if (pastResults != null && pastResults.size() > 0) {

                                Collections.sort(pastResults, new Comparator<TeacherAppointmentModel.ResultBean>() {
                                    public int compare(TeacherAppointmentModel.ResultBean o1, TeacherAppointmentModel.ResultBean o2) {

                                        if (o2.getAppointmentDate() == null || o1.getAppointmentDate() == null)
                                            return 0;

                                        return o2.getAppointmentDate().compareTo(o1.getAppointmentDate());
                                    }
                                });


                                scheduledAdapter = new ScheduledAdapter(getActivity());
                                rvPast.setAdapter(scheduledAdapter);

                                Log.e("Appointment Size ", "" + pastResults.size());
                                for (int i = 0; i < pastResults.size(); i++) {

                                    TeacherAppointmentModel.ResultBean bean = pastResults.get(i);

                                    if (bean.getAppointmentDate() != null) {

                                        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
                                        DateTime serviceDate = fmt.parseDateTime(bean.getAppointmentDate());
                                        DateTime CurrentDate = fmt.parseDateTime(AdminApp.getInstance().currentDate());

                                        if (serviceDate.isBefore(CurrentDate) || serviceDate.isEqual(CurrentDate)) {

                                            if (bean.getAppointmentTime().contains("-")) {
                                                String[] aa = bean.getAppointmentTime().split("-");
                                                time = aa[0].trim();
                                                Log.e("TIME", time);

                                                Calendar cal = Calendar.getInstance(TimeZone.getDefault());
                                                Date currentLocalTime = cal.getTime();
                                                DateFormat date = new SimpleDateFormat("hh:mm a");
                                                date.setTimeZone(TimeZone.getDefault());

                                                currentTime = date.format(currentLocalTime);

                                            } else {

                                                time = bean.getAppointmentTime();
                                                Calendar cal = Calendar.getInstance(TimeZone.getDefault());
                                                Date currentLocalTime = cal.getTime();
                                                DateFormat date = new SimpleDateFormat("hh:mm a");
                                                date.setTimeZone(TimeZone.getDefault());

                                                currentTime = date.format(currentLocalTime);


                                            }

                                            String finalDate = DateFunction.ConvertDate(bean.getAppointmentDate(), "yyyy-MM-dd", "dd MMMM yyyy");

                                            if (finalDate.contains(" ")) {
                                                String[] pDates = finalDate.split(" ");
                                                if (pDates.length > 2) {
                                                    String month = pDates[1];
                                                    String year = pDates[2];
                                                    if (!headers.contains(month)) {
                                                        if (time != null && currentTime != null) {
                                                            if (serviceDate.isEqual(CurrentDate)) {
                                                                if (checkTimeForPast(time, currentTime)) {
                                                                    headers.add(month);
                                                                    TeacherAppointmentModel.ResultBean entity = new TeacherAppointmentModel.ResultBean();
                                                                    entity.setAppointmentDate(bean.getAppointmentDate());
                                                                    entity.setType(1);
                                                                    entity.setMonth(month + " " + year);
                                                                    AppointmentFragment.this.pastResults.add(entity);
                                                                    AppointmentFragment.this.pastResults.add(bean);
                                                                }
                                                            } else {
                                                                headers.add(month);
                                                                TeacherAppointmentModel.ResultBean entity = new TeacherAppointmentModel.ResultBean();
                                                                entity.setAppointmentDate(bean.getAppointmentDate());
                                                                entity.setType(1);
                                                                entity.setMonth(month + " " + year);
                                                                AppointmentFragment.this.pastResults.add(entity);
                                                                AppointmentFragment.this.pastResults.add(bean);
                                                            }
                                                        }
                                                    } else {
                                                        if (time != null && currentTime != null) {
                                                            if (serviceDate.isEqual(CurrentDate)) {
                                                                if (checkTimeForPast(time, currentTime))
                                                                    AppointmentFragment.this.pastResults.add(bean);
                                                            } else {
                                                                AppointmentFragment.this.pastResults.add(bean);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }

                                if (AppointmentFragment.this.pastResults.size() > 0) {
                                    pastViewAnimator.setDisplayedChild(1);
                                    scheduledAdapter.addItem(AppointmentFragment.this.pastResults);
                                } else {
                                    pastViewAnimator.setDisplayedChild(2);
                                }

                            } else {

                                pastViewAnimator.setDisplayedChild(2);
                            }


                        } else {

                            pastViewAnimator.setDisplayedChild(2);
                            txtNoPastData.setText(response.body().getMessage());
                        }

                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_PAST_APPOINMENT);
                        pastViewAnimator.setDisplayedChild(2);
                        txtNoPastData.setText(error.message());
                    }
                } catch (Exception e) {

                    pastViewAnimator.setDisplayedChild(2);
                    txtNoPastData.setText(getString(R.string.somethingwrong));
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<TeacherAppointmentModel> call, Throwable t) {
                if (!call.isCanceled()) {
                    System.out.println(t.getMessage());
                    pastViewAnimator.setDisplayedChild(2);
                    txtNoPastData.setText(App_Constants.NO_INTERNET);
                }
            }
        });
    }


    private boolean checkTimeForUpcoming(String time, String currenTime) {

        try {

            LocalTime serverTime = DateTimeFormat.forPattern("hh:mm a").withLocale(Locale.ENGLISH).parseLocalTime(time);
            LocalTime currentTime = DateTimeFormat.forPattern("hh:mm a").withLocale(Locale.ENGLISH).parseLocalTime(currenTime);

            if (serverTime.isAfter(currentTime)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {

            e.printStackTrace();
        }


        return false;
    }

    private boolean checkTimeForPast(String time, String currenTime) {

        try {

            DateTimeFormatter fmt = DateTimeFormat.forPattern("hh:mm a");
            DateTime serverTime = fmt.parseDateTime(time);
            DateTime currentTime = fmt.parseDateTime(currenTime);

            if (currentTime.isAfter(serverTime)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {

            e.printStackTrace();
        }


        return false;
    }

    @Override
    public void onDayClick(int date, int month, int year) {

        if (results != null)
            setEventsForDayAdapter(date, month, year);
    }

    @Override
    public void getSchoolEvents(List<SchoolActivityResponse.ResultBean> sEvents, int date, int month, int year) {

    }

    @Override
    public void getClassEvents(List<ClassActivityResponse.ResultBean> cEvents, int date, int month, int year) {

    }

    private void setEventsForDayAdapter(int date, int month, int year) {

        StringBuilder temp_month = new StringBuilder();
        if (month < 10) {
            temp_month.append("0").append(month);
        } else {
            temp_month.append(month);
        }

        StringBuilder temp_day = new StringBuilder();
        if (date < 10) {
            temp_day.append("0").append(date);
        } else {
            temp_day.append(date);
        }

        selectedDate = year + "-" + temp_month.toString() + "-" + temp_day.toString();
        Log.d("SELECTED DATE : ", "" + selectedDate);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, date);
        calendar.set(Calendar.MONTH, month - 1);
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);


        mWeekView.goToToday();
    }


    @Override
    public void onAppointmentCancel(final String aId, final int adapterPosition, final TeacherAppointmentModel.ResultBean entity) {

        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle(getString(R.string.alert));
        alertDialog.setMessage(getString(R.string.sure_to_delete_appointment));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();

                        appointmentId = aId;
                        adapterPos = adapterPosition;
                        Appointmententity = entity;

                        showProgress();

                        cancelCall = AdminApp.getInstance().getApi().cancelAppointment(appointmentId);

                        cancelCall.enqueue(new Callback<AppointmentConfirmResponse>() {
                            @Override
                            public void onResponse(Call<AppointmentConfirmResponse> call, Response<AppointmentConfirmResponse> response) {

                                try {
                                    if (response.isSuccessful()) {
                                        if (response.body().isStatus()) {


                                            entity.setStatus("cancelled");
                                            if (upcomingAdapter != null)
                                                upcomingAdapter.notifyItemChanged(adapterPosition, entity);

                                            Utility.showSnackBar(parentViewAnimator, response.body().getMessage());

                                        } else {

                                            Utility.showSnackBar(parentViewAnimator, response.body().getMessage());
                                        }
                                    } else {

                                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_CANCEL_APPOINTMENT);
                                        Utility.showSnackBar(parentViewAnimator, error.message());
                                    }
                                } catch (Exception e) {

                                    e.printStackTrace();
                                    Utility.showSnackBar(parentViewAnimator, getString(R.string.somethingwrong));
                                }
                                hideProgress();
                            }

                            @Override
                            public void onFailure(Call<AppointmentConfirmResponse> call, Throwable t) {
                                if (!call.isCanceled()) {
                                    hideProgress();
                                    Utility.showSnackBar(parentViewAnimator, App_Constants.NO_INTERNET);
                                }
                            }
                        });

                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });
        alertDialog.show();


    }

    @Override
    public void onAppointmentConfirm(final String aId, final int adapterPosition, final TeacherAppointmentModel.ResultBean entity) {

        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle(getString(R.string.alert));
        alertDialog.setMessage(getString(R.string.sure_to_confirm_appointment));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();

                        appointmentId = aId;
                        adapterPos = adapterPosition;
                        Appointmententity = entity;

                        showProgress();
                        confirmCall = AdminApp.getInstance().getApi().confirmAppointment(appointmentId);

                        confirmCall.enqueue(new Callback<AppointmentConfirmResponse>() {
                            @Override
                            public void onResponse(Call<AppointmentConfirmResponse> call, Response<AppointmentConfirmResponse> response) {

                                try {
                                    if (response.isSuccessful()) {
                                        if (response.body().isStatus()) {

                                            AdminApp.getInstance().addEventToDeviceCalander(entity);
                                            entity.setStatus("confirmed");
                                            if (upcomingAdapter != null)
                                                upcomingAdapter.notifyItemChanged(adapterPosition, entity);

                                            Utility.showSnackBar(parentViewAnimator, response.body().getMessage());

                                        } else {

                                            Utility.showSnackBar(parentViewAnimator, response.body().getMessage());
                                        }
                                    } else {

                                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_CONFIRM_APPOINMENT);
                                        Utility.showSnackBar(parentViewAnimator, error.message());
                                    }
                                } catch (Exception e) {

                                    Utility.showSnackBar(parentViewAnimator, getString(R.string.somethingwrong));
                                }
                                hideProgress();
                            }

                            @Override
                            public void onFailure(Call<AppointmentConfirmResponse> call, Throwable t) {
                                if (!call.isCanceled()) {
                                    hideProgress();
                                    Utility.showSnackBar(parentViewAnimator, App_Constants.NO_INTERNET);
                                }
                            }
                        });

                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });
        alertDialog.show();


    }


    @Override
    public void OnEventEdit(TeacherAppointmentModel.ResultBean entity, int position) {

       /* startActivityForResult(new Intent(getActivity(), AddScheduleActivity.class)
                .putExtra(AddScheduleActivity.TYPE, AddScheduleActivity.UPDATE)
                .putExtra(AddScheduleActivity.OBJECT, entity), 1111);*/
    }

    @Override
    public void OnEventCancel(final TeacherAppointmentModel.ResultBean entity, final int position) {
        onAppointmentCancel(entity.getAppointmentId(), position, entity);
    }

    @Override
    public void OnEventNotification(TeacherAppointmentModel.ResultBean entity, int position) {

    }


    private void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    private void showProgress() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.processing));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }


    @Override
    public void onStop() {
        if (allCall != null)
            allCall.cancel();
        if (upcomingCall != null)
            upcomingCall.cancel();
        if (pastCall != null)
            pastCall.cancel();
        if (updateEventCall != null)
            updateEventCall.cancel();
        if (AddEventCall != null)
            AddEventCall.cancel();
        if (cancelCall != null)
            cancelCall.cancel();
        if (confirmCall != null)
            confirmCall.cancel();
        if (appointmentByIdcall != null)
            appointmentByIdcall.cancel();

        super.onStop();
    }

  /*  @OnClick(R.id.txt_Schedule)
    public void onViewClicked() {

        startActivityForResult(new Intent(getActivity(), AddScheduleActivity.class)
                .putExtra(AddScheduleActivity.SELECTED_DATE, selectedDate), 1111);
    }*/

    @Override
    public void onReload(String apiName) {

        if (isVisible()) {

            switch (apiName) {
                case App_Constants.API_TEACHER_APPOINTMENT:
                    getTeachersAllAppointment();
                    break;
                case App_Constants.API_UPCOMING_APPOINMENT:
                    getUpCommingAppointment();
                    break;
                case App_Constants.API_TEACHER_CLASS_SCHEDULE:
                    getTeacherClassSchedule();
                    break;
                case App_Constants.API_PAST_APPOINMENT:
                    getPastAppointment();
                    break;
                case App_Constants.API_CANCEL_APPOINTMENT:
                    onAppointmentCancel(appointmentId, adapterPos, Appointmententity);
                    break;
                case App_Constants.API_CONFIRM_APPOINMENT:
                    onAppointmentConfirm(appointmentId, adapterPos, Appointmententity);
                    break;
                case App_Constants.API_APPOINTMENT_BY_ID:
                    getAppointmentById(Long.parseLong(appoId));
                    break;
                case App_Constants.API_UPDATE_EVENT:
                    UpdateEvent();
                    break;
                case App_Constants.API_ADD_EVENT:
                    AddEvent();
                    break;
            }

        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1111 && resultCode == Activity.RESULT_OK) {
            if (isVisible()) {

                if (sgAppointment.getCheckedRadioButtonId() == R.id.rb_schedule) {
                    getTeachersAllAppointment();
                } else if (sgAppointment.getCheckedRadioButtonId() == R.id.rb_upcoming) {
                    InitUpcomingAppointment();
                } else if (sgAppointment.getCheckedRadioButtonId() == R.id.rb_past) {
                    InitPastAppointment();
                }
            }

            selectedDate = data.getStringExtra("selectedDate");
            String aap[] = selectedDate.split("-");
            for (String a : aap)
                Log.e("DATE ", a);

            setEventsForDayAdapter(Integer.parseInt(aap[2]), Integer.parseInt(aap[1]), Integer.parseInt(aap[0]));

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }





    private void setupDateTimeInterpreter(final boolean shortDate) {
        mWeekView.setDateTimeInterpreter(new DateTimeInterpreter() {

            @Override
            public String interpretDate(Calendar date) {

                SimpleDateFormat weekdayNameFormat = new SimpleDateFormat("EEE", Locale.getDefault());
                String weekday = weekdayNameFormat.format(date.getTime());
                SimpleDateFormat format = new SimpleDateFormat(" M/d", Locale.getDefault());

                // All android api level do not have a standard way of getting the first letter of
                // the week day name. Hence we get the first char programmatically.
                // Details: http://stackoverflow.com/questions/16959502/get-one-letter-abbreviation-of-week-day-of-a-date-in-java#answer-16959657
                if (shortDate)
                    weekday = String.valueOf(weekday.charAt(0));
                return weekday.toUpperCase() + format.format(date.getTime());
            }

            @Override
            public String interpretTime(int hour) {


                String timeSet = "";
                if (hour > 12) {
                    hour -= 12;
                    timeSet = hour + " PM";
                } else if (hour == 0) {
                    hour += 12;
                    timeSet = hour + " AM";
                } else if (hour == 12) {
                    timeSet = hour + " PM";
                } else {
                    timeSet = hour + " AM";
                }

                return timeSet;

                // return hour > 11 ? (hour - 12) + " PM" : (hour == 0 ? "12 AM" : hour + " AM");
            }
        });
    }


    @Override
    public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {

        List<TeacherAppointmentModel.ResultBean> selectedDayEvents = new ArrayList<>();
        List<WeekViewEvent> events = new ArrayList<WeekViewEvent>();

        if (results != null) {
            for (TeacherAppointmentModel.ResultBean bean : results) {
                if (bean.getAppointmentDate() != null && bean.getAppointmentDate().equals(selectedDate)
                        && !bean.getStatus().equals("cancelled") && bean.getAppointmentTitle() != null && !bean.getAppointmentTitle().isEmpty()) {

                    if (bean.getAppointmentTime() != null && bean.getAppointmentTime().contains("-"))
                        time = bean.getAppointmentTime().split("-")[0].trim();
                    else
                        time = bean.getAppointmentTime();
                    Calendar cal = Calendar.getInstance(TimeZone.getDefault());
                    Date currentLocalTime = cal.getTime();
                    DateFormat date1 = new SimpleDateFormat("hh:mm a");
                    date1.setTimeZone(TimeZone.getDefault());
                    currentTime = date1.format(currentLocalTime);

                    DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
                    DateTime serviceDate = fmt.parseDateTime(bean.getAppointmentDate());
                    DateTime CurrentDate = fmt.parseDateTime(AdminApp.getInstance().currentDate());

                    if (bean.getFrom() != null && bean.getFrom().equals(AdminApp.getInstance().getAdmin().getId())) {

                        if (serviceDate.isAfter(CurrentDate) || serviceDate.isEqual(CurrentDate)) {

                            if (serviceDate.isEqual(CurrentDate)) {
                                if (checkTimeForUpcoming(time, currentTime)) {
                                    bean.setEditable(true);
                                } else {
                                    bean.setEditable(false);
                                }
                            } else {
                                bean.setEditable(true);
                            }

                        } else {
                            bean.setEditable(false);
                        }

                    } else {
                        bean.setEditable(false);
                    }
                    selectedDayEvents.add(bean);
                }
            }

            if (selectedDayEvents.size() > 0) {
                if (selectedDate != null) {
                    int compareValue = DateFunction.compareDateWithTodayDate(selectedDate);
                    isActionButtonsVisible = compareValue >= 0;

                }


                for (TeacherAppointmentModel.ResultBean beans : selectedDayEvents) {

                    if (beans.getAppointmentTime().contains("-")) {

                        try {

                            String[] time = beans.getAppointmentTime().split("-");
                            String start = time[0].trim();
                            String end = time[1].trim();

                            SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
                            SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
                            Date sd = parseFormat.parse(start);
                            Date ed = parseFormat.parse(end);
                            String fsd = displayFormat.format(sd);
                            String fed = displayFormat.format(ed);


                            Calendar startTime = Calendar.getInstance();
                            startTime.set(Calendar.HOUR_OF_DAY, Integer.parseInt(fsd.split(" ")[0].split(":")[0]));
                            startTime.set(Calendar.MINUTE, Integer.parseInt(fsd.split(" ")[0].split(":")[1]));
                            startTime.set(Calendar.SECOND, 0);
                            startTime.set(Calendar.MONTH, newMonth - 1);
                            startTime.set(Calendar.YEAR, newYear);
                            Calendar endTime = (Calendar) startTime.clone();
                            endTime.set(Calendar.HOUR_OF_DAY, Integer.parseInt(fed.split(" ")[0].split(":")[0]));
                            endTime.set(Calendar.MINUTE, Integer.parseInt(fed.split(" ")[0].split(":")[1]));
                            startTime.set(Calendar.SECOND, 55);
                            endTime.set(Calendar.MONTH, newMonth - 1);
                            WeekViewEvent event = new WeekViewEvent(Long.parseLong(beans.getAppointmentId()), beans.getAppointmentTitle(), startTime, endTime);
                            event.setColor(getResources().getColor(R.color.colorPrimary));
                            events.add(event);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                mWeekView.notifyDatasetChanged();

            } else {
                //scheduleViewAnimator.setDisplayedChild(2);
            }

        } else {

            // scheduleViewAnimator.setDisplayedChild(2);
        }
        return events;
    }

    @Override
    public void onStudentSelected(boolean makeEnabled, List<String> studentList) {

        if (isVisible()) {
            studentIdList.clear();
            studentIdList.addAll(studentList);
//          studentListAdapter.makeSingleChoice();
        }
    }

    @Override
    public void onEventLongPress(final WeekViewEvent event, RectF eventRect) {


        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setTitle(getString(R.string.select_operation));
        builderSingle.setCancelable(true);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                getActivity(),
                R.layout.raw_dilaog);


        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTime SelectedDate = fmt.parseDateTime(selectedDate);
        DateTime CurrentDate = fmt.parseDateTime(AdminApp.getInstance().currentDate());

        if (SelectedDate.isAfter(CurrentDate) || SelectedDate.isEqual(CurrentDate)) {
            arrayAdapter.add(getString(R.string.view));
            arrayAdapter.add(getString(R.string.edit));
            arrayAdapter.add(getString(R.string.delete));
        } else {
            arrayAdapter.add(getString(R.string.view));
        }

        builderSingle.setAdapter(
                arrayAdapter,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //String strName = arrayAdapter.getItem(which);
                        String appointmentType = App_Constants.PAST;

                        String type = arrayAdapter.getItem(which);
                        if (type.equalsIgnoreCase(getString(R.string.edit))) {
                            getAppointmentById(event.getId());
                        } else if (type.equalsIgnoreCase(getString(R.string.delete))) {
                            deleteAppointment(event.getId());
                        } else if (type.equalsIgnoreCase(getString(R.string.view))) {

                            if (selectedDate != null) {
                                int compareValue = DateFunction.compareDateWithTodayDate(selectedDate);
                                if (compareValue >= 0) {

                                    appointmentType = App_Constants.UPCOMING;
                                }
                            }

                            Intent intent = new Intent(getActivity(), AppointmentDetailActivity.class);
                            intent.putExtra(AppointmentDetailActivity.TYPE, appointmentType);
                            intent.putExtra(AppointmentDetailActivity.APPOINMENT_ID, String.valueOf(event.getId()));
                            startActivity(intent);
                            if (getActivity() != null)
                                getActivity().overridePendingTransition(R.anim.enter, R.anim.leave);
                        }
                        // Toast.makeText(getContext(), "" + type, Toast.LENGTH_SHORT).show();


                    }
                });

        AlertDialog alertDialog = builderSingle.create();
        alertDialog.show();

    }

    @Override
    public void onEmptyViewLongPress(Calendar time) {
        // getTeacherMeetingTimeSlots(time.get(Calendar.HOUR_OF_DAY) + ":" + time.get(Calendar.MINUTE));
    }

    private void getAppointmentById(long id) {

        scheduleInnerViewAnimator.setDisplayedChild(1);

        appoId = String.valueOf(id);

        appointmentByIdcall = AdminApp.getInstance().getApi().getAppointmentById(appoId);

        appointmentByIdcall.enqueue(new Callback<AppointmentByIdResponse>() {
            @Override
            public void onResponse(Call<AppointmentByIdResponse> call, Response<AppointmentByIdResponse> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            appointmentByIdModel = response.body().getResult();
                            if (appointmentByIdModel != null)
                                setValues(appointmentByIdModel);
                        } else {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_APPOINTMENT_BY_ID);
                        Toast.makeText(getActivity(), "" + error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {

                    Toast.makeText(getActivity(), getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<AppointmentByIdResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    scheduleInnerViewAnimator.setDisplayedChild(0);
                    Toast.makeText(getActivity(), App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void setValues(AppointmentByIdResponse.ResultBean model) {

        fabAdd.setVisibility(View.GONE);
        rlEdit.setVisibility(View.GONE);
        txtTime.setText(model.getAppointmentTime().split("-")[0].trim());
        txtGrade.setText(model.getGradeId());
        txtClass.setText(model.getClassId());
        edittext.setText(model.getAppointmentTitle());
        btnAssign.setText(getString(R.string.update));


        linearGrade.setAlpha(0.5f);
        linearGrade.setEnabled(false);

        linearClass.setAlpha(0.5f);
        linearClass.setEnabled(false);

        /* linearTime.setAlpha(0.5f);
        linearTime.setEnabled(false);

        linearDuartion.setAlpha(0.5f);
        linearDuartion.setEnabled(false);*/

        DateTime time1 = DateTime.parse(model.getAppointmentTime().split("-")[0].trim(), DateTimeFormat.forPattern("hh:mm a"));
        DateTime time2 = DateTime.parse(model.getAppointmentTime().split("-")[1].trim(), DateTimeFormat.forPattern("hh:mm a"));
        Minutes minutes = Minutes.minutesBetween(time1, time2);
        System.out.println(minutes.getMinutes());

        endTime = model.getAppointmentTime().split("-")[1].trim();

        txtDuartion.setText(minutes.getMinutes() + " minutes");
        getStudentList();

    }


    private void getStudentList() {

        studentListCall = AdminApp.getInstance().getApi().getStudentList(entity.getSchoolId(), txtGrade.getText().toString(),
                txtClass.getText().toString(), AdminApp.getInstance().getAdmin().getSchoolYear());
        studentListCall.enqueue(new Callback<StudentListModel>() {
            @Override
            public void onResponse(Call<StudentListModel> call, Response<StudentListModel> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            studentListAdapter = new StudentListAdapter(getActivity());
                            rvStudents.setAdapter(studentListAdapter);
                            studentListAdapter.setOnStudentSelectedListener(AppointmentFragment.this);

                            List<StudentListModel.ResultBean> results = response.body().getResult();
                            List<StudentListModel.ResultBean> updateResults = new ArrayList<StudentListModel.ResultBean>();

                            if (results != null && results.size() > 0) {

                                for (StudentListModel.ResultBean entity : results) {

                                    if (appointmentByIdModel.getStudentList() != null && appointmentByIdModel.getStudentList().size() > 0) {
                                        for (String ss : appointmentByIdModel.getStudentList()) {

                                            if (ss.equals(entity.getStudentId())) {
                                                entity.setSelected(true);
                                                entity.setEnabled(false);
                                                studentIdList.add(ss);
                                                updateResults.add(entity);
                                            }
                                        }
                                    }
                                }

                                studentListAdapter.addItem(updateResults);
                                linearArea.setVisibility(View.VISIBLE);

                            } else {
                                Toast.makeText(getActivity(), "" + getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_STUDENT_LIST);
                        Toast.makeText(getActivity(), "" + error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(getActivity(), "" + getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                scheduleInnerViewAnimator.setDisplayedChild(0);

            }

            @Override
            public void onFailure(Call<StudentListModel> call, Throwable t) {
                if (!call.isCanceled()) {
                    scheduleInnerViewAnimator.setDisplayedChild(0);
                    Toast.makeText(getActivity(), App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    private void UpdateEvent() {

        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTime SelectedDate = fmt.parseDateTime(selectedDate);
        DateTime CurrentDate = fmt.parseDateTime(AdminApp.getInstance().currentDate());

        if (SelectedDate.isBefore(CurrentDate)) {

            Utility.showSnackBar(rvStudents, getString(R.string.select_future_date));
            return;
        }

        String startTime = txtTime.getText().toString();

        String sTime = "08:00 AM";
        String eTime = "05:00 PM";


        DateTimeFormatter dtf = DateTimeFormat.forPattern("hh:mm a");
        DateTimeFormatter locale = dtf.withLocale(Locale.getDefault());

        LocalTime start = LocalTime.parse(sTime, locale);
        LocalTime stop = LocalTime.parse(eTime, locale);
        LocalTime target = LocalTime.parse(startTime, locale);

        Boolean isTargetAfterStartAndBeforeStop = ((target.isAfter(start) && target.isBefore(stop))
                || target.isEqual(start) || target.isEqual(stop));

        if (isTargetAfterStartAndBeforeStop) {

            SimpleDateFormat sdf4 = new SimpleDateFormat("hh:mm a");
            String currentDateandTime = sdf4.format(new Date());

            LocalTime currenTime = LocalTime.parse(currentDateandTime, locale);
            LocalTime select = LocalTime.parse(startTime, locale);

            if (SelectedDate.isEqual(CurrentDate)) {

                if (currenTime.isAfter(select)) {
                    Toast.makeText(getActivity(), getString(R.string.select_diffrent_time), Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        } else {

            Utility.showSnackBar(rvStudents, getString(R.string.select_time_btw_8_to_5));
            return;
        }

        scheduleInnerViewAnimator.setDisplayedChild(1);


        AddEventParams params = new AddEventParams();
        params.setAppointmentId(appointmentByIdModel.getAppointmentId());
        params.setAppointmentDate(selectedDate);
        params.setAppointmentTime(txtTime.getText().toString() + " - " + endTime);
        params.setGradeId(txtGrade.getText().toString());
        params.setClassId(txtClass.getText().toString());
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
        params.setSchoolYear(AdminApp.getInstance().getAdmin().getSchoolYear());
        params.setStudentList(studentIdList);
        params.setFrom(AdminApp.getInstance().getAdmin().getId());
        params.setSubjectName(appointmentByIdModel.getSubjectName());
        params.setTeacherEmailId(AdminApp.getInstance().getAdmin().getEmailId());
        params.setTeacherId(AdminApp.getInstance().getAdmin().getId());
        params.setTeacherMobileNo(AdminApp.getInstance().getAdmin().getPhoneNo());
        params.setTeacherName(AdminApp.getInstance().getAdmin().getFirstName() + " " + AdminApp.getInstance().getAdmin().getLastName());
        params.setAppointmentTitle(edittext.getText().toString());


        updateEventCall = AdminApp.getInstance().getApi().updateEvent(params);
        updateEventCall.enqueue(new Callback<AddEventModel>() {
            @Override
            public void onResponse(Call<AddEventModel> call, Response<AddEventModel> response) {

                Utility.hideProgress();
                studentListAdapter.fallBack();

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            Utility.showToast(getActivity(), response.body().getMessage());
                            clearSelections();
                            if (sgAppointment.getCheckedRadioButtonId() == R.id.rb_schedule) {
                                if (entity != null) {
                                    getTeachersAllAppointment();
                                    scheduleInnerViewAnimator.setDisplayedChild(0);
                                } else {
                                    parentViewAnimator.setDisplayedChild(1);
                                    mainViewAnimator.setDisplayedChild(0);
                                }
                            }
                        } else {
                            Utility.showSnackBar(rvStudents, response.body().getMessage());
                        }

                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_UPDATE_EVENT);
                        Utility.error(getActivity(), error.message());

                    }
                } catch (Exception e) {

                    Utility.showSnackBar(rvStudents, getString(R.string.somethingwrong));
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<AddEventModel> call, Throwable t) {
                if (!call.isCanceled()) {
                    System.out.println(t.getMessage());
                    Utility.showSnackBar(rvStudents, App_Constants.NO_INTERNET);
                    Utility.hideProgress();
                }
            }
        });
    }


    private void deleteAppointment(final long id) {

        showProgress();

        cancelCall = AdminApp.getInstance().getApi().cancelAppointment(String.valueOf(id));

        cancelCall.enqueue(new Callback<AppointmentConfirmResponse>() {
            @Override
            public void onResponse(Call<AppointmentConfirmResponse> call, Response<AppointmentConfirmResponse> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {


                            clearSelections();
                            getTeachersAllAppointment();
                            Utility.showSnackBar(parentViewAnimator, response.body().getMessage());

                        } else {

                            Utility.showSnackBar(parentViewAnimator, response.body().getMessage());
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_CANCEL_APPOINTMENT);
                        Utility.showSnackBar(parentViewAnimator, error.message());
                    }
                } catch (Exception e) {

                    Utility.showSnackBar(parentViewAnimator, getString(R.string.somethingwrong));
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<AppointmentConfirmResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    hideProgress();
                    Utility.showSnackBar(parentViewAnimator, App_Constants.NO_INTERNET);
                }
            }
        });

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(App_Constants.PAST_APPOINTMENT_LIST, pastResults);
        outState.putParcelableArrayList(App_Constants.UPCOMING_APPOINTMENT_LIST, upcomingResults);
        outState.putInt(App_Constants.CHECKED_ITEM, sgAppointment.getCheckedRadioButtonId());
        outState.putParcelableArrayList(App_Constants.ALL_APPOINTMENT_LIST, results);

    }

    public static class AddEventParams {


        /**
         * appointmentDate : YYYY-MM-dd
         * appointmentTime : HH:mm
         * appointmentTitle : string
         * classId : string
         * files : [{"filePath":"string"}]
         * from : string
         * gradeId : string
         * schoolId : string
         * schoolYear : YYYY-YYYY
         * studentList : ["string"]
         * subjectName : string
         * teacherEmailId : string
         * teacherId : string
         * teacherMobileNo : string
         * teacherName : string
         */
        private String appointmentId;
        private String appointmentDate;
        private String appointmentTime;
        private String appointmentTitle;
        private String classId;
        private String from;
        private String gradeId;
        private String schoolId;
        private String schoolYear;
        private String subjectName;
        private String teacherEmailId;
        private String teacherId;
        private String teacherMobileNo;
        private String teacherName;
        private List<FilesBean> files;
        private List<String> studentList;

        public String getAppointmentId() {
            return appointmentId;
        }

        public void setAppointmentId(String appointmentId) {
            this.appointmentId = appointmentId;
        }

        public String getAppointmentDate() {
            return appointmentDate;
        }

        public void setAppointmentDate(String appointmentDate) {
            this.appointmentDate = appointmentDate;
        }

        public String getAppointmentTime() {
            return appointmentTime;
        }

        public void setAppointmentTime(String appointmentTime) {
            this.appointmentTime = appointmentTime;
        }

        public String getAppointmentTitle() {
            return appointmentTitle;
        }

        public void setAppointmentTitle(String appointmentTitle) {
            this.appointmentTitle = appointmentTitle;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getGradeId() {
            return gradeId;
        }

        public void setGradeId(String gradeId) {
            this.gradeId = gradeId;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getSchoolYear() {
            return schoolYear;
        }

        public void setSchoolYear(String schoolYear) {
            this.schoolYear = schoolYear;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public String getTeacherEmailId() {
            return teacherEmailId;
        }

        public void setTeacherEmailId(String teacherEmailId) {
            this.teacherEmailId = teacherEmailId;
        }

        public String getTeacherId() {
            return teacherId;
        }

        public void setTeacherId(String teacherId) {
            this.teacherId = teacherId;
        }

        public String getTeacherMobileNo() {
            return teacherMobileNo;
        }

        public void setTeacherMobileNo(String teacherMobileNo) {
            this.teacherMobileNo = teacherMobileNo;
        }

        public String getTeacherName() {
            return teacherName;
        }

        public void setTeacherName(String teacherName) {
            this.teacherName = teacherName;
        }

        public List<FilesBean> getFiles() {
            return files;
        }

        public void setFiles(List<FilesBean> files) {
            this.files = files;
        }

        public List<String> getStudentList() {
            return studentList;
        }

        public void setStudentList(List<String> studentList) {
            this.studentList = studentList;
        }

        public static class FilesBean {
            /**
             * filePath : string
             */

            private String filePath;

            public String getFilePath() {
                return filePath;
            }

            public void setFilePath(String filePath) {
                this.filePath = filePath;
            }
        }
    }


    public static class UpdateEventParams {


        /**
         * appointmentDate : YYYY-MM-dd
         * appointmentTime : HH:mm
         * appointmentTitle : string
         * classId : string
         * files : [{"filePath":"string"}]
         * from : string
         * gradeId : string
         * schoolId : string
         * schoolYear : YYYY-YYYY
         * studentList : ["string"]
         * subjectName : string
         * teacherEmailId : string
         * teacherId : string
         * teacherMobileNo : string
         * teacherName : string
         */
        private String appointmentId;
        private String appointmentDate;
        private String appointmentTime;
        private String appointmentTitle;
        private String classId;
        private String from;
        private String gradeId;
        private String schoolId;
        private String schoolYear;
        private String subjectName;
        private String teacherEmailId;
        private String teacherId;
        private String teacherMobileNo;
        private String teacherName;
        private List<FilesBean> files;
        private List<String> studentList;

        public String getAppointmentId() {
            return appointmentId;
        }

        public void setAppointmentId(String appointmentId) {
            this.appointmentId = appointmentId;
        }

        public String getAppointmentDate() {
            return appointmentDate;
        }

        public void setAppointmentDate(String appointmentDate) {
            this.appointmentDate = appointmentDate;
        }

        public String getAppointmentTime() {
            return appointmentTime;
        }

        public void setAppointmentTime(String appointmentTime) {
            this.appointmentTime = appointmentTime;
        }

        public String getAppointmentTitle() {
            return appointmentTitle;
        }

        public void setAppointmentTitle(String appointmentTitle) {
            this.appointmentTitle = appointmentTitle;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getGradeId() {
            return gradeId;
        }

        public void setGradeId(String gradeId) {
            this.gradeId = gradeId;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getSchoolYear() {
            return schoolYear;
        }

        public void setSchoolYear(String schoolYear) {
            this.schoolYear = schoolYear;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public String getTeacherEmailId() {
            return teacherEmailId;
        }

        public void setTeacherEmailId(String teacherEmailId) {
            this.teacherEmailId = teacherEmailId;
        }

        public String getTeacherId() {
            return teacherId;
        }

        public void setTeacherId(String teacherId) {
            this.teacherId = teacherId;
        }

        public String getTeacherMobileNo() {
            return teacherMobileNo;
        }

        public void setTeacherMobileNo(String teacherMobileNo) {
            this.teacherMobileNo = teacherMobileNo;
        }

        public String getTeacherName() {
            return teacherName;
        }

        public void setTeacherName(String teacherName) {
            this.teacherName = teacherName;
        }

        public List<FilesBean> getFiles() {
            return files;
        }

        public void setFiles(List<FilesBean> files) {
            this.files = files;
        }

        public List<String> getStudentList() {
            return studentList;
        }

        public void setStudentList(List<String> studentList) {
            this.studentList = studentList;
        }

        public static class FilesBean {
            /**
             * filePath : string
             */

            private String filePath;

            public String getFilePath() {
                return filePath;
            }

            public void setFilePath(String filePath) {
                this.filePath = filePath;
            }
        }
    }


}
