/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.appointment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.appointment.past.PastAppointmentFragment;
import ca.dataready.SmartSchoolAdmin.appointment.schedule.ScheduleAppointmentFragment;
import ca.dataready.SmartSchoolAdmin.appointment.upcoming.UpcomingAppointmentFragment;
import ca.dataready.SmartSchoolAdmin.counter.BadgeCountTracker;
import info.hoang8f.android.segmented.SegmentedGroup;

import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.APPOINTMENT_ID;
import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.SCHEDULE_PUSH_NOTIFICATION_BROADCAST;


public class AppointmentMainFragment extends Fragment {

    @BindView(R.id.rb_schedule)
    RadioButton rbSchedule;
    @BindView(R.id.rb_upcoming)
    RadioButton rbUpcoming;
    @BindView(R.id.rb_past)
    RadioButton rbPast;
    @BindView(R.id.sgAppointment)
    SegmentedGroup sgAppointment;
    private String appointmentId;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_appointment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) getActivity()).setSubTitle(getString(R.string.appointment));
        BadgeCountTracker.saveAppointmentBadgeCounter(getActivity(), 0);
        Init();
    }

    private void Init() {

        rbSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (rbSchedule.isChecked())
                    InitScheduleAppointent();
            }
        });

        rbUpcoming.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (rbUpcoming.isChecked())
                    InitUpcomingAppointment();
            }
        });

        rbPast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (rbPast.isChecked())
                    InitPastAppointment();
            }
        });

        InitScheduleAppointent();
    }


    private void InitScheduleAppointent() {

        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.frame, new ScheduleAppointmentFragment()).commit();
    }

    private void InitUpcomingAppointment() {

        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.frame, new UpcomingAppointmentFragment()).commit();
    }

    private void InitPastAppointment() {

        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.frame, new PastAppointmentFragment()).commit();
    }


}
