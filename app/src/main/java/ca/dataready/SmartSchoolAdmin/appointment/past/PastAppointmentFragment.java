/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.appointment.past;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ViewAnimator;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.Listeners.OnReloadListener;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.DateFunction;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.appointment.adapter.ScheduledAdapter;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.TeacherAppointmentModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PastAppointmentFragment extends Fragment implements OnReloadListener {


    @BindView(R.id.rv_past)
    RecyclerView rvPast;
    @BindView(R.id.txt_no_past_data)
    TextView txtNoPastData;
    @BindView(R.id.past_viewAnimator)
    ViewAnimator pastViewAnimator;
    @BindView(R.id.swipeRefreshLayoutPast)
    SwipeRefreshLayout swipeRefreshLayoutPast;
    Unbinder unbinder;
    private CREDENTIAL.ResultBean entity;
    private ScheduledAdapter scheduledAdapter;
    private Call<TeacherAppointmentModel> pastCall;
    private String time,currentTime;

    public PastAppointmentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_past_appointment, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) getActivity()).setOnReloadListener(this);
        Init();
    }


    private void Init() {

        rvPast.setLayoutManager(new LinearLayoutManager(getActivity()));
        swipeRefreshLayoutPast.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (isVisible()) {
                    if (entity != null) {
                        getPastAppointment();
                    } else {
                        pastViewAnimator.setDisplayedChild(2);
                    }
                }

                swipeRefreshLayoutPast.setRefreshing(false);
            }
        });


        entity = AdminApp.getInstance().getAdmin();

        if (entity != null) {
            getPastAppointment();
        } else {
            pastViewAnimator.setDisplayedChild(2);
        }
    }

    private void getPastAppointment() {

        pastViewAnimator.setDisplayedChild(0);
        pastCall = AdminApp.getInstance().getApi().getTeacherAppoinments(entity.getSchoolId(), entity.getSchoolYear(), entity.getId());
        pastCall.enqueue(new Callback<TeacherAppointmentModel>() {
            @Override
            public void onResponse(Call<TeacherAppointmentModel> call, Response<TeacherAppointmentModel> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                           ArrayList<TeacherAppointmentModel.ResultBean> pastResults = new ArrayList<TeacherAppointmentModel.ResultBean>();
                            List<TeacherAppointmentModel.ResultBean> results = response.body().getResult();
                            List<String> headers = new ArrayList<>();

                            if (results != null && results.size() > 0) {

                                Collections.sort(results, new Comparator<TeacherAppointmentModel.ResultBean>() {
                                    public int compare(TeacherAppointmentModel.ResultBean o1, TeacherAppointmentModel.ResultBean o2) {

                                        if (o2.getAppointmentDate() == null || o1.getAppointmentDate() == null)
                                            return 0;

                                        return o2.getAppointmentDate().compareTo(o1.getAppointmentDate());
                                    }
                                });


                                scheduledAdapter = new ScheduledAdapter(getActivity());
                                rvPast.setAdapter(scheduledAdapter);

                                Log.e("Appointment Size ", "" + results.size());
                                for (int i = 0; i < results.size(); i++) {

                                    TeacherAppointmentModel.ResultBean bean = results.get(i);

                                    if (bean.getAppointmentDate() != null) {

                                        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
                                        DateTime serviceDate = fmt.parseDateTime(bean.getAppointmentDate());
                                        DateTime CurrentDate = fmt.parseDateTime(AdminApp.getInstance().currentDate());

                                        if (serviceDate.isBefore(CurrentDate) || serviceDate.isEqual(CurrentDate)) {

                                            if (bean.getAppointmentTime().contains("-")) {
                                                String[] aa = bean.getAppointmentTime().split("-");
                                                time = aa[0].trim();
                                                Log.e("TIME", time);

                                                Calendar cal = Calendar.getInstance(TimeZone.getDefault());
                                                Date currentLocalTime = cal.getTime();
                                                DateFormat date = new SimpleDateFormat("hh:mm a");
                                                date.setTimeZone(TimeZone.getDefault());

                                                currentTime = date.format(currentLocalTime);

                                            } else {

                                                time = bean.getAppointmentTime();
                                                Calendar cal = Calendar.getInstance(TimeZone.getDefault());
                                                Date currentLocalTime = cal.getTime();
                                                DateFormat date = new SimpleDateFormat("hh:mm a");
                                                date.setTimeZone(TimeZone.getDefault());

                                                currentTime = date.format(currentLocalTime);


                                            }

                                            String finalDate = DateFunction.ConvertDate(bean.getAppointmentDate(), "yyyy-MM-dd", "dd MMMM yyyy");

                                            if (finalDate.contains(" ")) {
                                                String[] pDates = finalDate.split(" ");
                                                if (pDates.length > 2) {
                                                    String month = pDates[1];
                                                    String year = pDates[2];
                                                    if (!headers.contains(month)) {
                                                        if (time != null && currentTime != null) {
                                                            if (serviceDate.isEqual(CurrentDate)) {
                                                                if (checkTimeForPast(time, currentTime)) {
                                                                    headers.add(month);
                                                                    TeacherAppointmentModel.ResultBean entity = new TeacherAppointmentModel.ResultBean();
                                                                    entity.setAppointmentDate(bean.getAppointmentDate());
                                                                    entity.setType(1);
                                                                    entity.setMonth(month + " " + year);
                                                                    pastResults.add(entity);
                                                                    pastResults.add(bean);
                                                                }
                                                            } else {
                                                                headers.add(month);
                                                                TeacherAppointmentModel.ResultBean entity = new TeacherAppointmentModel.ResultBean();
                                                                entity.setAppointmentDate(bean.getAppointmentDate());
                                                                entity.setType(1);
                                                                entity.setMonth(month + " " + year);
                                                                pastResults.add(entity);
                                                                pastResults.add(bean);
                                                            }
                                                        }
                                                    } else {
                                                        if (time != null && currentTime != null) {
                                                            if (serviceDate.isEqual(CurrentDate)) {
                                                                if (checkTimeForPast(time, currentTime))
                                                                    pastResults.add(bean);
                                                            } else {
                                                                pastResults.add(bean);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }

                                if (pastResults.size() > 0) {
                                    pastViewAnimator.setDisplayedChild(1);
                                    scheduledAdapter.addItem(pastResults);
                                } else {
                                    pastViewAnimator.setDisplayedChild(2);
                                }

                            } else {

                                pastViewAnimator.setDisplayedChild(2);
                            }


                        } else {

                            pastViewAnimator.setDisplayedChild(2);
                            txtNoPastData.setText(response.body().getMessage());
                        }

                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_PAST_APPOINMENT);
                        pastViewAnimator.setDisplayedChild(2);
                        txtNoPastData.setText(error.message());
                    }
                } catch (Exception e) {

                    pastViewAnimator.setDisplayedChild(2);
                    txtNoPastData.setText(getString(R.string.somethingwrong));
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<TeacherAppointmentModel> call, Throwable t) {
                if (!call.isCanceled()) {
                    System.out.println(t.getMessage());
                    pastViewAnimator.setDisplayedChild(2);
                    txtNoPastData.setText(App_Constants.NO_INTERNET);
                }
            }
        });
    }

    private boolean checkTimeForPast(String time, String currenTime) {

        try {

            DateTimeFormatter fmt = DateTimeFormat.forPattern("hh:mm a");
            DateTime serverTime = fmt.parseDateTime(time);
            DateTime currentTime = fmt.parseDateTime(currenTime);

            if (currentTime.isAfter(serverTime)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {

            e.printStackTrace();
        }


        return false;
    }

    @Override
    public void onStop() {
        super.onStop();
        if(pastCall!=null)
            pastCall.cancel();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onReload(String apiName) {

        if (isVisible()) {

            switch (apiName) {
                case App_Constants.API_PAST_APPOINMENT:
                    getPastAppointment();
                    break;

            }

        }
    }
}
