/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.appointment.upcoming;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ViewAnimator;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.Listeners.OnAppointmentConfirmationListener;
import ca.dataready.SmartSchoolAdmin.Listeners.OnReloadListener;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.DateFunction;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.appointment.adapter.UpcomingAdapter;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.AppointmentConfirmResponse;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.TeacherAppointmentModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.SCHEDULE_PUSH_NOTIFICATION_BROADCAST;


public class UpcomingAppointmentFragment extends Fragment implements OnReloadListener, OnAppointmentConfirmationListener {


    @BindView(R.id.rv_upcoming)
    RecyclerView rvUpcoming;
    @BindView(R.id.txt_no_upcoming_data)
    TextView txtNoUpcomingData;
    @BindView(R.id.upcoming_viewAnimator)
    ViewAnimator upcomingViewAnimator;
    @BindView(R.id.swipeRefreshLayoutUpcoming)
    SwipeRefreshLayout swipeRefreshLayoutUpcoming;
    Unbinder unbinder;
    CREDENTIAL.ResultBean entity;
    private Call<TeacherAppointmentModel> upcomingCall;
    private UpcomingAdapter upcomingAdapter;
    private String time, currentTime;
    private String aId;
    private ProgressDialog progressDialog;
    private String appointmentId;
    private int adapterPos;
    private TeacherAppointmentModel.ResultBean Appointmententity;
    private Call<AppointmentConfirmResponse> cancelCall;
    private Call<AppointmentConfirmResponse> confirmCall;
    public static boolean isScheduleVisible = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_upcoming, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mAppointmentPushNotificationReceiver,
                new IntentFilter(SCHEDULE_PUSH_NOTIFICATION_BROADCAST));
        ((HomeActivity) getActivity()).setOnReloadListener(this);
        Init();
    }

    private BroadcastReceiver mAppointmentPushNotificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (isVisible() && entity != null) {
                getUpCommingAppointment();
            } else {
                upcomingViewAnimator.setDisplayedChild(2);
            }

        }
    };

    private void Init() {

        rvUpcoming.setLayoutManager(new LinearLayoutManager(getActivity()));
        swipeRefreshLayoutUpcoming.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (isVisible()) {

                    if (entity != null) {
                        getUpCommingAppointment();
                    } else {
                        upcomingViewAnimator.setDisplayedChild(2);
                    }

                    swipeRefreshLayoutUpcoming.setRefreshing(false);
                }

            }
        });

        entity = AdminApp.getInstance().getAdmin();

        if (entity != null) {
            getUpCommingAppointment();
        } else {
            upcomingViewAnimator.setDisplayedChild(2);
        }
    }


    private void getUpCommingAppointment() {

        upcomingViewAnimator.setDisplayedChild(0);
        upcomingCall = AdminApp.getInstance().getApi().getTeacherAppoinments(entity.getSchoolId(), entity.getSchoolYear(), entity.getId());
        upcomingCall.enqueue(new Callback<TeacherAppointmentModel>() {
            @Override
            public void onResponse(Call<TeacherAppointmentModel> call, Response<TeacherAppointmentModel> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {


                            List<TeacherAppointmentModel.ResultBean> results = response.body().getResult();
                            List<TeacherAppointmentModel.ResultBean> upcomingResults = new ArrayList<>();
                            List<String> headers = new ArrayList<>();

                            if (results != null && results.size() > 0) {

                                Collections.sort(results, new Comparator<TeacherAppointmentModel.ResultBean>() {
                                    public int compare(TeacherAppointmentModel.ResultBean o1, TeacherAppointmentModel.ResultBean o2) {

                                        if (o2.getAppointmentDate() == null || o1.getAppointmentDate() == null)
                                            return 0;

                                        return o2.getAppointmentDate().compareTo(o1.getAppointmentDate());
                                    }
                                });

                                upcomingAdapter = new UpcomingAdapter(getActivity());
                                rvUpcoming.setAdapter(upcomingAdapter);
                                upcomingAdapter.setOnAppointmentConfirmationListener(UpcomingAppointmentFragment.this);

                                Log.e("Appointment Size ", "" + results.size());
                                for (int i = 0; i < results.size(); i++) {

                                    TeacherAppointmentModel.ResultBean bean = results.get(i);

                                    if (bean.getAppointmentDate() != null && bean.getAppointmentTime() != null) {

                                        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
                                        DateTime serviceDate = fmt.parseDateTime(bean.getAppointmentDate());
                                        DateTime CurrentDate = fmt.parseDateTime(AdminApp.getInstance().currentDate());

                                        if (serviceDate.isAfter(CurrentDate) || serviceDate.isEqual(CurrentDate)) {

                                            if (bean.getAppointmentTime().contains("-")) {
                                                String[] aa = bean.getAppointmentTime().split("-");
                                                time = aa[0].trim();
                                                Log.e("TIME", time);

                                                Calendar cal = Calendar.getInstance(TimeZone.getDefault());
                                                Date currentLocalTime = cal.getTime();
                                                DateFormat date = new SimpleDateFormat("hh:mm a");
                                                date.setTimeZone(TimeZone.getDefault());

                                                currentTime = date.format(currentLocalTime);

                                            } else {

                                                time = bean.getAppointmentTime();
                                                Calendar cal = Calendar.getInstance(TimeZone.getDefault());
                                                Date currentLocalTime = cal.getTime();
                                                DateFormat date = new SimpleDateFormat("hh:mm a");
                                                date.setTimeZone(TimeZone.getDefault());

                                                currentTime = date.format(currentLocalTime);

                                            }

                                            String finalDate = DateFunction.ConvertDate(bean.getAppointmentDate(), "yyyy-MM-dd", "dd MMMM yyyy");

                                            if (finalDate.contains(" ")) {
                                                String[] pDates = finalDate.split(" ");
                                                if (pDates.length > 2) {
                                                    String month = pDates[1];
                                                    String year = pDates[2];
                                                    if (!headers.contains(month)) {
                                                        if (time != null && currentTime != null) {
                                                            if (serviceDate.isEqual(CurrentDate)) {
                                                                if (checkTimeForUpcoming(time, currentTime)) {
                                                                    headers.add(month);
                                                                    TeacherAppointmentModel.ResultBean entity = new TeacherAppointmentModel.ResultBean();
                                                                    entity.setAppointmentDate(bean.getAppointmentDate());
                                                                    entity.setType(1);
                                                                    entity.setMonth(month + " " + year);
                                                                    upcomingResults.add(entity);
                                                                    upcomingResults.add(bean);
                                                                }
                                                            } else {
                                                                headers.add(month);
                                                                TeacherAppointmentModel.ResultBean entity = new TeacherAppointmentModel.ResultBean();
                                                                entity.setAppointmentDate(bean.getAppointmentDate());
                                                                entity.setType(1);
                                                                entity.setMonth(month + " " + year);
                                                                upcomingResults.add(entity);
                                                                upcomingResults.add(bean);
                                                            }
                                                        }
                                                    } else {
                                                        if (time != null && currentTime != null) {
                                                            if (serviceDate.isEqual(CurrentDate)) {
                                                                if (checkTimeForUpcoming(time, currentTime))
                                                                    upcomingResults.add(bean);
                                                            } else {
                                                                upcomingResults.add(bean);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }


                                if (upcomingResults.size() > 0) {
                                    upcomingViewAnimator.setDisplayedChild(1);
                                    upcomingAdapter.addItem(upcomingResults);
                                } else {
                                    upcomingViewAnimator.setDisplayedChild(2);
                                }


                            } else {

                                upcomingViewAnimator.setDisplayedChild(2);
                            }


                        } else {

                            upcomingViewAnimator.setDisplayedChild(2);
                            txtNoUpcomingData.setText(response.body().getMessage());
                        }

                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_UPCOMING_APPOINMENT);
                        upcomingViewAnimator.setDisplayedChild(2);
                        txtNoUpcomingData.setText(error.message());
                    }
                } catch (Exception e) {

                    upcomingViewAnimator.setDisplayedChild(2);
                    AdminApp.getInstance().ToastSomethingWrong();
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<TeacherAppointmentModel> call, Throwable t) {
                if (!call.isCanceled()) {
                    System.out.println(t.getMessage());
                    upcomingViewAnimator.setDisplayedChild(2);
                    txtNoUpcomingData.setText(App_Constants.NO_INTERNET);

                }
            }
        });
    }

    private boolean checkTimeForUpcoming(String time, String currenTime) {

        try {

            LocalTime serverTime = DateTimeFormat.forPattern("hh:mm a").withLocale(Locale.ENGLISH).parseLocalTime(time);
            LocalTime currentTime = DateTimeFormat.forPattern("hh:mm a").withLocale(Locale.ENGLISH).parseLocalTime(currenTime);

            if (serverTime.isAfter(currentTime)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {

            e.printStackTrace();
        }


        return false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onReload(String apiName) {

        if (isVisible()) {

            switch (apiName) {
                case App_Constants.API_UPCOMING_APPOINMENT:
                    getUpCommingAppointment();
                    break;
                case App_Constants.API_CANCEL_APPOINTMENT:
                    onAppointmentCancel(appointmentId, adapterPos, Appointmententity);
                    break;
                case App_Constants.API_CONFIRM_APPOINMENT:
                    onAppointmentConfirm(appointmentId, adapterPos, Appointmententity);
                    break;
            }

        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (upcomingCall != null)
            upcomingCall.cancel();

        if (confirmCall != null)
            confirmCall.cancel();

        if (cancelCall != null)
            cancelCall.cancel();
    }


    @Override
    public void onAppointmentCancel(String aId, final int adapterPosition, final TeacherAppointmentModel.ResultBean entity) {

        appointmentId = aId;
        adapterPos = adapterPosition;
        Appointmententity = entity;

        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle(getString(R.string.alert));
        alertDialog.setMessage(getString(R.string.sure_to_delete_appointment));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();

                        showProgress();

                        cancelCall = AdminApp.getInstance().getApi().cancelAppointment(appointmentId);
                        cancelCall.enqueue(new Callback<AppointmentConfirmResponse>() {
                            @Override
                            public void onResponse(Call<AppointmentConfirmResponse> call, Response<AppointmentConfirmResponse> response) {

                                try {
                                    if (response.isSuccessful()) {
                                        if (response.body().isStatus()) {


                                            entity.setStatus("cancelled");
                                            if (upcomingAdapter != null)
                                                upcomingAdapter.notifyItemChanged(adapterPosition, entity);

                                            Utility.showSnackBar(upcomingViewAnimator, response.body().getMessage());

                                        } else {

                                            Utility.showSnackBar(upcomingViewAnimator, response.body().getMessage());
                                        }
                                    } else {

                                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_CANCEL_APPOINTMENT);
                                        Utility.showSnackBar(upcomingViewAnimator, error.message());
                                    }
                                } catch (Exception e) {

                                    e.printStackTrace();
                                    Utility.showSnackBar(upcomingViewAnimator, getString(R.string.somethingwrong));
                                }
                                hideProgress();
                            }

                            @Override
                            public void onFailure(Call<AppointmentConfirmResponse> call, Throwable t) {
                                if (!call.isCanceled()) {
                                    hideProgress();
                                    Utility.showSnackBar(upcomingViewAnimator, App_Constants.NO_INTERNET);
                                }
                            }
                        });

                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });
        alertDialog.show();

    }

    @Override
    public void onAppointmentConfirm(String aId, final int adapterPosition, final TeacherAppointmentModel.ResultBean entity) {

        appointmentId = aId;
        adapterPos = adapterPosition;
        Appointmententity = entity;

        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle(getString(R.string.alert));
        alertDialog.setMessage(getString(R.string.sure_to_confirm_appointment));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();

                        showProgress();
                        confirmCall = AdminApp.getInstance().getApi().confirmAppointment(appointmentId);
                        confirmCall.enqueue(new Callback<AppointmentConfirmResponse>() {
                            @Override
                            public void onResponse(Call<AppointmentConfirmResponse> call, Response<AppointmentConfirmResponse> response) {

                                try {
                                    if (response.isSuccessful()) {
                                        if (response.body().isStatus()) {

                                            AdminApp.getInstance().addEventToDeviceCalander(entity);
                                            entity.setStatus("confirmed");
                                            if (upcomingAdapter != null)
                                                upcomingAdapter.notifyItemChanged(adapterPosition, entity);

                                            Utility.showSnackBar(upcomingViewAnimator, response.body().getMessage());

                                        } else {

                                            Utility.showSnackBar(upcomingViewAnimator, response.body().getMessage());
                                        }
                                    } else {

                                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_CONFIRM_APPOINMENT);
                                        Utility.showSnackBar(upcomingViewAnimator, error.message());
                                    }
                                } catch (Exception e) {

                                    Utility.showSnackBar(upcomingViewAnimator, getString(R.string.somethingwrong));
                                }
                                hideProgress();
                            }

                            @Override
                            public void onFailure(Call<AppointmentConfirmResponse> call, Throwable t) {
                                if (!call.isCanceled()) {
                                    hideProgress();
                                    Utility.showSnackBar(upcomingViewAnimator, App_Constants.NO_INTERNET);
                                }
                            }
                        });

                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });
        alertDialog.show();
    }


    private void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    private void showProgress() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.processing));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void onPause() {
        super.onPause();
        isScheduleVisible = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        isScheduleVisible = true;
    }
}
