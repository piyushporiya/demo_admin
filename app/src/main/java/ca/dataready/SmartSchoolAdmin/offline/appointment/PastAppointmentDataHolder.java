/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.offline.appointment;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import ca.dataready.SmartSchoolAdmin.offline.OfflineConstans;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.TeacherAppointmentModel;


/**
 * Created by social_jaydeep on 13/10/17.
 */

public class PastAppointmentDataHolder {

    public static void savePastAppointments(Context context, List<TeacherAppointmentModel.ResultBean> model) {

        String s = new Gson().toJson(model);
        SharedPreferences preferences = context.getSharedPreferences(OfflineConstans.OFFLINE_PREFERENCES, Context.MODE_PRIVATE);
        preferences.edit().putString(OfflineConstans.PAST_APPOINTMENT_DATA, s).apply();

    }

    public static List<TeacherAppointmentModel.ResultBean> getPastAppointments(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(OfflineConstans.OFFLINE_PREFERENCES, Context.MODE_PRIVATE);
        String s = preferences.getString(OfflineConstans.PAST_APPOINTMENT_DATA, "");
        Gson gson = new Gson();
        Type type = new TypeToken<List<TeacherAppointmentModel.ResultBean>>() {
        }.getType();
        return gson.fromJson(s, type);
    }
}
