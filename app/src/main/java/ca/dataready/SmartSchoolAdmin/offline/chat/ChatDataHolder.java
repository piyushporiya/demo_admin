/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.offline.chat;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import ca.dataready.SmartSchoolAdmin.offline.OfflineConstans;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.ThreadCommunicationModel;


/**
 * Created by social_jaydeep on 13/10/17.
 */

public class ChatDataHolder {

    public static void saveChat(Context context, List<ThreadCommunicationModel.ResultBean> model, String messageId) {

        String s = new Gson().toJson(model);
        SharedPreferences preferences = context.getSharedPreferences(OfflineConstans.OFFLINE_PREFERENCES, Context.MODE_PRIVATE);
        preferences.edit().putString(messageId, s).apply();

    }

    public static List<ThreadCommunicationModel.ResultBean> getChat(Context context, String messageId) {
        SharedPreferences preferences = context.getSharedPreferences(OfflineConstans.OFFLINE_PREFERENCES, Context.MODE_PRIVATE);
        String s = preferences.getString(messageId, "");
        Gson gson = new Gson();
        Type type = new TypeToken<List<ThreadCommunicationModel.ResultBean>>() {
        }.getType();
        return gson.fromJson(s, type);
    }

}
