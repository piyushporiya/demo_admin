/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.offline;

/**
 * Created by social_jaydeep on 10/10/17.
 */

public class OfflineConstans {

    public static final String OFFLINE_PREFERENCES = "offline_prefs";

    public static final String VIEW_CREATE_APPOINTMENT_DATA = "view_create_appointment_data";
    public static final String UPCOMING_APPOINTMENT_DATA = "upcoming_appointment_data";
    public static final String PAST_APPOINTMENT_DATA = "past_appointment_data";
    public static final String VIEW_MESSAGES_DATA = "view_message_data";
    public static final String STUDENT_PROFILE_DATA = "student_profile_data_of_";
    public static final String CREDIT_CARD_LIST_DATA = "credit_card_data_of_";
    public static final String COMMUNICATION_WITH_SCHOOL = "comm_with_school_of_";
    public static final String COMMUNICATION_WITH_TEACHER = "comm_with_teacher_of_";
    public static final String DASHBOARD_DATA = "dashboard_data_of_";
    public static final String NOTIFICATION_DATA = "notification_data_of_";
}
