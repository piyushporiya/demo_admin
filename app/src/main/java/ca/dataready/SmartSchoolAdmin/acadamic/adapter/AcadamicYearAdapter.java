/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.acadamic.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.acadamic.AcadamicYearFragment;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.grade.adapter.ClassAdapter;
import ca.dataready.SmartSchoolAdmin.server.GetYearListResponse;

public class AcadamicYearAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private int gridCount;
    private AcadamicYearFragment fragment;
    private List<GetYearListResponse.ResultBean> beans;
    private ClassAdapter adapter;

    public AcadamicYearAdapter(Context context, AcadamicYearFragment fragment) {
        this.context = context;
        this.gridCount = gridCount;
        this.fragment = fragment;
        beans = new ArrayList<>();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new ItemViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_acadamic_year, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {

        final GetYearListResponse.ResultBean bean = beans.get(position);

        if (holder instanceof ItemViewHolder) {

            ((ItemViewHolder) holder).txtYear.setText(bean.getSchoolYear());
            ((ItemViewHolder) holder).txtYearEndDate.setText(bean.getYearEndDate());
            ((ItemViewHolder) holder).txtYearStartDate.setText(bean.getYearStartDate());
            ((ItemViewHolder) holder).status.setVisibility(bean.isActive() ? View.VISIBLE:View.GONE);

            ((ItemViewHolder) holder).imgMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    fragment.showPopUp(((ItemViewHolder) holder).imgMore,holder.getAdapterPosition(),bean);
                }
            });

        }


    }

    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }

    public void addItems(List<GetYearListResponse.ResultBean> data) {

        beans.addAll(data);
        notifyDataSetChanged();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.txt_year)
        TextView txtYear;
        @BindView(R.id.txt_year_start_date)
        TextView txtYearStartDate;
        @BindView(R.id.txt_year_end_date)
        TextView txtYearEndDate;
        @BindView(R.id.img_more)
        ImageView imgMore;
        @BindView(R.id.card_grade)
        CardView cardGrade;
        @BindView(R.id.status)
        View status;
       // R.layout.raw_acadamic_year

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
