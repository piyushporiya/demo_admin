/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.acadamic;


import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.Listeners.OnReloadListener;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.acadamic.adapter.AcadamicYearAdapter;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.AddUpdateAcademicYearParams;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.CommonAcadamicYearResponse;
import ca.dataready.SmartSchoolAdmin.server.EndSchoolYearParams;
import ca.dataready.SmartSchoolAdmin.server.GetYearListResponse;
import ca.dataready.SmartSchoolAdmin.server.SetActiveYearParams;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AcadamicYearFragment extends Fragment implements OnReloadListener, SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.recyclerView)
    SuperRecyclerView recyclerView;
    @BindView(R.id.fab_create)
    FloatingActionButton fabCreate;
    Unbinder unbinder;
    private GridLayoutManager gridLayoutManager;
    private Call<GetYearListResponse> yearListCall;
    private List<GetYearListResponse.ResultBean> yearBeans;
    private AcadamicYearAdapter adapter;
    private String schoolYearId, schoolYear;
    private Call<CommonAcadamicYearResponse> activeYearCall;
    private String startDate, endDate;
    private String activeYearId;
    private Call<CommonAcadamicYearResponse> endYearCall;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_acadamic_year, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) getActivity()).setSubTitle(getString(R.string.acedmic_year));
        ((HomeActivity) getActivity()).setOnReloadListener(this);
        Init();
    }

    private void Init() {

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            gridLayoutManager = new GridLayoutManager(getContext(), 4);
        } else {
            gridLayoutManager = new GridLayoutManager(getContext(), 3);
        }

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setRefreshListener(AcadamicYearFragment.this);

        getYearList();
    }

    private void getYearList() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();
        if (entity == null)
            return;

        yearListCall = AdminApp.getInstance().getApi().getYearList(entity.getSchoolId());
        yearListCall.enqueue(new Callback<GetYearListResponse>() {
            @Override
            public void onResponse(Call<GetYearListResponse> call, Response<GetYearListResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            yearBeans = response.body().getResult();
                            if (yearBeans != null && yearBeans.size() > 0) {
                                setAdapter();
                            } else {
                                adapter = new AcadamicYearAdapter(getActivity(), AcadamicYearFragment.this);
                                recyclerView.setAdapter(adapter);
                                TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                                emptyView.setText(getString(R.string.no_data));
                            }
                        } else {

                            adapter = new AcadamicYearAdapter(getActivity(), AcadamicYearFragment.this);
                            recyclerView.setAdapter(adapter);
                            TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                            emptyView.setText(response.body().getMessage());
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_GET_YEAR_LIST);
                        adapter = new AcadamicYearAdapter(getActivity(), AcadamicYearFragment.this);
                        recyclerView.setAdapter(adapter);
                        TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                        emptyView.setText(error.message());

                    }
                } catch (Exception e) {
                    adapter = new AcadamicYearAdapter(getActivity(), AcadamicYearFragment.this);
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(getString(R.string.somethingwrong));
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<GetYearListResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    System.out.println(t.getMessage());
                    adapter = new AcadamicYearAdapter(getActivity(), AcadamicYearFragment.this);
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(App_Constants.NO_INTERNET);

                }
            }
        });

    }

    private void setAdapter() {

        adapter = new AcadamicYearAdapter(getActivity(), AcadamicYearFragment.this);
        recyclerView.setAdapter(adapter);

        if(yearBeans!=null&& yearBeans.size()>0) {

            for (GetYearListResponse.ResultBean bean : yearBeans) {

                if (bean.getSchoolYear().equals(AdminApp.getInstance().getAdmin().getSchoolYear())) {
                    bean.setActive(true);
                } else {
                    bean.setActive(false);
                }
            }
        }

        adapter.addItems(yearBeans);
    }

    public void showPopUp(ImageView imgMore, int position, GetYearListResponse.ResultBean bean) {

        PopupMenu popup = new PopupMenu(imgMore.getContext(), imgMore);
        MenuInflater inflater = popup.getMenuInflater();

        // if (bean.getStatus().equals(App_Constants.YEAR_ACTIVE) || bean.getStatus().equals(App_Constants.YEAR_COMPLETED))
        //inflater.inflate(R.menu.edit_acadamic_year_active_menu, popup.getMenu());
        // else
        inflater.inflate(R.menu.edit_acadamic_year_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new CardMenuItemClickListener(position, bean));
        popup.show();
    }

    private class CardMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        private int position;
        private GetYearListResponse.ResultBean bean;

        CardMenuItemClickListener(int positon, GetYearListResponse.ResultBean bean) {
            this.position = positon;
            this.bean = bean;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {

                case R.id.item_edit:

                    addUpdateAcadamicYearDialog(App_Constants.UPDATE_ACADAMIC_YEAR, bean);

                    break;

                case R.id.item_active:

                   /* schoolYear = bean.getSchoolYear();
                    schoolYearId = bean.getId();

                    for (GetYearListResponse.ResultBean bean : yearBeans) {

                        if (bean.getStatus().equals(App_Constants.YEAR_ACTIVE)) {
                            activeYearId = bean.getId();
                            break;
                        }
                    }

                    if (activeYearId != null)
                        setActiveYearDialog();
                    else {
                        Utility.showProgress(getActivity(),getString(R.string.processing));
                        setActiveYear();
                    }*/

                    CREDENTIAL credential = AdminApp.getInstance().getCredential();
                    credential.getResult().get(0).setSchoolYear(bean.getSchoolYear());
                    AdminApp.getInstance().saveCredential(credential);

                    ((HomeActivity) getActivity()).setTitle();
                    setAdapter();

                    break;

                default:

                    break;
            }
            return false;
        }
    }

    private void setActiveYearDialog() {


        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle(getString(R.string.alert));
        alertDialog.setMessage(getString(R.string.want_to_complte_year));

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                alertDialog.dismiss();
                endSchoolYear();
            }
        });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                alertDialog.dismiss();

            }
        });

        alertDialog.show();

    }

    private void endSchoolYear() {

        EndSchoolYearParams params = new EndSchoolYearParams();
        params.setId(activeYearId);
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
        params.setSchoolYear(schoolYear);

        Utility.showProgress(getActivity(), getString(R.string.processing));

        endYearCall = AdminApp.getInstance().getApi().endSchoolYear(params);
        endYearCall.enqueue(new Callback<CommonAcadamicYearResponse>() {
            @Override
            public void onResponse(Call<CommonAcadamicYearResponse> call, Response<CommonAcadamicYearResponse> response) {
                try {
                    if (response.isSuccessful()) {

                        if (response.body().isStatus()) {
                            setActiveYear();
                        } else {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Utility.hideProgress();
                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_END_YEAR);
                        Toast.makeText(getActivity(), error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Utility.hideProgress();
                    e.printStackTrace();
                    Toast.makeText(getActivity(), getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CommonAcadamicYearResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(getActivity(), App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void setActiveYear() {

        SetActiveYearParams params = new SetActiveYearParams();
        params.setId(schoolYearId);
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
        params.setSchoolYear(schoolYear);

        activeYearCall = AdminApp.getInstance().getApi().setActiveYear(params);
        activeYearCall.enqueue(new Callback<CommonAcadamicYearResponse>() {
            @Override
            public void onResponse(Call<CommonAcadamicYearResponse> call, Response<CommonAcadamicYearResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {

                        if (response.body().isStatus()) {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            getYearList();
                        } else {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_SET_ACTIVE_YEAR);
                        Toast.makeText(getActivity(), error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CommonAcadamicYearResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(getActivity(), App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    @OnClick({R.id.fab_create})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.fab_create:

                addUpdateAcadamicYearDialog(App_Constants.ADD_ACADAMIC_YEAR, null);

                break;
        }
    }

    private void addUpdateAcadamicYearDialog(final String type, final GetYearListResponse.ResultBean bean) {

        final View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_add_academic_year, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if (type.equals(App_Constants.ADD_ACADAMIC_YEAR))
            builder.setTitle(R.string.add_acadamic_year);
        else
            builder.setTitle(R.string.update_acadamic_year);
        builder.setView(view);
        builder.setCancelable(true);

        final EditText etYearStartDate = (EditText) view.findViewById(R.id.et_year_start_date);
        final EditText etYearEndDate = (EditText) view.findViewById(R.id.et_year_end_date);

        if (!type.equals(App_Constants.ADD_ACADAMIC_YEAR)) {

            etYearStartDate.setText(bean.getYearStartDate());
            etYearStartDate.setSelection(bean.getYearStartDate().length());
            etYearEndDate.setText(bean.getYearEndDate());
            etYearEndDate.setSelection(bean.getYearEndDate().length());
            schoolYearId = bean.getId();
        }

        builder.setPositiveButton(R.string.submit, null);

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {

                etYearStartDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        setDate(etYearStartDate);
                    }
                });

                etYearEndDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        setDate(etYearEndDate);
                    }
                });

                Button button = ((AlertDialog) alertDialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        if (etYearStartDate.getText().toString().isEmpty()) {
                            etYearStartDate.setError(getString(R.string.field_required));
                        } else if (etYearEndDate.getText().toString().isEmpty()) {
                            etYearEndDate.setError(getString(R.string.field_required));
                        } else {
                            alertDialog.dismiss();
                            startDate = etYearStartDate.getText().toString();
                            endDate = etYearEndDate.getText().toString();


                            if (type.equals(App_Constants.ADD_ACADAMIC_YEAR))
                                addAcedamicYear();
                            else
                                updateAcademicYear();
                        }
                    }
                });
            }
        });
        alertDialog.show();
    }


    private void addAcedamicYear() {

        AddUpdateAcademicYearParams params = new AddUpdateAcademicYearParams();
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
        params.setYearStartDate(startDate);
        params.setYearEndDate(endDate);
        params.setSchoolYear(startDate.split("-")[0] + "-" + endDate.split("-")[0]);

        Utility.showProgress(getActivity(), getString(R.string.processing));

        activeYearCall = AdminApp.getInstance().getApi().addSchoolYear(params);
        activeYearCall.enqueue(new Callback<CommonAcadamicYearResponse>() {
            @Override
            public void onResponse(Call<CommonAcadamicYearResponse> call, Response<CommonAcadamicYearResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            getYearList();
                        } else {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_ADD_ACADEMIC_YEAR);
                        Toast.makeText(getActivity(), error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CommonAcadamicYearResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(getActivity(), App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    private void updateAcademicYear() {

        AddUpdateAcademicYearParams params = new AddUpdateAcademicYearParams();
        params.setId(schoolYearId);
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
        params.setYearStartDate(startDate);
        params.setYearEndDate(endDate);
        params.setSchoolYear(startDate.split("-")[0] + "-" + endDate.split("-")[0]);

        Utility.showProgress(getActivity(), getString(R.string.processing));

        activeYearCall = AdminApp.getInstance().getApi().updateSchoolYear(params);
        activeYearCall.enqueue(new Callback<CommonAcadamicYearResponse>() {
            @Override
            public void onResponse(Call<CommonAcadamicYearResponse> call, Response<CommonAcadamicYearResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            getYearList();
                        } else {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_UPDATE_ACADEMIC_YEAR);
                        Toast.makeText(getActivity(), error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CommonAcadamicYearResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(getActivity(), App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });


    }


    private void setDate(final EditText editText) {

        final Calendar mCalender = Calendar.getInstance();

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub

                mCalender.set(Calendar.YEAR, year);
                mCalender.set(Calendar.MONTH, monthOfYear);
                mCalender.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                editText.setText(sdf.format(mCalender.getTime()));

            }

        };

        DatePickerDialog dialog = new DatePickerDialog(getActivity()
                , R.style.datepickerCustom, date, mCalender.get(Calendar.YEAR), mCalender.get(Calendar.MONTH),
                mCalender.get(Calendar.DAY_OF_MONTH));

        dialog.show();

    }

    @Override
    public void onReload(String apiName) {

        if (apiName != null && isVisible()) {
            if (apiName.equals(App_Constants.API_GET_YEAR_LIST)) {
                getYearList();
            } else if (apiName.equals(App_Constants.API_SET_ACTIVE_YEAR)) {
                setActiveYear();
            } else if (apiName.equals(App_Constants.API_ADD_ACADEMIC_YEAR)) {
                addAcedamicYear();
            } else if (apiName.equals(App_Constants.API_UPDATE_ACADEMIC_YEAR)) {
                updateAcademicYear();
            } else if (apiName.equals(App_Constants.API_END_YEAR)) {
                endSchoolYear();
            }
        }
    }

    @Override
    public void onRefresh() {

        if (isVisible())
            getYearList();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            gridLayoutManager = new GridLayoutManager(getContext(), 4);
        } else {
            gridLayoutManager = new GridLayoutManager(getContext(), 3);
        }

        recyclerView.setLayoutManager(gridLayoutManager);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (activeYearCall != null)
            activeYearCall.cancel();
        if (yearListCall != null)
            yearListCall.cancel();
        if (endYearCall != null)
            endYearCall.cancel();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
