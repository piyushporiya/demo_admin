/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.expired;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ViewAnimator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.login.LoginActivity;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.UpdateTokenResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ExpiredActivity extends BaseActivity {


    @BindView(R.id.viewAnimator)
    ViewAnimator viewAnimator;
    Call<UpdateTokenResponse> call;
    String apiName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expired);
        ButterKnife.bind(this);
        this.setFinishOnTouchOutside(false);
        if (getIntent().getExtras() != null) {
            apiName = getIntent().getStringExtra("api");
        }

    }


    @Override
    public void onStop() {
        super.onStop();
        APIError.status = false;
    }

    @OnClick({R.id.dismiss, R.id.update})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.dismiss:
                AdminApp.getInstance().logout();
                Intent intent = new Intent(ExpiredActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

                break;
            case R.id.update:
                serviceCall();
                break;
        }
    }

    private void serviceCall() {
        // Store values at the time of the login attempt.
        viewAnimator.setDisplayedChild(1);

        final CREDENTIAL entity = AdminApp.getInstance().getCredential();
        call = AdminApp.getInstance().getApi().updatetoken(AdminApp.getInstance().getAdmin().getEmailId(), entity.getAuthtoken(), entity.getAuthtokenexpires());

        call.enqueue(new Callback<UpdateTokenResponse>() {
            @Override
            public void onResponse(Call<UpdateTokenResponse> call, Response<UpdateTokenResponse> response) {
                viewAnimator.setDisplayedChild(0);
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            entity.setAuthtoken(response.body().getAuthtoken());
                            entity.setAuthtokenexpires(response.body().getAuthtokenexpires());
                            AdminApp.getInstance().saveCredential(entity);
                            setCallbacks();
                            //finish();
                        } else {

                            Utility.showToast(ExpiredActivity.this, response.body().getMessage());
                        }
                    } else {

                   /* APIError error = APIError.parseError(response);*/
                   /* Utill.error(ExpiredActivity.this, error.message());*/

                    }
                } catch (Exception e) {

                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<UpdateTokenResponse> call, Throwable t) {
                viewAnimator.setDisplayedChild(0);
                Utility.showSnackBar(viewAnimator, App_Constants.NO_INTERNET);
            }
        });
    }

    private void setCallbacks() {
        Intent intent = new Intent();
        intent.putExtra("api", apiName);
        setResult(Activity.RESULT_OK, intent);
        onBackPressed();
    }
}
