/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.route.history;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.malinskiy.superrecyclerview.SuperRecyclerView;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.map.HttpConnection;
import ca.dataready.SmartSchoolAdmin.Utilities.map.PathJSONParser;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.route.history.adapter.TripHistoryAdapter;
import ca.dataready.SmartSchoolAdmin.route.history.adapter.TripsOfRouteAdapter;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.AllTripsForRoute;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.GetAllRoutesResponse;
import ca.dataready.SmartSchoolAdmin.server.GetTripHistory;
import ca.dataready.SmartSchoolAdmin.server.MapModel;
import info.hoang8f.android.segmented.SegmentedGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RouteHistoryActivity extends BaseActivity implements OnMapReadyCallback {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rb_passanger_info)
    RadioButton rbPassangerInfo;
    @BindView(R.id.rb_trip_info)
    RadioButton rbTripInfo;
    @BindView(R.id.sgInfo)
    SegmentedGroup sgInfo;
    @BindView(R.id.txt_error)
    TextView txtError;
    @BindView(R.id.viewAnimator)
    ViewAnimator parentViewAnimator;
    @BindView(R.id.piRecyclerView)
    SuperRecyclerView piRecyclerView;
    @BindView(R.id.infoviewAnimator)
    ViewAnimator infoviewAnimator;
    @BindView(R.id.txtdate)
    TextView txtdate;
    @BindView(R.id.linear_date)
    RelativeLayout linearDate;
    @BindView(R.id.txt_trip)
    TextView txtTrip;
    @BindView(R.id.linear_trips)
    RelativeLayout linearTrips;
    private GetAllRoutesResponse.ResultBean bean;
    private String routeId, selectedDate;
    private Call<AllTripsForRoute> appApi;
    private ArrayList<AllTripsForRoute.ResultBean> results;
    private TripsOfRouteAdapter adapter;
    private SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private String tripId;
    private Call<GetTripHistory> tripHistoryApi;
    private TripHistoryAdapter tripHistoryAdapter;
    ArrayList<MapModel> latlongModel = new ArrayList<>();
    ArrayList<LatLng> markerPoints = new ArrayList<>();
    private String tripType;
    private ListPopupWindow listPopupWindow;
    private List<String> tripIdsList;
    private GridLayoutManager gridLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_history);
        ButterKnife.bind(this);
        setHeaderInfo();
        Init();
    }

    private void setHeaderInfo() {

        setSupportActionBar(toolbar);

        if (AdminApp.getInstance().getAdmin() != null)
            getSupportActionBar().setTitle(AdminApp.getInstance().getAdmin().getSchoolName() + " " + AdminApp.getInstance().getAdmin().getSchoolYear());
        else
            getSupportActionBar().setTitle(getString(R.string.app_name));

        getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + getString(R.string.route_history) + "</small>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void Init() {

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);

        sgInfo.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.rb_passanger_info) {
                    infoviewAnimator.setDisplayedChild(0);
                } else {
                    infoviewAnimator.setDisplayedChild(1);
                }
            }
        });

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            gridLayoutManager = new GridLayoutManager(RouteHistoryActivity.this, 3);
        } else {
            gridLayoutManager = new GridLayoutManager(RouteHistoryActivity.this, 2);
        }

        piRecyclerView.setLayoutManager(gridLayoutManager);

        tripHistoryAdapter = new TripHistoryAdapter(RouteHistoryActivity.this);
        piRecyclerView.setAdapter(tripHistoryAdapter);
        TextView emptyView = (TextView) piRecyclerView.getEmptyView().findViewById(R.id.empty);
        emptyView.setText(getString(R.string.no_data));

        if (getIntent().getExtras() != null) {

            bean = getIntent().getParcelableExtra(App_Constants.OBJECT);
            if (bean != null) {
                routeId = bean.getRouteId();
            }
        }

        Calendar mCalender = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        selectedDate = sdf.format(mCalender.getTime());
        txtdate.setText(selectedDate);

        ApiCall();
    }

    private void ApiCall() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();

        appApi = AdminApp.getInstance().getApi().getAllTripsForRoute(entity.getSchoolId(), routeId, selectedDate);
        appApi.enqueue(new Callback<AllTripsForRoute>() {
            @Override
            public void onResponse(Call<AllTripsForRoute> call, Response<AllTripsForRoute> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            results = response.body().getResult();
                        }
                    } else {
                        APIError.parseError(response, RouteHistoryActivity.this, App_Constants.API_GET_TRIPS_OF_ROUTES);
                    }
                } catch (Exception e) {
                    Toast.makeText(RouteHistoryActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
                parentViewAnimator.setDisplayedChild(1);
            }

            @Override
            public void onFailure(Call<AllTripsForRoute> call, Throwable t) {
                if (!call.isCanceled()) {
                    parentViewAnimator.setDisplayedChild(1);
                    Toast.makeText(RouteHistoryActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @OnClick({R.id.linear_date, R.id.linear_trips})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linear_date:
                setDate(txtdate);
                break;
            case R.id.linear_trips:
                show_drop_down_to_select_option(linearTrips, App_Constants.TRIP_IDS);
                break;
        }
    }

    private void show_drop_down_to_select_option(RelativeLayout layout, final String which) {

        tripIdsList = new ArrayList<>();

        if (results != null && results.size() > 0) {
            listPopupWindow = new ListPopupWindow(RouteHistoryActivity.this);
            if (which.equals(App_Constants.TRIP_IDS)) {


                for (AllTripsForRoute.ResultBean beans : results) {
                    if (!tripIdsList.contains(beans.getTripId()))
                        tripIdsList.add(beans.getTripId());
                }

                listPopupWindow.setAdapter(new ArrayAdapter(RouteHistoryActivity.this, R.layout.list_dropdown_item, tripIdsList));

            }

            listPopupWindow.setAnchorView(layout);
            listPopupWindow.setWidth(ListPopupWindow.WRAP_CONTENT);
            listPopupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);

            listPopupWindow.setModal(true);
            listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    if (which.equals(App_Constants.TRIP_IDS)) {
                        txtTrip.setText(tripIdsList.get(i));
                        tripId = tripIdsList.get(i);

                        if (infoviewAnimator.getDisplayedChild() == 0)
                            piRecyclerView.showProgress();

                        getTripHistory();
                    }
                    listPopupWindow.dismiss();

                }
            });
            listPopupWindow.show();
        } else {
            Toast.makeText(this, getString(R.string.no_trip_ids_avail), Toast.LENGTH_SHORT).show();
        }
    }

    private void setDate(final TextView textView) {

        final Calendar mCalender = Calendar.getInstance();

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub

                mCalender.set(Calendar.YEAR, year);
                mCalender.set(Calendar.MONTH, monthOfYear);
                mCalender.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                textView.setText(sdf.format(mCalender.getTime()));
                selectedDate = sdf.format(mCalender.getTime());
                txtTrip.setText(getString(R.string.select_trip_id));
                ApiCall();

            }

        };

        DatePickerDialog dialog = new DatePickerDialog(this
                , R.style.datepickerCustom, date, mCalender.get(Calendar.YEAR), mCalender.get(Calendar.MONTH),
                mCalender.get(Calendar.DAY_OF_MONTH));

        dialog.show();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == APIError.UPDATE_TOKEN && resultCode == RESULT_OK) {

            if (data != null) {
                String apiName = data.getStringExtra("api");
                onRelaod(apiName);
            }
        }
    }

    private void onRelaod(String apiName) {
        if (apiName != null) {
            if (apiName.equals(App_Constants.API_GET_TRIPS_OF_ROUTES)) {
                ApiCall();
            } else if (apiName.equals(App_Constants.API_GET_TRIP_HISTORY)) {
                getTripHistory();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (appApi != null)
            appApi.cancel();

        if (tripHistoryApi != null)
            tripHistoryApi.cancel();
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

 /*   public void showTripHistory(String tripId) {

        this.tripId = tripId;

        if (infoviewAnimator.getDisplayedChild() == 0)
            piRecyclerView.showProgress();

        getTripHistory();
    }*/

    private void getTripHistory() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();

        tripHistoryApi = AdminApp.getInstance().getApi().getTripHistory(entity.getSchoolId(), AdminApp.getInstance().getAdmin().getSchoolYear(), tripId);
        tripHistoryApi.enqueue(new Callback<GetTripHistory>() {
            @Override
            public void onResponse(Call<GetTripHistory> call, Response<GetTripHistory> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            final GetTripHistory.ResultBean results = response.body().getResult();
                            if (results != null) {

                                if (results.getBoardingDetails() != null && results.getBoardingDetails().size() > 0) {

                                    tripHistoryAdapter = new TripHistoryAdapter(RouteHistoryActivity.this);
                                    piRecyclerView.setAdapter(tripHistoryAdapter);
                                    tripHistoryAdapter.addItem(results.getBoardingDetails());

                                } else {

                                    tripHistoryAdapter = new TripHistoryAdapter(RouteHistoryActivity.this);
                                    piRecyclerView.setAdapter(tripHistoryAdapter);
                                    TextView emptyView = (TextView) piRecyclerView.getEmptyView().findViewById(R.id.empty);
                                    emptyView.setText(getString(R.string.no_data));
                                }

                                if (results.getStopage() != null && results.getStopage().size() > 0) {

                                    markerPoints.clear();
                                    latlongModel.clear();

                                    for (GetTripHistory.ResultBean.StopageBean stopageBean : results.getStopage()) {
                                        latlongModel.add(new MapModel(Double.parseDouble(stopageBean.getLatitude()), Double.parseDouble(stopageBean.getLongitude()), stopageBean.getAddress(), stopageBean.getSequenceId()));
                                    }
                                }

                                tripType = results.getTripType();
                                if (mMap != null) {
                                    mMap.clear();

                                    drawRoute();
                                    //addBusPolyline(results.getPositionDetails());

                                } else {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {

                                            drawRoute();
                                            //addBusPolyline(results.getPositionDetails());
                                        }
                                    }, 500);
                                }

                            } else {

                                tripHistoryAdapter = new TripHistoryAdapter(RouteHistoryActivity.this);
                                piRecyclerView.setAdapter(tripHistoryAdapter);
                                TextView emptyView = (TextView) piRecyclerView.getEmptyView().findViewById(R.id.empty);
                                emptyView.setText(getString(R.string.no_data));
                            }

                        } else {

                            tripHistoryAdapter = new TripHistoryAdapter(RouteHistoryActivity.this);
                            piRecyclerView.setAdapter(tripHistoryAdapter);
                            TextView emptyView = (TextView) piRecyclerView.getEmptyView().findViewById(R.id.empty);
                            emptyView.setText(getString(R.string.no_data));
                        }
                    } else {

                        APIError error = APIError.parseError(response, RouteHistoryActivity.this, App_Constants.API_GET_TRIP_HISTORY);
                        tripHistoryAdapter = new TripHistoryAdapter(RouteHistoryActivity.this);
                        piRecyclerView.setAdapter(tripHistoryAdapter);
                        TextView emptyView = (TextView) piRecyclerView.getEmptyView().findViewById(R.id.empty);
                        emptyView.setText(error.message());

                    }
                } catch (Exception e) {

                    tripHistoryAdapter = new TripHistoryAdapter(RouteHistoryActivity.this);
                    piRecyclerView.setAdapter(tripHistoryAdapter);
                    TextView emptyView = (TextView) piRecyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(getString(R.string.somethingwrong));
                }

            }

            @Override
            public void onFailure(Call<GetTripHistory> call, Throwable t) {
                if (!call.isCanceled()) {
                    tripHistoryAdapter = new TripHistoryAdapter(RouteHistoryActivity.this);
                    piRecyclerView.setAdapter(tripHistoryAdapter);
                    TextView emptyView = (TextView) piRecyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(App_Constants.NO_INTERNET);
                }
            }
        });

    }

    private void addBusPolyline(List<GetTripHistory.ResultBean.PositionDetailsBean> positionDetails) {

        if (positionDetails != null && positionDetails.size() > 0) {

            for (GetTripHistory.ResultBean.PositionDetailsBean bean : positionDetails) {
                mMap.addPolyline(new PolylineOptions()
                        .add(new LatLng(Double.parseDouble(bean.getLatitude()), Double.parseDouble(bean.getLongitude())))
                        .width(15)
                        .color(Color.BLUE).geodesic(true));
            }
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
    }


    private void drawRoute() {

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        if (latlongModel.size() > 0) {

            for (int i = 0; i < latlongModel.size(); i++) {
                MarkerOptions markerOptions = new MarkerOptions();
                LatLng latLng = new LatLng(latlongModel.get(i).getLat(), latlongModel.get(i).getLon());
                markerPoints.add(latLng);
                if (i == 0) {
                    if (tripType != null && tripType.equalsIgnoreCase(App_Constants.PICK_UP))
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                    else if (tripType != null && tripType.equalsIgnoreCase(App_Constants.DROP_OFF))
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                    else
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                } else if (i == latlongModel.size() - 1) {
                    if (tripType != null && tripType.equalsIgnoreCase(App_Constants.PICK_UP))
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_school_icon));
                    else if (tripType != null && tripType.equalsIgnoreCase(App_Constants.DROP_OFF))
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_school_green));
                    else
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_school_icon));
                } else {
                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                }
                markerOptions.position(latLng);
                markerOptions.title(latlongModel.get(i).getStopId() + "-" + latlongModel.get(i).getTitle());
                //markerOptions.snippet("place");
                mMap.addMarker(markerOptions);
                builder.include(latLng);
                addCircle(latLng);
            }


        }

        final LatLngBounds bounds = builder.build();


        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {

                float[] results = new float[1];
                Location.distanceBetween(bounds.northeast.latitude, bounds.northeast.longitude,
                        bounds.southwest.latitude, bounds.southwest.longitude, results);

                CameraUpdate cu = null;
                if (results[0] < 1000) { // distance is less than 1 km -> set to zoom level 15
                    cu = CameraUpdateFactory.newLatLngZoom(bounds.getCenter(), 14);

                } else {
                    int padding = 40; // offset from edges of the map in pixels
                    cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                }
                if (cu != null) {
                    mMap.animateCamera(cu);
                }
            }
        });


        if (latlongModel.size() > 0) {
            String url = getMapsApiDirectionsUrl();
            ReadTask downloadTask = new ReadTask();
            downloadTask.execute(url);
        }
    }

    private void addCircle(LatLng latLng) {

        CircleOptions circleOptions1 = new CircleOptions()
                .center(latLng)
                .radius(App_Constants.CIRCLE_RADIUS).strokeColor(Color.BLACK)
                .strokeWidth(1).fillColor(0x500000ff);
        mMap.addCircle(circleOptions1);

    }

    private String getMapsApiDirectionsUrl() {

        // Origin of route
        String str_origin = "origin=" + latlongModel.get(0).getLat() + "," + latlongModel.get(0).getLon();
        // Destination of route
        String str_dest = "destination=" + latlongModel.get(latlongModel.size() - 1).getLat() + "," + latlongModel.get(latlongModel.size() - 1).getLon();

        // Sensor enabled
        String sensor = "sensor=false";

        // Waypoints
        String waypoints = "";
        for (int i = 0; i < markerPoints.size(); i++) {
            LatLng point = (LatLng) markerPoints.get(i);
            if (i == 0)
                waypoints = "waypoints=";
            waypoints += point.latitude + "," + point.longitude + "|";
        }

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + waypoints;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?key=AIzaSyBaK8M0QdcKkYYid-DH0iRcpFLd_IGws40&" + parameters;

        Log.e("URL", url);

        return url;

    }


    private class ReadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                HttpConnection http = new HttpConnection();
                data = http.readUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                new ParserTask().execute(result);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }

        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }


        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {

            ArrayList<LatLng> polylinePoints;
            PolylineOptions lineOptions = null;

            if (result != null && result.size() > 0) {
                // Traversing through all the routes
                for (int i = 0; i < result.size(); i++) {
                    polylinePoints = new ArrayList<LatLng>();
                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);

                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        polylinePoints.add(position);
                    }

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(polylinePoints);
                    lineOptions.width(10);
                    lineOptions.color(Color.parseColor("#ff00ff"));
                }
            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                mMap.addPolyline(lineOptions);
            } else {
                //Toast.makeText(AdminActivity.this, getString(R.string.no_path), Toast.LENGTH_SHORT).show();
                //clearMap();
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            gridLayoutManager = new GridLayoutManager(RouteHistoryActivity.this, 3);
        } else {
            gridLayoutManager = new GridLayoutManager(RouteHistoryActivity.this, 2);
        }
        piRecyclerView.setLayoutManager(gridLayoutManager);
    }

}
