/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.route.manage.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.route.manage.ManageRouteActivity;
import ca.dataready.SmartSchoolAdmin.server.AppApi;
import ca.dataready.SmartSchoolAdmin.server.AssignPassangerParams;
import ca.dataready.SmartSchoolAdmin.server.PassengerOnStopModel;

public class PassengerOnStopAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<PassengerOnStopModel> beans = new ArrayList<>();

    public PassengerOnStopAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_passanger_on_stop, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        PassengerOnStopModel bean = beans.get(position);

        if (holder instanceof ItemViewHolder) {

            ((ItemViewHolder) holder).passangerName.setText(bean.getName());
            Glide.with(context)
                    .load(AppApi.BASE_URL + bean.getProfilePic())
                    .into(((ItemViewHolder) holder).ivStudentImage);
        }
    }

    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }

    public void removeItem(int position) {
        beans.remove(position);
        if (beans.size() == 0) {
            ((ManageRouteActivity) context).NoPassangerAtStop();
        }
        notifyItemRemoved(position);
    }

    public void addItem(List<PassengerOnStopModel> passangerList) {

        beans.addAll(passangerList);
        notifyDataSetChanged();
    }

    public List<AssignPassangerParams.PassengerListBean> getSelectedPassanger(int adapterPosition) {

        List<AssignPassangerParams.PassengerListBean> models = new ArrayList<>();
        for (int i = 0; i < beans.size(); i++) {

            if(i == adapterPosition) {

                PassengerOnStopModel bean = beans.get(i);

                AssignPassangerParams.PassengerListBean model = new AssignPassangerParams.PassengerListBean();
                model.setId(bean.getId());
                model.setProfileType(bean.getProfileType());
                model.setRouteId("");
                model.setRouteName("");
                model.setRouteType(bean.getRouteType());
                model.setStopId("");
                models.add(model);

                return models;
            }
        }

        return models;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView passangerName;
        public CircularImageView ivStudentImage;
        public RelativeLayout viewBackground, viewForeground;
        //R.layout.raw_passanger_on_stop

        public ItemViewHolder(View itemView) {
            super(itemView);
            passangerName = itemView.findViewById(R.id.txt_passanger_name);
            ivStudentImage = itemView.findViewById(R.id.iv_student_image);
            viewBackground = itemView.findViewById(R.id.view_background);
            viewForeground = itemView.findViewById(R.id.view_foreground);
        }
    }
}
