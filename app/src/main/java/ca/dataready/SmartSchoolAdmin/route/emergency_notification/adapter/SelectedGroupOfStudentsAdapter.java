/*
 * Copyright (c) 2017.                                                       
 *                                                                           
 * ***********************************************************************  
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                           
 * ________________________________________________________________________  
 *                                                                           
 *    [2010] - [2018] SripathiSolutions Corp.                                
 *    [2010] - [2018] DataReady Technology Corp.                             
 *   ________________________________________________________________________
 *                                                                           
 *    All Rights Reserved.                                                   
 *                                                                           
 *   NOTICE:  All information contained herein is, and remains               
 *   the property of DataReady Technology  and its suppliers,                
 *   if any.  The intellectual and technical concepts contained              
 *   herein are proprietary to DataReady Technology Incorporated             
 *   and its suppliers and may be covered by U.S. and Foreign Patents,       
 *   patents in process, and are protected by trade secret or copyright law. 
 *   Dissemination of this information or reproduction of this material      
 *   is strictly forbidden unless prior written permission is obtained       
 *   from DataReady Technology Incorporated.                                 
 *                                                                           
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                            
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.route.emergency_notification.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.server.AppApi;
import ca.dataready.SmartSchoolAdmin.server.GetTotalBoardingResponse;

public class SelectedGroupOfStudentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<GetTotalBoardingResponse.ResultBean> beans;
    private boolean isSingleChoice = false;

    public SelectedGroupOfStudentsAdapter(Context context) {
        this.context = context;
        beans = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new PickUpStopViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_selected_student, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        final GetTotalBoardingResponse.ResultBean bean = beans.get(position);

        if (holder instanceof PickUpStopViewHolder) {

            ((PickUpStopViewHolder) holder).txtStudentName.setText(bean.getFirstName() + " " + bean.getLastName());
            if (bean.getProfilePic() != null && !bean.getProfilePic().isEmpty()) {
                Glide.with(context)
                        .load(AppApi.BASE_URL + bean.getProfilePic())
                        .into(((PickUpStopViewHolder) holder).ivStudentImg);
            }

            ((PickUpStopViewHolder) holder).ivSelection.setVisibility(View.GONE);

            if (bean.isSelected()) {
                ((PickUpStopViewHolder) holder).ivSelection.setImageResource(R.drawable.ic_present);
            } else {
                ((PickUpStopViewHolder) holder).ivSelection.setImageResource(R.drawable.ic_normal);
            }


            ((PickUpStopViewHolder) holder).ivSelection.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                        bean.setSelected(!bean.isSelected());

                        if (bean.isSelected()) {
                            ((PickUpStopViewHolder) holder).ivSelection.setImageResource(R.drawable.ic_present);
                        } else {
                            ((PickUpStopViewHolder) holder).ivSelection.setImageResource(R.drawable.ic_normal);
                        }
                }
            });
        }
    }

    public void setSingleChoiceMode(boolean value) {

        isSingleChoice = value;
    }

    private void makeOtherUnChecked(int position) {

        for (int i = 0; i < beans.size(); i++) {

            if (i != position)
                beans.get(i).setSelected(false);
        }

        notifyDataSetChanged();
    }



    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }

    public void addItem(ArrayList<GetTotalBoardingResponse.ResultBean> results) {

        beans.addAll(results);
        notifyDataSetChanged();
    }

    public ArrayList<GetTotalBoardingResponse.ResultBean> getVisibleStudent() {

        ArrayList<GetTotalBoardingResponse.ResultBean> visibleStudentsList = new ArrayList<>();
        for (GetTotalBoardingResponse.ResultBean bean : beans) {
            visibleStudentsList.add(bean);
        }

        return visibleStudentsList;
    }


    class PickUpStopViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.my_image_view)
        CircularImageView ivStudentImg;
        @BindView(R.id.iv_Selection)
        ImageView ivSelection;
        @BindView(R.id.txt_StudentName)
        TextView txtStudentName;
        @BindView(R.id.main_Linear)
        LinearLayout mainLinear;

        // R.layout.raw_dialog_pick_up_stop

        public PickUpStopViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

