/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.route.history.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.server.AppApi;
import ca.dataready.SmartSchoolAdmin.server.GetTripHistory;
import ca.dataready.SmartSchoolAdmin.userinfo.staff.IStaffFragment;

public class TripHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private IStaffFragment fragment;
    private List<GetTripHistory.ResultBean.BoardingDetailsBean> beans;

    public TripHistoryAdapter(Context context) {
        this.context = context;
        beans = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_trip_history, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        final GetTripHistory.ResultBean.BoardingDetailsBean bean = beans.get(position);

        if (holder instanceof ItemViewHolder) {

            if (bean.getProfilePic() != null && !bean.getProfilePic().isEmpty()) {
                Glide.with(context)
                        .load(AppApi.BASE_URL + bean.getProfilePic())
                        .into(((ItemViewHolder) holder).ivDriverImage);
            }
            ((ItemViewHolder) holder).txtName.setText(bean.getFirstName() + " " + bean.getLastName());

            if (bean.getIntime() != null) {
                if (bean.getIntime().contains(" "))
                    ((ItemViewHolder) holder).txtInTime.setText("In Time : " + bean.getIntime().split(" ")[1]);
                else
                    ((ItemViewHolder) holder).txtInTime.setText("In Time : " + bean.getIntime());
            } else {
                ((ItemViewHolder) holder).txtInTime.setText("In Time : NA");
            }

            if (bean.getOuttime() != null) {
                if (bean.getOuttime().contains(" "))
                    ((ItemViewHolder) holder).txtOutTime.setText("Out Time : " + bean.getOuttime().split(" ")[1]);
                else
                    ((ItemViewHolder) holder).txtOutTime.setText("Out Time : " + bean.getOuttime());
            } else {
                ((ItemViewHolder) holder).txtOutTime.setText("Out Time : NA");
            }

        }

    }

    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }

    public void addItem(List<GetTripHistory.ResultBean.BoardingDetailsBean> results) {

        beans.addAll(results);
        notifyDataSetChanged();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.iv_driver_image)
        CircularImageView ivDriverImage;
        @BindView(R.id.txt_name)
        TextView txtName;
        @BindView(R.id.txt_in_time)
        TextView txtInTime;
        @BindView(R.id.txt_out_time)
        TextView txtOutTime;
        //R.layout.raw_trip_history

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
