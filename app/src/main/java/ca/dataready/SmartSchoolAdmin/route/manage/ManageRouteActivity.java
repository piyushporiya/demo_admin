/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.route.manage;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.RecyclerItemTouchHelper;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.Utilities.map.HttpConnection;
import ca.dataready.SmartSchoolAdmin.Utilities.map.PathJSONParser;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.route.manage.adapter.PassengerOnStopAdapter;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.AssignPassangerParams;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.CommonResponse;
import ca.dataready.SmartSchoolAdmin.server.GetAllRoutesResponse;
import ca.dataready.SmartSchoolAdmin.server.GetPassangersResponse;
import ca.dataready.SmartSchoolAdmin.server.MapModel;
import ca.dataready.SmartSchoolAdmin.server.PassengerOnStopModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManageRouteActivity extends BaseActivity implements OnMapReadyCallback, RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    private static final double CIRCLE_RADIUS = 150;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.parent_viewAnimator)
    ViewAnimator parentViewAnimator;
    @BindView(R.id.txt_passenger_count)
    TextView txtPassengerCount;
    private SupportMapFragment mapFragment;
    private GoogleMap mMap;
    ArrayList<MapModel> latlongModel = new ArrayList<>();
    ArrayList<LatLng> markerPoints = new ArrayList<>();
    List<CircleOptions> circles = new ArrayList<CircleOptions>();
    private String tripType;
    private GetAllRoutesResponse.ResultBean bean;
    private GetPassangersResponse.ResultBean results;
    private Call<GetPassangersResponse> pendingStudentsApi;
    private String routeId;
    private Call<CommonResponse> assignCall;
    List<AssignPassangerParams.PassengerListBean> passangerList = new ArrayList<>();
    private PassengerOnStopAdapter adapter;
    private ViewAnimator dialogViewAnimator;
    private Marker passengerMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_route);
        ButterKnife.bind(this);
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        setHeaderInfo();
        Init();
    }

    private void setHeaderInfo() {

        setSupportActionBar(toolbar);

        if (AdminApp.getInstance().getAdmin() != null)
            getSupportActionBar().setTitle(AdminApp.getInstance().getAdmin().getSchoolName() + " " + AdminApp.getInstance().getAdmin().getSchoolYear());
        else
            getSupportActionBar().setTitle(getString(R.string.app_name));

        getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + getString(R.string.manage_route) + "</small>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    private void Init() {

        mapFragment.getMapAsync(this);

        if (getIntent().getExtras() != null) {

            bean = getIntent().getParcelableExtra(App_Constants.OBJECT);

            if (bean != null) {

                routeId = bean.getRouteId();
                tripType = bean.getRouteType();

                if (bean.getStopage() != null && bean.getStopage().size() > 0) {

                    for (GetAllRoutesResponse.ResultBean.StopageBean stopageBean : bean.getStopage()) {
                        latlongModel.add(new MapModel(Double.parseDouble(stopageBean.getLatitude())
                                , Double.parseDouble(stopageBean.getLongitude())
                                , stopageBean.getAddress()
                                , stopageBean.getSequenceId()));
                    }
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            getPassengers();

                        }
                    }, 500);
                }
            }
        }
    }


    private void getPassengers() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();

        pendingStudentsApi = AdminApp.getInstance().getApi().getPassangers(entity.getSchoolId(), entity.getSchoolYear(), routeId, tripType);
        pendingStudentsApi.enqueue(new Callback<GetPassangersResponse>() {
            @Override
            public void onResponse(Call<GetPassangersResponse> call, Response<GetPassangersResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            results = response.body().getResult();
                            if (results != null) {

                                setData();

                            } else {

                                drawRoute();
                                txtPassengerCount.setText(getString(R.string.passanger_count) + " : " + 0);
                                Toast.makeText(ManageRouteActivity.this, getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                                parentViewAnimator.setDisplayedChild(1);
                            }

                        } else {

                            drawRoute();
                            txtPassengerCount.setText(getString(R.string.passanger_count) + " : " + 0);
                            Toast.makeText(ManageRouteActivity.this, getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                            parentViewAnimator.setDisplayedChild(1);
                        }

                    } else {

                        txtPassengerCount.setText(getString(R.string.passanger_count) + " : " + 0);
                        APIError error = APIError.parseError(response, ManageRouteActivity.this, App_Constants.API_PENDING_STUDENTS);
                        Toast.makeText(ManageRouteActivity.this, error.message(), Toast.LENGTH_SHORT).show();
                        parentViewAnimator.setDisplayedChild(1);

                    }
                } catch (Exception e) {
                    txtPassengerCount.setText(getString(R.string.passanger_count) + " : " + 0);
                    Toast.makeText(ManageRouteActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                    parentViewAnimator.setDisplayedChild(1);
                }

            }

            @Override
            public void onFailure(Call<GetPassangersResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    txtPassengerCount.setText(getString(R.string.passanger_count) + " : " + 0);
                    parentViewAnimator.setDisplayedChild(1);
                    Toast.makeText(ManageRouteActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void setData() {

        if (mMap != null) {
            mMap.clear();
            drawRoute();
        }
        List<String> passengerCountList = new ArrayList<>();

        if (results.getStudentDetails() != null && results.getStudentDetails().size() > 0) {

            for (GetPassangersResponse.ResultBean.StudentDetailsBean bean : results.getStudentDetails()) {

                if (bean.getGeopoint() != null && !bean.getGeopoint().isEmpty()) {

                    String geoPoint = bean.getGeopoint();
                    if (geoPoint.contains(",")) {

                        double lat = Double.parseDouble(bean.getGeopoint().split(",")[0].trim());
                        double lon = Double.parseDouble(bean.getGeopoint().split(",")[1].trim());

                        if (bean.getRouteDetails() != null) {

                            if (tripType.equalsIgnoreCase(App_Constants.PICK_UP)) {

                                if (bean.getRouteDetails().getPickupStopId() != null && !bean.getRouteDetails().getPickupStopId().isEmpty()) {

                                    passengerCountList.add(bean.getFirstName() + " " + bean.getLastName());

                                    mMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(lat, lon))
                                            .title(bean.getFirstName() + " " + bean.getLastName())
                                            .draggable(true)
                                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                                } else {

                                    mMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(lat, lon))
                                            .title(bean.getFirstName() + " " + bean.getLastName())
                                            .draggable(true)
                                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)));
                                }
                            } else {

                                if (bean.getRouteDetails().getDropoffStopId() != null && !bean.getRouteDetails().getDropoffStopId().isEmpty()) {

                                    passengerCountList.add(bean.getFirstName() + " " + bean.getLastName());

                                    mMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(lat, lon))
                                            .title(bean.getFirstName() + " " + bean.getLastName())
                                            .draggable(true)
                                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                                } else {

                                    mMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(lat, lon))
                                            .title(bean.getFirstName() + " " + bean.getLastName())
                                            .draggable(true)
                                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)));
                                }
                            }
                        } else {

                            mMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(lat, lon))
                                    .title(bean.getFirstName() + " " + bean.getLastName())
                                    .draggable(true)
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)));
                        }
                    }
                }
            }
        }

        if (results.getStaffDetails() != null && results.getStaffDetails().size() > 0) {

            for (GetPassangersResponse.ResultBean.StaffDetailsBean bean : results.getStaffDetails()) {

                if (bean.getGeopoint() != null && !bean.getGeopoint().isEmpty()) {

                    String geoPoint = bean.getGeopoint();
                    if (geoPoint.contains(",")) {

                        double lat = Double.parseDouble(bean.getGeopoint().split(",")[0].trim());
                        double lon = Double.parseDouble(bean.getGeopoint().split(",")[1].trim());

                        if (bean.getRouteDetails() != null) {

                            if (tripType.equalsIgnoreCase(App_Constants.PICK_UP)) {

                                if (bean.getRouteDetails().getPickupStopId() != null && !bean.getRouteDetails().getPickupStopId().isEmpty()) {

                                    passengerCountList.add(bean.getFirstName() + " " + bean.getLastName());

                                    mMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(lat, lon))
                                            .title(bean.getFirstName() + " " + bean.getLastName())
                                            .draggable(true)
                                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                                } else {

                                    mMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(lat, lon))
                                            .title(bean.getFirstName() + " " + bean.getLastName())
                                            .draggable(true)
                                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)));
                                }
                            } else {

                                if (bean.getRouteDetails().getDropoffStopId() != null && !bean.getRouteDetails().getDropoffStopId().isEmpty()) {

                                    passengerCountList.add(bean.getFirstName() + " " + bean.getLastName());

                                    mMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(lat, lon))
                                            .title(bean.getFirstName() + " " + bean.getLastName())
                                            .draggable(true)
                                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                                } else {

                                    mMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(lat, lon))
                                            .title(bean.getFirstName() + " " + bean.getLastName())
                                            .draggable(true)
                                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)));
                                }
                            }
                        } else {

                            mMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(lat, lon))
                                    .title(bean.getFirstName() + " " + bean.getLastName())
                                    .draggable(true)
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)));
                        }
                    }
                }
            }
        }

        txtPassengerCount.setText(getString(R.string.passanger_count) + " : " + passengerCountList.size());

        parentViewAnimator.setDisplayedChild(1);
    }

 /*   class getMarkerAsyncTask extends AsyncTask<Void, Void, Bitmap> {

        private double lat;
        private double lon;
        private GetPassangersResponse.ResultBean.StaffDetailsBean bean;
        private Bitmap bmp;

        public getMarkerAsyncTask(double lat, double lon, GetPassangersResponse.ResultBean.StaffDetailsBean bean) {

            this.lat = lat;
            this.lon = lon;
            this.bean = bean;
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            URL url ;
            try {
                url = new URL("myurl");
                bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bmp;
        }
        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);

        }
    }*/


    public ArrayList<MapModel> sortLocations(ArrayList<MapModel> locations, final double myLatitude, final double myLongitude) {

        Comparator comp = new Comparator<MapModel>() {
            @Override
            public int compare(MapModel o, MapModel o2) {
                float[] result1 = new float[3];
                Location.distanceBetween(myLatitude, myLongitude, o.getLat(), o.getLon(), result1);
                Float distance1 = result1[0];

                float[] result2 = new float[3];
                Location.distanceBetween(myLatitude, myLongitude, o2.getLat(), o2.getLon(), result2);
                Float distance2 = result2[0];

                return distance1.compareTo(distance2);
            }
        };

        Collections.sort(locations, comp);
        return locations;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

                for (int i = 0; i < circles.size(); i++) {

                    CircleOptions co = circles.get(i);

                    float[] distance = new float[2];
                    Location.distanceBetween(latLng.latitude, latLng.longitude,
                            co.getCenter().latitude, co.getCenter().longitude, distance);

                    if (distance[0] > co.getRadius()) {
                        //Toast.makeText(getBaseContext(), "Outside", Toast.LENGTH_SHORT).show();
                    } else {
                        getPassengersOnStopDialog(i);
                        break;
                    }
                }
            }
        });


        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker arg0) {
                // TODO Auto-generated method stub
                Log.d("System out", "onMarkerDragStart..." + arg0.getPosition().latitude + "..." + arg0.getPosition().longitude);
            }

            @SuppressWarnings("unchecked")
            @Override
            public void onMarkerDragEnd(Marker marker) {
                // TODO Auto-generated method stub
                Log.d("System out", "onMarkerDragEnd..." + marker.getPosition().latitude + "..." + marker.getPosition().longitude);
                mMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));

                for (int i = 0; i < circles.size(); i++) {

                    CircleOptions co = circles.get(i);

                    float[] distance = new float[2];
                    Location.distanceBetween(marker.getPosition().latitude, marker.getPosition().longitude,
                            co.getCenter().latitude, co.getCenter().longitude, distance);

                    if (distance[0] > co.getRadius()) {
                        //Toast.makeText(getBaseContext(), "Outside", Toast.LENGTH_SHORT).show();
                    } else {
                        // Toast.makeText(getBaseContext(), "Inside", Toast.LENGTH_SHORT).show();
                        assignPassengerDialog(i, marker);
                        break;
                    }
                }
            }

            @Override
            public void onMarkerDrag(Marker arg0) {
                // TODO Auto-generated method stub
                Log.i("System out", "onMarkerDrag...");
            }
        });
    }

    private void getPassengersOnStopDialog(final int position) {

        final View view = LayoutInflater.from(ManageRouteActivity.this).inflate(R.layout.dialog_passanger_on_stop, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(ManageRouteActivity.this);
        builder.setView(view);
        builder.setCancelable(true);

        final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        dialogViewAnimator = (ViewAnimator) view.findViewById(R.id.viewAnimator);
        final TextView txtStopNo = (TextView) view.findViewById(R.id.txt_stop_no);

        builder.setPositiveButton(R.string.dismiss, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });
        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {

                recyclerView.setLayoutManager(new LinearLayoutManager(ManageRouteActivity.this));
                recyclerView.addItemDecoration(new DividerItemDecoration(ManageRouteActivity.this, DividerItemDecoration.VERTICAL));
                ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, ManageRouteActivity.this);
                new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);
                adapter = new PassengerOnStopAdapter(ManageRouteActivity.this);
                recyclerView.setAdapter(adapter);

                List<PassengerOnStopModel> passangerList = new ArrayList<>();

                if (bean != null) {

                    if (bean.getStopage() != null && bean.getStopage().size() > 0) {

                        for (int i = 0; i < bean.getStopage().size(); i++) {

                            if (i == position) {

                                txtStopNo.setText(getString(R.string.passanger_on_stop) + " " + bean.getStopage().get(i).getStopId());

                                if (results != null && results.getStudentDetails() != null && results.getStudentDetails().size() > 0) {

                                    for (GetPassangersResponse.ResultBean.StudentDetailsBean studentBean : results.getStudentDetails()) {

                                        if (tripType != null && tripType.equalsIgnoreCase(App_Constants.PICK_UP)) {

                                            if (studentBean.getRouteDetails() != null) {

                                                if (studentBean.getRouteDetails().getPickupStopId() != null
                                                        && studentBean.getRouteDetails().getPickupStopId().equals(bean.getStopage().get(i).getStopId())) {

                                                    PassengerOnStopModel passengerListBean = new PassengerOnStopModel();
                                                    passengerListBean.setId(studentBean.getId());
                                                    passengerListBean.setProfileType(App_Constants.STUDENT);
                                                    passengerListBean.setName(studentBean.getFirstName() + " " + studentBean.getLastName());
                                                    passengerListBean.setProfilePic(studentBean.getProfilePic());
                                                    passengerListBean.setRouteId("");
                                                    passengerListBean.setRouteName("");
                                                    passengerListBean.setRouteType(bean.getRouteType());
                                                    passengerListBean.setStopId("");
                                                    passangerList.add(passengerListBean);

                                                }
                                            }
                                        } else if (tripType != null && tripType.equalsIgnoreCase(App_Constants.DROP_OFF)) {

                                            if (studentBean.getRouteDetails() != null) {

                                                if (studentBean.getRouteDetails().getDropoffStopId() != null
                                                        && studentBean.getRouteDetails().getDropoffStopId().equals(bean.getStopage().get(i).getStopId())) {

                                                    PassengerOnStopModel passengerListBean = new PassengerOnStopModel();
                                                    passengerListBean.setId(studentBean.getId());
                                                    passengerListBean.setProfileType(App_Constants.STUDENT);
                                                    passengerListBean.setName(studentBean.getFirstName() + " " + studentBean.getLastName());
                                                    passengerListBean.setProfilePic(studentBean.getProfilePic());
                                                    passengerListBean.setRouteId("");
                                                    passengerListBean.setRouteName("");
                                                    passengerListBean.setRouteType(bean.getRouteType());
                                                    passengerListBean.setStopId("");
                                                    passangerList.add(passengerListBean);
                                                }
                                            }
                                        }
                                    }
                                }

                                if (results != null && results.getStaffDetails() != null && results.getStaffDetails().size() > 0) {

                                    for (GetPassangersResponse.ResultBean.StaffDetailsBean staffDetailsBean : results.getStaffDetails()) {

                                        if (tripType != null && tripType.equalsIgnoreCase(App_Constants.PICK_UP)) {

                                            if (staffDetailsBean.getRouteDetails() != null) {

                                                if (staffDetailsBean.getRouteDetails().getPickupStopId() != null
                                                        && staffDetailsBean.getRouteDetails().getPickupStopId().equals(bean.getStopage().get(i).getStopId())) {

                                                    PassengerOnStopModel passengerListBean = new PassengerOnStopModel();
                                                    passengerListBean.setId(staffDetailsBean.getId());
                                                    passengerListBean.setProfileType(App_Constants.STAFF);
                                                    passengerListBean.setName(staffDetailsBean.getFirstName() + " " + staffDetailsBean.getLastName());
                                                    passengerListBean.setProfilePic(staffDetailsBean.getProfilePic());
                                                    passengerListBean.setRouteId("");
                                                    passengerListBean.setRouteName("");
                                                    passengerListBean.setRouteType(bean.getRouteType());
                                                    passengerListBean.setStopId("");
                                                    passangerList.add(passengerListBean);

                                                }
                                            }
                                        } else if (tripType != null && tripType.equalsIgnoreCase(App_Constants.DROP_OFF)) {

                                            if (staffDetailsBean.getRouteDetails() != null) {

                                                if (staffDetailsBean.getRouteDetails().getDropoffStopId() != null
                                                        && staffDetailsBean.getRouteDetails().getDropoffStopId().equals(bean.getStopage().get(i).getStopId())) {

                                                    PassengerOnStopModel passengerListBean = new PassengerOnStopModel();
                                                    passengerListBean.setId(staffDetailsBean.getId());
                                                    passengerListBean.setProfileType(App_Constants.STAFF);
                                                    passengerListBean.setName(staffDetailsBean.getFirstName() + " " + staffDetailsBean.getLastName());
                                                    passengerListBean.setProfilePic(staffDetailsBean.getProfilePic());
                                                    passengerListBean.setRouteId("");
                                                    passengerListBean.setRouteName("");
                                                    passengerListBean.setRouteType(bean.getRouteType());
                                                    passengerListBean.setStopId("");
                                                    passangerList.add(passengerListBean);
                                                }
                                            }
                                        }
                                    }
                                }

                                break;
                            }
                        }
                    }

                    if (passangerList.size() > 0) {
                        adapter.addItem(passangerList);
                        dialogViewAnimator.setDisplayedChild(0);
                    } else {
                        dialogViewAnimator.setDisplayedChild(1);
                    }
                }


            }
        });
        dialog.show();

    }

    private void assignPassengerDialog(final int position, final Marker marker) {

        AlertDialog alertDialog = new AlertDialog.Builder(ManageRouteActivity.this).create();
        alertDialog.setTitle(getString(R.string.assign_passanger));
        alertDialog.setMessage(getString(R.string.sure_to_asssign_passeneger));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        if (bean != null) {

                            AssignPassangerParams.PassengerListBean passengerListBean = new AssignPassangerParams.PassengerListBean();

                            if (results.getStudentDetails() != null && results.getStudentDetails().size() > 0) {

                                for (GetPassangersResponse.ResultBean.StudentDetailsBean bean : results.getStudentDetails()) {

                                    if (bean.getFirstName() != null && !bean.getFirstName().isEmpty()
                                            && bean.getLastName() != null && !bean.getLastName().isEmpty()) {

                                        if ((bean.getFirstName() + " " + bean.getLastName()).equals(marker.getTitle())) {

                                            passengerListBean.setId(bean.getId());
                                            passengerListBean.setProfileType(App_Constants.STUDENT);
                                            break;
                                        }

                                    }
                                }
                            }

                            if (results.getStaffDetails() != null && results.getStaffDetails().size() > 0) {

                                for (GetPassangersResponse.ResultBean.StaffDetailsBean bean : results.getStaffDetails()) {

                                    if (bean.getFirstName() != null && !bean.getFirstName().isEmpty()
                                            && bean.getLastName() != null && !bean.getLastName().isEmpty()) {

                                        if ((bean.getFirstName() + " " + bean.getLastName()).equals(marker.getTitle())) {

                                            passengerListBean.setId(bean.getId());
                                            passengerListBean.setProfileType(App_Constants.STAFF);
                                            break;
                                        }

                                    }
                                }
                            }

                            passengerListBean.setRouteId(bean.getRouteId());
                            passengerListBean.setRouteName(bean.getName());
                            passengerListBean.setRouteType(bean.getRouteType());

                            if (bean.getStopage() != null && bean.getStopage().size() > 0) {
                                for (int i = 0; i < bean.getStopage().size(); i++) {
                                    if (i == position) {
                                        passengerListBean.setStopId(bean.getStopage().get(i).getStopId());
                                        break;
                                    }
                                }
                            }

                            passangerList.add(passengerListBean);

                            dialog.dismiss();

                            passengerMarker = marker;

                            assignPassenger();
                        } else {
                            dialog.dismiss();
                        }
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    private void drawRoute() {

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        if (latlongModel.size() > 0) {

            for (int i = 0; i < latlongModel.size(); i++) {
                MarkerOptions markerOptions = new MarkerOptions();
                LatLng latLng = new LatLng(latlongModel.get(i).getLat(), latlongModel.get(i).getLon());
                markerPoints.add(latLng);
                if (i == 0) {// if i=0 set the marker color Green, it'll be the starting point of the trip.
                    if (tripType != null && tripType.equalsIgnoreCase(App_Constants.PICK_UP))
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                    else if (tripType != null && tripType.equalsIgnoreCase(App_Constants.DROP_OFF))
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                    else
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                } else if (i == latlongModel.size() - 1) {// if i=size-1 set the marker color Red, it'll be the ending// point of the trip.
                    if (tripType != null && tripType.equalsIgnoreCase(App_Constants.PICK_UP))
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_school_icon));
                    else if (tripType != null && tripType.equalsIgnoreCase(App_Constants.DROP_OFF))
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_school_green));
                    else
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_school_icon));
                } else {//else set the marker color Azure, it will be the stops or waypoints.
                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                }
                markerOptions.position(latLng);
                markerOptions.title(latlongModel.get(i).getStopId() + "-" + latlongModel.get(i).getTitle());
                mMap.addMarker(markerOptions);
                builder.include(latLng);
                addCircle(latLng);
            }
        }

        final LatLngBounds bounds = builder.build();

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {

                float[] results = new float[1];
                Location.distanceBetween(bounds.northeast.latitude, bounds.northeast.longitude,
                        bounds.southwest.latitude, bounds.southwest.longitude, results);

                CameraUpdate cu = null;
                if (results[0] < 1000) { // distance is less than 1 km -> set to zoom level 15
                    cu = CameraUpdateFactory.newLatLngZoom(bounds.getCenter(), 14);

                } else {
                    int padding = 40; // offset from edges of the map in pixels
                    cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                }
                if (cu != null) {
                    mMap.animateCamera(cu);
                }
            }
        });


        if (latlongModel.size() > 0) {
            String url = getMapsApiDirectionsUrl();
            ReadTask downloadTask = new ReadTask();
            downloadTask.execute(url);
        }
    }

    private void addCircle(LatLng latLng) {

        CircleOptions circleOptions1 = new CircleOptions()
                .center(latLng)
                .radius(App_Constants.CIRCLE_RADIUS).strokeColor(Color.BLACK)
                .strokeWidth(1).fillColor(0x500000ff);
        mMap.addCircle(circleOptions1);
        circles.add(circleOptions1);

    }


    private String getMapsApiDirectionsUrl() {

        // Origin of route
        String str_origin = "origin=" + latlongModel.get(0).getLat() + "," + latlongModel.get(0).getLon();

        // Destination of route
        String str_dest = "destination=" + latlongModel.get(latlongModel.size() - 1).getLat() + "," + latlongModel.get(latlongModel.size() - 1).getLon();

        // Sensor enabled
        String sensor = "sensor=false";

        // Waypoints
        String waypoints = "";
        for (int i = 0; i < markerPoints.size(); i++) {
            LatLng point = (LatLng) markerPoints.get(i);
            if (i == 0)
                waypoints = "waypoints=";
            waypoints += point.latitude + "," + point.longitude + "|";
        }

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + waypoints;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?key=AIzaSyBaK8M0QdcKkYYid-DH0iRcpFLd_IGws40&" + parameters;

        Log.e("URL", url);

        return url;

    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {

        if (viewHolder instanceof PassengerOnStopAdapter.ItemViewHolder) {
            passangerList.addAll(adapter.getSelectedPassanger(viewHolder.getAdapterPosition()));
            adapter.removeItem(viewHolder.getAdapterPosition());
            unAssignPassenger();
        }

    }

    public void NoPassangerAtStop() {

        if (dialogViewAnimator != null)
            dialogViewAnimator.setDisplayedChild(1);
    }


    private class ReadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                HttpConnection http = new HttpConnection();
                data = http.readUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                new ParserTask().execute(result);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }

        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }


        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {

            ArrayList<LatLng> polylinePoints;
            PolylineOptions lineOptions = null;

            if (result != null && result.size() > 0) {
                // Traversing through all the routes
                for (int i = 0; i < result.size(); i++) {
                    polylinePoints = new ArrayList<LatLng>();
                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);

                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        polylinePoints.add(position);
                    }

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(polylinePoints);
                    lineOptions.width(10);
                    Random rnd = new Random();
                    lineOptions.color(Color.parseColor("#ff00ff"));
                }
            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                mMap.addPolyline(lineOptions);
                // isPathDrawn = true;
            } else {
                //Toast.makeText(AdminActivity.this, getString(R.string.no_path), Toast.LENGTH_SHORT).show();
                //clearMap();
            }
        }
    }


    private void assignPassenger() {

        AssignPassangerParams params = new AssignPassangerParams();
        params.setPassengerList(passangerList);

        Utility.showProgress(ManageRouteActivity.this, getString(R.string.processing));

        assignCall = AdminApp.getInstance().getApi().assignPassanger(params);
        assignCall.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            Toast.makeText(ManageRouteActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            if (passangerList != null && passangerList.size() > 0) {

                                if (passangerList.get(0).getProfileType().equalsIgnoreCase(App_Constants.STUDENT)) {

                                    if (results != null && results.getStudentDetails() != null && results.getStudentDetails().size() > 0) {

                                        for (GetPassangersResponse.ResultBean.StudentDetailsBean bean : results.getStudentDetails()) {

                                            if (bean.getId().equalsIgnoreCase(passangerList.get(0).getId())) {

                                                if (bean.getRouteDetails() != null) {
                                                    if (passangerList.get(0).getRouteType().equalsIgnoreCase(App_Constants.PICK_UP)) {
                                                        bean.getRouteDetails().setPickupStopId(passangerList.get(0).getStopId());
                                                        break;
                                                    } else {
                                                        bean.getRouteDetails().setDropoffStopId(passangerList.get(0).getStopId());
                                                        break;
                                                    }
                                                }
                                            }

                                        }
                                    }
                                } else if (passangerList.get(0).getProfileType().equalsIgnoreCase(App_Constants.STAFF)) {

                                    if (results != null && results.getStaffDetails() != null && results.getStaffDetails().size() > 0) {

                                        for (GetPassangersResponse.ResultBean.StaffDetailsBean bean : results.getStaffDetails()) {

                                            if (bean.getId().equalsIgnoreCase(passangerList.get(0).getId())) {

                                                if (bean.getRouteDetails() != null) {
                                                    if (passangerList.get(0).getRouteType().equalsIgnoreCase(App_Constants.PICK_UP)) {
                                                        bean.getRouteDetails().setPickupStopId(passangerList.get(0).getStopId());
                                                        break;
                                                    } else {
                                                        bean.getRouteDetails().setDropoffStopId(passangerList.get(0).getStopId());
                                                        break;
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }
                            }

                            if (passengerMarker != null) {
                                passengerMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));

                                txtPassengerCount.setText(getString(R.string.passanger_count) + " : " + (Integer.parseInt(txtPassengerCount.getText().toString().split(":")[1].trim()) + 1));
                            }
                            clearList();

                            //setBackIntent();
                        } else {
                            Toast.makeText(ManageRouteActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            clearList();
                        }
                    } else {
                        APIError error = APIError.parseError(response, ManageRouteActivity.this, App_Constants.API_ASSIGN_PASSANGER);
                        Toast.makeText(ManageRouteActivity.this, error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    clearList();
                    Toast.makeText(ManageRouteActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    clearList();
                    Utility.hideProgress();
                    Toast.makeText(ManageRouteActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void unAssignPassenger() {

        AssignPassangerParams params = new AssignPassangerParams();
        params.setPassengerList(passangerList);

        assignCall = AdminApp.getInstance().getApi().assignPassanger(params);
        assignCall.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            Toast.makeText(ManageRouteActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                            if (passangerList != null && passangerList.size() > 0) {

                                if (passangerList.get(0).getProfileType().equalsIgnoreCase(App_Constants.STUDENT)) {

                                    if (results != null && results.getStudentDetails() != null && results.getStudentDetails().size() > 0) {

                                        for (GetPassangersResponse.ResultBean.StudentDetailsBean bean : results.getStudentDetails()) {

                                            if (bean.getId().equalsIgnoreCase(passangerList.get(0).getId())) {

                                                if (bean.getRouteDetails() != null) {
                                                    if (passangerList.get(0).getRouteType().equalsIgnoreCase(App_Constants.PICK_UP)) {
                                                        bean.getRouteDetails().setPickupStopId("");
                                                        break;
                                                    } else {
                                                        bean.getRouteDetails().setDropoffStopId("");
                                                        break;
                                                    }
                                                }
                                            }

                                        }
                                    }
                                } else if (passangerList.get(0).getProfileType().equalsIgnoreCase(App_Constants.STAFF)) {

                                    if (results != null && results.getStaffDetails() != null && results.getStaffDetails().size() > 0) {

                                        for (GetPassangersResponse.ResultBean.StaffDetailsBean bean : results.getStaffDetails()) {

                                            if (bean.getId().equalsIgnoreCase(passangerList.get(0).getId())) {

                                                if (bean.getRouteDetails() != null) {
                                                    if (passangerList.get(0).getRouteType().equalsIgnoreCase(App_Constants.PICK_UP)) {
                                                        bean.getRouteDetails().setPickupStopId("");
                                                        break;
                                                    } else {
                                                        bean.getRouteDetails().setDropoffStopId("");
                                                        break;
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }
                            }
                            setData();
                            clearList();
                        } else {
                            Toast.makeText(ManageRouteActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            clearList();
                        }
                    } else {
                        APIError error = APIError.parseError(response, ManageRouteActivity.this, App_Constants.API_UNASSIGN_PASSANGER);
                        Toast.makeText(ManageRouteActivity.this, error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    clearList();
                    Toast.makeText(ManageRouteActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    clearList();
                    Toast.makeText(ManageRouteActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void clearList() {

        passangerList.clear();
    }


    private void setBackIntent() {

        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        onBackPressed();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (assignCall != null)
            assignCall.cancel();

        if (pendingStudentsApi != null)
            pendingStudentsApi.cancel();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == APIError.UPDATE_TOKEN && resultCode == RESULT_OK) {

            if (data != null) {
                String apiName = data.getStringExtra("api");
                onReload(apiName);
            }
        }
    }

    private void onReload(String apiName) {

        if (apiName != null) {
            if (apiName.equals(App_Constants.API_ASSIGN_PASSANGER)) {
                assignPassenger();
            } else if (apiName.equals(App_Constants.API_PENDING_STUDENTS)) {
                getPassengers();
            } else if (apiName.equals(App_Constants.API_UNASSIGN_PASSANGER)) {
                unAssignPassenger();
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
