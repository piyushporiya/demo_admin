/*
 * Copyright (c) 2017.                                                       
 *                                                                           
 * ***********************************************************************  
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                           
 * ________________________________________________________________________  
 *                                                                           
 *    [2010] - [2018] SripathiSolutions Corp.                                
 *    [2010] - [2018] DataReady Technology Corp.                             
 *   ________________________________________________________________________
 *                                                                           
 *    All Rights Reserved.                                                   
 *                                                                           
 *   NOTICE:  All information contained herein is, and remains               
 *   the property of DataReady Technology  and its suppliers,                
 *   if any.  The intellectual and technical concepts contained              
 *   herein are proprietary to DataReady Technology Incorporated             
 *   and its suppliers and may be covered by U.S. and Foreign Patents,       
 *   patents in process, and are protected by trade secret or copyright law. 
 *   Dissemination of this information or reproduction of this material      
 *   is strictly forbidden unless prior written permission is obtained       
 *   from DataReady Technology Incorporated.                                 
 *                                                                           
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                            
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.route.history.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.route.history.RouteHistoryActivity;
import ca.dataready.SmartSchoolAdmin.server.AllTripsForRoute;

public class TripsOfRouteAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<AllTripsForRoute.ResultBean> beans;

    public TripsOfRouteAdapter(Context context) {
        this.context = context;
        beans = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_trips_of_route, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        final AllTripsForRoute.ResultBean bean = beans.get(position);

        if (holder instanceof ItemViewHolder) {

            ((ItemViewHolder) holder).txtTripName.setText(bean.getTripId());
            ((ItemViewHolder) holder).txtStatus.setText("Status : " + bean.getStatus());

            if (bean.isSelected()) {
                ((ItemViewHolder) holder).rl.setBackgroundResource(R.drawable.item_back_selected);
            } else {
                ((ItemViewHolder) holder).rl.setBackgroundResource(R.drawable.item_back);
            }

            ((ItemViewHolder) holder).rl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
/*
                    bean.setSelected(!bean.isSelected());

                    if (bean.isSelected()) {
                        ((ItemViewHolder) holder).rl.setBackgroundResource(R.drawable.item_back_selected);
                        ((RouteHistoryActivity) context).showTripHistory(bean.getTripId());
                    } else {
                        ((ItemViewHolder) holder).rl.setBackgroundResource(R.drawable.item_back);
                    }

                    makeOtherUnSelected(holder.getAdapterPosition());*/


                }
            });
        }

    }

    private void makeOtherUnSelected(int adapterPosition) {

        for (int i = 0; i < beans.size(); i++) {
            if (i != adapterPosition) {
                beans.get(i).setSelected(false);
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }

    public void addItem(ArrayList<AllTripsForRoute.ResultBean> results) {

        beans.addAll(results);
        notifyDataSetChanged();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.txt_trip_name)
        TextView txtTripName;
        @BindView(R.id.rl)
        LinearLayout rl;
        @BindView(R.id.txt_status)
        TextView txtStatus;
        // R.layout.raw_trips_of_route

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
