/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.route.add;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.Utilities.map.HttpConnection;
import ca.dataready.SmartSchoolAdmin.Utilities.map.MapWrapperLayout;
import ca.dataready.SmartSchoolAdmin.Utilities.map.OnInfoWindowElemTouchListener;
import ca.dataready.SmartSchoolAdmin.Utilities.map.PathJSONParser;
import ca.dataready.SmartSchoolAdmin.Utilities.place.PlaceAPIAdapter;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.AssignDriverToRouteParams;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.CommonResponse;
import ca.dataready.SmartSchoolAdmin.server.CreateRouteParams;
import ca.dataready.SmartSchoolAdmin.server.CreateRouteResponse;
import ca.dataready.SmartSchoolAdmin.server.GetAllDriverResponse;
import ca.dataready.SmartSchoolAdmin.server.GetAllRoutesResponse;
import ca.dataready.SmartSchoolAdmin.server.GetFullRouteInfo;
import ca.dataready.SmartSchoolAdmin.server.GetSchoolInfo;
import ca.dataready.SmartSchoolAdmin.server.MapModel;
import ca.dataready.SmartSchoolAdmin.userinfo.driver.IDriverFragment;
import ca.dataready.SmartSchoolAdmin.userinfo.driver.adapter.DriverAdapter;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.GradeList;
import info.hoang8f.android.segmented.SegmentedGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddRouteActivity extends BaseActivity implements OnMapReadyCallback {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txtProfile)
    TextView txtProfile;
    @BindView(R.id.card_route_info)
    CardView cardRouteInfo;
    @BindView(R.id.rb_pickup)
    RadioButton rbPickup;
    @BindView(R.id.rb_dropoff)
    RadioButton rbDropoff;
    @BindView(R.id.sgRouteType)
    SegmentedGroup sgRouteType;
    @BindView(R.id.et_route_id)
    EditText etRouteId;
    @BindView(R.id.et_route_name)
    EditText etRouteName;
    @BindView(R.id.layout_route_info)
    CardView layoutRouteInfo;
    @BindView(R.id.txt_error)
    TextView txtError;
    @BindView(R.id.viewAnimator)
    ViewAnimator viewAnimator;
    @BindView(R.id.et_search_place)
    AutoCompleteTextView etSearchPlace;
    @BindView(R.id.img_search)
    ImageView imgSearch;
    @BindView(R.id.txt_address)
    TextView txtAddress;
    @BindView(R.id.txt_latitude)
    TextView txtLatitude;
    @BindView(R.id.txt_longitude)
    TextView txtLongitude;
    @BindView(R.id.img_cancel)
    ImageView imgCancel;
    @BindView(R.id.card_address)
    CardView cardAddress;
    @BindView(R.id.txt_add_location)
    TextView txtAddLocation;
    @BindView(R.id.txt_start_location)
    TextView txtStartLocation;
    @BindView(R.id.txt_end_location)
    TextView txtEndLocation;
    @BindView(R.id.btn_optimize)
    Button btnOptimize;
    @BindView(R.id.chk_origin)
    CheckBox chkOrigin;
    @BindView(R.id.btn_finalize)
    Button btnFinalize;
    @BindView(R.id.et_driver)
    EditText etDriver;

    private SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private MapWrapperLayout mapWrapperLayout;
    private OnInfoWindowElemTouchListener infoButtonListener;
    private Marker currentMarker;
    ArrayList<MapModel> latlongModel = new ArrayList<>();
    ArrayList<LatLng> markerPoints = new ArrayList<>();
    private Polyline polyline;
    private Call<CreateRouteResponse> createCall;
    private boolean isStartEndPoint = false;
    private Marker endLocationMarker;
    private GetAllRoutesResponse.ResultBean bean;
    private Call<CommonResponse> updateCall;
    private Call<GetAllDriverResponse> allDriverApi;
    private ArrayList<GetAllDriverResponse.ResultBean> allDriverResults;
    private ListPopupWindow listPopupWindow;
    private ArrayList<String> drivers;
    private CreateRouteResponse.ResultBean createRouteResults;
    private Call<CommonResponse> assignDriverToRouteCall;
    private Call<GetFullRouteInfo> fullRouteInfoApi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_route);
        ButterKnife.bind(this);
        setHeaderInfo();
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapWrapperLayout = (MapWrapperLayout) findViewById(R.id.map_relative_layout);

        mapFragment.getMapAsync(this);
        Init();
    }

    private void setHeaderInfo() {

        setSupportActionBar(toolbar);

        if (AdminApp.getInstance().getAdmin() != null)
            getSupportActionBar().setTitle(AdminApp.getInstance().getAdmin().getSchoolName() + " " + AdminApp.getInstance().getAdmin().getSchoolYear());
        else
            getSupportActionBar().setTitle(getString(R.string.app_name));

        getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + getString(R.string.add_route) + "</small>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void Init() {

        if (getIntent().getExtras() != null) {

            bean = getIntent().getParcelableExtra(App_Constants.OBJECT);
            btnFinalize.setText(getString(R.string.update));

            if (bean != null) {

                if (bean.getStopage() != null && bean.getStopage().size() > 0) {

                    txtEndLocation.setText(bean.getStopage().get(bean.getStopage().size() - 1).getAddress());

                    etRouteId.setText(bean.getRouteId());

                    if (bean.getRouteType().equalsIgnoreCase(rbPickup.getText().toString())) {
                        rbPickup.setChecked(true);
                        txtStartLocation.setText(AdminApp.getInstance().getSchoolInfo().getResult().getBusStandInfo().getAddress());
                        txtEndLocation.setText(AdminApp.getInstance().getAdmin().getSchoolAddress());
                    } else {
                        rbDropoff.setChecked(true);
                        txtStartLocation.setText(AdminApp.getInstance().getAdmin().getSchoolAddress());
                        txtEndLocation.setText(AdminApp.getInstance().getSchoolInfo().getResult().getBusStandInfo().getAddress());
                    }

                    etRouteName.setText(bean.getName());

                    for (int i = 0; i < bean.getStopage().size(); i++) {

                        GetAllRoutesResponse.ResultBean.StopageBean stopageBean = bean.getStopage().get(i);

                        if (i == 0) {
                            if (bean.getRouteType() != null && bean.getRouteType().equalsIgnoreCase(App_Constants.PICK_UP)) {
                                latlongModel.add(new MapModel(Double.parseDouble(stopageBean.getLatitude()), Double.parseDouble(stopageBean.getLongitude()), stopageBean.getAddress(), false, true));
                            } else if (bean.getRouteType() != null && bean.getRouteType().equalsIgnoreCase(App_Constants.DROP_OFF)) {
                                latlongModel.add(new MapModel(Double.parseDouble(stopageBean.getLatitude()), Double.parseDouble(stopageBean.getLongitude()), stopageBean.getAddress(), true, false));
                            } else {
                                latlongModel.add(new MapModel(Double.parseDouble(stopageBean.getLatitude()), Double.parseDouble(stopageBean.getLongitude()), stopageBean.getAddress(), false, true));
                            }
                        } else if (i == bean.getStopage().size() - 1) {

                            if (bean.getRouteType() != null && bean.getRouteType().equalsIgnoreCase(App_Constants.PICK_UP)) {
                                latlongModel.add(new MapModel(Double.parseDouble(stopageBean.getLatitude()), Double.parseDouble(stopageBean.getLongitude()), stopageBean.getAddress(), true, false));
                            } else if (bean.getRouteType() != null && bean.getRouteType().equalsIgnoreCase(App_Constants.DROP_OFF)) {
                                latlongModel.add(new MapModel(Double.parseDouble(stopageBean.getLatitude()), Double.parseDouble(stopageBean.getLongitude()), stopageBean.getAddress(), false, true));
                            } else {
                                latlongModel.add(new MapModel(Double.parseDouble(stopageBean.getLatitude()), Double.parseDouble(stopageBean.getLongitude()), stopageBean.getAddress(), true, false));
                            }
                        } else {
                            latlongModel.add(new MapModel(Double.parseDouble(stopageBean.getLatitude()), Double.parseDouble(stopageBean.getLongitude()), stopageBean.getAddress(), false, false));
                        }
                    }

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            for (int i = 0; i < latlongModel.size(); i++) {

                                MapModel mapBean = latlongModel.get(i);

                                if (i == 0) {

                                    if (bean.getRouteType() != null && bean.getRouteType().equalsIgnoreCase(App_Constants.PICK_UP)) {

                                        mMap.addMarker(new MarkerOptions()
                                                .position(new LatLng(mapBean.getLat(), mapBean.getLon())).title(mapBean.getTitle())
                                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));

                                    } else if (bean.getRouteType() != null && bean.getRouteType().equalsIgnoreCase(App_Constants.DROP_OFF)) {

                                        mMap.addMarker(new MarkerOptions()
                                                .position(new LatLng(mapBean.getLat(), mapBean.getLon())).title(mapBean.getTitle())
                                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_school_green)));

                                    } else {

                                        mMap.addMarker(new MarkerOptions()
                                                .position(new LatLng(mapBean.getLat(), mapBean.getLon())).title(mapBean.getTitle())
                                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));

                                    }

                                } else if (i == latlongModel.size() - 1) {

                                    if (bean.getRouteType() != null && bean.getRouteType().equalsIgnoreCase(App_Constants.PICK_UP)) {

                                        mMap.addMarker(new MarkerOptions()
                                                .position(new LatLng(mapBean.getLat(), mapBean.getLon())).title(mapBean.getTitle())
                                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_school_icon)));

                                    } else if (bean.getRouteType() != null && bean.getRouteType().equalsIgnoreCase(App_Constants.DROP_OFF)) {

                                        mMap.addMarker(new MarkerOptions()
                                                .position(new LatLng(mapBean.getLat(), mapBean.getLon())).title(mapBean.getTitle())
                                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                                    } else {

                                        mMap.addMarker(new MarkerOptions()
                                                .position(new LatLng(mapBean.getLat(), mapBean.getLon())).title(mapBean.getTitle())
                                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_school_icon)));

                                    }

                                } else {

                                    mMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(mapBean.getLat(), mapBean.getLon())).title(mapBean.getTitle())
                                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
                                }
                            }

                            drawRoute();

                        }
                    }, 500);

                }

                getFullRouteInfo();
            }

        } else {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    setUpInitialLocation(true);

                }
            }, 500);

            getAllDriver();
        }

        enableOptimizeBtn(false);
        sgRouteType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                clearMap();

                if (checkedId == R.id.rb_pickup) {

                    setUpInitialLocation(true);

                } else {
                    setUpInitialLocation(false);
                }

            }
        });


        etSearchPlace.setAdapter(new PlaceAPIAdapter(getApplicationContext(), R.layout.autocomplete_list_item));
        etSearchPlace.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String address = (String) parent.getItemAtPosition(position);
                try {

                   /* if (currentMarker != null) {
                        removePlaceFromList(currentMarker.getTitle());
                        currentMarker.remove();
                    }*/

                    hideKeyboard();

                    imgSearch.setImageResource(R.drawable.ic_clear);
                    imgSearch.setContentDescription(getString(R.string.clear_all));
                    txtAddress.setText(address);

                    showLocationCard(true);

                    getLocationFormAddress(address);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getAllDriver() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();

        allDriverApi = AdminApp.getInstance().getApi().getAllDrivers(entity.getSchoolId());
        allDriverApi.enqueue(new Callback<GetAllDriverResponse>() {
            @Override
            public void onResponse(Call<GetAllDriverResponse> call, Response<GetAllDriverResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            allDriverResults = response.body().getResult();
                        }
                    } else {
                        APIError.parseError(response, AddRouteActivity.this, App_Constants.API_GET_ALL_DRIVERS);
                    }
                } catch (Exception e) {
                    Toast.makeText(AddRouteActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
                viewAnimator.setDisplayedChild(1);
            }

            @Override
            public void onFailure(Call<GetAllDriverResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    viewAnimator.setDisplayedChild(1);
                    Toast.makeText(AddRouteActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getFullRouteInfo() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();

        fullRouteInfoApi = AdminApp.getInstance().getApi().getFullRouteInfo(entity.getSchoolId(), entity.getSchoolYear(), bean.getRouteId(), bean.getUniqueId());
        fullRouteInfoApi.enqueue(new Callback<GetFullRouteInfo>() {
            @Override
            public void onResponse(Call<GetFullRouteInfo> call, Response<GetFullRouteInfo> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            if (response.body().getResult() != null) {

                                if (response.body().getResult().getDriverDetails() != null) {

                                    etDriver.setText(response.body().getResult().getDriverDetails().getFirstName() + " "
                                            + response.body().getResult().getDriverDetails().getLastName());
                                }
                            }
                        }
                    } else {
                        APIError.parseError(response, AddRouteActivity.this, App_Constants.API_GET_FULL_ROUTE_INFO);
                    }
                } catch (Exception e) {
                    Toast.makeText(AddRouteActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
                getAllDriver();
            }

            @Override
            public void onFailure(Call<GetFullRouteInfo> call, Throwable t) {
                if (!call.isCanceled()) {
                    viewAnimator.setDisplayedChild(1);
                    Toast.makeText(AddRouteActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void clearMap() {

        latlongModel.clear();
        markerPoints.clear();
        if (mMap != null)
            mMap.clear();
        polyline = null;
    }

    private void setUpInitialLocation(boolean isPickup) {

        if (mMap != null) {

            if (AdminApp.getInstance().getSchoolInfo() != null) {

                GetSchoolInfo.ResultBean.BusStandInfoBean bean = AdminApp.getInstance().getSchoolInfo().getResult().getBusStandInfo();

                if (bean != null) {
                    if (isPickup)
                        txtStartLocation.setText(bean.getAddress());
                    else
                        txtEndLocation.setText(bean.getAddress());

                    if (bean.getGeopoint() != null && bean.getGeopoint().contains(",")) {

                        MarkerOptions marker = new MarkerOptions();
                        marker.position(new LatLng(Double.parseDouble(bean.getGeopoint().split(",")[0].trim()), Double.parseDouble(bean.getGeopoint().split(",")[1].trim())));
                        marker.title(bean.getAddress());
                        if (isPickup)
                            marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                        else
                            marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_school_green));
                        mMap.addMarker(marker);

                        if (isPickup) {

                            latlongModel.add(new MapModel(Double.parseDouble(bean.getGeopoint().split(",")[0].trim())
                                    , Double.parseDouble(bean.getGeopoint().split(",")[1].trim())
                                    , bean.getAddress(), false, true));
                        } else {

                            latlongModel.add(new MapModel(Double.parseDouble(bean.getGeopoint().split(",")[0].trim())
                                    , Double.parseDouble(bean.getGeopoint().split(",")[1].trim())
                                    , bean.getAddress(), true, false));
                        }
                    }
                }
            }


            CREDENTIAL.ResultBean admin = AdminApp.getInstance().getAdmin();

            if (isPickup)
                txtEndLocation.setText(admin.getSchoolAddress());
            else
                txtStartLocation.setText(admin.getSchoolAddress());

            if (admin.getGeopoint() != null && admin.getGeopoint().contains(",")) {

                MarkerOptions marker = new MarkerOptions();
                marker.position(new LatLng(Double.parseDouble(admin.getGeopoint().split(",")[0].trim()), Double.parseDouble(admin.getGeopoint().split(",")[1].trim())));
                marker.title(admin.getSchoolAddress());
                if (isPickup)
                    marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_school_icon));
                else
                    marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                mMap.addMarker(marker);

                if (isPickup) {
                    latlongModel.add(new MapModel(Double.parseDouble(admin.getGeopoint().split(",")[0].trim())
                            , Double.parseDouble(admin.getGeopoint().split(",")[1].trim())
                            , admin.getSchoolAddress(), true, false));
                } else {

                    latlongModel.add(new MapModel(Double.parseDouble(admin.getGeopoint().split(",")[0].trim())
                            , Double.parseDouble(admin.getGeopoint().split(",")[1].trim())
                            , admin.getSchoolAddress(), false, true));
                }

                centerMapOnMyLocation(new LatLng(Double.parseDouble(admin.getGeopoint().split(",")[0].trim()), Double.parseDouble(admin.getGeopoint().split(",")[1].trim())));
            }
        }
    }

    private void showLocationCard(boolean showCard) {

        if (showCard) {

            cardAddress.setVisibility(View.VISIBLE);
            cardAddress.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_top));

        } else {

            cardAddress.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down));
            cardAddress.setVisibility(View.GONE);
        }
    }


    private void enableOptimizeBtn(boolean isEnabled) {

        btnOptimize.setEnabled(isEnabled);
        btnOptimize.setAlpha(isEnabled ? 1.0f : 0.5f);
    }

    private void removePlaceFromList(String place) {

        List<MapModel> tempList = new ArrayList<>();

        for (MapModel model : latlongModel) {
            if (!model.getTitle().equals(place)) {
                tempList.add(model);
            }
        }

        latlongModel.clear();
        latlongModel.addAll(tempList);

        if (latlongModel != null && latlongModel.size() > 0)
            enableOptimizeBtn(true);
        else
            enableOptimizeBtn(false);

    }

    private void getLocationFormAddress(String address) {

        Geocoder coder = new Geocoder(AddRouteActivity.this);
        List<Address> addresses;

        try {
            addresses = coder.getFromLocationName(address, 5);
            if (addresses == null) {
                Toast.makeText(this, "address not found", Toast.LENGTH_SHORT).show();
                return;
            }

            if (addresses.size() == 0) {
                Toast.makeText(this, "address not found", Toast.LENGTH_SHORT).show();
                return;
            }

            Address location = addresses.get(0);
            if (mMap != null && location != null) {

                String lat = String.format(Locale.getDefault(), "%.5f", location.getLatitude());
                String lon = String.format(Locale.getDefault(), "%.5f", location.getLongitude());

                txtLatitude.setText(lat);
                txtLongitude.setText(lon);

                addMarker(location.getLatitude(), location.getLongitude(), txtAddress.getText().toString());
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }


    private String getAddressFromLocation(double latitude, double longitude) {

        Geocoder geocoder;
        String fullAddress = "";
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();

            fullAddress = address;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return fullAddress;
    }


    private void addMarker(double latitude, double longitude, String address) {

        LatLng location = new LatLng(latitude, longitude);

        if (mMap != null) {

            //chkOrigin.setChecked(false);

            currentMarker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(location.latitude, location.longitude)).title(address)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

            enableOptimizeBtn(true);

            centerMapOnMyLocation(location);
        }

    }


    private void centerMapOnMyLocation(LatLng location) {

        LatLng myLocation = new LatLng(location.latitude,
                location.longitude);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation,
                15));

    }

    private void hideKeyboard() {

        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null)
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        mapWrapperLayout.init(mMap, getPixelsFromDp(this, 39 + 20));

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {

                if (cardAddress.getVisibility() == View.VISIBLE) {

                    if (currentMarker != null) {
                        removePlaceFromList(currentMarker.getTitle());
                        currentMarker.remove();
                    }

                    showLocationCard(false);
                }

                String lat = String.format(Locale.getDefault(), "%.5f", latLng.latitude);
                String lon = String.format(Locale.getDefault(), "%.5f", latLng.longitude);

                txtLatitude.setText(lat);
                txtLongitude.setText(lon);

                String address = getAddressFromLocation(latLng.latitude, latLng.longitude);
                if (address.isEmpty())
                    txtAddress.setText(getString(R.string.unknown_address));
                else
                    txtAddress.setText(address);

                addMarker(latLng.latitude, latLng.longitude, txtAddress.getText().toString());

                showLocationCard(true);

            }
        });

        /*mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker arg0) {
                // TODO Auto-generated method stub
                Log.d("System out", "onMarkerDragStart..."+arg0.getPosition().latitude+"..."+arg0.getPosition().longitude);
            }

            @SuppressWarnings("unchecked")
            @Override
            public void onMarkerDragEnd(Marker arg0) {
                // TODO Auto-generated method stub
                Log.d("System out", "onMarkerDragEnd..."+arg0.getPosition().latitude+"..."+arg0.getPosition().longitude);

                mMap.animateCamera(CameraUpdateFactory.newLatLng(arg0.getPosition()));
            }

            @Override
            public void onMarkerDrag(Marker arg0) {
                // TODO Auto-generated method stub
                Log.i("System out", "onMarkerDrag...");
            }
        });*/

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

                if (cardAddress.getVisibility() == View.VISIBLE) {

                    if (currentMarker != null) {

                        removePlaceFromList(currentMarker.getTitle());
                        currentMarker.remove();

                        if (latlongModel != null && latlongModel.size() > 0)
                            enableOptimizeBtn(true);
                        else
                            enableOptimizeBtn(false);
                    }

                    showLocationCard(false);
                }

            }
        });

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                // marker.remove();
                return false;
            }
        });

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {

                boolean isEndPoint = false;
                boolean isSchool = false;
                for (MapModel bean : latlongModel) {

                    if (bean.getTitle().equalsIgnoreCase(marker.getTitle())) {

                        if (bean.isSchool()) {
                            isSchool = true;
                            break;
                        }

                        if (bean.isStartOrEnd()) {
                            isEndPoint = true;
                            break;
                        }
                    }
                }

                if (isEndPoint || isSchool) {
                    return;
                }

                removePlaceFromList(marker.getTitle());
                marker.remove();

            }

        });

        mMap.setInfoWindowAdapter(new RouteInfoWindowAdapter());
    }

    @OnClick({R.id.et_driver, R.id.img_search, R.id.img_cancel, R.id.txt_add_location, R.id.btn_optimize, R.id.btn_finalize})
    public void onViewClicked(View view) {
        switch (view.getId()) {


            case R.id.et_driver:

                show_drop_down_to_select_option(etDriver);

                break;

            case R.id.btn_optimize:

                if (latlongModel != null && latlongModel.size() > 0) {

                    //check if end point is added to the map
                    boolean isEndPointAdded = false;

                    for (MapModel bean : latlongModel) {

                        if (bean.isStartOrEnd()) {
                            isEndPointAdded = true;
                        }
                        Log.e("isEND", "" + bean.isStartOrEnd());
                    }

                    if (!isEndPointAdded) {
                        Toast.makeText(this, getString(R.string.add_end_location), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    //swap the list , end point should be the last in the list
                    MapModel endBean = null;
                    for (MapModel bean : latlongModel) {

                        if (bean.isStartOrEnd()) {
                            endBean = bean;
                        }
                    }

                    if (endBean == null)
                        return;

                    while (latlongModel.indexOf(endBean) != latlongModel.size() - 1) {
                        int i = latlongModel.indexOf(endBean);
                        Collections.swap(latlongModel, i, i + 1);
                    }

                    for (MapModel bean : latlongModel) {
                        Log.e("isStart", "" + bean.isSchool());
                        Log.e("isEND", "" + bean.isStartOrEnd());
                    }

                    drawRoute();
                }

                break;

            case R.id.img_search:

                if (imgSearch.getContentDescription().equals(getString(R.string.clear_all))) {
                    imgSearch.setImageResource(R.drawable.ic_search);
                    imgSearch.setContentDescription(getString(R.string.search));
                    etSearchPlace.getText().clear();
                }

                break;

            case R.id.img_cancel:

                showLocationCard(false);
                if (currentMarker != null) {
                    removePlaceFromList(currentMarker.getTitle());
                    currentMarker.remove();
                }

                break;

            case R.id.txt_add_location:

                if (currentMarker != null) {
                    currentMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
                    latlongModel.add(new MapModel(currentMarker.getPosition().latitude, currentMarker.getPosition().longitude, txtAddress.getText().toString(), false, false));
                }
                showLocationCard(false);
                //addMarker(txtLatitude.getText().toString(), txtLongitude.getText().toString(), txtAddress.getText().toString());

                break;

            case R.id.btn_finalize:

                if (etRouteName.getText().toString().isEmpty()) {
                    etRouteName.setError(getString(R.string.field_required));
                    return;
                }

                if (etDriver.getText().toString().isEmpty()) {
                    etDriver.setError(getString(R.string.please_select_driver));
                    return;
                }

                if (latlongModel == null) {
                    Toast.makeText(this, getString(R.string.please_add_points), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (latlongModel.size() <= 1) {
                    Toast.makeText(this, getString(R.string.please_add_points), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (polyline == null) {
                    Toast.makeText(this, getString(R.string.please_optimize_route), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (btnFinalize.getText().toString().equalsIgnoreCase(getString(R.string.update)))
                    updateRoute();
                else
                    createRoute();

                break;
        }
    }


    private void show_drop_down_to_select_option(EditText layout) {

        drivers = new ArrayList<>();
        if (allDriverResults != null && allDriverResults.size() > 0) {

            listPopupWindow = new ListPopupWindow(AddRouteActivity.this);

            for (GetAllDriverResponse.ResultBean beans : allDriverResults) {
                drivers.add(beans.getFirstName() + " " + beans.getLastName());
            }

            listPopupWindow.setAdapter(new ArrayAdapter(AddRouteActivity.this, R.layout.list_dropdown_item, drivers));
            listPopupWindow.setAnchorView(layout);
            listPopupWindow.setWidth(ListPopupWindow.WRAP_CONTENT);
            listPopupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);

            listPopupWindow.setModal(true);
            listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    AddRouteActivity.this.etDriver.setText(drivers.get(i));
                    listPopupWindow.dismiss();

                }
            });
            listPopupWindow.show();
        } else {

            Toast.makeText(this, getString(R.string.no_driver_avail), Toast.LENGTH_SHORT).show();
        }
    }

    class RouteInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private final View infoWindow;

        RouteInfoWindowAdapter() {
            infoWindow = getLayoutInflater().inflate(R.layout.raw_route_info_window, null);
        }

        @Override
        public View getInfoContents(Marker marker) {

            TextView tvTitle = ((TextView) infoWindow.findViewById(R.id.txt_address));
            tvTitle.setText(marker.getTitle());
            TextView tvSnippet = ((TextView) infoWindow.findViewById(R.id.txt_remove_location));


            infoButtonListener = new OnInfoWindowElemTouchListener(tvSnippet,
                    getResources().getDrawable(R.drawable.button_back),
                    getResources().getDrawable(R.drawable.button_back)) {
                @Override
                protected void onClickConfirmed(View v, Marker marker) {
                    // Here we can perform some action triggered after clicking the button
                    Toast.makeText(AddRouteActivity.this, marker.getTitle() + "'s button clicked!", Toast.LENGTH_SHORT).show();
                    marker.remove();
                }
            };
            infoButtonListener.setMarker(marker);
            tvSnippet.setOnTouchListener(infoButtonListener);


            mapWrapperLayout.setMarkerWithInfoWindow(marker, infoWindow);
            return infoWindow;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            // TODO Auto-generated method stub
            return null;
        }
    }

    public static int getPixelsFromDp(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    private void drawRoute() {

        if (polyline != null) {
            polyline.remove();
            markerPoints.clear();
        }

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        if (latlongModel.size() > 0) {

            for (int i = 0; i < latlongModel.size(); i++) {
                // MarkerOptions markerOptions = new MarkerOptions();
                LatLng latLng = new LatLng(latlongModel.get(i).getLat(), latlongModel.get(i).getLon());
                markerPoints.add(latLng);
               /* markerPoints.add(latLng);
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                markerOptions.position(latLng);
                markerOptions.title(latlongModel.get(i).getTitle());
                mMap.addMarker(markerOptions);*/
                builder.include(latLng);
            }
        }

        final LatLngBounds bounds = builder.build();


        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {

                float[] results = new float[1];
                Location.distanceBetween(bounds.northeast.latitude, bounds.northeast.longitude,
                        bounds.southwest.latitude, bounds.southwest.longitude, results);

                CameraUpdate cu = null;
                if (results[0] < 1000) { // distance is less than 1 km -> set to zoom level 15
                    cu = CameraUpdateFactory.newLatLngZoom(bounds.getCenter(), 14);

                } else {
                    int padding = 40; // offset from edges of the map in pixels
                    cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                }
                if (cu != null) {
                    mMap.animateCamera(cu);
                }
            }
        });


        if (latlongModel.size() > 0) {
            String url = getMapsApiDirectionsUrl();
            ReadTask downloadTask = new ReadTask();
            downloadTask.execute(url);
        }
    }


    private String getMapsApiDirectionsUrl() {

        /* MapModel bean = null;
        for (int i = 0; i < latlongModel.size(); i++) {
            if (latlongModel.get(i).isOrigin()) {
                bean = latlongModel.get(i);
                break;
            }
        }

        while (latlongModel.indexOf(bean) != 0) {
            int i = latlongModel.indexOf(bean);
            Collections.swap(latlongModel, i, i - 1);
        }
        */

        // Origin of route
        String str_origin = "origin=" + latlongModel.get(0).getLat() + "," + latlongModel.get(0).getLon();
        // Destination of route
        String str_dest = "destination=" + latlongModel.get(latlongModel.size() - 1).getLat() + "," + latlongModel.get(latlongModel.size() - 1).getLon();

        // Sensor enabled
        String sensor = "sensor=false";

        // Waypoints
        String waypoints = "";
        for (int i = 0; i < markerPoints.size(); i++) {
            LatLng point = (LatLng) markerPoints.get(i);
            if (i == 0)
                waypoints = "waypoints=";
            waypoints += point.latitude + "," + point.longitude + "|";
        }

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + waypoints;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?key=AIzaSyBaK8M0QdcKkYYid-DH0iRcpFLd_IGws40&" + parameters;

        Log.e("URL", url);

        return url;

    }


    private class ReadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                HttpConnection http = new HttpConnection();
                data = http.readUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                new ParserTask().execute(result);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }

        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }


        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {

            ArrayList<LatLng> polylinePoints;
            PolylineOptions lineOptions = null;

            if (result != null && result.size() > 0) {
                // Traversing through all the routes
                for (int i = 0; i < result.size(); i++) {
                    polylinePoints = new ArrayList<LatLng>();
                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);

                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        polylinePoints.add(position);
                    }

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(polylinePoints);
                    lineOptions.width(10);
                    lineOptions.color(Color.parseColor("#ff00ff"));
                }
            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                polyline = mMap.addPolyline(lineOptions);
                // isPathDrawn = true;
            } else {
                //Toast.makeText(AdminActivity.this, getString(R.string.no_path), Toast.LENGTH_SHORT).show();
                //clearMap();
            }
        }
    }


    private void createRoute() {

        CreateRouteParams params = new CreateRouteParams();
        params.setName(etRouteName.getText().toString());

        if (sgRouteType.getCheckedRadioButtonId() == R.id.rb_pickup)
            params.setRouteType(rbPickup.getText().toString());
        else
            params.setRouteType(rbDropoff.getText().toString());

        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());

        List<CreateRouteParams.StopageBean> stopList = new ArrayList<>();
        for (int i = 0; i < latlongModel.size(); i++) {

            MapModel bean = latlongModel.get(i);

            CreateRouteParams.StopageBean stopageBean = new CreateRouteParams.StopageBean();
            stopageBean.setAddress(bean.getTitle());
            stopageBean.setIsSchool(bean.isSchool());
            stopageBean.setLatitude(String.valueOf(bean.getLat()));
            stopageBean.setLongitude(String.valueOf(bean.getLon()));
            stopageBean.setSequenceId(String.valueOf(i + 1));
            stopageBean.setStopId(String.valueOf(i + 1));
            stopList.add(stopageBean);
        }
        params.setStopage(stopList);

        Utility.showProgress(AddRouteActivity.this, getString(R.string.processing));

        createCall = AdminApp.getInstance().getApi().createRoute(params);
        createCall.enqueue(new Callback<CreateRouteResponse>() {
            @Override
            public void onResponse(Call<CreateRouteResponse> call, Response<CreateRouteResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            Toast.makeText(AddRouteActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            createRouteResults = response.body().getResult();
                            if (createRouteResults != null)
                                assignDriverToRoute();
                            else
                                setBackIntent();

                        } else {
                            Toast.makeText(AddRouteActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        APIError error = APIError.parseError(response, AddRouteActivity.this, App_Constants.API_CREATE_ROUTE);
                        Toast.makeText(AddRouteActivity.this, error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(AddRouteActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CreateRouteResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(AddRouteActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void assignDriverToRoute() {

        AssignDriverToRouteParams params = new AssignDriverToRouteParams();
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());

        if (allDriverResults != null && allDriverResults.size() > 0) {
            for (GetAllDriverResponse.ResultBean bean : allDriverResults) {
                if ((bean.getFirstName() + " " + bean.getLastName()).equalsIgnoreCase(etDriver.getText().toString())) {
                    params.setDriverId(bean.getDriverId());
                    break;
                }
            }
        }

        AssignDriverToRouteParams.RouteBeanBean routeBean = new AssignDriverToRouteParams.RouteBeanBean();
        routeBean.setId(createRouteResults.getRouteId());
        routeBean.setName(createRouteResults.getName());
        routeBean.setRouteType(createRouteResults.getRouteType());
        routeBean.setUniqueId(createRouteResults.getUniqueId());
        params.setRouteBean(routeBean);

        assignDriverToRouteCall = AdminApp.getInstance().getApi().assignDriverToRoute(params);
        assignDriverToRouteCall.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            //Toast.makeText(AddRouteActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            setBackIntent();
                        } else {
                            Toast.makeText(AddRouteActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        APIError error = APIError.parseError(response, AddRouteActivity.this, App_Constants.API_ASSIGN_DRIVER);
                        Toast.makeText(AddRouteActivity.this, error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(AddRouteActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(AddRouteActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    private void updateRoute() {

        CreateRouteParams params = new CreateRouteParams();
        params.setName(etRouteName.getText().toString());
        params.setId(bean.getId());

        if (sgRouteType.getCheckedRadioButtonId() == R.id.rb_pickup)
            params.setRouteType(rbPickup.getText().toString());
        else
            params.setRouteType(rbDropoff.getText().toString());

        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());

        List<CreateRouteParams.StopageBean> stopList = new ArrayList<>();
        for (int i = 0; i < latlongModel.size(); i++) {

            MapModel bean = latlongModel.get(i);

            CreateRouteParams.StopageBean stopageBean = new CreateRouteParams.StopageBean();
            stopageBean.setAddress(bean.getTitle());
            stopageBean.setIsSchool(bean.isSchool());
            stopageBean.setLatitude(String.valueOf(bean.getLat()));
            stopageBean.setLongitude(String.valueOf(bean.getLon()));
            stopageBean.setSequenceId(String.valueOf(i + 1));
            stopageBean.setStopId(String.valueOf(i + 1));
            stopList.add(stopageBean);
        }
        params.setStopage(stopList);

        Utility.showProgress(AddRouteActivity.this, getString(R.string.processing));

        updateCall = AdminApp.getInstance().getApi().updateRoute(params);
        updateCall.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            Toast.makeText(AddRouteActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            if (bean != null)
                                unassignDriverFromRoute();
                            else
                                setBackIntent();
                        } else {
                            Toast.makeText(AddRouteActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        APIError error = APIError.parseError(response, AddRouteActivity.this, App_Constants.API_UPDATE_ROUTE);
                        Toast.makeText(AddRouteActivity.this, error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(AddRouteActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(AddRouteActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void unassignDriverFromRoute() {

        AssignDriverToRouteParams params = new AssignDriverToRouteParams();
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());

        if (allDriverResults != null && allDriverResults.size() > 0) {
            for (GetAllDriverResponse.ResultBean bean : allDriverResults) {
                if ((bean.getFirstName() + " " + bean.getLastName()).equalsIgnoreCase(etDriver.getText().toString())) {
                    params.setDriverId(bean.getDriverId());
                    break;
                }
            }
        }

        AssignDriverToRouteParams.RouteBeanBean routeBean = new AssignDriverToRouteParams.RouteBeanBean();
        routeBean.setId(bean.getRouteId());
        routeBean.setName(bean.getName());
        routeBean.setRouteType(bean.getRouteType());
        routeBean.setUniqueId(bean.getUniqueId());
        params.setRouteBean(routeBean);

        assignDriverToRouteCall = AdminApp.getInstance().getApi().assignDriverToRoute(params);
        assignDriverToRouteCall.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            //Toast.makeText(AddRouteActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            setBackIntent();
                        } else {
                            Toast.makeText(AddRouteActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        APIError error = APIError.parseError(response, AddRouteActivity.this, App_Constants.API_UN_ASSIGN_DRIVER);
                        Toast.makeText(AddRouteActivity.this, error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(AddRouteActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(AddRouteActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == APIError.UPDATE_TOKEN && resultCode == RESULT_OK) {

            if (data != null) {
                String apiName = data.getStringExtra("api");
                onRelaod(apiName);
            }

        }
    }

    private void onRelaod(String apiName) {

        if (apiName != null) {
            if (apiName.equals(App_Constants.API_CREATE_ROUTE)) {
                createRoute();
            } else if (apiName.equals(App_Constants.API_UPDATE_ROUTE)) {
                updateRoute();
            } else if (apiName.equals(App_Constants.API_ASSIGN_DRIVER)) {
                assignDriverToRoute();
            } else if (apiName.equals(App_Constants.API_GET_ALL_DRIVERS)) {
                getAllDriver();
            } else if (apiName.equals(App_Constants.API_UN_ASSIGN_DRIVER)) {
                unassignDriverFromRoute();
            } else if (apiName.equals(App_Constants.API_GET_FULL_ROUTE_INFO)) {
                getFullRouteInfo();
            }

        }
    }


    private void setBackIntent() {

        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        onBackPressed();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (createCall != null)
            createCall.cancel();

        if (updateCall != null)
            updateCall.cancel();

        if (allDriverApi != null)
            allDriverApi.cancel();

        if (fullRouteInfoApi != null)
            fullRouteInfoApi.cancel();

        if (assignDriverToRouteCall != null)
            assignDriverToRouteCall.cancel();
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
