/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.route.emergency_notification.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.server.GetTripHistory;

public class CommunityPointsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<GetTripHistory.ResultBean.StopageBean> beans;
    private boolean isSingleChoice = false;

    public CommunityPointsAdapter(Context context) {
        this.context = context;
        beans = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new PickUpStopViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_community_stop, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        final GetTripHistory.ResultBean.StopageBean bean = beans.get(position);
        if (holder instanceof PickUpStopViewHolder) {

            ((PickUpStopViewHolder) holder).txtAddress.setText(bean.getSequenceId() + "-" + bean.getAddress());

            if (bean.isSelected())
                ((PickUpStopViewHolder) holder).chk.setChecked(true);
            else
                ((PickUpStopViewHolder) holder).chk.setChecked(false);

            ((PickUpStopViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    bean.setSelected(!bean.isSelected());

                    if (isSingleChoice) {
                        makeOtherUnChecked(holder.getAdapterPosition());
                    }else{
                        notifyDataSetChanged();
                    }
                }
            });
        }
    }

    public void setSingleChoiceMode(boolean value) {

        isSingleChoice = value;
    }

    private void makeOtherUnChecked(int position) {

        for (int i = 0; i < beans.size(); i++) {

            if (i != position)
                beans.get(i).setSelected(false);
        }

        notifyDataSetChanged();
    }

    public String getSelectedStopID() {

        String stopId = null;

        for (int i = 0; i < beans.size(); i++) {

            if (beans.get(i).isSelected()) {
                stopId = beans.get(i).getStopId();
                break;
            }
        }

        return stopId;
    }

    public List<String> getSelectedStopIDList() {

        List<String> stopIdList = new ArrayList<>();

        for (int i = 0; i < beans.size(); i++) {

            if (beans.get(i).isSelected()) {
                stopIdList.add(beans.get(i).getStopId());
            }
        }

        return stopIdList;
    }


    public String getSelectedStopName() {

        String stopName = null;

        for (int i = 0; i < beans.size(); i++) {

            if (beans.get(i).isSelected()) {
                stopName = beans.get(i).getAddress();
                break;
            }
        }

        return stopName;
    }

    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }

    public void addItem(List<GetTripHistory.ResultBean.StopageBean> results) {

        beans.addAll(results);
        notifyDataSetChanged();
    }


    class PickUpStopViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.txt_address)
        TextView txtAddress;
        @BindView(R.id.chk)
        CheckBox chk;
        // R.layout.raw_dialog_pick_up_stop

        public PickUpStopViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

