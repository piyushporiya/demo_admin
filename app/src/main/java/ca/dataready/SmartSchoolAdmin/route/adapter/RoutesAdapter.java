/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.route.adapter;


import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.route.RouteFragment;
import ca.dataready.SmartSchoolAdmin.server.GetAllRoutesResponse;

public class RoutesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private RouteFragment fragment;
    private ArrayList<GetAllRoutesResponse.ResultBean> beans;
    private ArrayList<GetAllRoutesResponse.ResultBean> beansCopy;

    public RoutesAdapter(Context context, RouteFragment routeFragment) {
        this.context = context;
        fragment = routeFragment;
        beans = new ArrayList<>();
        beansCopy = new ArrayList<>();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_routes, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        final GetAllRoutesResponse.ResultBean bean = beans.get(position);

        if (holder instanceof ItemViewHolder) {

            if (bean.getDriverDetails() != null) {
                ((ItemViewHolder) holder).txtDriverName.setText(bean.getDriverDetails().getFirstName() + " " + bean.getDriverDetails().getLastName());
                ((ItemViewHolder) holder).txtDriverName.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(context, R.drawable.ic_driver_gray_55), null, null, null);
                ((ItemViewHolder) holder).txtDriverName.setTextColor(ContextCompat.getColor(context, R.color.gray_55));
            } else {
                ((ItemViewHolder) holder).txtDriverName.setText(context.getString(R.string.no_driver_assigned));
                ((ItemViewHolder) holder).txtDriverName.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(context, R.drawable.ic_driver_gray_aa), null, null, null);
                ((ItemViewHolder) holder).txtDriverName.setTextColor(ContextCompat.getColor(context, R.color.gray_aa));
            }
            ((ItemViewHolder) holder).txtRouteName.setText(bean.getName() + " (" + bean.getRouteId() + ")");
            ((ItemViewHolder) holder).txtRouteType.setText(bean.getRouteType());

            ((ItemViewHolder) holder).imgMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    fragment.showPopUp(((ItemViewHolder) holder).imgMore, holder.getAdapterPosition(), bean);
                }
            });

        }

    }

    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }

    public void addItem(ArrayList<GetAllRoutesResponse.ResultBean> results) {

        /*Collections.sort(results, new Comparator<GetAllRoutesResponse.ResultBean>() {
            public int compare(GetAllRoutesResponse.ResultBean obj1, GetAllRoutesResponse.ResultBean obj2) {
                // ## Ascending order

                if (obj1.getName() == null || obj2.getName() == null)
                    return 0;

                return obj1.getName().compareToIgnoreCase(obj2.getName()); // To compare string values
                // return Integer.valueOf(obj1.empId).compareTo(obj2.empId); // To compare integer values

                // ## Descending order
                // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
                // return Integer.valueOf(obj2.empId).compareTo(obj1.empId); // To compare integer values
            }
        });*/

        beans.addAll(results);
        beansCopy.addAll(results);
        notifyDataSetChanged();
    }

    public void filter(String text) {

        text = text.toLowerCase();
        beans.clear();
        if (!text.isEmpty()) {
            for (GetAllRoutesResponse.ResultBean item : beansCopy) {
                if (item.getName() != null && item.getName().toLowerCase().contains(text)
                        || item.getRouteId() != null && item.getRouteId().toLowerCase().contains(text)) {
                    beans.add(item);
                }
            }
        } else {
            beans.addAll(beansCopy);
        }

        notifyDataSetChanged();
    }


    class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_driver_name)
        TextView txtDriverName;
        @BindView(R.id.img_more)
        ImageView imgMore;
        @BindView(R.id.txt_route_name)
        TextView txtRouteName;
        @BindView(R.id.txt_route_type)
        TextView txtRouteType;
        //R.layout.raw_routes

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
