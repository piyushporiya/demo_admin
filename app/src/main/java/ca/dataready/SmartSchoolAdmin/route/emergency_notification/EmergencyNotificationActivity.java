/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.route.emergency_notification;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.route.emergency_notification.adapter.CommunityPointsAdapter;
import ca.dataready.SmartSchoolAdmin.route.emergency_notification.adapter.GroupOfStudentsAdapter;
import ca.dataready.SmartSchoolAdmin.route.emergency_notification.adapter.SelectedGroupOfStudentsAdapter;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.AllTripsForRoute;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.CommonResponse;
import ca.dataready.SmartSchoolAdmin.server.GetAllRoutesResponse;
import ca.dataready.SmartSchoolAdmin.server.GetTotalBoardingResponse;
import ca.dataready.SmartSchoolAdmin.server.GetTripHistory;
import ca.dataready.SmartSchoolAdmin.server.SendEmergencyCommunicationParams;
import info.hoang8f.android.segmented.SegmentedGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmergencyNotificationActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txt_trip)
    TextView txtTrip;
    @BindView(R.id.linear_trip)
    RelativeLayout linearTrip;
    @BindView(R.id.rb_all)
    RadioButton rbAll;
    @BindView(R.id.rb_community_point)
    RadioButton rbCommunityPoint;
    @BindView(R.id.sgSe)
    SegmentedGroup sgSe;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.txt_error)
    TextView txtError;
    @BindView(R.id.viewAnimator)
    ViewAnimator viewAnimator;
    @BindView(R.id.rb_group_of_student)
    RadioButton rbGroupOfStudent;
    @BindView(R.id.recyclerView)
    RecyclerView ssRecyclerView;
    @BindView(R.id.txtEmergecyCommunicationTrip)
    TextView txtEmergecyCommunicationTrip;
    @BindView(R.id.card_emergency_contact_trip)
    CardView cardEmergencyContactTrip;
    @BindView(R.id.et_title)
    EditText etTitle;
    @BindView(R.id.et_description)
    EditText etDescription;
    @BindView(R.id.layout_emergency_trip)
    CardView layoutEmergencyTrip;
    @BindView(R.id.txtEmergecyCommunicationRoute)
    TextView txtEmergecyCommunicationRoute;
    @BindView(R.id.card_emergency_contact_route)
    CardView cardEmergencyContactRoute;
    @BindView(R.id.et_route_title)
    EditText etRouteTitle;
    @BindView(R.id.et_route_description)
    EditText etRouteDescription;
    @BindView(R.id.layout_emergency_contact_route)
    CardView layoutEmergencyContactRoute;
    @BindView(R.id.btn_submit_route)
    Button btnSubmitRoute;
    @BindView(R.id.txt_edit)
    TextView txtEdit;
    private List<String> tripIdsList;
    private GetAllRoutesResponse.ResultBean bean;
    private String routeId, selectedDate;
    private Call<AllTripsForRoute> appApi;
    private ArrayList<AllTripsForRoute.ResultBean> results;
    private ListPopupWindow listPopupWindow;
    private String tripId;
    private Call<GetTripHistory> tripHistoryApi;
    private AlertDialog alertDialog;
    private CommunityPointsAdapter adapter;
    private String tripType;
    private Call<GetTotalBoardingResponse> apiTotalBoarding;
    private AlertDialog alertDialogTotalBoarding;
    private GroupOfStudentsAdapter adapterTotalBoarding;
    private SelectedGroupOfStudentsAdapter adapterSelectedStudents;
    private Call<CommonResponse> activeTripECApi;
    private String uniqueId;
    private Call<CommonResponse> routeECApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_contact);
        ButterKnife.bind(this);
        setHeaderInfo();
        Init();
    }

    private void setHeaderInfo() {

        setSupportActionBar(toolbar);

        if (AdminApp.getInstance().getAdmin() != null)
            getSupportActionBar().setTitle(AdminApp.getInstance().getAdmin().getSchoolName() + " " + AdminApp.getInstance().getAdmin().getSchoolYear());
        else
            getSupportActionBar().setTitle(getString(R.string.app_name));

        getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + getString(R.string.emergency_notification) + "</small>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void Init() {

        ssRecyclerView.setLayoutManager(new LinearLayoutManager(EmergencyNotificationActivity.this, LinearLayoutManager.HORIZONTAL, false));

        sgSe.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.rb_all) {

                    adapterSelectedStudents = new SelectedGroupOfStudentsAdapter(EmergencyNotificationActivity.this);
                    ssRecyclerView.setAdapter(adapterSelectedStudents);

                    txtEdit.setVisibility(View.GONE);

                } else if (checkedId == R.id.rb_community_point) {

                    if (txtTrip.getText().toString().equalsIgnoreCase(getString(R.string.select_trip_id))) {
                        Toast.makeText(EmergencyNotificationActivity.this, getString(R.string.please_select_trip_id), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    adapterSelectedStudents = new SelectedGroupOfStudentsAdapter(EmergencyNotificationActivity.this);
                    ssRecyclerView.setAdapter(adapterSelectedStudents);

                    txtEdit.setVisibility(View.GONE);

                    getTripHistory();

                } else if (checkedId == R.id.rb_group_of_student) {

                    if (txtTrip.getText().toString().equalsIgnoreCase(getString(R.string.select_trip_id))) {
                        Toast.makeText(EmergencyNotificationActivity.this, getString(R.string.please_select_trip_id), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (results == null)
                        return;

                    if (results.size() == 0)
                        return;

                    for (AllTripsForRoute.ResultBean bean : results) {

                        if (txtTrip.getText().toString().equalsIgnoreCase(bean.getTripId())) {

                            tripType = bean.getTripType();
                            break;
                        }
                    }

                    getTotalBoarding();
                }
            }
        });

        if (getIntent().getExtras() != null) {

            bean = getIntent().getParcelableExtra(App_Constants.OBJECT);
            if (bean != null) {
                routeId = bean.getRouteId();
                uniqueId = bean.getUniqueId();
            }
        }

        Calendar mCalender = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        selectedDate = sdf.format(mCalender.getTime());

        ApiCall();
    }

    private void ApiCall() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();

        appApi = AdminApp.getInstance().getApi().getAllTripsForRoute(entity.getSchoolId(), routeId, selectedDate);
        appApi.enqueue(new Callback<AllTripsForRoute>() {
            @Override
            public void onResponse(Call<AllTripsForRoute> call, Response<AllTripsForRoute> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            results = response.body().getResult();
                        }
                    } else {
                        APIError.parseError(response, EmergencyNotificationActivity.this, App_Constants.API_GET_TRIPS_OF_ROUTES);
                    }
                } catch (Exception e) {
                    Toast.makeText(EmergencyNotificationActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
                viewAnimator.setDisplayedChild(1);

            }

            @Override
            public void onFailure(Call<AllTripsForRoute> call, Throwable t) {
                if (!call.isCanceled()) {
                    viewAnimator.setDisplayedChild(1);
                    Toast.makeText(EmergencyNotificationActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    @OnClick({R.id.txt_edit, R.id.btn_submit, R.id.btn_submit_route, R.id.linear_trip, R.id.card_emergency_contact_route, R.id.card_emergency_contact_trip})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txt_edit:
                getTotalBoarding();
                break;
            case R.id.btn_submit:

                if (etDescription.getText().toString().isEmpty()) {
                    etDescription.setError(getString(R.string.field_required));
                    return;
                }

                if (sgSe.getCheckedRadioButtonId() == R.id.rb_all) {

                    if (bean == null)
                        return;

                    tripType = bean.getRouteType();
                }

                if (sgSe.getCheckedRadioButtonId() == R.id.rb_community_point) {

                    if (results == null)
                        return;

                    if (results.size() == 0)
                        return;

                    for (AllTripsForRoute.ResultBean bean : results) {

                        if (txtTrip.getText().toString().equalsIgnoreCase(bean.getTripId())) {
                            tripType = bean.getTripType();
                            break;
                        }
                    }
                }

                if (sgSe.getCheckedRadioButtonId() == R.id.rb_group_of_student) {

                    if (results == null)
                        return;

                    if (results.size() == 0)
                        return;

                    for (AllTripsForRoute.ResultBean bean : results) {

                        if (txtTrip.getText().toString().equalsIgnoreCase(bean.getTripId())) {
                            tripType = bean.getTripType();
                            break;
                        }
                    }
                }

                sendECToActiveTrip();

                break;
            case R.id.btn_submit_route:

                if (etRouteDescription.getText().toString().isEmpty()) {
                    etRouteDescription.setError(getString(R.string.field_required));
                    return;
                }

                if (bean == null)
                    return;

                tripType = bean.getRouteType();
                Log.e("tripType", "" + tripType);

                sendECToRoute();

                break;
            case R.id.linear_trip:

                show_drop_down_to_select_option(linearTrip, App_Constants.TRIP_IDS);

                break;

            case R.id.card_emergency_contact_route:

                layoutEmergencyContactRoute.setVisibility(layoutEmergencyContactRoute.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                if (layoutEmergencyTrip.getVisibility() == View.VISIBLE) {
                    layoutEmergencyTrip.setVisibility(View.GONE);
                    txtEmergecyCommunicationTrip.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(EmergencyNotificationActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutEmergencyContactRoute.getVisibility() == View.VISIBLE)
                    txtEmergecyCommunicationRoute.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(EmergencyNotificationActivity.this, R.drawable.ic_arrow_up), null);
                else
                    txtEmergecyCommunicationRoute.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(EmergencyNotificationActivity.this, R.drawable.ic_arrow_down), null);

                break;

            case R.id.card_emergency_contact_trip:

                layoutEmergencyTrip.setVisibility(layoutEmergencyTrip.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                if (layoutEmergencyContactRoute.getVisibility() == View.VISIBLE) {
                    layoutEmergencyContactRoute.setVisibility(View.GONE);
                    txtEmergecyCommunicationRoute.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(EmergencyNotificationActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutEmergencyTrip.getVisibility() == View.VISIBLE)
                    txtEmergecyCommunicationTrip.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(EmergencyNotificationActivity.this, R.drawable.ic_arrow_up), null);
                else
                    txtEmergecyCommunicationTrip.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(EmergencyNotificationActivity.this, R.drawable.ic_arrow_down), null);

                break;
        }
    }

    private void show_drop_down_to_select_option(RelativeLayout layout, final String which) {

        tripIdsList = new ArrayList<>();

        if (results != null && results.size() > 0) {
            listPopupWindow = new ListPopupWindow(EmergencyNotificationActivity.this);
            if (which.equals(App_Constants.TRIP_IDS)) {


                for (AllTripsForRoute.ResultBean beans : results) {
                    if (!tripIdsList.contains(beans.getTripId()))
                        tripIdsList.add(beans.getTripId());
                }

                listPopupWindow.setAdapter(new ArrayAdapter(EmergencyNotificationActivity.this, R.layout.list_dropdown_item, tripIdsList));

            }

            listPopupWindow.setAnchorView(layout);
            listPopupWindow.setWidth(ListPopupWindow.WRAP_CONTENT);
            listPopupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);

            listPopupWindow.setModal(true);
            listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    if (which.equals(App_Constants.TRIP_IDS)) {
                        txtTrip.setText(tripIdsList.get(i));
                        tripId = tripIdsList.get(i);

                        if (sgSe.getCheckedRadioButtonId() == R.id.rb_community_point)
                            getTripHistory();
                        else if (sgSe.getCheckedRadioButtonId() == R.id.rb_group_of_student) {

                            if (results == null)
                                return;

                            if (results.size() == 0)
                                return;

                            for (AllTripsForRoute.ResultBean bean : results) {

                                if (txtTrip.getText().toString().equalsIgnoreCase(bean.getTripId())) {

                                    tripType = bean.getTripType();
                                    break;
                                }
                            }

                            getTotalBoarding();
                        }
                    }
                    listPopupWindow.dismiss();

                }
            });
            listPopupWindow.show();
        } else {
            Toast.makeText(this, getString(R.string.no_trip_ids_avail), Toast.LENGTH_SHORT).show();
        }
    }


    private void getTripHistory() {

        AlertDialog.Builder builder = new AlertDialog.Builder(EmergencyNotificationActivity.this);

        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        builder.setCancelable(false);
        View view = LayoutInflater.from(EmergencyNotificationActivity.this).inflate(R.layout.raw_community_point_dialog, null);
        builder.setView(view);

        final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        final ViewAnimator viewAnimator = (ViewAnimator) view.findViewById(R.id.viewAnimator);
        final TextView txtNoData = (TextView) view.findViewById(R.id.txt_no_data);

        recyclerView.setLayoutManager(new LinearLayoutManager(EmergencyNotificationActivity.this));
        recyclerView.addItemDecoration(new DividerItemDecoration(EmergencyNotificationActivity.this, DividerItemDecoration.VERTICAL));

        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                rbAll.setChecked(true);
                dialogInterface.dismiss();
            }
        });

        alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {

                CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();

                tripHistoryApi = AdminApp.getInstance().getApi().getTripHistory(entity.getSchoolId(), AdminApp.getInstance().getAdmin().getSchoolYear(), tripId);
                tripHistoryApi.enqueue(new Callback<GetTripHistory>() {
                    @Override
                    public void onResponse(Call<GetTripHistory> call, Response<GetTripHistory> response) {
                        try {
                            if (response.isSuccessful()) {
                                if (response.body().isStatus()) {

                                    adapter = new CommunityPointsAdapter(EmergencyNotificationActivity.this);
                                    recyclerView.setAdapter(adapter);
                                    adapter.setSingleChoiceMode(false);

                                    final GetTripHistory.ResultBean results = response.body().getResult();
                                    if (results != null) {

                                        if (results.getStopage() != null && results.getStopage().size() > 0) {

                                            adapter.addItem(results.getStopage());
                                            viewAnimator.setDisplayedChild(1);

                                        } else {

                                            viewAnimator.setDisplayedChild(2);
                                            txtNoData.setText(getString(R.string.no_data));
                                        }

                                    } else {
                                        viewAnimator.setDisplayedChild(2);
                                        txtNoData.setText(getString(R.string.no_data));
                                    }

                                } else {
                                    viewAnimator.setDisplayedChild(2);
                                    txtNoData.setText(getString(R.string.no_data));
                                }
                            } else {

                                if (alertDialog != null && alertDialog.isShowing())
                                    alertDialog.dismiss();

                                APIError error = APIError.parseError(response, EmergencyNotificationActivity.this, App_Constants.API_GET_TRIP_HISTORY);
                                viewAnimator.setDisplayedChild(2);
                                txtNoData.setText(error.message());

                            }
                        } catch (Exception e) {
                            viewAnimator.setDisplayedChild(2);
                            txtNoData.setText(getString(R.string.somethingwrong));
                        }

                    }

                    @Override
                    public void onFailure(Call<GetTripHistory> call, Throwable t) {
                        if (!call.isCanceled()) {
                            viewAnimator.setDisplayedChild(2);
                            txtNoData.setText(App_Constants.NO_INTERNET);
                        }
                    }
                });

            }
        });


        alertDialog.show();


        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.90);

        if (alertDialog != null && alertDialog.getWindow() != null) {
            alertDialog.getWindow().setLayout(screenWidth, ViewGroup.LayoutParams.WRAP_CONTENT); //set below the setContentview
        }

    }


    private void getTotalBoarding() {

        AlertDialog.Builder builder = new AlertDialog.Builder(EmergencyNotificationActivity.this);

        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        builder.setCancelable(true);
        View view = LayoutInflater.from(EmergencyNotificationActivity.this).inflate(R.layout.raw_group_of_student_dialog, null);
        builder.setView(view);

        final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        final ViewAnimator viewAnimator = (ViewAnimator) view.findViewById(R.id.viewAnimator);
        final TextView txtNoData = (TextView) view.findViewById(R.id.txt_no_data);

        recyclerView.setLayoutManager(new GridLayoutManager(EmergencyNotificationActivity.this, 5));

        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                adapterSelectedStudents = new SelectedGroupOfStudentsAdapter(EmergencyNotificationActivity.this);
                ssRecyclerView.setAdapter(adapterSelectedStudents);

                if (adapterTotalBoarding != null && adapterTotalBoarding.getSelectedStudent().size() > 0) {
                    adapterSelectedStudents.addItem(adapterTotalBoarding.getSelectedStudent());
                    txtEdit.setVisibility(View.VISIBLE);
                } else {
                    txtEdit.setVisibility(View.GONE);
                    rbAll.setChecked(true);
                }


                dialogInterface.dismiss();
            }
        });

        builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                rbAll.setChecked(true);
                dialogInterface.dismiss();
            }
        });

        alertDialogTotalBoarding = builder.create();
        alertDialogTotalBoarding.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {

                CREDENTIAL.ResultBean bean = AdminApp.getInstance().getAdmin();
                apiTotalBoarding = AdminApp.getInstance().getApi().getTotalBoarding(bean.getSchoolId(), bean.getSchoolYear(), routeId, tripId, tripType);
                apiTotalBoarding.enqueue(new Callback<GetTotalBoardingResponse>() {
                    @Override
                    public void onResponse(Call<GetTotalBoardingResponse> call, Response<GetTotalBoardingResponse> response) {

                        try {
                            if (response.isSuccessful()) {

                                if (response.body().isStatus()) {

                                    adapterTotalBoarding = new GroupOfStudentsAdapter(EmergencyNotificationActivity.this);
                                    recyclerView.setAdapter(adapterTotalBoarding);
                                    adapterTotalBoarding.setSingleChoiceMode(false);

                                    if (response.body().getResult() != null && response.body().getResult().size() > 0) {

                                        if (adapterSelectedStudents != null && adapterSelectedStudents.getItemCount() > 0) {

                                            for (GetTotalBoardingResponse.ResultBean bean : response.body().getResult()) {

                                                for (GetTotalBoardingResponse.ResultBean model : adapterSelectedStudents.getVisibleStudent()) {

                                                    if (bean.getCardId().equals(model.getCardId())) {

                                                        bean.setSelected(true);
                                                    }
                                                }
                                            }
                                        }

                                        adapterTotalBoarding.addItem(response.body().getResult());
                                        viewAnimator.setDisplayedChild(1);
                                    } else {

                                        viewAnimator.setDisplayedChild(2);
                                        txtNoData.setText(getString(R.string.no_data));
                                    }
                                } else {

                                    viewAnimator.setDisplayedChild(2);
                                    txtNoData.setText(getString(R.string.no_data));
                                }
                            } else {

                                if (alertDialogTotalBoarding != null && alertDialogTotalBoarding.isShowing())
                                    alertDialogTotalBoarding.dismiss();

                                APIError error = APIError.parseError(response, EmergencyNotificationActivity.this, App_Constants.API_GET_TOTAL_BOARDING);
                                viewAnimator.setDisplayedChild(2);
                                txtNoData.setText(error.message());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            viewAnimator.setDisplayedChild(2);
                            txtNoData.setText(getString(R.string.somethingwrong));

                        }
                    }

                    @Override
                    public void onFailure(Call<GetTotalBoardingResponse> call, Throwable t) {

                        if (!call.isCanceled()) {
                            viewAnimator.setDisplayedChild(2);
                            txtNoData.setText(App_Constants.NO_INTERNET);
                        }
                    }
                });

            }
        });


        alertDialogTotalBoarding.show();


        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.90);

        if (alertDialogTotalBoarding != null && alertDialogTotalBoarding.getWindow() != null) {
            alertDialogTotalBoarding.getWindow().setLayout(screenWidth, ViewGroup.LayoutParams.WRAP_CONTENT); //set below the setContentview
        }
    }

    private void sendECToActiveTrip() {

        Utility.showProgress(EmergencyNotificationActivity.this, getString(R.string.processing));

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();
        SendEmergencyCommunicationParams params = new SendEmergencyCommunicationParams();
        params.setMessage(etDescription.getText().toString());
        params.setRouteId(routeId);
        params.setRouteType(tripType);
        params.setSchoolId(entity.getSchoolId());
        params.setSchoolYear(entity.getSchoolYear());
        params.setStaffEmailId(entity.getEmailId());
        params.setStaffId(entity.getId());
        params.setStaffName(entity.getFirstName() + " " + entity.getLastName());
        params.setStaffProfilePic(entity.getProfilePic());
        params.setUniqueId(uniqueId);

        List<String> stopIdsList = new ArrayList<>();
        if (sgSe.getCheckedRadioButtonId() == R.id.rb_community_point) {
            if (adapter != null && adapter.getSelectedStopIDList().size() > 0) {
                stopIdsList.addAll(adapter.getSelectedStopIDList());
            }
        }
        params.setStopId(stopIdsList);

        List<SendEmergencyCommunicationParams.ToIdBean> studentsList = new ArrayList<>();
        if (sgSe.getCheckedRadioButtonId() == R.id.rb_group_of_student) {
            if (adapterSelectedStudents != null && adapterSelectedStudents.getVisibleStudent().size() > 0) {

                for (GetTotalBoardingResponse.ResultBean bean : adapterSelectedStudents.getVisibleStudent()) {

                    SendEmergencyCommunicationParams.ToIdBean studentsBean = new SendEmergencyCommunicationParams.ToIdBean();
                    studentsBean.setCardId(bean.getCardId());
                    studentsBean.setProfileId(bean.getProfileId());
                    studentsBean.setProfileType(bean.getProfileType());
                    studentsList.add(studentsBean);
                }
            }
        }
        params.setToId(studentsList);


        activeTripECApi = AdminApp.getInstance().getApi().sendEmergencyCommunication(params);
        activeTripECApi.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            Toast.makeText(EmergencyNotificationActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            setBackIntent();

                        } else {
                            Toast.makeText(EmergencyNotificationActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        APIError error = APIError.parseError(response, EmergencyNotificationActivity.this, App_Constants.API_SEND_EMERGENCY_COMMUNICATION_TO_ACTIVE_TRIP);
                        Toast.makeText(EmergencyNotificationActivity.this, "" + error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(EmergencyNotificationActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(EmergencyNotificationActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void sendECToRoute() {

        Utility.showProgress(EmergencyNotificationActivity.this, getString(R.string.processing));

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();
        SendEmergencyCommunicationParams params = new SendEmergencyCommunicationParams();
        params.setMessage(etRouteDescription.getText().toString());
        params.setRouteId(routeId);
        params.setRouteType(tripType);
        params.setSchoolId(entity.getSchoolId());
        params.setSchoolYear(entity.getSchoolYear());
        params.setStaffEmailId(entity.getEmailId());
        params.setStaffId(entity.getId());
        params.setStaffName(entity.getFirstName() + " " + entity.getLastName());
        params.setStaffProfilePic(entity.getProfilePic());
        params.setUniqueId(uniqueId);
        List<String> stopIdsList = new ArrayList<>();
        params.setStopId(stopIdsList);
        List<SendEmergencyCommunicationParams.ToIdBean> studentsList = new ArrayList<>();
        params.setToId(studentsList);

        routeECApi = AdminApp.getInstance().getApi().sendEmergencyCommunication(params);
        routeECApi.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            Toast.makeText(EmergencyNotificationActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            setBackIntent();

                        } else {
                            Toast.makeText(EmergencyNotificationActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        APIError error = APIError.parseError(response, EmergencyNotificationActivity.this, App_Constants.API_SEND_EMERGENCY_COMMUNICATION_TO_ROUTE);
                        Toast.makeText(EmergencyNotificationActivity.this, "" + error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(EmergencyNotificationActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(EmergencyNotificationActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void setBackIntent() {
        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == APIError.UPDATE_TOKEN && resultCode == RESULT_OK) {

            if (data != null) {
                String apiName = data.getStringExtra("api");
                onRelaod(apiName);
            }
        }
    }

    private void onRelaod(String apiName) {
        if (apiName != null) {
            if (apiName.equals(App_Constants.API_GET_TRIPS_OF_ROUTES)) {
                ApiCall();
            } else if (apiName.equals(App_Constants.API_GET_TRIP_HISTORY)) {
                getTripHistory();
            } else if (apiName.equals(App_Constants.API_SEND_EMERGENCY_COMMUNICATION_TO_ACTIVE_TRIP)) {
                sendECToActiveTrip();
            } else if (apiName.equals(App_Constants.API_SEND_EMERGENCY_COMMUNICATION_TO_ROUTE)) {
                sendECToRoute();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (appApi != null)
            appApi.cancel();

        if (tripHistoryApi != null)
            tripHistoryApi.cancel();

        if (activeTripECApi != null)
            activeTripECApi.cancel();

        if (routeECApi != null)
            routeECApi.cancel();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

}
