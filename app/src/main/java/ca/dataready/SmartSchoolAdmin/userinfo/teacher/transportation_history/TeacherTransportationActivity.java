/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.userinfo.teacher.transportation_history;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.LastItemDecoration;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.AppApi;
import ca.dataready.SmartSchoolAdmin.server.GetAllTeacherResponse;
import ca.dataready.SmartSchoolAdmin.server.StudentBordingDetails;
import ca.dataready.SmartSchoolAdmin.userinfo.student.transportation_history.adapter.StudentTransportationAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TeacherTransportationActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.img_back_arrow)
    ImageView imgBackArrow;
    @BindView(R.id.profile)
    SimpleDraweeView profile;
    @BindView(R.id.txt_toolbar_title)
    TextView txtToolbarTitle;
    @BindView(R.id.toolbar)
    RelativeLayout toolbar;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.recyclerView)
    SuperRecyclerView recyclerView;
    GetAllTeacherResponse.ResultBean bean;
    @BindView(R.id.txt_to_date)
    TextView txtToDate;
    private GridLayoutManager gridLayoutManager;
    private StudentTransportationAdapter adapter;
    private Call<StudentBordingDetails> apiCall;
    private String emaidId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_transportation);
        ButterKnife.bind(this);
        Init();
    }

    private void Init() {

        if (getIntent().getExtras() != null) {
            bean = getIntent().getParcelableExtra(App_Constants.OBJECT);
            if (bean != null) {
                profile.setImageURI(AppApi.BASE_URL + bean.getProfilePic());
                emaidId = bean.getEmailId();
                txtToolbarTitle.setText(bean.getFirstName() + " " + bean.getLastName());
            }
        }


       /* if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            gridLayoutManager = new GridLayoutManager(this, 3);
        } else {
            gridLayoutManager = new GridLayoutManager(this, 2);
        }*/

        recyclerView.setLayoutManager(new LinearLayoutManager(TeacherTransportationActivity.this));
        recyclerView.addItemDecoration(new LastItemDecoration());
        recyclerView.setRefreshListener(this);

        adapter = new StudentTransportationAdapter(TeacherTransportationActivity.this);
        recyclerView.setAdapter(adapter);
        TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
        emptyView.setText(getString(R.string.no_data));

    }

    @OnClick({R.id.img_back_arrow, R.id.txt_date, R.id.txt_to_date})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back_arrow:

                finish();

                break;

            case R.id.txt_date:

                setDate(txtDate, false);

                break;

            case R.id.txt_to_date:

                setDate(txtToDate, true);

                break;
        }
    }

    private void setDate(final TextView textView, final boolean callApi) {

        final Calendar mCalender = Calendar.getInstance();

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub

                mCalender.set(Calendar.YEAR, year);
                mCalender.set(Calendar.MONTH, monthOfYear);
                mCalender.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                textView.setText(sdf.format(mCalender.getTime()));

                if (callApi) {
                    getTransportationDetails();
                } else {
                    txtToDate.setText(getString(R.string.select_to_date));
                }

            }
        };

        DatePickerDialog dialog = new DatePickerDialog(this
                , R.style.datepickerCustom, date, mCalender.get(Calendar.YEAR), mCalender.get(Calendar.MONTH),
                mCalender.get(Calendar.DAY_OF_MONTH));

        dialog.show();

    }

    private void getTransportationDetails() {

        recyclerView.showProgress();

        apiCall = AdminApp.getInstance().getApi().getStaffTransportationDetails(AdminApp.getInstance().getAdmin().getSchoolId()
                , AdminApp.getInstance().getAdmin().getSchoolYear(), emaidId, txtDate.getText().toString(), txtToDate.getText().toString());
        apiCall.enqueue(new Callback<StudentBordingDetails>() {
            @Override
            public void onResponse(Call<StudentBordingDetails> call, final Response<StudentBordingDetails> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            if (response.body().getResult() != null && response.body().getResult().size() > 0) {

                                adapter = new StudentTransportationAdapter(TeacherTransportationActivity.this);
                                recyclerView.setAdapter(adapter);
                                adapter.addItem(response.body().getResult());

                            } else {

                                adapter = new StudentTransportationAdapter(TeacherTransportationActivity.this);
                                recyclerView.setAdapter(adapter);
                                TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                                emptyView.setText(getString(R.string.no_data));
                            }

                        } else {

                            adapter = new StudentTransportationAdapter(TeacherTransportationActivity.this);
                            recyclerView.setAdapter(adapter);
                            TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                            emptyView.setText(response.body().getMessage());

                        }
                    } else {

                        APIError error = APIError.parseError(response, TeacherTransportationActivity.this, App_Constants.API_TRANSPORTATION_DETAILS);
                        adapter = new StudentTransportationAdapter(TeacherTransportationActivity.this);
                        recyclerView.setAdapter(adapter);
                        TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                        emptyView.setText(error.message());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    adapter = new StudentTransportationAdapter(TeacherTransportationActivity.this);
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(getString(R.string.somethingwrong));
                }
            }

            @Override
            public void onFailure(Call<StudentBordingDetails> call, Throwable t) {
                if (!call.isCanceled()) {
                    adapter = new StudentTransportationAdapter(TeacherTransportationActivity.this);
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(App_Constants.NO_INTERNET);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == APIError.UPDATE_TOKEN && resultCode == RESULT_OK) {

            if (data != null) {
                String apiName = data.getStringExtra("api");
                onReload(apiName);
            }

        }
    }

    public void onReload(String apiName) {

        if (apiName != null) {
            if (apiName.equals(App_Constants.API_TRANSPORTATION_DETAILS)) {
                getTransportationDetails();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (apiCall != null)
            apiCall.cancel();
    }

    @Override
    public void onRefresh() {

        getTransportationDetails();
    }
}
