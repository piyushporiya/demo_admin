/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.userinfo.teacher.adapter;


import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.MonthYearPicker;
import ca.dataready.SmartSchoolAdmin.server.AddTeacherParams;
import ca.dataready.SmartSchoolAdmin.userinfo.teacher.add.AddTeacherActivity;

public class ExperienceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<AddTeacherParams.ExperienceDetailsBean.ExperiencesBean> beans;
    private MonthYearPicker myp;


    public ExperienceAdapter(Context context) {
        this.context = context;
        beans = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_experience, parent, false));
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        final AddTeacherParams.ExperienceDetailsBean.ExperiencesBean bean = beans.get(position);

        if (holder instanceof ItemViewHolder) {


            ((ItemViewHolder) holder).etOrganizationName.setText(bean.getOrganizationName());
            ((ItemViewHolder) holder).etValidFrom.setText(bean.getFromYear());
            ((ItemViewHolder) holder).etValidTill.setText(bean.getToYear());
            ((ItemViewHolder) holder).etAddress.setText(bean.getAddress());


            ((ItemViewHolder) holder).etOrganizationName.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    bean.setOrganizationName(charSequence.toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });


            ((ItemViewHolder) holder).etValidFrom.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    /*if (!((ItemViewHolder) holder).etValidTill.getText().toString().isEmpty()) {
                        if (charSequence.toString().isEmpty())
                            bean.setPassedYear("");
                        else
                            bean.setPassedYear(charSequence.toString() + " - " + ((ItemViewHolder) holder).etValidTill.getText().toString() );
                    }*/

                    bean.setFromYear(charSequence.toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });


            ((ItemViewHolder) holder).etValidTill.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    /*if (!((ItemViewHolder) holder).etValidFrom.getText().toString().isEmpty()) {
                        if (charSequence.toString().isEmpty())
                            bean.setPassedYear("");
                        else
                            bean.setPassedYear(((ItemViewHolder) holder).etValidFrom.getText().toString() + " - " + charSequence.toString());
                    }*/

                    bean.setToYear(charSequence.toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            ((ItemViewHolder) holder).etAddress.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    bean.setAddress(charSequence.toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            ((ItemViewHolder) holder).etValidFrom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    setMonthYear(((ItemViewHolder) holder).etValidFrom);

                }
            });

            ((ItemViewHolder) holder).etValidTill.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    setMonthYear(((ItemViewHolder) holder).etValidTill);
                }
            });

            ((ItemViewHolder) holder).imgRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    beans.remove(bean);
                    notifyDataSetChanged();
                }
            });

        }

    }


    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }

    public void addItem(ArrayList<AddTeacherParams.ExperienceDetailsBean.ExperiencesBean> results) {

        beans.addAll(results);

        notifyDataSetChanged();
    }

    private void setMonthYear(final EditText editText) {


        myp = new MonthYearPicker(((AddTeacherActivity) context));
        myp.build(new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                editText.setText(myp.getSelectedMonthName() + " " + myp.getSelectedYear());

            }
        }, null);
        myp.show();

    }

    public ArrayList<AddTeacherParams.ExperienceDetailsBean.ExperiencesBean> getExperienceData() {

        return beans;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.et_organization_name)
        EditText etOrganizationName;
        @BindView(R.id.et_valid_from)
        EditText etValidFrom;
        @BindView(R.id.et_valid_till)
        EditText etValidTill;
        @BindView(R.id.img_remove)
        ImageView imgRemove;
        @BindView(R.id.et_address)
        EditText etAddress;
        //R.layout.raw_qualification

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
