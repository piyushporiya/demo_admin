/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.userinfo.student.add;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.LatLng;
import com.hbb20.CountryCodePicker;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.DateFunction;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.Utilities.place.PlaceAPIAdapter;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.common.adapter.AttachmentAdapter;
import ca.dataready.SmartSchoolAdmin.configuration.school_info.SchoolInfoFragment;
import ca.dataready.SmartSchoolAdmin.fileselectoer.FileSelectionActivity;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.AddDriverModel;
import ca.dataready.SmartSchoolAdmin.server.AddStudentParams;
import ca.dataready.SmartSchoolAdmin.server.AddStudentResponse;
import ca.dataready.SmartSchoolAdmin.server.AddTeacherParams;
import ca.dataready.SmartSchoolAdmin.server.AppApi;
import ca.dataready.SmartSchoolAdmin.server.GetLatLngFromAddressResponse;
import ca.dataready.SmartSchoolAdmin.server.GetQrCodeResponse;
import ca.dataready.SmartSchoolAdmin.server.GetStudentProfileResponse;
import ca.dataready.SmartSchoolAdmin.server.GetStudentsList;
import ca.dataready.SmartSchoolAdmin.server.MultipleImageUpload;
import ca.dataready.SmartSchoolAdmin.userinfo.student.admin_process.StudentAdminProcessActivity;
import ca.dataready.SmartSchoolAdmin.userinfo.teacher.adapter.ExperienceAdapter;
import ca.dataready.SmartSchoolAdmin.viewresponse.adapter.FilesAdapter;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.FileModel;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.ImageUpload;
import info.hoang8f.android.segmented.SegmentedGroup;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddStudentActivity extends BaseActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txtProfile)
    TextView txtProfile;
    @BindView(R.id.card_profile_info)
    CardView cardProfileInfo;
    @BindView(R.id.et_first_name)
    EditText etFirstName;
    @BindView(R.id.et_last_name)
    EditText etLastName;
    @BindView(R.id.et_dob)
    EditText etDob;
    @BindView(R.id.rb_male)
    RadioButton rbMale;
    @BindView(R.id.rb_female)
    RadioButton rbFemale;
    @BindView(R.id.sgGender)
    SegmentedGroup sgGender;
    @BindView(R.id.et_date_of_joining)
    EditText etJoiningDate;
    @BindView(R.id.et_address)
    AutoCompleteTextView etAddress;
    @BindView(R.id.img_address)
    ImageView imgAddress;
    @BindView(R.id.teacher_profile_pic)
    ImageView studentProfilePic;
    @BindView(R.id.linaer_capture_image)
    LinearLayout linaerCaptureImage;
    @BindView(R.id.btn_edit)
    Button btnEdit;
    @BindView(R.id.btn_next)
    Button btnNext;
    @BindView(R.id.layout_ptofile_info)
    CardView layoutPtofileInfo;
    @BindView(R.id.txtEmergecy)
    TextView txtEmergecy;
    @BindView(R.id.card_emergency_contact)
    CardView cardEmergencyContact;
    @BindView(R.id.et_ec_first_name)
    EditText etEcFirstName;
    @BindView(R.id.et_ec_last_name)
    EditText etEcLastName;
    @BindView(R.id.et_ec_relation)
    EditText etEcRelation;
    @BindView(R.id.et_ec_phone)
    EditText etEcPhone;
    @BindView(R.id.et_ec_email)
    EditText etEcEmail;
    @BindView(R.id.et_ec_address)
    AutoCompleteTextView etEcAddress;
    @BindView(R.id.img_ec_address)
    ImageView imgEcAddress;
    @BindView(R.id.btn_ec_previous)
    Button btnEcPrevious;
    @BindView(R.id.btn_ec_next)
    Button btnEcNext;
    @BindView(R.id.layout_emergency_contact)
    CardView layoutEmergencyContact;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.txt_achivements)
    TextView txtAchivements;
    @BindView(R.id.card_achivements)
    CardView cardAchivements;
    @BindView(R.id.btn_sa_previous)
    Button btnSaPrevious;
    @BindView(R.id.btn_sa_next)
    Button btnSaNext;
    @BindView(R.id.layout_achivements)
    CardView layoutAchivements;
    @BindView(R.id.txt_error)
    TextView txtError;
    @BindView(R.id.viewAnimator)
    ViewAnimator viewAnimator;
    @BindView(R.id.et_studentId)
    EditText etStudentId;
    @BindView(R.id.et_birth_palace)
    EditText etBirthPalace;
    @BindView(R.id.et_nationality)
    EditText etNationality;
    @BindView(R.id.et_mother_tounge)
    EditText etMotherTounge;
    @BindView(R.id.et_religion)
    EditText etReligion;
    @BindView(R.id.txtParentInfo)
    TextView txtParentInfo;
    @BindView(R.id.card_parent_info)
    CardView cardParentInfo;
    @BindView(R.id.et_pi_father_first_name)
    EditText etPiFatherFirstName;
    @BindView(R.id.et_pi_father_last_name)
    EditText etPiFatherLastName;
    @BindView(R.id.et_pi_father_occuption)
    EditText etPiFatherOccuption;
    @BindView(R.id.et_pi_father_phone)
    EditText etPiFatherPhone;
    @BindView(R.id.et_pi_father_email_address)
    EditText etPiFatherEmailAddress;
    @BindView(R.id.et_pi_mother_first_name)
    EditText etPiMotherFirstName;
    @BindView(R.id.et_pi_mother_last_name)
    EditText etPiMotherLastName;
    @BindView(R.id.et_pi_mother_occuption)
    EditText etPiMotherOccuption;
    @BindView(R.id.et_pi_mother_phone)
    EditText etPiMotherPhone;
    @BindView(R.id.et_pi_mother_email_address)
    EditText etPiMotherEmailAddress;
    @BindView(R.id.father_profile_pic)
    ImageView fatherProfilePic;
    @BindView(R.id.linaer_capture_father_image)
    LinearLayout linaerCaptureFatherImage;
    @BindView(R.id.btn_father_pic_edit)
    Button btnFatherPicEdit;
    @BindView(R.id.mother_profile_pic)
    ImageView motherProfilePic;
    @BindView(R.id.linaer_capture_mother_image)
    LinearLayout linaerCaptureMotherImage;
    @BindView(R.id.btn_mother_pic_edit)
    Button btnMotherPicEdit;
    @BindView(R.id.btn_pi_previous)
    Button btnPiPrevious;
    @BindView(R.id.btn_pi_next)
    Button btnPiNext;
    @BindView(R.id.layout_parent_info)
    CardView layoutParentInfo;
    @BindView(R.id.et_ec_dob)
    EditText etEcDob;
    @BindView(R.id.et_ec_education)
    EditText etEcEducation;
    @BindView(R.id.et_ec_occupation)
    EditText etEcOccupation;
    @BindView(R.id.txtPastSchool)
    TextView txtPastSchool;
    @BindView(R.id.card_past_info_contact)
    CardView cardPastInfoContact;
    @BindView(R.id.et_psi_school_name)
    EditText etPsiSchoolName;
    @BindView(R.id.et_psi_passing_year)
    EditText etPsiPassingYear;
    @BindView(R.id.et_psi_grade)
    EditText etPsiGrade;
    @BindView(R.id.et_psi_class)
    EditText etPsiClass;
    @BindView(R.id.et_psi_address)
    AutoCompleteTextView etPsiAddress;
    @BindView(R.id.img_psi_address)
    ImageView imgPsiAddress;
    @BindView(R.id.btn_psi_previous)
    Button btnPsiPrevious;
    @BindView(R.id.btn_psi_next)
    Button btnPsiNext;
    @BindView(R.id.layout_past_info_contact)
    CardView layoutPastInfoContact;
    @BindView(R.id.txtPhysician)
    TextView txtPhysician;
    @BindView(R.id.card_physician_contact)
    CardView cardPhysicianContact;
    @BindView(R.id.et_pci_first_name)
    EditText etPciFirstName;
    @BindView(R.id.et_pci_last_name)
    EditText etPciLastName;
    @BindView(R.id.et_pci_phone_number)
    EditText etPciPhoneNumber;
    @BindView(R.id.et_pci_fax_number)
    EditText etPciFaxNumber;
    @BindView(R.id.btn_pci_previous)
    Button btnPciPrevious;
    @BindView(R.id.btn_pci_next)
    Button btnPciNext;
    @BindView(R.id.layout_physician_contact)
    CardView layoutPhysicianContact;
    @BindView(R.id.img_pci_address)
    ImageView imgPciAddress;
    @BindView(R.id.chk_transportation)
    CheckBox chkTransportation;
    @BindView(R.id.rb_at_home)
    RadioButton rbAtHome;
    @BindView(R.id.rb_community_point)
    RadioButton rbCommunityPoint;
    @BindView(R.id.sgTransportation)
    SegmentedGroup sgTransportation;
    @BindView(R.id.textFeedback)
    TextView textFeedback;
    @BindView(R.id.card_feedback)
    CardView cardFeedback;
    @BindView(R.id.et_fb_trasportation_comment)
    EditText etFbTrasportationComment;
    @BindView(R.id.et_fb_school_comment)
    EditText etFbSchoolComment;
    @BindView(R.id.layout_card_feedback)
    CardView layoutCardFeedback;
    @BindView(R.id.btn_fb_previous)
    Button btnFbPrevious;
    @BindView(R.id.btn_fb_next)
    Button btnFbNext;
    @BindView(R.id.et_pci_address)
    AutoCompleteTextView etPciAddress;
    @BindView(R.id.txt_profile_attachment)
    TextView txtProfileAttachment;
    @BindView(R.id.rv_profile_attachment)
    RecyclerView rvProfileAttachment;
    @BindView(R.id.txt_parent_attachment)
    TextView txtParentAttachment;
    @BindView(R.id.rv_parent_attachment)
    RecyclerView rvParentAttachment;
    @BindView(R.id.txt_ec_attachment)
    TextView txtEcAttachment;
    @BindView(R.id.rv_ec_attachment)
    RecyclerView rvEcAttachment;
    @BindView(R.id.txt_past_school_attachment)
    TextView txtPastSchoolAttachment;
    @BindView(R.id.rv_past_school_attachment)
    RecyclerView rvPastSchoolAttachment;
    @BindView(R.id.txt_other_attachments)
    TextView txtOtherAttachments;
    @BindView(R.id.rv_other_attachment)
    RecyclerView rvOtherAttachment;
    @BindView(R.id.img_qrcode)
    ImageView imgQrcode;
    @BindView(R.id.txt_qr_not_avail)
    TextView txtQrNotAvail;
    @BindView(R.id.qr_frame)
    FrameLayout qrFrame;
    @BindView(R.id.country_code_pi_father)
    CountryCodePicker countryCodePiFather;
    @BindView(R.id.country_code_pi_mother)
    CountryCodePicker countryCodePiMother;
    @BindView(R.id.chk_send_invitation)
    CheckBox chkSendInvitation;
    @BindView(R.id.country_code_ec)
    CountryCodePicker countryCodeEc;
    @BindView(R.id.country_code_pci)
    CountryCodePicker countryCodePci;
    /*  @BindView(R.id.recyclerView)
          RecyclerView recyclerView;*/
    private Call<AddStudentResponse> registerStudentCall;
    private String picturePath;
    private String studentPicURL, fatherPicURL, motherPicURL;
    private FilesAdapter filesAdapter;
    private List<FileModel> selectedFiles = new ArrayList<>();
    private String documentSelectionType;
    private String selectedGender, selectedMartialStatus;
    private ExperienceAdapter experienceAdapter;
    private ArrayList<AddTeacherParams.QualificationDetailsBean.QualificationsBean> QualificationList;
    private ArrayList<AddTeacherParams.ExperienceDetailsBean.ExperiencesBean> experienceList;
    private Call<GetStudentProfileResponse> getStudentInfoCall;
    private String emailId;
    private String id;
    private Call<AddStudentResponse> updateStudentCall;
    private Call<GetLatLngFromAddressResponse> locationCall;
    private String profileType;
    private boolean isPending;
    private String studentId;
    private GetStudentsList.ResultBean bean;
    private boolean isOldStudent;
    private Call<MultipleImageUpload> attachMentCall;
    private AttachmentAdapter attachmentAdapter;
    private List<MultipleImageUpload.ResultBean> profileFiles = new ArrayList<>();
    private List<MultipleImageUpload.ResultBean> parentFiles = new ArrayList<>();
    private List<MultipleImageUpload.ResultBean> pastSchoolFiles = new ArrayList<>();
    private List<MultipleImageUpload.ResultBean> emergencyFiles = new ArrayList<>();
    private List<MultipleImageUpload.ResultBean> otherFiles = new ArrayList<>();
    private Call<GetQrCodeResponse> getQRCall;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);
        ButterKnife.bind(this);
        setHeaderInfo();
        Init();
    }

    private void setHeaderInfo() {

        setSupportActionBar(toolbar);

        if (AdminApp.getInstance().getAdmin() != null)
            getSupportActionBar().setTitle(AdminApp.getInstance().getAdmin().getSchoolName() + " " + AdminApp.getInstance().getAdmin().getSchoolYear());
        else
            getSupportActionBar().setTitle(getString(R.string.app_name));

        getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + getString(R.string.add_student) + "</small>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void Init() {


        rvProfileAttachment.setLayoutManager(new LinearLayoutManager(AddStudentActivity.this));
        rvProfileAttachment.addItemDecoration(new DividerItemDecoration(AddStudentActivity.this, DividerItemDecoration.VERTICAL));

        rvParentAttachment.setLayoutManager(new LinearLayoutManager(AddStudentActivity.this));
        rvParentAttachment.addItemDecoration(new DividerItemDecoration(AddStudentActivity.this, DividerItemDecoration.VERTICAL));

        rvPastSchoolAttachment.setLayoutManager(new LinearLayoutManager(AddStudentActivity.this));
        rvPastSchoolAttachment.addItemDecoration(new DividerItemDecoration(AddStudentActivity.this, DividerItemDecoration.VERTICAL));

        rvEcAttachment.setLayoutManager(new LinearLayoutManager(AddStudentActivity.this));
        rvEcAttachment.addItemDecoration(new DividerItemDecoration(AddStudentActivity.this, DividerItemDecoration.VERTICAL));

        rvOtherAttachment.setLayoutManager(new LinearLayoutManager(AddStudentActivity.this));
        rvOtherAttachment.addItemDecoration(new DividerItemDecoration(AddStudentActivity.this, DividerItemDecoration.VERTICAL));

        enableTansportation(false);

        chkTransportation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    enableTansportation(true);
                } else {
                    enableTansportation(false);
                }
            }
        });


        etAddress.setAdapter(new PlaceAPIAdapter(getApplicationContext(), R.layout.autocomplete_list_item));
        etAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String address = (String) parent.getItemAtPosition(position);
                try {
                    getLocationFromAddress(imgAddress, address);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        etEcAddress.setAdapter(new PlaceAPIAdapter(getApplicationContext(), R.layout.autocomplete_list_item));
        etEcAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String address = (String) parent.getItemAtPosition(position);
                try {
                    getLocationFromAddress(imgEcAddress, address);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        etPsiAddress.setAdapter(new PlaceAPIAdapter(getApplicationContext(), R.layout.autocomplete_list_item));
        etPsiAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String address = (String) parent.getItemAtPosition(position);
                try {
                    getLocationFromAddress(imgPsiAddress, address);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        etPciAddress.setAdapter(new PlaceAPIAdapter(getApplicationContext(), R.layout.autocomplete_list_item));
        etPciAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String address = (String) parent.getItemAtPosition(position);
                try {
                    getLocationFromAddress(imgPciAddress, address);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });




        if (getIntent().getExtras() != null) {

            bean = getIntent().getParcelableExtra(App_Constants.OBJECT);
            if (bean != null) {
                emailId = bean.getEmailId();
                isPending = bean.isPending();
                studentId = bean.getStudentId();
                btnSaNext.setText(getString(R.string.update));
                getStudentInfo();
            }

        } else {

            Glide.with(this)
                    .load(MapImageUrl(600, 150, 21.1702, 72.8311))
                    .into(imgAddress);

            Glide.with(this)
                    .load(MapImageUrl(600, 150, 21.1702, 72.8311))
                    .into(imgEcAddress);

            Glide.with(this)
                    .load(MapImageUrl(600, 150, 21.1702, 72.8311))
                    .into(imgPsiAddress);

            Glide.with(this)
                    .load(MapImageUrl(600, 150, 21.1702, 72.8311))
                    .into(imgPciAddress);

            viewAnimator.setDisplayedChild(1);
        }


      /*  etAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().length() != 0 && editable.toString().length() % 6 == 0) {

                    getLocationFromAddress(imgAddress, editable.toString());

                }
            }
        });*/

       /* etEcAddress.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().length() != 0 && editable.toString().length() % 6 == 0) {

                    getLocationFromAddress(imgEcAddress, editable.toString());

                }
            }
        });*/

       /* etPsiAddress.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().length() != 0 && editable.toString().length() % 6 == 0) {

                    getLocationFromAddress(imgPsiAddress, editable.toString());

                }
            }
        });*/

       /* etPciAddress.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().length() != 0 && editable.toString().length() % 6 == 0) {

                    getLocationFromAddress(imgPciAddress, editable.toString());

                }
            }
        });*/
    }

    private void enableTansportation(boolean isEnabled) {

        sgTransportation.setEnabled(isEnabled);
        rbAtHome.setEnabled(isEnabled);
        rbCommunityPoint.setEnabled(isEnabled);

        sgTransportation.setAlpha(isEnabled ? 1.0f : 0.5f);
    }

    private ArrayList<AddDriverModel> getData() {

        ArrayList<AddDriverModel> model = new ArrayList<>();
        model.add(new AddDriverModel(0, true));
        model.add(new AddDriverModel(1, false));

        return model;
    }

    @OnClick({R.id.card_profile_info, R.id.card_parent_info, R.id.card_emergency_contact, R.id.card_past_info_contact, R.id.card_achivements, R.id.card_physician_contact
            , R.id.btn_next, R.id.btn_pi_next, R.id.btn_pi_previous, R.id.btn_ec_next, R.id.btn_ec_previous
            , R.id.btn_psi_next, R.id.btn_psi_previous, R.id.btn_pci_next, R.id.btn_pci_previous, R.id.btn_fb_next, R.id.btn_fb_previous
            , R.id.btn_sa_next, R.id.btn_sa_previous
            , R.id.txt_profile_attachment, R.id.txt_parent_attachment, R.id.txt_past_school_attachment, R.id.txt_ec_attachment, R.id.txt_other_attachments
            , R.id.et_dob, R.id.et_date_of_joining, R.id.btn_edit
            , R.id.linaer_capture_image, R.id.linaer_capture_father_image, R.id.linaer_capture_mother_image, R.id.btn_father_pic_edit
            , R.id.btn_mother_pic_edit, R.id.et_ec_dob, R.id.card_feedback})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.card_profile_info:

                layoutPtofileInfo.setVisibility(layoutPtofileInfo.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                if (layoutEmergencyContact.getVisibility() == View.VISIBLE) {
                    layoutEmergencyContact.setVisibility(View.GONE);
                    txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutParentInfo.getVisibility() == View.VISIBLE) {
                    layoutParentInfo.setVisibility(View.GONE);
                    txtParentInfo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPastInfoContact.getVisibility() == View.VISIBLE) {
                    layoutPastInfoContact.setVisibility(View.GONE);
                    txtPastSchool.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutAchivements.getVisibility() == View.VISIBLE) {
                    layoutAchivements.setVisibility(View.GONE);
                    txtAchivements.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPhysicianContact.getVisibility() == View.VISIBLE) {
                    layoutPhysicianContact.setVisibility(View.GONE);
                    txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutCardFeedback.getVisibility() == View.VISIBLE) {
                    layoutCardFeedback.setVisibility(View.GONE);
                    textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPtofileInfo.getVisibility() == View.VISIBLE)
                    txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_up), null);
                else
                    txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);


                break;

            case R.id.card_parent_info:

                layoutParentInfo.setVisibility(layoutParentInfo.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                if (layoutPtofileInfo.getVisibility() == View.VISIBLE) {
                    layoutPtofileInfo.setVisibility(View.GONE);
                    txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutEmergencyContact.getVisibility() == View.VISIBLE) {
                    layoutEmergencyContact.setVisibility(View.GONE);
                    txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPastInfoContact.getVisibility() == View.VISIBLE) {
                    layoutPastInfoContact.setVisibility(View.GONE);
                    txtPastSchool.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutCardFeedback.getVisibility() == View.VISIBLE) {
                    layoutCardFeedback.setVisibility(View.GONE);
                    textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutAchivements.getVisibility() == View.VISIBLE) {
                    layoutAchivements.setVisibility(View.GONE);
                    txtAchivements.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPhysicianContact.getVisibility() == View.VISIBLE) {
                    layoutPhysicianContact.setVisibility(View.GONE);
                    txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutParentInfo.getVisibility() == View.VISIBLE)
                    txtParentInfo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_up), null);
                else
                    txtParentInfo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);

                break;

            case R.id.card_past_info_contact:

                layoutPastInfoContact.setVisibility(layoutPastInfoContact.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                if (layoutPtofileInfo.getVisibility() == View.VISIBLE) {
                    layoutPtofileInfo.setVisibility(View.GONE);
                    txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutParentInfo.getVisibility() == View.VISIBLE) {
                    layoutParentInfo.setVisibility(View.GONE);
                    txtParentInfo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutEmergencyContact.getVisibility() == View.VISIBLE) {
                    layoutEmergencyContact.setVisibility(View.GONE);
                    txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutAchivements.getVisibility() == View.VISIBLE) {
                    layoutAchivements.setVisibility(View.GONE);
                    txtAchivements.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPhysicianContact.getVisibility() == View.VISIBLE) {
                    layoutPhysicianContact.setVisibility(View.GONE);
                    txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutCardFeedback.getVisibility() == View.VISIBLE) {
                    layoutCardFeedback.setVisibility(View.GONE);
                    textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPastInfoContact.getVisibility() == View.VISIBLE)
                    txtPastSchool.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_up), null);
                else
                    txtPastSchool.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);


                break;

            case R.id.card_emergency_contact:

                layoutEmergencyContact.setVisibility(layoutEmergencyContact.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                if (layoutPtofileInfo.getVisibility() == View.VISIBLE) {
                    layoutPtofileInfo.setVisibility(View.GONE);
                    txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutParentInfo.getVisibility() == View.VISIBLE) {
                    layoutParentInfo.setVisibility(View.GONE);
                    txtParentInfo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPastInfoContact.getVisibility() == View.VISIBLE) {
                    layoutPastInfoContact.setVisibility(View.GONE);
                    txtPastSchool.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutAchivements.getVisibility() == View.VISIBLE) {
                    layoutAchivements.setVisibility(View.GONE);
                    txtAchivements.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPhysicianContact.getVisibility() == View.VISIBLE) {
                    layoutPhysicianContact.setVisibility(View.GONE);
                    txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutCardFeedback.getVisibility() == View.VISIBLE) {
                    layoutCardFeedback.setVisibility(View.GONE);
                    textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutEmergencyContact.getVisibility() == View.VISIBLE)
                    txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_up), null);
                else
                    txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);


                break;

            case R.id.card_physician_contact:

                layoutPhysicianContact.setVisibility(layoutPhysicianContact.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                if (layoutPtofileInfo.getVisibility() == View.VISIBLE) {
                    layoutPtofileInfo.setVisibility(View.GONE);
                    txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutParentInfo.getVisibility() == View.VISIBLE) {
                    layoutParentInfo.setVisibility(View.GONE);
                    txtParentInfo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPastInfoContact.getVisibility() == View.VISIBLE) {
                    layoutPastInfoContact.setVisibility(View.GONE);
                    txtPastSchool.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutEmergencyContact.getVisibility() == View.VISIBLE) {
                    layoutEmergencyContact.setVisibility(View.GONE);
                    txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutAchivements.getVisibility() == View.VISIBLE) {
                    layoutAchivements.setVisibility(View.GONE);
                    txtAchivements.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }
                if (layoutCardFeedback.getVisibility() == View.VISIBLE) {
                    layoutCardFeedback.setVisibility(View.GONE);
                    textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPhysicianContact.getVisibility() == View.VISIBLE)
                    txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_up), null);
                else
                    txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);

                break;

            case R.id.card_achivements:

                layoutAchivements.setVisibility(layoutAchivements.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                if (layoutPtofileInfo.getVisibility() == View.VISIBLE) {
                    layoutPtofileInfo.setVisibility(View.GONE);
                    txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutParentInfo.getVisibility() == View.VISIBLE) {
                    layoutParentInfo.setVisibility(View.GONE);
                    txtParentInfo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPhysicianContact.getVisibility() == View.VISIBLE) {
                    layoutPhysicianContact.setVisibility(View.GONE);
                    txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPastInfoContact.getVisibility() == View.VISIBLE) {
                    layoutPastInfoContact.setVisibility(View.GONE);
                    txtPastSchool.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutEmergencyContact.getVisibility() == View.VISIBLE) {
                    layoutEmergencyContact.setVisibility(View.GONE);
                    txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutCardFeedback.getVisibility() == View.VISIBLE) {
                    layoutCardFeedback.setVisibility(View.GONE);
                    textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutAchivements.getVisibility() == View.VISIBLE)
                    txtAchivements.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_up), null);
                else
                    txtAchivements.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);

                break;

            case R.id.card_feedback:

                layoutCardFeedback.setVisibility(layoutCardFeedback.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                if (layoutPtofileInfo.getVisibility() == View.VISIBLE) {
                    layoutPtofileInfo.setVisibility(View.GONE);
                    txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutParentInfo.getVisibility() == View.VISIBLE) {
                    layoutParentInfo.setVisibility(View.GONE);
                    txtParentInfo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPhysicianContact.getVisibility() == View.VISIBLE) {
                    layoutPhysicianContact.setVisibility(View.GONE);
                    txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPastInfoContact.getVisibility() == View.VISIBLE) {
                    layoutPastInfoContact.setVisibility(View.GONE);
                    txtPastSchool.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutEmergencyContact.getVisibility() == View.VISIBLE) {
                    layoutEmergencyContact.setVisibility(View.GONE);
                    txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutAchivements.getVisibility() == View.VISIBLE) {
                    layoutAchivements.setVisibility(View.GONE);
                    txtAchivements.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutCardFeedback.getVisibility() == View.VISIBLE)
                    textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_up), null);
                else
                    textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);


                break;

            case R.id.btn_next:

                if (studentPicURL == null) {
                    Toast.makeText(this, getString(R.string.please_upload_profile_pic), Toast.LENGTH_SHORT).show();
                } else if (etFirstName.getText().toString().isEmpty()) {
                    etFirstName.setError(getString(R.string.field_required));
                } else if (etLastName.getText().toString().isEmpty()) {
                    etLastName.setError(getString(R.string.field_required));
                } else if (etDob.getText().toString().isEmpty()) {
                    Toast.makeText(this, getString(R.string.please_select_dob), Toast.LENGTH_SHORT).show();
                } else if (etAddress.getText().toString().isEmpty()) {
                    etAddress.setError(getString(R.string.field_required));
                } else {
                    setUpViews(false, true, false, false,
                            false, false, false);
                }
                break;

            case R.id.btn_pi_next:

                setUpViews(false, false, false, true
                        , false, false, false);

                break;


            case R.id.btn_pi_previous:

                setUpViews(true, false, false, false
                        , false, false, false);

                break;

            case R.id.btn_ec_next:

                setUpViews(false, false, true, false
                        , false, false, false);

                break;

            case R.id.btn_ec_previous:

                setUpViews(false, true, false, false
                        , false, false, false);

                break;


            case R.id.btn_psi_next:

                setUpViews(false, false, false, false
                        , false, true, false);

                break;

            case R.id.btn_psi_previous:

                setUpViews(false, false, false, true
                        , false, false, false);

                break;

            case R.id.btn_sa_next:

                validateData();

                break;

            case R.id.btn_sa_previous:

                setUpViews(false, false, false, false
                        , false, false, true);

                break;

            case R.id.btn_pci_next:

                setUpViews(false, false, false, false
                        , false, false, true);

                break;

            case R.id.btn_pci_previous:

                setUpViews(false, false, true, false
                        , false, false, false);

                break;


            case R.id.btn_fb_next:

                setUpViews(false, false, false, false
                        , true, false, false);

                break;


            case R.id.btn_fb_previous:

                setUpViews(false, false, false, false
                        , false, true, false);

                break;


            case R.id.linaer_capture_image:

                selectPhoto(App_Constants.PROFILE_IMAGE);

                break;

            case R.id.linaer_capture_father_image:

                selectPhoto(App_Constants.FATHER_PROFILE_IMAGE);

                break;

            case R.id.linaer_capture_mother_image:

                selectPhoto(App_Constants.MOTHER_PROFILE_IMAGE);

                break;

            case R.id.txt_profile_attachment:

                selectDocument(App_Constants.PROFILE_ATTACHMENT);

                break;

            case R.id.txt_parent_attachment:

                selectDocument(App_Constants.PARENT_ATTACHMENT);

                break;

            case R.id.txt_past_school_attachment:

                selectDocument(App_Constants.PAST_SCHOOL_ATTACHMENT);

                break;

            case R.id.txt_ec_attachment:

                selectDocument(App_Constants.EMERGENCY_ATTACHMENT);

                break;

            case R.id.txt_other_attachments:

                selectDocument(App_Constants.OTHER_ATTACHMENT);

                break;

            case R.id.et_dob:

                setDate(etDob);

                break;

            case R.id.et_ec_dob:

                setDate(etEcDob);

                break;


            case R.id.et_date_of_joining:

                setDate(etJoiningDate);

                break;

            case R.id.btn_edit:

                selectPhoto(App_Constants.PROFILE_IMAGE);

                break;

            case R.id.btn_father_pic_edit:

                selectPhoto(App_Constants.FATHER_PROFILE_IMAGE);

                break;

            case R.id.btn_mother_pic_edit:

                selectPhoto(App_Constants.MOTHER_PROFILE_IMAGE);

                break;


        }
    }

    private void selectPhoto(String profileType) {

        this.profileType = profileType;

        final Dialog dialog = new Dialog(AddStudentActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_image_picker_dialog);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setLayout(getScreenWidth(AddStudentActivity.this) / 2, LinearLayoutCompat.LayoutParams.WRAP_CONTENT);
        }
        dialog.setCancelable(true);
        dialog.show();

        Button gallery = (Button) dialog.findViewById(R.id.bGallery);
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                galleryIntent();

            }
        });

        Button camera = (Button) dialog.findViewById(R.id.bCamera);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                cameraIntent();

            }
        });

    }

    public int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    }

    public void cameraIntent() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 0);
    }

    private void galleryIntent() {

        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, 1);
    }


    private void validateData() {

        if (!isValidProfileData()) {

            setUpViews(true, false, false, false, false, false, false);

        } else {

            if (getIntent().getExtras() != null) {

                if (isOldStudent)
                    registerStudent();
                else
                    updateStudent();
            } else {
                registerStudent();
            }

        }
    }

    private boolean isValidProfileData() {


        if (studentPicURL == null) {
            Toast.makeText(this, getString(R.string.please_upload_profile_pic), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (etFirstName.getText().toString().isEmpty()) {
            etFirstName.setError(getString(R.string.field_required));
            return false;
        }

        if (etLastName.getText().toString().isEmpty()) {
            etLastName.setError(getString(R.string.field_required));
            return false;
        }

        if (etDob.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.please_select_dob), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (etAddress.getText().toString().isEmpty()) {
            etAddress.setError(getString(R.string.field_required));
            return false;
        }

        return true;

    }


    private void setUpViews(boolean isProfileViewVisible, boolean isParentInfoViewVisible, boolean isPasSchoolInfoViewVisible
            , boolean isEmergecyViewVisible, boolean isStaffAchivementViewVisible, boolean isPhysicianViewVisible, boolean isFeedbackViewVisible) {

        layoutPtofileInfo.setVisibility(isProfileViewVisible ? View.VISIBLE : View.GONE);
        layoutParentInfo.setVisibility(isParentInfoViewVisible ? View.VISIBLE : View.GONE);
        layoutPastInfoContact.setVisibility(isPasSchoolInfoViewVisible ? View.VISIBLE : View.GONE);
        layoutEmergencyContact.setVisibility(isEmergecyViewVisible ? View.VISIBLE : View.GONE);
        layoutAchivements.setVisibility(isStaffAchivementViewVisible ? View.VISIBLE : View.GONE);
        layoutPhysicianContact.setVisibility(isPhysicianViewVisible ? View.VISIBLE : View.GONE);
        layoutCardFeedback.setVisibility(isFeedbackViewVisible ? View.VISIBLE : View.GONE);

        if (isProfileViewVisible) {
            txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_up), null);
        } else {
            txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
        }

        if (isParentInfoViewVisible) {
            txtParentInfo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_up), null);
        } else {
            txtParentInfo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
        }

        if (isPasSchoolInfoViewVisible) {
            txtPastSchool.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_up), null);
        } else {
            txtPastSchool.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
        }

        if (isEmergecyViewVisible) {
            txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_up), null);
        } else {
            txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
        }

        if (isStaffAchivementViewVisible) {
            txtAchivements.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_up), null);
        } else {
            txtAchivements.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
        }

        if (isPhysicianViewVisible) {
            txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_up), null);
        } else {
            txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
        }

        if (isFeedbackViewVisible) {
            textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_up), null);
        } else {
            textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddStudentActivity.this, R.drawable.ic_arrow_down), null);
        }

    }


    private void selectDocument(String type) {

        documentSelectionType = type;

        final Dialog dialog = new Dialog(AddStudentActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setLayout(AdminApp.getInstance().getScreenWidth(AddStudentActivity.this) / 2, LinearLayoutCompat.LayoutParams.WRAP_CONTENT);
        }
        dialog.setCancelable(true);
        dialog.show();

        Button gallery = (Button) dialog.findViewById(R.id.bGallery);
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                Intent intent = new Intent(AddStudentActivity.this, FileSelectionActivity.class);
                startActivityForResult(intent, App_Constants.FILE_SELECT);

            }
        });

        Button camera = (Button) dialog.findViewById(R.id.bCamera);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                cameraAttachmentIntent();

            }
        });

    }

    public void cameraAttachmentIntent() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, App_Constants.CAMERA_ATTACHMENT_RESULT_CODE);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        super.onBackPressed();
    }

    public String MapImageUrl(int X, int Y, double lat, double lon) {
        String url = "https://maps.googleapis.com/maps/api/staticmap?zoom=15&size=" + X + "x" + Y + "&maptype=roadmap&markers=color:red%7Clabel:R%7C" + lat + "," + lon + "&sensor=false";
        Log.e("MAP URL ", "" + url);
        return url;
    }


    private void setDate(final EditText editText) {

        final Calendar mCalender = Calendar.getInstance();

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub

                mCalender.set(Calendar.YEAR, year);
                mCalender.set(Calendar.MONTH, monthOfYear);
                mCalender.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                editText.setText(sdf.format(mCalender.getTime()));

            }

        };

        DatePickerDialog dialog = new DatePickerDialog(this
                , R.style.datepickerCustom, date, mCalender.get(Calendar.YEAR), mCalender.get(Calendar.MONTH),
                mCalender.get(Calendar.DAY_OF_MONTH));

        dialog.show();

    }


    public boolean isValidEmailId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }


    public void getLocationFromAddress(final ImageView imageView, String strAddress) {

        Geocoder coder = new Geocoder(AddStudentActivity.this);
        List<Address> addresses;

        try {
            addresses = coder.getFromLocationName(strAddress, 5);
            if (addresses == null) {
                Toast.makeText(this, "address not found", Toast.LENGTH_SHORT).show();
                return;
            }

            if (addresses.size() == 0) {
                Toast.makeText(this, "address not found", Toast.LENGTH_SHORT).show();
                return;
            }

            Address location = addresses.get(0);
            if (location != null) {

                Glide.with(AddStudentActivity.this)
                        .load(MapImageUrl(600, 150, location.getLatitude(), location.getLongitude()))
                        .into(imageView);
            }
        } catch (IOException ex) {
            ex.printStackTrace();

            Glide.with(AddStudentActivity.this)
                    .load(MapImageUrl(600, 150, 82.8628, 135.0000))
                    .into(imageView);
        }
    }



    public LatLng getLatLng(String strAddress) {

        Geocoder coder = new Geocoder(AddStudentActivity.this);
        List<Address> addresses;

        try {
            addresses = coder.getFromLocationName(strAddress, 5);
            if (addresses == null) {
                return new LatLng(0.0, 0.0);
            }

            if (addresses.size() == 0) {
                return new LatLng(0.0, 0.0);
            }

            Address location = addresses.get(0);
            if (location != null) {
                return new LatLng(location.getLatitude(), location.getLongitude());
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            return new LatLng(0.0, 0.0);
        }

        return new LatLng(0.0, 0.0);
    }

    public void registerStudent() {

        if (sgGender.getCheckedRadioButtonId() == R.id.rb_male)
            selectedGender = App_Constants.MALE;
        else
            selectedGender = App_Constants.FEMALE;

        AddStudentParams params = new AddStudentParams();

        params.setAcademicInfo(false);
        params.setProfilePic(studentPicURL);
        params.setFirstName(etFirstName.getText().toString());
        params.setLastName(etLastName.getText().toString());
        params.setDob(DateFunction.ConvertDate(etDob.getText().toString(), "dd/MM/yyyy", "yyyy-MM-dd"));
        params.setGender(selectedGender);
        LatLng latLng = getLatLng(etAddress.getText().toString());
        params.setGeopoint(latLng.latitude + "," + latLng.longitude);
        params.setDateofJoining(DateFunction.ConvertDate(etJoiningDate.getText().toString(), "dd/MM/yyyy", "yyyy-MM-dd"));
        params.setBirthPlace(etBirthPalace.getText().toString());
        params.setNationality(etNationality.getText().toString());
        params.setMotherTounge(etMotherTounge.getText().toString());
        params.setReligion(etReligion.getText().toString());
        params.setAddress(etAddress.getText().toString());
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());

        if (isOldStudent)
            params.setStudentId(etStudentId.getText().toString());

        if (chkTransportation.isChecked()) {
            params.setRequiresTransportation("true");
            if (sgTransportation.getCheckedRadioButtonId() == R.id.rb_at_home)
                params.setPickPoint(rbAtHome.getText().toString());
            else
                params.setPickPoint(rbCommunityPoint.getText().toString());
        } else {
            params.setRequiresTransportation("false");
        }

        List<AddStudentParams.FilesBeanXXX> finalProfileFiles = new ArrayList<>();
        if (profileFiles != null && profileFiles.size() > 0) {
            for (MultipleImageUpload.ResultBean bean : profileFiles) {
                AddStudentParams.FilesBeanXXX model = new AddStudentParams.FilesBeanXXX();
                model.setFileName(bean.getFileName());
                model.setFilePath(bean.getFilepath());
                finalProfileFiles.add(model);
            }
        }
        params.setFiles(finalProfileFiles);

        AddStudentParams.ParentDetailsBean parentBeans = new AddStudentParams.ParentDetailsBean();
        parentBeans.setFatherProfilePic(fatherPicURL);
        parentBeans.setFatherFirstName(etPiFatherFirstName.getText().toString());
        parentBeans.setFatherLastName(etPiFatherLastName.getText().toString());
        parentBeans.setFatherEmailId(etPiFatherEmailAddress.getText().toString());
        if (!etPiFatherPhone.getText().toString().isEmpty())
            parentBeans.setFatherPhoneNo("(+" + countryCodePiFather.getSelectedCountryCode() + ")" + etPiFatherPhone.getText().toString());
        else
            parentBeans.setFatherPhoneNo(etPiFatherPhone.getText().toString());
        parentBeans.setFatherOccupation(etPiFatherOccuption.getText().toString());

        parentBeans.setMotherProfilePic(motherPicURL);
        parentBeans.setMotherFirstName(etPiMotherFirstName.getText().toString());
        parentBeans.setMotherLastName(etPiMotherLastName.getText().toString());
        parentBeans.setMotherEmailId(etPiMotherEmailAddress.getText().toString());
        if (!etPiMotherPhone.getText().toString().isEmpty())
            parentBeans.setMotherPhoneNo("(+" + countryCodePiMother.getSelectedCountryCode() + ")" + etPiMotherPhone.getText().toString());
        else
            parentBeans.setMotherPhoneNo(etPiMotherPhone.getText().toString());
        parentBeans.setMotherOccupation(etPiMotherOccuption.getText().toString());

        List<AddStudentParams.ParentDetailsBean.FilesBeanX> finalqualFiles = new ArrayList<>();
        if (parentFiles != null && parentFiles.size() > 0) {
            for (MultipleImageUpload.ResultBean bean : parentFiles) {
                AddStudentParams.ParentDetailsBean.FilesBeanX model = new AddStudentParams.ParentDetailsBean.FilesBeanX();
                model.setFileName(bean.getFileName());
                model.setFilePath(bean.getFilepath());
                finalqualFiles.add(model);
            }
        }
        parentBeans.setFiles(finalqualFiles);

        params.setParentDetails(parentBeans);

        AddStudentParams.GuardianDetailsBean emerBeans = new AddStudentParams.GuardianDetailsBean();
        emerBeans.setGuardianFirstName(etEcFirstName.getText().toString());
        emerBeans.setGuardianLastName(etEcLastName.getText().toString());
        emerBeans.setRelation(etEcRelation.getText().toString());
        emerBeans.setDob(DateFunction.ConvertDate(etEcDob.getText().toString(), "dd/MM/yyyy", "yyyy-MM-dd"));
        emerBeans.setEducation(etEcEducation.getText().toString());
        emerBeans.setOccupation(etEcOccupation.getText().toString());
        if (!etEcPhone.getText().toString().isEmpty())
            emerBeans.setPhoneNo("(+" + countryCodeEc.getSelectedCountryCode() + ")" + etEcPhone.getText().toString());
        else
            emerBeans.setPhoneNo(etEcPhone.getText().toString());
        emerBeans.setEmailId(etEcEmail.getText().toString());
        emerBeans.setAddress(etEcAddress.getText().toString());
        List<AddStudentParams.GuardianDetailsBean.FilesBean> finalecFiles = new ArrayList<>();
        if (emergencyFiles != null && emergencyFiles.size() > 0) {
            for (MultipleImageUpload.ResultBean bean : emergencyFiles) {
                AddStudentParams.GuardianDetailsBean.FilesBean model = new AddStudentParams.GuardianDetailsBean.FilesBean();
                model.setFileName(bean.getFileName());
                model.setFilePath(bean.getFilepath());
                finalecFiles.add(model);
            }
        }
        emerBeans.setFiles(finalecFiles);
        params.setGuardianDetails(emerBeans);

        AddStudentParams.PastSchoolDetailsBean pastSchoolBeans = new AddStudentParams.PastSchoolDetailsBean();
        pastSchoolBeans.setSchoolName(etPsiSchoolName.getText().toString());
        pastSchoolBeans.setYearOfPassing(etPsiPassingYear.getText().toString());
        pastSchoolBeans.setGradeId(etPsiGrade.getText().toString());
        pastSchoolBeans.setClassId(etPsiClass.getText().toString());
        pastSchoolBeans.setAddress(etPsiAddress.getText().toString());

        List<AddStudentParams.PastSchoolDetailsBean.FilesBeanXX> finalexpFiles = new ArrayList<>();
        if (pastSchoolFiles != null && pastSchoolFiles.size() > 0) {
            for (MultipleImageUpload.ResultBean bean : pastSchoolFiles) {
                AddStudentParams.PastSchoolDetailsBean.FilesBeanXX model = new AddStudentParams.PastSchoolDetailsBean.FilesBeanXX();
                model.setFileName(bean.getFileName());
                model.setFilePath(bean.getFilepath());
                finalexpFiles.add(model);
            }
        }
        pastSchoolBeans.setFiles(finalexpFiles);

        params.setPastSchoolDetails(pastSchoolBeans);

        AddStudentParams.PhysicianDetailsBean physicianDetailsBean = new AddStudentParams.PhysicianDetailsBean();
        physicianDetailsBean.setFirstName(etPciFirstName.getText().toString());
        physicianDetailsBean.setLastName(etPciLastName.getText().toString());
        if (!etPciPhoneNumber.getText().toString().isEmpty())
            physicianDetailsBean.setPhone("(+" + countryCodePci.getSelectedCountryCode() + ")" + etPciPhoneNumber.getText().toString());
        else
            physicianDetailsBean.setPhone(etPciPhoneNumber.getText().toString());
        physicianDetailsBean.setFax(etPciFaxNumber.getText().toString());
        physicianDetailsBean.setAddress(etPciAddress.getText().toString());
        params.setPhysicianDetails(physicianDetailsBean);

        AddStudentParams.NotesBean notesBean = new AddStudentParams.NotesBean();
        notesBean.setSchoolCounselor(etFbSchoolComment.getText().toString());
        notesBean.setTransportationCounselor(etFbTrasportationComment.getText().toString());
        params.setNotes(notesBean);

        Utility.showProgress(AddStudentActivity.this, getString(R.string.processing));

        registerStudentCall = AdminApp.getInstance().getApi().registerStudent(params);
        registerStudentCall.enqueue(new Callback<AddStudentResponse>() {
            @Override
            public void onResponse(Call<AddStudentResponse> call, Response<AddStudentResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            Toast.makeText(AddStudentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            finish();
                            if (!isOldStudent) {
                                startActivity(new Intent(AddStudentActivity.this, StudentAdminProcessActivity.class)
                                        .putExtra(App_Constants.OBJECT, response.body().getResult()));
                                overridePendingTransition(R.anim.enter, R.anim.leave);
                            }
                        } else {
                            Toast.makeText(AddStudentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        APIError error = APIError.parseError(response, AddStudentActivity.this, App_Constants.API_REGISTER_STUDENT);
                        Toast.makeText(AddStudentActivity.this, error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(AddStudentActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddStudentResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Log.e("STUDNET ADD ERROR", "" + t.getMessage());
                    Toast.makeText(AddStudentActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    public void updateStudent() {

        if (sgGender.getCheckedRadioButtonId() == R.id.rb_male)
            selectedGender = App_Constants.MALE;
        else
            selectedGender = App_Constants.FEMALE;

        AddStudentParams params = new AddStudentParams();

        if (isPending) {
            params.setId(id);
        } else {
            params.setId(id);
            params.setAcademicInfo(true);
        }

        if (studentPicURL != null)
            params.setProfilePic(studentPicURL);
        params.setFirstName(etFirstName.getText().toString());
        params.setLastName(etLastName.getText().toString());
        params.setDob(DateFunction.ConvertDate(etDob.getText().toString(), "dd/MM/yyyy", "yyyy-MM-dd"));
        params.setGender(selectedGender);
        LatLng latLng = getLatLng(etAddress.getText().toString());
        params.setGeopoint(latLng.latitude + "," + latLng.longitude);
        params.setDateofJoining(DateFunction.ConvertDate(etJoiningDate.getText().toString(), "dd/MM/yyyy", "yyyy-MM-dd"));
        params.setBirthPlace(etBirthPalace.getText().toString());
        params.setNationality(etNationality.getText().toString());
        params.setMotherTounge(etMotherTounge.getText().toString());
        params.setReligion(etReligion.getText().toString());
        params.setAddress(etAddress.getText().toString());
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());

        List<AddStudentParams.FilesBeanXXX> finalProfileFiles = new ArrayList<>();
        if (profileFiles != null && profileFiles.size() > 0) {
            for (MultipleImageUpload.ResultBean bean : profileFiles) {
                AddStudentParams.FilesBeanXXX model = new AddStudentParams.FilesBeanXXX();
                model.setFileName(bean.getFileName());
                model.setFilePath(bean.getFilepath());
                finalProfileFiles.add(model);
            }
        }
        params.setFiles(finalProfileFiles);

        if (chkTransportation.isChecked()) {
            params.setRequiresTransportation("true");
            if (sgTransportation.getCheckedRadioButtonId() == R.id.rb_at_home)
                params.setPickPoint(rbAtHome.getText().toString());
            else
                params.setPickPoint(rbCommunityPoint.getText().toString());
        } else {
            params.setRequiresTransportation("false");
        }


        AddStudentParams.ParentDetailsBean parentBeans = new AddStudentParams.ParentDetailsBean();
        if (fatherPicURL != null)
            parentBeans.setFatherProfilePic(fatherPicURL);
        parentBeans.setFatherFirstName(etPiFatherFirstName.getText().toString());
        parentBeans.setFatherLastName(etPiFatherLastName.getText().toString());
        parentBeans.setFatherEmailId(etPiFatherEmailAddress.getText().toString());
        if (!etPiFatherPhone.getText().toString().isEmpty())
            parentBeans.setFatherPhoneNo("(+" + countryCodePiFather.getSelectedCountryCode() + ")" + etPiFatherPhone.getText().toString());
        else
            parentBeans.setFatherPhoneNo(etPiFatherPhone.getText().toString());
        parentBeans.setFatherOccupation(etPiFatherOccuption.getText().toString());

        if (motherPicURL != null)
            parentBeans.setMotherProfilePic(motherPicURL);
        parentBeans.setMotherFirstName(etPiMotherFirstName.getText().toString());
        parentBeans.setMotherLastName(etPiMotherLastName.getText().toString());
        parentBeans.setMotherEmailId(etPiMotherEmailAddress.getText().toString());
        if (!etPiMotherPhone.getText().toString().isEmpty())
            parentBeans.setMotherPhoneNo("(+" + countryCodePiMother.getSelectedCountryCode() + ")" + etPiMotherPhone.getText().toString());
        else
            parentBeans.setMotherPhoneNo(etPiMotherPhone.getText().toString());
        parentBeans.setMotherOccupation(etPiMotherOccuption.getText().toString());

        List<AddStudentParams.ParentDetailsBean.FilesBeanX> finalqualFiles = new ArrayList<>();
        if (parentFiles != null && parentFiles.size() > 0) {
            for (MultipleImageUpload.ResultBean bean : parentFiles) {
                AddStudentParams.ParentDetailsBean.FilesBeanX model = new AddStudentParams.ParentDetailsBean.FilesBeanX();
                model.setFileName(bean.getFileName());
                model.setFilePath(bean.getFilepath());
                finalqualFiles.add(model);
            }
        }
        parentBeans.setFiles(finalqualFiles);

        params.setParentDetails(parentBeans);

        AddStudentParams.GuardianDetailsBean emerBeans = new AddStudentParams.GuardianDetailsBean();
        emerBeans.setGuardianFirstName(etEcFirstName.getText().toString());
        emerBeans.setGuardianLastName(etEcLastName.getText().toString());
        emerBeans.setRelation(etEcRelation.getText().toString());
        emerBeans.setDob(DateFunction.ConvertDate(etEcDob.getText().toString(), "dd/MM/yyyy", "yyyy-MM-dd"));
        emerBeans.setEducation(etEcEducation.getText().toString());
        emerBeans.setOccupation(etEcOccupation.getText().toString());
        if (!etEcPhone.getText().toString().isEmpty())
            emerBeans.setPhoneNo("(+" + countryCodeEc.getSelectedCountryCode() + ")" + etEcPhone.getText().toString());
        else
            emerBeans.setPhoneNo(etEcPhone.getText().toString());
        emerBeans.setEmailId(etEcEmail.getText().toString());
        emerBeans.setAddress(etEcAddress.getText().toString());

        List<AddStudentParams.GuardianDetailsBean.FilesBean> finalecFiles = new ArrayList<>();
        if (emergencyFiles != null && emergencyFiles.size() > 0) {
            for (MultipleImageUpload.ResultBean bean : emergencyFiles) {
                AddStudentParams.GuardianDetailsBean.FilesBean model = new AddStudentParams.GuardianDetailsBean.FilesBean();
                model.setFileName(bean.getFileName());
                model.setFilePath(bean.getFilepath());
                finalecFiles.add(model);
            }
        }
        emerBeans.setFiles(finalecFiles);

        params.setGuardianDetails(emerBeans);

        AddStudentParams.PastSchoolDetailsBean pastSchoolBeans = new AddStudentParams.PastSchoolDetailsBean();
        pastSchoolBeans.setSchoolName(etPsiSchoolName.getText().toString());
        pastSchoolBeans.setYearOfPassing(etPsiPassingYear.getText().toString());
        pastSchoolBeans.setGradeId(etPsiGrade.getText().toString());
        pastSchoolBeans.setClassId(etPsiClass.getText().toString());
        pastSchoolBeans.setAddress(etPsiAddress.getText().toString());

        List<AddStudentParams.PastSchoolDetailsBean.FilesBeanXX> finalexpFiles = new ArrayList<>();
        if (pastSchoolFiles != null && pastSchoolFiles.size() > 0) {
            for (MultipleImageUpload.ResultBean bean : pastSchoolFiles) {
                AddStudentParams.PastSchoolDetailsBean.FilesBeanXX model = new AddStudentParams.PastSchoolDetailsBean.FilesBeanXX();
                model.setFileName(bean.getFileName());
                model.setFilePath(bean.getFilepath());
                finalexpFiles.add(model);
            }
        }
        pastSchoolBeans.setFiles(finalexpFiles);

        params.setPastSchoolDetails(pastSchoolBeans);

        AddStudentParams.PhysicianDetailsBean physicianDetailsBean = new AddStudentParams.PhysicianDetailsBean();
        physicianDetailsBean.setFirstName(etPciFirstName.getText().toString());
        physicianDetailsBean.setLastName(etPciLastName.getText().toString());
        if (!etPciPhoneNumber.getText().toString().isEmpty())
            physicianDetailsBean.setPhone("(+" + countryCodePci.getSelectedCountryCode() + ")" + etPciPhoneNumber.getText().toString());
        else
            physicianDetailsBean.setPhone(etPciPhoneNumber.getText().toString());
        physicianDetailsBean.setFax(etPciFaxNumber.getText().toString());
        physicianDetailsBean.setAddress(etPciAddress.getText().toString());
        params.setPhysicianDetails(physicianDetailsBean);

        AddStudentParams.NotesBean notesBean = new AddStudentParams.NotesBean();
        notesBean.setSchoolCounselor(etFbSchoolComment.getText().toString());
        notesBean.setTransportationCounselor(etFbTrasportationComment.getText().toString());
        params.setNotes(notesBean);

        Utility.showProgress(AddStudentActivity.this, getString(R.string.processing));

        updateStudentCall = AdminApp.getInstance().getApi().updateStudent(params);
        updateStudentCall.enqueue(new Callback<AddStudentResponse>() {
            @Override
            public void onResponse(Call<AddStudentResponse> call, Response<AddStudentResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            Toast.makeText(AddStudentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            setBackIntent();
                        } else {
                            Toast.makeText(AddStudentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        APIError error = APIError.parseError(response, AddStudentActivity.this, App_Constants.API_UPDATE_STUDENT);
                        Toast.makeText(AddStudentActivity.this, error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(AddStudentActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddStudentResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(AddStudentActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void getStudentInfo() {

        viewAnimator.setDisplayedChild(0);

        getStudentInfoCall = AdminApp.getInstance().getApi().getStudentsInfo(AdminApp.getInstance().getAdmin().getSchoolId(), studentId);
        getStudentInfoCall.enqueue(new Callback<GetStudentProfileResponse>() {
            @Override
            public void onResponse(Call<GetStudentProfileResponse> call, Response<GetStudentProfileResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            setValues(response.body());
                            getQRCode();

                        } else {

                            if (bean.getProfilePic() != null) {

                                studentPicURL = bean.getProfilePic();

                                linaerCaptureImage.setVisibility(View.GONE);

                                Glide.with(AddStudentActivity.this)
                                        .load(AppApi.BASE_URL + bean.getProfilePic())
                                        .into(studentProfilePic);
                            }

                            if (bean.getStudentId() != null) {
                                etStudentId.setText(bean.getStudentId());
                                etStudentId.setAlpha(0.5f);
                                etStudentId.setEnabled(false);
                            }
                            if (bean.getFirstName() != null)
                                etFirstName.setText(bean.getFirstName());
                            if (bean.getLastName() != null)
                                etLastName.setText(bean.getLastName());
                            if (bean.getDob() != null && !bean.getDob().isEmpty())
                                etDob.setText(DateFunction.ConvertDate(bean.getDob(), "yyyy-MM-dd", "dd/MM/yyyy"));
                            if (bean.getDateofJoining() != null && !bean.getDateofJoining().isEmpty())
                                etJoiningDate.setText(DateFunction.ConvertDate(bean.getDateofJoining(), "yyyy-MM-dd", "dd/MM/yyyy"));
                            if (bean.getAddress() != null)
                                etAddress.setText(bean.getAddress());
                            if (bean.getBirthPlace() != null)
                                etBirthPalace.setText(bean.getBirthPlace());
                            if (bean.getNationality() != null)
                                etNationality.setText(bean.getNationality());
                            if (bean.getMotherTounge() != null)
                                etMotherTounge.setText(bean.getMotherTounge());
                            if (bean.getReligion() != null)
                                etReligion.setText(bean.getReligion());


                            if (bean.getGender() != null) {
                                if (bean.getGender().equals(App_Constants.MALE)) {
                                    rbMale.setChecked(true);
                                } else {
                                    rbFemale.setChecked(true);
                                }
                            }

                            if (bean.getAddress() != null && !bean.getAddress().isEmpty()) {
                                getLocationFromAddress(imgAddress, bean.getAddress());
                            } else {
                                Glide.with(AddStudentActivity.this)
                                        .load(MapImageUrl(600, 150, 21.1702, 72.8311))
                                        .into(imgAddress);
                            }

                            isOldStudent = true;
                            getQRCode();
                        }
                    } else {

                        APIError error = APIError.parseError(response, AddStudentActivity.this, App_Constants.API_GET_STUDENT_INFO);
                        viewAnimator.setDisplayedChild(2);
                        txtError.setText(error.message());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    viewAnimator.setDisplayedChild(2);
                    txtError.setText(getString(R.string.somethingwrong));
                }
            }

            @Override
            public void onFailure(Call<GetStudentProfileResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    viewAnimator.setDisplayedChild(2);
                    txtError.setText(App_Constants.NO_INTERNET);
                }
            }
        });

    }

    private void getQRCode() {

        getQRCall = AdminApp.getInstance().getApi().getQrCode(AdminApp.getInstance().getAdmin().getSchoolId(), studentId, "student");
        getQRCall.enqueue(new Callback<GetQrCodeResponse>() {
            @Override
            public void onResponse(Call<GetQrCodeResponse> call, Response<GetQrCodeResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            if (response.body().getResult() != null && response.body().getResult().getQrcode() != null) {

                                qrFrame.setVisibility(View.VISIBLE);
                                txtQrNotAvail.setVisibility(View.GONE);
                                Glide.with(AddStudentActivity.this)
                                        .load(AppApi.BASE_URL + response.body().getResult().getQrcode())
                                        .into(imgQrcode);
                                viewAnimator.setDisplayedChild(1);

                            } else {

                                qrFrame.setVisibility(View.VISIBLE);
                                txtQrNotAvail.setVisibility(View.VISIBLE);
                                viewAnimator.setDisplayedChild(1);
                            }
                        } else {

                            qrFrame.setVisibility(View.VISIBLE);
                            txtQrNotAvail.setVisibility(View.VISIBLE);
                            viewAnimator.setDisplayedChild(1);
                        }
                    } else {

                        APIError error = APIError.parseError(response, AddStudentActivity.this, App_Constants.API_GET_QR);
                        viewAnimator.setDisplayedChild(2);
                        txtError.setText(error.message());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    viewAnimator.setDisplayedChild(2);
                    txtError.setText(getString(R.string.somethingwrong));
                }
            }

            @Override
            public void onFailure(Call<GetQrCodeResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    viewAnimator.setDisplayedChild(2);
                    txtError.setText(App_Constants.NO_INTERNET);
                }
            }
        });
    }

    private void setValues(GetStudentProfileResponse body) {

        GetStudentProfileResponse.ResultBean bean = body.getResult();

        if (bean != null) {

            id = bean.getId();

            if (bean.getProfilePic() != null) {

                studentPicURL = bean.getProfilePic();

                linaerCaptureImage.setVisibility(View.GONE);

                Glide.with(this)
                        .load(AppApi.BASE_URL + bean.getProfilePic())
                        .into(studentProfilePic);
            }

            if (bean.getStudentId() != null) {
                etStudentId.setText(bean.getStudentId());
                etStudentId.setAlpha(0.5f);
                etStudentId.setEnabled(false);
            }
            if (bean.getFirstName() != null)
                etFirstName.setText(bean.getFirstName());
            if (bean.getLastName() != null)
                etLastName.setText(bean.getLastName());
            if (bean.getDob() != null && !bean.getDob().isEmpty())
                etDob.setText(DateFunction.ConvertDate(bean.getDob(), "yyyy-MM-dd", "dd/MM/yyyy"));
            if (bean.getDateofJoining() != null && !bean.getDateofJoining().isEmpty())
                etJoiningDate.setText(DateFunction.ConvertDate(bean.getDateofJoining(), "yyyy-MM-dd", "dd/MM/yyyy"));
            if (bean.getAddress() != null)
                etAddress.setText(bean.getAddress());
            if (bean.getBirthPlace() != null)
                etBirthPalace.setText(bean.getBirthPlace());
            if (bean.getNationality() != null)
                etNationality.setText(bean.getNationality());
            if (bean.getMotherTounge() != null)
                etMotherTounge.setText(bean.getMotherTounge());
            if (bean.getReligion() != null)
                etReligion.setText(bean.getReligion());

            if (bean.getRequiresTransportation() != null && !bean.getRequiresTransportation().isEmpty()) {

                chkTransportation.setChecked(true);

                if (bean.getRequiresTransportation().equals(rbAtHome.getText().toString()))
                    rbAtHome.setChecked(true);
                else
                    rbCommunityPoint.setChecked(true);
            }

            if (bean.getGender() != null) {
                if (bean.getGender().equals(App_Constants.MALE)) {
                    rbMale.setChecked(true);
                } else {
                    rbFemale.setChecked(true);
                }
            }

            if (bean.getAddress() != null && !bean.getAddress().isEmpty()) {
                getLocationFromAddress(imgAddress, bean.getAddress());
            } else {
                Glide.with(this)
                        .load(MapImageUrl(600, 150, 21.1702, 72.8311))
                        .into(imgAddress);
            }

            if (bean.getFiles() != null && bean.getFiles().size() > 0) {

                for (GetStudentProfileResponse.ResultBean.FilesBean profileFilesBean : bean.getFiles()) {
                    MultipleImageUpload.ResultBean profileFileBean = new MultipleImageUpload.ResultBean();
                    profileFileBean.setFileName(profileFilesBean.getFileName());
                    profileFileBean.setFilepath(profileFilesBean.getFilePath());
                    profileFiles.add(profileFileBean);
                }

                attachmentAdapter = new AttachmentAdapter(AddStudentActivity.this, App_Constants.PROFILE_ATTACHMENT);
                rvProfileAttachment.setAdapter(attachmentAdapter);
                attachmentAdapter.addItem(profileFiles);
            }


            if (bean.getParentDetails() != null) {

                GetStudentProfileResponse.ResultBean.ParentDetailsBean parentBean = bean.getParentDetails();

                if (parentBean.getFatherProfilePic() != null) {

                    fatherPicURL = parentBean.getFatherProfilePic();

                    linaerCaptureFatherImage.setVisibility(View.GONE);

                    Glide.with(this)
                            .load(AppApi.BASE_URL + parentBean.getFatherProfilePic())
                            .into(fatherProfilePic);
                }

                if (parentBean.getFatherFirstName() != null)
                    etPiFatherFirstName.setText(parentBean.getFatherFirstName());
                if (parentBean.getFatherLastName() != null)
                    etPiFatherLastName.setText(parentBean.getFatherLastName());
                if (parentBean.getFatherEmailId() != null)
                    etPiFatherEmailAddress.setText(parentBean.getFatherEmailId());
                if (parentBean.getFatherOccupation() != null)
                    etPiFatherOccuption.setText(parentBean.getFatherOccupation());
                if (parentBean.getFatherPhoneNo() != null && !parentBean.getFatherPhoneNo().isEmpty()) {
                    //(+672)7894561222
                    if (parentBean.getFatherPhoneNo().contains(")")) {
                        String cCode = parentBean.getFatherPhoneNo().substring(2, parentBean.getFatherPhoneNo().lastIndexOf(")"));
                        String phoneNo = parentBean.getFatherPhoneNo().substring(parentBean.getFatherPhoneNo().lastIndexOf(")") + 1);
                        etPiFatherPhone.setText(phoneNo);
                        countryCodePiFather.setCountryForPhoneCode(Integer.parseInt(cCode));
                    } else {
                        etPiFatherPhone.setText(parentBean.getFatherPhoneNo());
                    }
                }

                if (parentBean.getMotherProfilePic() != null) {

                    motherPicURL = parentBean.getMotherProfilePic();

                    linaerCaptureMotherImage.setVisibility(View.GONE);

                    Glide.with(this)
                            .load(AppApi.BASE_URL + parentBean.getMotherProfilePic())
                            .into(motherProfilePic);
                }

                if (parentBean.getMotherFirstName() != null)
                    etPiMotherFirstName.setText(parentBean.getMotherFirstName());
                if (parentBean.getMotherLastName() != null)
                    etPiMotherLastName.setText(parentBean.getMotherLastName());
                if (parentBean.getMotherEmailId() != null)
                    etPiMotherEmailAddress.setText(parentBean.getMotherEmailId());
                if (parentBean.getMotherPhoneNo() != null && !parentBean.getMotherPhoneNo().isEmpty()) {
                    //(+672)7894561222
                    if (parentBean.getMotherPhoneNo().contains(")")) {
                        String cCode = parentBean.getMotherPhoneNo().substring(2, parentBean.getMotherPhoneNo().lastIndexOf(")"));
                        String phoneNo = parentBean.getMotherPhoneNo().substring(parentBean.getMotherPhoneNo().lastIndexOf(")") + 1);
                        etPiMotherPhone.setText(phoneNo);
                        countryCodePiMother.setCountryForPhoneCode(Integer.parseInt(cCode));
                    } else {
                        etPiMotherPhone.setText(parentBean.getMotherPhoneNo());
                    }
                }
                if (parentBean.getMotherOccupation() != null)
                    etPiMotherOccuption.setText(parentBean.getMotherOccupation());

                if (parentBean.getFiles() != null && parentBean.getFiles().size() > 0) {

                    for (GetStudentProfileResponse.ResultBean.ParentDetailsBean.FilesBean qualFilesBean : parentBean.getFiles()) {
                        MultipleImageUpload.ResultBean qualResultBean = new MultipleImageUpload.ResultBean();
                        qualResultBean.setFileName(qualFilesBean.getFileName());
                        qualResultBean.setFilepath(qualFilesBean.getFilePath());
                        parentFiles.add(qualResultBean);
                    }

                    attachmentAdapter = new AttachmentAdapter(AddStudentActivity.this, App_Constants.PARENT_ATTACHMENT);
                    rvParentAttachment.setAdapter(attachmentAdapter);
                    attachmentAdapter.addItem(parentFiles);
                }

            }

            if (bean.getGuardianDetails() != null) {

                GetStudentProfileResponse.ResultBean.GuardianDetailsBean emergencyDeailsBean = bean.getGuardianDetails();

                if (emergencyDeailsBean.getEmailId() != null)
                    etEcEmail.setText(emergencyDeailsBean.getEmailId());
                if (emergencyDeailsBean.getDob() != null && !emergencyDeailsBean.getDob().isEmpty())
                    etEcDob.setText(DateFunction.ConvertDate(emergencyDeailsBean.getDob(), "yyyy-MM-dd", "dd/MM/yyyy"));
                if (emergencyDeailsBean.getGuardianFirstName() != null)
                    etEcFirstName.setText(emergencyDeailsBean.getGuardianFirstName());
                if (emergencyDeailsBean.getGuardianLastName() != null)
                    etEcLastName.setText(emergencyDeailsBean.getGuardianLastName());
                if (emergencyDeailsBean.getPhoneNo() != null && !emergencyDeailsBean.getPhoneNo().isEmpty()) {
                    //(+672)7894561222
                    if (emergencyDeailsBean.getPhoneNo().contains(")")) {
                        String cCode = emergencyDeailsBean.getPhoneNo().substring(2, emergencyDeailsBean.getPhoneNo().lastIndexOf(")"));
                        String phoneNo = emergencyDeailsBean.getPhoneNo().substring(emergencyDeailsBean.getPhoneNo().lastIndexOf(")") + 1);
                        etEcPhone.setText(phoneNo);
                        countryCodeEc.setCountryForPhoneCode(Integer.parseInt(cCode));
                    } else {
                        etEcPhone.setText(emergencyDeailsBean.getPhoneNo());
                    }
                }
                if (emergencyDeailsBean.getRelation() != null)
                    etEcRelation.setText(emergencyDeailsBean.getRelation());
                if (emergencyDeailsBean.getEducation() != null)
                    etEcEducation.setText(emergencyDeailsBean.getEducation());
                if (emergencyDeailsBean.getOccupation() != null)
                    etEcOccupation.setText(emergencyDeailsBean.getOccupation());

                if (emergencyDeailsBean.getAddress() != null && !bean.getAddress().isEmpty()) {
                    getLocationFromAddress(imgEcAddress, emergencyDeailsBean.getAddress());
                    etEcAddress.setText(emergencyDeailsBean.getAddress());
                } else {
                    Glide.with(this)
                            .load(MapImageUrl(600, 150, 21.1702, 72.8311)).into(imgEcAddress);
                }

                if (emergencyDeailsBean.getFiles() != null && emergencyDeailsBean.getFiles().size() > 0) {

                    for (GetStudentProfileResponse.ResultBean.GuardianDetailsBean.FilesBean ecFilesBean : emergencyDeailsBean.getFiles()) {
                        MultipleImageUpload.ResultBean ecResultBean = new MultipleImageUpload.ResultBean();
                        ecResultBean.setFileName(ecFilesBean.getFileName());
                        ecResultBean.setFilepath(ecFilesBean.getFilePath());
                        emergencyFiles.add(ecResultBean);
                    }

                    attachmentAdapter = new AttachmentAdapter(AddStudentActivity.this, App_Constants.EMERGENCY_ATTACHMENT);
                    rvEcAttachment.setAdapter(attachmentAdapter);
                    attachmentAdapter.addItem(emergencyFiles);
                }

            } else {

                Glide.with(this)
                        .load(MapImageUrl(600, 150, 21.1702, 72.8311))
                        .into(imgEcAddress);
            }

            if (bean.getPastSchoolDetails() != null) {

                GetStudentProfileResponse.ResultBean.PastSchoolDetailsBean pastDeailsBean = bean.getPastSchoolDetails();

                if (pastDeailsBean.getSchoolName() != null)
                    etPsiSchoolName.setText(pastDeailsBean.getSchoolName());
                if (pastDeailsBean.getYearOfPassing() != null)
                    etPsiPassingYear.setText(pastDeailsBean.getYearOfPassing());
                if (pastDeailsBean.getClassId() != null)
                    etPsiClass.setText(pastDeailsBean.getClassId());
                if (pastDeailsBean.getGradeId() != null)
                    etPsiGrade.setText(pastDeailsBean.getGradeId());

                if (pastDeailsBean.getAddress() != null) {
                    getLocationFromAddress(imgPsiAddress, pastDeailsBean.getAddress());
                    etPsiAddress.setText(pastDeailsBean.getAddress());
                } else {
                    Glide.with(this)
                            .load(MapImageUrl(600, 150, 21.1702, 72.8311)).into(imgPsiAddress);
                }

                if (pastDeailsBean.getFiles() != null && pastDeailsBean.getFiles().size() > 0) {

                    for (GetStudentProfileResponse.ResultBean.PastSchoolDetailsBean.FilesBean expFilesBean : pastDeailsBean.getFiles()) {
                        MultipleImageUpload.ResultBean expResultBean = new MultipleImageUpload.ResultBean();
                        expResultBean.setFileName(expFilesBean.getFileName());
                        expResultBean.setFilepath(expFilesBean.getFilePath());
                        pastSchoolFiles.add(expResultBean);
                    }

                    attachmentAdapter = new AttachmentAdapter(AddStudentActivity.this, App_Constants.PAST_SCHOOL_ATTACHMENT);
                    rvPastSchoolAttachment.setAdapter(attachmentAdapter);
                    attachmentAdapter.addItem(pastSchoolFiles);
                }

            } else {

                Glide.with(this)
                        .load(MapImageUrl(600, 150, 21.1702, 72.8311)).into(imgPsiAddress);
            }

            if (bean.getPhysicianDetails() != null) {

                GetStudentProfileResponse.ResultBean.PhysicianDetailsBean physicianDetailsBean = bean.getPhysicianDetails();

                if (physicianDetailsBean.getFirstName() != null)
                    etPciFirstName.setText(physicianDetailsBean.getFirstName());
                if (physicianDetailsBean.getLastName() != null)
                    etPciLastName.setText(physicianDetailsBean.getLastName());
                if (physicianDetailsBean.getPhone() != null && !physicianDetailsBean.getPhone().isEmpty()) {
                    //(+672)7894561222
                    if (physicianDetailsBean.getPhone().contains(")")) {
                        String cCode = physicianDetailsBean.getPhone().substring(2, physicianDetailsBean.getPhone().lastIndexOf(")"));
                        String phoneNo = physicianDetailsBean.getPhone().substring(physicianDetailsBean.getPhone().lastIndexOf(")") + 1);
                        etPciPhoneNumber.setText(phoneNo);
                        countryCodePci.setCountryForPhoneCode(Integer.parseInt(cCode));
                    } else {
                        etPciPhoneNumber.setText(physicianDetailsBean.getPhone());
                    }
                }
                if (physicianDetailsBean.getFax() != null)
                    etPciFaxNumber.setText(physicianDetailsBean.getFax());

                if (physicianDetailsBean.getAddress() != null) {
                    getLocationFromAddress(imgPciAddress, physicianDetailsBean.getAddress());
                    etPciAddress.setText(physicianDetailsBean.getAddress());
                } else {
                    Glide.with(this)
                            .load(MapImageUrl(600, 150, 21.1702, 72.8311)).into(imgPciAddress);
                }
            } else {
                Glide.with(this)
                        .load(MapImageUrl(600, 150, 21.1702, 72.8311)).into(imgPciAddress);
            }


            if (bean.getNotes() != null) {

                GetStudentProfileResponse.ResultBean.NotesBean notesBean = bean.getNotes();

                if (notesBean.getSchoolCounselor() != null)
                    etFbSchoolComment.setText(notesBean.getSchoolCounselor());
                if (notesBean.getTransportationCounselor() != null)
                    etFbTrasportationComment.setText(notesBean.getTransportationCounselor());

            }

        }
    }

    private void setBackIntent() {

        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        onBackPressed();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == APIError.UPDATE_TOKEN && resultCode == RESULT_OK) {

            if (data != null) {
                String apiName = data.getStringExtra("api");
                onRelaod(apiName);
            }

        } else if (requestCode == 0 && resultCode == RESULT_OK) {

            Bitmap imageBitmap = null;
            Bundle extras = data.getExtras();
            if (extras != null) {
                imageBitmap = (Bitmap) extras.get("data");
            }

            Uri tempUri = getImageUri(getApplicationContext(), imageBitmap);
            File finalFile = new File(getRealPathFromURI(tempUri));
            picturePath = finalFile.toString();

            if (profileType.equals(App_Constants.PROFILE_IMAGE)) {
                linaerCaptureImage.setVisibility(View.GONE);
                studentProfilePic.setImageURI(Uri.fromFile(finalFile));
            } else if (profileType.equals(App_Constants.FATHER_PROFILE_IMAGE)) {
                linaerCaptureFatherImage.setVisibility(View.GONE);
                fatherProfilePic.setImageURI(Uri.fromFile(finalFile));
            } else if (profileType.equals(App_Constants.MOTHER_PROFILE_IMAGE)) {
                linaerCaptureMotherImage.setVisibility(View.GONE);
                motherProfilePic.setImageURI(Uri.fromFile(finalFile));
            }

            System.out.println(finalFile);
            UploadPic();

        } else if (requestCode == 1 && resultCode == RESULT_OK) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver()
                    .query(selectedImage, filePathColumn, null, null,
                            null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);

            if (profileType.equals(App_Constants.PROFILE_IMAGE)) {
                linaerCaptureImage.setVisibility(View.GONE);
                studentProfilePic.setImageURI(selectedImage);
            } else if (profileType.equals(App_Constants.FATHER_PROFILE_IMAGE)) {
                linaerCaptureFatherImage.setVisibility(View.GONE);
                fatherProfilePic.setImageURI(selectedImage);
            } else if (profileType.equals(App_Constants.MOTHER_PROFILE_IMAGE)) {
                linaerCaptureMotherImage.setVisibility(View.GONE);
                motherProfilePic.setImageURI(selectedImage);
            }
            cursor.close();

            System.out.println(selectedImage);
            UploadPic();

        } else if (requestCode == App_Constants.FILE_SELECT && resultCode == RESULT_OK) {

            ArrayList<File> Files = (ArrayList<File>) data.getSerializableExtra(FileSelectionActivity.FILES_TO_UPLOAD);
            for (File file : Files) {
                String uri = file.getAbsolutePath();
                Log.e("PATH========", uri);
                if (file.toString().contains("/")) {
                    selectedFiles.add(new FileModel(file.toString().substring(file.toString().lastIndexOf("/") + 1), uri));
                }
            }

            if (selectedFiles != null && selectedFiles.size() > 0) {
                UploadDocument();
            }

        } else if (requestCode == App_Constants.CAMERA_ATTACHMENT_RESULT_CODE && resultCode == RESULT_OK) {

            Bitmap imageBitmap = null;
            Bundle extras = data.getExtras();
            if (extras != null) {
                imageBitmap = (Bitmap) extras.get("data");
            }

            Uri tempUri = getImageUri(getApplicationContext(), imageBitmap);
            File finalFile = new File(getRealPathFromURI(tempUri));
            picturePath = finalFile.toString();

            selectedFiles.add(new FileModel(picturePath.substring(picturePath.lastIndexOf("/") + 1), picturePath));

            if (selectedFiles != null && selectedFiles.size() > 0) {
                UploadDocument();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void UploadDocument() {

        Utility.showProgress(AddStudentActivity.this, getString(R.string.processing));
        List<MultipartBody.Part> parts = new ArrayList<>();

        for (FileModel ss : selectedFiles) {

            File file = new File(ss.getPath());
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part body = MultipartBody.Part.createFormData("files", file.getName(), requestFile);
            parts.add(body);
        }

        attachMentCall = AdminApp.getInstance().getApi().uploadMultipleFile(parts);
        attachMentCall.enqueue(new Callback<MultipleImageUpload>() {
            @Override
            public void onResponse(Call<MultipleImageUpload> call, Response<MultipleImageUpload> response) {

                try {
                    if (response.isSuccessful()) {
                        Utility.hideProgress();
                        if (response.body().isStatus()) {
                            if (response.body().getResult() != null) {

                                attachmentAdapter = new AttachmentAdapter(AddStudentActivity.this, documentSelectionType);

                                if (documentSelectionType.equals(App_Constants.PROFILE_ATTACHMENT)) {

                                    profileFiles.addAll(response.body().getResult());
                                    rvProfileAttachment.setAdapter(attachmentAdapter);
                                    attachmentAdapter.addItem(profileFiles);

                                } else if (documentSelectionType.equals(App_Constants.PARENT_ATTACHMENT)) {

                                    parentFiles.addAll(response.body().getResult());
                                    rvParentAttachment.setAdapter(attachmentAdapter);
                                    attachmentAdapter.addItem(parentFiles);

                                } else if (documentSelectionType.equals(App_Constants.PAST_SCHOOL_ATTACHMENT)) {

                                    pastSchoolFiles.addAll(response.body().getResult());
                                    rvPastSchoolAttachment.setAdapter(attachmentAdapter);
                                    attachmentAdapter.addItem(pastSchoolFiles);

                                } else if (documentSelectionType.equals(App_Constants.EMERGENCY_ATTACHMENT)) {

                                    emergencyFiles.addAll(response.body().getResult());
                                    rvEcAttachment.setAdapter(attachmentAdapter);
                                    attachmentAdapter.addItem(emergencyFiles);

                                } else if (documentSelectionType.equals(App_Constants.OTHER_ATTACHMENT)) {

                                    otherFiles.addAll(response.body().getResult());
                                    rvOtherAttachment.setAdapter(attachmentAdapter);
                                    attachmentAdapter.addItem(otherFiles);

                                }
                                clearLists();

                            } else {
                                Toast.makeText(AddStudentActivity.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(AddStudentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        APIError error = APIError.parseError(response, AddStudentActivity.this, App_Constants.API_MULTIPLE_FILE_UPLOAD);
                        Utility.error(AddStudentActivity.this, error.message());
                    }
                } catch (Exception e) {
                    Utility.hideProgress();
                    e.printStackTrace();
                    Toast.makeText(AddStudentActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<MultipleImageUpload> call, Throwable t) {
                Utility.hideProgress();
                Log.e("ERROR UPLOAD", "" + t.getMessage());
                Toast.makeText(AddStudentActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void clearLists() {

        selectedFiles.clear();
    }

    public void removeUploadItem(MultipleImageUpload.ResultBean file, String documentSelectionType) {

        if (documentSelectionType.equals(App_Constants.PROFILE_ATTACHMENT)) {

            if (profileFiles != null && profileFiles.size() > 0) {
                profileFiles.remove(file);
            }

        } else if (documentSelectionType.equals(App_Constants.EMERGENCY_ATTACHMENT)) {

            if (emergencyFiles != null && emergencyFiles.size() > 0) {
                emergencyFiles.remove(file);
            }

        } else if (documentSelectionType.equals(App_Constants.PARENT_ATTACHMENT)) {

            if (parentFiles != null && parentFiles.size() > 0) {
                parentFiles.remove(file);
            }

        } else if (documentSelectionType.equals(App_Constants.PAST_SCHOOL_ATTACHMENT)) {

            if (pastSchoolFiles != null && pastSchoolFiles.size() > 0) {
                pastSchoolFiles.remove(file);
            }
        }

    }

    private void UploadPic() {

        Utility.showProgress(AddStudentActivity.this, getString(R.string.processing));

        File file = new File(picturePath);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        Call<ImageUpload> call = AdminApp.getInstance().getApi().upload(body);

        call.enqueue(new Callback<ImageUpload>() {
            @Override
            public void onResponse(Call<ImageUpload> call, Response<ImageUpload> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {

                        if (response.body().isStatus()) {
                            if (response.body().getResult() != null) {
                                if (profileType.equals(App_Constants.PROFILE_IMAGE)) {
                                    studentPicURL = response.body().getResult().getFilepath();
                                    System.out.println(studentPicURL);
                                } else if (profileType.equals(App_Constants.FATHER_PROFILE_IMAGE)) {
                                    fatherPicURL = response.body().getResult().getFilepath();
                                    System.out.println(fatherPicURL);
                                } else if (profileType.equals(App_Constants.MOTHER_PROFILE_IMAGE)) {
                                    motherPicURL = response.body().getResult().getFilepath();
                                    System.out.println(motherPicURL);
                                }
                            } else {

                                Toast.makeText(AddStudentActivity.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                            }
                        } else {

                            Toast.makeText(AddStudentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        APIError error = APIError.parseError(response, AddStudentActivity.this, App_Constants.API_UPLOAD);
                        Utility.error(AddStudentActivity.this, error.message());
                    }
                } catch (Exception e) {

                    e.printStackTrace();
                    Toast.makeText(AddStudentActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ImageUpload> call, Throwable t) {
                Utility.hideProgress();
                Log.e("ERROR UPLOAD", "" + t.getMessage());
                Toast.makeText(AddStudentActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void onRelaod(String apiName) {

        if (apiName != null) {

            if (apiName.equals(App_Constants.API_REGISTER_STUDENT)) {

                registerStudent();

            } else if (apiName.equals(App_Constants.API_GET_STUDENT_INFO)) {

                getStudentInfo();

            } else if (apiName.equals(App_Constants.API_UPDATE_STUDENT)) {

                updateStudent();

            } else if (apiName.equals(App_Constants.API_UPLOAD)) {

                UploadPic();

            } else if (apiName.equals(App_Constants.API_MULTIPLE_FILE_UPLOAD)) {

                UploadDocument();

            } else if (apiName.equals(App_Constants.API_GET_QR)) {

                getQRCode();
            }
        }
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (registerStudentCall != null)
            registerStudentCall.cancel();

        if (updateStudentCall != null)
            updateStudentCall.cancel();

        if (getStudentInfoCall != null)
            getStudentInfoCall.cancel();

        if (locationCall != null)
            locationCall.cancel();

        if (attachMentCall != null)
            attachMentCall.cancel();

        if (getQRCall != null)
            getQRCall.cancel();
    }


}
