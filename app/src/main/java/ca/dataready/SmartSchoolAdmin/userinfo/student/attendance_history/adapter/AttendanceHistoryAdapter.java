/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.userinfo.student.attendance_history.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.DateFunction;
import ca.dataready.SmartSchoolAdmin.server.GetAttendanceDetails;

public class AttendanceHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<GetAttendanceDetails.ResultBean> beans;

    public AttendanceHistoryAdapter(Context context) {
        this.context = context;
        beans = new ArrayList<>();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_attendance, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        GetAttendanceDetails.ResultBean bean = beans.get(position);

        if (holder instanceof ItemViewHolder) {

            ((ItemViewHolder) holder).txtTeacherName.setText(bean.getTeacherName());
            ((ItemViewHolder) holder).txtDate.setText(DateFunction.ConvertDate(bean.getAttendanceDate(), "yyyy-MM-dd", "dd MMM yyyy"));
            ((ItemViewHolder) holder).txtSubject.setText(bean.getSubjectName() + " (" + bean.getSubjectStartTime() + " - " + bean.getSubjectEndTime() + ")");

            if (bean.isSubjectAttendance()) {
                if (bean.isIsLate()) {
                    ((ItemViewHolder) holder).txtStatus.setText(context.getString(R.string.status) + " : Late");
                } else {
                    ((ItemViewHolder) holder).txtStatus.setText(context.getString(R.string.status) + " : Present");
                }
            } else {
                ((ItemViewHolder) holder).txtStatus.setText(context.getString(R.string.status) + " : Absent");
            }

        }
    }

    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }

    public void addItem(List<GetAttendanceDetails.ResultBean> results) {
        beans.addAll(results);
        notifyDataSetChanged();
    }


    class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_teacher_name)
        TextView txtTeacherName;
        @BindView(R.id.txt_date)
        TextView txtDate;
        @BindView(R.id.txt_subject)
        TextView txtSubject;
        @BindView(R.id.txt_status)
        TextView txtStatus;
        //R.layout.raw_attendance

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
