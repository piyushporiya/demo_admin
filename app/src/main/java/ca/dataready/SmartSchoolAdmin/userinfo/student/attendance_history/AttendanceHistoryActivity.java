/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.userinfo.student.attendance_history;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewAnimator;

import com.facebook.drawee.view.SimpleDraweeView;
import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.LastItemDecoration;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.AppApi;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.GetAttendanceDetails;
import ca.dataready.SmartSchoolAdmin.server.GetStudentsList;
import ca.dataready.SmartSchoolAdmin.userinfo.student.attendance_history.adapter.AttendanceHistoryAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AttendanceHistoryActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.img_back_arrow)
    ImageView imgBackArrow;
    @BindView(R.id.profile)
    SimpleDraweeView profile;
    @BindView(R.id.txt_toolbar_title)
    TextView txtToolbarTitle;
    GetStudentsList.ResultBean bean;
    @BindView(R.id.recyclerView)
    SuperRecyclerView recyclerView;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.parent_viewAnimator)
    ViewAnimator parentViewAnimator;
    private AttendanceHistoryAdapter adapter;
    private Call<GetAttendanceDetails> apiCall;
    private List<GetAttendanceDetails.ResultBean> results;
    private GridLayoutManager gridLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_history);
        ButterKnife.bind(this);
        Init();
    }

    private void Init() {

        if (getIntent().getExtras() != null) {
            bean = getIntent().getParcelableExtra(App_Constants.OBJECT);
            if (bean != null) {
                profile.setImageURI(AppApi.BASE_URL + bean.getProfilePic());
                txtToolbarTitle.setText(bean.getFirstName() + " " + bean.getLastName());
            }
        }


        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            gridLayoutManager = new GridLayoutManager(this, 3);
        } else {
            gridLayoutManager = new GridLayoutManager(this, 2);
        }

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.addItemDecoration(new LastItemDecoration());
        recyclerView.setRefreshListener(this);

        adapter = new AttendanceHistoryAdapter(AttendanceHistoryActivity.this);
        recyclerView.setAdapter(adapter);
        TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
        emptyView.setText(getString(R.string.no_data));
        parentViewAnimator.setDisplayedChild(1);

    }


    @OnClick({R.id.img_back_arrow, R.id.txt_date})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.img_back_arrow:
                finish();
                break;

            case R.id.txt_date:

                setDate(txtDate);

                break;
        }
    }

    private void setDate(final TextView textView) {

        final Calendar mCalender = Calendar.getInstance();

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub

                mCalender.set(Calendar.YEAR, year);
                mCalender.set(Calendar.MONTH, monthOfYear);
                mCalender.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                textView.setText(sdf.format(mCalender.getTime()));
                getAttendanceDetails();

            }

        };

        DatePickerDialog dialog = new DatePickerDialog(this
                , R.style.datepickerCustom, date, mCalender.get(Calendar.YEAR), mCalender.get(Calendar.MONTH),
                mCalender.get(Calendar.DAY_OF_MONTH));

        dialog.show();

    }

    private void getAttendanceDetails() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();

        apiCall = AdminApp.getInstance().getApi().getAttendanceDetails(entity.getSchoolId(), entity.getSchoolYear(), bean.getGradeId()
                , bean.getClassId(), bean.getStudentId(), txtDate.getText().toString());
        apiCall.enqueue(new Callback<GetAttendanceDetails>() {
            @Override
            public void onResponse(Call<GetAttendanceDetails> call, Response<GetAttendanceDetails> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            results = response.body().getResult();
                            if (results != null && results.size() > 0) {

                                setData();

                            } else {

                                results = null;
                                adapter = new AttendanceHistoryAdapter(AttendanceHistoryActivity.this);
                                recyclerView.setAdapter(adapter);
                                TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                                emptyView.setText(getString(R.string.no_data));
                                parentViewAnimator.setDisplayedChild(1);
                            }

                        } else {

                            results = null;
                            adapter = new AttendanceHistoryAdapter(AttendanceHistoryActivity.this);
                            recyclerView.setAdapter(adapter);
                            TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                            emptyView.setText(getString(R.string.no_data));
                            parentViewAnimator.setDisplayedChild(1);
                        }
                    } else {

                        APIError error = APIError.parseError(response, AttendanceHistoryActivity.this, App_Constants.API_ATTENDANCE_DETAILS);
                        adapter = new AttendanceHistoryAdapter(AttendanceHistoryActivity.this);
                        recyclerView.setAdapter(adapter);
                        TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                        emptyView.setText(error.message());
                        parentViewAnimator.setDisplayedChild(1);

                    }
                } catch (Exception e) {

                    adapter = new AttendanceHistoryAdapter(AttendanceHistoryActivity.this);
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(getString(R.string.somethingwrong));
                    parentViewAnimator.setDisplayedChild(1);
                }

            }

            @Override
            public void onFailure(Call<GetAttendanceDetails> call, Throwable t) {
                if (!call.isCanceled()) {
                    parentViewAnimator.setDisplayedChild(1);
                    adapter = new AttendanceHistoryAdapter(AttendanceHistoryActivity.this);
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(App_Constants.NO_INTERNET);
                }
            }
        });
    }


    private void setData() {


        adapter = new AttendanceHistoryAdapter(this);
        recyclerView.setAdapter(adapter);

        adapter.addItem(results);
        parentViewAnimator.setDisplayedChild(1);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == APIError.UPDATE_TOKEN && resultCode == RESULT_OK) {

            if (data != null) {
                String apiName = data.getStringExtra("api");
                onReload(apiName);
            }

        }
    }

    public void onReload(String apiName) {

        if (apiName != null) {
            if (apiName.equals(App_Constants.API_ATTENDANCE_DETAILS)) {
                getAttendanceDetails();
            }
        }
    }

    @Override
    public void onRefresh() {

        getAttendanceDetails();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (apiCall != null)
            apiCall.cancel();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            gridLayoutManager = new GridLayoutManager(this, 3);
        } else {
            gridLayoutManager = new GridLayoutManager(this, 2);
        }
        recyclerView.setLayoutManager(gridLayoutManager);
    }
}
