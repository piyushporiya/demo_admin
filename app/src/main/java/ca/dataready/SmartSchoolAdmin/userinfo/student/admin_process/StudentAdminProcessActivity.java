/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.userinfo.student.admin_process;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.facebook.drawee.view.SimpleDraweeView;
import com.hbb20.CountryCodePicker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.AddStudentResponse;
import ca.dataready.SmartSchoolAdmin.server.AppApi;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.GetStudentsList;
import ca.dataready.SmartSchoolAdmin.server.GetYearListResponse;
import ca.dataready.SmartSchoolAdmin.server.RegisterStudentAdminProcessResponse;
import ca.dataready.SmartSchoolAdmin.server.StudentAdminProcessParams;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.GradeList;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.StudentListModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StudentAdminProcessActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txtAdminProcess)
    TextView txtAdminProcess;
    @BindView(R.id.card_admin_process)
    CardView cardAdminProcess;
    @BindView(R.id.iv_student_image)
    SimpleDraweeView ivStudentImage;
    @BindView(R.id.txt_student_name)
    TextView txtStudentName;
    @BindView(R.id.txt_student_id)
    TextView txtStudentId;
    @BindView(R.id.et_acedamic_year)
    EditText etAcedamicYear;
    @BindView(R.id.et_emailId)
    EditText etEmailId;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.txt_Grade)
    TextView txtGrade;
    @BindView(R.id.grade_img)
    ImageView gradeImg;
    @BindView(R.id.linear_Grade)
    RelativeLayout linearGrade;
    @BindView(R.id.txt_Class)
    TextView txtClass;
    @BindView(R.id.class_img)
    ImageView classImg;
    @BindView(R.id.linear_Class)
    RelativeLayout linearClass;
    @BindView(R.id.btn_fill_later)
    Button btnFillLater;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.layout_admin_process)
    CardView layoutAdminProcess;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.txt_error)
    TextView txtError;
    @BindView(R.id.viewAnimator)
    ViewAnimator viewAnimator;
    @BindView(R.id.country_code)
    CountryCodePicker countryCode;
    private Call<GradeList> call;
    private ArrayList<GradeList.ResultBean.GradeBean> teacherBeans;
    private ListPopupWindow listPopupWindow;
    private List<String> gradeList = new ArrayList<>();
    private List<String> classList = new ArrayList<>();
    private List<String> yearList = new ArrayList<>();
    private String gradeId, classId;
    private Call<RegisterStudentAdminProcessResponse> registerCall;
    AddStudentResponse.ResultBean bean;
    GetStudentsList.ResultBean entity;
    StudentListModel.ResultBean changeEntity;
    private Call<GetYearListResponse> yearListCall;
    private List<GetYearListResponse.ResultBean> yearBeans;
    private String year, dob, firstName, lastName, profilePic, studentID;
    private Call<RegisterStudentAdminProcessResponse> updateCall;
    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_admin_process);
        ButterKnife.bind(this);
        setHeaderInfo();
        Init();
    }

    private void setHeaderInfo() {

        setSupportActionBar(toolbar);

        if (AdminApp.getInstance().getAdmin() != null)
            getSupportActionBar().setTitle(AdminApp.getInstance().getAdmin().getSchoolName() + " " + AdminApp.getInstance().getAdmin().getSchoolYear());
        else
            getSupportActionBar().setTitle(getString(R.string.app_name));

        getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + getString(R.string.student_admin_process) + "</small>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void Init() {

        if (getIntent().getExtras() != null) {

            String type = getIntent().getStringExtra(App_Constants.TYPE);

            if (type != null && type.equals(App_Constants.UPDATE))
                entity = getIntent().getParcelableExtra(App_Constants.OBJECT);
            else if (type != null && type.equals(App_Constants.CHANGE_ASSIGNMENT))
                changeEntity = getIntent().getParcelableExtra(App_Constants.OBJECT);
            else
                bean = getIntent().getParcelableExtra(App_Constants.OBJECT);

            if (bean != null) {

                txtStudentName.setText(bean.getFirstName() + " " + bean.getLastName());
                txtStudentId.setText(getString(R.string.student_id) + " : " + bean.getStudentId());
                ivStudentImage.setImageURI(AppApi.BASE_URL + bean.getProfilePic());
                dob = bean.getDob();
                firstName = bean.getFirstName();
                lastName = bean.getLastName();
                profilePic = bean.getProfilePic();
                studentID = bean.getStudentId();

            } else if (entity != null) {

                txtStudentName.setText(entity.getFirstName() + " " + entity.getLastName());
                txtStudentId.setText(getString(R.string.student_id) + " : " + entity.getStudentId());
                ivStudentImage.setImageURI(AppApi.BASE_URL + entity.getProfilePic());

                btnFillLater.setVisibility(View.GONE);

                dob = entity.getDob();
                firstName = entity.getFirstName();
                lastName = entity.getLastName();
                profilePic = entity.getProfilePic();
                studentID = entity.getStudentId();
                classId = entity.getClassId();
                gradeId = entity.getGradeId();
                year = entity.getSchoolYear();
                id = entity.getId();

                if (!entity.isPending()) {
                    etEmailId.setText(entity.getEmailId());

                    if (entity.getPhoneNo() != null && !entity.getPhoneNo().isEmpty()) {
                        //(+672)7894561222
                        if (entity.getPhoneNo().contains(")")) {
                            String cCode = entity.getPhoneNo().substring(2, entity.getPhoneNo().lastIndexOf(")"));
                            String phoneNo = entity.getPhoneNo().substring(entity.getPhoneNo().lastIndexOf(")") + 1);
                            etPhone.setText(phoneNo);
                            countryCode.setCountryForPhoneCode(Integer.parseInt(cCode));
                        } else {
                            etPhone.setText(entity.getPhoneNo());
                        }
                    }

                    etAcedamicYear.setText(entity.getSchoolYear());
                    txtClass.setText(entity.getClassId());
                    txtGrade.setText(entity.getGradeId());
                    btnSubmit.setText(getString(R.string.update));
                } else {
                    btnSubmit.setText(getString(R.string.submit));
                }

            } else if (changeEntity != null) {

                txtStudentName.setText(changeEntity.getFirstName() + " " + changeEntity.getLastName());
                txtStudentId.setText(getString(R.string.student_id) + " : " + changeEntity.getStudentId());
                ivStudentImage.setImageURI(AppApi.BASE_URL + changeEntity.getProfilePic());

                btnFillLater.setVisibility(View.GONE);

                dob = changeEntity.getDob();
                firstName = changeEntity.getFirstName();
                lastName = changeEntity.getLastName();
                profilePic = changeEntity.getProfilePic();
                studentID = changeEntity.getStudentId();
                classId = changeEntity.getClassId();
                gradeId = changeEntity.getGradeId();
                year = changeEntity.getSchoolYear();
                id = changeEntity.getId();

                etEmailId.setText(changeEntity.getEmailId());

                if (changeEntity.getPhoneNo() != null && !changeEntity.getPhoneNo().isEmpty()) {
                    //(+672)7894561222
                    if (changeEntity.getPhoneNo().contains(")")) {
                        String cCode = changeEntity.getPhoneNo().substring(2, changeEntity.getPhoneNo().lastIndexOf(")"));
                        String phoneNo = changeEntity.getPhoneNo().substring(changeEntity.getPhoneNo().lastIndexOf(")") + 1);
                        etPhone.setText(phoneNo);
                        countryCode.setCountryForPhoneCode(Integer.parseInt(cCode));
                    } else {
                        etPhone.setText(changeEntity.getPhoneNo());
                    }
                }
                etAcedamicYear.setText(changeEntity.getSchoolYear());
                txtClass.setText(changeEntity.getClassId());
                txtGrade.setText(changeEntity.getGradeId());
                btnSubmit.setText(getString(R.string.update));

            }
        }

        ApiCall();

    }


    private void ApiCall() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();
        if (entity == null)
            return;

        call = AdminApp.getInstance().getApi().getClassSchedule(entity.getSchoolId(), entity.getSchoolYear());
        call.enqueue(new Callback<GradeList>() {
            @Override
            public void onResponse(Call<GradeList> call, Response<GradeList> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            teacherBeans = response.body().getResult().getGrade();
                        } else {
                            Utility.showSnackBar(viewAnimator, response.body().getMessage());
                        }
                    } else {

                        APIError error = APIError.parseError(response, StudentAdminProcessActivity.this, App_Constants.API_TEACHER_CLASS_SCHEDULE);
                        Utility.error(StudentAdminProcessActivity.this, error.message());
                        viewAnimator.setDisplayedChild(1);

                    }
                } catch (Exception e) {
                    Utility.showSnackBar(viewAnimator, getString(R.string.somethingwrong));
                    e.printStackTrace();
                }

                getYearList();
            }

            @Override
            public void onFailure(Call<GradeList> call, Throwable t) {
                if (!call.isCanceled()) {
                    getYearList();
                    System.out.println(t.getMessage());
                    Utility.showSnackBar(viewAnimator, App_Constants.NO_INTERNET);

                }
            }
        });
    }

    private void getYearList() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();
        if (entity == null)
            return;

        yearListCall = AdminApp.getInstance().getApi().getYearList(entity.getSchoolId());
        yearListCall.enqueue(new Callback<GetYearListResponse>() {
            @Override
            public void onResponse(Call<GetYearListResponse> call, Response<GetYearListResponse> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            yearBeans = response.body().getResult();
                        } else {
                            Utility.showSnackBar(viewAnimator, response.body().getMessage());
                        }
                    } else {

                        APIError error = APIError.parseError(response, StudentAdminProcessActivity.this, App_Constants.API_GET_YEAR_LIST);
                        Utility.error(StudentAdminProcessActivity.this, error.message());

                    }
                } catch (Exception e) {
                    Utility.showSnackBar(viewAnimator, getString(R.string.somethingwrong));
                    e.printStackTrace();
                }

                viewAnimator.setDisplayedChild(1);
            }

            @Override
            public void onFailure(Call<GetYearListResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    viewAnimator.setDisplayedChild(1);
                    System.out.println(t.getMessage());
                    Utility.showSnackBar(viewAnimator, App_Constants.NO_INTERNET);

                }
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);

    }

    @OnClick({R.id.card_admin_process, R.id.linear_Grade, R.id.linear_Class, R.id.btn_fill_later, R.id.btn_submit, R.id.et_acedamic_year})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.card_admin_process:

                layoutAdminProcess.setVisibility(layoutAdminProcess.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                if (layoutAdminProcess.getVisibility() == View.VISIBLE)
                    txtAdminProcess.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(StudentAdminProcessActivity.this, R.drawable.ic_arrow_up), null);
                else
                    txtAdminProcess.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(StudentAdminProcessActivity.this, R.drawable.ic_arrow_down), null);

                break;
            case R.id.linear_Grade:
                show_drop_down_to_select_option(linearGrade, App_Constants.GRADE);
                break;
            case R.id.linear_Class:
                show_drop_down_to_select_option(linearClass, App_Constants.CLASS);
                break;
            case R.id.btn_fill_later:
                setBackIntent();
                break;
            case R.id.btn_submit:
                validateData();
                break;
            case R.id.et_acedamic_year:
                show_year_list(etAcedamicYear);
                break;
        }
    }

    private void validateData() {

        if (year == null) {
            etAcedamicYear.setError(getString(R.string.field_required));
        } else if (etEmailId.getText().toString().isEmpty()) {
            etEmailId.setError(getString(R.string.field_required));
        } else if (!isValidEmaillId(etEmailId.getText().toString())) {
            etEmailId.setError(getString(R.string.field_required));
        } else if (etPhone.getText().toString().isEmpty()) {
            etPhone.setError(getString(R.string.field_required));
        } else if (gradeId == null) {
            txtGrade.setError(getString(R.string.field_required));
        } else if (classId == null) {
            txtClass.setError(getString(R.string.field_required));
        } else {

            if (btnSubmit.getText().toString().equals(getString(R.string.submit)))
                registerStudent();
            else
                updateStudent();
        }
    }

    private void registerStudent() {

        StudentAdminProcessParams params = new StudentAdminProcessParams();
        params.setGradeId(gradeId);
        params.setClassId(classId);
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
        params.setSchoolYear(year);
        params.setEmailId(etEmailId.getText().toString());
        if (!etPhone.getText().toString().isEmpty())
            params.setPhoneNo("(+" + countryCode.getSelectedCountryCode() + ")" + etPhone.getText().toString());
        else
            params.setPhoneNo(etPhone.getText().toString());
        params.setFirstName(firstName);
        params.setLastName(lastName);
        params.setProfilePic(profilePic);
        params.setParentId("1");
        params.setDob(dob);
        params.setStudentId(studentID);

        Utility.showProgress(StudentAdminProcessActivity.this, getString(R.string.processing));

        registerCall = AdminApp.getInstance().getApi().registerStudentAdminProcess(params);
        registerCall.enqueue(new Callback<RegisterStudentAdminProcessResponse>() {
            @Override
            public void onResponse(Call<RegisterStudentAdminProcessResponse> call, Response<RegisterStudentAdminProcessResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            Toast.makeText(StudentAdminProcessActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            setBackIntent();
                        } else {
                            Toast.makeText(StudentAdminProcessActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        APIError error = APIError.parseError(response, StudentAdminProcessActivity.this, App_Constants.API_REGISTER_STUDENT_ADMIN_PROCESS);
                        Toast.makeText(StudentAdminProcessActivity.this, error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(StudentAdminProcessActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RegisterStudentAdminProcessResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(StudentAdminProcessActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void updateStudent() {

        StudentAdminProcessParams params = new StudentAdminProcessParams();
        params.setGradeId(gradeId);
        params.setClassId(classId);
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
        params.setSchoolYear(year);
        params.setEmailId(etEmailId.getText().toString());
        if (!etPhone.getText().toString().isEmpty())
            params.setPhoneNo("(+" + countryCode.getSelectedCountryCode() + ")" + etPhone.getText().toString());
        else
            params.setPhoneNo(etPhone.getText().toString());
        params.setId(id);

        Utility.showProgress(StudentAdminProcessActivity.this, getString(R.string.processing));

        updateCall = AdminApp.getInstance().getApi().updateStudentAdminProcess(params);
        updateCall.enqueue(new Callback<RegisterStudentAdminProcessResponse>() {
            @Override
            public void onResponse(Call<RegisterStudentAdminProcessResponse> call, Response<RegisterStudentAdminProcessResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            Toast.makeText(StudentAdminProcessActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            setBackIntent();
                        } else {
                            Toast.makeText(StudentAdminProcessActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        APIError error = APIError.parseError(response, StudentAdminProcessActivity.this, App_Constants.API_UPDATE_STUDENT_ADMIN_PROCESS);
                        Toast.makeText(StudentAdminProcessActivity.this, error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(StudentAdminProcessActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RegisterStudentAdminProcessResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(StudentAdminProcessActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void setBackIntent() {

        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        onBackPressed();
    }

    private void show_drop_down_to_select_option(RelativeLayout layout, final String which) {

        if (teacherBeans != null && teacherBeans.size() > 0) {

            listPopupWindow = new ListPopupWindow(StudentAdminProcessActivity.this);
            if (which.equals(App_Constants.GRADE)) {

                gradeList.clear();
                for (GradeList.ResultBean.GradeBean beans : teacherBeans) {
                    if (!gradeList.contains(beans.getGradeId()))
                        gradeList.add(beans.getGradeId());
                }

                listPopupWindow.setAdapter(new ArrayAdapter(StudentAdminProcessActivity.this, R.layout.list_dropdown_item, gradeList));

            } else if (which.equals(App_Constants.CLASS)) {

                classList.clear();
                for (GradeList.ResultBean.GradeBean beans : teacherBeans) {

                    if (txtGrade.getText().toString().equals(beans.getGradeId())) {
                        if (!classList.contains(beans.getClassX()))
                            for (int i = 0; i < beans.getClassX().size(); i++)
                                classList.add(beans.getClassX().get(i).getClassId());
                    }
                }

                if (classList != null && classList.size() > 0) {
                    Collections.sort(classList, new Comparator<String>() {
                        public int compare(String obj1, String obj2) {
                            // ## Ascending order
                            return obj1.compareToIgnoreCase(obj2); // To compare string values
                        }
                    });
                }

                listPopupWindow.setAdapter(new ArrayAdapter(StudentAdminProcessActivity.this, R.layout.list_dropdown_item, classList));
            }

            listPopupWindow.setAnchorView(layout);
            listPopupWindow.setWidth(ListPopupWindow.WRAP_CONTENT);
            listPopupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);

            listPopupWindow.setModal(true);
            listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    if (which.equals(App_Constants.GRADE)) {
                        txtGrade.setText(gradeList.get(i));
                        gradeId = gradeList.get(i);
                        txtClass.setText(getString(R.string.select_class_label));
                    } else if (which.equals(App_Constants.CLASS)) {
                        txtClass.setText(classList.get(i));
                        classId = classList.get(i);
                    }
                    listPopupWindow.dismiss();

                }
            });
            listPopupWindow.show();
        }
    }

    private void show_year_list(EditText layout) {

        if (yearBeans != null && yearBeans.size() > 0) {

            listPopupWindow = new ListPopupWindow(StudentAdminProcessActivity.this);

            yearList.clear();
            for (GetYearListResponse.ResultBean beans : yearBeans) {
                if (!yearList.contains(beans.getSchoolYear()))
                    yearList.add(beans.getSchoolYear());
            }

            listPopupWindow.setAdapter(new ArrayAdapter(StudentAdminProcessActivity.this, R.layout.list_dropdown_item, yearList));

            listPopupWindow.setAnchorView(layout);
            listPopupWindow.setWidth(ListPopupWindow.WRAP_CONTENT);
            listPopupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);

            listPopupWindow.setModal(true);
            listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    etAcedamicYear.setText(yearList.get(i));
                    year = yearList.get(i);
                    listPopupWindow.dismiss();

                }
            });
            listPopupWindow.show();
        }
    }

    public boolean isValidEmaillId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == APIError.UPDATE_TOKEN && resultCode == RESULT_OK) {

            if (data != null) {
                String apiName = data.getStringExtra("api");
                onRelaod(apiName);
            }

        }
    }


    private void onRelaod(String apiName) {

        if (apiName != null) {

            if (apiName.equals(App_Constants.API_REGISTER_STUDENT_ADMIN_PROCESS)) {

                registerStudent();

            } else if (apiName.equals(App_Constants.API_UPDATE_STUDENT_ADMIN_PROCESS)) {

                updateStudent();

            }
        }
    }

}
