/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.userinfo.driver;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.util.ArrayList;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.Listeners.OnReloadListener;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.LastItemDecoration;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.GetAllDriverResponse;
import ca.dataready.SmartSchoolAdmin.userinfo.driver.adapter.DriverAdapter;
import ca.dataready.SmartSchoolAdmin.userinfo.driver.add.AddDriverActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


public class IDriverFragment extends Fragment implements OnReloadListener, SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.recyclerView)
    SuperRecyclerView recyclerView;
    @BindView(R.id.parent_viewAnimator)
    ViewAnimator parentViewAnimator;
    @BindView(R.id.fab_create)
    FloatingActionButton fabCreate;
    Unbinder unbinder;
    @BindView(R.id.et_search)
    EditText etSearch;
    @BindView(R.id.search_img)
    ImageView searchImg;
    private Call<GetAllDriverResponse> appApi;
    private DriverAdapter adapter;
    private ArrayList<GetAllDriverResponse.ResultBean> results;
    private GridLayoutManager gridLayoutManager;
    private String email, name;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_idriver, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) getActivity()).setSubTitle(getString(R.string.driver));
        ((HomeActivity) getActivity()).setOnReloadListener(this);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            gridLayoutManager = new GridLayoutManager(getContext(), 3);
        } else {
            gridLayoutManager = new GridLayoutManager(getContext(), 2);
        }

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.addItemDecoration(new LastItemDecoration());
        recyclerView.setRefreshListener(IDriverFragment.this);

        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                String text = arg0.toString().toLowerCase();
                if (text.length() == 0) {
                    recyclerView.showProgress();
                    getAllDriver();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
            }
        });

        if (savedInstanceState != null) {

            results = savedInstanceState.getParcelableArrayList(App_Constants.OBJECT);
            if (results != null && results.size() > 0) {

                setData();

            } else {

                adapter = new DriverAdapter(getActivity(), IDriverFragment.this);
                recyclerView.setAdapter(adapter);
                TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                emptyView.setText(getString(R.string.no_data));
                parentViewAnimator.setDisplayedChild(1);
            }

        } else {

            Init();
        }

    }

    private void Init() {

        getAllDriver();
    }

    private void getAllDriver() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();

        if(entity==null)
            return;

        appApi = AdminApp.getInstance().getApi().getAllDrivers(entity.getSchoolId());
        appApi.enqueue(new Callback<GetAllDriverResponse>() {
            @Override
            public void onResponse(Call<GetAllDriverResponse> call, Response<GetAllDriverResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            results = response.body().getResult();
                            if (results != null && results.size() > 0) {

                                setData();

                            } else {

                                results = null;
                                adapter = new DriverAdapter(getActivity(), IDriverFragment.this);
                                recyclerView.setAdapter(adapter);
                                TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                                emptyView.setText(getString(R.string.no_data));
                                parentViewAnimator.setDisplayedChild(1);
                            }

                        } else {

                            results = null;
                            adapter = new DriverAdapter(getActivity(), IDriverFragment.this);
                            recyclerView.setAdapter(adapter);
                            TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                            emptyView.setText(getString(R.string.no_data));
                            parentViewAnimator.setDisplayedChild(1);
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_GET_ALL_DRIVERS);
                        adapter = new DriverAdapter(getActivity(), IDriverFragment.this);
                        recyclerView.setAdapter(adapter);
                        TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                        emptyView.setText(error.message());
                        parentViewAnimator.setDisplayedChild(1);

                    }
                } catch (Exception e) {

                    adapter = new DriverAdapter(getActivity(), IDriverFragment.this);
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(getString(R.string.somethingwrong));
                    parentViewAnimator.setDisplayedChild(1);
                }

            }

            @Override
            public void onFailure(Call<GetAllDriverResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    parentViewAnimator.setDisplayedChild(1);
                    adapter = new DriverAdapter(getActivity(), IDriverFragment.this);
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(App_Constants.NO_INTERNET);
                }
            }
        });

    }


    private void setData() {

        adapter = new DriverAdapter(getActivity(), IDriverFragment.this);
        recyclerView.setAdapter(adapter);

        adapter.addItem(results);
        parentViewAnimator.setDisplayedChild(1);

    }


    public void showPopUp(ImageView imgMore, int position, GetAllDriverResponse.ResultBean bean) {

        PopupMenu popup = new PopupMenu(imgMore.getContext(), imgMore);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.edit_driver_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new CardMenuItemClickListener(position, bean));
        popup.show();
    }

    @OnClick({R.id.search_img, R.id.fab_create})
    public void onViewClicked(View view) {

        switch (view.getId()) {

            case R.id.search_img:

                if (etSearch.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), getString(R.string.please_enter_text), Toast.LENGTH_SHORT).show();
                } else {
                    searchDriverApi();
                }

                break;

            case R.id.fab_create:

                startActivityForResult(new Intent(getActivity(), AddDriverActivity.class), App_Constants.UPDATE_LISTING);
                if (getActivity() != null)
                    getActivity().overridePendingTransition(R.anim.enter, R.anim.leave);

                break;
        }
    }

    private void searchDriverApi() {


        if (isValidEmaillId(etSearch.getText().toString().trim())) {
            email = etSearch.getText().toString();
            name = "";
        } else {
            name = etSearch.getText().toString();
            email = "";
        }

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();

        recyclerView.showProgress();

        appApi = AdminApp.getInstance().getApi().searchDriver(entity.getSchoolId(), email, name);
        appApi.enqueue(new Callback<GetAllDriverResponse>() {
            @Override
            public void onResponse(Call<GetAllDriverResponse> call, Response<GetAllDriverResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            results = response.body().getResult();
                            if (results != null && results.size() > 0) {

                                setData();

                            } else {

                                results = null;
                                adapter = new DriverAdapter(getActivity(), IDriverFragment.this);
                                recyclerView.setAdapter(adapter);
                                TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                                emptyView.setText(getString(R.string.no_data));
                                parentViewAnimator.setDisplayedChild(1);
                            }

                        } else {

                            results = null;
                            adapter = new DriverAdapter(getActivity(), IDriverFragment.this);
                            recyclerView.setAdapter(adapter);
                            TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                            emptyView.setText(getString(R.string.no_data));
                            parentViewAnimator.setDisplayedChild(1);
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_SEARCH_DRIVER);
                        adapter = new DriverAdapter(getActivity(), IDriverFragment.this);
                        recyclerView.setAdapter(adapter);
                        TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                        emptyView.setText(error.message());
                        parentViewAnimator.setDisplayedChild(1);

                    }
                } catch (Exception e) {

                    adapter = new DriverAdapter(getActivity(), IDriverFragment.this);
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(getString(R.string.somethingwrong));
                    parentViewAnimator.setDisplayedChild(1);
                }

            }

            @Override
            public void onFailure(Call<GetAllDriverResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    parentViewAnimator.setDisplayedChild(1);
                    adapter = new DriverAdapter(getActivity(), IDriverFragment.this);
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(App_Constants.NO_INTERNET);
                }
            }
        });
    }


    private boolean isValidEmaillId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    private class CardMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        private int position;
        private GetAllDriverResponse.ResultBean bean;

        CardMenuItemClickListener(int positon, GetAllDriverResponse.ResultBean bean) {
            this.position = positon;
            this.bean = bean;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {

                case R.id.item_edit:

                    startActivityForResult(new Intent(getActivity(), AddDriverActivity.class)
                            .putExtra(App_Constants.OBJECT, bean), App_Constants.UPDATE_LISTING);
                    if (getActivity() != null)
                        getActivity().overridePendingTransition(R.anim.enter, R.anim.leave);

                    break;

                default:
            }
            return false;
        }
    }

    @Override
    public void onRefresh() {

        if (isVisible())
            getAllDriver();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == APIError.UPDATE_TOKEN && resultCode == RESULT_OK) {

            if (data != null) {
                String apiName = data.getStringExtra("api");
                onReload(apiName);
            }

        } else if (requestCode == App_Constants.UPDATE_LISTING && resultCode == RESULT_OK) {

            getAllDriver();

        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onReload(String apiName) {

        if (apiName != null) {

            if (isVisible()) {
                if (apiName.equals(App_Constants.API_GET_ALL_DRIVERS)) {
                    getAllDriver();
                } else if (apiName.equals(App_Constants.API_SEARCH_DRIVER)) {
                    searchDriverApi();
                }
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(App_Constants.OBJECT, results);
    }


    @Override
    public void onStop() {
        super.onStop();
        if (appApi != null)
            appApi.cancel();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            gridLayoutManager = new GridLayoutManager(getContext(), 3);
        } else {
            gridLayoutManager = new GridLayoutManager(getContext(), 2);
        }
        recyclerView.setLayoutManager(gridLayoutManager);
    }

}