/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.userinfo.teacher.add;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.LatLng;
import com.hbb20.CountryCodePicker;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.DateFunction;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.Utilities.place.PlaceAPIAdapter;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.common.adapter.AttachmentAdapter;
import ca.dataready.SmartSchoolAdmin.fileselectoer.FileSelectionActivity;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.AddDriverModel;
import ca.dataready.SmartSchoolAdmin.server.AddTeacherParams;
import ca.dataready.SmartSchoolAdmin.server.AddTeacherResponse;
import ca.dataready.SmartSchoolAdmin.server.AppApi;
import ca.dataready.SmartSchoolAdmin.server.GetAllTeacherResponse;
import ca.dataready.SmartSchoolAdmin.server.GetLatLngFromAddressResponse;
import ca.dataready.SmartSchoolAdmin.server.GetQrCodeResponse;
import ca.dataready.SmartSchoolAdmin.server.GetTeacherInfo;
import ca.dataready.SmartSchoolAdmin.server.MultipleImageUpload;
import ca.dataready.SmartSchoolAdmin.server.QualificationModel;
import ca.dataready.SmartSchoolAdmin.userinfo.teacher.adapter.ExperienceAdapter;
import ca.dataready.SmartSchoolAdmin.userinfo.teacher.adapter.QualificationAdapter;
import ca.dataready.SmartSchoolAdmin.viewresponse.adapter.FilesAdapter;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.FileModel;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.ImageUpload;
import info.hoang8f.android.segmented.SegmentedGroup;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddTeacherActivity extends BaseActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txtProfile)
    TextView txtProfile;
    @BindView(R.id.card_profile_info)
    CardView cardProfileInfo;
    @BindView(R.id.et_first_name)
    EditText etFirstName;
    @BindView(R.id.et_last_name)
    EditText etLastName;
    @BindView(R.id.et_dob)
    EditText etDob;
    @BindView(R.id.rb_male)
    RadioButton rbMale;
    @BindView(R.id.rb_female)
    RadioButton rbFemale;
    @BindView(R.id.sgGender)
    SegmentedGroup sgGender;
    @BindView(R.id.rb_single)
    RadioButton rbSingle;
    @BindView(R.id.rb_married)
    RadioButton rbMarried;
    @BindView(R.id.sgMartialStatus)
    SegmentedGroup sgMartialStatus;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_Email)
    EditText etEmail;
    @BindView(R.id.et_joining_date)
    EditText etJoiningDate;
    @BindView(R.id.et_position)
    EditText etPosition;
    @BindView(R.id.et_department)
    EditText etDepartment;
    @BindView(R.id.et_address)
    AutoCompleteTextView etAddress;
    @BindView(R.id.img_address)
    ImageView imgAddress;
    @BindView(R.id.teacher_profile_pic)
    ImageView teacherProfilePic;
    @BindView(R.id.linaer_capture_image)
    LinearLayout linaerCaptureImage;
    @BindView(R.id.btn_edit)
    Button btnEdit;
    @BindView(R.id.btn_next)
    Button btnNext;
    @BindView(R.id.layout_ptofile_info)
    CardView layoutPtofileInfo;
    @BindView(R.id.txtEmergecy)
    TextView txtEmergecy;
    @BindView(R.id.card_emergency_contact)
    CardView cardEmergencyContact;
    @BindView(R.id.et_ec_first_name)
    EditText etEcFirstName;
    @BindView(R.id.et_ec_last_name)
    EditText etEcLastName;
    @BindView(R.id.et_ec_relation)
    EditText etEcRelation;
    @BindView(R.id.et_ec_phone)
    EditText etEcPhone;
    @BindView(R.id.et_ec_email)
    EditText etEcEmail;
    @BindView(R.id.et_ec_address)
    AutoCompleteTextView etEcAddress;
    @BindView(R.id.img_ec_address)
    ImageView imgEcAddress;
    @BindView(R.id.btn_ec_previous)
    Button btnEcPrevious;
    @BindView(R.id.btn_ec_next)
    Button btnEcNext;
    @BindView(R.id.layout_emergency_contact)
    CardView layoutEmergencyContact;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.txt_qualification)
    TextView txtQualification;
    @BindView(R.id.add_more_qualification)
    ImageView addMoreQualification;
    @BindView(R.id.card_qualification)
    CardView cardQualification;
    @BindView(R.id.rv_qualification)
    RecyclerView rvQualification;
    @BindView(R.id.btn_ql_previous)
    Button btnQlPrevious;
    @BindView(R.id.btn_ql_next)
    Button btnQlNext;
    @BindView(R.id.layout_qualification)
    CardView layoutQualification;
    @BindView(R.id.txt_experience)
    TextView txtExperience;
    @BindView(R.id.add_more_experience)
    ImageView addMoreExperience;
    @BindView(R.id.card_experience)
    CardView cardExperience;
    @BindView(R.id.rv_experience)
    RecyclerView rvExperience;
    @BindView(R.id.btn_exp_previous)
    Button btnExpPrevious;
    @BindView(R.id.btn_exp_next)
    Button btnExpNext;
    @BindView(R.id.layout_experience)
    CardView layoutExperience;
    @BindView(R.id.txt_achivements)
    TextView txtAchivements;
    @BindView(R.id.card_achivements)
    CardView cardAchivements;
    @BindView(R.id.btn_sa_previous)
    Button btnSaPrevious;
    @BindView(R.id.btn_sa_next)
    Button btnSaNext;
    @BindView(R.id.layout_achivements)
    CardView layoutAchivements;
    @BindView(R.id.txt_error)
    TextView txtError;
    @BindView(R.id.viewAnimator)
    ViewAnimator viewAnimator;
    @BindView(R.id.txtPhysician)
    TextView txtPhysician;
    @BindView(R.id.card_physician_contact)
    CardView cardPhysicianContact;
    @BindView(R.id.et_pci_first_name)
    EditText etPciFirstName;
    @BindView(R.id.et_pci_last_name)
    EditText etPciLastName;
    @BindView(R.id.et_pci_phone_number)
    EditText etPciPhoneNumber;
    @BindView(R.id.et_pci_fax_number)
    EditText etPciFaxNumber;
    @BindView(R.id.et_pci_address)
    AutoCompleteTextView etPciAddress;
    @BindView(R.id.img_pci_address)
    ImageView imgPciAddress;
    @BindView(R.id.btn_pci_previous)
    Button btnPciPrevious;
    @BindView(R.id.btn_pci_next)
    Button btnPciNext;
    @BindView(R.id.layout_physician_contact)
    CardView layoutPhysicianContact;
    @BindView(R.id.chk_transportation)
    CheckBox chkTransportation;
    @BindView(R.id.rb_at_home)
    RadioButton rbAtHome;
    @BindView(R.id.rb_community_point)
    RadioButton rbCommunityPoint;
    @BindView(R.id.sgTransportation)
    SegmentedGroup sgTransportation;
    @BindView(R.id.card_feedback)
    CardView cardFeedback;
    @BindView(R.id.textFeedback)
    TextView textFeedback;
    @BindView(R.id.layout_card_feedback)
    CardView layoutFeedback;
    @BindView(R.id.et_fb_trasportation_comment)
    EditText etFbTrasportationComment;
    @BindView(R.id.et_fb_school_comment)
    EditText etFbSchoolComment;
    @BindView(R.id.btn_fb_previous)
    Button btnFbPrevious;
    @BindView(R.id.btn_fb_next)
    Button btnFbNext;
    @BindView(R.id.txt_profile_attachment)
    TextView txtProfileAttachment;
    @BindView(R.id.rv_profile_attachment)
    RecyclerView rvProfileAttachment;
    @BindView(R.id.txt_qulification_attachment)
    TextView txtQulificationAttachment;
    @BindView(R.id.rv_qualification_attachment)
    RecyclerView rvQualificationAttachment;
    @BindView(R.id.txt_exp_attachment)
    TextView txtExpAttachment;
    @BindView(R.id.rv_exp_attachment)
    RecyclerView rvExpAttachment;
    @BindView(R.id.txt_ec_attachment)
    TextView txtEcAttachment;
    @BindView(R.id.rv_ec_attachment)
    RecyclerView rvEcAttachment;
    @BindView(R.id.txt_other_attachments)
    TextView txtOtherAttachments;
    @BindView(R.id.rv_other_attachment)
    RecyclerView rvOtherAttachment;
    @BindView(R.id.img_qrcode)
    ImageView imgQrcode;
    @BindView(R.id.txt_qr_not_avail)
    TextView txtQrNotAvail;
    @BindView(R.id.qr_frame)
    FrameLayout qrFrame;
    @BindView(R.id.country_code)
    CountryCodePicker countryCode;
    @BindView(R.id.country_code_ec)
    CountryCodePicker countryCodeEc;
    @BindView(R.id.country_code_pci)
    CountryCodePicker countryCodePci;
    /*  @BindView(R.id.recyclerView)
          RecyclerView recyclerView;*/
    private Call<AddTeacherResponse> registerTeacherCall;
    private String picturePath;
    private String teacherPicURL;
    private FilesAdapter filesAdapter;
    private List<FileModel> selectedFiles = new ArrayList<>();
    private String documentSelectionType;
    private String selectedGender, selectedMartialStatus;
    private QualificationAdapter qualificationAdapter;
    private ExperienceAdapter experienceAdapter;
    private ArrayList<AddTeacherParams.QualificationDetailsBean.QualificationsBean> QualificationList;
    private ArrayList<AddTeacherParams.ExperienceDetailsBean.ExperiencesBean> experienceList;
    private Call<GetTeacherInfo> getTeacherInfoCall;
    private String emailId;
    private String id;
    private Call<AddTeacherResponse> updateTeacherCall;
    private Call<GetLatLngFromAddressResponse> locationCall;
    private Call<MultipleImageUpload> attachMentCall;
    private AttachmentAdapter attachmentAdapter;
    private List<MultipleImageUpload.ResultBean> profileFiles = new ArrayList<>();
    private List<MultipleImageUpload.ResultBean> qualificationFiles = new ArrayList<>();
    private List<MultipleImageUpload.ResultBean> experienceFiles = new ArrayList<>();
    private List<MultipleImageUpload.ResultBean> emergencyFiles = new ArrayList<>();
    private List<MultipleImageUpload.ResultBean> otherFiles = new ArrayList<>();
    private Call<GetQrCodeResponse> getQRCall;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_teacher);
        ButterKnife.bind(this);
        setHeaderInfo();
        Init();
    }

    private void setHeaderInfo() {

        setSupportActionBar(toolbar);

        if (AdminApp.getInstance().getAdmin() != null)
            getSupportActionBar().setTitle(AdminApp.getInstance().getAdmin().getSchoolName() + " " + AdminApp.getInstance().getAdmin().getSchoolYear());
        else
            getSupportActionBar().setTitle(getString(R.string.app_name));

        getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + getString(R.string.add_teacher) + "</small>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void Init() {

        rvProfileAttachment.setLayoutManager(new LinearLayoutManager(AddTeacherActivity.this));
        rvProfileAttachment.addItemDecoration(new DividerItemDecoration(AddTeacherActivity.this, DividerItemDecoration.VERTICAL));

        rvQualificationAttachment.setLayoutManager(new LinearLayoutManager(AddTeacherActivity.this));
        rvQualificationAttachment.addItemDecoration(new DividerItemDecoration(AddTeacherActivity.this, DividerItemDecoration.VERTICAL));

        rvExpAttachment.setLayoutManager(new LinearLayoutManager(AddTeacherActivity.this));
        rvExpAttachment.addItemDecoration(new DividerItemDecoration(AddTeacherActivity.this, DividerItemDecoration.VERTICAL));

        rvEcAttachment.setLayoutManager(new LinearLayoutManager(AddTeacherActivity.this));
        rvEcAttachment.addItemDecoration(new DividerItemDecoration(AddTeacherActivity.this, DividerItemDecoration.VERTICAL));

        rvOtherAttachment.setLayoutManager(new LinearLayoutManager(AddTeacherActivity.this));
        rvOtherAttachment.addItemDecoration(new DividerItemDecoration(AddTeacherActivity.this, DividerItemDecoration.VERTICAL));

        enableTansportation(false);

        chkTransportation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    enableTansportation(true);
                } else {
                    enableTansportation(false);
                }
            }
        });

        rvQualification.setLayoutManager(new LinearLayoutManager(this));
        qualificationAdapter = new QualificationAdapter(this);
        rvQualification.setAdapter(qualificationAdapter);

        rvExperience.setLayoutManager(new LinearLayoutManager(this));
        experienceAdapter = new ExperienceAdapter(this);
        rvExperience.setAdapter(experienceAdapter);


        if (getIntent().getExtras() != null) {

            GetAllTeacherResponse.ResultBean bean = getIntent().getParcelableExtra(App_Constants.OBJECT);
            if (bean != null) {
                emailId = bean.getEmailId();
                getTeacherInfo();
            }

            Glide.with(this)
                    .load(MapImageUrl(600, 150, 21.1702, 72.8311))
                    .into(imgPciAddress);

        } else {

            Glide.with(this)
                    .load(MapImageUrl(600, 150, 21.1702, 72.8311))
                    .into(imgAddress);

            Glide.with(this)
                    .load(MapImageUrl(600, 150, 21.1702, 72.8311))
                    .into(imgEcAddress);

            Glide.with(this)
                    .load(MapImageUrl(600, 150, 21.1702, 72.8311))
                    .into(imgPciAddress);

            viewAnimator.setDisplayedChild(1);
        }

        etAddress.setAdapter(new PlaceAPIAdapter(getApplicationContext(), R.layout.autocomplete_list_item));
        etAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String address = (String) parent.getItemAtPosition(position);
                try {
                    getLocationFromAddress(imgAddress, address);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        etEcAddress.setAdapter(new PlaceAPIAdapter(getApplicationContext(), R.layout.autocomplete_list_item));
        etEcAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String address = (String) parent.getItemAtPosition(position);
                try {
                    getLocationFromAddress(imgEcAddress, address);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });



        etPciAddress.setAdapter(new PlaceAPIAdapter(getApplicationContext(), R.layout.autocomplete_list_item));
        etPciAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String address = (String) parent.getItemAtPosition(position);
                try {
                    getLocationFromAddress(imgPciAddress, address);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

       /* etAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().length() != 0 && editable.toString().length() % 6 == 0) {

                    getLocationFromAddress(imgAddress, editable.toString());

                }
            }
        });

        etEcAddress.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().length() != 0 && editable.toString().length() % 6 == 0) {

                    getLocationFromAddress(imgEcAddress, editable.toString());

                }
            }
        });

        etPciAddress.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().length() != 0 && editable.toString().length() % 6 == 0) {
                    getLocationFromAddress(imgPciAddress, editable.toString());
                }
            }
        });

*/
    }

    private void enableTansportation(boolean isEnabled) {

        sgTransportation.setEnabled(isEnabled);
        rbAtHome.setEnabled(isEnabled);
        rbCommunityPoint.setEnabled(isEnabled);
        sgTransportation.setAlpha(isEnabled ? 1.0f : 0.5f);
    }

    private ArrayList<AddDriverModel> getData() {

        ArrayList<AddDriverModel> model = new ArrayList<>();
        model.add(new AddDriverModel(0, true));
        model.add(new AddDriverModel(1, false));

        return model;
    }

    @OnClick({R.id.btn_fb_next, R.id.btn_fb_previous, R.id.btn_pci_next, R.id.btn_pci_previous, R.id.btn_sa_next
            , R.id.btn_sa_previous, R.id.card_achivements, R.id.btn_exp_previous, R.id.btn_exp_next, R.id.add_more_experience, R.id.card_experience
            , R.id.btn_ql_previous, R.id.card_qualification, R.id.btn_ql_next, R.id.btn_ec_next
            , R.id.txt_profile_attachment, R.id.txt_qulification_attachment, R.id.txt_exp_attachment, R.id.txt_ec_attachment, R.id.txt_other_attachments
            , R.id.et_dob, R.id.et_joining_date, R.id.btn_edit, R.id.card_profile_info
            , R.id.card_emergency_contact, R.id.card_physician_contact, R.id.btn_next, R.id.btn_ec_previous, R.id.linaer_capture_image, R.id.add_more_qualification, R.id.card_feedback})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.btn_fb_next:

                setUpViews(false, false, false, false
                        , true, false, false);

                break;

            case R.id.btn_fb_previous:

                setUpViews(false, false, false, false
                        , false, true, false);

                break;

            case R.id.btn_pci_next:

                setUpViews(false, false, false, false
                        , false, false, true);

                break;

            case R.id.btn_pci_previous:

                setUpViews(false, false, false, true
                        , false, false, false);

                break;

            case R.id.btn_sa_next:

                validateData();

                break;

            case R.id.btn_sa_previous:

                setUpViews(false, false, false, false
                        , false, false, true);

                break;

            case R.id.btn_exp_next:

                ArrayList<AddTeacherParams.ExperienceDetailsBean.ExperiencesBean> results = experienceAdapter.getExperienceData();
                experienceList = new ArrayList<>();
                if (results != null && results.size() > 0) {

                    for (AddTeacherParams.ExperienceDetailsBean.ExperiencesBean bean : results) {

                        Log.e("DATA", "ORGANIZATION " + bean.getOrganizationName()
                                + " ,Passed Year " + bean.getFromYear() + " - " + bean.getToYear() + " ,ADDRESS " + bean.getAddress());

                        if (bean.getOrganizationName().isEmpty()) {
                            Toast.makeText(this, getString(R.string.please_select_organization_name), Toast.LENGTH_SHORT).show();
                            break;
                        }

                        if (bean.getFromYear().isEmpty()) {
                            Toast.makeText(this, getString(R.string.please_select_from_date), Toast.LENGTH_SHORT).show();
                            break;
                        }

                        if (bean.getToYear().isEmpty()) {
                            Toast.makeText(this, getString(R.string.please_select_to_date), Toast.LENGTH_SHORT).show();
                            break;
                        }

                        if (bean.getAddress().isEmpty()) {
                            Toast.makeText(this, getString(R.string.please_select_university), Toast.LENGTH_SHORT).show();
                            break;
                        }

                        experienceList.add(bean);
                    }

                    if (experienceList != null && experienceList.size() > 0) {

                        setUpViews(false, false, false
                                , true, false, false, false);
                    }

                } else {

                    setUpViews(false, false, false
                            , true, false, false, false);
                }

                break;

            case R.id.btn_exp_previous:

                setUpViews(false, true, false
                        , false, false, false, false);

                break;


            case R.id.btn_ql_next:

                ArrayList<QualificationModel> qualificationResults = qualificationAdapter.getQualificationData();
                ArrayList<QualificationModel> updatedList = new ArrayList<>();
                QualificationList = new ArrayList<>();

                if (qualificationResults != null && qualificationResults.size() > 0) {

                    for (QualificationModel bean : qualificationResults) {

                        Log.e("DATA", "UNIVERSITY " + bean.getUniversity()
                                + " ,Passed Year " + bean.getValidFrom() + " - " + bean.getValidTo() + " ,QULIFICATIOn " + bean.getQualificationName());

                        if (bean.getQualificationName().isEmpty()) {
                            Toast.makeText(this, getString(R.string.please_select_organization_name), Toast.LENGTH_SHORT).show();
                            break;
                        }

                        if (bean.getValidFrom().isEmpty()) {
                            Toast.makeText(this, getString(R.string.please_select_from_date), Toast.LENGTH_SHORT).show();
                            break;
                        }

                        if (bean.getValidTo().isEmpty()) {
                            Toast.makeText(this, getString(R.string.please_select_to_date), Toast.LENGTH_SHORT).show();
                            break;
                        }

                        if (bean.getUniversity().isEmpty()) {
                            Toast.makeText(this, getString(R.string.please_select_university), Toast.LENGTH_SHORT).show();
                            break;
                        }
                        updatedList.add(bean);
                    }


                    if (updatedList != null && updatedList.size() > 0) {

                        for (QualificationModel model : updatedList) {

                            AddTeacherParams.QualificationDetailsBean.QualificationsBean qBean = new AddTeacherParams.QualificationDetailsBean.QualificationsBean();
                            qBean.setPassedYear(model.getValidFrom() + " - " + model.getValidTo());
                            qBean.setUniversity(model.getUniversity());
                            qBean.setQualificationName(model.getQualificationName());
                            QualificationList.add(qBean);

                        }

                        setUpViews(false, false, true
                                , false, false, false, false);
                    }

                } else {

                    setUpViews(false, false, true
                            , false, false, false, false);
                }

                break;


            case R.id.btn_ql_previous:

                setUpViews(true, false, false
                        , false, false, false, false);

                break;

            case R.id.btn_ec_next:

                setUpViews(false, false, false
                        , false, false, true, false);

                break;

            case R.id.btn_ec_previous:

                setUpViews(false, false, true
                        , false, false, false, false);

                break;

            case R.id.btn_next:

                if (etFirstName.getText().toString().isEmpty()) {
                    etFirstName.setError(getString(R.string.field_required));
                } else if (etLastName.getText().toString().isEmpty()) {
                    etLastName.setError(getString(R.string.field_required));
                } else if (etDob.getText().toString().isEmpty()) {
                    Toast.makeText(this, getString(R.string.please_select_dob), Toast.LENGTH_SHORT).show();
                } else if (etPhone.getText().toString().isEmpty()) {
                    etPhone.setError(getString(R.string.field_required));
                } else if (etEmail.getText().toString().isEmpty()) {
                    etEmail.setError(getString(R.string.field_required));
                } else if (!isValidEmaillId(etEmail.getText().toString())) {
                    etEmail.setError(getString(R.string.invalid_email));
                } else if (etAddress.getText().toString().isEmpty()) {
                    etAddress.setError(getString(R.string.field_required));
                } else {
                    setUpViews(false, true, false
                            , false, false, false, false);
                }
                break;


            case R.id.add_more_qualification:

                rvQualification.setVisibility(View.VISIBLE);

                ArrayList<QualificationModel> beans = new ArrayList<>();

                QualificationModel bean = new QualificationModel();
                bean.setPassedYear(App_Constants.EMPTY);
                bean.setQualificationName(App_Constants.EMPTY);
                bean.setUniversity(App_Constants.EMPTY);
                bean.setValidFrom(App_Constants.EMPTY);
                bean.setValidTo(App_Constants.EMPTY);

                beans.add(bean);

                qualificationAdapter.addItem(beans);

                break;

            case R.id.add_more_experience:

                rvExperience.setVisibility(View.VISIBLE);

                ArrayList<AddTeacherParams.ExperienceDetailsBean.ExperiencesBean> expBeans = new ArrayList<>();

                AddTeacherParams.ExperienceDetailsBean.ExperiencesBean expBean = new AddTeacherParams.ExperienceDetailsBean.ExperiencesBean();
                expBean.setAddress(App_Constants.EMPTY);
                expBean.setOrganizationName(App_Constants.EMPTY);
                expBean.setFromYear(App_Constants.EMPTY);
                expBean.setToYear(App_Constants.EMPTY);

                expBeans.add(expBean);

                experienceAdapter.addItem(expBeans);

                break;


            case R.id.linaer_capture_image:

                selectPhoto();

                break;

            case R.id.txt_profile_attachment:

                selectDocument(App_Constants.PROFILE_ATTACHMENT);

                break;

            case R.id.txt_qulification_attachment:

                selectDocument(App_Constants.QUALIFICATION_ATTACHMENT);

                break;

            case R.id.txt_exp_attachment:

                selectDocument(App_Constants.EXPERIENCE_ATTCHEMTENT);

                break;

            case R.id.txt_ec_attachment:

                selectDocument(App_Constants.EMERGENCY_ATTACHMENT);

                break;

            case R.id.txt_other_attachments:

                selectDocument(App_Constants.OTHER_ATTACHMENT);

                break;


            case R.id.et_dob:

                setDate(etDob);

                break;


            case R.id.et_joining_date:

                setDate(etJoiningDate);

                break;

            case R.id.btn_edit:

                selectPhoto();

                break;


            case R.id.card_profile_info:

                layoutPtofileInfo.setVisibility(layoutPtofileInfo.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                if (layoutEmergencyContact.getVisibility() == View.VISIBLE) {
                    layoutEmergencyContact.setVisibility(View.GONE);
                    txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutQualification.getVisibility() == View.VISIBLE) {
                    layoutQualification.setVisibility(View.GONE);
                    txtQualification.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutExperience.getVisibility() == View.VISIBLE) {
                    layoutExperience.setVisibility(View.GONE);
                    txtExperience.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutAchivements.getVisibility() == View.VISIBLE) {
                    layoutAchivements.setVisibility(View.GONE);
                    txtAchivements.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPhysicianContact.getVisibility() == View.VISIBLE) {
                    layoutPhysicianContact.setVisibility(View.GONE);
                    txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }
                if (layoutFeedback.getVisibility() == View.VISIBLE) {
                    layoutFeedback.setVisibility(View.GONE);
                    textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPtofileInfo.getVisibility() == View.VISIBLE)
                    txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_up), null);
                else
                    txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);

                addMoreQualification.setVisibility(layoutQualification.getVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);
                addMoreExperience.setVisibility(layoutExperience.getVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);

                break;

            case R.id.card_qualification:

                layoutQualification.setVisibility(layoutQualification.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                if (layoutPtofileInfo.getVisibility() == View.VISIBLE) {
                    layoutPtofileInfo.setVisibility(View.GONE);
                    txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutEmergencyContact.getVisibility() == View.VISIBLE) {
                    layoutEmergencyContact.setVisibility(View.GONE);
                    txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutExperience.getVisibility() == View.VISIBLE) {
                    layoutExperience.setVisibility(View.GONE);
                    txtExperience.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutAchivements.getVisibility() == View.VISIBLE) {
                    layoutAchivements.setVisibility(View.GONE);
                    txtAchivements.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPhysicianContact.getVisibility() == View.VISIBLE) {
                    layoutPhysicianContact.setVisibility(View.GONE);
                    txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutFeedback.getVisibility() == View.VISIBLE) {
                    layoutFeedback.setVisibility(View.GONE);
                    textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutQualification.getVisibility() == View.VISIBLE)
                    txtQualification.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_up), null);
                else
                    txtQualification.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);

                addMoreQualification.setVisibility(layoutQualification.getVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);
                addMoreExperience.setVisibility(layoutExperience.getVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);

                break;

            case R.id.card_experience:

                layoutExperience.setVisibility(layoutExperience.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                if (layoutPtofileInfo.getVisibility() == View.VISIBLE) {
                    layoutPtofileInfo.setVisibility(View.GONE);
                    txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutQualification.getVisibility() == View.VISIBLE) {
                    layoutQualification.setVisibility(View.GONE);
                    txtQualification.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutEmergencyContact.getVisibility() == View.VISIBLE) {
                    layoutEmergencyContact.setVisibility(View.GONE);
                    txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutAchivements.getVisibility() == View.VISIBLE) {
                    layoutAchivements.setVisibility(View.GONE);
                    txtAchivements.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPhysicianContact.getVisibility() == View.VISIBLE) {
                    layoutPhysicianContact.setVisibility(View.GONE);
                    txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutFeedback.getVisibility() == View.VISIBLE) {
                    layoutFeedback.setVisibility(View.GONE);
                    textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutExperience.getVisibility() == View.VISIBLE)
                    txtExperience.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_up), null);
                else
                    txtExperience.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);

                addMoreQualification.setVisibility(layoutQualification.getVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);
                addMoreExperience.setVisibility(layoutExperience.getVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);

                break;

            case R.id.card_emergency_contact:

                layoutEmergencyContact.setVisibility(layoutEmergencyContact.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                if (layoutPtofileInfo.getVisibility() == View.VISIBLE) {
                    layoutPtofileInfo.setVisibility(View.GONE);
                    txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutQualification.getVisibility() == View.VISIBLE) {
                    layoutQualification.setVisibility(View.GONE);
                    txtQualification.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutExperience.getVisibility() == View.VISIBLE) {
                    layoutExperience.setVisibility(View.GONE);
                    txtExperience.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutAchivements.getVisibility() == View.VISIBLE) {
                    layoutAchivements.setVisibility(View.GONE);
                    txtAchivements.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPhysicianContact.getVisibility() == View.VISIBLE) {
                    layoutPhysicianContact.setVisibility(View.GONE);
                    txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutFeedback.getVisibility() == View.VISIBLE) {
                    layoutFeedback.setVisibility(View.GONE);
                    textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutEmergencyContact.getVisibility() == View.VISIBLE)
                    txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_up), null);
                else
                    txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);

                addMoreQualification.setVisibility(layoutQualification.getVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);
                addMoreExperience.setVisibility(layoutExperience.getVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);

                break;

            case R.id.card_achivements:

                layoutAchivements.setVisibility(layoutAchivements.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                if (layoutPtofileInfo.getVisibility() == View.VISIBLE) {
                    layoutPtofileInfo.setVisibility(View.GONE);
                    txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutQualification.getVisibility() == View.VISIBLE) {
                    layoutQualification.setVisibility(View.GONE);
                    txtQualification.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutExperience.getVisibility() == View.VISIBLE) {
                    layoutExperience.setVisibility(View.GONE);
                    txtExperience.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutEmergencyContact.getVisibility() == View.VISIBLE) {
                    layoutEmergencyContact.setVisibility(View.GONE);
                    txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPhysicianContact.getVisibility() == View.VISIBLE) {
                    layoutPhysicianContact.setVisibility(View.GONE);
                    txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutFeedback.getVisibility() == View.VISIBLE) {
                    layoutFeedback.setVisibility(View.GONE);
                    textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutAchivements.getVisibility() == View.VISIBLE)
                    txtAchivements.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_up), null);
                else
                    txtAchivements.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);

                addMoreQualification.setVisibility(layoutQualification.getVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);
                addMoreExperience.setVisibility(layoutExperience.getVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);

                break;

            case R.id.card_physician_contact:


                layoutPhysicianContact.setVisibility(layoutPhysicianContact.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                if (layoutPtofileInfo.getVisibility() == View.VISIBLE) {
                    layoutPtofileInfo.setVisibility(View.GONE);
                    txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutQualification.getVisibility() == View.VISIBLE) {
                    layoutQualification.setVisibility(View.GONE);
                    txtQualification.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutExperience.getVisibility() == View.VISIBLE) {
                    layoutExperience.setVisibility(View.GONE);
                    txtExperience.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutEmergencyContact.getVisibility() == View.VISIBLE) {
                    layoutEmergencyContact.setVisibility(View.GONE);
                    txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutAchivements.getVisibility() == View.VISIBLE) {
                    layoutAchivements.setVisibility(View.GONE);
                    txtAchivements.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutFeedback.getVisibility() == View.VISIBLE) {
                    layoutFeedback.setVisibility(View.GONE);
                    textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPhysicianContact.getVisibility() == View.VISIBLE)
                    txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_up), null);
                else
                    txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);

                addMoreQualification.setVisibility(layoutQualification.getVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);
                addMoreExperience.setVisibility(layoutExperience.getVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);

                break;

            case R.id.card_feedback:

                layoutFeedback.setVisibility(layoutFeedback.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);


                if (layoutPtofileInfo.getVisibility() == View.VISIBLE) {
                    layoutPtofileInfo.setVisibility(View.GONE);
                    txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutQualification.getVisibility() == View.VISIBLE) {
                    layoutQualification.setVisibility(View.GONE);
                    txtQualification.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutExperience.getVisibility() == View.VISIBLE) {
                    layoutExperience.setVisibility(View.GONE);
                    txtExperience.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutEmergencyContact.getVisibility() == View.VISIBLE) {
                    layoutEmergencyContact.setVisibility(View.GONE);
                    txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutAchivements.getVisibility() == View.VISIBLE) {
                    layoutAchivements.setVisibility(View.GONE);
                    txtAchivements.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }
                if (layoutPhysicianContact.getVisibility() == View.VISIBLE) {
                    layoutPhysicianContact.setVisibility(View.GONE);
                    txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutFeedback.getVisibility() == View.VISIBLE)
                    textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_up), null);
                else
                    textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);


                addMoreQualification.setVisibility(layoutQualification.getVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);
                addMoreExperience.setVisibility(layoutExperience.getVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);
                break;

            case R.id.add:

                break;

        }
    }

    private void selectPhoto() {

        final Dialog dialog = new Dialog(AddTeacherActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_image_picker_dialog);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setLayout(getScreenWidth(AddTeacherActivity.this) / 2, LinearLayoutCompat.LayoutParams.WRAP_CONTENT);
        }
        dialog.setCancelable(true);
        dialog.show();

        Button gallery = (Button) dialog.findViewById(R.id.bGallery);
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                galleryIntent();

            }
        });

        Button camera = (Button) dialog.findViewById(R.id.bCamera);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                cameraIntent();

            }
        });

    }

    public int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    }

    private void galleryIntent() {

        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, 1);
    }

    private void validateData() {

        if (!isValidProfileData()) {
            setUpViews(true, false, false, false, false, false, false);
        } else if (!isValidQualificationData()) {
            setUpViews(false, true, false, false, false, false, false);
        } else if (!isValidExperienceData()) {
            setUpViews(false, false, true, false, false, false, false);
        } else {

            if (getIntent().getExtras() != null)
                updateTeacher();
            else
                registerTeacher();

        }
    }

    private boolean isValidProfileData() {


        if (etFirstName.getText().toString().isEmpty()) {
            etFirstName.setError(getString(R.string.field_required));
            return false;
        }

        if (etLastName.getText().toString().isEmpty()) {
            etLastName.setError(getString(R.string.field_required));
            return false;
        }

        if (etDob.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.please_select_dob), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (etPhone.getText().toString().isEmpty()) {
            etPhone.setError(getString(R.string.field_required));
            return false;
        }

        if (etEmail.getText().toString().isEmpty()) {
            etEmail.setError(getString(R.string.field_required));
            return false;
        }

        if (!isValidEmaillId(etEmail.getText().toString())) {
            etEmail.setError(getString(R.string.invalid_email));
            return false;
        }

        if (etAddress.getText().toString().isEmpty()) {
            etAddress.setError(getString(R.string.field_required));
            return false;
        }

        if (teacherPicURL == null) {
            Toast.makeText(this, getString(R.string.please_upload_profile_pic), Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;

    }

    private boolean isValidQualificationData() {


        ArrayList<QualificationModel> qualificationResults = qualificationAdapter.getQualificationData();
        ArrayList<QualificationModel> updatedList = new ArrayList<>();
        QualificationList = new ArrayList<>();
        if (qualificationResults != null && qualificationResults.size() > 0) {

            for (QualificationModel bean : qualificationResults) {

                Log.e("DATA", "UNIVERSITY " + bean.getUniversity()
                        + " ,Passed Year " + bean.getValidFrom() + " - " + bean.getValidTo() + " ,QULIFICATIOn " + bean.getQualificationName());

                if (bean.getQualificationName().isEmpty()) {
                    Toast.makeText(this, getString(R.string.please_select_organization_name), Toast.LENGTH_SHORT).show();
                    return false;
                }

                if (bean.getValidFrom().isEmpty()) {
                    Toast.makeText(this, getString(R.string.please_select_from_date), Toast.LENGTH_SHORT).show();
                    return false;
                }

                if (bean.getValidTo().isEmpty()) {
                    Toast.makeText(this, getString(R.string.please_select_to_date), Toast.LENGTH_SHORT).show();
                    return false;
                }

                if (bean.getUniversity().isEmpty()) {
                    Toast.makeText(this, getString(R.string.please_select_university), Toast.LENGTH_SHORT).show();
                    return false;
                }

                updatedList.add(bean);
            }

            if (updatedList != null && updatedList.size() > 0) {

                for (QualificationModel model : updatedList) {

                    AddTeacherParams.QualificationDetailsBean.QualificationsBean qBean = new AddTeacherParams.QualificationDetailsBean.QualificationsBean();
                    qBean.setPassedYear(model.getValidFrom() + " - " + model.getValidTo());
                    qBean.setUniversity(model.getUniversity());
                    qBean.setQualificationName(model.getQualificationName());
                    QualificationList.add(qBean);

                }

                return QualificationList != null && QualificationList.size() > 0;

            } else {

                return false;
            }

        } else {

            return true;
        }

    }

    private boolean isValidExperienceData() {


        ArrayList<AddTeacherParams.ExperienceDetailsBean.ExperiencesBean> results = experienceAdapter.getExperienceData();
        experienceList = new ArrayList<>();
        if (results != null && results.size() > 0) {

            for (AddTeacherParams.ExperienceDetailsBean.ExperiencesBean bean : results) {

                Log.e("DATA", "ORGANIZATION " + bean.getOrganizationName()
                        + " ,Passed Year " + bean.getFromYear() + " - " + bean.getToYear() + " ,ADDRESS " + bean.getAddress());

                if (bean.getOrganizationName().isEmpty()) {
                    Toast.makeText(this, getString(R.string.please_select_organization_name), Toast.LENGTH_SHORT).show();
                    return false;
                }

                if (bean.getFromYear().isEmpty()) {
                    Toast.makeText(this, getString(R.string.please_select_from_date), Toast.LENGTH_SHORT).show();
                    return false;
                }

                if (bean.getToYear().isEmpty()) {
                    Toast.makeText(this, getString(R.string.please_select_to_date), Toast.LENGTH_SHORT).show();
                    return false;
                }

                if (bean.getAddress().isEmpty()) {
                    Toast.makeText(this, getString(R.string.please_select_address), Toast.LENGTH_SHORT).show();
                    return false;
                }

                experienceList.add(bean);
            }

            return experienceList != null && experienceList.size() > 0;

        } else {

            return true;
        }


    }


    private void setUpViews(boolean isProfileViewVisible, boolean isQualificationViewVisible, boolean isExperienceViewVisible, boolean isEmergecyViewVisible
            , boolean isStaffAchivementViewVisible, boolean isPhysicianViewVisible, boolean isFeedbackViewVisible) {


        layoutPtofileInfo.setVisibility(isProfileViewVisible ? View.VISIBLE : View.GONE);
        layoutQualification.setVisibility(isQualificationViewVisible ? View.VISIBLE : View.GONE);
        layoutExperience.setVisibility(isExperienceViewVisible ? View.VISIBLE : View.GONE);
        layoutEmergencyContact.setVisibility(isEmergecyViewVisible ? View.VISIBLE : View.GONE);
        layoutAchivements.setVisibility(isStaffAchivementViewVisible ? View.VISIBLE : View.GONE);
        layoutPhysicianContact.setVisibility(isPhysicianViewVisible ? View.VISIBLE : View.GONE);
        layoutFeedback.setVisibility(isFeedbackViewVisible ? View.VISIBLE : View.GONE);

        if (isProfileViewVisible) {
            txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_up), null);
        } else {
            txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
        }

        if (isQualificationViewVisible) {
            txtQualification.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_up), null);
        } else {
            txtQualification.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
        }

        if (isExperienceViewVisible) {
            txtExperience.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_up), null);
        } else {
            txtExperience.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
        }

        if (isEmergecyViewVisible) {
            txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_up), null);
        } else {
            txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
        }

        if (isStaffAchivementViewVisible) {
            txtAchivements.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_up), null);
        } else {
            txtAchivements.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
        }

        if (isPhysicianViewVisible) {
            txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_up), null);
        } else {
            txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
        }

        if (isFeedbackViewVisible) {
            textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_up), null);
        } else {
            textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddTeacherActivity.this, R.drawable.ic_arrow_down), null);
        }

        addMoreQualification.setVisibility(layoutQualification.getVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);
        addMoreExperience.setVisibility(layoutExperience.getVisibility() == View.VISIBLE ? View.VISIBLE : View.GONE);

    }


    private void selectDocument(String type) {

        documentSelectionType = type;

        final Dialog dialog = new Dialog(AddTeacherActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setLayout(AdminApp.getInstance().getScreenWidth(AddTeacherActivity.this) / 2, LinearLayoutCompat.LayoutParams.WRAP_CONTENT);
        }
        dialog.setCancelable(true);
        dialog.show();

        Button gallery = (Button) dialog.findViewById(R.id.bGallery);
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                Intent intent = new Intent(AddTeacherActivity.this, FileSelectionActivity.class);
                startActivityForResult(intent, App_Constants.FILE_SELECT);

            }
        });

        Button camera = (Button) dialog.findViewById(R.id.bCamera);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                cameraAttachmentIntent();

            }
        });

    }

    public void cameraAttachmentIntent() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, App_Constants.CAMERA_ATTACHMENT_RESULT_CODE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        super.onBackPressed();
    }

    public String MapImageUrl(int X, int Y, double lat, double lon) {
        String url = "https://maps.googleapis.com/maps/api/staticmap?zoom=15&size=" + X + "x" + Y + "&maptype=roadmap&markers=color:red%7Clabel:R%7C" + lat + "," + lon + "&sensor=false";
        Log.e("MAP URL ", "" + url);
        return url;
    }

    public void cameraIntent() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 0);
    }

    private void setDate(final EditText editText) {

        final Calendar mCalender = Calendar.getInstance();

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub

                mCalender.set(Calendar.YEAR, year);
                mCalender.set(Calendar.MONTH, monthOfYear);
                mCalender.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                editText.setText(sdf.format(mCalender.getTime()));

            }

        };

        DatePickerDialog dialog = new DatePickerDialog(this
                , R.style.datepickerCustom, date, mCalender.get(Calendar.YEAR), mCalender.get(Calendar.MONTH),
                mCalender.get(Calendar.DAY_OF_MONTH));

        dialog.show();

    }


    public boolean isValidEmaillId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }


    public void getLocationFromAddress(final ImageView imageView, String strAddress) {

        Geocoder coder = new Geocoder(AddTeacherActivity.this);
        List<Address> addresses;

        try {
            addresses = coder.getFromLocationName(strAddress, 5);
            if (addresses == null) {
                Toast.makeText(this, "address not found", Toast.LENGTH_SHORT).show();
                return;
            }

            if (addresses.size() == 0) {
                Toast.makeText(this, "address not found", Toast.LENGTH_SHORT).show();
                return;
            }

            Address location = addresses.get(0);
            if (location != null) {

                Glide.with(AddTeacherActivity.this)
                        .load(MapImageUrl(600, 150, location.getLatitude(), location.getLongitude()))
                        .into(imageView);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }


       /* locationCall = AdminApp.getInstance().getLocationAPI().getLatLongFromAddress(strAddress, "AIzaSyA7CA-yqCMgrxX1QleVOMq2bMcYdCwZiVk");
        locationCall.enqueue(new Callback<GetLatLngFromAddressResponse>() {
            @Override
            public void onResponse(Call<GetLatLngFromAddressResponse> call, Response<GetLatLngFromAddressResponse> response) {

                if (response.isSuccessful()) {

                    List<GetLatLngFromAddressResponse.ResultsBean> results = response.body().getResults();
                    if (results != null && results.size() > 0) {
                        GetLatLngFromAddressResponse.ResultsBean.GeometryBean geometryBean = results.get(0).getGeometry();
                        if (geometryBean != null) {
                            GetLatLngFromAddressResponse.ResultsBean.GeometryBean.LocationBean locationBean = geometryBean.getLocation();
                            if (locationBean != null) {
                                Glide.with(AddTeacherActivity.this)
                                        .load(((AddTeacherActivity) AddTeacherActivity.this).MapImageUrl(600, 150, locationBean.getLat(), locationBean.getLng()))
                                        .into(imageView);
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<GetLatLngFromAddressResponse> call, Throwable t) {

            }
        });
*/
    }

    public LatLng getLatLng(String strAddress) {

        Geocoder coder = new Geocoder(AddTeacherActivity.this);
        List<Address> addresses;

        try {
            addresses = coder.getFromLocationName(strAddress, 5);
            if (addresses == null) {
                return new LatLng(0.0, 0.0);
            }

            if (addresses.size() == 0) {
                return new LatLng(0.0, 0.0);
            }

            Address location = addresses.get(0);
            if (location != null) {

                return new LatLng(location.getLatitude(), location.getLongitude());
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            return new LatLng(0.0, 0.0);
        }

        return new LatLng(0.0, 0.0);
    }

    public void registerTeacher() {

        if (sgGender.getCheckedRadioButtonId() == R.id.rb_male)
            selectedGender = App_Constants.MALE;
        else
            selectedGender = App_Constants.FEMALE;

        if (sgMartialStatus.getCheckedRadioButtonId() == R.id.rb_single)
            selectedMartialStatus = App_Constants.SINGLE;
        else
            selectedMartialStatus = App_Constants.MARRIED;


        AddTeacherParams params = new AddTeacherParams();

        params.setProfilePic(teacherPicURL);
        params.setFirstName(etFirstName.getText().toString());
        params.setLastName(etLastName.getText().toString());
        params.setDob(DateFunction.ConvertDate(etDob.getText().toString(), "dd/MM/yyyy", "yyyy-MM-dd"));
        params.setGender(selectedGender);
        params.setMaritalStatus(selectedMartialStatus);
        params.setJoiningDate(DateFunction.ConvertDate(etJoiningDate.getText().toString(), "dd/MM/yyyy", "yyyy-MM-dd"));
        if (!etPhone.getText().toString().isEmpty())
            params.setPhoneNo("(+" + countryCode.getSelectedCountryCode() + ")" + etPhone.getText().toString());
        else
            params.setPhoneNo(etPhone.getText().toString());
        params.setEmailId(etEmail.getText().toString());
        params.setRole(etPosition.getText().toString());
        params.setDepartment(etDepartment.getText().toString());
        params.setAddress(etAddress.getText().toString());
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
        params.setUserType(App_Constants.TEACHER);
        LatLng latLng = getLatLng(etAddress.getText().toString());
        params.setGeopoint(latLng.latitude + "," + latLng.longitude);

        if (chkTransportation.isChecked()) {
            params.setRequiresTransportation("true");
            if (sgTransportation.getCheckedRadioButtonId() == R.id.rb_at_home)
                params.setPickPoint(rbAtHome.getText().toString());
            else
                params.setPickPoint(rbCommunityPoint.getText().toString());
        } else {
            params.setRequiresTransportation("false");
        }

        List<AddTeacherParams.FilesBeanXXX> finalProfileFiles = new ArrayList<>();
        if (profileFiles != null && profileFiles.size() > 0) {
            for (MultipleImageUpload.ResultBean bean : profileFiles) {
                AddTeacherParams.FilesBeanXXX model = new AddTeacherParams.FilesBeanXXX();
                model.setFileName(bean.getFileName());
                model.setFilePath(bean.getFilepath());
                finalProfileFiles.add(model);
            }
        }
        params.setFiles(finalProfileFiles);

        //ADD QUALIFICATION DATA
        AddTeacherParams.QualificationDetailsBean qualBeans = new AddTeacherParams.QualificationDetailsBean();
        qualBeans.setQualifications(QualificationList);

        List<AddTeacherParams.QualificationDetailsBean.FilesBeanXX> finalqualFiles = new ArrayList<>();
        if (qualificationFiles != null && qualificationFiles.size() > 0) {
            for (MultipleImageUpload.ResultBean bean : qualificationFiles) {
                AddTeacherParams.QualificationDetailsBean.FilesBeanXX model = new AddTeacherParams.QualificationDetailsBean.FilesBeanXX();
                model.setFileName(bean.getFileName());
                model.setFilePath(bean.getFilepath());
                finalqualFiles.add(model);
            }
        }
        qualBeans.setFiles(finalqualFiles);

        params.setQualificationDetails(qualBeans);

        //ADD EXPERIENCE DATA
        AddTeacherParams.ExperienceDetailsBean expBeans = new AddTeacherParams.ExperienceDetailsBean();
        expBeans.setExperiences(experienceList);

        List<AddTeacherParams.ExperienceDetailsBean.FilesBeanX> finalexpFiles = new ArrayList<>();
        if (experienceFiles != null && experienceFiles.size() > 0) {
            for (MultipleImageUpload.ResultBean bean : experienceFiles) {
                AddTeacherParams.ExperienceDetailsBean.FilesBeanX model = new AddTeacherParams.ExperienceDetailsBean.FilesBeanX();
                model.setFileName(bean.getFileName());
                model.setFilePath(bean.getFilepath());
                finalexpFiles.add(model);
            }
        }
        expBeans.setFiles(finalexpFiles);

        params.setExperienceDetails(expBeans);


        //ADD EMERGENCY CONTACT DATA
        AddTeacherParams.EmergencyDetailsBean ecBeans = new AddTeacherParams.EmergencyDetailsBean();
        ecBeans.setEmailId(etEcEmail.getText().toString());
        ecBeans.setAddress(etEcAddress.getText().toString());
        ecBeans.setFirstName(etEcFirstName.getText().toString());
        ecBeans.setLastName(etEcLastName.getText().toString());
        if (!etEcPhone.getText().toString().isEmpty())
            ecBeans.setPhoneNo("(+" + countryCodeEc.getSelectedCountryCode() + ")" + etEcPhone.getText().toString());
        else
            ecBeans.setPhoneNo(etEcPhone.getText().toString());
        ecBeans.setRelation(etEcRelation.getText().toString());

        List<AddTeacherParams.EmergencyDetailsBean.FilesBean> finalecFiles = new ArrayList<>();
        if (emergencyFiles != null && emergencyFiles.size() > 0) {
            for (MultipleImageUpload.ResultBean bean : emergencyFiles) {
                AddTeacherParams.EmergencyDetailsBean.FilesBean model = new AddTeacherParams.EmergencyDetailsBean.FilesBean();
                model.setFileName(bean.getFileName());
                model.setFilePath(bean.getFilepath());
                finalecFiles.add(model);
            }
        }
        ecBeans.setFiles(finalecFiles);
        params.setEmergencyDetails(ecBeans);

        AddTeacherParams.PhysicianDetailsBean physicianDetailsBean = new AddTeacherParams.PhysicianDetailsBean();
        physicianDetailsBean.setFirstName(etPciFirstName.getText().toString());
        physicianDetailsBean.setLastName(etPciLastName.getText().toString());
        if (!etPciPhoneNumber.getText().toString().isEmpty())
            physicianDetailsBean.setPhone("(+" + countryCodePci.getSelectedCountryCode() + ")" + etPciPhoneNumber.getText().toString());
        else
            physicianDetailsBean.setPhone(etPciPhoneNumber.getText().toString());
        physicianDetailsBean.setFax(etPciFaxNumber.getText().toString());
        physicianDetailsBean.setAddress(etPciAddress.getText().toString());
        params.setPhysicianDetails(physicianDetailsBean);

        AddTeacherParams.NotesBean notesBean = new AddTeacherParams.NotesBean();
        notesBean.setSchoolCounselor(etFbSchoolComment.getText().toString());
        notesBean.setTransportationCounselor(etFbTrasportationComment.getText().toString());
        params.setNotes(notesBean);

        /*List<AddTeacherParams.S.FilesBean> finalecFiles = new ArrayList<>();
        if (emergencyFiles != null && emergencyFiles.size() > 0) {
            for (MultipleImageUpload.ResultBean bean : emergencyFiles) {
                AddTeacherParams.EmergencyDetailsBean.FilesBean model = new AddTeacherParams.EmergencyDetailsBean.FilesBean();
                model.setFileName(bean.getFileName());
                model.setFileName(bean.getFileName());
                finalecFiles.add(model);
            }
        }
        ecBeans.setFiles(finalecFiles);*/

        Utility.showProgress(AddTeacherActivity.this, getString(R.string.processing));

        registerTeacherCall = AdminApp.getInstance().getApi().registerTeacher(params);
        registerTeacherCall.enqueue(new Callback<AddTeacherResponse>() {
            @Override
            public void onResponse(Call<AddTeacherResponse> call, Response<AddTeacherResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            Toast.makeText(AddTeacherActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            setBackIntent();
                        } else {
                            Toast.makeText(AddTeacherActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        APIError error = APIError.parseError(response, AddTeacherActivity.this, App_Constants.API_REGISTER_TEACHER);
                        Toast.makeText(AddTeacherActivity.this, error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(AddTeacherActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddTeacherResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(AddTeacherActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    public void updateTeacher() {

        if (sgGender.getCheckedRadioButtonId() == R.id.rb_male)
            selectedGender = App_Constants.MALE;
        else
            selectedGender = App_Constants.FEMALE;

        if (sgMartialStatus.getCheckedRadioButtonId() == R.id.rb_single)
            selectedMartialStatus = App_Constants.SINGLE;
        else
            selectedMartialStatus = App_Constants.MARRIED;


        AddTeacherParams params = new AddTeacherParams();

        params.setId(id);
        if (teacherPicURL != null)
            params.setProfilePic(teacherPicURL);
        params.setFirstName(etFirstName.getText().toString());
        params.setLastName(etLastName.getText().toString());
        params.setDob(DateFunction.ConvertDate(etDob.getText().toString(), "dd/MM/yyyy", "yyyy-MM-dd"));
        params.setGender(selectedGender);
        params.setMaritalStatus(selectedMartialStatus);
        params.setJoiningDate(DateFunction.ConvertDate(etJoiningDate.getText().toString(), "dd/MM/yyyy", "yyyy-MM-dd"));
        if (!etPhone.getText().toString().isEmpty())
            params.setPhoneNo("(+" + countryCode.getSelectedCountryCode() + ")" + etPhone.getText().toString());
        else
            params.setPhoneNo(etPhone.getText().toString());
        params.setEmailId(etEmail.getText().toString());
        params.setRole(etPosition.getText().toString());
        params.setDepartment(etDepartment.getText().toString());
        params.setAddress(etAddress.getText().toString());
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
        params.setUserType(App_Constants.TEACHER);
        LatLng latLng = getLatLng(etAddress.getText().toString());
        params.setGeopoint(latLng.latitude + "," + latLng.longitude);

        if (chkTransportation.isChecked()) {
            params.setRequiresTransportation("true");
            if (sgTransportation.getCheckedRadioButtonId() == R.id.rb_at_home)
                params.setPickPoint(rbAtHome.getText().toString());
            else
                params.setPickPoint(rbCommunityPoint.getText().toString());
        } else {
            params.setRequiresTransportation("false");
        }

        List<AddTeacherParams.FilesBeanXXX> finalProfileFiles = new ArrayList<>();
        if (profileFiles != null && profileFiles.size() > 0) {
            for (MultipleImageUpload.ResultBean bean : profileFiles) {
                AddTeacherParams.FilesBeanXXX model = new AddTeacherParams.FilesBeanXXX();
                model.setFileName(bean.getFileName());
                model.setFilePath(bean.getFilepath());
                finalProfileFiles.add(model);
            }
        }
        params.setFiles(finalProfileFiles);

        AddTeacherParams.QualificationDetailsBean qualBeans = new AddTeacherParams.QualificationDetailsBean();
        qualBeans.setQualifications(QualificationList);

        List<AddTeacherParams.QualificationDetailsBean.FilesBeanXX> finalqualFiles = new ArrayList<>();
        if (qualificationFiles != null && qualificationFiles.size() > 0) {
            for (MultipleImageUpload.ResultBean bean : qualificationFiles) {
                AddTeacherParams.QualificationDetailsBean.FilesBeanXX model = new AddTeacherParams.QualificationDetailsBean.FilesBeanXX();
                model.setFileName(bean.getFileName());
                model.setFilePath(bean.getFilepath());
                finalqualFiles.add(model);
            }
        }
        qualBeans.setFiles(finalqualFiles);
        params.setQualificationDetails(qualBeans);

        AddTeacherParams.ExperienceDetailsBean expBeans = new AddTeacherParams.ExperienceDetailsBean();
        expBeans.setExperiences(experienceList);

        List<AddTeacherParams.ExperienceDetailsBean.FilesBeanX> finalexpFiles = new ArrayList<>();
        if (experienceFiles != null && experienceFiles.size() > 0) {
            for (MultipleImageUpload.ResultBean bean : experienceFiles) {
                AddTeacherParams.ExperienceDetailsBean.FilesBeanX model = new AddTeacherParams.ExperienceDetailsBean.FilesBeanX();
                model.setFileName(bean.getFileName());
                model.setFilePath(bean.getFilepath());
                finalexpFiles.add(model);
            }
        }
        expBeans.setFiles(finalexpFiles);

        params.setExperienceDetails(expBeans);

        AddTeacherParams.EmergencyDetailsBean ecBeans = new AddTeacherParams.EmergencyDetailsBean();
        ecBeans.setEmailId(etEcEmail.getText().toString());
        ecBeans.setAddress(etEcAddress.getText().toString());
        ecBeans.setFirstName(etEcFirstName.getText().toString());
        ecBeans.setLastName(etEcLastName.getText().toString());
        if (!etEcPhone.getText().toString().isEmpty())
            ecBeans.setPhoneNo("(+" + countryCodeEc.getSelectedCountryCode() + ")" + etEcPhone.getText().toString());
        else
            ecBeans.setPhoneNo(etEcPhone.getText().toString());
        ecBeans.setRelation(etEcRelation.getText().toString());

        List<AddTeacherParams.EmergencyDetailsBean.FilesBean> finalecFiles = new ArrayList<>();
        if (emergencyFiles != null && emergencyFiles.size() > 0) {
            for (MultipleImageUpload.ResultBean bean : emergencyFiles) {
                AddTeacherParams.EmergencyDetailsBean.FilesBean model = new AddTeacherParams.EmergencyDetailsBean.FilesBean();
                model.setFileName(bean.getFileName());
                model.setFilePath(bean.getFilepath());
                finalecFiles.add(model);
            }
        }
        ecBeans.setFiles(finalecFiles);

        params.setEmergencyDetails(ecBeans);


        AddTeacherParams.PhysicianDetailsBean physicianDetailsBean = new AddTeacherParams.PhysicianDetailsBean();
        physicianDetailsBean.setFirstName(etPciFirstName.getText().toString());
        physicianDetailsBean.setLastName(etPciLastName.getText().toString());
        if (!etPciPhoneNumber.getText().toString().isEmpty())
            physicianDetailsBean.setPhone("(+" + countryCodePci.getSelectedCountryCode() + ")" + etPciPhoneNumber.getText().toString());
        else
            physicianDetailsBean.setPhone(etPciPhoneNumber.getText().toString());
        physicianDetailsBean.setFax(etPciFaxNumber.getText().toString());
        physicianDetailsBean.setAddress(etPciAddress.getText().toString());
        params.setPhysicianDetails(physicianDetailsBean);

        AddTeacherParams.NotesBean notesBean = new AddTeacherParams.NotesBean();
        notesBean.setSchoolCounselor(etFbSchoolComment.getText().toString());
        notesBean.setTransportationCounselor(etFbTrasportationComment.getText().toString());
        params.setNotes(notesBean);

        Utility.showProgress(AddTeacherActivity.this, getString(R.string.processing));

        updateTeacherCall = AdminApp.getInstance().getApi().updateTeacher(params);
        updateTeacherCall.enqueue(new Callback<AddTeacherResponse>() {
            @Override
            public void onResponse(Call<AddTeacherResponse> call, Response<AddTeacherResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            Toast.makeText(AddTeacherActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            setBackIntent();
                        } else {
                            Toast.makeText(AddTeacherActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        APIError error = APIError.parseError(response, AddTeacherActivity.this, App_Constants.API_UPDATE_TEACHER);
                        Toast.makeText(AddTeacherActivity.this, error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(AddTeacherActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddTeacherResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(AddTeacherActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void getTeacherInfo() {

        viewAnimator.setDisplayedChild(0);

        getTeacherInfoCall = AdminApp.getInstance().getApi().getTeacherInfo(emailId);
        getTeacherInfoCall.enqueue(new Callback<GetTeacherInfo>() {
            @Override
            public void onResponse(Call<GetTeacherInfo> call, Response<GetTeacherInfo> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            setValues(response.body());
                            getQRCode();
                        } else {
                            getQRCode();
                        }
                    } else {

                        APIError error = APIError.parseError(response, AddTeacherActivity.this, App_Constants.API_GET_TEACHER_INFO);
                        viewAnimator.setDisplayedChild(2);
                        txtError.setText(error.message());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    viewAnimator.setDisplayedChild(2);
                    txtError.setText(getString(R.string.somethingwrong));
                }
            }

            @Override
            public void onFailure(Call<GetTeacherInfo> call, Throwable t) {

                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    viewAnimator.setDisplayedChild(2);
                    txtError.setText(App_Constants.NO_INTERNET);
                }
            }
        });


    }

    private void getQRCode() {

        getQRCall = AdminApp.getInstance().getApi().getQrCode(AdminApp.getInstance().getAdmin().getSchoolId(), emailId, "teacher");
        getQRCall.enqueue(new Callback<GetQrCodeResponse>() {
            @Override
            public void onResponse(Call<GetQrCodeResponse> call, Response<GetQrCodeResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            if (response.body().getResult() != null && response.body().getResult().getQrcode() != null) {
                                qrFrame.setVisibility(View.VISIBLE);
                                txtQrNotAvail.setVisibility(View.GONE);
                                Glide.with(AddTeacherActivity.this)
                                        .load(AppApi.BASE_URL + response.body().getResult().getQrcode())
                                        .into(imgQrcode);
                                viewAnimator.setDisplayedChild(1);
                            } else {

                                qrFrame.setVisibility(View.VISIBLE);
                                txtQrNotAvail.setVisibility(View.VISIBLE);
                                viewAnimator.setDisplayedChild(1);
                            }
                        } else {

                            qrFrame.setVisibility(View.VISIBLE);
                            txtQrNotAvail.setVisibility(View.VISIBLE);
                            viewAnimator.setDisplayedChild(1);
                        }
                    } else {

                        APIError error = APIError.parseError(response, AddTeacherActivity.this, App_Constants.API_GET_QR);
                        viewAnimator.setDisplayedChild(2);
                        txtError.setText(error.message());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    viewAnimator.setDisplayedChild(2);
                    txtError.setText(getString(R.string.somethingwrong));
                }
            }

            @Override
            public void onFailure(Call<GetQrCodeResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    viewAnimator.setDisplayedChild(2);
                    txtError.setText(App_Constants.NO_INTERNET);
                }
            }
        });
    }


    private void setValues(GetTeacherInfo body) {


        GetTeacherInfo.ResultBean bean = body.getResult();

        if (bean != null) {

            id = bean.getId();

            teacherPicURL = bean.getProfilePic();

            btnSaNext.setText(getString(R.string.update));

            if (bean.getProfilePic() != null && !bean.getProfilePic().isEmpty()) {

                linaerCaptureImage.setVisibility(View.GONE);

                Glide.with(this)
                        .load(AppApi.BASE_URL + bean.getProfilePic())
                        .into(teacherProfilePic);
            }

            if (bean.getFirstName() != null)
                etFirstName.setText(bean.getFirstName());
            if (bean.getLastName() != null)
                etLastName.setText(bean.getLastName());
            if (bean.getDob() != null)
                etDob.setText(DateFunction.ConvertDate(bean.getDob(), "yyyy-MM-dd", "dd/MM/yyyy"));
            if (bean.getJoiningDate() != null)
                etJoiningDate.setText(DateFunction.ConvertDate(bean.getJoiningDate(), "yyyy-MM-dd", "dd/MM/yyyy"));

            if (bean.getPhoneNo() != null && !bean.getPhoneNo().isEmpty()) {
                //(+672)7894561222
                if (bean.getPhoneNo().contains(")")) {
                    String cCode = bean.getPhoneNo().substring(2, bean.getPhoneNo().lastIndexOf(")"));
                    String phoneNo = bean.getPhoneNo().substring(bean.getPhoneNo().lastIndexOf(")") + 1);
                    etPhone.setText(phoneNo);
                    countryCode.setCountryForPhoneCode(Integer.parseInt(cCode));
                } else {
                    etPhone.setText(bean.getPhoneNo());
                }
            }

            if (bean.getEmailId() != null)
                etEmail.setText(bean.getEmailId());
            if (bean.getRole() != null)
                etPosition.setText(bean.getRole());
            if (bean.getDepartment() != null)
                etDepartment.setText(bean.getDepartment());
            if (bean.getAddress() != null)
                etAddress.setText(bean.getAddress());

            if (bean.getAddress() != null && !bean.getAddress().isEmpty()) {
                getLocationFromAddress(imgAddress, bean.getAddress());
            } else {
                Glide.with(AddTeacherActivity.this)
                        .load(MapImageUrl(600, 150, 21.1702, 72.8311))
                        .into(imgAddress);
            }

            if (bean.getFiles() != null && bean.getFiles().size() > 0) {

                for (GetTeacherInfo.ResultBean.FilesBean profileFilesBean : bean.getFiles()) {
                    MultipleImageUpload.ResultBean profileFileBean = new MultipleImageUpload.ResultBean();
                    profileFileBean.setFileName(profileFilesBean.getFileName());
                    profileFileBean.setFilepath(profileFilesBean.getFilePath());
                    profileFiles.add(profileFileBean);
                }

                attachmentAdapter = new AttachmentAdapter(AddTeacherActivity.this, App_Constants.PROFILE_ATTACHMENT);
                rvProfileAttachment.setAdapter(attachmentAdapter);
                attachmentAdapter.addItem(profileFiles);
            }

            if (bean.getGender() != null) {

                if (bean.getGender().equals(App_Constants.MALE)) {
                    rbMale.setChecked(true);
                } else {
                    rbFemale.setChecked(true);
                }
            }

            if (bean.getRequiresTransportation() != null && !bean.getRequiresTransportation().isEmpty()) {

                chkTransportation.setChecked(true);

                if (bean.getRequiresTransportation().equals(rbAtHome.getText().toString()))
                    rbAtHome.setChecked(true);
                else
                    rbCommunityPoint.setChecked(true);
            }

            if (bean.getMaritalStatus() != null) {

                if (bean.getMaritalStatus().equals(App_Constants.SINGLE)) {
                    rbSingle.setChecked(true);
                } else {
                    rbMarried.setChecked(true);
                }
            }


            if (bean.getAddress() != null && !bean.getAddress().isEmpty()) {
                getLocationFromAddress(imgAddress, bean.getAddress());
            } else {
                Glide.with(this)
                        .load(MapImageUrl(600, 150, 21.1702, 72.8311))
                        .into(imgAddress);
            }

            if (bean.getQualificationDetails() != null) {

                GetTeacherInfo.ResultBean.QualificationDetailsBean qualificationDetailsBean = bean.getQualificationDetails();

                if (qualificationDetailsBean.getQualifications() != null && qualificationDetailsBean.getQualifications().size() > 0) {

                    ArrayList<QualificationModel> models = new ArrayList<>();

                    for (GetTeacherInfo.ResultBean.QualificationDetailsBean.QualificationsBean qualificationsBean : qualificationDetailsBean.getQualifications()) {

                        QualificationModel model = new QualificationModel();
                        model.setQualificationName(qualificationsBean.getQualificationName());
                        model.setUniversity(qualificationsBean.getUniversity());
                        if (qualificationsBean.getPassedYear().contains("-")) {
                            model.setValidFrom(qualificationsBean.getPassedYear().split("-")[0].trim());
                            model.setValidTo(qualificationsBean.getPassedYear().split("-")[1].trim());
                        } else {
                            model.setValidFrom(App_Constants.EMPTY);
                            model.setValidTo(App_Constants.EMPTY);
                        }
                        models.add(model);
                    }

                    if (models.size() > 0) {
                        rvQualification.setVisibility(View.VISIBLE);
                        qualificationAdapter.addItem(models);
                    }
                }

                if (qualificationDetailsBean.getFiles() != null && qualificationDetailsBean.getFiles().size() > 0) {

                    for (GetTeacherInfo.ResultBean.QualificationDetailsBean.FilesBean qualFilesBean : qualificationDetailsBean.getFiles()) {
                        MultipleImageUpload.ResultBean qualResultBean = new MultipleImageUpload.ResultBean();
                        qualResultBean.setFileName(qualFilesBean.getFileName());
                        qualResultBean.setFilepath(qualFilesBean.getFilePath());
                        qualificationFiles.add(qualResultBean);
                    }

                    attachmentAdapter = new AttachmentAdapter(AddTeacherActivity.this, App_Constants.QUALIFICATION_ATTACHMENT);
                    rvQualificationAttachment.setAdapter(attachmentAdapter);
                    attachmentAdapter.addItem(qualificationFiles);
                }

            }

            if (bean.getExperienceDetails() != null) {

                GetTeacherInfo.ResultBean.ExperienceDetailsBean experienceDetailsBean = bean.getExperienceDetails();

                if (experienceDetailsBean.getExperiences() != null && experienceDetailsBean.getExperiences().size() > 0) {

                    ArrayList<AddTeacherParams.ExperienceDetailsBean.ExperiencesBean> models = new ArrayList<>();

                    for (GetTeacherInfo.ResultBean.ExperienceDetailsBean.ExperiencesBean qualificationsBean : experienceDetailsBean.getExperiences()) {

                        AddTeacherParams.ExperienceDetailsBean.ExperiencesBean model = new AddTeacherParams.ExperienceDetailsBean.ExperiencesBean();
                        model.setAddress(qualificationsBean.getAddress());
                        model.setToYear(qualificationsBean.getToYear());
                        model.setFromYear(qualificationsBean.getFromYear());
                        model.setOrganizationName(qualificationsBean.getOrganizationName());
                        models.add(model);
                    }

                    if (models.size() > 0) {
                        rvExperience.setVisibility(View.VISIBLE);
                        experienceAdapter.addItem(models);
                    }
                }

                if (experienceDetailsBean.getFiles() != null && experienceDetailsBean.getFiles().size() > 0) {

                    for (GetTeacherInfo.ResultBean.ExperienceDetailsBean.FilesBean expFilesBean : experienceDetailsBean.getFiles()) {
                        MultipleImageUpload.ResultBean expResultBean = new MultipleImageUpload.ResultBean();
                        expResultBean.setFileName(expFilesBean.getFileName());
                        expResultBean.setFilepath(expFilesBean.getFilePath());
                        experienceFiles.add(expResultBean);
                    }

                    attachmentAdapter = new AttachmentAdapter(AddTeacherActivity.this, App_Constants.EXPERIENCE_ATTCHEMTENT);
                    rvExpAttachment.setAdapter(attachmentAdapter);
                    attachmentAdapter.addItem(experienceFiles);
                }

            }


            if (bean.getEmergencyDetails() != null) {

                GetTeacherInfo.ResultBean.EmergencyDetailsBean emergencyDeailsBean = bean.getEmergencyDetails();

                if (emergencyDeailsBean.getEmailId() != null)
                    etEcEmail.setText(emergencyDeailsBean.getEmailId());
                if (emergencyDeailsBean.getAddress() != null)
                    etEcAddress.setText(emergencyDeailsBean.getAddress());
                if (emergencyDeailsBean.getFirstName() != null)
                    etEcFirstName.setText(emergencyDeailsBean.getFirstName());
                if (emergencyDeailsBean.getLastName() != null)
                    etEcLastName.setText(emergencyDeailsBean.getLastName());

                if (emergencyDeailsBean.getPhoneNo() != null && !emergencyDeailsBean.getPhoneNo().isEmpty()) {
                    //(+672)7894561222
                    if (emergencyDeailsBean.getPhoneNo().contains(")")) {
                        String cCode = emergencyDeailsBean.getPhoneNo().substring(2, emergencyDeailsBean.getPhoneNo().lastIndexOf(")"));
                        String phoneNo = emergencyDeailsBean.getPhoneNo().substring(emergencyDeailsBean.getPhoneNo().lastIndexOf(")") + 1);
                        etEcPhone.setText(phoneNo);
                        countryCodeEc.setCountryForPhoneCode(Integer.parseInt(cCode));
                    } else {
                        etEcPhone.setText(bean.getPhoneNo());
                    }
                }


                if (emergencyDeailsBean.getRelation() != null)
                    etEcRelation.setText(emergencyDeailsBean.getRelation());

                if (emergencyDeailsBean.getAddress() != null && !emergencyDeailsBean.getAddress().isEmpty()) {
                    getLocationFromAddress(imgEcAddress, emergencyDeailsBean.getAddress());
                    etEcAddress.setText(emergencyDeailsBean.getAddress());
                } else {
                    Glide.with(this)
                            .load(MapImageUrl(600, 150, 21.1702, 72.8311)).into(imgEcAddress);
                }

                if (emergencyDeailsBean.getFiles() != null && emergencyDeailsBean.getFiles().size() > 0) {

                    for (GetTeacherInfo.ResultBean.EmergencyDetailsBean.FilesBean ecFilesBean : emergencyDeailsBean.getFiles()) {
                        MultipleImageUpload.ResultBean ecResultBean = new MultipleImageUpload.ResultBean();
                        ecResultBean.setFileName(ecFilesBean.getFileName());
                        ecResultBean.setFilepath(ecFilesBean.getFilePath());
                        emergencyFiles.add(ecResultBean);
                    }

                    attachmentAdapter = new AttachmentAdapter(AddTeacherActivity.this, App_Constants.EMERGENCY_ATTACHMENT);
                    rvEcAttachment.setAdapter(attachmentAdapter);
                    attachmentAdapter.addItem(emergencyFiles);
                }

            } else {

                Glide.with(this)
                        .load(MapImageUrl(600, 150, 21.1702, 72.8311))
                        .into(imgEcAddress);
            }

            if (bean.getPhysicianDetails() != null) {

                GetTeacherInfo.ResultBean.PhysicianDetailsBean physicianDetailsBean = bean.getPhysicianDetails();

                if (physicianDetailsBean.getFirstName() != null)
                    etPciFirstName.setText(physicianDetailsBean.getFirstName());
                if (physicianDetailsBean.getLastName() != null)
                    etPciLastName.setText(physicianDetailsBean.getLastName());
                if (physicianDetailsBean.getPhone() != null && !physicianDetailsBean.getPhone().isEmpty()) {
                    //(+672)7894561222
                    if (physicianDetailsBean.getPhone().contains(")")) {
                        String cCode = physicianDetailsBean.getPhone().substring(2, physicianDetailsBean.getPhone().lastIndexOf(")"));
                        String phoneNo = physicianDetailsBean.getPhone().substring(physicianDetailsBean.getPhone().lastIndexOf(")") + 1);
                        etPciPhoneNumber.setText(phoneNo);
                        countryCodePci.setCountryForPhoneCode(Integer.parseInt(cCode));
                    } else {
                        etPciPhoneNumber.setText(bean.getPhoneNo());
                    }
                }
                if (physicianDetailsBean.getFax() != null)
                    etPciFaxNumber.setText(physicianDetailsBean.getFax());

                if (physicianDetailsBean.getAddress() != null && !physicianDetailsBean.getAddress().isEmpty()) {
                    getLocationFromAddress(imgPciAddress, physicianDetailsBean.getAddress());
                    etPciAddress.setText(physicianDetailsBean.getAddress());
                } else {
                    Glide.with(this)
                            .load(MapImageUrl(600, 150, 21.1702, 72.8311)).into(imgPciAddress);
                }
            } else {
                Glide.with(this)
                        .load(MapImageUrl(600, 150, 21.1702, 72.8311)).into(imgPciAddress);
            }


            if (bean.getNotes() != null) {

                GetTeacherInfo.ResultBean.NotesBean notesBean = bean.getNotes();

                if (notesBean.getSchoolCounselor() != null)
                    etFbSchoolComment.setText(notesBean.getSchoolCounselor());
                if (notesBean.getTransportationCounselor() != null)
                    etFbTrasportationComment.setText(notesBean.getTransportationCounselor());

            }

        }

    }

    private void setBackIntent() {

        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        onBackPressed();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == APIError.UPDATE_TOKEN && resultCode == RESULT_OK) {

            if (data != null) {
                String apiName = data.getStringExtra("api");
                onRelaod(apiName);
            }

        } else if (requestCode == 0 && resultCode == RESULT_OK) {

            Bitmap imageBitmap = null;
            Bundle extras = data.getExtras();
            if (extras != null) {
                imageBitmap = (Bitmap) extras.get("data");
            }

            Uri tempUri = getImageUri(getApplicationContext(), imageBitmap);
            File finalFile = new File(getRealPathFromURI(tempUri));
            picturePath = finalFile.toString();

            linaerCaptureImage.setVisibility(View.GONE);
            teacherProfilePic.setImageURI(Uri.fromFile(finalFile));

            System.out.println(finalFile);

            UploadPic();

        } else if (requestCode == 1 && resultCode == RESULT_OK) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver()
                    .query(selectedImage, filePathColumn, null, null,
                            null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);

            linaerCaptureImage.setVisibility(View.GONE);
            teacherProfilePic.setImageURI(selectedImage);

            cursor.close();

            System.out.println(selectedImage);

            UploadPic();

        } else if (requestCode == App_Constants.FILE_SELECT && resultCode == RESULT_OK) {

            ArrayList<File> Files = (ArrayList<File>) data.getSerializableExtra(FileSelectionActivity.FILES_TO_UPLOAD);
            for (File file : Files) {
                String uri = file.getAbsolutePath();
                Log.e("PATH========", uri);
                if (file.toString().contains("/")) {
                    selectedFiles.add(new FileModel(file.toString().substring(file.toString().lastIndexOf("/") + 1), uri));
                }
            }

            if (selectedFiles != null && selectedFiles.size() > 0) {
                UploadDocument();
            }

        } else if (requestCode == App_Constants.CAMERA_ATTACHMENT_RESULT_CODE && resultCode == RESULT_OK) {

            Bitmap imageBitmap = null;
            Bundle extras = data.getExtras();
            if (extras != null) {
                imageBitmap = (Bitmap) extras.get("data");
            }

            Uri tempUri = getImageUri(getApplicationContext(), imageBitmap);
            File finalFile = new File(getRealPathFromURI(tempUri));
            picturePath = finalFile.toString();

            selectedFiles.add(new FileModel(picturePath.substring(picturePath.lastIndexOf("/") + 1), picturePath));

            if (selectedFiles != null && selectedFiles.size() > 0) {
                UploadDocument();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    private void UploadDocument() {

        Utility.showProgress(AddTeacherActivity.this, getString(R.string.processing));
        List<MultipartBody.Part> parts = new ArrayList<>();

        for (FileModel ss : selectedFiles) {

            File file = new File(ss.getPath());
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part body = MultipartBody.Part.createFormData("files", file.getName(), requestFile);
            parts.add(body);
        }

        attachMentCall = AdminApp.getInstance().getApi().uploadMultipleFile(parts);
        attachMentCall.enqueue(new Callback<MultipleImageUpload>() {
            @Override
            public void onResponse(Call<MultipleImageUpload> call, Response<MultipleImageUpload> response) {

                try {
                    if (response.isSuccessful()) {
                        Utility.hideProgress();
                        if (response.body().isStatus()) {
                            if (response.body().getResult() != null) {

                                attachmentAdapter = new AttachmentAdapter(AddTeacherActivity.this, documentSelectionType);

                                if (documentSelectionType.equals(App_Constants.PROFILE_ATTACHMENT)) {

                                    profileFiles.addAll(response.body().getResult());
                                    rvProfileAttachment.setAdapter(attachmentAdapter);
                                    attachmentAdapter.addItem(profileFiles);

                                } else if (documentSelectionType.equals(App_Constants.QUALIFICATION_ATTACHMENT)) {

                                    qualificationFiles.addAll(response.body().getResult());
                                    rvQualificationAttachment.setAdapter(attachmentAdapter);
                                    attachmentAdapter.addItem(qualificationFiles);

                                } else if (documentSelectionType.equals(App_Constants.EXPERIENCE_ATTCHEMTENT)) {

                                    experienceFiles.addAll(response.body().getResult());
                                    rvExpAttachment.setAdapter(attachmentAdapter);
                                    attachmentAdapter.addItem(experienceFiles);

                                } else if (documentSelectionType.equals(App_Constants.EMERGENCY_ATTACHMENT)) {

                                    emergencyFiles.addAll(response.body().getResult());
                                    rvEcAttachment.setAdapter(attachmentAdapter);
                                    attachmentAdapter.addItem(emergencyFiles);

                                } else if (documentSelectionType.equals(App_Constants.OTHER_ATTACHMENT)) {

                                    otherFiles.addAll(response.body().getResult());
                                    rvOtherAttachment.setAdapter(attachmentAdapter);
                                    attachmentAdapter.addItem(otherFiles);

                                }
                                clearLists();

                            } else {
                                Toast.makeText(AddTeacherActivity.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(AddTeacherActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        APIError error = APIError.parseError(response, AddTeacherActivity.this, App_Constants.API_MULTIPLE_FILE_UPLOAD);
                        Utility.error(AddTeacherActivity.this, error.message());
                    }
                } catch (Exception e) {
                    Utility.hideProgress();
                    e.printStackTrace();
                    Toast.makeText(AddTeacherActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<MultipleImageUpload> call, Throwable t) {
                Utility.hideProgress();
                Log.e("ERROR UPLOAD", "" + t.getMessage());
                Toast.makeText(AddTeacherActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void clearLists() {

        selectedFiles.clear();
    }

    public void removeUploadItem(MultipleImageUpload.ResultBean file, String documentSelectionType) {

        if (documentSelectionType.equals(App_Constants.PROFILE_ATTACHMENT)) {

            if (profileFiles != null && profileFiles.size() > 0) {
                profileFiles.remove(file);
            }

        } else if (documentSelectionType.equals(App_Constants.EMERGENCY_ATTACHMENT)) {

            if (emergencyFiles != null && emergencyFiles.size() > 0) {
                emergencyFiles.remove(file);
            }

        } else if (documentSelectionType.equals(App_Constants.QUALIFICATION_ATTACHMENT)) {

            if (qualificationFiles != null && qualificationFiles.size() > 0) {
                qualificationFiles.remove(file);
            }

        } else if (documentSelectionType.equals(App_Constants.EXPERIENCE_ATTCHEMTENT)) {

            if (experienceFiles != null && experienceFiles.size() > 0) {
                experienceFiles.remove(file);
            }
        }

    }

    private void UploadPic() {

        Utility.showProgress(AddTeacherActivity.this, getString(R.string.processing));

        File file = new File(picturePath);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        Call<ImageUpload> call = AdminApp.getInstance().getApi().upload(body);

        call.enqueue(new Callback<ImageUpload>() {
            @Override
            public void onResponse(Call<ImageUpload> call, Response<ImageUpload> response) {

                try {
                    if (response.isSuccessful()) {

                        if (response.body().isStatus()) {
                            if (response.body().getResult() != null) {
                                teacherPicURL = response.body().getResult().getFilepath();
                                System.out.println(teacherPicURL);
                                Utility.hideProgress();
                            } else {
                                Utility.hideProgress();
                                Toast.makeText(AddTeacherActivity.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Utility.hideProgress();
                            Toast.makeText(AddTeacherActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        APIError error = APIError.parseError(response, AddTeacherActivity.this, App_Constants.API_UPLOAD);
                        Utility.error(AddTeacherActivity.this, error.message());
                    }
                } catch (Exception e) {
                    Utility.hideProgress();
                    e.printStackTrace();
                    Toast.makeText(AddTeacherActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<ImageUpload> call, Throwable t) {
                Utility.hideProgress();
                Log.e("ERROR UPLOAD", "" + t.getMessage());
                Toast.makeText(AddTeacherActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void onRelaod(String apiName) {

        if (apiName != null) {

            if (apiName.equals(App_Constants.API_REGISTER_TEACHER)) {

                registerTeacher();

            } else if (apiName.equals(App_Constants.API_GET_TEACHER_INFO)) {

                getTeacherInfo();

            } else if (apiName.equals(App_Constants.API_UPDATE_TEACHER)) {

                updateTeacher();

            } else if (apiName.equals(App_Constants.API_UPLOAD)) {

                UploadPic();

            } else if (apiName.equals(App_Constants.API_MULTIPLE_FILE_UPLOAD)) {

                UploadDocument();

            } else if (apiName.equals(App_Constants.API_GET_QR)) {

                getQRCode();
            }
        }
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (registerTeacherCall != null)
            registerTeacherCall.cancel();

        if (updateTeacherCall != null)
            updateTeacherCall.cancel();

        if (getTeacherInfoCall != null)
            getTeacherInfoCall.cancel();

        if (locationCall != null)
            locationCall.cancel();

        if (attachMentCall != null)
            attachMentCall.cancel();

        if (getQRCall != null)
            getQRCall.cancel();
    }


}
