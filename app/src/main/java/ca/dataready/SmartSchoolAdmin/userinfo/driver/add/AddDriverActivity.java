/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.userinfo.driver.add;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.LatLng;
import com.hbb20.CountryCodePicker;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.DateFunction;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.Utilities.place.PlaceAPIAdapter;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.common.adapter.AttachmentAdapter;
import ca.dataready.SmartSchoolAdmin.fileselectoer.FileSelectionActivity;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.AddDriverModel;
import ca.dataready.SmartSchoolAdmin.server.AddDriverResponse;
import ca.dataready.SmartSchoolAdmin.server.AppApi;
import ca.dataready.SmartSchoolAdmin.server.GetAllDriverResponse;
import ca.dataready.SmartSchoolAdmin.server.GetQrCodeResponse;
import ca.dataready.SmartSchoolAdmin.server.MultipleImageUpload;
import ca.dataready.SmartSchoolAdmin.userinfo.driver.add.adapter.AddDriverAdapter;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.FileModel;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.ImageUpload;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddDriverActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    AddDriverAdapter adapter;
    @BindView(R.id.et_first_name)
    EditText etFirstName;
    @BindView(R.id.et_last_name)
    EditText etLastName;
    @BindView(R.id.et_dob)
    EditText etDob;
    @BindView(R.id.et_licence_number)
    EditText etLicenceNumber;
    @BindView(R.id.et_country)
    EditText etCountry;
    @BindView(R.id.et_issued_date)
    EditText etIssuedDate;
    @BindView(R.id.et_expiry_date)
    EditText etExpiryDate;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_Email)
    EditText etEmail;
    @BindView(R.id.et_present_address)
    AutoCompleteTextView etPresentAddress;
    @BindView(R.id.img_present_address)
    ImageView imgPresentAddress;
    @BindView(R.id.et_permanant_address)
    AutoCompleteTextView etPermanantAddress;
    @BindView(R.id.img_permenant_address)
    ImageView imgPermenantAddress;
    @BindView(R.id.driver_profile_pic)
    ImageView driverProfilePic;
    @BindView(R.id.linaer_capture_image)
    LinearLayout linaerCaptureImage;
    @BindView(R.id.btn_edit)
    Button btnEdit;
    @BindView(R.id.btn_next)
    Button btnNext;
    @BindView(R.id.layout_ptofile_info)
    CardView layoutPtofileInfo;
    @BindView(R.id.img_address)
    ImageView imgAddress;
    @BindView(R.id.btn_previous)
    Button btnPrevious;
    @BindView(R.id.btn_start_admin_process)
    Button btnStartAdminProcess;
    @BindView(R.id.layout_emergency_contact)
    CardView layoutEmergencyContact;
    @BindView(R.id.card_profile_info)
    CardView cardProfileInfo;
    @BindView(R.id.card_emergency_contact)
    CardView cardEmergencyContact;
    @BindView(R.id.txtProfile)
    TextView txtProfile;
    @BindView(R.id.txtEmergecy)
    TextView txtEmergecy;
    @BindView(R.id.et_ec_first_name)
    EditText etEcFirstName;
    @BindView(R.id.et_ec_last_name)
    EditText etEcLastName;
    @BindView(R.id.et_ec_relation)
    EditText etEcRelation;
    @BindView(R.id.et_ec_phone)
    EditText etEcPhone;
    @BindView(R.id.et_ec_email)
    EditText etEcEmail;
    @BindView(R.id.et_address)
    AutoCompleteTextView etAddress;
    @BindView(R.id.add_driver_document)
    TextView addDriverDocument;
    @BindView(R.id.rv_driver_attachment)
    RecyclerView rvDriverAttachment;
    @BindView(R.id.linear_driver_attachments)
    LinearLayout linearDriverAttachments;
    @BindView(R.id.txtPhysician)
    TextView txtPhysician;
    @BindView(R.id.card_physician_contact)
    CardView cardPhysicianContact;
    @BindView(R.id.et_pci_first_name)
    EditText etPciFirstName;
    @BindView(R.id.et_pci_last_name)
    EditText etPciLastName;
    @BindView(R.id.et_pci_phone_number)
    EditText etPciPhoneNumber;
    @BindView(R.id.et_pci_fax_number)
    EditText etPciFaxNumber;
    @BindView(R.id.et_pci_address)
    AutoCompleteTextView etPciAddress;
    @BindView(R.id.img_pci_address)
    ImageView imgPciAddress;
    @BindView(R.id.btn_pci_previous)
    Button btnPciPrevious;
    @BindView(R.id.btn_pci_next)
    Button btnPciNext;
    @BindView(R.id.layout_physician_contact)
    CardView layoutPhysicianContact;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.textFeedback)
    TextView textFeedback;
    @BindView(R.id.card_feedback)
    CardView cardFeedback;
    @BindView(R.id.layout_card_feedback)
    CardView layoutCardFeedback;
    @BindView(R.id.et_fb_trasportation_comment)
    EditText etFbTrasportationComment;
    @BindView(R.id.et_fb_school_comment)
    EditText etFbSchoolComment;
    @BindView(R.id.btn_fb_previous)
    Button btnFbPrevious;
    @BindView(R.id.btn_fb_next)
    Button btnFbNext;
    @BindView(R.id.rv_ec_attachment)
    RecyclerView rvEcAttachment;
    @BindView(R.id.txt_ec_attachment)
    TextView txtEcAttachment;
    @BindView(R.id.img_qrcode)
    ImageView imgQrcode;
    @BindView(R.id.txt_qr_not_avail)
    TextView txtQrNotAvail;
    @BindView(R.id.qr_frame)
    FrameLayout qrFrame;
    @BindView(R.id.country_code)
    CountryCodePicker countryCode;
    @BindView(R.id.country_code_pci)
    CountryCodePicker countryCodePci;
    @BindView(R.id.country_code_ec)
    CountryCodePicker countryCodeEc;
    /*  @BindView(R.id.recyclerView)
      RecyclerView recyclerView;*/
    private Call<AddDriverResponse> registerDriverCall;
    private String picturePath;
    private String driverPicURL;
    private AttachmentAdapter attachmentAdapter;
    private List<FileModel> selectedFiles = new ArrayList<>();
    private List<MultipleImageUpload.ResultBean> profileFiles = new ArrayList<>();
    private List<MultipleImageUpload.ResultBean> emergencyFiles = new ArrayList<>();
    private String documentSelectionType;
    private String driverId;
    private Call<GetQrCodeResponse> getQRCall;
    private Call<MultipleImageUpload> attachMentCall;
    private Call<ImageUpload> uploadPiccall;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_driver);
        ButterKnife.bind(this);
        setHeaderInfo();
        Init();
    }

    private void setHeaderInfo() {

        setSupportActionBar(toolbar);

        if (AdminApp.getInstance().getAdmin() != null)
            getSupportActionBar().setTitle(AdminApp.getInstance().getAdmin().getSchoolName() + " " + AdminApp.getInstance().getAdmin().getSchoolYear());
        else
            getSupportActionBar().setTitle(getString(R.string.app_name));

        getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + getString(R.string.add_driver) + "</small>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void Init() {

      /*  recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new LastItemDecoration());
        recyclerView.setNestedScrollingEnabled(false);
        adapter = new AddDriverAdapter(this);
        recyclerView.setAdapter(adapter);
        adapter.addItem(getData());*/

        rvDriverAttachment.setLayoutManager(new LinearLayoutManager(AddDriverActivity.this));
        rvDriverAttachment.addItemDecoration(new DividerItemDecoration(AddDriverActivity.this, DividerItemDecoration.VERTICAL));

        rvEcAttachment.setLayoutManager(new LinearLayoutManager(AddDriverActivity.this));
        rvEcAttachment.addItemDecoration(new DividerItemDecoration(AddDriverActivity.this, DividerItemDecoration.VERTICAL));


        if (getIntent().getExtras() != null) {

            GetAllDriverResponse.ResultBean bean = getIntent().getParcelableExtra(App_Constants.OBJECT);
            if (bean != null) {

                btnStartAdminProcess.setText(getString(R.string.update));

                driverPicURL = bean.getProfilePic();

                driverId = bean.getDriverId();

                getQrCode();

                if (bean.getProfilePic() != null && !bean.getProfilePic().isEmpty()) {

                    linaerCaptureImage.setVisibility(View.GONE);

                    Glide.with(this)
                            .load(AppApi.BASE_URL + bean.getProfilePic())
                            .into(driverProfilePic);
                }

                if (bean.getFirstName() != null)
                    etFirstName.setText(bean.getFirstName());
                if (bean.getLastName() != null)
                    etLastName.setText(bean.getLastName());
                if (bean.getDob() != null && !bean.getDob().isEmpty())
                    etDob.setText(DateFunction.ConvertDate(bean.getDob(), "yyyy-MM-dd", "dd/MM/yyyy"));
                if (bean.getCertificateNo() != null)
                    etLicenceNumber.setText(bean.getCertificateNo());
                if (bean.getCountry() != null)
                    etCountry.setText(bean.getCountry());
                if (bean.getIssuedDate() != null && !bean.getIssuedDate().isEmpty())
                    etIssuedDate.setText(DateFunction.ConvertDate(bean.getIssuedDate(), "yyyy-MM-dd", "dd/MM/yyyy"));
                if (bean.getExpiryDate() != null && !bean.getExpiryDate().isEmpty())
                    etExpiryDate.setText(DateFunction.ConvertDate(bean.getExpiryDate(), "yyyy-MM-dd", "dd/MM/yyyy"));
                if (bean.getPhoneNo() != null && !bean.getPhoneNo().isEmpty()) {
                    //(+672)7894561222
                    if (bean.getPhoneNo().contains(")")) {
                        String cCode = bean.getPhoneNo().substring(2, bean.getPhoneNo().lastIndexOf(")"));
                        String phoneNo = bean.getPhoneNo().substring(bean.getPhoneNo().lastIndexOf(")") + 1);
                        etPhone.setText(phoneNo);
                        countryCode.setCountryForPhoneCode(Integer.parseInt(cCode));
                    } else {
                        etPhone.setText(bean.getPhoneNo());
                    }
                }
                if (bean.getEmailId() != null)
                    etEmail.setText(bean.getEmailId());
                if (bean.getPresentAddress() != null)
                    etPresentAddress.setText(bean.getPresentAddress());
                if (bean.getParmanentAddress() != null)
                    etPermanantAddress.setText(bean.getParmanentAddress());

                if (bean.getFiles() != null && bean.getFiles().size() > 0) {

                    for (GetAllDriverResponse.ResultBean.FilesBean profileFilesBean : bean.getFiles()) {
                        MultipleImageUpload.ResultBean profileFileBean = new MultipleImageUpload.ResultBean();
                        profileFileBean.setFileName(profileFilesBean.getFileName());
                        profileFileBean.setFilepath(profileFilesBean.getFilePath());
                        profileFiles.add(profileFileBean);
                    }

                    attachmentAdapter = new AttachmentAdapter(AddDriverActivity.this, App_Constants.PROFILE_ATTACHMENT);
                    rvDriverAttachment.setAdapter(attachmentAdapter);
                    attachmentAdapter.addItem(profileFiles);
                }


                if (bean.getPresentAddress() != null && !bean.getPresentAddress().isEmpty()) {
                    getLocationFromAddress(imgPresentAddress, bean.getPresentAddress());
                } else {
                    Glide.with(this)
                            .load(MapImageUrl(600, 150, 21.1702, 72.8311))
                            .into(imgPresentAddress);
                }


                if (bean.getParmanentAddress() != null && !bean.getPresentAddress().isEmpty()) {
                    getLocationFromAddress(imgPermenantAddress, bean.getParmanentAddress());
                } else {
                    Glide.with(this)
                            .load(MapImageUrl(600, 150, 21.1702, 72.8311))
                            .into(imgPermenantAddress);
                }


                if (bean.getEmergencyDetails() != null) {

                    GetAllDriverResponse.ResultBean.EmergencyDetailsBean emergencyDeailsBean = bean.getEmergencyDetails();

                    if (emergencyDeailsBean.getEmailId() != null)
                        etEcEmail.setText(emergencyDeailsBean.getEmailId());
                    if (emergencyDeailsBean.getAddress() != null)
                        etAddress.setText(emergencyDeailsBean.getAddress());
                    if (emergencyDeailsBean.getFirstName() != null)
                        etEcFirstName.setText(emergencyDeailsBean.getFirstName());
                    if (emergencyDeailsBean.getLastName() != null)
                        etEcLastName.setText(emergencyDeailsBean.getLastName());
                    if (emergencyDeailsBean.getPhoneNo() != null && !emergencyDeailsBean.getPhoneNo().isEmpty()) {
                        //(+672)7894561222
                        if (emergencyDeailsBean.getPhoneNo().contains(")")) {
                            String cCode = emergencyDeailsBean.getPhoneNo().substring(2, emergencyDeailsBean.getPhoneNo().lastIndexOf(")"));
                            String phoneNo = emergencyDeailsBean.getPhoneNo().substring(emergencyDeailsBean.getPhoneNo().lastIndexOf(")") + 1);
                            etEcPhone.setText(phoneNo);
                            countryCodeEc.setCountryForPhoneCode(Integer.parseInt(cCode));
                        } else {
                            etEcPhone.setText(bean.getPhoneNo());
                        }
                    }

                    if (emergencyDeailsBean.getRelation() != null)
                        etEcRelation.setText(emergencyDeailsBean.getRelation());

                    if (emergencyDeailsBean.getAddress() != null && !emergencyDeailsBean.getAddress().isEmpty()) {
                        getLocationFromAddress(imgAddress, emergencyDeailsBean.getAddress());
                    } else {
                        Glide.with(this)
                                .load(MapImageUrl(600, 150, 21.1702, 72.8311))
                                .into(imgAddress);
                    }

                    if (emergencyDeailsBean.getFiles() != null && emergencyDeailsBean.getFiles().size() > 0) {

                        for (GetAllDriverResponse.ResultBean.EmergencyDetailsBean.FilesBean emergencyFilesBean : emergencyDeailsBean.getFiles()) {
                            MultipleImageUpload.ResultBean maResultBean = new MultipleImageUpload.ResultBean();
                            maResultBean.setFileName(emergencyFilesBean.getFileName());
                            maResultBean.setFilepath(emergencyFilesBean.getFilePath());
                            emergencyFiles.add(maResultBean);
                        }

                        attachmentAdapter = new AttachmentAdapter(AddDriverActivity.this, App_Constants.EMERGENCY_ATTACHMENT);
                        rvEcAttachment.setAdapter(attachmentAdapter);
                        attachmentAdapter.addItem(emergencyFiles);
                    }

                } else {

                    Glide.with(this)
                            .load(MapImageUrl(600, 150, 21.1702, 72.8311))
                            .into(imgAddress);
                }

                if (bean.getPhysicianDetails() != null) {

                    GetAllDriverResponse.ResultBean.PhysicianDetailsBean physicianDetailsBean = bean.getPhysicianDetails();

                    if (physicianDetailsBean.getFirstName() != null)
                        etPciFirstName.setText(physicianDetailsBean.getFirstName());
                    if (physicianDetailsBean.getLastName() != null)
                        etPciLastName.setText(physicianDetailsBean.getLastName());
                    if (physicianDetailsBean.getPhone() != null && !physicianDetailsBean.getPhone().isEmpty()) {
                        //(+672)7894561222
                        if (physicianDetailsBean.getPhone().contains(")")) {
                            String cCode = physicianDetailsBean.getPhone().substring(2, physicianDetailsBean.getPhone().lastIndexOf(")"));
                            String phoneNo = physicianDetailsBean.getPhone().substring(physicianDetailsBean.getPhone().lastIndexOf(")") + 1);
                            etPciPhoneNumber.setText(phoneNo);
                            countryCodePci.setCountryForPhoneCode(Integer.parseInt(cCode));
                        } else {
                            etPciPhoneNumber.setText(bean.getPhoneNo());
                        }
                    }
                    if (physicianDetailsBean.getFax() != null)
                        etPciFaxNumber.setText(physicianDetailsBean.getFax());

                    if (physicianDetailsBean.getAddress() != null && !physicianDetailsBean.getAddress().isEmpty()) {
                        getLocationFromAddress(imgPciAddress, physicianDetailsBean.getAddress());
                        etPciAddress.setText(physicianDetailsBean.getAddress());
                    } else {
                        Glide.with(this)
                                .load(MapImageUrl(600, 150, 21.1702, 72.8311))
                                .into(imgPciAddress);
                    }
                } else {
                    Glide.with(this)
                            .load(MapImageUrl(600, 150, 21.1702, 72.8311))
                            .into(imgPciAddress);
                }


                if (bean.getNotes() != null) {

                    GetAllDriverResponse.ResultBean.NotesBean notesBean = bean.getNotes();

                    if (notesBean.getSchoolCounselor() != null)
                        etFbSchoolComment.setText(notesBean.getSchoolCounselor());
                    if (notesBean.getTransportationCounselor() != null)
                        etFbTrasportationComment.setText(notesBean.getTransportationCounselor());

                }


            }

        } else {


            Glide.with(this)
                    .load(MapImageUrl(600, 150, 21.1702, 72.8311))
                    .into(imgPresentAddress);

            Glide.with(this)
                    .load(MapImageUrl(600, 150, 21.1702, 72.8311))
                    .into(imgPermenantAddress);

            Glide.with(this)
                    .load(MapImageUrl(600, 150, 21.1702, 72.8311))
                    .into(imgAddress);

            Glide.with(this)
                    .load(MapImageUrl(600, 150, 21.1702, 72.8311))
                    .into(imgPciAddress);
        }

        etPresentAddress.setAdapter(new PlaceAPIAdapter(getApplicationContext(), R.layout.autocomplete_list_item));
        etPresentAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String address = (String) parent.getItemAtPosition(position);
                try {
                    getLocationFromAddress(imgPresentAddress, address);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        etPermanantAddress.setAdapter(new PlaceAPIAdapter(getApplicationContext(), R.layout.autocomplete_list_item));
        etPermanantAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String address = (String) parent.getItemAtPosition(position);
                try {
                    getLocationFromAddress(imgPermenantAddress, address);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        etAddress.setAdapter(new PlaceAPIAdapter(getApplicationContext(), R.layout.autocomplete_list_item));
        etAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String address = (String) parent.getItemAtPosition(position);
                try {
                    getLocationFromAddress(imgAddress, address);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        etPciAddress.setAdapter(new PlaceAPIAdapter(getApplicationContext(), R.layout.autocomplete_list_item));
        etPciAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String address = (String) parent.getItemAtPosition(position);
                try {
                    getLocationFromAddress(imgPciAddress, address);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        /*etPresentAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().length() != 0 && editable.toString().length() % 6 == 0) {

                    getLocationFromAddress(imgPresentAddress, editable.toString());

                }
            }
        });

        etPermanantAddress.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().length() != 0 && editable.toString().length() % 6 == 0) {

                    getLocationFromAddress(imgPermenantAddress, editable.toString());

                }
            }
        });


        etAddress.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().length() != 0 && editable.toString().length() % 6 == 0) {

                    getLocationFromAddress(imgAddress, editable.toString());

                }
            }
        });

        etPciAddress.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().length() != 0 && editable.toString().length() % 6 == 0) {

                    getLocationFromAddress(imgPciAddress, editable.toString());

                }
            }
        });*/
    }

    private void getQrCode() {

        getQRCall = AdminApp.getInstance().getApi().getQrCode(AdminApp.getInstance().getAdmin().getSchoolId(), driverId, "driver");
        getQRCall.enqueue(new Callback<GetQrCodeResponse>() {
            @Override
            public void onResponse(Call<GetQrCodeResponse> call, Response<GetQrCodeResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            if (response.body().getResult() != null && response.body().getResult().getQrcode() != null) {
                                qrFrame.setVisibility(View.VISIBLE);
                                txtQrNotAvail.setVisibility(View.GONE);
                                Glide.with(AddDriverActivity.this)
                                        .load(AppApi.BASE_URL + response.body().getResult().getQrcode())
                                        .into(imgQrcode);
                            } else {

                                qrFrame.setVisibility(View.VISIBLE);
                                txtQrNotAvail.setVisibility(View.VISIBLE);
                            }
                        } else {

                            qrFrame.setVisibility(View.VISIBLE);
                            txtQrNotAvail.setVisibility(View.VISIBLE);
                        }
                    } else {

                        APIError error = APIError.parseError(response, AddDriverActivity.this, App_Constants.API_GET_QR);

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GetQrCodeResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    Utility.hideProgress();

                }
            }
        });
    }


    private ArrayList<AddDriverModel> getData() {

        ArrayList<AddDriverModel> model = new ArrayList<>();
        model.add(new AddDriverModel(0, true));
        model.add(new AddDriverModel(1, false));

        return model;
    }

    @OnClick({R.id.btn_fb_next, R.id.btn_fb_previous, R.id.btn_pci_next, R.id.btn_pci_previous, R.id.btn_start_admin_process, R.id.add_driver_document, R.id.et_dob, R.id.et_issued_date, R.id.et_expiry_date, R.id.btn_edit, R.id.card_profile_info
            , R.id.card_emergency_contact, R.id.btn_next, R.id.btn_previous, R.id.linaer_capture_image, R.id.card_physician_contact, R.id.card_feedback, R.id.txt_ec_attachment})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.btn_fb_next:

                setUpViews(false, true, false, false);

                break;

            case R.id.btn_fb_previous:

                setUpViews(false, false, true, false);

                break;

            case R.id.linaer_capture_image:

                selectPhoto();

                break;

            case R.id.add_driver_document:

                selectDocument(App_Constants.PROFILE_ATTACHMENT);

                break;

            case R.id.txt_ec_attachment:

                selectDocument(App_Constants.EMERGENCY_ATTACHMENT);

                break;

            case R.id.et_dob:

                setDate(etDob);

                break;

            case R.id.et_issued_date:

                setDate(etIssuedDate);

                break;

            case R.id.et_expiry_date:

                setDate(etExpiryDate);

                break;

            case R.id.btn_edit:

                selectPhoto();

                break;

            case R.id.card_physician_contact:

                layoutPhysicianContact.setVisibility(layoutPhysicianContact.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                if (layoutEmergencyContact.getVisibility() == View.VISIBLE) {
                    layoutEmergencyContact.setVisibility(View.GONE);
                    txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPtofileInfo.getVisibility() == View.VISIBLE) {
                    layoutPtofileInfo.setVisibility(View.GONE);
                    txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutCardFeedback.getVisibility() == View.VISIBLE) {
                    layoutCardFeedback.setVisibility(View.GONE);
                    textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPhysicianContact.getVisibility() == View.VISIBLE)
                    txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_up), null);
                else
                    txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_down), null);

                break;

            case R.id.card_profile_info:

                layoutPtofileInfo.setVisibility(layoutPtofileInfo.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                if (layoutEmergencyContact.getVisibility() == View.VISIBLE) {
                    layoutEmergencyContact.setVisibility(View.GONE);
                    txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPhysicianContact.getVisibility() == View.VISIBLE) {
                    layoutPhysicianContact.setVisibility(View.GONE);
                    txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutCardFeedback.getVisibility() == View.VISIBLE) {
                    layoutCardFeedback.setVisibility(View.GONE);
                    textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPtofileInfo.getVisibility() == View.VISIBLE)
                    txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_up), null);
                else
                    txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_down), null);

                break;

            case R.id.card_emergency_contact:

                layoutEmergencyContact.setVisibility(layoutEmergencyContact.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                if (layoutPtofileInfo.getVisibility() == View.VISIBLE) {
                    layoutPtofileInfo.setVisibility(View.GONE);
                    txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutPhysicianContact.getVisibility() == View.VISIBLE) {
                    layoutPhysicianContact.setVisibility(View.GONE);
                    txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutCardFeedback.getVisibility() == View.VISIBLE) {
                    layoutCardFeedback.setVisibility(View.GONE);
                    textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutEmergencyContact.getVisibility() == View.VISIBLE)
                    txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_up), null);
                else
                    txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_down), null);

                break;

            case R.id.card_feedback:

                layoutCardFeedback.setVisibility(layoutCardFeedback.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);


                if (layoutPtofileInfo.getVisibility() == View.VISIBLE) {
                    layoutPtofileInfo.setVisibility(View.GONE);
                    txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_down), null);
                }


                if (layoutEmergencyContact.getVisibility() == View.VISIBLE) {
                    layoutEmergencyContact.setVisibility(View.GONE);
                    txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_down), null);
                }


                if (layoutPhysicianContact.getVisibility() == View.VISIBLE) {
                    layoutPhysicianContact.setVisibility(View.GONE);
                    txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_down), null);
                }

                if (layoutCardFeedback.getVisibility() == View.VISIBLE)
                    textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_up), null);
                else
                    textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_down), null);


                break;


            case R.id.btn_pci_next:

                setUpViews(false, false, false, true);

                break;

            case R.id.btn_pci_previous:

                setUpViews(true, false, false, false);

                break;

            case R.id.btn_next:

                if (etFirstName.getText().toString().isEmpty()) {
                    etFirstName.setError(getString(R.string.field_required));
                } else if (etLastName.getText().toString().isEmpty()) {
                    etLastName.setError(getString(R.string.field_required));
                } else if (etDob.getText().toString().isEmpty()) {
                    Toast.makeText(this, getString(R.string.please_select_dob), Toast.LENGTH_SHORT).show();
                } else if (etLicenceNumber.getText().toString().isEmpty()) {
                    etLicenceNumber.setError(getString(R.string.field_required));
                } else if (etCountry.getText().toString().isEmpty()) {
                    etCountry.setError(getString(R.string.field_required));
                } else if (etIssuedDate.getText().toString().isEmpty()) {
                    Toast.makeText(this, getString(R.string.please_select_issued_date), Toast.LENGTH_SHORT).show();
                } else if (etExpiryDate.getText().toString().isEmpty()) {
                    Toast.makeText(this, getString(R.string.please_select_expiry_date), Toast.LENGTH_SHORT).show();
                } else if (etPhone.getText().toString().isEmpty()) {
                    etPhone.setError(getString(R.string.field_required));
                } else if (etEmail.getText().toString().isEmpty()) {
                    etEmail.setError(getString(R.string.field_required));
                } else if (!isValidEmaillId(etEmail.getText().toString())) {
                    etEmail.setError(getString(R.string.invalid_email));
                } else if (etPresentAddress.getText().toString().isEmpty()) {
                    etPresentAddress.setError(getString(R.string.field_required));
                } else if (etPermanantAddress.getText().toString().isEmpty()) {
                    etPermanantAddress.setError(getString(R.string.field_required));
                } else {

                    setUpViews(false, true, false, false);
                }
                break;

            case R.id.btn_previous:

                setUpViews(false, false, false, true);

                break;

            case R.id.btn_start_admin_process:

                validateData();

                break;


        }
    }

    private void validateData() {

        if (!validProfileData()) {

            setUpViews(true, false, false, false);

        } else {

            if (getIntent().getExtras() != null)
                updateDriver();
            else
                registerDriver();
        }
    }

    private boolean validProfileData() {

        if (driverPicURL == null) {
            Toast.makeText(this, getString(R.string.please_upload_profile_pic), Toast.LENGTH_SHORT).show();
            return false;
        } else if (etFirstName.getText().toString().isEmpty()) {
            etFirstName.setError(getString(R.string.field_required));
            return false;
        } else if (etLastName.getText().toString().isEmpty()) {
            etLastName.setError(getString(R.string.field_required));
            return false;
        } else if (etDob.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.please_select_dob), Toast.LENGTH_SHORT).show();
            return false;
        } else if (etLicenceNumber.getText().toString().isEmpty()) {
            etLicenceNumber.setError(getString(R.string.field_required));
            return false;
        } else if (etCountry.getText().toString().isEmpty()) {
            etCountry.setError(getString(R.string.field_required));
            return false;
        } else if (etIssuedDate.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.please_select_issued_date), Toast.LENGTH_SHORT).show();
            return false;
        } else if (etExpiryDate.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.please_select_expiry_date), Toast.LENGTH_SHORT).show();
            return false;
        } else if (etPhone.getText().toString().isEmpty()) {
            etPhone.setError(getString(R.string.field_required));
            return false;
        } else if (etEmail.getText().toString().isEmpty()) {
            etEmail.setError(getString(R.string.field_required));
            return false;
        } else if (!isValidEmaillId(etEmail.getText().toString())) {
            etEmail.setError(getString(R.string.invalid_email));
            return false;
        } else if (etPresentAddress.getText().toString().isEmpty()) {
            etPresentAddress.setError(getString(R.string.field_required));
            return false;
        } else if (etPermanantAddress.getText().toString().isEmpty()) {
            etPermanantAddress.setError(getString(R.string.field_required));
            return false;
        }

        return true;
    }

    private void setUpViews(boolean isProfileViewVisible, boolean isEmergecyViewVisible
            , boolean isPhysicianViewVisible, boolean isFeedbackViewVisible) {


        layoutPtofileInfo.setVisibility(isProfileViewVisible ? View.VISIBLE : View.GONE);
        layoutEmergencyContact.setVisibility(isEmergecyViewVisible ? View.VISIBLE : View.GONE);
        layoutPhysicianContact.setVisibility(isPhysicianViewVisible ? View.VISIBLE : View.GONE);
        layoutCardFeedback.setVisibility(isFeedbackViewVisible ? View.VISIBLE : View.GONE);

        if (isProfileViewVisible) {
            txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_up), null);
        } else {
            txtProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_down), null);
        }

        if (isEmergecyViewVisible) {
            txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_up), null);
        } else {
            txtEmergecy.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_down), null);
        }

        if (isPhysicianViewVisible) {
            txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_up), null);
        } else {
            txtPhysician.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_down), null);
        }

        if (isFeedbackViewVisible) {
            textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_up), null);
        } else {
            textFeedback.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AddDriverActivity.this, R.drawable.ic_arrow_down), null);
        }

    }

    private void selectPhoto() {

        final Dialog dialog = new Dialog(AddDriverActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_image_picker_dialog);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setLayout(getScreenWidth(AddDriverActivity.this) / 2, LinearLayoutCompat.LayoutParams.WRAP_CONTENT);
        }
        dialog.setCancelable(true);
        dialog.show();

        Button gallery = (Button) dialog.findViewById(R.id.bGallery);
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                galleryIntent();

            }
        });

        Button camera = (Button) dialog.findViewById(R.id.bCamera);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                cameraIntent();

            }
        });

    }

    public int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    }


    private void selectDocument(String type) {

        documentSelectionType = type;

        final Dialog dialog = new Dialog(AddDriverActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setLayout(AdminApp.getInstance().getScreenWidth(AddDriverActivity.this) / 2, LinearLayoutCompat.LayoutParams.WRAP_CONTENT);
        }
        dialog.setCancelable(true);
        dialog.show();

        Button gallery = (Button) dialog.findViewById(R.id.bGallery);
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                Intent intent = new Intent(AddDriverActivity.this, FileSelectionActivity.class);
                startActivityForResult(intent, App_Constants.FILE_SELECT);

            }
        });

        Button camera = (Button) dialog.findViewById(R.id.bCamera);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                cameraAttachmentIntent();
            }
        });

    }

    public void cameraAttachmentIntent() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, App_Constants.CAMERA_ATTACHMENT_RESULT_CODE);
    }

    private void galleryIntent() {

        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, 1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        super.onBackPressed();
    }

    public String MapImageUrl(int X, int Y, double lat, double lon) {
        String url = "https://maps.googleapis.com/maps/api/staticmap?zoom=15&size=" + X + "x" + Y + "&maptype=roadmap&markers=color:red%7Clabel:R%7C" + lat + "," + lon + "&sensor=false";
        Log.e("MAP URL ", "" + url);
        return url;
    }

    public void cameraIntent() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 0);
    }

    private void setDate(final EditText editText) {

        final Calendar mCalender = Calendar.getInstance();

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub

                mCalender.set(Calendar.YEAR, year);
                mCalender.set(Calendar.MONTH, monthOfYear);
                mCalender.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                editText.setText(sdf.format(mCalender.getTime()));

            }

        };

        DatePickerDialog dialog = new DatePickerDialog(this
                , R.style.datepickerCustom, date, mCalender.get(Calendar.YEAR), mCalender.get(Calendar.MONTH),
                mCalender.get(Calendar.DAY_OF_MONTH));

        dialog.show();

    }


    public boolean isValidEmaillId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }


    public void getLocationFromAddress(final ImageView imageView, String strAddress) {

        Geocoder coder = new Geocoder(AddDriverActivity.this);
        List<Address> addresses;

        try {
            addresses = coder.getFromLocationName(strAddress, 5);
            if (addresses == null) {
                Toast.makeText(this, "address not found", Toast.LENGTH_SHORT).show();
                return;
            }

            if (addresses.size() == 0) {
                Toast.makeText(this, "address not found", Toast.LENGTH_SHORT).show();
                return;
            }

            Address location = addresses.get(0);
            if (location != null) {

                Glide.with(AddDriverActivity.this)
                        .load(((AddDriverActivity) AddDriverActivity.this).MapImageUrl(600, 150, location.getLatitude(), location.getLongitude()))
                        .into(imageView);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }


       /* Call<GetLatLngFromAddressResponse> call = AdminApp.getInstance().getLocationAPI().getLatLongFromAddress(strAddress, "AIzaSyA7CA-yqCMgrxX1QleVOMq2bMcYdCwZiVk");
        call.enqueue(new Callback<GetLatLngFromAddressResponse>() {
            @Override
            public void onResponse(Call<GetLatLngFromAddressResponse> call, Response<GetLatLngFromAddressResponse> response) {

                if (response.isSuccessful()) {

                    List<GetLatLngFromAddressResponse.ResultsBean> results = response.body().getResults();
                    if (results != null && results.size() > 0) {
                        GetLatLngFromAddressResponse.ResultsBean.GeometryBean geometryBean = results.get(0).getGeometry();
                        if (geometryBean != null) {
                            GetLatLngFromAddressResponse.ResultsBean.GeometryBean.LocationBean locationBean = geometryBean.getLocation();
                            if (locationBean != null) {
                                Glide.with(AddDriverActivity.this)
                                        .load(((AddDriverActivity) AddDriverActivity.this).MapImageUrl(600, 150, locationBean.getLat(), locationBean.getLng()))
                                        .into(imageView);
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<GetLatLngFromAddressResponse> call, Throwable t) {

            }
        });*/

    }

    public LatLng getLatLng(String strAddress) {

        Geocoder coder = new Geocoder(AddDriverActivity.this);
        List<Address> addresses;

        try {
            addresses = coder.getFromLocationName(strAddress, 5);
            if (addresses == null) {
                return new LatLng(0.0, 0.0);
            }

            if (addresses.size() == 0) {
                return new LatLng(0.0, 0.0);
            }

            Address location = addresses.get(0);
            if (location != null) {

                return new LatLng(location.getLatitude(), location.getLongitude());
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            return new LatLng(0.0, 0.0);
        }

        return new LatLng(0.0, 0.0);
    }

    public void registerDriver() {

        AddDriverParams params = new AddDriverParams();

        params.setProfilePic(driverPicURL);
        params.setFirstName(etFirstName.getText().toString());
        params.setLastName(etLastName.getText().toString());
        params.setDob(DateFunction.ConvertDate(etDob.getText().toString(), "dd/MM/yyyy", "yyyy-MM-dd"));
        params.setExpiryDate(DateFunction.ConvertDate(etExpiryDate.getText().toString(), "dd/MM/yyyy", "yyyy-MM-dd"));
        params.setIssuedDate(DateFunction.ConvertDate(etIssuedDate.getText().toString(), "dd/MM/yyyy", "yyyy-MM-dd"));
        params.setCertificateNo(etLicenceNumber.getText().toString());
        params.setCountry(etCountry.getText().toString());
        if (!etPhone.getText().toString().isEmpty())
            params.setPhoneNo("(+" + countryCode.getSelectedCountryCode() + ")" + etPhone.getText().toString());
        else
            params.setPhoneNo(etPhone.getText().toString());
        params.setEmailId(etEmail.getText().toString());

        LatLng latLng = getLatLng(etAddress.getText().toString());
        params.setGeopoint(latLng.latitude + "," + latLng.longitude);

        params.setPresentAddress(etPresentAddress.getText().toString());
        params.setParmanentAddress(etPermanantAddress.getText().toString());
        params.setDriverId(etEmail.getText().toString());
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());

        List<AddDriverParams.FilesBeanX> finalProfileFiles = new ArrayList<>();
        if (profileFiles != null && profileFiles.size() > 0) {
            for (MultipleImageUpload.ResultBean bean : profileFiles) {
                AddDriverParams.FilesBeanX model = new AddDriverParams.FilesBeanX();
                model.setFileName(bean.getFileName());
                model.setFileName(bean.getFileName());
                finalProfileFiles.add(model);
            }
        }
        params.setFiles(finalProfileFiles);

        AddDriverParams.EmergencyDetailsBean ecBeans = new AddDriverParams.EmergencyDetailsBean();
        ecBeans.setEmailId(etEcEmail.getText().toString());
        ecBeans.setAddress(etAddress.getText().toString());
        ecBeans.setFirstName(etEcFirstName.getText().toString());
        ecBeans.setLastName(etEcLastName.getText().toString());
        if (!etEcPhone.getText().toString().isEmpty())
            ecBeans.setPhoneNo("(+" + countryCodeEc.getSelectedCountryCode() + ")" + etEcPhone.getText().toString());
        else
            ecBeans.setPhoneNo(etEcPhone.getText().toString());
        ecBeans.setRelation(etEcRelation.getText().toString());

        List<AddDriverParams.EmergencyDetailsBean.FilesBean> finalEmergencyFiles = new ArrayList<>();
        if (emergencyFiles != null && emergencyFiles.size() > 0) {
            for (MultipleImageUpload.ResultBean bean : emergencyFiles) {
                AddDriverParams.EmergencyDetailsBean.FilesBean model = new AddDriverParams.EmergencyDetailsBean.FilesBean();
                model.setFileName(bean.getFileName());
                model.setFileName(bean.getFileName());
                finalEmergencyFiles.add(model);
            }
        }
        ecBeans.setFiles(finalEmergencyFiles);
        params.setEmergencyDetails(ecBeans);

        AddDriverParams.PhysicianDetailsBean physicianDetailsBean = new AddDriverParams.PhysicianDetailsBean();
        physicianDetailsBean.setFirstName(etPciFirstName.getText().toString());
        physicianDetailsBean.setLastName(etPciLastName.getText().toString());
        if (!etPciPhoneNumber.getText().toString().isEmpty())
            physicianDetailsBean.setPhone("(+" + countryCodePci.getSelectedCountryCode() + ")" + etPciPhoneNumber.getText().toString());
        else
            physicianDetailsBean.setPhone(etPciPhoneNumber.getText().toString());
        physicianDetailsBean.setFax(etPciFaxNumber.getText().toString());
        physicianDetailsBean.setAddress(etPciAddress.getText().toString());
        params.setPhysicianDetails(physicianDetailsBean);

        AddDriverParams.NotesBean notesBean = new AddDriverParams.NotesBean();
        notesBean.setSchoolCounselor(etFbSchoolComment.getText().toString());
        notesBean.setTransportationCounselor(etFbTrasportationComment.getText().toString());
        params.setNotes(notesBean);


        Utility.showProgress(AddDriverActivity.this, getString(R.string.processing));

        registerDriverCall = AdminApp.getInstance().getApi().registerDriver(params);
        registerDriverCall.enqueue(new Callback<AddDriverResponse>() {
            @Override
            public void onResponse(Call<AddDriverResponse> call, Response<AddDriverResponse> response) {
                Utility.hideProgress();
                try {

                    if (response.isSuccessful()) {

                        if (response.body().isStatus()) {
                            Toast.makeText(AddDriverActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            setBackIntent();
                        } else {
                            Toast.makeText(AddDriverActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        APIError error = APIError.parseError(response, AddDriverActivity.this, App_Constants.API_REGISTER_DRIVERS);
                        Toast.makeText(AddDriverActivity.this, error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(AddDriverActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddDriverResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(AddDriverActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void updateDriver() {

        AddDriverParams params = new AddDriverParams();

        if (driverPicURL != null)
            params.setProfilePic(driverPicURL);
        params.setFirstName(etFirstName.getText().toString());
        params.setLastName(etLastName.getText().toString());
        params.setDob(DateFunction.ConvertDate(etDob.getText().toString(), "dd/MM/yyyy", "yyyy-MM-dd"));
        params.setExpiryDate(DateFunction.ConvertDate(etExpiryDate.getText().toString(), "dd/MM/yyyy", "yyyy-MM-dd"));
        params.setIssuedDate(DateFunction.ConvertDate(etIssuedDate.getText().toString(), "dd/MM/yyyy", "yyyy-MM-dd"));
        params.setCertificateNo(etLicenceNumber.getText().toString());
        params.setCountry(etCountry.getText().toString());
        if (!etPhone.getText().toString().isEmpty())
            params.setPhoneNo("(+" + countryCode.getSelectedCountryCode() + ")" + etPhone.getText().toString());
        else
            params.setPhoneNo(etPhone.getText().toString());
        params.setEmailId(etEmail.getText().toString());
        params.setPresentAddress(etPresentAddress.getText().toString());
        params.setParmanentAddress(etPermanantAddress.getText().toString());
        params.setDriverId(etEmail.getText().toString());
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
        LatLng latLng = getLatLng(etAddress.getText().toString());
        params.setGeopoint(latLng.latitude + "," + latLng.longitude);

        List<AddDriverParams.FilesBeanX> finalProfileFiles = new ArrayList<>();
        if (profileFiles != null && profileFiles.size() > 0) {
            for (MultipleImageUpload.ResultBean bean : profileFiles) {
                AddDriverParams.FilesBeanX model = new AddDriverParams.FilesBeanX();
                model.setFileName(bean.getFileName());
                model.setFileName(bean.getFileName());
                finalProfileFiles.add(model);
            }
        }
        params.setFiles(finalProfileFiles);


        AddDriverParams.EmergencyDetailsBean ecBeans = new AddDriverParams.EmergencyDetailsBean();
        ecBeans.setEmailId(etEcEmail.getText().toString());
        ecBeans.setAddress(etAddress.getText().toString());
        ecBeans.setFirstName(etEcFirstName.getText().toString());
        ecBeans.setLastName(etEcLastName.getText().toString());
        if (!etEcPhone.getText().toString().isEmpty())
            ecBeans.setPhoneNo("(+" + countryCodeEc.getSelectedCountryCode() + ")" + etEcPhone.getText().toString());
        else
            ecBeans.setPhoneNo(etEcPhone.getText().toString());
        ecBeans.setRelation(etEcRelation.getText().toString());

        List<AddDriverParams.EmergencyDetailsBean.FilesBean> finalEmergencyFiles = new ArrayList<>();
        if (emergencyFiles != null && emergencyFiles.size() > 0) {
            for (MultipleImageUpload.ResultBean bean : emergencyFiles) {
                AddDriverParams.EmergencyDetailsBean.FilesBean model = new AddDriverParams.EmergencyDetailsBean.FilesBean();
                model.setFileName(bean.getFileName());
                model.setFileName(bean.getFileName());
                finalEmergencyFiles.add(model);
            }
        }
        ecBeans.setFiles(finalEmergencyFiles);
        params.setEmergencyDetails(ecBeans);

        AddDriverParams.PhysicianDetailsBean physicianDetailsBean = new AddDriverParams.PhysicianDetailsBean();
        physicianDetailsBean.setFirstName(etPciFirstName.getText().toString());
        physicianDetailsBean.setLastName(etPciLastName.getText().toString());
        if (!etPciPhoneNumber.getText().toString().isEmpty())
            physicianDetailsBean.setPhone("(+" + countryCodePci.getSelectedCountryCode() + ")" + etPciPhoneNumber.getText().toString());
        else
            physicianDetailsBean.setPhone(etPciPhoneNumber.getText().toString());
        physicianDetailsBean.setFax(etPciFaxNumber.getText().toString());
        physicianDetailsBean.setAddress(etPciAddress.getText().toString());
        params.setPhysicianDetails(physicianDetailsBean);

        AddDriverParams.NotesBean notesBean = new AddDriverParams.NotesBean();
        notesBean.setSchoolCounselor(etFbSchoolComment.getText().toString());
        notesBean.setTransportationCounselor(etFbTrasportationComment.getText().toString());
        params.setNotes(notesBean);


        Utility.showProgress(AddDriverActivity.this, getString(R.string.processing));

        registerDriverCall = AdminApp.getInstance().getApi().updateDriver(params);
        registerDriverCall.enqueue(new Callback<AddDriverResponse>() {
            @Override
            public void onResponse(Call<AddDriverResponse> call, Response<AddDriverResponse> response) {
                Utility.hideProgress();
                try {

                    if (response.isSuccessful()) {

                        if (response.body().isStatus()) {
                            Toast.makeText(AddDriverActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            setBackIntent();
                        } else {
                            Toast.makeText(AddDriverActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        APIError error = APIError.parseError(response, AddDriverActivity.this, App_Constants.API_UPDATE_DRIVERS);
                        Toast.makeText(AddDriverActivity.this, error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(AddDriverActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddDriverResponse> call, Throwable t) {

                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(AddDriverActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void setBackIntent() {

        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        onBackPressed();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == APIError.UPDATE_TOKEN && resultCode == RESULT_OK) {

            if (data != null) {
                String apiName = data.getStringExtra("api");
                onReload(apiName);
            }

        } else if (requestCode == 0 && resultCode == RESULT_OK) {

            Bitmap imageBitmap = null;
            Bundle extras = data.getExtras();
            if (extras != null) {
                imageBitmap = (Bitmap) extras.get("data");
            }

            Uri tempUri = getImageUri(getApplicationContext(), imageBitmap);
            File finalFile = new File(getRealPathFromURI(tempUri));
            picturePath = finalFile.toString();

            linaerCaptureImage.setVisibility(View.GONE);
            driverProfilePic.setImageURI(Uri.fromFile(finalFile));

            System.out.println(finalFile);

            UploadPic();

        } else if (requestCode == 1 && resultCode == RESULT_OK) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver()
                    .query(selectedImage, filePathColumn, null, null,
                            null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);

            linaerCaptureImage.setVisibility(View.GONE);
            driverProfilePic.setImageURI(selectedImage);

            cursor.close();

            System.out.println(selectedImage);

            UploadPic();

        } else if (requestCode == App_Constants.FILE_SELECT && resultCode == RESULT_OK) {

            ArrayList<File> Files = (ArrayList<File>) data.getSerializableExtra(FileSelectionActivity.FILES_TO_UPLOAD);
            for (File file : Files) {
                String uri = file.getAbsolutePath();
                Log.e("PATH========", uri);
                if (file.toString().contains("/")) {
                    selectedFiles.add(new FileModel(file.toString().substring(file.toString().lastIndexOf("/") + 1), uri));
                }
            }

            if (selectedFiles != null && selectedFiles.size() > 0) {
                UploadDocument();
            }

        } else if (requestCode == App_Constants.CAMERA_ATTACHMENT_RESULT_CODE && resultCode == RESULT_OK) {

            Bitmap imageBitmap = null;
            Bundle extras = data.getExtras();
            if (extras != null) {
                imageBitmap = (Bitmap) extras.get("data");
            }

            Uri tempUri = getImageUri(getApplicationContext(), imageBitmap);
            File finalFile = new File(getRealPathFromURI(tempUri));
            picturePath = finalFile.toString();

            selectedFiles.add(new FileModel(picturePath.substring(picturePath.lastIndexOf("/") + 1), picturePath));

            if (selectedFiles != null && selectedFiles.size() > 0) {
                UploadDocument();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    private void UploadDocument() {

        Utility.showProgress(AddDriverActivity.this, getString(R.string.processing));
        List<MultipartBody.Part> parts = new ArrayList<>();

        for (FileModel ss : selectedFiles) {

            File file = new File(ss.getPath());
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part body = MultipartBody.Part.createFormData("files", file.getName(), requestFile);
            parts.add(body);
        }

        attachMentCall = AdminApp.getInstance().getApi().uploadMultipleFile(parts);
        attachMentCall.enqueue(new Callback<MultipleImageUpload>() {
            @Override
            public void onResponse(Call<MultipleImageUpload> call, Response<MultipleImageUpload> response) {

                try {
                    if (response.isSuccessful()) {
                        Utility.hideProgress();
                        if (response.body().isStatus()) {
                            if (response.body().getResult() != null) {

                                attachmentAdapter = new AttachmentAdapter(AddDriverActivity.this, documentSelectionType);

                                if (documentSelectionType.equals(App_Constants.PROFILE_ATTACHMENT)) {

                                    profileFiles.addAll(response.body().getResult());
                                    rvDriverAttachment.setAdapter(attachmentAdapter);
                                    attachmentAdapter.addItem(profileFiles);

                                } else if (documentSelectionType.equals(App_Constants.EMERGENCY_ATTACHMENT)) {

                                    emergencyFiles.addAll(response.body().getResult());
                                    rvEcAttachment.setAdapter(attachmentAdapter);
                                    attachmentAdapter.addItem(emergencyFiles);

                                }
                                clearLists();
                            } else {
                                Toast.makeText(AddDriverActivity.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(AddDriverActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        APIError error = APIError.parseError(response, AddDriverActivity.this, App_Constants.API_MULTIPLE_FILE_UPLOAD);
                        Utility.error(AddDriverActivity.this, error.message());
                    }
                } catch (Exception e) {
                    Utility.hideProgress();
                    e.printStackTrace();
                    Toast.makeText(AddDriverActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<MultipleImageUpload> call, Throwable t) {
                Utility.hideProgress();
                Log.e("ERROR UPLOAD", "" + t.getMessage());
                Toast.makeText(AddDriverActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void clearLists() {

        selectedFiles.clear();
    }

    private void UploadPic() {

        Utility.showProgress(AddDriverActivity.this, getString(R.string.processing));

        File file = new File(picturePath);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        uploadPiccall = AdminApp.getInstance().getApi().upload(body);

        uploadPiccall.enqueue(new Callback<ImageUpload>() {
            @Override
            public void onResponse(Call<ImageUpload> call, Response<ImageUpload> response) {

                try {
                    if (response.isSuccessful()) {

                        if (response.body().isStatus()) {
                            if (response.body().getResult() != null) {
                                driverPicURL = response.body().getResult().getFilepath();
                                System.out.println(driverPicURL);
                                Utility.hideProgress();
                            } else {
                                Utility.hideProgress();
                                Toast.makeText(AddDriverActivity.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Utility.hideProgress();
                            Toast.makeText(AddDriverActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        APIError error = APIError.parseError(response, AddDriverActivity.this, App_Constants.API_UPLOAD);
                        Utility.error(AddDriverActivity.this, error.message());
                    }
                } catch (Exception e) {
                    Utility.hideProgress();
                    e.printStackTrace();
                    Toast.makeText(AddDriverActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<ImageUpload> call, Throwable t) {
                Utility.hideProgress();
                Log.e("ERROR UPLOAD", "" + t.getMessage());
                Toast.makeText(AddDriverActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void onReload(String apiName) {

        if (apiName != null) {

            if (apiName.equals(App_Constants.API_REGISTER_DRIVERS)) {

                registerDriver();

            } else if (apiName.equals(App_Constants.API_UPLOAD)) {

                UploadPic();

            } else if (apiName.equals(App_Constants.API_UPDATE_DRIVERS)) {

                updateDriver();

            } else if (apiName.equals(App_Constants.API_MULTIPLE_FILE_UPLOAD)) {

                UploadDocument();

            } else if (apiName.equals(App_Constants.API_GET_QR)) {

                getQrCode();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (uploadPiccall != null)
            uploadPiccall.cancel();

        if (attachMentCall != null)
            attachMentCall.cancel();

        if (registerDriverCall != null)
            registerDriverCall.cancel();

        if (getQRCall != null)
            getQRCall.cancel();
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public void removeUploadItem(MultipleImageUpload.ResultBean file, String documentSelectionType) {

        if (documentSelectionType.equals(App_Constants.PROFILE_ATTACHMENT)) {

            if (profileFiles != null && profileFiles.size() > 0) {
                profileFiles.remove(file);
            }

        } else if (documentSelectionType.equals(App_Constants.EMERGENCY_ATTACHMENT)) {

            if (emergencyFiles != null && emergencyFiles.size() > 0) {
                emergencyFiles.remove(file);
            }
        }

    }


    public static class AddDriverParams {


        /**
         * activationDate : string
         * certificateNo : string
         * city : string
         * country : string
         * deactivationDate : string
         * department : string
         * dob : string
         * driverId : string
         * driverPin : string
         * emailId : string
         * emergencyDetails : {"address":"string","emailId":"string","files":[{"fileName":"string","filePath":"string"}],"firstName":"string","lastName":"string","phoneNo":"string","relation":"string"}
         * expiryDate : string
         * files : [{"fileName":"string","filePath":"string"}]
         * firstName : string
         * gender : string
         * geopoint : string
         * issuedBy : string
         * issuedDate : string
         * lastName : string
         * parmanentAddress : string
         * phoneNo : string
         * presentAddress : string
         * profilePic : string
         * routeDetails : [{"id":"string","name":"string","routeType":"string","uniqueId":"string"}]
         * schoolId : string
         * state : string
         */

        private String activationDate;
        private String certificateNo;
        private String city;
        private String country;
        private String deactivationDate;
        private String department;
        private String dob;
        private String driverId;
        private String driverPin;
        private String emailId;
        private EmergencyDetailsBean emergencyDetails;
        private String expiryDate;
        private String firstName;
        private String gender;
        private String geopoint;
        private String issuedBy;
        private String issuedDate;
        private String lastName;
        private String parmanentAddress;
        private String phoneNo;
        private String presentAddress;
        private String profilePic;
        private String schoolId;
        private String state;
        private List<FilesBeanX> files;
        private List<RouteDetailsBean> routeDetails;
        private NotesBean notes;
        private PhysicianDetailsBean physicianDetails;


        public NotesBean getNotes() {
            return notes;
        }

        public void setNotes(NotesBean notes) {
            this.notes = notes;
        }

        public PhysicianDetailsBean getPhysicianDetails() {
            return physicianDetails;
        }

        public void setPhysicianDetails(PhysicianDetailsBean physicianDetails) {
            this.physicianDetails = physicianDetails;
        }

        public String getActivationDate() {
            return activationDate;
        }

        public void setActivationDate(String activationDate) {
            this.activationDate = activationDate;
        }

        public String getCertificateNo() {
            return certificateNo;
        }

        public void setCertificateNo(String certificateNo) {
            this.certificateNo = certificateNo;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getDeactivationDate() {
            return deactivationDate;
        }

        public void setDeactivationDate(String deactivationDate) {
            this.deactivationDate = deactivationDate;
        }

        public String getDepartment() {
            return department;
        }

        public void setDepartment(String department) {
            this.department = department;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getDriverId() {
            return driverId;
        }

        public void setDriverId(String driverId) {
            this.driverId = driverId;
        }

        public String getDriverPin() {
            return driverPin;
        }

        public void setDriverPin(String driverPin) {
            this.driverPin = driverPin;
        }

        public String getEmailId() {
            return emailId;
        }

        public void setEmailId(String emailId) {
            this.emailId = emailId;
        }

        public EmergencyDetailsBean getEmergencyDetails() {
            return emergencyDetails;
        }

        public void setEmergencyDetails(EmergencyDetailsBean emergencyDetails) {
            this.emergencyDetails = emergencyDetails;
        }

        public String getExpiryDate() {
            return expiryDate;
        }

        public void setExpiryDate(String expiryDate) {
            this.expiryDate = expiryDate;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getGeopoint() {
            return geopoint;
        }

        public void setGeopoint(String geopoint) {
            this.geopoint = geopoint;
        }

        public String getIssuedBy() {
            return issuedBy;
        }

        public void setIssuedBy(String issuedBy) {
            this.issuedBy = issuedBy;
        }

        public String getIssuedDate() {
            return issuedDate;
        }

        public void setIssuedDate(String issuedDate) {
            this.issuedDate = issuedDate;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getParmanentAddress() {
            return parmanentAddress;
        }

        public void setParmanentAddress(String parmanentAddress) {
            this.parmanentAddress = parmanentAddress;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public String getPresentAddress() {
            return presentAddress;
        }

        public void setPresentAddress(String presentAddress) {
            this.presentAddress = presentAddress;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public List<FilesBeanX> getFiles() {
            return files;
        }

        public void setFiles(List<FilesBeanX> files) {
            this.files = files;
        }

        public List<RouteDetailsBean> getRouteDetails() {
            return routeDetails;
        }

        public void setRouteDetails(List<RouteDetailsBean> routeDetails) {
            this.routeDetails = routeDetails;
        }

        public static class EmergencyDetailsBean {
            /**
             * address : string
             * emailId : string
             * files : [{"fileName":"string","filePath":"string"}]
             * firstName : string
             * lastName : string
             * phoneNo : string
             * relation : string
             */

            private String address;
            private String emailId;
            private String firstName;
            private String lastName;
            private String phoneNo;
            private String relation;
            private List<FilesBean> files;

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getEmailId() {
                return emailId;
            }

            public void setEmailId(String emailId) {
                this.emailId = emailId;
            }

            public String getFirstName() {
                return firstName;
            }

            public void setFirstName(String firstName) {
                this.firstName = firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public void setLastName(String lastName) {
                this.lastName = lastName;
            }

            public String getPhoneNo() {
                return phoneNo;
            }

            public void setPhoneNo(String phoneNo) {
                this.phoneNo = phoneNo;
            }

            public String getRelation() {
                return relation;
            }

            public void setRelation(String relation) {
                this.relation = relation;
            }

            public List<FilesBean> getFiles() {
                return files;
            }

            public void setFiles(List<FilesBean> files) {
                this.files = files;
            }

            public static class FilesBean {
                /**
                 * fileName : string
                 * filePath : string
                 */

                private String fileName;
                private String filePath;

                public String getFileName() {
                    return fileName;
                }

                public void setFileName(String fileName) {
                    this.fileName = fileName;
                }

                public String getFilePath() {
                    return filePath;
                }

                public void setFilePath(String filePath) {
                    this.filePath = filePath;
                }
            }
        }

        public static class FilesBeanX {
            /**
             * fileName : string
             * filePath : string
             */

            private String fileName;
            private String filePath;

            public String getFileName() {
                return fileName;
            }

            public void setFileName(String fileName) {
                this.fileName = fileName;
            }

            public String getFilePath() {
                return filePath;
            }

            public void setFilePath(String filePath) {
                this.filePath = filePath;
            }
        }

        public static class RouteDetailsBean {
            /**
             * id : string
             * name : string
             * routeType : string
             * uniqueId : string
             */

            private String id;
            private String name;
            private String routeType;
            private String uniqueId;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getRouteType() {
                return routeType;
            }

            public void setRouteType(String routeType) {
                this.routeType = routeType;
            }

            public String getUniqueId() {
                return uniqueId;
            }

            public void setUniqueId(String uniqueId) {
                this.uniqueId = uniqueId;
            }
        }

        public static class NotesBean {
            /**
             * schoolCounselor : string
             * transportationCounselor : string
             */

            private String schoolCounselor;
            private String transportationCounselor;

            public String getSchoolCounselor() {
                return schoolCounselor;
            }

            public void setSchoolCounselor(String schoolCounselor) {
                this.schoolCounselor = schoolCounselor;
            }

            public String getTransportationCounselor() {
                return transportationCounselor;
            }

            public void setTransportationCounselor(String transportationCounselor) {
                this.transportationCounselor = transportationCounselor;
            }
        }

        public static class PhysicianDetailsBean {
            /**
             * address : string
             * city : string
             * country : string
             * email : string
             * fax : string
             * firstName : string
             * lastName : string
             * phone : string
             * state : string
             */

            private String address;
            private String city;
            private String country;
            private String email;
            private String fax;
            private String firstName;
            private String lastName;
            private String phone;
            private String state;

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getFax() {
                return fax;
            }

            public void setFax(String fax) {
                this.fax = fax;
            }

            public String getFirstName() {
                return firstName;
            }

            public void setFirstName(String firstName) {
                this.firstName = firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public void setLastName(String lastName) {
                this.lastName = lastName;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }
        }
    }

}
