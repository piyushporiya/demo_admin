/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.userinfo.student.transportation_history.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.DateFunction;
import ca.dataready.SmartSchoolAdmin.server.StudentBordingDetails;


/**
 * Created by social_jaydeep on 07/09/17.
 */

public class StudentTransportationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<StudentBordingDetails.ResultBean> rowItems;
    private LayoutInflater mInflater;


    public StudentTransportationAdapter(Context context) {
        this.context = context;
        this.rowItems = new ArrayList<StudentBordingDetails.ResultBean>();
        mInflater = LayoutInflater.from(context);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewGroup vVideos = (ViewGroup) mInflater.inflate(R.layout.raw_trip_detail, parent, false);
        return new TripViewHolder(vVideos);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        StudentBordingDetails.ResultBean bean = rowItems.get(position);

        if(holder instanceof TripViewHolder) {

            String formmatedDate = DateFunction.ConvertDate(bean.getBoardingDate(), "yyyy-MM-dd", "dd MMM yyyy");
            ((TripViewHolder) holder).txtBoardingDate.setText(formmatedDate);

            if (bean.getScanTime() != null) {
                String time = bean.getScanTime().split(" ")[1];
                String timeInMinnHour = time.split(":")[0] + ":" + time.split(":")[1];
                ((TripViewHolder) holder).txtScanTime.setText(timeInMinnHour);
            }
            ((TripViewHolder) holder).txtOnBoard.setText(bean.getOnboard());
        }

    }

    @Override
    public int getItemCount() {
        return rowItems == null ? 0 : rowItems.size();
    }


    public void addItem(List<StudentBordingDetails.ResultBean> productDetails) {

        rowItems.addAll(productDetails);
        notifyDataSetChanged();
    }

    public void clear() {
        rowItems.clear();
        notifyDataSetChanged();
    }

    class TripViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_boarding_date)
        TextView txtBoardingDate;
        @BindView(R.id.txt_scan_time)
        TextView txtScanTime;
        @BindView(R.id.txt_on_board)
        TextView txtOnBoard;

        TripViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}


