/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.userinfo.driver.add.adapter;


import android.app.DatePickerDialog;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.server.AddDriverModel;
import ca.dataready.SmartSchoolAdmin.server.GetLatLngFromAddressResponse;
import ca.dataready.SmartSchoolAdmin.userinfo.driver.add.AddDriverActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddDriverAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<AddDriverModel> beans;
    private Calendar myCalendar = Calendar.getInstance();
    private static final int VIEW_PROFILE_INFO = 0;
    private static final int VIEW_EMERGENCY_CONTACT = 1;
    AddDriverActivity.AddDriverParams params;

    public AddDriverAdapter(Context context) {
        this.context = context;
        beans = new ArrayList<>();
        params = new AddDriverActivity.AddDriverParams();
    }

    @Override
    public int getItemViewType(int position) {
        if (beans.get(position).getType() == 0)
            return VIEW_PROFILE_INFO;
        else
            return VIEW_EMERGENCY_CONTACT;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEW_PROFILE_INFO) {
            return new ProfileInfoViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_driver_profile_info, parent, false));
        } else if (viewType == VIEW_EMERGENCY_CONTACT) {
            return new EmergencyContactViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_driver_emergency_contact, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        final AddDriverModel bean = beans.get(position);

        if (holder instanceof ProfileInfoViewHolder) {

            if (bean.isExpanded()) {
                ((ProfileInfoViewHolder) holder).layoutPtofileInfo.setVisibility(View.VISIBLE);
                ((ProfileInfoViewHolder) holder).txtHeaderTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(context, R.drawable.ic_arrow_up), null);
            } else {
                ((ProfileInfoViewHolder) holder).layoutPtofileInfo.setVisibility(View.GONE);
                ((ProfileInfoViewHolder) holder).txtHeaderTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(context, R.drawable.ic_arrow_down), null);
            }

            Glide.with(context)
                    .load(((AddDriverActivity) context).MapImageUrl(600, 150, 21.1702, 72.8311))
                    .into(((ProfileInfoViewHolder) holder).imgPresentAddress);

            Glide.with(context)
                    .load(((AddDriverActivity) context).MapImageUrl(600, 150, 21.1702, 72.8311))
                    .into(((ProfileInfoViewHolder) holder).imgPermenantAddress);

          /*  ((ProfileInfoViewHolder) holder).txtHeader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    bean.setExpanded(!bean.isExpanded());

                    if (bean.isExpanded()) {
                        ((ProfileInfoViewHolder) holder).layoutPtofileInfo.setVisibility(View.VISIBLE);
                        ((ProfileInfoViewHolder) holder).txtHeaderTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(context, R.drawable.ic_arrow_up), null);
                    } else {
                        ((ProfileInfoViewHolder) holder).layoutPtofileInfo.setVisibility(View.GONE);
                        ((ProfileInfoViewHolder) holder).txtHeaderTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(context, R.drawable.ic_arrow_down), null);
                    }

                    makeOtherCollapsed(holder.getAdapterPosition());
                }
            });*/

            ((ProfileInfoViewHolder) holder).etPresentAddress.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {

                    if (editable.toString().length() != 0 && editable.toString().length() % 6 == 0) {

                        getLocationFromAddress(((ProfileInfoViewHolder) holder).imgPresentAddress, editable.toString());

                    }
                }
            });

            ((ProfileInfoViewHolder) holder).etPermanantAddress.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {

                    if (editable.toString().length() != 0 && editable.toString().length() % 6 == 0) {

                        getLocationFromAddress(((ProfileInfoViewHolder) holder).imgPermenantAddress, editable.toString());

                    }
                }
            });

            ((ProfileInfoViewHolder) holder).etDob.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    setDate(((ProfileInfoViewHolder) holder).etDob);
                }
            });

            ((ProfileInfoViewHolder) holder).etIssuedDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    setDate(((ProfileInfoViewHolder) holder).etIssuedDate);
                }
            });

            ((ProfileInfoViewHolder) holder).etExpiryDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    setDate(((ProfileInfoViewHolder) holder).etExpiryDate);
                }
            });

            ((ProfileInfoViewHolder) holder).btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ((AddDriverActivity)context).cameraIntent();
                }
            });


            ((ProfileInfoViewHolder) holder).btnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (((ProfileInfoViewHolder) holder).etFirstName.getText().toString().isEmpty()) {
                        ((ProfileInfoViewHolder) holder).etFirstName.setError(context.getString(R.string.field_required));
                    } else if (((ProfileInfoViewHolder) holder).etLastName.getText().toString().isEmpty()) {
                        ((ProfileInfoViewHolder) holder).etLastName.setError(context.getString(R.string.field_required));
                    } else if (((ProfileInfoViewHolder) holder).etDob.getText().toString().isEmpty()) {
                        Toast.makeText(context, context.getString(R.string.please_select_dob), Toast.LENGTH_SHORT).show();
                    } else if (((ProfileInfoViewHolder) holder).etLicenceNumber.getText().toString().isEmpty()) {
                        ((ProfileInfoViewHolder) holder).etLicenceNumber.setError(context.getString(R.string.field_required));
                    } else if (((ProfileInfoViewHolder) holder).etCountry.getText().toString().isEmpty()) {
                        ((ProfileInfoViewHolder) holder).etCountry.setError(context.getString(R.string.field_required));
                    } else if (((ProfileInfoViewHolder) holder).etIssuedDate.getText().toString().isEmpty()) {
                        Toast.makeText(context, context.getString(R.string.please_select_issued_date), Toast.LENGTH_SHORT).show();
                    } else if (((ProfileInfoViewHolder) holder).etExpiryDate.getText().toString().isEmpty()) {
                        Toast.makeText(context, context.getString(R.string.please_select_expiry_date), Toast.LENGTH_SHORT).show();
                    } else if (((ProfileInfoViewHolder) holder).etPhone.getText().toString().isEmpty()) {
                        ((ProfileInfoViewHolder) holder).etPhone.setError(context.getString(R.string.field_required));
                    } else if (((ProfileInfoViewHolder) holder).etEmail.getText().toString().isEmpty()) {
                        ((ProfileInfoViewHolder) holder).etEmail.setError(context.getString(R.string.field_required));
                    } else if (!((AddDriverActivity) context).isValidEmaillId(((ProfileInfoViewHolder) holder).etEmail.getText().toString())) {
                        ((ProfileInfoViewHolder) holder).etEmail.setError(context.getString(R.string.invalid_email));
                    } else if (((ProfileInfoViewHolder) holder).etPresentAddress.getText().toString().isEmpty()) {
                        ((ProfileInfoViewHolder) holder).etPresentAddress.setError(context.getString(R.string.field_required));
                    } else if (((ProfileInfoViewHolder) holder).etPermanantAddress.getText().toString().isEmpty()) {
                        ((ProfileInfoViewHolder) holder).etPermanantAddress.setError(context.getString(R.string.field_required));
                    } else {
                        params.setProfilePic("");
                        params.setFirstName(((ProfileInfoViewHolder) holder).etFirstName.getText().toString());
                        params.setLastName(((ProfileInfoViewHolder) holder).etLastName.getText().toString());
                        params.setDob(((ProfileInfoViewHolder) holder).etDob.getText().toString());
                        params.setCertificateNo(((ProfileInfoViewHolder) holder).etLicenceNumber.getText().toString());
                        params.setCountry(((ProfileInfoViewHolder) holder).etCountry.getText().toString());
                        params.setIssuedDate(((ProfileInfoViewHolder) holder).etIssuedDate.getText().toString());
                        params.setExpiryDate(((ProfileInfoViewHolder) holder).etExpiryDate.getText().toString());
                        params.setPhoneNo(((ProfileInfoViewHolder) holder).etPhone.getText().toString());
                        params.setEmailId(((ProfileInfoViewHolder) holder).etEmail.getText().toString());
                        params.setPresentAddress(((ProfileInfoViewHolder) holder).etPresentAddress.getText().toString());
                        params.setParmanentAddress(((ProfileInfoViewHolder) holder).etPermanantAddress.getText().toString());
                        params.setDriverId(((ProfileInfoViewHolder) holder).etEmail.getText().toString());
                        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
                        makeOtherExpanded(holder.getAdapterPosition());

                    }
                }
            });

        } else if (holder instanceof EmergencyContactViewHolder) {

            if (bean.isExpanded()) {
                ((EmergencyContactViewHolder) holder).layoutEmergencyContact.setVisibility(View.VISIBLE);
                ((EmergencyContactViewHolder) holder).txtHeaderTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(context, R.drawable.ic_arrow_up), null);
            } else {
                ((EmergencyContactViewHolder) holder).layoutEmergencyContact.setVisibility(View.GONE);
                ((EmergencyContactViewHolder) holder).txtHeaderTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(context, R.drawable.ic_arrow_down), null);
            }

            Glide.with(context)
                    .load(((AddDriverActivity) context).MapImageUrl(600, 150, 21.1702, 72.8311))
                    .into(((EmergencyContactViewHolder) holder).imgAddress);

           /* ((EmergencyContactViewHolder) holder).txtHeader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    bean.setExpanded(!bean.isExpanded());

                    if (bean.isExpanded()) {
                        ((EmergencyContactViewHolder) holder).layoutEmergencyContact.setVisibility(View.VISIBLE);
                        ((EmergencyContactViewHolder) holder).txtHeaderTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(context, R.drawable.ic_arrow_up), null);
                    } else {
                        ((EmergencyContactViewHolder) holder).layoutEmergencyContact.setVisibility(View.GONE);
                        ((EmergencyContactViewHolder) holder).txtHeaderTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(context, R.drawable.ic_arrow_down), null);
                    }

                    makeOtherCollapsed(holder.getAdapterPosition());
                }
            });*/

            ((EmergencyContactViewHolder) holder).btnPrevious.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    makeOtherExpanded(holder.getAdapterPosition());
                }
            });

            ((EmergencyContactViewHolder) holder).btnStartAdminProcess.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                   // ((AddDriverActivity) context).registerStudent(params);
                }
            });
        }
    }

    private void setDate(final EditText editText) {

        final Calendar mCalender = Calendar.getInstance();

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub

                mCalender.set(Calendar.YEAR, year);
                mCalender.set(Calendar.MONTH, monthOfYear);
                mCalender.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                editText.setText(sdf.format(mCalender.getTime()));

            }

        };

        DatePickerDialog dialog = new DatePickerDialog(context
                , R.style.datepickerCustom, date, mCalender.get(Calendar.YEAR), mCalender.get(Calendar.MONTH),
                mCalender.get(Calendar.DAY_OF_MONTH));

        dialog.show();

    }

    public void getLocationFromAddress(final ImageView imageView, String strAddress) {

        Call<GetLatLngFromAddressResponse> call = AdminApp.getInstance().getLocationAPI().getLatLongFromAddress(strAddress, "AIzaSyA7CA-yqCMgrxX1QleVOMq2bMcYdCwZiVk");
        call.enqueue(new Callback<GetLatLngFromAddressResponse>() {
            @Override
            public void onResponse(Call<GetLatLngFromAddressResponse> call, Response<GetLatLngFromAddressResponse> response) {

                if (response.isSuccessful()) {

                    List<GetLatLngFromAddressResponse.ResultsBean> results = response.body().getResults();
                    if (results != null && results.size() > 0) {
                        GetLatLngFromAddressResponse.ResultsBean.GeometryBean geometryBean = results.get(0).getGeometry();
                        if (geometryBean != null) {
                            GetLatLngFromAddressResponse.ResultsBean.GeometryBean.LocationBean locationBean = geometryBean.getLocation();
                            if (locationBean != null) {
                                Glide.with(context)
                                        .load(((AddDriverActivity) context).MapImageUrl(600, 150, locationBean.getLat(), locationBean.getLng()))
                                        .into(imageView);
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<GetLatLngFromAddressResponse> call, Throwable t) {

            }
        });

    }

    private void makeOtherCollapsed(int adapterPosition) {

        for (int i = 0; i < beans.size(); i++) {

            if (i != adapterPosition) {
                beans.get(i).setExpanded(false);
            }
        }

        notifyDataSetChanged();
    }

    private void makeOtherExpanded(int adapterPosition) {

        for (int i = 0; i < beans.size(); i++) {

            if (i == adapterPosition) {
                beans.get(i).setExpanded(false);
            } else {
                beans.get(i).setExpanded(true);
            }
        }

        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }

    public void addItem(ArrayList<AddDriverModel> results) {

        beans.addAll(results);
        notifyDataSetChanged();
    }

    class ProfileInfoViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.img_present_address)
        ImageView imgPresentAddress;
        @BindView(R.id.img_permenant_address)
        ImageView imgPermenantAddress;
        @BindView(R.id.txtProfile)
        CardView layoutPtofileInfo;
      /*  @BindView(R.id.txt_header)
        CardView txtHeader;*/
        @BindView(R.id.btn_next)
        Button btnNext;
        @BindView(R.id.txt_header_title)
        TextView txtHeaderTitle;
        @BindView(R.id.et_first_name)
        EditText etFirstName;
        @BindView(R.id.et_last_name)
        EditText etLastName;
        @BindView(R.id.et_dob)
        EditText etDob;
        @BindView(R.id.et_licence_number)
        EditText etLicenceNumber;
        @BindView(R.id.et_country)
        EditText etCountry;
        @BindView(R.id.et_issued_date)
        EditText etIssuedDate;
        @BindView(R.id.et_expiry_date)
        EditText etExpiryDate;
        @BindView(R.id.et_phone)
        EditText etPhone;
        @BindView(R.id.et_Email)
        EditText etEmail;
        @BindView(R.id.et_present_address)
        EditText etPresentAddress;
        @BindView(R.id.et_permanant_address)
        EditText etPermanantAddress;
        @BindView(R.id.driver_profile_pic)
        ImageView driverProfilePic;
        @BindView(R.id.linaer_capture_image)
        LinearLayout linaerCaptureImage;
        @BindView(R.id.btn_edit)
        Button btnEdit;

       // R.layout.raw_driver_profile_info

        public ProfileInfoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    class EmergencyContactViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.img_address)
        ImageView imgAddress;
        @BindView(R.id.layout_emergency_contact)
        CardView layoutEmergencyContact;
        /*@BindView(R.id.txt_header)
        CardView txtHeader;*/
        @BindView(R.id.btn_previous)
        Button btnPrevious;
        @BindView(R.id.txt_header_title)
        TextView txtHeaderTitle;
        @BindView(R.id.btn_start_admin_process)
        Button btnStartAdminProcess;

        // R.layout.raw_driver_emergency_contact

        public EmergencyContactViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
