/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.userinfo.teacher.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.server.AppApi;
import ca.dataready.SmartSchoolAdmin.server.GetAllTeacherResponse;
import ca.dataready.SmartSchoolAdmin.userinfo.teacher.ITeacherFragment;

public class TeacherAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ITeacherFragment fragment;
    private ArrayList<GetAllTeacherResponse.ResultBean> beans;

    public TeacherAdapter(Context context, ITeacherFragment iDriverFragment) {
        this.context = context;
        fragment = iDriverFragment;
        beans = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_driver, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        final GetAllTeacherResponse.ResultBean bean = beans.get(position);

        if (holder instanceof ItemViewHolder) {

            // ((ItemViewHolder) holder).ivDriverImage.setImageURI(AppApi.BASE_URL + bean.getProfilePic());.

            if (bean.getProfilePic() != null && !bean.getProfilePic().isEmpty()) {
                Glide.with(context)
                        .load(AppApi.BASE_URL + bean.getProfilePic())
                        .into(((ItemViewHolder) holder).ivDriverImage);
            }


            ((ItemViewHolder) holder).txtDriverName.setText(bean.getFirstName() + " " + bean.getLastName());
            ((ItemViewHolder) holder).txtEmail.setText(bean.getEmailId());
            ((ItemViewHolder) holder).txtCall.setText(bean.getPhoneNo());

            ((ItemViewHolder) holder).imgMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    fragment.showPopUp(((ItemViewHolder) holder).imgMore, holder.getAdapterPosition(), bean);
                }
            });

        }

    }

    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }

    public void addItem(ArrayList<GetAllTeacherResponse.ResultBean> results) {

        beans.addAll(results);
        notifyDataSetChanged();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.iv_driver_image)
        CircularImageView ivDriverImage;
        @BindView(R.id.txt_driver_name)
        TextView txtDriverName;
        @BindView(R.id.img_more)
        ImageView imgMore;
        @BindView(R.id.txt_email)
        TextView txtEmail;
        @BindView(R.id.txt_call)
        TextView txtCall;
        //R.layout.raw_driver

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
