/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.userinfo.student;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.PopupMenu;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;

import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.Listeners.OnReloadListener;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.LastItemDecoration;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.GetStudentsList;
import ca.dataready.SmartSchoolAdmin.userinfo.student.adapter.StudentsListAdapter;
import ca.dataready.SmartSchoolAdmin.userinfo.student.add.AddStudentActivity;
import ca.dataready.SmartSchoolAdmin.userinfo.student.admin_process.StudentAdminProcessActivity;
import ca.dataready.SmartSchoolAdmin.userinfo.student.attendance_history.AttendanceHistoryActivity;
import ca.dataready.SmartSchoolAdmin.userinfo.student.transportation_history.StudentTransportationActivity;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.GradeList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


public class IStudentFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, OnReloadListener {


    @BindView(R.id.txt_Grade)
    TextView txtGrade;
    @BindView(R.id.grade_img)
    ImageView gradeImg;
    @BindView(R.id.linear_Grade)
    RelativeLayout linearGrade;
    @BindView(R.id.txt_Class)
    TextView txtClass;
    @BindView(R.id.class_img)
    ImageView classImg;
    @BindView(R.id.linear_Class)
    RelativeLayout linearClass;
    @BindView(R.id.et_search)
    EditText etSearch;
    @BindView(R.id.search_img)
    ImageView searchImg;
    @BindView(R.id.txt_clear_all)
    TextView txtClearAll;
    @BindView(R.id.llheader)
    LinearLayout llheader;
    @BindView(R.id.recyclerView)
    SuperRecyclerView recyclerView;
    @BindView(R.id.parent_viewAnimator)
    ViewAnimator parentViewAnimator;
    @BindView(R.id.fab_create)
    FloatingActionButton fabCreate;
    Unbinder unbinder;
    private GridLayoutManager gridLayoutManager;
    private Call<GradeList> call;
    private ArrayList<GradeList.ResultBean.GradeBean> teacherBeans;
    private ListPopupWindow listPopupWindow;
    private List<String> gradeList = new ArrayList<>();
    private List<String> classList = new ArrayList<>();
    private String gradeId;
    private String classId;
    private Call<GetStudentsList> pendingStudentsApi;
    private ArrayList<GetStudentsList.ResultBean> results;
    private StudentsListAdapter adapter;
    private String type;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_istudent, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        ((HomeActivity) getActivity()).setSubTitle(getString(R.string.student));
        ((HomeActivity) getActivity()).setOnReloadListener(this);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            gridLayoutManager = new GridLayoutManager(getContext(), 3);
        } else {
            gridLayoutManager = new GridLayoutManager(getContext(), 2);
        }

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.addItemDecoration(new LastItemDecoration());
        recyclerView.setRefreshListener(IStudentFragment.this);

        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                String text = arg0.toString().toLowerCase();
                if (text.length() == 0) {
                    recyclerView.showProgress();

                    if (txtClass.getText().toString().equals(getString(R.string.select_class_label))
                            && txtGrade.getText().toString().equals(getString(R.string.select_grade_label)))
                        getPendingStudents();
                    else
                        getConfirmStudentList();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
            }
        });

        if (savedInstanceState != null) {

            results = savedInstanceState.getParcelableArrayList(App_Constants.OBJECT);
            teacherBeans = savedInstanceState.getParcelableArrayList(App_Constants.TEACHER_CLASS_SCHEDULE_OBJECT);
            type = savedInstanceState.getString(App_Constants.STUDENT_LIST_TYPE);
            gradeId = savedInstanceState.getString(App_Constants.GRADE_ID);
            classId = savedInstanceState.getString(App_Constants.CLASS_ID);

            if (gradeId != null)
                txtGrade.setText(gradeId);
            else
                txtGrade.setText(getString(R.string.select_grade_label));

            if (classId != null)
                txtClass.setText(classId);
            else
                txtClass.setText(getString(R.string.select_class_label));


            if (results != null && results.size() > 0) {

                setData(type);

            } else {

                adapter = new StudentsListAdapter(getActivity(), IStudentFragment.this);
                recyclerView.setAdapter(adapter);
                TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                emptyView.setText(getString(R.string.no_data));
                parentViewAnimator.setDisplayedChild(1);
            }

        } else {

            Init();
        }
    }

    private void Init() {
        ApiCall();
    }


    private void ApiCall() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();
        if (entity == null)
            return;

        call = AdminApp.getInstance().getApi().getClassSchedule(entity.getSchoolId(), entity.getSchoolYear());
        call.enqueue(new Callback<GradeList>() {
            @Override
            public void onResponse(Call<GradeList> call, Response<GradeList> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            teacherBeans = response.body().getResult().getGrade();
                        } else {
                            Utility.showSnackBar(recyclerView, response.body().getMessage());
                        }

                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_TEACHER_CLASS_SCHEDULE);
                        Utility.error(getActivity(), error.message());
                        parentViewAnimator.setDisplayedChild(1);

                    }
                } catch (Exception e) {

                    if(isAdded()) {
                        Utility.showSnackBar(recyclerView, getString(R.string.somethingwrong));
                    }
                    e.printStackTrace();

                }
                getPendingStudents();
            }

            @Override
            public void onFailure(Call<GradeList> call, Throwable t) {
                if (!call.isCanceled()) {
                    getPendingStudents();
                    System.out.println(t.getMessage());
                    Utility.showSnackBar(recyclerView, App_Constants.NO_INTERNET);

                }
            }
        });
    }

    private void getPendingStudents() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();

        pendingStudentsApi = AdminApp.getInstance().getApi().getPendingStudents(entity.getSchoolId());
        pendingStudentsApi.enqueue(new Callback<GetStudentsList>() {
            @Override
            public void onResponse(Call<GetStudentsList> call, Response<GetStudentsList> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            results = response.body().getResult();
                            if (results != null && results.size() > 0) {

                                setData(App_Constants.PENDING);

                            } else {

                                results = null;
                                adapter = new StudentsListAdapter(getActivity(), IStudentFragment.this);
                                recyclerView.setAdapter(adapter);
                                TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                                emptyView.setText(getString(R.string.no_data));
                                parentViewAnimator.setDisplayedChild(1);
                            }

                        } else {

                            results = null;
                            adapter = new StudentsListAdapter(getActivity(), IStudentFragment.this);
                            recyclerView.setAdapter(adapter);
                            TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                            emptyView.setText(getString(R.string.no_data));
                            parentViewAnimator.setDisplayedChild(1);
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_PENDING_STUDENTS);
                        adapter = new StudentsListAdapter(getActivity(), IStudentFragment.this);
                        recyclerView.setAdapter(adapter);
                        TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                        emptyView.setText(error.message());
                        parentViewAnimator.setDisplayedChild(1);

                    }
                } catch (Exception e) {

                    adapter = new StudentsListAdapter(getActivity(), IStudentFragment.this);
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(getString(R.string.somethingwrong));
                    parentViewAnimator.setDisplayedChild(1);
                }

            }

            @Override
            public void onFailure(Call<GetStudentsList> call, Throwable t) {
                if (!call.isCanceled()) {
                    parentViewAnimator.setDisplayedChild(1);
                    adapter = new StudentsListAdapter(getActivity(), IStudentFragment.this);
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(App_Constants.NO_INTERNET);
                }
            }
        });

    }


    private void setData(String type) {

        this.type = type;

        adapter = new StudentsListAdapter(getActivity(), IStudentFragment.this);
        recyclerView.setAdapter(adapter);


        if (type.equals(App_Constants.PENDING)) {

            for (GetStudentsList.ResultBean bean : results) {
                bean.setPending(true);
            }

        } else if (type.equals(App_Constants.CONFIRM)) {

            for (GetStudentsList.ResultBean bean : results) {
                bean.setPending(false);
            }
        }

        adapter.addItem(results);
        parentViewAnimator.setDisplayedChild(1);

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.search_img, R.id.linear_Grade, R.id.linear_Class, R.id.fab_create, R.id.txt_clear_all})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.search_img:
                recyclerView.showProgress();
                searchStudentApi();
                break;
            case R.id.linear_Grade:
                show_drop_down_to_select_option(linearGrade, App_Constants.GRADE);
                break;
            case R.id.linear_Class:
                show_drop_down_to_select_option(linearClass, App_Constants.CLASS);
                break;
            case R.id.fab_create:

                startActivityForResult(new Intent(getActivity(), AddStudentActivity.class), App_Constants.UPDATE_LISTING);
                if (getActivity() != null)
                    getActivity().overridePendingTransition(R.anim.enter, R.anim.leave);

                break;
            case R.id.txt_clear_all:
                clearAll();
                break;
        }
    }


    private void clearAll() {

        if(!txtClass.getText().toString().equals(getString(R.string.select_class_label))
                || !txtGrade.getText().toString().equals(getString(R.string.select_grade_label))
                || !etSearch.getText().toString().isEmpty()){

            txtClass.setText(getString(R.string.select_class_label));
            txtGrade.setText(getString(R.string.select_grade_label));
            etSearch.setText(App_Constants.EMPTY);
            getPendingStudents();

        }
    }

    private void show_drop_down_to_select_option(RelativeLayout layout, final String which) {

        if (teacherBeans != null && teacherBeans.size() > 0) {

            listPopupWindow = new ListPopupWindow(getActivity());
            if (which.equals(App_Constants.GRADE)) {

                gradeList.clear();
                for (GradeList.ResultBean.GradeBean beans : teacherBeans) {
                    if (!gradeList.contains(beans.getGradeId()))
                        gradeList.add(beans.getGradeId());
                }

                listPopupWindow.setAdapter(new ArrayAdapter(getActivity(), R.layout.list_dropdown_item, gradeList));

            } else if (which.equals(App_Constants.CLASS)) {

                classList.clear();
                for (GradeList.ResultBean.GradeBean beans : teacherBeans) {

                    if (txtGrade.getText().toString().equals(beans.getGradeId())) {
                        if (!classList.contains(beans.getClassX()))
                            for (int i = 0; i < beans.getClassX().size(); i++)
                                classList.add(beans.getClassX().get(i).getClassId());
                    }
                }

                if (classList != null && classList.size() > 0) {
                    Collections.sort(classList, new Comparator<String>() {
                        public int compare(String obj1, String obj2) {
                            // ## Ascending order
                            return obj1.compareToIgnoreCase(obj2); // To compare string values
                        }
                    });
                }

                listPopupWindow.setAdapter(new ArrayAdapter(getActivity(), R.layout.list_dropdown_item, classList));
            }

            listPopupWindow.setAnchorView(layout);
            listPopupWindow.setWidth(ListPopupWindow.WRAP_CONTENT);
            listPopupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);

            listPopupWindow.setModal(true);
            listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    if (which.equals(App_Constants.GRADE)) {
                        txtGrade.setText(gradeList.get(i));
                        gradeId = gradeList.get(i);
                        txtClass.setText(getString(R.string.select_class_label));
                    } else if (which.equals(App_Constants.CLASS)) {
                        txtClass.setText(classList.get(i));
                        classId = classList.get(i);
                        recyclerView.showProgress();
                        getConfirmStudentList();
                    }
                    listPopupWindow.dismiss();

                }
            });
            listPopupWindow.show();
        }
    }

    private void getConfirmStudentList() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();

        pendingStudentsApi = AdminApp.getInstance().getApi().getConfirmStudents(entity.getSchoolId(), gradeId, classId, entity.getSchoolYear());
        pendingStudentsApi.enqueue(new Callback<GetStudentsList>() {
            @Override
            public void onResponse(Call<GetStudentsList> call, Response<GetStudentsList> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            results = response.body().getResult();
                            if (results != null && results.size() > 0) {

                                setData(App_Constants.CONFIRM);

                            } else {

                                results = null;
                                adapter = new StudentsListAdapter(getActivity(), IStudentFragment.this);
                                recyclerView.setAdapter(adapter);
                                TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                                emptyView.setText(getString(R.string.no_data));
                                parentViewAnimator.setDisplayedChild(1);
                            }

                        } else {

                            results = null;
                            adapter = new StudentsListAdapter(getActivity(), IStudentFragment.this);
                            recyclerView.setAdapter(adapter);
                            TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                            emptyView.setText(getString(R.string.no_data));
                            parentViewAnimator.setDisplayedChild(1);
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_CONFIRM_STUDENTS);
                        adapter = new StudentsListAdapter(getActivity(), IStudentFragment.this);
                        recyclerView.setAdapter(adapter);
                        TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                        emptyView.setText(error.message());
                        parentViewAnimator.setDisplayedChild(1);

                    }
                } catch (Exception e) {

                    adapter = new StudentsListAdapter(getActivity(), IStudentFragment.this);
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(getString(R.string.somethingwrong));
                    parentViewAnimator.setDisplayedChild(1);
                }

            }

            @Override
            public void onFailure(Call<GetStudentsList> call, Throwable t) {
                if (!call.isCanceled()) {
                    parentViewAnimator.setDisplayedChild(1);
                    adapter = new StudentsListAdapter(getActivity(), IStudentFragment.this);
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(App_Constants.NO_INTERNET);
                }
            }
        });

    }


    private void searchStudentApi() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();

        pendingStudentsApi = AdminApp.getInstance().getApi().searchStudents(entity.getSchoolId(), entity.getSchoolYear(), etSearch.getText().toString(), etSearch.getText().toString());
        pendingStudentsApi.enqueue(new Callback<GetStudentsList>() {
            @Override
            public void onResponse(Call<GetStudentsList> call, Response<GetStudentsList> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            results = response.body().getResult();
                            if (results != null && results.size() > 0) {

                                setData(App_Constants.CONFIRM);

                            } else {

                                results = null;
                                adapter = new StudentsListAdapter(getActivity(), IStudentFragment.this);
                                recyclerView.setAdapter(adapter);
                                TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                                emptyView.setText(getString(R.string.no_data));
                                parentViewAnimator.setDisplayedChild(1);
                            }

                        } else {

                            results = null;
                            adapter = new StudentsListAdapter(getActivity(), IStudentFragment.this);
                            recyclerView.setAdapter(adapter);
                            TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                            emptyView.setText(getString(R.string.no_data));
                            parentViewAnimator.setDisplayedChild(1);
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_SEARCH_STUDENT);
                        adapter = new StudentsListAdapter(getActivity(), IStudentFragment.this);
                        recyclerView.setAdapter(adapter);
                        TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                        emptyView.setText(error.message());
                        parentViewAnimator.setDisplayedChild(1);

                    }
                } catch (Exception e) {

                    adapter = new StudentsListAdapter(getActivity(), IStudentFragment.this);
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(getString(R.string.somethingwrong));
                    parentViewAnimator.setDisplayedChild(1);
                }

            }

            @Override
            public void onFailure(Call<GetStudentsList> call, Throwable t) {
                if (!call.isCanceled()) {
                    parentViewAnimator.setDisplayedChild(1);
                    adapter = new StudentsListAdapter(getActivity(), IStudentFragment.this);
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(App_Constants.NO_INTERNET);
                }
            }
        });

    }

    public void showPopUp(ImageView imgMore, int position, GetStudentsList.ResultBean bean) {


        PopupMenu popup = new PopupMenu(imgMore.getContext(), imgMore);
        MenuInflater inflater = popup.getMenuInflater();

        if(bean.isPending()){
            inflater.inflate(R.menu.edit_pending_student_menu, popup.getMenu());
        }else {
            inflater.inflate(R.menu.edit_student_menu, popup.getMenu());
        }
        popup.setOnMenuItemClickListener(new CardMenuItemClickListener(position, bean));
        popup.show();
    }

    private class CardMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        private int position;
        private GetStudentsList.ResultBean bean;

        CardMenuItemClickListener(int positon, GetStudentsList.ResultBean bean) {
            this.position = positon;
            this.bean = bean;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {

                case R.id.item_edit_student_data:

                    startActivityForResult(new Intent(getActivity(), AddStudentActivity.class)
                            .putExtra(App_Constants.OBJECT, bean), App_Constants.UPDATE_LISTING);
                    if(getActivity()!=null)
                        getActivity().overridePendingTransition(R.anim.enter, R.anim.leave);

                    break;

                case R.id.item_edit_admin_data:

                    startActivityForResult(new Intent(getActivity(), StudentAdminProcessActivity.class)
                            .putExtra(App_Constants.OBJECT, bean)
                            .putExtra(App_Constants.TYPE, App_Constants.UPDATE), App_Constants.UPDATE_LISTING);
                    if(getActivity()!=null)
                        getActivity().overridePendingTransition(R.anim.enter, R.anim.leave);

                    break;

                case R.id.item_attendance_history:

                    startActivityForResult(new Intent(getActivity(), AttendanceHistoryActivity.class)
                            .putExtra(App_Constants.OBJECT, bean), App_Constants.UPDATE_LISTING);
                    if(getActivity()!=null)
                        getActivity().overridePendingTransition(R.anim.enter, R.anim.leave);


                    break;

                case R.id.item_transport_history:

                    startActivityForResult(new Intent(getActivity(), StudentTransportationActivity.class)
                            .putExtra(App_Constants.OBJECT, bean), App_Constants.UPDATE_LISTING);
                    if(getActivity()!=null)
                        getActivity().overridePendingTransition(R.anim.enter, R.anim.leave);

                    break;

                default:
            }
            return false;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == APIError.UPDATE_TOKEN && resultCode == RESULT_OK) {

            if (data != null) {
                String apiName = data.getStringExtra("api");
                onReload(apiName);
            }

        } else if (requestCode == App_Constants.UPDATE_LISTING && resultCode == RESULT_OK) {

            if (txtClass.getText().toString().equals(getString(R.string.select_class_label))
                    && txtGrade.getText().toString().equals(getString(R.string.select_grade_label)))
                getPendingStudents();
            else if (!etSearch.getText().toString().isEmpty()) {
                searchStudentApi();
            } else {
                getConfirmStudentList();
            }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onReload(String apiName) {

        if (apiName != null) {

            if (isVisible()) {
                if (apiName.equals(App_Constants.API_TEACHER_CLASS_SCHEDULE)) {
                    ApiCall();
                } else if (apiName.equals(App_Constants.API_PENDING_STUDENTS)) {
                    getPendingStudents();
                } else if (apiName.equals(App_Constants.API_CONFIRM_STUDENTS)) {
                    getConfirmStudentList();
                } else if (apiName.equals(App_Constants.API_SEARCH_STUDENT)) {
                    searchStudentApi();
                }
            }
        }
    }

    @Override
    public void onRefresh() {

        if (isVisible()) {
            if (txtClass.getText().toString().equals(getString(R.string.select_class_label))
                    && txtGrade.getText().toString().equals(getString(R.string.select_grade_label)))
                getPendingStudents();
            else if (!etSearch.getText().toString().isEmpty()) {
                searchStudentApi();
            } else
                getConfirmStudentList();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(App_Constants.OBJECT, results);
        outState.putParcelableArrayList(App_Constants.TEACHER_CLASS_SCHEDULE_OBJECT, teacherBeans);
        outState.putString(App_Constants.STUDENT_LIST_TYPE, type);
        outState.putString(App_Constants.CLASS_ID, classId);
        outState.putString(App_Constants.GRADE_ID, gradeId);

    }

    @Override
    public void onStop() {
        super.onStop();

        if (pendingStudentsApi != null)
            pendingStudentsApi.cancel();

        if(call!=null)
            call.cancel();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            gridLayoutManager = new GridLayoutManager(getContext(), 3);
        } else {
            gridLayoutManager = new GridLayoutManager(getContext(), 2);
        }
        recyclerView.setLayoutManager(gridLayoutManager);
    }
}
