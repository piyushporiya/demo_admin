/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.userinfo.student.adapter;


import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.server.AppApi;
import ca.dataready.SmartSchoolAdmin.server.GetStudentsList;
import ca.dataready.SmartSchoolAdmin.userinfo.student.IStudentFragment;

public class StudentsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private IStudentFragment fragment;
    private ArrayList<GetStudentsList.ResultBean> beans;

    public StudentsListAdapter(Context context, IStudentFragment iDriverFragment) {
        this.context = context;
        fragment = iDriverFragment;
        beans = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_pending_students, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        final GetStudentsList.ResultBean bean = beans.get(position);

        if (holder instanceof ItemViewHolder) {

            if (bean.getProfilePic() != null && !bean.getProfilePic().isEmpty()) {
                Glide.with(context)
                        .load(AppApi.BASE_URL + bean.getProfilePic())
                        .into(((ItemViewHolder) holder).ivStudentImage);
            }
            ((ItemViewHolder) holder).txtStudentName.setText(bean.getFirstName() + " " + bean.getLastName());
            ((ItemViewHolder) holder).txtStudentId.setText(context.getString(R.string.student_id) + " : " + bean.getStudentId());

            if (bean.isPending()) {

                ((ItemViewHolder) holder).txtPending.setText(context.getString(R.string.pending));
                ((ItemViewHolder) holder).txtPending.setTextColor(ContextCompat.getColor(context, R.color.dark_orange));
                ((ItemViewHolder) holder).txtPending.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(context, R.drawable.ic_people), null, null, null);

            } else {

                if (bean.getEmailId() != null && bean.getEmailId().isEmpty()) {
                    ((ItemViewHolder) holder).txtPending.setText("NA");
                } else if (bean.getEmailId() != null) {
                    ((ItemViewHolder) holder).txtPending.setText(bean.getEmailId());
                }
                ((ItemViewHolder) holder).txtPending.setTextColor(ContextCompat.getColor(context, R.color.gray_55));
                ((ItemViewHolder) holder).txtPending.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(context, R.drawable.ic_email), null, null, null);
            }

            ((ItemViewHolder) holder).imgMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    fragment.showPopUp(((ItemViewHolder) holder).imgMore,holder.getAdapterPosition(),bean);
                }
            });

        }

    }

    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }

    public void addItem(ArrayList<GetStudentsList.ResultBean> results) {

        beans.addAll(results);
        notifyDataSetChanged();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.iv_student_image)
        CircularImageView ivStudentImage;
        @BindView(R.id.txt_student_name)
        TextView txtStudentName;
        @BindView(R.id.img_more)
        ImageView imgMore;
        @BindView(R.id.txt_pending)
        TextView txtPending;
        @BindView(R.id.txt_student_id)
        TextView txtStudentId;
        //R.layout.raw_pending_students

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
