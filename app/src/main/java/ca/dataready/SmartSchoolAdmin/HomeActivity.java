/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.Views.LastItemDecoration;
import ca.dataready.SmartSchoolAdmin.acadamic.AcadamicYearFragment;
import ca.dataready.SmartSchoolAdmin.active_route.ActiveRoutesFragment;
import ca.dataready.SmartSchoolAdmin.adapter.DrawerAdapter;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.appointment.AppointmentFragment;
import ca.dataready.SmartSchoolAdmin.appointment.AppointmentMainFragment;
import ca.dataready.SmartSchoolAdmin.appointment.details.AppointmentDetailActivity;
import ca.dataready.SmartSchoolAdmin.dashboard.DashBoardFragment;
import ca.dataready.SmartSchoolAdmin.login.LoginActivity;
import ca.dataready.SmartSchoolAdmin.Listeners.OnReloadListener;
import ca.dataready.SmartSchoolAdmin.admission.driver.ADriverFragment;
import ca.dataready.SmartSchoolAdmin.admission.staff.AStaffFragment;
import ca.dataready.SmartSchoolAdmin.admission.student.AStudentFragment;
import ca.dataready.SmartSchoolAdmin.admission.teacher.ATecherFragment;
import ca.dataready.SmartSchoolAdmin.classchannel.ClassChannelFragment;
import ca.dataready.SmartSchoolAdmin.classevents.ClassEventsFragment;
import ca.dataready.SmartSchoolAdmin.grade.GradeFragment;
import ca.dataready.SmartSchoolAdmin.route.RouteFragment;
import ca.dataready.SmartSchoolAdmin.schoolchannel.SchoolChannelFragment;
import ca.dataready.SmartSchoolAdmin.schoolevents.SchoolEventsFragment;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.AppApi;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.DrawerModel;
import ca.dataready.SmartSchoolAdmin.subject.SubjectFragment;
import ca.dataready.SmartSchoolAdmin.userinfo.driver.IDriverFragment;
import ca.dataready.SmartSchoolAdmin.userinfo.driver.add.AddDriverActivity;
import ca.dataready.SmartSchoolAdmin.userinfo.staff.IStaffFragment;
import ca.dataready.SmartSchoolAdmin.userinfo.staff.add.AddStaffActivity;
import ca.dataready.SmartSchoolAdmin.userinfo.student.IStudentFragment;
import ca.dataready.SmartSchoolAdmin.userinfo.student.add.AddStudentActivity;
import ca.dataready.SmartSchoolAdmin.userinfo.teacher.ITeacherFragment;
import ca.dataready.SmartSchoolAdmin.userinfo.teacher.add.AddTeacherActivity;
import ca.dataready.SmartSchoolAdmin.viewresponse.ViewResponseFragment;

import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.APPOINTMENT;
import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.APPOINTMENT_ID;
import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.CHANNEL_TYPE;
import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.CLASS_ID;
import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.COMMUNICATION;
import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.GRADE_ID;
import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.GROUP_ID;
import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.MESSAGE_ID;
import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.MESSAGE_TYPE;
import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.NOTIFICATION_ID;
import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.PROFILE_ID;
import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.SCHOOL_ID;
import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.STUDENT_ID;

public class HomeActivity extends BaseActivity {

    @BindView(R.id.container)
    LinearLayout container;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private SlidingMenu menu;
    private ImageView menuToggle;
    OnReloadListener onReloadListener;
    String channelType, messageType;
    private String gradId, classId, studentId, profileId, schoolId, messageId, appointmentId, groupId, notificationId;

    // RelativeLayout logoutlayout;
    public static final String RELOAD = "reload_reciver";
    private DrawerAdapter drawerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setHeaderInfo();
        try {
            menu = new SlidingMenu(this);
            menu.setMode(SlidingMenu.LEFT);
            menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
            menu.setShadowWidthRes(R.dimen.shadow_width);
            menu.setShadowDrawable(R.drawable.shadow);
            menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
            menu.setFadeDegree(0.8f);
            menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
            menu.setMenu(R.layout.leftmenu);
            menu.setSlidingEnabled(true);

            if (toolbar != null) {
                toolbar.setNavigationIcon(R.drawable.ic_menu);
                toolbar.setTag(0);
            }

            View leftView = menu.getMenu();
            initLeftMenu(leftView);

        } catch (Exception e) {
            e.printStackTrace();
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(mUpdateFragmentReceiver,
                new IntentFilter(App_Constants.UPDATE_FRAGEMENT_BROADCAST));

        addFirstFragment();
        onNewIntent(getIntent());
    }

    private BroadcastReceiver mUpdateFragmentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent != null) {

                String type = intent.getStringExtra(App_Constants.TYPE);
                Fragment fragment = null;

                if (type != null && type.equals(App_Constants.APPOINTMENT)) {

                    fragment = new AppointmentMainFragment();
                }

                if (fragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.container, fragment).addToBackStack(null).commitAllowingStateLoss();
                }

            }

        }
    };

    private void setHeaderInfo() {

        toolbar.setNavigationIcon(R.drawable.ic_menu);
        setSupportActionBar(toolbar);
        setTitle();
    }

    public void setTitle() {

        CREDENTIAL.ResultBean bean = AdminApp.getInstance().getAdmin();
        if (bean != null) {
            getSupportActionBar().setTitle(bean.getSchoolName() + " " + AdminApp.getInstance().getAdmin().getSchoolYear());
        } else {
            getSupportActionBar().setTitle(getString(R.string.app_name));
        }
    }

    public void setSubTitle(String subTitle) {
        getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + subTitle + "</small>"));
    }


    private void initLeftMenu(View leftView) {

        RecyclerView recyclerView = (RecyclerView) leftView.findViewById(R.id.recyclerView);
        TextView txtTeacherName = (TextView) leftView.findViewById(R.id.name);
        TextView txtTeacherEmail = (TextView) leftView.findViewById(R.id.email);
        SimpleDraweeView teacherPic = (SimpleDraweeView) leftView.findViewById(R.id.teacher_pic);
        SimpleDraweeView schoolimage = (SimpleDraweeView) leftView.findViewById(R.id.schoolimage);

        CREDENTIAL.ResultBean bean = AdminApp.getInstance().getAdmin();
        if (bean != null) {
            txtTeacherName.setText(bean.getFirstName() + " " + bean.getLastName());
            txtTeacherEmail.setText(bean.getEmailId());
            teacherPic.setImageURI(AppApi.BASE_URL + bean.getProfilePic());
            schoolimage.setImageURI(AppApi.BASE_URL + bean.getSchoolProfilePic());
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new LastItemDecoration());
        drawerAdapter = new DrawerAdapter(HomeActivity.this);
        recyclerView.setAdapter(drawerAdapter);
        drawerAdapter.addItems(getData());
    }

    private List<DrawerModel> getData() {

        CREDENTIAL mainEntity = AdminApp.getInstance().getCredential();
        CREDENTIAL.ResultBean.SchoolpermissionsBean schoolPermissions = null;
        CREDENTIAL.ResultBean.UserpermissionsBean.AdminmodulesBean userEntity = null;

        if (mainEntity != null && mainEntity.getResult() != null && mainEntity.getResult().size() > 0) {
            schoolPermissions = mainEntity.getResult().get(0).getSchoolpermissions();
            CREDENTIAL.ResultBean.UserpermissionsBean ue = mainEntity.getResult().get(0).getUserpermissions();
            if (ue != null) {
                userEntity = ue.getAdminmodules();
            }
        }

        List<DrawerModel> models = new ArrayList<>();

        if (schoolPermissions != null) {

            //communication list
            if (schoolPermissions.getAdminmodules().isCommunication() || schoolPermissions.getAdminmodules().isChannel() || schoolPermissions.getAdminmodules().isAppointment()) {

                List<DrawerModel.SubItem> communicationList = new ArrayList<>();

                if (schoolPermissions.getAdminmodules().isCommunication()) {

                    if (userEntity != null)
                        communicationList.add(new DrawerModel.SubItem(getString(R.string.view_and_respond), R.drawable.ic_view_respond, new ViewResponseFragment(), userEntity.isCommunication()));
                    else
                        communicationList.add(new DrawerModel.SubItem(getString(R.string.view_and_respond), R.drawable.ic_view_respond, new ViewResponseFragment(), true));
                }

                if (schoolPermissions.getAdminmodules().isChannel()) {

                    if (userEntity != null) {
                        communicationList.add(new DrawerModel.SubItem(getString(R.string.school_channel), R.drawable.ic_school, new SchoolChannelFragment(), userEntity.isChannel()));
                        communicationList.add(new DrawerModel.SubItem(getString(R.string.class_channel), R.drawable.ic_class_channel, new ClassChannelFragment(), userEntity.isChannel()));
                    } else {
                        communicationList.add(new DrawerModel.SubItem(getString(R.string.school_channel), R.drawable.ic_school, new SchoolChannelFragment(), true));
                        communicationList.add(new DrawerModel.SubItem(getString(R.string.class_channel), R.drawable.ic_class_channel, new ClassChannelFragment(), true));
                    }
                }

                if (schoolPermissions.getAdminmodules().isAppointment()) {
                    if (userEntity != null) {
                        communicationList.add(new DrawerModel.SubItem(getString(R.string.appointment), R.drawable.ic_appoinment, new AppointmentFragment(), userEntity.isAppointment()));
                    } else {
                        communicationList.add(new DrawerModel.SubItem(getString(R.string.appointment), R.drawable.ic_appoinment, new AppointmentFragment(), true));
                    }
                }

                models.add(new DrawerModel(getString(R.string.communication), communicationList));
            }

            //class schedule list
            List<DrawerModel.SubItem> classscheduleList = new ArrayList<>();

            classscheduleList.add(new DrawerModel.SubItem(getString(R.string.grade), R.drawable.ic_grade, new GradeFragment(), true));
            classscheduleList.add(new DrawerModel.SubItem(getString(R.string.acedmic_year), R.drawable.ic_acedamic_year, new AcadamicYearFragment(), true));

            if (schoolPermissions.getAdminmodules().isEvents()) {
                if (userEntity != null) {
                    classscheduleList.add(new DrawerModel.SubItem(getString(R.string.class_events), R.drawable.ic_events, new ClassEventsFragment(), userEntity.isEvents()));
                    classscheduleList.add(new DrawerModel.SubItem(getString(R.string.school_events), R.drawable.ic_events, new SchoolEventsFragment(), userEntity.isEvents()));
                } else {
                    classscheduleList.add(new DrawerModel.SubItem(getString(R.string.class_events), R.drawable.ic_events, new ClassEventsFragment(), true));
                    classscheduleList.add(new DrawerModel.SubItem(getString(R.string.school_events), R.drawable.ic_events, new SchoolEventsFragment(), true));
                }
            }

            models.add(new DrawerModel(getString(R.string.class_schedules_info), classscheduleList));

            //admission List
            List<DrawerModel.SubItem> admissionList = new ArrayList<>();

            if (schoolPermissions.getAdminmodules().isStudent()) {
                if (userEntity != null) {
                    admissionList.add(new DrawerModel.SubItem(getString(R.string.student), R.drawable.ic_student, new AStudentFragment(), userEntity.isStudent()));
                } else {
                    admissionList.add(new DrawerModel.SubItem(getString(R.string.student), R.drawable.ic_student, new AStudentFragment(), true));
                }
            }

            admissionList.add(new DrawerModel.SubItem(getString(R.string.teacher), R.drawable.ic_teacher, new ATecherFragment(), true));

            if (schoolPermissions.getAdminmodules().isStaff()) {
                if (userEntity != null) {
                    admissionList.add(new DrawerModel.SubItem(getString(R.string.staff), R.drawable.ic_teacher, new AStaffFragment(), userEntity.isStaff()));
                } else {
                    admissionList.add(new DrawerModel.SubItem(getString(R.string.staff), R.drawable.ic_teacher, new AStaffFragment(), true));
                }
            }

            if (schoolPermissions.getAdminmodules().isDriver()) {
                if (userEntity != null)
                    admissionList.add(new DrawerModel.SubItem(getString(R.string.driver), R.drawable.ic_driver, new ADriverFragment(), userEntity.isDriver()));
                else
                    admissionList.add(new DrawerModel.SubItem(getString(R.string.driver), R.drawable.ic_driver, new ADriverFragment(), true));
            }

            models.add(new DrawerModel(getString(R.string.addmission_on_board), admissionList));

            //userInfo List
            List<DrawerModel.SubItem> userInfoList = new ArrayList<>();

            if (schoolPermissions.getAdminmodules().isStudent()) {
                if (userEntity != null)
                    userInfoList.add(new DrawerModel.SubItem(getString(R.string.student), R.drawable.ic_student, new IStudentFragment(), userEntity.isStudent()));
                else
                    userInfoList.add(new DrawerModel.SubItem(getString(R.string.student), R.drawable.ic_student, new IStudentFragment(), true));
            }

            userInfoList.add(new DrawerModel.SubItem(getString(R.string.teacher), R.drawable.ic_teacher, new ITeacherFragment(), true));

            if (schoolPermissions.getAdminmodules().isStaff()) {

                if (userEntity != null)
                    userInfoList.add(new DrawerModel.SubItem(getString(R.string.staff), R.drawable.ic_staff, new IStaffFragment(), userEntity.isStaff()));
                else
                    userInfoList.add(new DrawerModel.SubItem(getString(R.string.staff), R.drawable.ic_staff, new IStaffFragment(), true));
            }

            if (schoolPermissions.getAdminmodules().isDriver()) {
                if (userEntity != null)
                    userInfoList.add(new DrawerModel.SubItem(getString(R.string.driver), R.drawable.ic_driver, new IDriverFragment(), userEntity.isDriver()));
                else
                    userInfoList.add(new DrawerModel.SubItem(getString(R.string.driver), R.drawable.ic_driver, new IDriverFragment(), true));
            }

            models.add(new DrawerModel(getString(R.string.user_info), userInfoList));

            //route List
            if (schoolPermissions.getAdminmodules().isRoute()) {

                List<DrawerModel.SubItem> transportList = new ArrayList<>();

                if (userEntity != null) {
                    transportList.add(new DrawerModel.SubItem(getString(R.string.route), R.drawable.ic_route, new RouteFragment(), userEntity.isRoute()));
                    transportList.add(new DrawerModel.SubItem(getString(R.string.active_route), R.drawable.ic_active_route, new ActiveRoutesFragment(), userEntity.isRoute()));
                } else {
                    transportList.add(new DrawerModel.SubItem(getString(R.string.route), R.drawable.ic_route, new RouteFragment(), true));
                    transportList.add(new DrawerModel.SubItem(getString(R.string.active_route), R.drawable.ic_active_route, new ActiveRoutesFragment(), true));
                }

                models.add(new DrawerModel(getString(R.string.transportation), transportList));
            }

            //logout
            DrawerModel model = new DrawerModel();
            model.setGroupTitle(getString(R.string.logout));
            model.setHasMoreItems(false);
            models.add(model);

        } else {

            if (userEntity != null) {

                //communication list
                List<DrawerModel.SubItem> communicationList = new ArrayList<>();

                communicationList.add(new DrawerModel.SubItem(getString(R.string.view_and_respond), R.drawable.ic_view_respond, new ViewResponseFragment(), userEntity.isCommunication()));
                communicationList.add(new DrawerModel.SubItem(getString(R.string.school_channel), R.drawable.ic_school, new SchoolChannelFragment(), userEntity.isChannel()));
                communicationList.add(new DrawerModel.SubItem(getString(R.string.class_channel), R.drawable.ic_class_channel, new ClassChannelFragment(), userEntity.isChannel()));
                communicationList.add(new DrawerModel.SubItem(getString(R.string.appointment), R.drawable.ic_appoinment, new AppointmentFragment(), userEntity.isAppointment()));
                models.add(new DrawerModel(getString(R.string.communication), communicationList));

                //class schedule list
                List<DrawerModel.SubItem> classscheduleList = new ArrayList<>();

                classscheduleList.add(new DrawerModel.SubItem(getString(R.string.grade), R.drawable.ic_grade, new GradeFragment(), true));
                classscheduleList.add(new DrawerModel.SubItem(getString(R.string.acedmic_year), R.drawable.ic_acedamic_year, new AcadamicYearFragment(), true));
                classscheduleList.add(new DrawerModel.SubItem(getString(R.string.class_events), R.drawable.ic_events, new ClassEventsFragment(), userEntity.isEvents()));
                classscheduleList.add(new DrawerModel.SubItem(getString(R.string.school_events), R.drawable.ic_events, new SchoolEventsFragment(), userEntity.isEvents()));
                models.add(new DrawerModel(getString(R.string.class_schedules_info), classscheduleList));

                //admission List
                List<DrawerModel.SubItem> admissionList = new ArrayList<>();

                admissionList.add(new DrawerModel.SubItem(getString(R.string.student), R.drawable.ic_student, new AStudentFragment(), userEntity.isStudent()));
                admissionList.add(new DrawerModel.SubItem(getString(R.string.teacher), R.drawable.ic_teacher, new ATecherFragment(), true));
                admissionList.add(new DrawerModel.SubItem(getString(R.string.staff), R.drawable.ic_teacher, new AStaffFragment(), userEntity.isStaff()));
                admissionList.add(new DrawerModel.SubItem(getString(R.string.driver), R.drawable.ic_driver, new ADriverFragment(), userEntity.isDriver()));
                models.add(new DrawerModel(getString(R.string.addmission_on_board), admissionList));

                //userInfo List
                List<DrawerModel.SubItem> userInfoList = new ArrayList<>();

                userInfoList.add(new DrawerModel.SubItem(getString(R.string.student), R.drawable.ic_student, new IStudentFragment(), userEntity.isStudent()));
                userInfoList.add(new DrawerModel.SubItem(getString(R.string.teacher), R.drawable.ic_teacher, new ITeacherFragment(), true));
                userInfoList.add(new DrawerModel.SubItem(getString(R.string.staff), R.drawable.ic_staff, new IStaffFragment(), userEntity.isStaff()));
                userInfoList.add(new DrawerModel.SubItem(getString(R.string.driver), R.drawable.ic_driver, new IDriverFragment(), userEntity.isDriver()));
                models.add(new DrawerModel(getString(R.string.user_info), userInfoList));

                //route List

                List<DrawerModel.SubItem> transportList = new ArrayList<>();
                transportList.add(new DrawerModel.SubItem(getString(R.string.route), R.drawable.ic_route, new RouteFragment(), userEntity.isRoute()));
                transportList.add(new DrawerModel.SubItem(getString(R.string.active_route), R.drawable.ic_active_route, new ActiveRoutesFragment(), userEntity.isRoute()));

                models.add(new DrawerModel(getString(R.string.transportation), transportList));

                //logout
                DrawerModel model = new DrawerModel();
                model.setGroupTitle(getString(R.string.logout));
                model.setHasMoreItems(false);
                models.add(model);

            } else {

                List<DrawerModel.SubItem> communicationList = new ArrayList<>();
                communicationList.add(new DrawerModel.SubItem(getString(R.string.view_and_respond), R.drawable.ic_view_respond, new ViewResponseFragment(), true));
                communicationList.add(new DrawerModel.SubItem(getString(R.string.school_channel), R.drawable.ic_school, new SchoolChannelFragment(), true));
                communicationList.add(new DrawerModel.SubItem(getString(R.string.class_channel), R.drawable.ic_class_channel, new ClassChannelFragment(), true));
                communicationList.add(new DrawerModel.SubItem(getString(R.string.appointment), R.drawable.ic_appoinment, new AppointmentFragment(), true));

                models.add(new DrawerModel(getString(R.string.communication), communicationList));

                List<DrawerModel.SubItem> classscheduleList = new ArrayList<>();
                classscheduleList.add(new DrawerModel.SubItem(getString(R.string.grade), R.drawable.ic_grade, new GradeFragment(), true));
                classscheduleList.add(new DrawerModel.SubItem(getString(R.string.acedmic_year), R.drawable.ic_acedamic_year, new GradeFragment(), true));
                classscheduleList.add(new DrawerModel.SubItem(getString(R.string.class_events), R.drawable.ic_events, new ClassEventsFragment(), true));
                classscheduleList.add(new DrawerModel.SubItem(getString(R.string.school_events), R.drawable.ic_events, new SchoolEventsFragment(), true));

                models.add(new DrawerModel(getString(R.string.class_schedules_info), classscheduleList));

                List<DrawerModel.SubItem> admissionList = new ArrayList<>();
                admissionList.add(new DrawerModel.SubItem(getString(R.string.student), R.drawable.ic_student, new AStudentFragment(), true));
                admissionList.add(new DrawerModel.SubItem(getString(R.string.teacher), R.drawable.ic_teacher, new ATecherFragment(), true));
                admissionList.add(new DrawerModel.SubItem(getString(R.string.staff), R.drawable.ic_staff, new AStaffFragment(), true));
                admissionList.add(new DrawerModel.SubItem(getString(R.string.driver), R.drawable.ic_driver, new ADriverFragment(), true));

                models.add(new DrawerModel(getString(R.string.addmission_on_board), admissionList));

                List<DrawerModel.SubItem> userInfoList = new ArrayList<>();
                userInfoList.add(new DrawerModel.SubItem(getString(R.string.student), R.drawable.ic_student, new IStudentFragment(), true));
                userInfoList.add(new DrawerModel.SubItem(getString(R.string.teacher), R.drawable.ic_teacher, new ITeacherFragment(), true));
                userInfoList.add(new DrawerModel.SubItem(getString(R.string.staff), R.drawable.ic_teacher, new IStaffFragment(), true));
                userInfoList.add(new DrawerModel.SubItem(getString(R.string.driver), R.drawable.ic_driver, new IDriverFragment(), true));

                models.add(new DrawerModel(getString(R.string.user_info), userInfoList));

                List<DrawerModel.SubItem> transportList = new ArrayList<>();
                transportList.add(new DrawerModel.SubItem(getString(R.string.route), R.drawable.ic_route, new RouteFragment(), true));

                models.add(new DrawerModel(getString(R.string.transportation), transportList));

                DrawerModel model = new DrawerModel();
                model.setGroupTitle(getString(R.string.logout));
                model.setHasMoreItems(false);
                models.add(model);
            }
        }

        return models;
    }


    private void addFirstFragment() {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.container, new DashBoardFragment());
        fragmentTransaction.commit();
    }

    public void closeDrawer() {

        if (menu.isMenuShowing()) {
            menu.toggle();
        }
    }


    public void LogoutDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
        builder.setTitle(getResources().getString(R.string.logout));
        builder.setMessage(getResources().getString(R.string.logout_msg));
        builder.setPositiveButton(getResources().getString(R.string.logout), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();
                AdminApp.getInstance().logout();
                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                finish();


            }
        });
        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });
        builder.setCancelable(false);
        final AlertDialog alertDialog1 = builder.create();
        alertDialog1.show();

        alertDialog1.getButton(alertDialog1.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(HomeActivity.this, R.color.colorPrimaryDark));
        alertDialog1.getButton(alertDialog1.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(HomeActivity.this, R.color.colorPrimaryDark));
    }


    public void Loadfragment(Fragment fragment) {

        if (fragment instanceof AStudentFragment) {

            startActivity(new Intent(HomeActivity.this, AddStudentActivity.class));
            overridePendingTransition(R.anim.enter, R.anim.leave);

        } else if (fragment instanceof ATecherFragment) {

            startActivity(new Intent(HomeActivity.this, AddTeacherActivity.class));
            overridePendingTransition(R.anim.enter, R.anim.leave);

        } else if (fragment instanceof AStaffFragment) {

            startActivity(new Intent(HomeActivity.this, AddStaffActivity.class));
            overridePendingTransition(R.anim.enter, R.anim.leave);

        } else if (fragment instanceof ADriverFragment) {

            startActivity(new Intent(HomeActivity.this, AddDriverActivity.class));
            overridePendingTransition(R.anim.enter, R.anim.leave);

        } else {

            if (fragment instanceof ViewResponseFragment) {
                Bundle bundle = new Bundle();
                bundle.putString(MESSAGE_ID, messageId);
                fragment.setArguments(bundle);
            }

            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }

    public void LoadFragmentWithPayLoad(Fragment fragment, String id) {

        if (fragment instanceof SubjectFragment) {

            Bundle bundle = new Bundle();
            bundle.putString(App_Constants.GRADE_ID, id);
            fragment.setArguments(bundle);
        }

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }


    public void LoadDashBoard() {

        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }

        addFirstFragment();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();

        if (itemId == R.id.itm_dshboard) {

            LoadDashBoard();

        } else if (itemId == android.R.id.home) {

            menu.toggle();
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == APIError.UPDATE_TOKEN && resultCode == RESULT_OK) {

            if (data != null) {
                String apiName = data.getStringExtra("api");
                reloadCurrentScreen(apiName);
            }

        } else {

            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
            if (fragment != null) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void reloadCurrentScreen(String apiName) {

        if (onReloadListener != null)
            onReloadListener.onReload(apiName);

    }

    public void setOnReloadListener(OnReloadListener onReloadListener) {
        this.onReloadListener = onReloadListener;
    }

    @Override
    public void onBackPressed() {

        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (menu.isMenuShowing()) {
            menu.toggle();
            return;
        }

        if (count > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public void onNewIntent(Intent intent) {

        Bundle extras = intent.getExtras();
        if (extras != null) {

            channelType = extras.getString(CHANNEL_TYPE);
            messageType = extras.getString(MESSAGE_TYPE);
            gradId = extras.getString(GRADE_ID);
            classId = extras.getString(CLASS_ID);
            studentId = extras.getString(STUDENT_ID);
            profileId = extras.getString(PROFILE_ID);
            schoolId = extras.getString(SCHOOL_ID);
            messageId = extras.getString(MESSAGE_ID);
            appointmentId = extras.getString(APPOINTMENT_ID);
            notificationId = extras.getString(NOTIFICATION_ID);
            groupId = extras.getString(GROUP_ID);

            Log.e("message", "found in home");
            Log.e("channelType", "" + channelType);
            Log.e("messageType", "" + messageType);
            Log.e("gradId", "" + gradId);
            Log.e("classId", "" + classId);
            Log.e("studentId", "" + studentId);
            Log.e("profileId", "" + profileId);
            Log.e("schoolId", "" + schoolId);
            Log.e("messageId", "" + messageId);
            Log.e("appointmentId", "" + appointmentId);
            Log.e("notificationId", "" + notificationId);
            Log.e("groupId", "" + groupId);


            if (messageType != null) {

                switch (messageType) {

                    case APPOINTMENT:

                        startActivity(new Intent(HomeActivity.this, AppointmentDetailActivity.class)
                                .putExtra(AppointmentDetailActivity.APPOINMENT_ID, appointmentId)
                                .putExtra(AppointmentDetailActivity.TYPE, AppointmentDetailActivity.UPCOMING));

                        break;

                    case COMMUNICATION:

                        Loadfragment(new ViewResponseFragment());

                        break;
                }
            }

        } else {
            Log.e("message", "not found in home");
        }
    }

}
