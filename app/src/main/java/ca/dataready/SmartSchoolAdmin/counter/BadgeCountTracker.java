/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.counter;


import android.content.Context;
import android.content.SharedPreferences;


public class BadgeCountTracker {

    // communication  counter
    public static void saveViewandResposndBadgeCounter(Context context, int counter) {

        SharedPreferences preferences = context.getSharedPreferences(CounterConstants.COUNTER_PREFERENCES, Context.MODE_PRIVATE);
        preferences.edit().putInt(CounterConstants.VIEW_AND_RESPOND_DATA, counter).apply();

    }

    public static int getViewandResposndBadgeCounter(Context context) {

        if (context != null) {
            SharedPreferences preferences = context.getSharedPreferences(CounterConstants.COUNTER_PREFERENCES, Context.MODE_PRIVATE);
            return preferences.getInt(CounterConstants.VIEW_AND_RESPOND_DATA, 0);
        } else {
            return 0;
        }

    }


    // communication single thread counter
    public static void saveViewandResposndSingleThreadBadgeCounter(Context context, int counter, String msgId) {

        SharedPreferences preferences = context.getSharedPreferences(CounterConstants.COUNTER_PREFERENCES, Context.MODE_PRIVATE);
        preferences.edit().putInt(CounterConstants.VIEW_AND_RESPOND_SINGEL_THREAD_DATA
                + CounterConstants.SEPARATOR
                + msgId, counter).apply();

    }

    public static int getViewandResposndSingleThreadBadgeCounter(Context context, String msgId) {

        if (context != null) {
            SharedPreferences preferences = context.getSharedPreferences(CounterConstants.COUNTER_PREFERENCES, Context.MODE_PRIVATE);
            return preferences.getInt(CounterConstants.VIEW_AND_RESPOND_SINGEL_THREAD_DATA
                    + CounterConstants.SEPARATOR
                    + msgId, 0);
        } else {
            return 0;
        }

    }


    //teacher comm counter
    public static void saveTeacherCommBadgeCounter(Context context, int counter) {

        SharedPreferences preferences = context.getSharedPreferences(CounterConstants.COUNTER_PREFERENCES, Context.MODE_PRIVATE);
        preferences.edit().putInt(CounterConstants.TEACHER_COMM_DATA, counter).apply();

    }

    public static int getTeacherCommBadgeCounter(Context context) {

        if (context != null) {
            SharedPreferences preferences = context.getSharedPreferences(CounterConstants.COUNTER_PREFERENCES, Context.MODE_PRIVATE);
            return preferences.getInt(CounterConstants.TEACHER_COMM_DATA, 0);
        } else {
            return 0;
        }

    }

    //teacher comm thread counter
    public static void saveTeacherCommSingleThreadBadgeCounter(Context context, int counter, String groupId) {

        SharedPreferences preferences = context.getSharedPreferences(CounterConstants.COUNTER_PREFERENCES, Context.MODE_PRIVATE);
        preferences.edit().putInt(CounterConstants.TEACHER_COMM_THREAD_DATA
                + CounterConstants.SEPARATOR
                + groupId, counter).apply();

    }

    public static int getTeacherCommSingleThreadBadgeCounter(Context context, String groupId) {

        if (context != null) {
            SharedPreferences preferences = context.getSharedPreferences(CounterConstants.COUNTER_PREFERENCES, Context.MODE_PRIVATE);
            return preferences.getInt(CounterConstants.TEACHER_COMM_THREAD_DATA + CounterConstants.SEPARATOR
                    + groupId, 0);
        } else {
            return 0;
        }

    }

    // Appointment  counter
    public static void saveAppointmentBadgeCounter(Context context, int counter) {

        SharedPreferences preferences = context.getSharedPreferences(CounterConstants.COUNTER_PREFERENCES, Context.MODE_PRIVATE);
        preferences.edit().putInt(CounterConstants.APPOINTMENT_DATA, counter).apply();

    }

    public static int getAppointmentBadgeCounter(Context context) {

        if (context != null) {
            SharedPreferences preferences = context.getSharedPreferences(CounterConstants.COUNTER_PREFERENCES, Context.MODE_PRIVATE);
            return preferences.getInt(CounterConstants.APPOINTMENT_DATA, 0);
        } else {
            return 0;
        }

    }


    // school channel  counter
    public static void saveSchoolChannelBadgeCounter(Context context, int counter) {

        SharedPreferences preferences = context.getSharedPreferences(CounterConstants.COUNTER_PREFERENCES, Context.MODE_PRIVATE);
        preferences.edit().putInt(CounterConstants.SCHOOL_CHANNEL_DATA, counter).apply();

    }

    public static int getSchoolChannelBadgeCounter(Context context) {

        if (context != null) {
            SharedPreferences preferences = context.getSharedPreferences(CounterConstants.COUNTER_PREFERENCES, Context.MODE_PRIVATE);
            return preferences.getInt(CounterConstants.SCHOOL_CHANNEL_DATA, 0);
        } else {
            return 0;
        }

    }


}
