/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.counter;


public class CounterConstants {

    public static final String COUNTER_PREFERENCES = "counter_prefs";

    public static final String SEPARATOR = "_";
    public static final String CLASS_CHANNEL_DATA = "class_channel_data_of";
    public static final String SCHOOL_CHANNEL_DATA = "school_channel_data_of";
    public static final String VIEW_AND_RESPOND_DATA = "view_and_respond_data_of";
    public static final String VIEW_AND_RESPOND_SINGEL_THREAD_DATA = "view_and_respond_single_thread_data_of";
    public static final String TEACHER_COMM_DATA = "teacher_comm_data_of";
    public static final String TEACHER_COMM_THREAD_DATA = "teacher_comm_thread_data_of";
    public static final String APPOINTMENT_DATA = "appointment_data_of";

}
