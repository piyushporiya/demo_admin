/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.classchannel.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.DateFunction;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.classchannel.details.ClassChannelDetailsActivity;
import ca.dataready.SmartSchoolAdmin.server.AppApi;
import ca.dataready.SmartSchoolAdmin.server.ClassChannelResponse;
import ca.dataready.SmartSchoolAdmin.viewresponse.adapter.FilesAdapter;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.FileModel;
import ca.dataready.SmartSchoolAdmin.web.WebActivity;

public class ClassChannelAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mcontex;
    private List<ClassChannelResponse.ResultBean> list;
    private FilesAdapter filesAdapter;
    private static final int TYPE_VIEW = 0;
    private static final int TYPE_HEADER = 1;

    private List<FileModel> selectedFiles = new ArrayList<>();

    public ClassChannelAdapter(Context mcontex) {
        this.mcontex = mcontex;
        this.list = new ArrayList<>();
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position).getType() == 1)
            return TYPE_HEADER;
        return TYPE_VIEW;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_VIEW) {

            return new NotificationHolder(LayoutInflater.from(mcontex).inflate(R.layout.raw_class_channel, parent, false));

        } else if (viewType == TYPE_HEADER) {

            return new HeaderViewHolder(LayoutInflater.from(mcontex).inflate(R.layout.raw_header, parent, false));

        } else {

            return null;
        }


    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        final ClassChannelResponse.ResultBean bean = list.get(position);


        if (holder instanceof NotificationHolder) {

            ((NotificationHolder) holder).txtClassGrade.setText(mcontex.getString(R.string.grade) + " - " + bean.getGradeId() + " " + mcontex.getString(R.string.Class) + " - " + bean.getClassId());
            ((NotificationHolder) holder).txtSortMsg.setText(bean.getNotificationShortMessage());
            ((NotificationHolder) holder).txtLongMsg.setText(bean.getNotificationLongMessage());
            ((NotificationHolder) holder).txtDate.setText(DateFunction.ConvertDate(bean.getNotificationDate(), "yyyy-MM-dd", "dd MMM yyyy"));
            ((NotificationHolder) holder).txtSortMsg.setText(bean.getNotificationShortMessage());

            ((NotificationHolder) holder).mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    mcontex.startActivity(new Intent(mcontex, ClassChannelDetailsActivity.class)
                            .putExtra(App_Constants.OBJECT, bean));
                    ((HomeActivity)mcontex).overridePendingTransition(R.anim.enter, R.anim.leave);
                }
            });


            if (bean.getFiles() != null && bean.getFiles().size() > 0) {

                ((NotificationHolder) holder).recyclerView.setLayoutManager(new LinearLayoutManager(mcontex));
                filesAdapter = new FilesAdapter(mcontex, true);
                ((NotificationHolder) holder).recyclerView.setAdapter(filesAdapter);
                selectedFiles.clear();
                if (bean.getFiles() != null && bean.getFiles().size() > 0) {
                    for (ClassChannelResponse.ResultBean.FilesBean model : bean.getFiles()) {
                        if (model.getFilePath().contains("/")) {
                            selectedFiles.add(new FileModel(model.getFilePath().substring(model.getFilePath().lastIndexOf("/") + 1), model.getFilePath()));
                        }
                    }
                }
                filesAdapter.addItem(selectedFiles);

            } else {
                ((NotificationHolder) holder).txtViewMore.setVisibility(View.GONE);
            }

            ((NotificationHolder) holder).txtLongMsg.setMaxLines(Integer.MAX_VALUE);

            if (bean.isExpanded()) {

                ((NotificationHolder) holder).txtViewMore.setVisibility(View.VISIBLE);
                ((NotificationHolder) holder).txtViewMore.setText(mcontex.getString(R.string.view_less));
                ((NotificationHolder) holder).txtLongMsg.setMaxLines(Integer.MAX_VALUE);
                ((NotificationHolder) holder).recyclerView.setVisibility(View.VISIBLE);
                ((NotificationHolder) holder).txtFileName.setVisibility(View.GONE);

            } else {

                if (bean.getFiles() != null && bean.getFiles().size() > 1) {

                    ((NotificationHolder) holder).txtViewMore.setVisibility(View.VISIBLE);
                    ((NotificationHolder) holder).recyclerView.setVisibility(View.GONE);
                    ((NotificationHolder) holder).txtFileName.setVisibility(View.VISIBLE);
                    ((NotificationHolder) holder).txtFileName.setText(selectedFiles.get(0).getName());

                } else if (bean.getFiles() != null && bean.getFiles().size() > 0) {

                    ((NotificationHolder) holder).txtViewMore.setVisibility(View.GONE);
                    ((NotificationHolder) holder).recyclerView.setVisibility(View.GONE);
                    ((NotificationHolder) holder).txtFileName.setVisibility(View.VISIBLE);
                    ((NotificationHolder) holder).txtFileName.setText(selectedFiles.get(0).getName());

                    ((NotificationHolder) holder).txtLongMsg.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                        @Override
                        public boolean onPreDraw() {
                            ((NotificationHolder) holder).txtLongMsg.getViewTreeObserver().removeOnPreDrawListener(this);
                            int lineCount = ((NotificationHolder) holder).txtLongMsg.getLineCount();
                            Log.e("TreeObserver", "onBind: " + lineCount);
                            if (lineCount > 1) {
                                ((NotificationHolder) holder).txtViewMore.setVisibility(View.VISIBLE);
                                ((NotificationHolder) holder).txtLongMsg.setMaxLines(1);
                            } else {
                                ((NotificationHolder) holder).txtViewMore.setVisibility(View.GONE);
                            }

                            return false;
                        }
                    });

                } else {

                    ((NotificationHolder) holder).txtViewMore.setVisibility(View.GONE);
                    ((NotificationHolder) holder).recyclerView.setVisibility(View.GONE);
                    ((NotificationHolder) holder).txtFileName.setVisibility(View.GONE);

                    ((NotificationHolder) holder).txtLongMsg.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                        @Override
                        public boolean onPreDraw() {
                            ((NotificationHolder) holder).txtLongMsg.getViewTreeObserver().removeOnPreDrawListener(this);
                            int lineCount = ((NotificationHolder) holder).txtLongMsg.getLineCount();
                            if (lineCount > 1) {
                                ((NotificationHolder) holder).txtViewMore.setVisibility(View.VISIBLE);
                                ((NotificationHolder) holder).txtLongMsg.setMaxLines(1);
                            } else {
                                ((NotificationHolder) holder).txtViewMore.setVisibility(View.GONE);
                            }

                            return false;
                        }
                    });
                }


                ((NotificationHolder) holder).txtViewMore.setText(mcontex.getString(R.string.view_more));


            }

            ((NotificationHolder) holder).txtFileName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (bean.getFiles() != null && bean.getFiles().size() > 0) {
                        mcontex.startActivity(new Intent(mcontex, WebActivity.class)
                                .putExtra(WebActivity.URL, AppApi.BASE_URL + bean.getFiles().get(0).getFilePath()));
                    }


                }
            });


            ((NotificationHolder) holder).txtViewMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    bean.setExpanded(!bean.isExpanded());
                    makeOtherCollapsed(position);

                }
            });

        } else if (holder instanceof HeaderViewHolder) {

            ((HeaderViewHolder) holder).txtMonth.setText(bean.getMonth());
        }

    }

    private void makeOtherCollapsed(int position) {

        for (int i = 0; i < list.size(); i++) {

            if (i != position) {
                list.get(i).setExpanded(false);
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public void addItem(List<ClassChannelResponse.ResultBean> result) {

      /*  Collections.sort(result, new Comparator<SchoolChannelResponse.ResultBean>() {
            public int compare(SchoolChannelResponse.ResultBean o1, SchoolChannelResponse.ResultBean o2) {

                if (o2.getCreationDate() == null || o1.getCreationDate() == null)
                    return 0;

                return o2.getCreationDate().compareTo(o1.getCreationDate());
            }
        });
*/
        list.addAll(result);
        notifyDataSetChanged();
    }

    public void clear() {

        list.clear();
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class NotificationHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.txt_sort_msg)
        TextView txtSortMsg;
        @BindView(R.id.txt_date)
        TextView txtDate;
        @BindView(R.id.txt_long_msg)
        TextView txtLongMsg;
        @BindView(R.id.img)
        ImageView img;
        @BindView(R.id.txt_file_name)
        TextView txtFileName;
        @BindView(R.id.recyclerView)
        RecyclerView recyclerView;
        @BindView(R.id.txt_view_more)
        TextView txtViewMore;
        View mView;
        @BindView(R.id.txt_class_grade)
        TextView txtClassGrade;

        // R.layout.raw_school_channel

        // R.layout.raw_class_channel
        NotificationHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mView = itemView;
        }
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_month)
        TextView txtMonth;

        HeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
