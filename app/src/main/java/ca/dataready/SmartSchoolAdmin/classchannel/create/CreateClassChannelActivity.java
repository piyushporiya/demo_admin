/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.classchannel.create;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.fileselectoer.FileSelectionActivity;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.CreateSchoolChannelResponse;
import ca.dataready.SmartSchoolAdmin.viewresponse.adapter.FilesAdapter;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.FileModel;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.GradeList;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.ImageUpload;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateClassChannelActivity extends BaseActivity {

    private static final int CAMERA_REQUEST_CODE = 102;
    private static final int REQUEST_CAMERA = 99;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.llheader)
    LinearLayout llheader;
    @BindView(R.id.title)
    EditText title;
    @BindView(R.id.msg)
    EditText msg;
    @BindView(R.id.iv_SpeechToText)
    ImageView ivSpeechToText;
    @BindView(R.id.img_attachment)
    ImageView imgAttachment;
    @BindView(R.id.iv_camera_AttachFile)
    ImageView ivCameraAttachFile;
    @BindView(R.id.sendfeedback)
    Button sendfeedback;
    @BindView(R.id.txt_attachment)
    TextView txtAttachment;
    @BindView(R.id.rvAttachments)
    RecyclerView rvAttachments;
    @BindView(R.id.llAttachments)
    LinearLayout llAttachments;
    @BindView(R.id.llTextArea)
    LinearLayout llTextArea;
    @BindView(R.id.parent_viewAnimator)
    ViewAnimator parentViewAnimator;
    @BindView(R.id.txt_Grade)
    TextView txtGrade;
    @BindView(R.id.linear_Grade)
    RelativeLayout linearGrade;
    @BindView(R.id.txt_Class)
    TextView txtClass;
    @BindView(R.id.linear_Section)
    RelativeLayout linearSection;
    @BindView(R.id.main)
    RelativeLayout main;
    private FilesAdapter filesAdapter;
    private List<FileModel> selectedFiles = new ArrayList<>();
    List<SendChannelParams.FilesBean> files = new ArrayList<>();
    private Handler mhandler = new Handler();
    private Call<CreateSchoolChannelResponse> api;
    private Call<GradeList> call;
    private ArrayList<GradeList.ResultBean.GradeBean> teacherBeans;
    private ListPopupWindow listPopupWindow;
    ArrayList<String> gradeList = new ArrayList<>();
    ArrayList<String> classList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_class_channel);
        ButterKnife.bind(this);
        setHeaderInfo();
        Init();
    }


    private void setHeaderInfo() {

        setSupportActionBar(toolbar);

        if (AdminApp.getInstance().getAdmin() != null)
            getSupportActionBar().setTitle(AdminApp.getInstance().getAdmin().getSchoolName() + " " + AdminApp.getInstance().getAdmin().getSchoolYear());
        else
            getSupportActionBar().setTitle(getString(R.string.app_name));

        getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + getString(R.string.post_messsage_to_class) + "</small>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void Init() {
        ApiCall();
    }

    private void ApiCall() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();
        if (entity == null)
            return;

        parentViewAnimator.setDisplayedChild(0);
        call = AdminApp.getInstance().getApi().getClassSchedule(entity.getSchoolId(), entity.getSchoolYear());
        call.enqueue(new Callback<GradeList>() {
            @Override
            public void onResponse(Call<GradeList> call, Response<GradeList> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            teacherBeans = response.body().getResult().getGrade();
                            parentViewAnimator.setDisplayedChild(1);
                            Log.d("List", "onResponse: " + response.body().getResult().getGrade());
                            //LoadStudentOnStartUp();

                        } else {
                            Utility.showSnackBar(main, response.body().getMessage());
                            parentViewAnimator.setDisplayedChild(1);
                        }

                    } else {

                        APIError error = APIError.parseError(response, CreateClassChannelActivity.this, App_Constants.API_TEACHER_CLASS_SCHEDULE);
                        Utility.error(CreateClassChannelActivity.this, error.message());
                        parentViewAnimator.setDisplayedChild(1);

                    }
                } catch (Exception e) {

                    Utility.showSnackBar(main, getString(R.string.somethingwrong));
                    e.printStackTrace();
                    parentViewAnimator.setDisplayedChild(1);
                }


            }

            @Override
            public void onFailure(Call<GradeList> call, Throwable t) {
                if (!call.isCanceled()) {

                    System.out.println(t.getMessage());
                    Utility.showSnackBar(main, App_Constants.NO_INTERNET);
                    parentViewAnimator.setDisplayedChild(1);
                }
            }
        });
    }


    @OnClick({R.id.linear_Grade, R.id.linear_Section, R.id.iv_SpeechToText, R.id.img_attachment, R.id.iv_camera_AttachFile, R.id.sendfeedback})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.linear_Grade:
                show_drop_down_to_select_option(linearGrade, App_Constants.GRADE);
                break;
            case R.id.linear_Section:
                show_drop_down_to_select_option(linearSection, App_Constants.CLASS);
                break;

            case R.id.iv_SpeechToText:
                promptSpeechInput();
                break;
            case R.id.img_attachment:
                Intent intent = new Intent(CreateClassChannelActivity.this, FileSelectionActivity.class);
                startActivityForResult(intent, App_Constants.FILE_SELECT);
                break;
            case R.id.iv_camera_AttachFile:

                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(CreateClassChannelActivity.this, Manifest.permission.CAMERA)
                            == PackageManager.PERMISSION_DENIED) {

                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                CAMERA_REQUEST_CODE);
                    } else {

                        cameraIntent();
                    }
                } else {
                    cameraIntent();
                }
                break;
            case R.id.sendfeedback:
                if (txtGrade.getText().toString().equals(getString(R.string.select_grade_label))) {
                    Toast.makeText(this, getString(R.string.please_select_grade), Toast.LENGTH_SHORT).show();
                } else if (txtClass.getText().toString().equals(getString(R.string.select_class_label))) {
                    Toast.makeText(this, getString(R.string.please_select_class), Toast.LENGTH_SHORT).show();
                } else if (title.getText().toString().isEmpty()) {
                    title.setError(getString(R.string.enter_title));
                } else if (msg.getText().toString().isEmpty()) {
                    msg.setError(getString(R.string.enter_msg));
                } else {
                    if (selectedFiles != null && selectedFiles.size() > 0) {
                        files.clear();
                        for (int i = 0; i < selectedFiles.size(); i++) {
                            if (i == 0)
                                UploadFile(true, selectedFiles.get(i).getPath());
                            else
                                UploadFile(false, selectedFiles.get(i).getPath());
                        }

                        mhandler.postDelayed(runnable, 1000);

                    } else {
                        sendMessage();
                    }
                }

                break;
        }
    }

    private void show_drop_down_to_select_option(RelativeLayout layout, final String which) {

        if (teacherBeans != null && teacherBeans.size() > 0) {

            listPopupWindow = new ListPopupWindow(CreateClassChannelActivity.this);
            if (which.equals(App_Constants.GRADE)) {

                gradeList.clear();
                for (GradeList.ResultBean.GradeBean beans : teacherBeans) {
                    if (!gradeList.contains(beans.getGradeId()))
                        gradeList.add(beans.getGradeId());
                }

                listPopupWindow.setAdapter(new ArrayAdapter(CreateClassChannelActivity.this, R.layout.list_dropdown_item, gradeList));

            } else if (which.equals(App_Constants.CLASS)) {

                classList.clear();
                for (GradeList.ResultBean.GradeBean beans : teacherBeans) {


                    if (txtGrade.getText().toString().equals(beans.getGradeId())) {
                        if (!classList.contains(beans.getClassX()))
                            for (int i = 0; i < beans.getClassX().size(); i++)
                                classList.add(beans.getClassX().get(i).getClassId());
                    }
                }

                if (classList != null && classList.size() > 0) {
                    Collections.sort(classList, new Comparator<String>() {
                        public int compare(String obj1, String obj2) {
                            // ## Ascending order
                            return obj1.compareToIgnoreCase(obj2); // To compare string values
                        }
                    });
                }

                listPopupWindow.setAdapter(new ArrayAdapter(CreateClassChannelActivity.this, R.layout.list_dropdown_item, classList));
            }

            listPopupWindow.setAnchorView(layout);
            listPopupWindow.setWidth(ListPopupWindow.WRAP_CONTENT);
            listPopupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);

            listPopupWindow.setModal(true);
            listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    if (which.equals(App_Constants.GRADE)) {
                        txtGrade.setText(gradeList.get(i));
                        txtClass.setText(getString(R.string.select_class_label));
                    } else if (which.equals(App_Constants.CLASS)) {
                        txtClass.setText(classList.get(i));
                    }
                    listPopupWindow.dismiss();

                }
            });
            listPopupWindow.show();
        }
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {

            if (selectedFiles != null && selectedFiles.size() > 0 && files.size() == selectedFiles.size()) {
                sendMessage();
            } else {
                mhandler.postDelayed(runnable, 5000);
            }
        }
    };

    private void UploadFile(final boolean isFirstTime, String imageName) {

        if (isFirstTime)
            Utility.showProgress(CreateClassChannelActivity.this, getString(R.string.processing));


        File file = new File(imageName);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        Call<ImageUpload> call = AdminApp.getInstance().getApi().upload(body);

        call.enqueue(new Callback<ImageUpload>() {
            @Override
            public void onResponse(Call<ImageUpload> call, Response<ImageUpload> response) {

                try {
                    if (response.isSuccessful()) {

                        if (response.body().isStatus()) {
                            if (response.body().getResult() != null) {
                                String image = response.body().getResult().getFilepath();
                                System.out.println(image);

                                SendChannelParams.FilesBean bean = new SendChannelParams.FilesBean();
                                bean.setFilePath(image);
                                files.add(bean);

                            } else {
                                Utility.hideProgress();
                                Toast.makeText(CreateClassChannelActivity.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Utility.hideProgress();
                            Toast.makeText(CreateClassChannelActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
                    Utility.hideProgress();
                    e.printStackTrace();
                    Toast.makeText(CreateClassChannelActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<ImageUpload> call, Throwable t) {
                Utility.hideProgress();
                Log.e("ERROR UPLOAD", "" + t.getMessage());
                Toast.makeText(CreateClassChannelActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void sendMessage() {

        if (!(selectedFiles != null && selectedFiles.size() > 0))
            Utility.showProgress(CreateClassChannelActivity.this, getString(R.string.processing));


        SendChannelParams params = new SendChannelParams();
        params.setChannelType("class");
        params.setGradeId(txtGrade.getText().toString());
        params.setClassId(txtClass.getText().toString());
        params.setNotificationDate(AdminApp.getInstance().currentDate());
        params.setNotificationLongMessage(msg.getText().toString());
        params.setNotificationShortMessage(title.getText().toString());
        params.setSchoolYear(AdminApp.getInstance().getAdmin().getSchoolYear());
        params.setSchoolId(AdminApp.getInstance().getAdmin().getSchoolId());
        params.setStaffEmailId(AdminApp.getInstance().getAdmin().getEmailId());
        params.setStaffId(AdminApp.getInstance().getAdmin().getId());
        params.setStaffName(AdminApp.getInstance().getAdmin().getFirstName() + " " + AdminApp.getInstance().getAdmin().getLastName());
        params.setStaffProfilePic(AdminApp.getInstance().getAdmin().getProfilePic());
        if (selectedFiles != null && selectedFiles.size() > 0) {
            params.setFiles(files);
            for (SendChannelParams.FilesBean s : files) {
                Log.e("========Files========", s.getFilePath());
            }
        }

        api = AdminApp.getInstance().getApi().sendMessageToClass(params);
        api.enqueue(new Callback<CreateSchoolChannelResponse>() {
            @Override
            public void onResponse(Call<CreateSchoolChannelResponse> call, Response<CreateSchoolChannelResponse> response) {

                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            Utility.showToast(CreateClassChannelActivity.this, response.body().getMessage());
                            setIntentData();
                        } else {
                            Utility.showSnackBar(main, response.body().getMessage());
                        }

                    } else {
                        APIError error = APIError.parseError(response, CreateClassChannelActivity.this, App_Constants.API_CREATE_CLASS_CHANNEL_MESSAGE);
                        Utility.error(CreateClassChannelActivity.this, error.message());
                    }
                } catch (Exception e) {
                    Utility.showSnackBar(main, getString(R.string.somethingwrong));
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<CreateSchoolChannelResponse> call, Throwable t) {
                if (!call.isCanceled()) {

                    Utility.hideProgress();
                    System.out.println(t.getMessage());
                    Utility.showSnackBar(main, App_Constants.NO_INTERNET);
                }
            }
        });
    }

    private void setIntentData() {

        Intent intent = new Intent();
        intent.putExtra(App_Constants.GRADE_ID, txtGrade.getText().toString());
        intent.putExtra(App_Constants.CLASS_ID, txtClass.getText().toString());
        setResult(Activity.RESULT_OK, intent);
        onBackPressed();
    }


    private void cameraIntent() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);

    }

    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, App_Constants.REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Utility.showToast(CreateClassChannelActivity.this, getString(R.string.speech_not_supported));
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == APIError.UPDATE_TOKEN && resultCode == RESULT_OK) {
            if (data != null) {
                String apiName = data.getStringExtra("api");
                onReload(apiName);
            }
        } else if (requestCode == App_Constants.FILE_SELECT && resultCode == RESULT_OK) {

            ArrayList<File> Files = (ArrayList<File>) data.getSerializableExtra(FileSelectionActivity.FILES_TO_UPLOAD);
            for (File file : Files) {
                String uri = file.getAbsolutePath();
                Log.e("PATH========", uri);
                if (file.toString().contains("/")) {
                    selectedFiles.add(new FileModel(file.toString().substring(file.toString().lastIndexOf("/") + 1), uri));
                }

            }
            // txtFileName.setVisibility(View.VISIBLE);
            // txtFileName.setText(Files.size() + " attachments");

            llAttachments.setVisibility(View.VISIBLE);
            rvAttachments.setLayoutManager(new LinearLayoutManager(CreateClassChannelActivity.this));
            rvAttachments.addItemDecoration(new DividerItemDecoration(CreateClassChannelActivity.this, DividerItemDecoration.VERTICAL));
            filesAdapter = new FilesAdapter(CreateClassChannelActivity.this);
            rvAttachments.setAdapter(filesAdapter);
            filesAdapter.addItem(selectedFiles);

        } else if (requestCode == App_Constants.REQ_CODE_SPEECH_INPUT && resultCode == RESULT_OK) {

            if (null != data) {

                ArrayList<String> result = data
                        .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

                if (msg.hasFocus())
                    msg.setText(result.get(0));
                else
                    title.setText(result.get(0));
            }
        } else if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {

            Bitmap imageBitmap = null;
            Bundle extras = data.getExtras();
            if (extras != null) {
                imageBitmap = (Bitmap) extras.get("data");

            }

            Uri tempUri = getImageUri(CreateClassChannelActivity.this, imageBitmap);
            File finalFile = new File(getRealPathFromURI(tempUri));
            String picturePath = finalFile.toString();
            System.out.println(finalFile);

            selectedFiles.add(new FileModel(picturePath.substring(picturePath.lastIndexOf("/") + 1), picturePath));

            llAttachments.setVisibility(View.VISIBLE);
            rvAttachments.setLayoutManager(new LinearLayoutManager(CreateClassChannelActivity.this));
            rvAttachments.addItemDecoration(new DividerItemDecoration(CreateClassChannelActivity.this, DividerItemDecoration.VERTICAL));
            filesAdapter = new FilesAdapter(CreateClassChannelActivity.this);
            rvAttachments.setAdapter(filesAdapter);
            filesAdapter.addItem(selectedFiles);

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }


    public void onReload(String apiName) {

        Log.e("OnReload", "Chat Screen");

        switch (apiName) {
            case App_Constants.API_CREATE_SCHOOL_CHANNEL_MESSAGE:
                sendfeedback.performClick();
                break;
            case App_Constants.API_TEACHER_CLASS_SCHEDULE:
                ApiCall();
                break;
        }

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        super.onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == CAMERA_REQUEST_CODE) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(CreateClassChannelActivity.this, getString(R.string.camera_permission_granted), Toast.LENGTH_LONG).show();
                cameraIntent();

            } else {
                Toast.makeText(CreateClassChannelActivity.this, getString(R.string.allow_app_setting), Toast.LENGTH_LONG).show();

            }

        }
    }

    public static class SendChannelParams {


        /**
         * channelType : string
         * classId : string
         * files : [{"filePath":"string"}]
         * gradeId : string
         * notificationDate : string
         * notificationLongMessage : string
         * notificationShortMessage : string
         * schoolId : string
         * schoolYear : YYYY-YYYY
         * staffEmailId : string
         * staffId : string
         * staffName : string
         * staffProfilePic : string
         */

        private String channelType;
        private String classId;
        private String gradeId;
        private String notificationDate;
        private String notificationLongMessage;
        private String notificationShortMessage;
        private String schoolId;
        private String schoolYear;
        private String staffEmailId;
        private String staffId;
        private String staffName;
        private String staffProfilePic;
        private List<FilesBean> files;

        public String getChannelType() {
            return channelType;
        }

        public void setChannelType(String channelType) {
            this.channelType = channelType;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getGradeId() {
            return gradeId;
        }

        public void setGradeId(String gradeId) {
            this.gradeId = gradeId;
        }

        public String getNotificationDate() {
            return notificationDate;
        }

        public void setNotificationDate(String notificationDate) {
            this.notificationDate = notificationDate;
        }

        public String getNotificationLongMessage() {
            return notificationLongMessage;
        }

        public void setNotificationLongMessage(String notificationLongMessage) {
            this.notificationLongMessage = notificationLongMessage;
        }

        public String getNotificationShortMessage() {
            return notificationShortMessage;
        }

        public void setNotificationShortMessage(String notificationShortMessage) {
            this.notificationShortMessage = notificationShortMessage;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getSchoolYear() {
            return schoolYear;
        }

        public void setSchoolYear(String schoolYear) {
            this.schoolYear = schoolYear;
        }

        public String getStaffEmailId() {
            return staffEmailId;
        }

        public void setStaffEmailId(String staffEmailId) {
            this.staffEmailId = staffEmailId;
        }

        public String getStaffId() {
            return staffId;
        }

        public void setStaffId(String staffId) {
            this.staffId = staffId;
        }

        public String getStaffName() {
            return staffName;
        }

        public void setStaffName(String staffName) {
            this.staffName = staffName;
        }

        public String getStaffProfilePic() {
            return staffProfilePic;
        }

        public void setStaffProfilePic(String staffProfilePic) {
            this.staffProfilePic = staffProfilePic;
        }

        public List<FilesBean> getFiles() {
            return files;
        }

        public void setFiles(List<FilesBean> files) {
            this.files = files;
        }

        public static class FilesBean {
            /**
             * filePath : string
             */

            private String filePath;

            public String getFilePath() {
                return filePath;
            }

            public void setFilePath(String filePath) {
                this.filePath = filePath;
            }
        }
    }
}
