/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.classchannel;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.ListPopupWindow;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;

import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.Listeners.OnReloadListener;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.DateFunction;
import ca.dataready.SmartSchoolAdmin.Utilities.LastItemDecoration;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.classchannel.adapter.ClassChannelAdapter;
import ca.dataready.SmartSchoolAdmin.classchannel.create.CreateClassChannelActivity;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.ClassChannelResponse;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.GradeList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ClassChannelFragment extends Fragment implements OnReloadListener, SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.txt_Grade)
    TextView txtGrade;
    @BindView(R.id.grade_img)
    ImageView gradeImg;
    @BindView(R.id.linear_Grade)
    RelativeLayout linearGrade;
    @BindView(R.id.txt_Class)
    TextView txtClass;
    @BindView(R.id.class_img)
    ImageView classImg;
    @BindView(R.id.linear_Section)
    RelativeLayout linearSection;
    @BindView(R.id.llheader)
    LinearLayout llheader;
    @BindView(R.id.recyclerView)
    SuperRecyclerView recyclerView;
    @BindView(R.id.parent_viewAnimator)
    ViewAnimator parentViewAnimator;
    @BindView(R.id.fab_create)
    FloatingActionButton fabCreate;
    Unbinder unbinder;
    private Call<GradeList> call;
    private ArrayList<GradeList.ResultBean.GradeBean> teacherBeans;
    private ListPopupWindow listPopupWindow;
    private List<String> gradeList = new ArrayList<>();
    private List<String> classList = new ArrayList<>();
    private String gradeId;
    private String classId;
    private Call<ClassChannelResponse> appApi;
    private ArrayList<ClassChannelResponse.ResultBean> results;
    private ClassChannelAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_class_channel, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) getActivity()).setSubTitle(getString(R.string.class_channel));
        ((HomeActivity) getActivity()).setOnReloadListener(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new LastItemDecoration());
        recyclerView.setRefreshListener(ClassChannelFragment.this);

        if (savedInstanceState != null) {

            results = savedInstanceState.getParcelableArrayList(App_Constants.OBJECT);
            teacherBeans = savedInstanceState.getParcelableArrayList(App_Constants.TEACHER_CLASS_SCHEDULE_OBJECT);
            gradeId = savedInstanceState.getString(App_Constants.GRADE_ID);
            classId = savedInstanceState.getString(App_Constants.CLASS_ID);

            txtGrade.setText(gradeId);
            txtClass.setText(classId);

            if (results != null && results.size() > 0) {

                setData();

            } else {

                adapter = new ClassChannelAdapter(getActivity());
                recyclerView.setAdapter(adapter);
                TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                emptyView.setText(getString(R.string.no_data));
                parentViewAnimator.setDisplayedChild(1);
            }

        } else {

            Init();
        }

    }

    private void Init() {

        ApiCall();
    }


    private void ApiCall() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();
        if (entity == null)
            return;

        call = AdminApp.getInstance().getApi().getClassSchedule(entity.getSchoolId(), entity.getSchoolYear());
        call.enqueue(new Callback<GradeList>() {
            @Override
            public void onResponse(Call<GradeList> call, Response<GradeList> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            teacherBeans = response.body().getResult().getGrade();
                        } else {
                            Utility.showSnackBar(recyclerView, response.body().getMessage());
                        }

                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_TEACHER_CLASS_SCHEDULE);
                        Utility.error(getActivity(), error.message());
                        parentViewAnimator.setDisplayedChild(1);

                    }
                } catch (Exception e) {

                    Utility.showSnackBar(recyclerView, getString(R.string.somethingwrong));
                    e.printStackTrace();

                }
                getClassNotification();
            }

            @Override
            public void onFailure(Call<GradeList> call, Throwable t) {
                if (!call.isCanceled()) {

                    getClassNotification();
                    System.out.println(t.getMessage());
                    Utility.showSnackBar(recyclerView, App_Constants.NO_INTERNET);

                }
            }
        });
    }

    private void show_drop_down_to_select_option(RelativeLayout layout, final String which) {

        if (teacherBeans != null && teacherBeans.size() > 0) {

            listPopupWindow = new ListPopupWindow(getActivity());
            if (which.equals(App_Constants.GRADE)) {

                gradeList.clear();
                for (GradeList.ResultBean.GradeBean beans : teacherBeans) {
                    if (!gradeList.contains(beans.getGradeId()))
                        gradeList.add(beans.getGradeId());
                }

                listPopupWindow.setAdapter(new ArrayAdapter(getActivity(), R.layout.list_dropdown_item, gradeList));

            } else if (which.equals(App_Constants.CLASS)) {

                classList.clear();
                for (GradeList.ResultBean.GradeBean beans : teacherBeans) {

                    if (txtGrade.getText().toString().equals(beans.getGradeId())) {
                        if (!classList.contains(beans.getClassX()))
                            for (int i = 0; i < beans.getClassX().size(); i++)
                                classList.add(beans.getClassX().get(i).getClassId());
                    }
                }

                if (classList != null && classList.size() > 0) {
                    Collections.sort(classList, new Comparator<String>() {
                        public int compare(String obj1, String obj2) {
                            // ## Ascending order
                            return obj1.compareToIgnoreCase(obj2); // To compare string values
                        }
                    });
                }

                listPopupWindow.setAdapter(new ArrayAdapter(getActivity(), R.layout.list_dropdown_item, classList));
            }

            listPopupWindow.setAnchorView(layout);
            listPopupWindow.setWidth(ListPopupWindow.WRAP_CONTENT);
            listPopupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);

            listPopupWindow.setModal(true);
            listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    if (which.equals(App_Constants.GRADE)) {
                        txtGrade.setText(gradeList.get(i));
                        gradeId = gradeList.get(i);
                        txtClass.setText(getString(R.string.select_class_label));
                    } else if (which.equals(App_Constants.CLASS)) {
                        txtClass.setText(classList.get(i));
                        classId = classList.get(i);
                        recyclerView.showProgress();
                        getClassNotification();
                    }
                    listPopupWindow.dismiss();

                }
            });
            listPopupWindow.show();
        }
    }


    private void getClassNotification() {

        if (txtGrade.getText().toString().equals(getString(R.string.select_grade_label))) {
            gradeId = "";
        }

        if (txtGrade.getText().toString().equals(getString(R.string.select_class_label))) {
            classId = "";
        }

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();

        appApi = AdminApp.getInstance().getApi().getClassChannelNotifications(entity.getSchoolId(), AdminApp.getInstance().getAdmin().getSchoolYear(), gradeId, classId);
        appApi.enqueue(new Callback<ClassChannelResponse>() {
            @Override
            public void onResponse(Call<ClassChannelResponse> call, Response<ClassChannelResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            results = response.body().getResult();
                            if (results != null && results.size() > 0) {

                                setData();

                            } else {

                                results = null;
                                adapter = new ClassChannelAdapter(getActivity());
                                recyclerView.setAdapter(adapter);
                                TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                                emptyView.setText(getString(R.string.no_data));
                                parentViewAnimator.setDisplayedChild(1);
                            }

                        } else {

                            results = null;
                            adapter = new ClassChannelAdapter(getActivity());
                            recyclerView.setAdapter(adapter);
                            TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                            emptyView.setText(getString(R.string.no_data));
                            parentViewAnimator.setDisplayedChild(1);
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_CLASS_CHANNEL_DATA);
                        adapter = new ClassChannelAdapter(getActivity());
                        recyclerView.setAdapter(adapter);
                        TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                        emptyView.setText(error.message());
                        parentViewAnimator.setDisplayedChild(1);

                    }
                } catch (Exception e) {

                    adapter = new ClassChannelAdapter(getActivity());
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(getString(R.string.somethingwrong));
                    parentViewAnimator.setDisplayedChild(1);
                }

            }

            @Override
            public void onFailure(Call<ClassChannelResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    parentViewAnimator.setDisplayedChild(1);
                    adapter = new ClassChannelAdapter(getActivity());
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(App_Constants.NO_INTERNET);
                }
            }
        });

    }


    private void setData() {

        adapter = new ClassChannelAdapter(getActivity());
        recyclerView.setAdapter(adapter);

        List<String> headers = new ArrayList<>();
        List<ClassChannelResponse.ResultBean> schoolChannelBeans = new ArrayList<>();

        for (int i = 0; i < results.size(); i++) {

            if (results.get(i).getCreationDate() != null && results.get(i).getCreationDate().contains(" ")) {

                String formattedDate = DateFunction.ConvertDate(results.get(i).getCreationDate(), "yyyy-MM-dd hh:mm:ss", "dd MMMM yyyy");

                String[] pDates = formattedDate.split(" ");
                if (pDates.length > 2) {
                    String month = pDates[1];
                    String year = pDates[2];
                    if (!headers.contains(month)) {
                        headers.add(month);
                        ClassChannelResponse.ResultBean bean = new ClassChannelResponse.ResultBean();
                        bean.setCreationDate(results.get(i).getCreationDate());
                        bean.setType(1);
                        bean.setMonth(month + " " + year);
                        schoolChannelBeans.add(bean);
                        schoolChannelBeans.add(results.get(i));
                    } else {
                        schoolChannelBeans.add(results.get(i));
                    }
                } else {
                    schoolChannelBeans.add(results.get(i));
                }
            }
        }

        adapter.addItem(schoolChannelBeans);
        parentViewAnimator.setDisplayedChild(1);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.linear_Grade, R.id.linear_Section, R.id.fab_create})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linear_Grade:
                show_drop_down_to_select_option(linearGrade, App_Constants.GRADE);
                break;
            case R.id.linear_Section:
                show_drop_down_to_select_option(linearSection, App_Constants.CLASS);
                break;
            case R.id.fab_create:

                startActivityForResult(new Intent(getActivity(), CreateClassChannelActivity.class), App_Constants.UPDATE_LISTING);
                if (getActivity() != null)
                    getActivity().overridePendingTransition(R.anim.enter, R.anim.leave);

                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == App_Constants.UPDATE_LISTING && resultCode == Activity.RESULT_OK) {

            if (isVisible()) {

                if (data != null) {

                    classId = data.getStringExtra(App_Constants.CLASS_ID);
                    gradeId = data.getStringExtra(App_Constants.GRADE_ID);

                    txtClass.setText(classId);
                    txtGrade.setText(gradeId);

                    getClassNotification();
                }
            }
        }
    }


    @Override
    public void onRefresh() {

        if (isVisible())
            getClassNotification();
    }

    @Override
    public void onReload(String apiName) {

        if (apiName != null) {

            if (isVisible()) {
                if (apiName.equals(App_Constants.API_CLASS_CHANNEL_DATA)) {
                    getClassNotification();
                } else if (apiName.equals(App_Constants.API_TEACHER_CLASS_SCHEDULE)) {
                    ApiCall();
                }
            }
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        if (appApi != null)
            appApi.cancel();
        if (call != null)
            call.cancel();
    }
}
