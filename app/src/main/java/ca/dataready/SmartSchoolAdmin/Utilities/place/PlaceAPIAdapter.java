/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.Utilities.place;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;

/**
 * Created by social_jaydeep on 15/12/16.
 */

public class PlaceAPIAdapter extends ArrayAdapter<String> implements Filterable {

    ArrayList<String> resultList;

    Context mContext;
    int mResource;

    PlaceAPI mPlaceAPI = new PlaceAPI();

    public PlaceAPIAdapter(Context context, int resource) {
        super(context, resource);

        mContext = context;
        mResource = resource;
    }

    @Override
    public int getCount() {
        // Last item will be the footer
        return resultList.size();
    }

    @Override
    public String getItem(int position) {
        return resultList.get(position);
    }

    @Override
    public Filter getFilter() {

        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                try {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        resultList = mPlaceAPI.autocomplete(constraint.toString());

                        filterResults.values = resultList;
                        filterResults.count = resultList.size();

                        return filterResults;

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                try {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        };

        return filter;
    }
}
