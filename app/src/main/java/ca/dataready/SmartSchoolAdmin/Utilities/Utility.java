/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.Utilities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;

import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;


/**
 * Created by c217 on 30/12/16.
 */

public class Utility {
    public static final String TAG = Utility.class.getSimpleName();


    private static AlertDialog dialogOffline;
    private static AlertDialog dialogServerAlert;
    private static Snackbar snackbar;
    public static final Calendar myCalendar = Calendar.getInstance();
    private static ProgressDialog pd;

    /**
     * common method to launch new activity.
     *
     * @param context
     * @param classToOpen
     */
    public static void launchEntryIntent(Activity context, Class classToOpen) {
        Intent intent = new Intent(context, classToOpen);
        context.startActivity(intent);
        context.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public static void launchExitIntent(Activity context, Class classToOpen) {
        Intent intent = new Intent(context, classToOpen);
        context.startActivity(intent);
        context.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);//2
    }

    public static void launchIntent(Activity activity, Class classtoOpen) {
        Intent intent = new Intent(activity, classtoOpen);
        activity.startActivity(intent);
    }

    public static void error(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void alert(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void msg(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void showProgress(Context context, String msg) {
        pd = new ProgressDialog(context);
        pd.setMessage(msg);
        pd.setCancelable(false);
        pd.show();
    }

    public static void hideProgress() {
        if (pd != null && pd.isShowing()) {
            pd.dismiss();
        }
    }

    public static String random() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(4);
        char tempChar;
        for (int i = 0; i < randomLength; i++) {
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            if (Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }


    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public static boolean checkImageResource(Context ctx, ImageView imageView,
                                             int imageResource) {
        boolean result = false;

        if (ctx != null && imageView != null && imageView.getDrawable() != null) {
            Drawable.ConstantState constantState;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                constantState = ctx.getResources()
                        .getDrawable(imageResource, ctx.getTheme())
                        .getConstantState();
            } else {
                constantState = ctx.getResources().getDrawable(imageResource)
                        .getConstantState();
            }

            if (imageView.getDrawable().getConstantState() == constantState) {
                result = true;
            }
        }

        return result;
    }


//    public static void showPopup(final Activity context, final EditText editText) {
//        int w = context.getWindow().getWindowManager().getDefaultDisplay().getWidth();
//        int h = context.getWindow().getWindowManager().getDefaultDisplay().getHeight();
//        final Dialog dialog = new Dialog(context);
//        dialog.setCancelable(false);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.select_gender);
//        dialog.getWindow().getAttributes().windowAnimations = R.style.animationName;
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.copyFrom(dialog.getWindow().getAttributes());
//        lp.width = (int) (w * 1.01);
//        lp.height = (int) (h / 3.3);
//        lp.gravity = Gravity.BOTTOM;
//        dialog.getWindow().setAttributes(lp);
//        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//        dialog.show();
//
//        dialog.findViewById(R.id.txt_male).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//                editText.setText(R.string.reg_male);
//            }
//        });
//        dialog.findViewById(R.id.txt_female).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//                editText.setText(R.string.reg_female);
//            }
//        });
//        dialog.findViewById(R.id.txt_other).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//    }


    /**
     * These is the method to show toast in android.
     *
     * @param message
     * @param context
     */
    public static void showToast(Context context, String message) {
        try {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e(TAG, "showToast Exception : " + e.toString());
        }
    }

    /**
     * To check for the connectivity.
     *
     * @param context
     * @return
     */
    public static boolean isConnected(Context context) {
        NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }


    /**
     * Show message when offline.
     *
     * @param context
     */
    public static void alertOffline(Context context) {
        if (dialogOffline == null || !dialogOffline.isShowing()) {
            dialogOffline = alert(context,
                    context.getString(R.string.msg_server_connection), context.getString(R.string.msg_internet_connection));
        }
    }


    /**
     * Common dialog to show for alerts.
     *
     * @param context
     * @param title
     * @param message
     * @return
     */
    public static AlertDialog alert(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (title != null) {
            builder.setTitle(title);
        }
        if (message != null) {
            builder.setMessage(message);
        }
        AlertDialog dialog = builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).create();

//        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);

        dialog.show();

        return dialog;
    }

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static String getRealPathFromURI(Context context, Uri uri) {
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
//    /**
//     * Alert for cant connect to server.
//     *
//     * @param context
//     */
//    public static void alertServerNotConnected(Context context) {
//        if (dialogServerAlert == null || !dialogServerAlert.isShowing()) {
//            dialogServerAlert = alert(context, context.getString(R.string.connectivity_problem), context.getString(R.string.msg_server_connection));
//        }
//    }

    public static void showSnackBar(View view, String msg) {
        try {
            snackbar = Snackbar
                    .make(view, msg, Snackbar.LENGTH_LONG);
            snackbar.setAction("", null);
            snackbar.show();
        } catch (Exception e) {
            e.fillInStackTrace();
        }

    }

    public static void setDateTimeField(Activity activity, final TextView textView, final String myFormat, boolean isDatePicker) {

        if (isDatePicker) {
            DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    // TODO Auto-generated method stub
                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    updateLabel(textView, myFormat, true, "", "", "");
                }

            };

            DatePickerDialog dialog = new DatePickerDialog(activity, R.style.datepickerCustom, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));
//        dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            dialog.show();
        } else {
            int hour = myCalendar.get(Calendar.HOUR_OF_DAY);
            int minute = myCalendar.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(activity, R.style.datepickerCustom, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                    String AM_PM;
                    if (selectedHour < 12) {
                        AM_PM = "AM";
                    } else {
                        AM_PM = "PM";
                        selectedHour = selectedHour % 12;
                        if(selectedHour==0){
                            selectedHour = 12;
                        }
                    }

                    if (selectedHour < 10 && selectedMinute < 10) {
                        updateLabel(textView, "", false, String.valueOf("0" + selectedHour), String.valueOf("0" + selectedMinute), AM_PM);
                    } else if (selectedHour < 10 || selectedMinute < 10) {
                        if (selectedHour < 10) {
                            updateLabel(textView, "", false, String.valueOf("0" + selectedHour), String.valueOf(selectedMinute), AM_PM);
                        } else {
                            updateLabel(textView, "", false, String.valueOf(selectedHour), String.valueOf("0" + selectedMinute), AM_PM);
                        }
                    } else {
                        updateLabel(textView, "", false, String.valueOf(selectedHour), String.valueOf(selectedMinute), AM_PM);
                    }
                }
            }, hour, minute, true);//Yes 24 hour time
            mTimePicker.setTitle("Select Time");
            mTimePicker.show();

        }
//        new DatePickerDialog(activity, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
//                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }


    public static void setDateTimeField(Activity activity, final TextView textView, final String myFormat, boolean isDatePicker, long maxDate) {

        if (isDatePicker) {
            DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    // TODO Auto-generated method stub
                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    updateLabel(textView, myFormat, true, "", "", "");
                }

            };

            DatePickerDialog dialog = new DatePickerDialog(activity, R.style.datepickerCustom, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));

            dialog.getDatePicker().setMaxDate(maxDate);
//        dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            dialog.show();
        } else {
            int hour = myCalendar.get(Calendar.HOUR_OF_DAY);
            int minute = myCalendar.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(activity, R.style.datepickerCustom, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                    String AM_PM;
                    if (selectedHour < 12) {
                        AM_PM = "AM";
                    } else {
                        AM_PM = "PM";
                        selectedHour = selectedHour % 12;
                        if(selectedHour==0){
                            selectedHour = 12;
                        }
                    }

                    if (selectedHour < 10 && selectedMinute < 10) {
                        updateLabel(textView, "", false, String.valueOf("0" + selectedHour), String.valueOf("0" + selectedMinute), AM_PM);
                    } else if (selectedHour < 10 || selectedMinute < 10) {
                        if (selectedHour < 10) {
                            updateLabel(textView, "", false, String.valueOf("0" + selectedHour), String.valueOf(selectedMinute), AM_PM);
                        } else {
                            updateLabel(textView, "", false, String.valueOf(selectedHour), String.valueOf("0" + selectedMinute), AM_PM);
                        }
                    } else {
                        updateLabel(textView, "", false, String.valueOf(selectedHour), String.valueOf(selectedMinute), AM_PM);
                    }
                }
            }, hour, minute, true);//Yes 24 hour time
            mTimePicker.setTitle("Select Time");
            mTimePicker.show();

        }
//        new DatePickerDialog(activity, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
//                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    public static void updateLabel(TextView textView, String myFormat, boolean isDatePicker, String selectedHour, String selectedMinute, String AM_PM) {

//        String myFormat = "yyyy/MM/dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        if (isDatePicker) {
            textView.setText(sdf.format(myCalendar.getTime()));
        } else {
            textView.setText(selectedHour + ":" + selectedMinute + " " + AM_PM);
        }
    }

    /**
     * Hide Keyboard
     *
     * @param mContext
     */
    public static void hideKeyboard(Context mContext) {

        InputMethodManager imm = (InputMethodManager) mContext
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(((Activity) mContext).getWindow()
                .getCurrentFocus().getWindowToken(), 0);
    }


    public static void askAlertPermission(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.ACCESS_FINE_LOCATION}, App_Constants.REQ_LOCATION);
            if (!Settings.canDrawOverlays(activity)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + activity.getPackageName()));
                activity.startActivityForResult(intent, 1);
            }
        }
    }


    /**
     * Open keyboard programmatically.
     *
     * @param view
     * @param context
     */
    public static void showSoftKeyboard(View view, Context context) {
        if (view.requestFocus()) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    public static String ChangeDateFormat(String inputDate) {
        DateFormat inputFormat = new SimpleDateFormat("MM/dd/yyyy");
        DateFormat outputFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = null;
        try {
            date = inputFormat.parse(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputDateStr = outputFormat.format(date);
        return outputDateStr;
    }

    public static Bitmap takeScreenShot(Activity activity, int width, int height, boolean is_home) {
        View view = activity.getWindow().getDecorView();
        Bitmap b = null;
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        Bitmap b1 = view.getDrawingCache();
        Rect frame = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
        int statusBarHeight = frame.top;
        if (is_home) {
            b = Bitmap.createBitmap(b1, 0, statusBarHeight, width, height - (statusBarHeight * 3));
            Log.e("IF CALLED", "::" + statusBarHeight * 3);
        } else {
            b = Bitmap.createBitmap(b1, 0, statusBarHeight - 10, width, (int) (height / 5.25));
            Log.e("ELSE CALLED", "::" + statusBarHeight);
        }
        view.destroyDrawingCache();
        return b;
    }

    public static boolean isNotEmptyEditText(EditText editText) {
        return !editText.getText().toString().trim().isEmpty();
    }

    public static int dpAsPixels(float scale, int sizeInDp) {

        return (int) (sizeInDp * scale + 0.5f);
    }

}