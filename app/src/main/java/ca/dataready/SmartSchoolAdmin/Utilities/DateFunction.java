/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.Utilities;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by c161 on 14/06/17.
 */

public class DateFunction {

    public static String ConvertDate(String tempdate, String from, String to) {
        SimpleDateFormat oldFormat = new SimpleDateFormat(from, Locale.ENGLISH);
        SimpleDateFormat newFormat = new SimpleDateFormat(to, Locale.ENGLISH);

        Date date = null;
        String str = tempdate;

        try {
            date = oldFormat.parse(tempdate);
            str = newFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;


    }

    public static String getTodayDate() {

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        return day + "-" + month + "-" + year;
    }

    public static String getTommorowDate() {

        Calendar calendar = Calendar.getInstance();

        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        return dateFormat.format(tomorrow);

    }

    public static Date ConvertNotificationDate(String tempdate) {


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
        Date date = null;
        try {
            date = sdf.parse(tempdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;

    }

    public static Date ConvertDate(String tempdate, String format) {


        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.ENGLISH);
        Date date = null;
        try {
            date = sdf.parse(tempdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;

    }

    public static int compareTime(String startTime, String endTime) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        Date d1 = sdf.parse(startTime);
        Date d2 = sdf.parse(endTime);
        Log.e("Strat Date", String.valueOf(d1));
        Log.e("End Date", String.valueOf(d2));
        return d2.compareTo(d1);
    }

    public static int compareDateWithTodayDate(String SELECTED_DATE) {

        Calendar calNow = Calendar.getInstance();
        Calendar calSelected = (Calendar) calNow.clone();
        String temp_date = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(Calendar.getInstance().getTime());
        Date currentDate = DateFunction.ConvertDate(temp_date, "yyyy-MM-dd");
        Log.d("Current Date", String.valueOf(currentDate));
        calNow.setTime(currentDate);
        calNow.set(Calendar.HOUR_OF_DAY, 0);
        calNow.set(Calendar.MINUTE, 0);
        calNow.set(Calendar.SECOND, 0);


        Date selectedDate = DateFunction.ConvertDate(SELECTED_DATE, "yyyy-MM-dd");
        Log.d("Selected Date", String.valueOf(selectedDate));
        calSelected.setTime(selectedDate);
        calSelected.set(Calendar.HOUR_OF_DAY, 0);
        calSelected.set(Calendar.MINUTE, 0);
        calSelected.set(Calendar.SECOND, 0);

        return calSelected.compareTo(calNow);
    }

    public static String get24htime(String time) throws ParseException {
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
        Date date = parseFormat.parse(time);
        return displayFormat.format(date);
    }

    public static String get15DayBeforeDate(String endDate) {


        Date temp_endDate = DateFunction.ConvertDate(endDate, "yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(temp_endDate);
        cal.add(Calendar.DAY_OF_YEAR, -15);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);

        return new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(cal.getTime());
    }
}
