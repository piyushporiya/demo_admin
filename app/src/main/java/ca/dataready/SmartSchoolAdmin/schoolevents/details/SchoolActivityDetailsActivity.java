/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.schoolevents.details;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.DateFunction;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.server.SchoolActivityResponse;
import ca.dataready.SmartSchoolAdmin.viewresponse.adapter.FilesAdapter;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.FileModel;


public class SchoolActivityDetailsActivity extends BaseActivity {

    public static final String OBJECT = "object";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    SchoolActivityResponse.ResultBean entity;
    @BindView(R.id.txt_date_value)
    TextView txtDateValue;
    @BindView(R.id.txt_start_time_value)
    TextView txtStartTimeValue;
    @BindView(R.id.txt_end_time_value)
    TextView txtEndTimeValue;
    @BindView(R.id.txt_repeat_value)
    TextView txtRepeatValue;
    @BindView(R.id.txt_title_value)
    TextView txtTitleValue;
    @BindView(R.id.txt_desc_value)
    TextView txtDescValue;
    @BindView(R.id.rvAttachments)
    RecyclerView rvAttachments;
    @BindView(R.id.llAttachments)
    LinearLayout llAttachments;
    @BindView(R.id.parent_viewAnimator)
    ViewAnimator parentViewAnimator;
    private FilesAdapter filesAdapter;
    private List<FileModel> selectedFiles = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_events_details);
        ButterKnife.bind(this);
        setHeaderInfo();

        if (getIntent().getExtras() != null) {

            entity = getIntent().getParcelableExtra(OBJECT);
            if (entity != null) {
                setValues(entity);
            }
        }
    }

    private void setValues(SchoolActivityResponse.ResultBean entity) {

        txtDateValue.setText(DateFunction.ConvertDate(entity.getNotificationDate(), "yyyy-MM-dd", "dd MMM yyy"));
        if (entity.getNotificationEndDate() != null)
            txtRepeatValue.setText(entity.getTimePeriod() + " ("+getString(R.string.until)+ " " + DateFunction.ConvertDate(entity.getNotificationEndDate(), "yyyy-MM-dd", "dd MMM yyy") + ")");
        else if (entity.getTimePeriod() != null && entity.getTimePeriod().isEmpty())
            txtRepeatValue.setText(getString(R.string.no_repeat));
        else if(entity.getTimePeriod() == null)
            txtRepeatValue.setText(getString(R.string.no_repeat));
        else
            txtRepeatValue.setText(entity.getTimePeriod());

        txtStartTimeValue.setText(entity.getNotificationStartTime());
        txtEndTimeValue.setText(entity.getNotificationEndTime());
        txtTitleValue.setText(entity.getNotificationShortMessage());
        txtDescValue.setText(entity.getNotificationLongMessage());

        if (entity.getFiles() != null && entity.getFiles().size() > 0) {

            rvAttachments.setLayoutManager(new LinearLayoutManager(SchoolActivityDetailsActivity.this));
            filesAdapter = new FilesAdapter(SchoolActivityDetailsActivity.this, true);
            rvAttachments.setAdapter(filesAdapter);
            selectedFiles.clear();
            if (entity.getFiles() != null && entity.getFiles().size() > 0) {
                for (SchoolActivityResponse.ResultBean.FilesBean model : entity.getFiles()) {
                    if (model.getFilePath().contains("/")) {
                        selectedFiles.add(new FileModel(model.getFilePath().substring(model.getFilePath().lastIndexOf("/") + 1), model.getFilePath()));
                    }
                }
            }
            filesAdapter.addItem(selectedFiles);

        } else {

            llAttachments.setVisibility(View.GONE);
        }

        parentViewAnimator.setDisplayedChild(1);
    }

    private void setHeaderInfo() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setElevation(5);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + getString(R.string.school_events_details) + "</small>"));
        getSupportActionBar().setTitle(AdminApp.getInstance().getAdmin().getSchoolName() +" "+AdminApp.getInstance().getAdmin().getSchoolYear());

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
