/*
 * Copyright (c) 2018.
 *
 *  ************************************************************************
 *     DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *   ________________________________________________________________________
 *
 *     [2010] - [2018] SripathiSolutions Corp.
 *      [2010] - [2018] DataReady Technology Corp.
 *    ________________________________________________________________________
 *
 *     All Rights Reserved.
 *
 *     NOTICE:  All information contained herein is, and remains
 *     the property of DataReady Technology  and its suppliers,
 *     if any.  The intellectual and technical concepts contained
 *     herein are proprietary to DataReady Technology Incorporated
 *     and its suppliers and may be covered by U.S. and Foreign Patents,
 *     patents in process, and are protected by trade secret or copyright law.
 *     Dissemination of this information or reproduction of this material
 *     is strictly forbidden unless prior written permission is obtained
 *     from DataReady Technology Incorporated.
 *
 *    *************************************************************************
 *    DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *    *************************************************************************
 *
 */

package ca.dataready.SmartSchoolAdmin.schoolevents.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.DateFunction;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.classevents.adapter.TodaysClassEventsAdapter;
import ca.dataready.SmartSchoolAdmin.schoolevents.SchoolEventsFragment;
import ca.dataready.SmartSchoolAdmin.schoolevents.details.SchoolActivityDetailsActivity;
import ca.dataready.SmartSchoolAdmin.server.AppApi;
import ca.dataready.SmartSchoolAdmin.server.SchoolActivityResponse;
import ca.dataready.SmartSchoolAdmin.viewresponse.adapter.FilesAdapter;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.FileModel;
import ca.dataready.SmartSchoolAdmin.web.WebActivity;

public class TodaysSchoolEventsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private SchoolEventsFragment schoolEventsFragment;
    private static final int TYPE_VIEW = 0;
    private static final int TYPE_HEADER = 1;
    private List<SchoolActivityResponse.ResultBean> beans;
    private List<FileModel> selectedFiles = new ArrayList<>();
    private FilesAdapter filesAdapter;

    public TodaysSchoolEventsAdapter(Context context, SchoolEventsFragment schoolEventsFragment) {
        this.context = context;
        this.schoolEventsFragment = schoolEventsFragment;
        beans = new ArrayList<>();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new ItemViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_today_school_activity, parent, false));

    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        final SchoolActivityResponse.ResultBean entity = beans.get(position);

        if (holder instanceof ItemViewHolder) {


            String formattedDate = DateFunction.ConvertDate(entity.getNotificationDate(), "yyyy-MM-dd", "dd MMM yyyy");
            ((ItemViewHolder) holder).txtDate.setText(formattedDate);
            ((ItemViewHolder) holder).txtTime.setText(context.getString(R.string.start_at) + " " + entity.getNotificationStartTime()
                    + " " + context.getString(R.string.end_at) + "  " + entity.getNotificationEndTime());
            ((ItemViewHolder) holder).txtMsg.setText(entity.getNotificationShortMessage());
            ((ItemViewHolder) holder).txtLongMsg.setText(entity.getNotificationLongMessage());

            /*if (entity.isUpcomingEvent()) {

                ((ItemViewHolder) holder).status.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));

            } else if (entity.isTodaysEvent()) {

                ((ItemViewHolder) holder).status.setBackgroundColor(ContextCompat.getColor(context, R.color.green));

            } else {

                ((ItemViewHolder) holder).status.setBackgroundColor(ContextCompat.getColor(context, R.color.gray));
            }*/

            if (entity.getFiles() != null && entity.getFiles().size() > 0) {

                ((ItemViewHolder) holder).recyclerView.setLayoutManager(new LinearLayoutManager(context));
                filesAdapter = new FilesAdapter(context, true);
                ((ItemViewHolder) holder).recyclerView.setAdapter(filesAdapter);
                selectedFiles.clear();
                if (entity.getFiles() != null && entity.getFiles().size() > 0) {
                    for (SchoolActivityResponse.ResultBean.FilesBean model : entity.getFiles()) {
                        if (model.getFilePath().contains("/")) {
                            selectedFiles.add(new FileModel(model.getFilePath().substring(model.getFilePath().lastIndexOf("/") + 1), model.getFilePath()));
                        }
                    }
                }
                filesAdapter.addItem(selectedFiles);

            }

            ((ItemViewHolder) holder).txtLongMsg.setMaxLines(Integer.MAX_VALUE);

            if (entity.isExpanded()) {

                ((ItemViewHolder) holder).txtViewMore.setVisibility(View.VISIBLE);
                ((ItemViewHolder) holder).txtViewMore.setText(App_Constants.VIEW_LESS);
                ((ItemViewHolder) holder).txtLongMsg.setMaxLines(Integer.MAX_VALUE);
                ((ItemViewHolder) holder).recyclerView.setVisibility(View.VISIBLE);
                ((ItemViewHolder) holder).txtFileName.setVisibility(View.GONE);

            } else {

                if (entity.getFiles() != null && entity.getFiles().size() > 1) {

                    ((ItemViewHolder) holder).txtLongMsg.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                        @Override
                        public boolean onPreDraw() {
                            ((ItemViewHolder) holder).txtLongMsg.getViewTreeObserver().removeOnPreDrawListener(this);
                            int lineCount = ((ItemViewHolder) holder).txtLongMsg.getLineCount();
                            Log.e("TreeObserver", "onBind: " + lineCount);
                            if (lineCount > 1)
                                ((ItemViewHolder) holder).txtLongMsg.setMaxLines(1);

                            return false;
                        }
                    });

                    ((ItemViewHolder) holder).txtViewMore.setVisibility(View.VISIBLE);
                    ((ItemViewHolder) holder).recyclerView.setVisibility(View.GONE);
                    ((ItemViewHolder) holder).txtFileName.setVisibility(View.VISIBLE);
                    ((ItemViewHolder) holder).txtFileName.setText(selectedFiles.get(0).getName());


                } else if (entity.getFiles() != null && entity.getFiles().size() > 0) {

                    ((ItemViewHolder) holder).txtViewMore.setVisibility(View.GONE);
                    ((ItemViewHolder) holder).recyclerView.setVisibility(View.GONE);
                    ((ItemViewHolder) holder).txtFileName.setVisibility(View.VISIBLE);
                    ((ItemViewHolder) holder).txtFileName.setText(selectedFiles.get(0).getName());

                    ((ItemViewHolder) holder).txtLongMsg.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                        @Override
                        public boolean onPreDraw() {
                            ((ItemViewHolder) holder).txtLongMsg.getViewTreeObserver().removeOnPreDrawListener(this);
                            int lineCount = ((ItemViewHolder) holder).txtLongMsg.getLineCount();
                            Log.e("TreeObserver", "onBind: " + lineCount);
                            if (lineCount > 1) {
                                ((ItemViewHolder) holder).txtViewMore.setVisibility(View.VISIBLE);
                                ((ItemViewHolder) holder).txtLongMsg.setMaxLines(1);
                            } else {
                                ((ItemViewHolder) holder).txtViewMore.setVisibility(View.GONE);
                            }

                            return false;
                        }
                    });

                } else {

                    ((ItemViewHolder) holder).txtViewMore.setVisibility(View.GONE);
                    ((ItemViewHolder) holder).recyclerView.setVisibility(View.GONE);
                    ((ItemViewHolder) holder).txtFileName.setVisibility(View.GONE);

                    ((ItemViewHolder) holder).txtLongMsg.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                        @Override
                        public boolean onPreDraw() {
                            ((ItemViewHolder) holder).txtLongMsg.getViewTreeObserver().removeOnPreDrawListener(this);
                            int lineCount = ((ItemViewHolder) holder).txtLongMsg.getLineCount();
                            Log.e("TreeObserver", "onBind: " + lineCount);
                            if (lineCount > 1) {
                                ((ItemViewHolder) holder).txtViewMore.setVisibility(View.VISIBLE);
                                ((ItemViewHolder) holder).txtLongMsg.setMaxLines(1);
                            } else {
                                ((ItemViewHolder) holder).txtViewMore.setVisibility(View.GONE);
                            }

                            return false;
                        }
                    });

                }


                ((ItemViewHolder) holder).txtViewMore.setText(App_Constants.VIEW_MORE);
                //   ((Dataholder) holder).txtLongMsg.setMaxLines(1);

            }

            ((ItemViewHolder) holder).txtFileName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (entity.getFiles() != null && entity.getFiles().size() > 0) {
                        context.startActivity(new Intent(context, WebActivity.class)
                                .putExtra(WebActivity.URL, AppApi.BASE_URL + entity.getFiles().get(0).getFilePath()));
                    }


                }
            });


            ((ItemViewHolder) holder).txtViewMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    entity.setExpanded(!entity.isExpanded());
                    makeOtherCollapsed(holder.getAdapterPosition());

                }
            });

            ((ItemViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, SchoolActivityDetailsActivity.class);
                    intent.putExtra(SchoolActivityDetailsActivity.OBJECT, entity);
                    context.startActivity(intent);
                    ((HomeActivity)context).overridePendingTransition(R.anim.enter,R.anim.leave);

                }
            });

            ((ItemViewHolder) holder).imgMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    schoolEventsFragment.showPopUp(((ItemViewHolder) holder).imgMore,holder.getAdapterPosition(),entity);
                }
            });

        }

    }

    private void makeOtherCollapsed(int position) {

        for (int i = 0; i < beans.size(); i++) {

            if (i != position) {
                beans.get(i).setExpanded(false);
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }

    public void addItems(List<SchoolActivityResponse.ResultBean> models) {

        beans.addAll(models);
        notifyDataSetChanged();

    }

    class ItemViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.txt_long_msg)
        TextView txtLongMsg;
        @BindView(R.id.txt_date)
        TextView txtDate;
        @BindView(R.id.txt_time)
        TextView txtTime;
        @BindView(R.id.img)
        ImageView img;
        @BindView(R.id.txt_msg)
        TextView txtMsg;
        @BindView(R.id.txt_file_name)
        TextView txtFileName;
        @BindView(R.id.recyclerView)
        RecyclerView recyclerView;
        @BindView(R.id.txt_view_more)
        TextView txtViewMore;
        @BindView(R.id.status)
        View status;
        @BindView(R.id.img_more)
        ImageView imgMore;
        //R.layout.raw_class_activity

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}



