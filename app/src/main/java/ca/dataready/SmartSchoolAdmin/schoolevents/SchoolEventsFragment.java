/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.schoolevents;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.malinskiy.superrecyclerview.SuperRecyclerView;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.Listeners.OnReloadListener;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.DateFunction;
import ca.dataready.SmartSchoolAdmin.Utilities.LastItemDecoration;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.calendarview.CalendarCustomView;
import ca.dataready.SmartSchoolAdmin.calendarview.callbacks.CompactCalendarViewListener;
import ca.dataready.SmartSchoolAdmin.classevents.create.CreateClassActivity;
import ca.dataready.SmartSchoolAdmin.schoolevents.adapter.TodaysSchoolEventsAdapter;
import ca.dataready.SmartSchoolAdmin.schoolevents.adapter.UpcomingSchoolEventsAdapter;
import ca.dataready.SmartSchoolAdmin.schoolevents.create.CreateSchoolEventActivity;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.ClassActivityResponse;
import ca.dataready.SmartSchoolAdmin.server.CommonResponse;
import ca.dataready.SmartSchoolAdmin.server.SchoolActivityResponse;
import info.hoang8f.android.segmented.SegmentedGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


public class SchoolEventsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, OnReloadListener, CompactCalendarViewListener {

    Unbinder unbinder;
    @BindView(R.id.rb_today)
    RadioButton rbToday;
    @BindView(R.id.rb_upcoming)
    RadioButton rbUpcoming;
    @BindView(R.id.sgSchoolEvents)
    SegmentedGroup sgSchoolEvents;
    @BindView(R.id.recyclerViewToday)
    SuperRecyclerView recyclerViewToday;
    @BindView(R.id.recyclerViewUpcoming)
    SuperRecyclerView recyclerViewUpcoming;
    @BindView(R.id.viewAnimator)
    ViewAnimator viewAnimator;
    @BindView(R.id.fav_add)
    FloatingActionButton favAdd;
    @BindView(R.id.calenderView)
    CalendarCustomView calenderView;
    private Call<SchoolActivityResponse> appApi;
    private TodaysSchoolEventsAdapter todaysSchoolEventsAdapter;
    private UpcomingSchoolEventsAdapter UpcomingSchoolEventsAdapter;
    private String time, currentTime;
    private ArrayList<String> headers = new ArrayList<>();
    public static boolean isSchoolEventVisible = false;
    private String notificationId, repeatEventId, notificationDate, notificationEndDate;
    private boolean isDeleteSeries;
    private Call<CommonResponse> deleteEventApi;
    private String selectedDate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_school_events, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        ((HomeActivity) getActivity()).setSubTitle(getString(R.string.school_events));
        ((HomeActivity) getActivity()).setOnReloadListener(this);
        calenderView.setListener(this);
        Init();
        super.onActivityCreated(savedInstanceState);
    }


    private void Init() {

        recyclerViewToday.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewToday.addItemDecoration(new LastItemDecoration());
        recyclerViewToday.setRefreshListener(SchoolEventsFragment.this);
        //recyclerView.setRefreshingColorResources(R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary);

        recyclerViewUpcoming.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewUpcoming.addItemDecoration(new LastItemDecoration());
        recyclerViewUpcoming.setRefreshListener(SchoolEventsFragment.this);


        sgSchoolEvents.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {

                if (i == R.id.rb_today) {


                    InitTodayEvent();

                } else if (i == R.id.rb_upcoming) {

                    InitUpcomingEvent();

                }
            }

        });

        selectedDate = AdminApp.getInstance().currentDate();
        InitTodayEvent();

    }

    private void InitTodayEvent() {

        viewAnimator.setDisplayedChild(0);

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();
        appApi = AdminApp.getInstance().getApi().getSchoolActivities(entity.getSchoolId(), AdminApp.getInstance().getAdmin().getSchoolYear());
        appApi.enqueue(new Callback<SchoolActivityResponse>() {
            @Override
            public void onResponse(Call<SchoolActivityResponse> call, Response<SchoolActivityResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            todaysSchoolEventsAdapter = new TodaysSchoolEventsAdapter(getActivity(), SchoolEventsFragment.this);
                            recyclerViewToday.setAdapter(todaysSchoolEventsAdapter);
                            List<SchoolActivityResponse.ResultBean> results = response.body().getResult();
                            List<SchoolActivityResponse.ResultBean> finalResults = new ArrayList<>();

                            if (results != null && results.size() > 0) {

                                calenderView.setSchoolEventsList(results);

                                for (int i = 0; i < results.size(); i++) {

                                    SchoolActivityResponse.ResultBean bean = results.get(i);

                                    if (bean.getNotificationDate() != null) {

                                        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
                                        DateTime serviceDate = fmt.parseDateTime(bean.getNotificationDate());
                                        DateTime selected = fmt.parseDateTime(selectedDate);

                                        if (serviceDate.isEqual(selected)) {
                                            finalResults.add(bean);
                                        }
                                    }
                                }


                                if (finalResults.size() > 0) {
                                    todaysSchoolEventsAdapter.addItems(finalResults);
                                } else {
                                    TextView emptyView = (TextView) recyclerViewToday.getEmptyView().findViewById(R.id.empty);
                                    emptyView.setText(getString(R.string.no_data));
                                }
                                //addDataForOfflineUse(results);

                            } else {
                                todaysSchoolEventsAdapter = new TodaysSchoolEventsAdapter(getActivity(), SchoolEventsFragment.this);
                                recyclerViewToday.setAdapter(todaysSchoolEventsAdapter);
                                TextView emptyView = (TextView) recyclerViewToday.getEmptyView().findViewById(R.id.empty);
                                emptyView.setText(getString(R.string.no_data));
                            }

                        } else {
                            todaysSchoolEventsAdapter = new TodaysSchoolEventsAdapter(getActivity(), SchoolEventsFragment.this);
                            recyclerViewToday.setAdapter(todaysSchoolEventsAdapter);
                            TextView emptyView = (TextView) recyclerViewToday.getEmptyView().findViewById(R.id.empty);
                            emptyView.setText(getString(R.string.no_data));
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_SCHOOL_ACTIVITY_DATA);
                        todaysSchoolEventsAdapter = new TodaysSchoolEventsAdapter(getActivity(), SchoolEventsFragment.this);
                        recyclerViewToday.setAdapter(todaysSchoolEventsAdapter);
                        TextView emptyView = (TextView) recyclerViewToday.getEmptyView().findViewById(R.id.empty);
                        emptyView.setText(error.message());

                    }
                } catch (Exception e) {

                    todaysSchoolEventsAdapter = new TodaysSchoolEventsAdapter(getActivity(), SchoolEventsFragment.this);
                    recyclerViewToday.setAdapter(todaysSchoolEventsAdapter);
                    TextView emptyView = (TextView) recyclerViewToday.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(getString(R.string.somethingwrong));
                }
            }

            @Override
            public void onFailure(Call<SchoolActivityResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    todaysSchoolEventsAdapter = new TodaysSchoolEventsAdapter(getActivity(), SchoolEventsFragment.this);
                    recyclerViewToday.setAdapter(todaysSchoolEventsAdapter);
                    TextView emptyView = (TextView) recyclerViewToday.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(App_Constants.NO_INTERNET);
                    //  if (SchoolApp.getInstance().getError(t).equals(getString(R.string.no_internet)));
                    //getDataForOfflineUse();
                }
            }
        });
    }


    private void InitUpcomingEvent() {

        viewAnimator.setDisplayedChild(1);

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();
        appApi = AdminApp.getInstance().getApi().getSchoolActivities(entity.getSchoolId(), AdminApp.getInstance().getAdmin().getSchoolYear());
        appApi.enqueue(new Callback<SchoolActivityResponse>() {
            @Override
            public void onResponse(Call<SchoolActivityResponse> call, Response<SchoolActivityResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            UpcomingSchoolEventsAdapter = new UpcomingSchoolEventsAdapter(getActivity(), SchoolEventsFragment.this);
                            recyclerViewUpcoming.setAdapter(UpcomingSchoolEventsAdapter);
                            List<SchoolActivityResponse.ResultBean> results = response.body().getResult();
                            List<SchoolActivityResponse.ResultBean> finalResults = new ArrayList<>();
                            ArrayList<String> headers = new ArrayList<>();

                            if (results != null && results.size() > 0) {

                                for (int i = 0; i < results.size(); i++) {

                                    SchoolActivityResponse.ResultBean bean = results.get(i);

                                    if (bean.getNotificationDate() != null) {

                                        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
                                        DateTime serviceDate = fmt.parseDateTime(bean.getNotificationDate());
                                        DateTime CurrentDate = fmt.parseDateTime(AdminApp.getInstance().currentDate());

                                        if (serviceDate.isAfter(CurrentDate)) {

                                            String finalDate = DateFunction.ConvertDate(bean.getNotificationDate(), "yyyy-MM-dd", "dd MMMM yyyy");

                                            if (finalDate.contains(" ")) {
                                                String[] pDates = finalDate.split(" ");
                                                if (pDates.length > 2) {
                                                    String month = pDates[1];
                                                    String year = pDates[2];
                                                    if (!headers.contains(month + " " + year)) {
                                                        headers.add(month + " " + year);
                                                        SchoolActivityResponse.ResultBean entity = new SchoolActivityResponse.ResultBean();
                                                        entity.setNotificationDate(bean.getNotificationDate());
                                                        entity.setType(1);
                                                        entity.setMonth(month + " " + year);
                                                        finalResults.add(entity);
                                                    }

                                                    finalResults.add(bean);

                                                }
                                            }
                                        }
                                    }
                                }

                                if (finalResults.size() > 0) {
                                    UpcomingSchoolEventsAdapter.addItems(finalResults);
                                } else {
                                    TextView emptyView = (TextView) recyclerViewUpcoming.getEmptyView().findViewById(R.id.empty);
                                    emptyView.setText(getString(R.string.no_data));
                                }
                                //addDataForOfflineUse(results);

                            } else {

                                UpcomingSchoolEventsAdapter = new UpcomingSchoolEventsAdapter(getActivity(), SchoolEventsFragment.this);
                                recyclerViewUpcoming.setAdapter(UpcomingSchoolEventsAdapter);
                                TextView emptyView = (TextView) recyclerViewUpcoming.getEmptyView().findViewById(R.id.empty);
                                emptyView.setText(getString(R.string.no_data));
                            }

                        } else {

                            UpcomingSchoolEventsAdapter = new UpcomingSchoolEventsAdapter(getActivity(), SchoolEventsFragment.this);
                            recyclerViewUpcoming.setAdapter(UpcomingSchoolEventsAdapter);
                            TextView emptyView = (TextView) recyclerViewUpcoming.getEmptyView().findViewById(R.id.empty);
                            emptyView.setText(getString(R.string.no_data));
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_CLASS_ACTIVITY_DATA);
                        UpcomingSchoolEventsAdapter = new UpcomingSchoolEventsAdapter(getActivity(), SchoolEventsFragment.this);
                        recyclerViewUpcoming.setAdapter(UpcomingSchoolEventsAdapter);
                        TextView emptyView = (TextView) recyclerViewUpcoming.getEmptyView().findViewById(R.id.empty);
                        emptyView.setText(error.message());

                    }
                } catch (Exception e) {

                    UpcomingSchoolEventsAdapter = new UpcomingSchoolEventsAdapter(getActivity(), SchoolEventsFragment.this);
                    recyclerViewUpcoming.setAdapter(UpcomingSchoolEventsAdapter);
                    TextView emptyView = (TextView) recyclerViewUpcoming.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(getString(R.string.somethingwrong));
                }
            }

            @Override
            public void onFailure(Call<SchoolActivityResponse> call, Throwable t) {
                if (!call.isCanceled()) {

                    UpcomingSchoolEventsAdapter = new UpcomingSchoolEventsAdapter(getActivity(), SchoolEventsFragment.this);
                    recyclerViewUpcoming.setAdapter(UpcomingSchoolEventsAdapter);
                    TextView emptyView = (TextView) recyclerViewUpcoming.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(App_Constants.NO_INTERNET);
                    //  if (SchoolApp.getInstance().getError(t).equals(getString(R.string.no_internet)));
                    //getDataForOfflineUse();
                }
            }
        });
    }

    private boolean isUpcomingEvent(String time, String currenTime) {

        try {

            DateTimeFormatter fmt = DateTimeFormat.forPattern("hh:mm a");
            DateTime serverTime = fmt.parseDateTime(time);
            DateTime currentTime = fmt.parseDateTime(currenTime);

            if (serverTime.isAfter(currentTime)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {

            e.printStackTrace();
        }

        return false;
    }


    @Override
    public void onReload(String apiName) {

        if (apiName != null && isVisible()) {
            if (apiName.equalsIgnoreCase(App_Constants.API_SCHOOL_ACTIVITY_DATA)) {
                if (isVisible()) {
                    if (sgSchoolEvents.getCheckedRadioButtonId() == R.id.rb_today) {
                        recyclerViewToday.hideProgress();
                        InitTodayEvent();
                    } else if (sgSchoolEvents.getCheckedRadioButtonId() == R.id.rb_upcoming) {
                        recyclerViewUpcoming.hideProgress();
                        InitUpcomingEvent();
                    }
                }
            } else if (apiName.equalsIgnoreCase(App_Constants.API_DELETE_EVENTS)) {

                deleteEvents();
            }
        }

    }

    @Override
    public void onRefresh() {

        if (isVisible()) {

            if (sgSchoolEvents.getCheckedRadioButtonId() == R.id.rb_today) {
                recyclerViewToday.hideProgress();
                InitTodayEvent();
            } else if (sgSchoolEvents.getCheckedRadioButtonId() == R.id.rb_upcoming) {
                recyclerViewUpcoming.hideProgress();
                InitUpcomingEvent();
            }
        }

    }

    public void showPopUp(ImageView imgMore, int position, SchoolActivityResponse.ResultBean bean) {

        PopupMenu popup = new PopupMenu(imgMore.getContext(), imgMore);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.edit_class_event, popup.getMenu());
        MenuItem repeatAll = popup.getMenu().findItem(R.id.item_repeat_all);
        if (bean.getNotificationEndDate() == null)
            repeatAll.setVisible(false);

        popup.setOnMenuItemClickListener(new CardMenuItemClickListener(position, bean));
        popup.show();
    }

    @Override
    public void onDayClick(int date, int month, int year) {

     /*   recyclerViewToday.showProgress();
        StringBuilder temp_month = new StringBuilder();
        if (month < 10) {
            temp_month.append("0").append(month);
        } else {
            temp_month.append(month);
        }

        StringBuilder temp_day = new StringBuilder();
        if (date < 10) {
            temp_day.append("0").append(date);
        } else {
            temp_day.append(date);
        }

        selectedDate = year + "-" + temp_month.toString() + "-" + temp_day.toString();
        Log.d("SELECTED DATE : ", "" + selectedDate);

        InitTodayEvent();*/
    }

    @Override
    public void getSchoolEvents(List<SchoolActivityResponse.ResultBean> results,int date, int month, int year) {

        StringBuilder temp_month = new StringBuilder();
        if (month < 10) {
            temp_month.append("0").append(month);
        } else {
            temp_month.append(month);
        }

        StringBuilder temp_day = new StringBuilder();
        if (date < 10) {
            temp_day.append("0").append(date);
        } else {
            temp_day.append(date);
        }

        selectedDate = year + "-" + temp_month.toString() + "-" + temp_day.toString();
        Log.d("SELECTED DATE : ", "" + selectedDate);


        todaysSchoolEventsAdapter = new TodaysSchoolEventsAdapter(getActivity(), SchoolEventsFragment.this);
        recyclerViewToday.setAdapter(todaysSchoolEventsAdapter);

        List<SchoolActivityResponse.ResultBean> finalResults = new ArrayList<>();

        for (int i = 0; i < results.size(); i++) {

            SchoolActivityResponse.ResultBean bean = results.get(i);

            if (bean.getNotificationDate() != null) {

                DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
                DateTime serviceDate = fmt.parseDateTime(bean.getNotificationDate());
                DateTime selected = fmt.parseDateTime(selectedDate);

                if (serviceDate.isEqual(selected)) {
                    finalResults.add(bean);
                }
            }
        }

        if (finalResults.size() > 0) {
            todaysSchoolEventsAdapter.addItems(finalResults);
        } else {
            TextView emptyView = (TextView) recyclerViewToday.getEmptyView().findViewById(R.id.empty);
            emptyView.setText(getString(R.string.no_data));
        }
    }

    @Override
    public void getClassEvents(List<ClassActivityResponse.ResultBean> cEvents, int date, int month, int year) {

    }


    private class CardMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        private int position;
        private SchoolActivityResponse.ResultBean bean;

        CardMenuItemClickListener(int positon, SchoolActivityResponse.ResultBean bean) {
            this.position = positon;
            this.bean = bean;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {

                case R.id.item_edit:

                    startActivityForResult(new Intent(getActivity(), CreateSchoolEventActivity.class)
                            .putExtra(CreateClassActivity.ACTION, CreateClassActivity.EDIT)
                            .putExtra(CreateClassActivity.OBJECT, bean), App_Constants.UPDATE_LISTING);
                    if (getActivity() != null)
                        getActivity().overridePendingTransition(R.anim.enter, R.anim.leave);

                    break;

                case R.id.item_repeat:

                    notificationId = bean.getNotificationId();
                    repeatEventId = "";
                    notificationDate = bean.getNotificationDate();
                    notificationEndDate = bean.getNotificationDate();
                    isDeleteSeries = false;

                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                    alertDialog.setTitle(getString(R.string.delet_event));
                    alertDialog.setMessage(getString(R.string.delet_event_msg));
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    deleteEvents();
                                }
                            });
                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();

                    break;

                case R.id.item_repeat_all:

                    notificationId = bean.getNotificationId();
                    repeatEventId = bean.getRepeatEventId();
                    notificationDate = bean.getNotificationDate();
                    notificationEndDate = bean.getNotificationEndDate();
                    isDeleteSeries = true;

                    AlertDialog allEventsalertDialog = new AlertDialog.Builder(getActivity()).create();
                    allEventsalertDialog.setTitle(getString(R.string.delet_all_events));
                    allEventsalertDialog.setMessage(getString(R.string.delet_all_events_msg));
                    allEventsalertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    deleteEvents();
                                }
                            });
                    allEventsalertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    allEventsalertDialog.show();

                    break;


                default:
            }
            return false;
        }
    }

    private void deleteEvents() {

        Utility.showProgress(getActivity(), getString(R.string.processing));

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();
        deleteEventApi = AdminApp.getInstance().getApi().deletEvents(entity.getSchoolId(), entity.getSchoolYear(), notificationId, repeatEventId, notificationDate, notificationEndDate, isDeleteSeries);
        deleteEventApi.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                Utility.hideProgress();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();

                            if (sgSchoolEvents.getCheckedRadioButtonId() == R.id.rb_today)
                                InitTodayEvent();
                            else
                                InitUpcomingEvent();

                        } else {
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_DELETE_EVENTS);
                        Toast.makeText(getActivity(), "" + error.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(getActivity(), getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    Utility.hideProgress();
                    Toast.makeText(getActivity(), App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        isSchoolEventVisible = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        isSchoolEventVisible = true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStop() {
        super.onStop();

        if (appApi != null)
            appApi.cancel();

        if (deleteEventApi != null)
            deleteEventApi.cancel();
    }

    @OnClick(R.id.fav_add)
    public void onViewClicked() {

        startActivityForResult(new Intent(getActivity(), CreateSchoolEventActivity.class), App_Constants.UPDATE_LISTING);
        if (getActivity() != null)
            getActivity().overridePendingTransition(R.anim.enter, R.anim.leave);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == APIError.UPDATE_TOKEN && resultCode == RESULT_OK) {

            String apiName = data.getStringExtra("api");
            onReload(apiName);

        } else if (requestCode == App_Constants.UPDATE_LISTING && resultCode == RESULT_OK) {

            if (sgSchoolEvents.getCheckedRadioButtonId() == R.id.rb_today) {
                recyclerViewToday.hideProgress();
                InitTodayEvent();
            } else if (sgSchoolEvents.getCheckedRadioButtonId() == R.id.rb_upcoming) {
                recyclerViewUpcoming.hideProgress();
                InitUpcomingEvent();
            }

        }
    }
}
