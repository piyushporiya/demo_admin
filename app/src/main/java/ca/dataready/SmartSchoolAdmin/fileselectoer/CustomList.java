/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.fileselectoer;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;

import ca.dataready.SmartSchoolAdmin.R;


public class CustomList extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] web;
    String ParentFolder;
    int selectedPosition = -1;

    public CustomList(Activity context, String[] web, String path) {
        super(context, R.layout.list_single, web);
        this.context = context;
        this.web = web;
        ParentFolder = path;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_single, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
        final CheckBox chk = (CheckBox) rowView.findViewById(R.id.myCheckBox);

        chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {/**/

                if (chk.isChecked()) {
                    ((FileSelectionActivity) context).setFiles(true, web[position]);
                } else {
                    ((FileSelectionActivity) context).setFiles(false, web[position]);
                }

                //makeOtherUnChecked(position);

            }
        });

        txtTitle.setText(web[position]);
        Picasso.with(context).load(
                new File(
                        ParentFolder + "/" + web[position]
                )).placeholder(R.drawable.document).resize(50, 50).into(imageView);



        return rowView;
    }



    private void makeOtherUnChecked(int position, CheckBox chk) {

        for (int i = 0; i < web.length; i++) {
            if (i != position) {
                chk.setChecked(false);
            }
        }

        notifyDataSetChanged();

    }
}
