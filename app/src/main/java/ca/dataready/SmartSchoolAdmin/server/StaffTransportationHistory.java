/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;

import java.util.List;

public class StaffTransportationHistory {

    /**
     * message : Details are below
     * status : true
     * authtoken : null
     * result : [{"routeId":"9","driverId":"piyushporiya@techsoftlabs.com","boardingDate":"2017-09-07","scanTime":"2017-09-07 12:09:01","onboard":"IN","cardId":"1290000001","latitude":"72.8642981","stopId":null,"tripId":"AV5bCglAeCSjgpUCcNMv","serverTime":"2017-09-07 06:33:51","longitude":null},{"routeId":"9","driverId":"piyushporiya@techsoftlabs.com","boardingDate":"2017-09-07","scanTime":"2017-09-07 12:11:14","onboard":"OUT","cardId":"1290000001","latitude":"72.8642289","stopId":null,"tripId":"AV5bCglAeCSjgpUCcNMv","serverTime":"2017-09-07 06:36:05","longitude":null}]
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private String authtokenexpires;
    private List<StudentBordingDetails.ResultBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public List<StudentBordingDetails.ResultBean> getResult() {
        return result;
    }

    public void setResult(List<StudentBordingDetails.ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * routeId : 9
         * driverId : piyushporiya@techsoftlabs.com
         * boardingDate : 2017-09-07
         * scanTime : 2017-09-07 12:09:01
         * onboard : IN
         * cardId : 1290000001
         * latitude : 72.8642981
         * stopId : null
         * tripId : AV5bCglAeCSjgpUCcNMv
         * serverTime : 2017-09-07 06:33:51
         * longitude : null
         */

        private String routeId;
        private String driverId;
        private String boardingDate;
        private String scanTime;
        private String onboard;
        private String cardId;
        private String latitude;
        private String stopId;
        private String tripId;
        private String serverTime;
        private String longitude;

        public String getRouteId() {
            return routeId;
        }

        public void setRouteId(String routeId) {
            this.routeId = routeId;
        }

        public String getDriverId() {
            return driverId;
        }

        public void setDriverId(String driverId) {
            this.driverId = driverId;
        }

        public String getBoardingDate() {
            return boardingDate;
        }

        public void setBoardingDate(String boardingDate) {
            this.boardingDate = boardingDate;
        }

        public String getScanTime() {
            return scanTime;
        }

        public void setScanTime(String scanTime) {
            this.scanTime = scanTime;
        }

        public String getOnboard() {
            return onboard;
        }

        public void setOnboard(String onboard) {
            this.onboard = onboard;
        }

        public String getCardId() {
            return cardId;
        }

        public void setCardId(String cardId) {
            this.cardId = cardId;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getStopId() {
            return stopId;
        }

        public void setStopId(String stopId) {
            this.stopId = stopId;
        }

        public String getTripId() {
            return tripId;
        }

        public void setTripId(String tripId) {
            this.tripId = tripId;
        }

        public String getServerTime() {
            return serverTime;
        }

        public void setServerTime(String serverTime) {
            this.serverTime = serverTime;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }
    }

}
