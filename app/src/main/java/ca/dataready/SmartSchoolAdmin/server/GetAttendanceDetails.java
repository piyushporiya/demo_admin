/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;


import java.util.List;

public class GetAttendanceDetails {


    /**
     * message : Informations are below
     * status : true
     * authtoken : null
     * result : [{"teacherMobileNo":"9898989898","gradeId":"3","teacherName":"Teacher three","subjectEndTime":"12:00","subjectAttendance":true,"subjectStartTime":"11:00","studentId":"1217","classId":"A","teacherId":"teacherthree@dataready.in","isLate":false,"teacherEmailId":"teacherthree@dataready.in","schoolId":"129","lateTime":"2018-03-24 13:36:57","schoolYear":"2017-2018","comment":"student is late","attendanceDate":"2018-03-24","subjectName":"History"}]
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private String authtokenexpires;
    private List<ResultBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * teacherMobileNo : 9898989898
         * gradeId : 3
         * teacherName : Teacher three
         * subjectEndTime : 12:00
         * subjectAttendance : true
         * subjectStartTime : 11:00
         * studentId : 1217
         * classId : A
         * teacherId : teacherthree@dataready.in
         * isLate : false
         * teacherEmailId : teacherthree@dataready.in
         * schoolId : 129
         * lateTime : 2018-03-24 13:36:57
         * schoolYear : 2017-2018
         * comment : student is late
         * attendanceDate : 2018-03-24
         * subjectName : History
         */

        private String teacherMobileNo;
        private String gradeId;
        private String teacherName;
        private String subjectEndTime;
        private boolean subjectAttendance;
        private String subjectStartTime;
        private String studentId;
        private String classId;
        private String teacherId;
        private boolean isLate;
        private String teacherEmailId;
        private String schoolId;
        private String lateTime;
        private String schoolYear;
        private String comment;
        private String attendanceDate;
        private String subjectName;

        public String getTeacherMobileNo() {
            return teacherMobileNo;
        }

        public void setTeacherMobileNo(String teacherMobileNo) {
            this.teacherMobileNo = teacherMobileNo;
        }

        public String getGradeId() {
            return gradeId;
        }

        public void setGradeId(String gradeId) {
            this.gradeId = gradeId;
        }

        public String getTeacherName() {
            return teacherName;
        }

        public void setTeacherName(String teacherName) {
            this.teacherName = teacherName;
        }

        public String getSubjectEndTime() {
            return subjectEndTime;
        }

        public void setSubjectEndTime(String subjectEndTime) {
            this.subjectEndTime = subjectEndTime;
        }

        public boolean isSubjectAttendance() {
            return subjectAttendance;
        }

        public void setSubjectAttendance(boolean subjectAttendance) {
            this.subjectAttendance = subjectAttendance;
        }

        public String getSubjectStartTime() {
            return subjectStartTime;
        }

        public void setSubjectStartTime(String subjectStartTime) {
            this.subjectStartTime = subjectStartTime;
        }

        public String getStudentId() {
            return studentId;
        }

        public void setStudentId(String studentId) {
            this.studentId = studentId;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getTeacherId() {
            return teacherId;
        }

        public void setTeacherId(String teacherId) {
            this.teacherId = teacherId;
        }

        public boolean isIsLate() {
            return isLate;
        }

        public void setIsLate(boolean isLate) {
            this.isLate = isLate;
        }

        public String getTeacherEmailId() {
            return teacherEmailId;
        }

        public void setTeacherEmailId(String teacherEmailId) {
            this.teacherEmailId = teacherEmailId;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getLateTime() {
            return lateTime;
        }

        public void setLateTime(String lateTime) {
            this.lateTime = lateTime;
        }

        public String getSchoolYear() {
            return schoolYear;
        }

        public void setSchoolYear(String schoolYear) {
            this.schoolYear = schoolYear;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getAttendanceDate() {
            return attendanceDate;
        }

        public void setAttendanceDate(String attendanceDate) {
            this.attendanceDate = attendanceDate;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }
    }
}
