/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;

public class UpdateSchoolLevelPermissionsParams {


    /**
     * schoolId : string
     * schoolpermissions : {"adminmodules":{"appointment":true,"channel":true,"communication":true,"driver":true,"events":true,"fee":true,"route":true,"staff":true,"student":true,"version":"string"},"apps":{"admin":true,"driver":true,"parent":true,"teacher":true},"drivermodules":{"fileupload":true,"pickupdropoff":true,"qrscan":true,"refreshFrequency":"string","version":"string","video":true},"parentmodules":{"appointment":true,"attendance":true,"channel":true,"communication":true,"events":true,"fee":true,"homework":true,"p2p":true,"refreshFrequency":"string","route":true,"version":"string"},"teachermodules":{"appointment":true,"attendance":true,"communication":true,"homework":true,"t2t":true,"version":"string"}}
     */

    private String schoolId;
    private SchoolpermissionsBean schoolpermissions;

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public SchoolpermissionsBean getSchoolpermissions() {
        return schoolpermissions;
    }

    public void setSchoolpermissions(SchoolpermissionsBean schoolpermissions) {
        this.schoolpermissions = schoolpermissions;
    }

    public static class SchoolpermissionsBean {
        /**
         * adminmodules : {"appointment":true,"channel":true,"communication":true,"driver":true,"events":true,"fee":true,"route":true,"staff":true,"student":true,"version":"string"}
         * apps : {"admin":true,"driver":true,"parent":true,"teacher":true}
         * drivermodules : {"fileupload":true,"pickupdropoff":true,"qrscan":true,"refreshFrequency":"string","version":"string","video":true}
         * parentmodules : {"appointment":true,"attendance":true,"channel":true,"communication":true,"events":true,"fee":true,"homework":true,"p2p":true,"refreshFrequency":"string","route":true,"version":"string"}
         * teachermodules : {"appointment":true,"attendance":true,"communication":true,"homework":true,"t2t":true,"version":"string"}
         */

        private AdminmodulesBean adminmodules;
        private AppsBean apps;
        private DrivermodulesBean drivermodules;
        private ParentmodulesBean parentmodules;
        private TeachermodulesBean teachermodules;

        public AdminmodulesBean getAdminmodules() {
            return adminmodules;
        }

        public void setAdminmodules(AdminmodulesBean adminmodules) {
            this.adminmodules = adminmodules;
        }

        public AppsBean getApps() {
            return apps;
        }

        public void setApps(AppsBean apps) {
            this.apps = apps;
        }

        public DrivermodulesBean getDrivermodules() {
            return drivermodules;
        }

        public void setDrivermodules(DrivermodulesBean drivermodules) {
            this.drivermodules = drivermodules;
        }

        public ParentmodulesBean getParentmodules() {
            return parentmodules;
        }

        public void setParentmodules(ParentmodulesBean parentmodules) {
            this.parentmodules = parentmodules;
        }

        public TeachermodulesBean getTeachermodules() {
            return teachermodules;
        }

        public void setTeachermodules(TeachermodulesBean teachermodules) {
            this.teachermodules = teachermodules;
        }

        public static class AdminmodulesBean {
            /**
             * appointment : true
             * channel : true
             * communication : true
             * driver : true
             * events : true
             * fee : true
             * route : true
             * staff : true
             * student : true
             * version : string
             */

            private boolean appointment;
            private boolean channel;
            private boolean communication;
            private boolean driver;
            private boolean events;
            private boolean fee;
            private boolean route;
            private boolean staff;
            private boolean student;
            private String version;

            public boolean isAppointment() {
                return appointment;
            }

            public void setAppointment(boolean appointment) {
                this.appointment = appointment;
            }

            public boolean isChannel() {
                return channel;
            }

            public void setChannel(boolean channel) {
                this.channel = channel;
            }

            public boolean isCommunication() {
                return communication;
            }

            public void setCommunication(boolean communication) {
                this.communication = communication;
            }

            public boolean isDriver() {
                return driver;
            }

            public void setDriver(boolean driver) {
                this.driver = driver;
            }

            public boolean isEvents() {
                return events;
            }

            public void setEvents(boolean events) {
                this.events = events;
            }

            public boolean isFee() {
                return fee;
            }

            public void setFee(boolean fee) {
                this.fee = fee;
            }

            public boolean isRoute() {
                return route;
            }

            public void setRoute(boolean route) {
                this.route = route;
            }

            public boolean isStaff() {
                return staff;
            }

            public void setStaff(boolean staff) {
                this.staff = staff;
            }

            public boolean isStudent() {
                return student;
            }

            public void setStudent(boolean student) {
                this.student = student;
            }

            public String getVersion() {
                return version;
            }

            public void setVersion(String version) {
                this.version = version;
            }
        }

        public static class AppsBean {
            /**
             * admin : true
             * driver : true
             * parent : true
             * teacher : true
             */

            private boolean admin;
            private boolean driver;
            private boolean parent;
            private boolean teacher;

            public boolean isAdmin() {
                return admin;
            }

            public void setAdmin(boolean admin) {
                this.admin = admin;
            }

            public boolean isDriver() {
                return driver;
            }

            public void setDriver(boolean driver) {
                this.driver = driver;
            }

            public boolean isParent() {
                return parent;
            }

            public void setParent(boolean parent) {
                this.parent = parent;
            }

            public boolean isTeacher() {
                return teacher;
            }

            public void setTeacher(boolean teacher) {
                this.teacher = teacher;
            }
        }

        public static class DrivermodulesBean {
            /**
             * fileupload : true
             * pickupdropoff : true
             * qrscan : true
             * refreshFrequency : string
             * version : string
             * video : true
             */

            private boolean fileupload;
            private boolean pickupdropoff;
            private boolean qrscan;
            private String refreshFrequency;
            private String version;
            private boolean video;

            public boolean isFileupload() {
                return fileupload;
            }

            public void setFileupload(boolean fileupload) {
                this.fileupload = fileupload;
            }

            public boolean isPickupdropoff() {
                return pickupdropoff;
            }

            public void setPickupdropoff(boolean pickupdropoff) {
                this.pickupdropoff = pickupdropoff;
            }

            public boolean isQrscan() {
                return qrscan;
            }

            public void setQrscan(boolean qrscan) {
                this.qrscan = qrscan;
            }

            public String getRefreshFrequency() {
                return refreshFrequency;
            }

            public void setRefreshFrequency(String refreshFrequency) {
                this.refreshFrequency = refreshFrequency;
            }

            public String getVersion() {
                return version;
            }

            public void setVersion(String version) {
                this.version = version;
            }

            public boolean isVideo() {
                return video;
            }

            public void setVideo(boolean video) {
                this.video = video;
            }
        }

        public static class ParentmodulesBean {
            /**
             * appointment : true
             * attendance : true
             * channel : true
             * communication : true
             * events : true
             * fee : true
             * homework : true
             * p2p : true
             * refreshFrequency : string
             * route : true
             * version : string
             */

            private boolean appointment;
            private boolean attendance;
            private boolean channel;
            private boolean communication;
            private boolean events;
            private boolean fee;
            private boolean homework;
            private boolean p2p;
            private String refreshFrequency;
            private boolean route;
            private String version;

            public boolean isAppointment() {
                return appointment;
            }

            public void setAppointment(boolean appointment) {
                this.appointment = appointment;
            }

            public boolean isAttendance() {
                return attendance;
            }

            public void setAttendance(boolean attendance) {
                this.attendance = attendance;
            }

            public boolean isChannel() {
                return channel;
            }

            public void setChannel(boolean channel) {
                this.channel = channel;
            }

            public boolean isCommunication() {
                return communication;
            }

            public void setCommunication(boolean communication) {
                this.communication = communication;
            }

            public boolean isEvents() {
                return events;
            }

            public void setEvents(boolean events) {
                this.events = events;
            }

            public boolean isFee() {
                return fee;
            }

            public void setFee(boolean fee) {
                this.fee = fee;
            }

            public boolean isHomework() {
                return homework;
            }

            public void setHomework(boolean homework) {
                this.homework = homework;
            }

            public boolean isP2p() {
                return p2p;
            }

            public void setP2p(boolean p2p) {
                this.p2p = p2p;
            }

            public String getRefreshFrequency() {
                return refreshFrequency;
            }

            public void setRefreshFrequency(String refreshFrequency) {
                this.refreshFrequency = refreshFrequency;
            }

            public boolean isRoute() {
                return route;
            }

            public void setRoute(boolean route) {
                this.route = route;
            }

            public String getVersion() {
                return version;
            }

            public void setVersion(String version) {
                this.version = version;
            }
        }

        public static class TeachermodulesBean {
            /**
             * appointment : true
             * attendance : true
             * communication : true
             * homework : true
             * t2t : true
             * version : string
             */

            private boolean appointment;
            private boolean attendance;
            private boolean communication;
            private boolean homework;
            private boolean t2t;
            private String version;

            public boolean isAppointment() {
                return appointment;
            }

            public void setAppointment(boolean appointment) {
                this.appointment = appointment;
            }

            public boolean isAttendance() {
                return attendance;
            }

            public void setAttendance(boolean attendance) {
                this.attendance = attendance;
            }

            public boolean isCommunication() {
                return communication;
            }

            public void setCommunication(boolean communication) {
                this.communication = communication;
            }

            public boolean isHomework() {
                return homework;
            }

            public void setHomework(boolean homework) {
                this.homework = homework;
            }

            public boolean isT2t() {
                return t2t;
            }

            public void setT2t(boolean t2t) {
                this.t2t = t2t;
            }

            public String getVersion() {
                return version;
            }

            public void setVersion(String version) {
                this.version = version;
            }
        }
    }
}
