/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;


public class AddDriverModel {


    private int type = 0;
    private boolean isExpanded = false;


    public AddDriverModel(int type, boolean isExpanded) {
        this.type = type;
        this.isExpanded = isExpanded;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }


}
