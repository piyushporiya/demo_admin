/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class GetTeacherInfo {


    /**
     * message : User is already exist, Please sign-in
     * status : true
     * authtoken : null
     * result : {"lastName":"Banner","role":"Sr. Chemistry Professor","gender":"single","emailId":"bruce@gmail.com","joiningDate":"","phoneNo":"7894561233","userpermissions":{"teachermodules":{"homework":true,"t2t":true,"appointment":true,"login":true,"communication":true,"attendance":true},"adminmodules":{"route":true,"driver":true,"student":true,"fee":true,"channel":true,"appointment":true,"staff":true,"login":true,"communication":true,"events":true},"drivermodules":{"login":true}},"password":"zb3m6bfc","isDeleted":false,"countryCode":null,"schoolId":"129","staffPin":104767,"deviceTokenId":"","id":"bruce@gmail.com","department":"","qualificationDetails":{"qualifications":[{"university":"NDA","passedYear":"March 2013 - March 2022","qualificationName":"AITS"}]},"address":"adajan surat","profilePic":null,"geopoint":null,"firstName":"Bruce","createdDate":"2018-03-19","dob":"2018-03-19","modifiedDate":"2018-03-19","experienceDetails":{"experiences":[{"toYear":"March 2019","address":"Adajan Surat","organizationName":"VMA","fromYear":"March 2011"}]},"emergencyDetails":{"firstName":"Betty","lastName":"Banner","education":null,"occupation":null,"address":"adajan surat","dob":null,"emailId":"betty@gmail.com","phoneNo":"7894561232","relation":"Wife"},"position":null,"userType":"teacher","maritalStatus":null}
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private ResultBean result;
    private String authtokenexpires;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public static class ResultBean {
        /**
         * lastName : Banner
         * role : Sr. Chemistry Professor
         * gender : single
         * emailId : bruce@gmail.com
         * joiningDate :
         * phoneNo : 7894561233
         * userpermissions : {"teachermodules":{"homework":true,"t2t":true,"appointment":true,"login":true,"communication":true,"attendance":true},"adminmodules":{"route":true,"driver":true,"student":true,"fee":true,"channel":true,"appointment":true,"staff":true,"login":true,"communication":true,"events":true},"drivermodules":{"login":true}}
         * password : zb3m6bfc
         * isDeleted : false
         * countryCode : null
         * schoolId : 129
         * staffPin : 104767
         * deviceTokenId :
         * id : bruce@gmail.com
         * department :
         * qualificationDetails : {"qualifications":[{"university":"NDA","passedYear":"March 2013 - March 2022","qualificationName":"AITS"}]}
         * address : adajan surat
         * profilePic : null
         * geopoint : null
         * firstName : Bruce
         * createdDate : 2018-03-19
         * dob : 2018-03-19
         * modifiedDate : 2018-03-19
         * experienceDetails : {"experiences":[{"toYear":"March 2019","address":"Adajan Surat","organizationName":"VMA","fromYear":"March 2011"}]}
         * emergencyDetails : {"firstName":"Betty","lastName":"Banner","education":null,"occupation":null,"address":"adajan surat","dob":null,"emailId":"betty@gmail.com","phoneNo":"7894561232","relation":"Wife"}
         * position : null
         * userType : teacher
         * maritalStatus : null
         */

        private String lastName;
        private String role;
        private String gender;
        private String emailId;
        private String joiningDate;
        private String phoneNo;
        private UserpermissionsBean userpermissions;
        private String password;
        private boolean isDeleted;
        private String countryCode;
        private String schoolId;
        private int staffPin;
        private String deviceTokenId;
        private String id;
        private String department;
        private QualificationDetailsBean qualificationDetails;
        private String address;
        private String profilePic;
        private String geopoint;
        private String firstName;
        private String createdDate;
        private String dob;
        private String modifiedDate;
        private ExperienceDetailsBean experienceDetails;
        private EmergencyDetailsBean emergencyDetails;
        private String position;
        private String userType;
        private String maritalStatus;
        private NotesBean notes;
        private PhysicianDetailsBean physicianDetails;
        private String requiresTransportation;
        private List<FilesBean> files;

        public List<FilesBean> getFiles() {
            return files;
        }

        public void setFiles(List<FilesBean> files) {
            this.files = files;
        }

        public NotesBean getNotes() {
            return notes;
        }

        public void setNotes(NotesBean notes) {
            this.notes = notes;
        }

        public PhysicianDetailsBean getPhysicianDetails() {
            return physicianDetails;
        }

        public void setPhysicianDetails(PhysicianDetailsBean physicianDetails) {
            this.physicianDetails = physicianDetails;
        }

        public String getRequiresTransportation() {
            return requiresTransportation;
        }

        public void setRequiresTransportation(String requiresTransportation) {
            this.requiresTransportation = requiresTransportation;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getEmailId() {
            return emailId;
        }

        public void setEmailId(String emailId) {
            this.emailId = emailId;
        }

        public String getJoiningDate() {
            return joiningDate;
        }

        public void setJoiningDate(String joiningDate) {
            this.joiningDate = joiningDate;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public UserpermissionsBean getUserpermissions() {
            return userpermissions;
        }

        public void setUserpermissions(UserpermissionsBean userpermissions) {
            this.userpermissions = userpermissions;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public boolean isIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(boolean isDeleted) {
            this.isDeleted = isDeleted;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public int getStaffPin() {
            return staffPin;
        }

        public void setStaffPin(int staffPin) {
            this.staffPin = staffPin;
        }

        public String getDeviceTokenId() {
            return deviceTokenId;
        }

        public void setDeviceTokenId(String deviceTokenId) {
            this.deviceTokenId = deviceTokenId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDepartment() {
            return department;
        }

        public void setDepartment(String department) {
            this.department = department;
        }

        public QualificationDetailsBean getQualificationDetails() {
            return qualificationDetails;
        }

        public void setQualificationDetails(QualificationDetailsBean qualificationDetails) {
            this.qualificationDetails = qualificationDetails;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        public String getGeopoint() {
            return geopoint;
        }

        public void setGeopoint(String geopoint) {
            this.geopoint = geopoint;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getModifiedDate() {
            return modifiedDate;
        }

        public void setModifiedDate(String modifiedDate) {
            this.modifiedDate = modifiedDate;
        }

        public ExperienceDetailsBean getExperienceDetails() {
            return experienceDetails;
        }

        public void setExperienceDetails(ExperienceDetailsBean experienceDetails) {
            this.experienceDetails = experienceDetails;
        }

        public EmergencyDetailsBean getEmergencyDetails() {
            return emergencyDetails;
        }

        public void setEmergencyDetails(EmergencyDetailsBean emergencyDetails) {
            this.emergencyDetails = emergencyDetails;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

        public String getMaritalStatus() {
            return maritalStatus;
        }

        public void setMaritalStatus(String maritalStatus) {
            this.maritalStatus = maritalStatus;
        }

        public class FilesBean  {
            /**
             * fileName : Apple vs Android.pdf
             * filePath : /schoolapp/images/45e75072-cc59-428c-9feb-b584d10f3e6b.pdf
             */

            private String fileName;
            private String filePath;

            public String getFileName() {
                return fileName;
            }

            public void setFileName(String fileName) {
                this.fileName = fileName;
            }

            public String getFilePath() {
                return filePath;
            }

            public void setFilePath(String filePath) {
                this.filePath = filePath;
            }


        }


        public static class UserpermissionsBean {
            /**
             * teachermodules : {"homework":true,"t2t":true,"appointment":true,"login":true,"communication":true,"attendance":true}
             * adminmodules : {"route":true,"driver":true,"student":true,"fee":true,"channel":true,"appointment":true,"staff":true,"login":true,"communication":true,"events":true}
             * drivermodules : {"login":true}
             */

            private TeachermodulesBean teachermodules;
            private AdminmodulesBean adminmodules;
            private DrivermodulesBean drivermodules;

            public TeachermodulesBean getTeachermodules() {
                return teachermodules;
            }

            public void setTeachermodules(TeachermodulesBean teachermodules) {
                this.teachermodules = teachermodules;
            }

            public AdminmodulesBean getAdminmodules() {
                return adminmodules;
            }

            public void setAdminmodules(AdminmodulesBean adminmodules) {
                this.adminmodules = adminmodules;
            }

            public DrivermodulesBean getDrivermodules() {
                return drivermodules;
            }

            public void setDrivermodules(DrivermodulesBean drivermodules) {
                this.drivermodules = drivermodules;
            }

            public static class TeachermodulesBean {
                /**
                 * homework : true
                 * t2t : true
                 * appointment : true
                 * login : true
                 * communication : true
                 * attendance : true
                 */

                private boolean homework;
                private boolean t2t;
                private boolean appointment;
                private boolean login;
                private boolean communication;
                private boolean attendance;

                public boolean isHomework() {
                    return homework;
                }

                public void setHomework(boolean homework) {
                    this.homework = homework;
                }

                public boolean isT2t() {
                    return t2t;
                }

                public void setT2t(boolean t2t) {
                    this.t2t = t2t;
                }

                public boolean isAppointment() {
                    return appointment;
                }

                public void setAppointment(boolean appointment) {
                    this.appointment = appointment;
                }

                public boolean isLogin() {
                    return login;
                }

                public void setLogin(boolean login) {
                    this.login = login;
                }

                public boolean isCommunication() {
                    return communication;
                }

                public void setCommunication(boolean communication) {
                    this.communication = communication;
                }

                public boolean isAttendance() {
                    return attendance;
                }

                public void setAttendance(boolean attendance) {
                    this.attendance = attendance;
                }
            }

            public static class AdminmodulesBean {
                /**
                 * route : true
                 * driver : true
                 * student : true
                 * fee : true
                 * channel : true
                 * appointment : true
                 * staff : true
                 * login : true
                 * communication : true
                 * events : true
                 */

                private boolean route;
                private boolean driver;
                private boolean student;
                private boolean fee;
                private boolean channel;
                private boolean appointment;
                private boolean staff;
                private boolean login;
                private boolean communication;
                private boolean events;

                public boolean isRoute() {
                    return route;
                }

                public void setRoute(boolean route) {
                    this.route = route;
                }

                public boolean isDriver() {
                    return driver;
                }

                public void setDriver(boolean driver) {
                    this.driver = driver;
                }

                public boolean isStudent() {
                    return student;
                }

                public void setStudent(boolean student) {
                    this.student = student;
                }

                public boolean isFee() {
                    return fee;
                }

                public void setFee(boolean fee) {
                    this.fee = fee;
                }

                public boolean isChannel() {
                    return channel;
                }

                public void setChannel(boolean channel) {
                    this.channel = channel;
                }

                public boolean isAppointment() {
                    return appointment;
                }

                public void setAppointment(boolean appointment) {
                    this.appointment = appointment;
                }

                public boolean isStaff() {
                    return staff;
                }

                public void setStaff(boolean staff) {
                    this.staff = staff;
                }

                public boolean isLogin() {
                    return login;
                }

                public void setLogin(boolean login) {
                    this.login = login;
                }

                public boolean isCommunication() {
                    return communication;
                }

                public void setCommunication(boolean communication) {
                    this.communication = communication;
                }

                public boolean isEvents() {
                    return events;
                }

                public void setEvents(boolean events) {
                    this.events = events;
                }
            }

            public static class DrivermodulesBean {
                /**
                 * login : true
                 */

                private boolean login;

                public boolean isLogin() {
                    return login;
                }

                public void setLogin(boolean login) {
                    this.login = login;
                }
            }
        }

        public static class QualificationDetailsBean {
            private List<QualificationsBean> qualifications;
            private List<FilesBean> files;

            public List<FilesBean> getFiles() {
                return files;
            }

            public void setFiles(List<FilesBean> files) {
                this.files = files;
            }

            public List<QualificationsBean> getQualifications() {
                return qualifications;
            }

            public void setQualifications(List<QualificationsBean> qualifications) {
                this.qualifications = qualifications;
            }

            public static class QualificationsBean {
                /**
                 * university : NDA
                 * passedYear : March 2013 - March 2022
                 * qualificationName : AITS
                 */

                private String university;
                private String passedYear;
                private String qualificationName;

                public String getUniversity() {
                    return university;
                }

                public void setUniversity(String university) {
                    this.university = university;
                }

                public String getPassedYear() {
                    return passedYear;
                }

                public void setPassedYear(String passedYear) {
                    this.passedYear = passedYear;
                }

                public String getQualificationName() {
                    return qualificationName;
                }

                public void setQualificationName(String qualificationName) {
                    this.qualificationName = qualificationName;
                }
            }

            public class FilesBean  {
                /**
                 * fileName : Apple vs Android.pdf
                 * filePath : /schoolapp/images/45e75072-cc59-428c-9feb-b584d10f3e6b.pdf
                 */

                private String fileName;
                private String filePath;

                public String getFileName() {
                    return fileName;
                }

                public void setFileName(String fileName) {
                    this.fileName = fileName;
                }

                public String getFilePath() {
                    return filePath;
                }

                public void setFilePath(String filePath) {
                    this.filePath = filePath;
                }


            }
        }

        public static class ExperienceDetailsBean {
            private List<ExperiencesBean> experiences;
            private List<FilesBean> files;

            public List<FilesBean> getFiles() {
                return files;
            }

            public void setFiles(List<FilesBean> files) {
                this.files = files;
            }

            public List<ExperiencesBean> getExperiences() {
                return experiences;
            }

            public void setExperiences(List<ExperiencesBean> experiences) {
                this.experiences = experiences;
            }

            public static class ExperiencesBean {
                /**
                 * toYear : March 2019
                 * address : Adajan Surat
                 * organizationName : VMA
                 * fromYear : March 2011
                 */

                private String toYear;
                private String address;
                private String organizationName;
                private String fromYear;

                public String getToYear() {
                    return toYear;
                }

                public void setToYear(String toYear) {
                    this.toYear = toYear;
                }

                public String getAddress() {
                    return address;
                }

                public void setAddress(String address) {
                    this.address = address;
                }

                public String getOrganizationName() {
                    return organizationName;
                }

                public void setOrganizationName(String organizationName) {
                    this.organizationName = organizationName;
                }

                public String getFromYear() {
                    return fromYear;
                }

                public void setFromYear(String fromYear) {
                    this.fromYear = fromYear;
                }
            }

            public class FilesBean  {
                /**
                 * fileName : Apple vs Android.pdf
                 * filePath : /schoolapp/images/45e75072-cc59-428c-9feb-b584d10f3e6b.pdf
                 */

                private String fileName;
                private String filePath;

                public String getFileName() {
                    return fileName;
                }

                public void setFileName(String fileName) {
                    this.fileName = fileName;
                }

                public String getFilePath() {
                    return filePath;
                }

                public void setFilePath(String filePath) {
                    this.filePath = filePath;
                }


            }
        }

        public static class EmergencyDetailsBean {
            /**
             * firstName : Betty
             * lastName : Banner
             * education : null
             * occupation : null
             * address : adajan surat
             * dob : null
             * emailId : betty@gmail.com
             * phoneNo : 7894561232
             * relation : Wife
             */

            private String firstName;
            private String lastName;
            private String education;
            private String occupation;
            private String address;
            private String dob;
            private String emailId;
            private String phoneNo;
            private String relation;
            private List<FilesBean> files;

            public List<FilesBean> getFiles() {
                return files;
            }

            public void setFiles(List<FilesBean> files) {
                this.files = files;
            }

            public String getFirstName() {
                return firstName;
            }

            public void setFirstName(String firstName) {
                this.firstName = firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public void setLastName(String lastName) {
                this.lastName = lastName;
            }

            public String getEducation() {
                return education;
            }

            public void setEducation(String education) {
                this.education = education;
            }

            public String getOccupation() {
                return occupation;
            }

            public void setOccupation(String occupation) {
                this.occupation = occupation;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getDob() {
                return dob;
            }

            public void setDob(String dob) {
                this.dob = dob;
            }

            public String getEmailId() {
                return emailId;
            }

            public void setEmailId(String emailId) {
                this.emailId = emailId;
            }

            public String getPhoneNo() {
                return phoneNo;
            }

            public void setPhoneNo(String phoneNo) {
                this.phoneNo = phoneNo;
            }

            public String getRelation() {
                return relation;
            }

            public void setRelation(String relation) {
                this.relation = relation;
            }

            public class FilesBean  {
                /**
                 * fileName : Apple vs Android.pdf
                 * filePath : /schoolapp/images/45e75072-cc59-428c-9feb-b584d10f3e6b.pdf
                 */

                private String fileName;
                private String filePath;

                public String getFileName() {
                    return fileName;
                }

                public void setFileName(String fileName) {
                    this.fileName = fileName;
                }

                public String getFilePath() {
                    return filePath;
                }

                public void setFilePath(String filePath) {
                    this.filePath = filePath;
                }


            }
        }


        public static class NotesBean {
            /**
             * schoolCounselor : string
             * transportationCounselor : string
             */

            private String schoolCounselor;
            private String transportationCounselor;

            public String getSchoolCounselor() {
                return schoolCounselor;
            }

            public void setSchoolCounselor(String schoolCounselor) {
                this.schoolCounselor = schoolCounselor;
            }

            public String getTransportationCounselor() {
                return transportationCounselor;
            }

            public void setTransportationCounselor(String transportationCounselor) {
                this.transportationCounselor = transportationCounselor;
            }
        }

        public static class PhysicianDetailsBean {
            /**
             * address : string
             * city : string
             * country : string
             * email : string
             * fax : string
             * firstName : string
             * lastName : string
             * phone : string
             * state : string
             */

            private String address;
            private String city;
            private String country;
            private String email;
            private String fax;
            private String firstName;
            private String lastName;
            private String phone;
            private String state;

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getFax() {
                return fax;
            }

            public void setFax(String fax) {
                this.fax = fax;
            }

            public String getFirstName() {
                return firstName;
            }

            public void setFirstName(String firstName) {
                this.firstName = firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public void setLastName(String lastName) {
                this.lastName = lastName;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }
        }

    }


}
