/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class GetAllRoutesResponse {


    /**
     * message : Route informations are below
     * status : true
     * authtoken : null
     * result : [{"stopage":[{"isSchool":true,"address":"udhana","latitude":"21.1431","stopId":"1","sequenceId":"1","longitude":"72.8431"},{"isSchool":false,"address":"GIDC, 137, Dr KV Hagrenad Road, Beside C.N.G. Pump, Alfa Industry, Unit Estate, Pandesara, Udhna, Surat, Gujarat 394221, India","latitude":"21.1448","stopId":"2","sequenceId":"2","longitude":"72.8372"},{"isSchool":false,"address":"Ashoka Shopping Centre, Plot No. 197, Udhana - Sachin Hwy, Near S B I, Pandesara GIDC, Udhna, Surat, Gujarat 394221, India","latitude":"21.1424","stopId":"3","sequenceId":"3","longitude":"72.8489"}],"driverDetails":{"firstName":"Piyush","lastName":"Poriya","driverId":"piyushporiya@techsoftlabs.com","profilePic":"/schoolapp/images/43b6206f-3715-42cf-b3de-27a6b75bd3c4.jpg","emailId":"piyushporiya@techsoftlabs.com"},"routeId":"13568","schoolId":"129","name":"Udhana","id":"129135681","routeType":"Dropoff","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"263, Dairy Faliya Rd, Minaxi Wadi, Katargam, Surat, Gujarat 395004, India","latitude":"21.2275","stopId":"1","sequenceId":"1","longitude":"72.8368"},{"isSchool":false,"address":"473, Lalita Chowkadi, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.2286","stopId":"2","sequenceId":"2","longitude":"72.8317"}],"driverDetails":{"firstName":"Piyush","lastName":"Poriya","driverId":"piyushporiya@techsoftlabs.com","profilePic":"/schoolapp/images/43b6206f-3715-42cf-b3de-27a6b75bd3c4.jpg","emailId":"piyushporiya@techsoftlabs.com"},"routeId":"8789","schoolId":"129","name":"surat","id":"1298789636528887714325376","routeType":"Dropoff","uniqueId":"636528887714325376"},{"stopage":[{"isSchool":true,"address":"Laxmikant Rd, Mahavir Nagar Society, Patel Nagar, Surat, Gujarat 395004, India","latitude":"21.2281","stopId":"1","sequenceId":"1","longitude":"72.8372"},{"isSchool":false,"address":"Laxmikant Rd, Mahavir Nagar Society, Patel Nagar, Surat, Gujarat 395004, India","latitude":"21.2275","stopId":"2","sequenceId":"2","longitude":"72.8371"}],"driverDetails":null,"routeId":"200100","schoolId":"129","name":"Pool 11","id":"129200100636530799030966400","routeType":"Pickup","uniqueId":"636530799030966400"},{"stopage":[{"isSchool":true,"address":"gajera sch","latitude":"21.2316","stopId":"1","sequenceId":"1","longitude":"72.8394"},{"isSchool":false,"address":"katargam s","latitude":"21.2281","stopId":"2","sequenceId":"2","longitude":"72.8338"},{"isSchool":false,"address":"dabholi circle ","latitude":"21.2305","stopId":"3","sequenceId":"3","longitude":"72.8218"},{"isSchool":false,"address":"smc northzone s","latitude":"21.2348","stopId":"4","sequenceId":"4","longitude":"72.836"}],"driverDetails":{"firstName":"Piyush","lastName":"Poriya","driverId":"piyushporiya@techsoftlabs.com","profilePic":"/schoolapp/images/43b6206f-3715-42cf-b3de-27a6b75bd3c4.jpg","emailId":"piyushporiya@techsoftlabs.com"},"routeId":"1234","schoolId":"129","name":"katargam","id":"12912341","routeType":"Pickup","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"varacha surat","latitude":"21.2437","stopId":"1","sequenceId":"1","longitude":"72.8873"},{"isSchool":false,"address":"katargam s","latitude":"21.2281","stopId":"2","sequenceId":"2","longitude":"72.8338"},{"isSchool":false,"address":"gajera scho","latitude":"21.2323","stopId":"3","sequenceId":"3","longitude":"72.8384"},{"isSchool":false,"address":"Dabholi cir","latitude":"21.1877","stopId":"4","sequenceId":"4","longitude":"72.8514"}],"driverDetails":null,"routeId":"12345","schoolId":"129","name":"varacha","id":"129123451","routeType":"Dropoff","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"hanuman mandir,sec 49 noida ,up","latitude":"28.5575","stopId":"1","sequenceId":"1","longitude":"77.3784"},{"isSchool":false,"address":"baraula maeket noida up","latitude":"28.5599","stopId":"2","sequenceId":"2","longitude":"77.3702"},{"isSchool":false,"address":"sec 37 noida up","latitude":"28.567","stopId":"3","sequenceId":"3","longitude":"77.3418"}],"driverDetails":{"firstName":"mahesh","lastName":"patel","driverId":"mahesh@gmail.com","profilePic":"/schoolapp/images/d69ba7b8-e9bf-47fd-a6a6-5ed72af81acb.png","emailId":"mahesh@gmail.com"},"routeId":"12911","schoolId":"129","name":"noida greater noida","id":"129129111","routeType":"pickup","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"sec 63 noida","latitude":"28.6266","stopId":"1","sequenceId":"1","longitude":"77.3848"},{"isSchool":false,"address":"sec 16 noida","latitude":"28.5787","stopId":"2","sequenceId":"2","longitude":"77.3174"}],"driverDetails":{"firstName":"Jitendra K","lastName":"Kumar","driverId":"jitendra@techsoftlabs.com","profilePic":"/schoolapp/images/93b820c6-96aa-4c20-b934-195743b1c66b.png","emailId":"jitendra@techsoftlabs.com"},"routeId":"12920","schoolId":"129","name":"data rady delhi","id":"129129201","routeType":"pickup","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"jakatnaka","latitude":"22.8223","stopId":"1","sequenceId":"1","longitude":"72.7581"},{"isSchool":false,"address":"varacha circle","latitude":"21.1877","stopId":"2","sequenceId":"2","longitude":"72.8514"}],"driverDetails":{"firstName":"Jay","lastName":"Patel","driverId":"jay@gmail.com","profilePic":"/schoolapp/images/99d02469-a88c-45b7-8d1b-2dfa50aa3341.png","emailId":"jay@gmail.com"},"routeId":"8765","schoolId":"129","name":"jakatnaka, surat","id":"12987651","routeType":"Dropoff","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"katargam","latitude":"21.2281","stopId":"1","sequenceId":"1","longitude":"72.8338"},{"isSchool":false,"address":"dabholi surat","latitude":"21.2314","stopId":"2","sequenceId":"2","longitude":"72.816"}],"driverDetails":null,"routeId":"12918","schoolId":"129","name":"Dabholi","id":"129129181","routeType":"pickup","uniqueId":"1"},{"stopage":[{"isSchool":false,"address":"Mini bazar, Surat ","latitude":"21.2112","stopId":"4","sequenceId":"4","longitude":"72.85"},{"isSchool":false,"address":"hirabag, Surat ","latitude":"21.214","stopId":"3","sequenceId":"3","longitude":"72.8627"},{"isSchool":false,"address":"kapodra, Surat ","latitude":"21.2188","stopId":"2","sequenceId":"2","longitude":"72.8746"},{"isSchool":true,"address":"Utran, Surat ","latitude":"21.2306","stopId":"1","sequenceId":"1","longitude":"72.8671"}],"driverDetails":{"firstName":"Piyush","lastName":"Poriya","driverId":"piyushporiya@techsoftlabs.com","profilePic":"/schoolapp/images/43b6206f-3715-42cf-b3de-27a6b75bd3c4.jpg","emailId":"piyushporiya@techsoftlabs.com"},"routeId":"12922","schoolId":"129","name":"Pickup Darshit Test route","id":"129129221","routeType":"pickup","uniqueId":"1"},{"stopage":[{"isSchool":false,"address":"Modern Public School","latitude":"26.439917","stopId":"1","sequenceId":"1","longitude":"80.286288"},{"isSchool":false,"address":"Shastri Chowk","latitude":"26.443618","stopId":"2","sequenceId":"2","longitude":"80.294579"},{"isSchool":false,"address":"Barra 2, Barra World Bank","latitude":"26.435744","stopId":"3","sequenceId":"3","longitude":"80.304164"},{"isSchool":false,"address":"Bypas Road","latitude":"26.431453","stopId":"4","sequenceId":"4","longitude":"80.307806"},{"isSchool":true,"address":"Public School","latitude":"26.440386","stopId":"5","sequenceId":"5","longitude":"80.28487"}],"driverDetails":{"firstName":"Jitendra K","lastName":"Kumar","driverId":"jitendra@techsoftlabs.com","profilePic":"/schoolapp/images/93b820c6-96aa-4c20-b934-195743b1c66b.png","emailId":"jitendra@techsoftlabs.com"},"routeId":"10","schoolId":"129","name":"SP10","id":"10694093SP10","routeType":"pickup","uniqueId":"694093"},{"stopage":[{"isSchool":false,"address":"Home","latitude":"20.931903","stopId":"1","sequenceId":"1","longitude":"72.917157"},{"isSchool":false,"address":"nandanvan","latitude":"20.933098","stopId":"2","sequenceId":"2","longitude":"72.91393"},{"isSchool":false,"address":"cross road","latitude":"20.922167","stopId":"3","sequenceId":"3","longitude":"72.909332"},{"isSchool":true,"address":"gandhi smruti","latitude":"20.920974","stopId":"4","sequenceId":"4","longitude":"72.925672"}],"driverDetails":{"firstName":"Piyush","lastName":"Poriya","driverId":"piyushporiya@techsoftlabs.com","profilePic":"/schoolapp/images/43b6206f-3715-42cf-b3de-27a6b75bd3c4.jpg","emailId":"piyushporiya@techsoftlabs.com"},"routeId":"9","schoolId":"129","name":"SP9-HOME","id":"9694092SP9-HOME","routeType":"pickup","uniqueId":"694092"},{"stopage":[{"isSchool":true,"address":"Laxmikant Rd, Mahavir Nagar Society, Patel Nagar, Surat, Gujarat 395004, India","latitude":"21.2282","stopId":"1","sequenceId":"1","longitude":"72.8375"},{"isSchool":false,"address":"23, Dairy Faliya Rd, Minaxi Wadi, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.2271","stopId":"2","sequenceId":"2","longitude":"72.8363"},{"isSchool":false,"address":"52, Ranchhodji Park Society 1, Shiv Darshan Society, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.2277","stopId":"3","sequenceId":"3","longitude":"72.832"}],"driverDetails":null,"routeId":"012020182020","schoolId":"129","name":"Pool 12","id":"129012020182020636530835490453376","routeType":"Pickup","uniqueId":"636530835490453376"},{"stopage":[{"isSchool":true,"address":"Laxmikant Rd, Mahavir Nagar Society, Patel Nagar, Surat, Gujarat 395004, India","latitude":"21.2282","stopId":"1","sequenceId":"1","longitude":"72.8375"},{"isSchool":false,"address":"23, Dairy Faliya Rd, Minaxi Wadi, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.2271","stopId":"2","sequenceId":"2","longitude":"72.8363"},{"isSchool":false,"address":"52, Ranchhodji Park Society 1, Shiv Darshan Society, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.2277","stopId":"3","sequenceId":"3","longitude":"72.832"}],"driverDetails":null,"routeId":"012020182020","schoolId":"129","name":"Pool 12","id":"129012020182020636530835615936384","routeType":"Pickup","uniqueId":"636530835615936384"},{"stopage":[{"isSchool":true,"address":"208, Khodiyar Krupa Society, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.2264","stopId":"1","sequenceId":"1","longitude":"72.8344"},{"isSchool":false,"address":"151, Amroli Rd, Kailashdham Society, Mahavir Nagar Society, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.229","stopId":"2","sequenceId":"2","longitude":"72.8384"},{"isSchool":false,"address":"195, Kanteshwar Society, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.2304","stopId":"3","sequenceId":"3","longitude":"72.8283"}],"driverDetails":null,"routeId":"300120330","schoolId":"129","name":"pool4","id":"129300120330636530009027735936","routeType":"Pickup","uniqueId":"636530009027735936"},{"stopage":[{"isSchool":true,"address":"B/101, Central Park, Pandesara GIDC, Udhna, Surat, Gujarat 394221, India","latitude":"21.1444","stopId":"1","sequenceId":"1","longitude":"72.8379"},{"isSchool":false,"address":"Ashoka Shopping Centre, Plot No. 197, Udhana - Sachin Hwy, Near S B I, Pandesara GIDC, Udhna, Surat, Gujarat 394221, India","latitude":"21.1425","stopId":"2","sequenceId":"2","longitude":"72.8491"},{"isSchool":false,"address":"udhana","latitude":"21.1431","stopId":"3","sequenceId":"3","longitude":"72.8431"}],"driverDetails":null,"routeId":"13569","schoolId":"129","name":"udhana","id":"129135691","routeType":"Pickup","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"katargam surat","latitude":"21.2281","stopId":"1","sequenceId":"1","longitude":"72.8338"},{"isSchool":false,"address":"gajera circle","latitude":"21.2268","stopId":"2","sequenceId":"2","longitude":"72.8381"}],"driverDetails":null,"routeId":"30012031","schoolId":"129","name":"pool 2","id":"12930012031636529990577977600","routeType":"Pickup","uniqueId":"636529990577977600"},{"stopage":[{"isSchool":true,"address":"katargam","latitude":"25.3548","stopId":"1","sequenceId":"1","longitude":"51.1839"},{"isSchool":false,"address":"varacha surat","latitude":"21.2437","stopId":"2","sequenceId":"2","longitude":"72.8873"}],"driverDetails":null,"routeId":"130020181","schoolId":"129","name":"varacha surat","id":"129130020181636529897323589376","routeType":"Pickup","uniqueId":"636529897323589376"},{"driverDetails":null,"routeId":"","schoolId":"129","name":"asdf","id":"1291","routeType":"Pickup","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"256, Dairy Faliya Rd, Minaxi Wadi, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.2276","stopId":"1","sequenceId":"1","longitude":"72.8359"},{"isSchool":false,"address":"14, Amroli Rd, Mahavir Nagar Society, Patel Nagar, Surat, Gujarat 395004, India","latitude":"21.2281","stopId":"2","sequenceId":"2","longitude":"72.8379"},{"isSchool":false,"address":"1-2, Dairy Faliya Rd, Limbachiya Faliya, Khodiyar Krupa Society, Katargam, Surat, Gujarat 395004, India","latitude":"21.227","stopId":"3","sequenceId":"3","longitude":"72.834"}],"driverDetails":null,"routeId":"12021612531","schoolId":"129","name":"Pool 13","id":"12912021612531636530841174379008","routeType":"Pickup","uniqueId":"636530841174379008"},{"stopage":[{"isSchool":true,"address":"akshardham delhi","latitude":"28.6127","stopId":"1","sequenceId":"1","longitude":"77.2773"},{"isSchool":false,"address":"India gate Delhi","latitude":"28.6129","stopId":"2","sequenceId":"2","longitude":"77.2295"},{"isSchool":false,"address":"international airport Delhi","latitude":"28.5564","stopId":"3","sequenceId":"3","longitude":"77.1015"}],"driverDetails":{"firstName":"Piyush","lastName":"Poriya","driverId":"piyushporiya@techsoftlabs.com","profilePic":"/schoolapp/images/43b6206f-3715-42cf-b3de-27a6b75bd3c4.jpg","emailId":"piyushporiya@techsoftlabs.com"},"routeId":"12907","schoolId":"129","name":"DL route","id":"129129071","routeType":"Dropoff","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"kanpur","latitude":"26.4499","stopId":"1","sequenceId":"1","longitude":"80.3319"},{"isSchool":false,"address":"lucknow","latitude":"26.8467","stopId":"2","sequenceId":"2","longitude":"80.9462"},{"isSchool":false,"address":"unnao","latitude":"26.5393","stopId":"3","sequenceId":"3","longitude":"80.4878"}],"driverDetails":{"firstName":"Pankaj","lastName":"Mr","driverId":"pankajcpmishra@gmail.com","profilePic":"/schoolapp/images/688f9945-7736-4d25-820a-6d346177b668.png","emailId":"pankajcpmishra@gmail.com"},"routeId":"12919","schoolId":"129","name":"kanpur lucknow","id":"129129191","routeType":"pickup","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"Ish Krupa, Bhandarkar Marg, Brhmanwada, Matunga, Mumbai, Maharashtra 400019, India","latitude":"19.0274","stopId":"1","sequenceId":"1","longitude":"72.8543"},{"isSchool":false,"address":"kapodara Surat","latitude":"21.2188","stopId":"2","sequenceId":"2","longitude":"72.8746"}],"driverDetails":{"firstName":"J","lastName":"Y.   K","driverId":"ndndndnd@dhdndjjdd.com","profilePic":"/schoolapp/images/db5982a8-31fb-4cc3-a91f-bc16f926252e.png","emailId":"ndndndnd@dhdndjjdd.com"},"routeId":"12925","schoolId":"129","name":"Katanga","id":"129129251","routeType":"pickup","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"Royal Square surat","latitude":"43.5025","stopId":"1","sequenceId":"1","longitude":"-96.7897"},{"isSchool":false,"address":"Vip Circle surat","latitude":"21.2315","stopId":"2","sequenceId":"2","longitude":"72.8663"},{"isSchool":false,"address":"kapodara surat","latitude":"21.2188","stopId":"3","sequenceId":"3","longitude":"72.8746"},{"isSchool":false,"address":"Hirabug surat","latitude":"21.2437","stopId":"4","sequenceId":"4","longitude":"72.8873"},{"isSchool":false,"address":"P.P Savani School","latitude":"21.21728","stopId":"5","sequenceId":"5","longitude":"72.859585"}],"driverDetails":{"firstName":"Piyush","lastName":"Poriya","driverId":"piyushporiya@techsoftlabs.com","profilePic":"/schoolapp/images/43b6206f-3715-42cf-b3de-27a6b75bd3c4.jpg","emailId":"piyushporiya@techsoftlabs.com"},"routeId":"9","schoolId":"129","name":"SP9-OFFICE","id":"96940921SP9-OFFICE","routeType":"dropoff","uniqueId":"6940921"},{"stopage":[{"isSchool":true,"address":"gajera school","latitude":"21.2323","stopId":"1","sequenceId":"1","longitude":"72.8384"},{"isSchool":false,"address":"katargam surat","latitude":"21.2281","stopId":"2","sequenceId":"2","longitude":"72.8338"},{"isSchool":false,"address":"varacha surat","latitude":"21.2437","stopId":"3","sequenceId":"3","longitude":"72.8873"},{"isSchool":false,"address":"smc northzone surat","latitude":"21.2348","stopId":"4","sequenceId":"4","longitude":"72.836"}],"driverDetails":null,"routeId":"3001201800","schoolId":"129","name":"kapodara","id":"1293001201800636529009144680192","routeType":"Pickup","uniqueId":"636529009144680192"},{"stopage":[{"isSchool":true,"address":"katargam surat","latitude":"21.2281","stopId":"1","sequenceId":"1","longitude":"72.8338"},{"isSchool":false,"address":"varacha surat","latitude":"21.2437","stopId":"2","sequenceId":"2","longitude":"72.8873"}],"driverDetails":null,"routeId":"30012030","schoolId":"129","name":"pool","id":"12930012030636529988667943552","routeType":"Pickup","uniqueId":"636529988667943552"},{"stopage":[{"isSchool":false,"address":"Vip Circle","latitude":"21.231759","stopId":"1","sequenceId":"1","longitude":"72.866349"},{"isSchool":false,"address":"Sudama Chowk","latitude":"21.237460","stopId":"2","sequenceId":"2","longitude":"72.877486"},{"isSchool":true,"address":"Ram Chowk","latitude":"21.241757","stopId":"3","sequenceId":"3","longitude":"72.880940"}],"driverDetails":{"firstName":"Piyush","lastName":"Poriya","driverId":"piyushporiya@techsoftlabs.com","profilePic":"/schoolapp/images/43b6206f-3715-42cf-b3de-27a6b75bd3c4.jpg","emailId":"piyushporiya@techsoftlabs.com"},"routeId":"12901","schoolId":"129","name":"SITR","id":"129011SITR","routeType":null,"uniqueId":"1"},{"stopage":[{"isSchool":"72.816","address":"dabholisurat","latitude":"21.2314","stopId":"","sequenceId":"","longitude":"72.816"}],"driverDetails":{"firstName":"Piyush","lastName":"Poriya","driverId":"piyushporiya@techsoftlabs.com","profilePic":"/schoolapp/images/43b6206f-3715-42cf-b3de-27a6b75bd3c4.jpg","emailId":"piyushporiya@techsoftlabs.com"},"routeId":"879","schoolId":"129","name":"dabholi","id":"1298791","routeType":"Pickup","uniqueId":"1"},{"stopage":[],"driverDetails":null,"routeId":"3001201802","schoolId":"129","name":"D Route 2","id":"1293001201802636528946988682112","routeType":"Pickup","uniqueId":"636528946988682112"},{"driverDetails":null,"routeId":"90","schoolId":"129","name":"SP9-HOME","id":"129901","routeType":"pickup","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"Laxmikant Rd, Mahavir Nagar Society, Patel Nagar, Surat, Gujarat 395004, India","latitude":"21.2282","stopId":"1","sequenceId":"1","longitude":"72.8375"},{"isSchool":false,"address":"23, Dairy Faliya Rd, Minaxi Wadi, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.2271","stopId":"2","sequenceId":"2","longitude":"72.8363"},{"isSchool":false,"address":"52, Ranchhodji Park Society 1, Shiv Darshan Society, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.2277","stopId":"3","sequenceId":"3","longitude":"72.832"}],"driverDetails":null,"routeId":"012020182020","schoolId":"129","name":"Pool 12","id":"129012020182020636530835919658496","routeType":"Pickup","uniqueId":"636530835919658496"},{"stopage":[{"isSchool":true,"address":"Laxmikant Rd, Mahavir Nagar Society, Patel Nagar, Surat, Gujarat 395004, India","latitude":"21.2283","stopId":"1","sequenceId":"1","longitude":"72.8375"},{"isSchool":false,"address":"129, Gopinath Society, Subhash Nagar Society, Vishal Nagar, सूरत, Gujarat 395004, India","latitude":"21.2284","stopId":"2","sequenceId":"2","longitude":"72.8286"},{"isSchool":false,"address":"231, Lalita Chowkadi, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.2298","stopId":"3","sequenceId":"3","longitude":"72.8315"}],"driverDetails":null,"routeId":"01022018002","schoolId":"129","name":"pool 9","id":"12901022018002636530784362992256","routeType":"Pickup","uniqueId":"636530784362992256"},{"stopage":[{"isSchool":true,"address":"KESCO F","latitude":"26.443911","stopId":"1","sequenceId":"1","longitude":"80.2936"},{"isSchool":false,"address":"Saurabh Guest House E","latitude":"26.44454","stopId":"2","sequenceId":"2","longitude":"80.291612"},{"isSchool":false,"address":"Bank of Baroda D","latitude":"26.446029","stopId":"3","sequenceId":"3","longitude":"80.286929"},{"isSchool":false,"address":"Ratan Lal Nagar,labour C","latitude":"26.443421","stopId":"4","sequenceId":"4","longitude":"80.287053"}],"driverDetails":{"firstName":"Jitendra K","lastName":"Kumar","driverId":"jitendra@techsoftlabs.com","profilePic":"/schoolapp/images/93b820c6-96aa-4c20-b934-195743b1c66b.png","emailId":"jitendra@techsoftlabs.com"},"routeId":"12","schoolId":"129","name":"SP12","id":"1269409321SP12","routeType":"dropoff","uniqueId":"69409321"},{"stopage":[{"isSchool":true,"address":"katargam surat","latitude":"21.2281","stopId":"1","sequenceId":"1","longitude":"72.8338"},{"isSchool":false,"address":"varacha surat","latitude":"21.2437","stopId":"2","sequenceId":"2","longitude":"72.8873"}],"driverDetails":null,"routeId":"30012030","schoolId":"129","name":"pool","id":"12930012030636529989383908608","routeType":"Pickup","uniqueId":"636529989383908608"},{"stopage":[{"isSchool":true,"address":"katargam surat","latitude":"21.2281","stopId":"1","sequenceId":"1","longitude":"72.8338"},{"isSchool":false,"address":"katargam surat","latitude":"21.2281","stopId":"2","sequenceId":"2","longitude":"72.8338"}],"driverDetails":null,"routeId":"01022018011","schoolId":"129","name":"pool 10","id":"12901022018011636530794656714880","routeType":"Pickup","uniqueId":"636530794656714880"},{"stopage":[{"isSchool":true,"address":"Laxmikant Rd, Mahavir Nagar Society, Patel Nagar, Surat, Gujarat 395004, India","latitude":"21.2273","stopId":"1","sequenceId":"1","longitude":"72.8377"},{"isSchool":false,"address":"katargam surat","latitude":"21.2281","stopId":"2","sequenceId":"2","longitude":"72.8338"},{"isSchool":false,"address":"117, Limbachiya Faliya, Khodiyar Krupa Society, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.2262","stopId":"3","sequenceId":"3","longitude":"72.8332"}],"driverDetails":null,"routeId":"0102201800","schoolId":"129","name":"Pool 8","id":"1290102201800636530733245148416","routeType":"Dropoff","uniqueId":"636530733245148416"},{"stopage":[{"isSchool":true,"address":"344, Lalita Chowkadi, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.2293","stopId":"1","sequenceId":"1","longitude":"72.8313"},{"isSchool":false,"address":"93, Fulpada Rd, Smruti Society, Patel Nagar, Surat, Gujarat 395004, India","latitude":"21.2274","stopId":"2","sequenceId":"2","longitude":"72.8395"},{"isSchool":false,"address":"193, Dabholi Rd, Nandanvan Society, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.23","stopId":"3","sequenceId":"3","longitude":"72.8276"}],"driverDetails":{"firstName":"Jitu","lastName":"Yadav","driverId":"umesh@techsoftlabs.com","profilePic":"/schoolapp/images/8bb2e518-d7df-4706-86c2-c9c47c4e8d01.png","emailId":"umesh@techsoftlabs.com"},"routeId":"pool 6","schoolId":"129","name":"30012035","id":"129pool 6636530011818453760","routeType":"Pickup","uniqueId":"636530011818453760"},{"stopage":[{"isSchool":true,"address":"chikuvadi surat","latitude":"21.2182","stopId":"1","sequenceId":"1","longitude":"72.8826"},{"isSchool":false,"address":"VIP Circle,surat","latitude":"21.2315","stopId":"2","sequenceId":"2","longitude":"72.8663"},{"isSchool":false,"address":"kapodara, surat","latitude":"21.2188","stopId":"3","sequenceId":"3","longitude":"72.8746"}],"driverDetails":{"firstName":"Piyush","lastName":"Poriya","driverId":"piyushporiya@techsoftlabs.com","profilePic":"/schoolapp/images/43b6206f-3715-42cf-b3de-27a6b75bd3c4.jpg","emailId":"piyushporiya@techsoftlabs.com"},"routeId":"102011","schoolId":"129","name":"Social Route","id":"129102011636530878117669248","routeType":"Pickup","uniqueId":"636530878117669248"},{"stopage":[{"isSchool":true,"address":"sec 63 noida","latitude":"28.6266","stopId":"1","sequenceId":"1","longitude":"77.3848"},{"isSchool":false,"address":"sec 16 noida","latitude":"28.5787","stopId":"2","sequenceId":"2","longitude":"77.3174"}],"driverDetails":{"firstName":"Jitu","lastName":"Yadav","driverId":"umesh@techsoftlabs.com","profilePic":"/schoolapp/images/8bb2e518-d7df-4706-86c2-c9c47c4e8d01.png","emailId":"umesh@techsoftlabs.com"},"routeId":"12921","schoolId":"129","name":"noida dataready","id":"129129211","routeType":"pickup","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"Surat ","latitude":"21.1701","stopId":"1","sequenceId":"1","longitude":"72.8302"},{"isSchool":false,"address":"varachha Surat","latitude":"21.2003","stopId":"2","sequenceId":"2","longitude":"72.8404"}],"driverDetails":null,"routeId":"12904","schoolId":"129","name":"dddd rtrt","id":"129129041","routeType":"pickup","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"Ayodhya ","latitude":"26.788","stopId":"1","sequenceId":"1","longitude":"82.1986"},{"isSchool":false,"address":"kaniganj ayodhya","latitude":"26.787","stopId":"2","sequenceId":"2","longitude":"82.2033"}],"driverDetails":{"firstName":"Piyush","lastName":"Poriya","driverId":"piyushporiya@techsoftlabs.com","profilePic":"/schoolapp/images/43b6206f-3715-42cf-b3de-27a6b75bd3c4.jpg","emailId":"piyushporiya@techsoftlabs.com"},"routeId":"12908","schoolId":"129","name":"Ram Drop","id":"129129081","routeType":"dropoff","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"sector 49 noida ","latitude":"28.5622","stopId":"1","sequenceId":"1","longitude":"77.3741"}],"driverDetails":null,"routeId":"1232123","schoolId":"129","name":"noida ","id":"1291232123636531454242735488","routeType":"Pickup","uniqueId":"636531454242735488"},{"stopage":[{"isSchool":true,"address":"\u2018klajsdk;figures","latitude":"49.9935","stopId":"1","sequenceId":"1","longitude":"20.299"}],"driverDetails":null,"routeId":"12913","schoolId":"129","name":"Modern School","id":"129129131","routeType":"Pickup","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"10, Ranchhodji Park Society 1, Shiv Darshan Society, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.2287","stopId":"1","sequenceId":"1","longitude":"72.8321"},{"isSchool":false,"address":"15, Amroli Rd, Smruti Society, Patel Nagar, Surat, Gujarat 395004, India","latitude":"21.2274","stopId":"2","sequenceId":"2","longitude":"72.8385"},{"isSchool":false,"address":"196, Kanteshwar Society, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.2303","stopId":"3","sequenceId":"3","longitude":"72.828"}],"driverDetails":{"firstName":"Jitendra K","lastName":"Kumar","driverId":"jitendra@techsoftlabs.com","profilePic":"/schoolapp/images/93b820c6-96aa-4c20-b934-195743b1c66b.png","emailId":"jitendra@techsoftlabs.com"},"routeId":"pool 5","schoolId":"129","name":"30012034","id":"129pool 5636530010659056000","routeType":"Pickup","uniqueId":"636530010659056000"},{"stopage":[{"isSchool":false,"address":"Hanuman Temple","latitude":"28.557471","stopId":"6","sequenceId":"6","longitude":"77.378570"},{"isSchool":false,"address":"Hindon Vihar UT","latitude":"28.554615","stopId":"5","sequenceId":"5","longitude":"77.373763"},{"isSchool":false,"address":"Dadri Main Rd 1","latitude":"28.550346","stopId":"4","sequenceId":"4","longitude":"77.378323"},{"isSchool":false,"address":"Dadri Main Rd","latitude":"28.551910","stopId":"3","sequenceId":"3","longitude":"77.377036"},{"isSchool":false,"address":"Link Rd","latitude":"28.552796","stopId":"2","sequenceId":"2","longitude":"77.376489"},{"isSchool":true,"address":"49 Office (School)","latitude":"28.557725","stopId":"1","sequenceId":"1","longitude":"77.378795"}],"driverDetails":{"firstName":"Jitendra K","lastName":"Kumar","driverId":"jitendra@techsoftlabs.com","profilePic":"/schoolapp/images/93b820c6-96aa-4c20-b934-195743b1c66b.png","emailId":"jitendra@techsoftlabs.com"},"routeId":"12903","schoolId":"129","name":"DS-hanumantemple-to-office-dropoff","id":"129129031","routeType":"dropoff","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"Utran, Surat","latitude":"21.2306","stopId":"1","sequenceId":"1","longitude":"72.8671"},{"isSchool":false,"address":"1, Surat - Kamrej Hwy, Anjana Society, Adarsh Society, Ram Nagar, Varachha, Surat, Gujarat 395006, India","latitude":"21.2174","stopId":"2","sequenceId":"2","longitude":"72.8664"},{"isSchool":false,"address":"Hirabag, Su","latitude":"21.214","stopId":"3","sequenceId":"3","longitude":"72.8627"},{"isSchool":false,"address":"Baroda Pristage, Surat","latitude":"21.2294","stopId":"4","sequenceId":"4","longitude":"72.842"}],"driverDetails":null,"routeId":"3001201802","schoolId":"129","name":"D Route 2","id":"1293001201802636528912610771200","routeType":"Pickup","uniqueId":"636528912610771200"},{"stopage":[{"isSchool":false,"address":"49 Office","latitude":"28.557725","stopId":"1","sequenceId":"1","longitude":"77.378795"},{"isSchool":false,"address":"Link Rd","latitude":"28.552796","stopId":"2","sequenceId":"2","longitude":"77.376489"},{"isSchool":false,"address":"Dadri Main Rd","latitude":"28.551910","stopId":"3","sequenceId":"3","longitude":"77.377036"},{"isSchool":false,"address":"Dadri Main Rd 1","latitude":"28.550346","stopId":"4","sequenceId":"4","longitude":"77.378323"},{"isSchool":false,"address":"Hindon Vihar UT","latitude":"28.554615","stopId":"5","sequenceId":"5","longitude":"77.373763"},{"isSchool":true,"address":"Hanuman Temple (School)","latitude":"28.557471","stopId":"6","sequenceId":"6","longitude":"77.378570"}],"driverDetails":{"firstName":"Jitendra K","lastName":"Kumar","driverId":"jitendra@techsoftlabs.com","profilePic":"/schoolapp/images/93b820c6-96aa-4c20-b934-195743b1c66b.png","emailId":"jitendra@techsoftlabs.com"},"routeId":"12902","schoolId":"129","name":"DS-office-to-hanumantemple-pickup","id":"129129021","routeType":"pickup","uniqueId":"1"},{"stopage":[{"isSchool":"72.8671","address":"Utran Surat","latitude":"21.2306","stopId":"1","sequenceId":"1","longitude":"72.8671"},{"isSchool":"72.8873","address":"Kapodra sura","latitude":"21.2437","stopId":"2","sequenceId":"2","longitude":"72.8873"},{"isSchool":"72.8617","address":"Hirabag Su","latitude":"21.2148","stopId":"3","sequenceId":"3","longitude":"72.8617"},{"isSchool":"72.8513","address":"Minibazar surat","latitude":"21.2114","stopId":"4","sequenceId":"4","longitude":"72.8513"}],"driverDetails":{"firstName":"J","lastName":"Y.   K","driverId":"ndndndnd@dhdndjjdd.com","profilePic":"/schoolapp/images/db5982a8-31fb-4cc3-a91f-bc16f926252e.png","emailId":"ndndndnd@dhdndjjdd.com"},"routeId":"30012018","schoolId":"129","name":"D Route 1","id":"12930012018636528900590937344","routeType":"Pickup","uniqueId":"636528900590937344"},{"stopage":[{"isSchool":"72.917157","address":"Home","latitude":"20.931903","stopId":"1","sequenceId":"1","longitude":"72.917157"},{"isSchool":"72.91393","address":"nandanvan","latitude":"20.933098","stopId":"2","sequenceId":"2","longitude":"72.91393"},{"isSchool":"72.909332","address":"cross road","latitude":"20.922167","stopId":"3","sequenceId":"3","longitude":"72.909332"},{"isSchool":"72.925672","address":"gandhi smruti","latitude":"20.920974","stopId":"4","sequenceId":"4","longitude":"72.925672"},{"isSchool":"72.9609","address":"kamreg surat","latitude":"21.2676","stopId":"","sequenceId":"","longitude":"72.9609"}],"driverDetails":{"firstName":"Piyush","lastName":"Poriya","driverId":"piyushporiya@techsoftlabs.com","profilePic":"/schoolapp/images/43b6206f-3715-42cf-b3de-27a6b75bd3c4.jpg","emailId":"piyushporiya@techsoftlabs.com"},"routeId":"9","schoolId":"129","name":"SP9-HOME","id":"12991","routeType":"pickup","uniqueId":"1"},{"stopage":[{"isSchool":"51.1839","address":"katar","latitude":"25.3548","stopId":"","sequenceId":"","longitude":"51.1839"}],"driverDetails":null,"routeId":"542","schoolId":"129","name":"katargam","id":"1295421","routeType":"Pickup","uniqueId":"1"},{"driverDetails":null,"routeId":"1","schoolId":"129","name":null,"id":"12911","routeType":"Pickup","uniqueId":"1"},{"driverDetails":null,"routeId":null,"schoolId":"129","name":null,"id":"129null1","routeType":"Pickup","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"332, Shreeji Park, Haridarshan Society, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.2281","stopId":"1","sequenceId":"1","longitude":"72.833"},{"isSchool":false,"address":"151, Amroli Rd, Kailashdham Society, Mahavir Nagar Society, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.229","stopId":"2","sequenceId":"2","longitude":"72.8384"},{"isSchool":false,"address":"195, Kanteshwar Society, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.2304","stopId":"3","sequenceId":"3","longitude":"72.8283"}],"driverDetails":null,"routeId":"300120330","schoolId":"129","name":"pool4","id":"129300120330636530009134919680","routeType":"Pickup","uniqueId":"636530009134919680"},{"stopage":[{"isSchool":true,"address":"14, Amroli Rd, Mahavir Nagar Society, Patel Nagar, Surat, Gujarat 395004, India","latitude":"21.2282","stopId":"1","sequenceId":"1","longitude":"72.8382"},{"isSchool":false,"address":"Laxmikant Rd, Mahavir Nagar Society, Patel Nagar, Surat, Gujarat 395004, India","latitude":"21.2282","stopId":"2","sequenceId":"2","longitude":"72.837"},{"isSchool":false,"address":"B-12, Laxmi Narayan Society, Raj Anand Society, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.2296","stopId":"3","sequenceId":"3","longitude":"72.8349"}],"driverDetails":null,"routeId":"01022018001","schoolId":"129","name":"pool 7","id":"12901022018001636530741431430784","routeType":"Pickup","uniqueId":"636530741431430784"},{"stopage":[{"isSchool":true,"address":"13, Bharatnath Society, Shreeji Park, Hari Darshan Society, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.2281","stopId":"1","sequenceId":"1","longitude":"72.8342"},{"isSchool":false,"address":"57, Shiv Darshan Society, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.2276","stopId":"2","sequenceId":"2","longitude":"72.8327"}],"driverDetails":null,"routeId":"30012032","schoolId":"129","name":"pool 3","id":"12930012032636529991382203776","routeType":"Pickup","uniqueId":"636529991382203776"},{"stopage":[{"isSchool":true,"address":"Dwarka temple gujarat","latitude":"22.2376","stopId":"1","sequenceId":"1","longitude":"68.9674"},{"isSchool":false,"address":"moksha dwaaram dwarka","latitude":"22.2377","stopId":"2","sequenceId":"2","longitude":"68.9677"},{"isSchool":false,"address":"sunset at dwarka gujarat","latitude":"22.2422","stopId":"3","sequenceId":"3","longitude":"68.9562"}],"driverDetails":null,"routeId":"12906","schoolId":"129","name":"RK Route","id":"129129061","routeType":"pickup","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"katargam surat","latitude":"21.2281","stopId":"1","sequenceId":"1","longitude":"72.8338"},{"isSchool":false,"address":"gajera circle","latitude":"21.2268","stopId":"2","sequenceId":"2","longitude":"72.8381"}],"driverDetails":null,"routeId":"30012031","schoolId":"129","name":"pool 2","id":"12930012031636529990755815296","routeType":"Pickup","uniqueId":"636529990755815296"},{"stopage":[{"isSchool":true,"address":"Katanga","latitude":"-8.88511","stopId":"1","sequenceId":"1","longitude":"26.4194"},{"isSchool":false,"address":"dab hole ","latitude":"16.9606","stopId":"2","sequenceId":"2","longitude":"73.5977"}],"driverDetails":null,"routeId":"12915","schoolId":"129","name":"maheshbha","id":"129129151","routeType":"pickup","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"surat","latitude":"21.1702","stopId":"1","sequenceId":"1","longitude":"72.8311"},{"isSchool":false,"address":"katargam surat","latitude":"21.2281","stopId":"2","sequenceId":"2","longitude":"72.8338"}],"driverDetails":{"firstName":"Jitu","lastName":"Yadav","driverId":"umesh@techsoftlabs.com","profilePic":"/schoolapp/images/8bb2e518-d7df-4706-86c2-c9c47c4e8d01.png","emailId":"umesh@techsoftlabs.com"},"routeId":"12916","schoolId":"129","name":"maheshbhai","id":"129129161","routeType":"pickup","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"Surat ","latitude":"21.1701","stopId":"1","sequenceId":"1","longitude":"72.8302"},{"isSchool":false,"address":"varachha Surat","latitude":"21.2003","stopId":"2","sequenceId":"2","longitude":"72.8404"}],"driverDetails":null,"routeId":"12905","schoolId":"129","name":"DDDDDD","id":"129129051","routeType":"pickup","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"\u2018klajsdk;figures","latitude":"49.9935","stopId":"1","sequenceId":"1","longitude":"20.299"}],"driverDetails":null,"routeId":"12912","schoolId":"129","name":"Modern School","id":"129129121","routeType":"Pickup","uniqueId":"1"},{"stopage":[{"isSchool":false,"address":"kamrej surat","latitude":"21.2676","stopId":"4","sequenceId":"4","longitude":"72.9609"},{"isSchool":false,"address":"laskana Surat ","latitude":"21.2442","stopId":"3","sequenceId":"3","longitude":"72.9188"},{"isSchool":false,"address":"Jakatnaka Surat","latitude":"21.1915","stopId":"2","sequenceId":"2","longitude":"72.821"},{"isSchool":true,"address":"Utran Surat ","latitude":"21.2306","stopId":"1","sequenceId":"1","longitude":"72.8671"}],"driverDetails":null,"routeId":"12924","schoolId":"129","name":"New test by Darshit","id":"129129241","routeType":"dropoff","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"smc northzone surat","latitude":"21.2348","stopId":"1","sequenceId":"1","longitude":"72.836"},{"isSchool":false,"address":"gajera school","latitude":"21.2323","stopId":"2","sequenceId":"2","longitude":"72.8384"},{"isSchool":false,"address":"katargam surat","latitude":"21.2281","stopId":"3","sequenceId":"3","longitude":"72.8338"},{"isSchool":false,"address":"varacha surat","latitude":"21.2437","stopId":"4","sequenceId":"4","longitude":"72.8873"}],"driverDetails":null,"routeId":"3001201800","schoolId":"129","name":"kapodara","id":"1293001201800636529008144003328","routeType":"Dropoff","uniqueId":"636529008144003328"},{"stopage":[{"isSchool":true,"address":"Hirabag","latitude":"33.872","stopId":"1","sequenceId":"1","longitude":"48.9751"},{"isSchool":false,"address":"kapodara surat","latitude":"21.2188","stopId":"2","sequenceId":"2","longitude":"72.8746"}],"driverDetails":null,"routeId":"13002018","schoolId":"129","name":"hi Rabat","id":"12913002018636529895923927552","routeType":"Pickup","uniqueId":"636529895923927552"},{"stopage":[{"isSchool":true,"address":"dabholisurat","latitude":"21.2314","stopId":"1","sequenceId":"1","longitude":"72.816"},{"isSchool":false,"address":"katargam s","latitude":"21.2281","stopId":"2","sequenceId":"2","longitude":"72.8338"}],"driverDetails":{"firstName":"Driver","lastName":"Test1","driverId":"drivertest@gmail.com","profilePic":"/schoolapp/images/6206972f-bf6c-4fda-87da-50d72963c85c.png","emailId":"drivertest@gmail.com"},"routeId":"8790","schoolId":"129","name":"dabholi","id":"12987901","routeType":"Dropoff","uniqueId":"1"},{"stopage":[{"isSchool":"72.8338","address":"katargam surat","latitude":"21.2281","stopId":"","sequenceId":"","longitude":"72.8338"}],"driverDetails":null,"routeId":"1243","schoolId":"129","name":"varacha","id":"12912431","routeType":"Pickup","uniqueId":"1"},{"stopage":[{"isSchool":"72.8311","address":"suraty","latitude":"21.1702","stopId":"","sequenceId":"","longitude":"72.8311"}],"driverDetails":null,"routeId":"546","schoolId":"129","name":"surat","id":"1295461","routeType":"Pickup","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"Rawdat Rashed Rd, Qatar","latitude":"25.3547","stopId":"1","sequenceId":"1","longitude":"51.1873"},{"isSchool":false,"address":"Rawdat Rashed Rd, Qatar","latitude":"25.3546","stopId":"2","sequenceId":"2","longitude":"51.1868"},{"isSchool":false,"address":"Unnamed Road, Qatar","latitude":"25.3562","stopId":"3","sequenceId":"3","longitude":"51.1778"}],"driverDetails":null,"routeId":"0201020","schoolId":"129","name":"pool 8","id":"1290201020636530647509843328","routeType":"Dropoff","uniqueId":"636530647509843328"},{"stopage":[{"isSchool":true,"address":"India gate","latitude":"28.0123","stopId":"1","sequenceId":"1","longitude":"77.2222"},{"isSchool":false,"address":"airport Delhi ","latitude":"28.5523","stopId":"2","sequenceId":"2","longitude":"77.145"},{"isSchool":false,"address":"palika bazar Delhi ","latitude":"28.631","stopId":"3","sequenceId":"3","longitude":"77.2186"}],"driverDetails":{"firstName":"Jitendra K","lastName":"Kumar","driverId":"jitendra@techsoftlabs.com","profilePic":"/schoolapp/images/93b820c6-96aa-4c20-b934-195743b1c66b.png","emailId":"jitendra@techsoftlabs.com"},"routeId":"12914","schoolId":"129","name":"Delhi New Route With Sales Team","id":"129129141","routeType":"dropoff","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"Not Out","latitude":"12.0268","stopId":"1","sequenceId":"1","longitude":"75.266"},{"isSchool":false,"address":"Is it ","latitude":"41.5892","stopId":"2","sequenceId":"2","longitude":"14.1931"}],"driverDetails":null,"routeId":"12909","schoolId":"129","name":"Test Route","id":"129129091","routeType":"pickup","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"surat","latitude":"21.1702","stopId":"1","sequenceId":"1","longitude":"72.8311"},{"isSchool":false,"address":"katargam","latitude":"21.2281","stopId":"2","sequenceId":"2","longitude":"72.8338"}],"driverDetails":null,"routeId":"12917","schoolId":"129","name":"mannish has","id":"129129171","routeType":"pickup","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"hello","latitude":"63.8355","stopId":"1","sequenceId":"1","longitude":"-20.3919"},{"isSchool":false,"address":"Fella","latitude":"49.7737","stopId":"2","sequenceId":"2","longitude":"6.78023"}],"driverDetails":null,"routeId":"12910","schoolId":"129","name":"Route Done","id":"129129101","routeType":"pickup","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"15, Amroli Rd, Smruti Society, Patel Nagar, Surat, Gujarat 395004, India","latitude":"21.2274","stopId":"1","sequenceId":"1","longitude":"72.8385"},{"isSchool":false,"address":"196, Kanteshwar Society, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.2303","stopId":"2","sequenceId":"2","longitude":"72.828"},{"isSchool":false,"address":"10, Ranchhodji Park Society 1, Shiv Darshan Society, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.2286","stopId":"3","sequenceId":"3","longitude":"72.8321"}],"driverDetails":{"firstName":"Jitendra K","lastName":"Kumar","driverId":"jitendra@techsoftlabs.com","profilePic":"/schoolapp/images/93b820c6-96aa-4c20-b934-195743b1c66b.png","emailId":"jitendra@techsoftlabs.com"},"routeId":"pool 5","schoolId":"129","name":"30012034","id":"129pool 5636530010534664576","routeType":"Pickup","uniqueId":"636530010534664576"},{"stopage":[{"isSchool":true,"address":"196, Kanteshwar Society, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.2303","stopId":"1","sequenceId":"1","longitude":"72.828"},{"isSchool":false,"address":"10, Ranchhodji Park Society 1, Shiv Darshan Society, Vishal Nagar, Surat, Gujarat 395004, India","latitude":"21.2286","stopId":"2","sequenceId":"2","longitude":"72.8321"},{"isSchool":false,"address":"15, Amroli Rd, Smruti Society, Patel Nagar, Surat, Gujarat 395004, India","latitude":"21.2274","stopId":"3","sequenceId":"3","longitude":"72.8385"},{"isSchool":false,"address":"surat railway station surat","latitude":"21.205","stopId":"4","sequenceId":"4","longitude":"72.8408"}],"driverDetails":{"firstName":"Jitendra K","lastName":"Kumar","driverId":"jitendra@techsoftlabs.com","profilePic":"/schoolapp/images/93b820c6-96aa-4c20-b934-195743b1c66b.png","emailId":"jitendra@techsoftlabs.com"},"routeId":"pool 5","schoolId":"129","name":"30012034","id":"129pool 5636530010617777024","routeType":"Dropoff","uniqueId":"636530010617777024"},{"stopage":[{"isSchool":false,"address":"Mini bazar Surat","latitude":"21.2112","stopId":"4","sequenceId":"4","longitude":"72.85"},{"isSchool":false,"address":"Hirabag, Surat","latitude":"21.214","stopId":"3","sequenceId":"3","longitude":"72.8627"},{"isSchool":false,"address":"Kapodra,Surat","latitude":"21.2188","stopId":"2","sequenceId":"2","longitude":"72.8746"},{"isSchool":true,"address":"Utran,Surat ","latitude":"21.2306","stopId":"1","sequenceId":"1","longitude":"72.8671"}],"driverDetails":{"firstName":"Piyush","lastName":"Poriya","driverId":"piyushporiya@techsoftlabs.com","profilePic":"/schoolapp/images/43b6206f-3715-42cf-b3de-27a6b75bd3c4.jpg","emailId":"piyushporiya@techsoftlabs.com"},"routeId":"12923","schoolId":"129","name":"Dropoff Darshit test route","id":"129129231","routeType":"dropoff","uniqueId":"1"},{"stopage":[{"isSchool":true,"address":"chop","latitude":"39.9489","stopId":"1","sequenceId":"1","longitude":"-75.194"},{"isSchool":false,"address":"low","latitude":"34.2542","stopId":"2","sequenceId":"2","longitude":"-110.03"},{"isSchool":false,"address":"chowpati","latitude":"21.1848","stopId":"3","sequenceId":"3","longitude":"72.8067"}],"driverDetails":{"firstName":"Piyush","lastName":"Poriya","driverId":"piyushporiya@techsoftlabs.com","profilePic":"/schoolapp/images/43b6206f-3715-42cf-b3de-27a6b75bd3c4.jpg","emailId":"piyushporiya@techsoftlabs.com"},"routeId":"123","schoolId":"129","name":"asdf","id":"1291231","routeType":"Pickup","uniqueId":"1"}]
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private String authtokenexpires;
    private ArrayList<ResultBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public ArrayList<ResultBean> getResult() {
        return result;
    }

    public void setResult(ArrayList<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean implements Parcelable {
        /**
         * stopage : [{"isSchool":true,"address":"udhana","latitude":"21.1431","stopId":"1","sequenceId":"1","longitude":"72.8431"},{"isSchool":false,"address":"GIDC, 137, Dr KV Hagrenad Road, Beside C.N.G. Pump, Alfa Industry, Unit Estate, Pandesara, Udhna, Surat, Gujarat 394221, India","latitude":"21.1448","stopId":"2","sequenceId":"2","longitude":"72.8372"},{"isSchool":false,"address":"Ashoka Shopping Centre, Plot No. 197, Udhana - Sachin Hwy, Near S B I, Pandesara GIDC, Udhna, Surat, Gujarat 394221, India","latitude":"21.1424","stopId":"3","sequenceId":"3","longitude":"72.8489"}]
         * driverDetails : {"firstName":"Piyush","lastName":"Poriya","driverId":"piyushporiya@techsoftlabs.com","profilePic":"/schoolapp/images/43b6206f-3715-42cf-b3de-27a6b75bd3c4.jpg","emailId":"piyushporiya@techsoftlabs.com"}
         * routeId : 13568
         * schoolId : 129
         * name : Udhana
         * id : 129135681
         * routeType : Dropoff
         * uniqueId : 1
         */

        private DriverDetailsBean driverDetails;
        private String routeId;
        private String schoolId;
        private String name;
        private String id;
        private String routeType;
        private String uniqueId;
        private List<StopageBean> stopage;

        public DriverDetailsBean getDriverDetails() {
            return driverDetails;
        }

        public void setDriverDetails(DriverDetailsBean driverDetails) {
            this.driverDetails = driverDetails;
        }

        public String getRouteId() {
            return routeId;
        }

        public void setRouteId(String routeId) {
            this.routeId = routeId;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getRouteType() {
            return routeType;
        }

        public void setRouteType(String routeType) {
            this.routeType = routeType;
        }

        public String getUniqueId() {
            return uniqueId;
        }

        public void setUniqueId(String uniqueId) {
            this.uniqueId = uniqueId;
        }

        public List<StopageBean> getStopage() {
            return stopage;
        }

        public void setStopage(List<StopageBean> stopage) {
            this.stopage = stopage;
        }

        public static class DriverDetailsBean implements Parcelable {
            /**
             * firstName : Piyush
             * lastName : Poriya
             * driverId : piyushporiya@techsoftlabs.com
             * profilePic : /schoolapp/images/43b6206f-3715-42cf-b3de-27a6b75bd3c4.jpg
             * emailId : piyushporiya@techsoftlabs.com
             */

            private String firstName;
            private String lastName;
            private String driverId;
            private String profilePic;
            private String emailId;

            public String getFirstName() {
                return firstName;
            }

            public void setFirstName(String firstName) {
                this.firstName = firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public void setLastName(String lastName) {
                this.lastName = lastName;
            }

            public String getDriverId() {
                return driverId;
            }

            public void setDriverId(String driverId) {
                this.driverId = driverId;
            }

            public String getProfilePic() {
                return profilePic;
            }

            public void setProfilePic(String profilePic) {
                this.profilePic = profilePic;
            }

            public String getEmailId() {
                return emailId;
            }

            public void setEmailId(String emailId) {
                this.emailId = emailId;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.firstName);
                dest.writeString(this.lastName);
                dest.writeString(this.driverId);
                dest.writeString(this.profilePic);
                dest.writeString(this.emailId);
            }

            public DriverDetailsBean() {
            }

            protected DriverDetailsBean(Parcel in) {
                this.firstName = in.readString();
                this.lastName = in.readString();
                this.driverId = in.readString();
                this.profilePic = in.readString();
                this.emailId = in.readString();
            }

            public static final Creator<DriverDetailsBean> CREATOR = new Creator<DriverDetailsBean>() {
                @Override
                public DriverDetailsBean createFromParcel(Parcel source) {
                    return new DriverDetailsBean(source);
                }

                @Override
                public DriverDetailsBean[] newArray(int size) {
                    return new DriverDetailsBean[size];
                }
            };
        }

        public static class StopageBean implements Parcelable {
            /**
             * isSchool : true
             * address : udhana
             * latitude : 21.1431
             * stopId : 1
             * sequenceId : 1
             * longitude : 72.8431
             */

            private boolean isSchool;
            private String address;
            private String latitude;
            private String stopId;
            private String sequenceId;
            private String longitude;

            public boolean isIsSchool() {
                return isSchool;
            }

            public void setIsSchool(boolean isSchool) {
                this.isSchool = isSchool;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getStopId() {
                return stopId;
            }

            public void setStopId(String stopId) {
                this.stopId = stopId;
            }

            public String getSequenceId() {
                return sequenceId;
            }

            public void setSequenceId(String sequenceId) {
                this.sequenceId = sequenceId;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeByte(this.isSchool ? (byte) 1 : (byte) 0);
                dest.writeString(this.address);
                dest.writeString(this.latitude);
                dest.writeString(this.stopId);
                dest.writeString(this.sequenceId);
                dest.writeString(this.longitude);
            }

            public StopageBean() {
            }

            protected StopageBean(Parcel in) {
                this.isSchool = in.readByte() != 0;
                this.address = in.readString();
                this.latitude = in.readString();
                this.stopId = in.readString();
                this.sequenceId = in.readString();
                this.longitude = in.readString();
            }

            public static final Creator<StopageBean> CREATOR = new Creator<StopageBean>() {
                @Override
                public StopageBean createFromParcel(Parcel source) {
                    return new StopageBean(source);
                }

                @Override
                public StopageBean[] newArray(int size) {
                    return new StopageBean[size];
                }
            };
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.driverDetails, flags);
            dest.writeString(this.routeId);
            dest.writeString(this.schoolId);
            dest.writeString(this.name);
            dest.writeString(this.id);
            dest.writeString(this.routeType);
            dest.writeString(this.uniqueId);
            dest.writeList(this.stopage);
        }

        public ResultBean() {
        }

        protected ResultBean(Parcel in) {
            this.driverDetails = in.readParcelable(DriverDetailsBean.class.getClassLoader());
            this.routeId = in.readString();
            this.schoolId = in.readString();
            this.name = in.readString();
            this.id = in.readString();
            this.routeType = in.readString();
            this.uniqueId = in.readString();
            this.stopage = new ArrayList<StopageBean>();
            in.readList(this.stopage, StopageBean.class.getClassLoader());
        }

        public static final Parcelable.Creator<ResultBean> CREATOR = new Parcelable.Creator<ResultBean>() {
            @Override
            public ResultBean createFromParcel(Parcel source) {
                return new ResultBean(source);
            }

            @Override
            public ResultBean[] newArray(int size) {
                return new ResultBean[size];
            }
        };
    }
}
