/*
 * Copyright (c) 2017.                                                       
 *                                                                           
 * ***********************************************************************  
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                           
 * ________________________________________________________________________  
 *                                                                           
 *    [2010] - [2018] SripathiSolutions Corp.                                
 *    [2010] - [2018] DataReady Technology Corp.                             
 *   ________________________________________________________________________
 *                                                                           
 *    All Rights Reserved.                                                   
 *                                                                           
 *   NOTICE:  All information contained herein is, and remains               
 *   the property of DataReady Technology  and its suppliers,                
 *   if any.  The intellectual and technical concepts contained              
 *   herein are proprietary to DataReady Technology Incorporated             
 *   and its suppliers and may be covered by U.S. and Foreign Patents,       
 *   patents in process, and are protected by trade secret or copyright law. 
 *   Dissemination of this information or reproduction of this material      
 *   is strictly forbidden unless prior written permission is obtained       
 *   from DataReady Technology Incorporated.                                 
 *                                                                           
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                            
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;


import java.util.List;

public class GetStudentProfileResponse {


    /**
     * message : Student informations are below
     * status : true
     * authtoken : null
     * result : {"lastName":"new","address":"indecisive ","gender":"male","academicInfo":false,"profilePic":"/schoolapp/images/24844e42-755f-4550-bfcd-f2d80114d2f9.png","geopoint":"35.7866,139.712","parentDetails":{"fatherFirstName":null,"fatherLastName":null,"fatherOccupation":null,"motherFirstName":null,"motherLastName":null,"motherEmailId":null,"fatherPhoneNo":null,"motherPhoneNo":null,"motherProfilePic":null,"fatherEmailId":null,"motherOccupation":null,"fatherProfilePic":null},"guardianDetails":{"guardianLastName":null,"guardianFirstName":null,"education":null,"occupation":null,"address":null,"dob":null,"emailId":null,"phoneNo":null,"relation":null},"pastSchoolDetails":{"gradeId":null,"classId":null,"address":null,"yearOfPassing":null,"schoolName":null},"religion":null,"studentId":"1295","firstName":"D test","birthPlace":"Surat","nationality":"","dob":"2018-03-08","schoolId":"129","id":"1291295","dateofJoining":"2018-03-08","motherTounge":null}
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private ResultBean result;
    private String authtokenexpires;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public static class ResultBean {
        /**
         * lastName : new
         * address : indecisive 
         * gender : male
         * academicInfo : false
         * profilePic : /schoolapp/images/24844e42-755f-4550-bfcd-f2d80114d2f9.png
         * geopoint : 35.7866,139.712
         * parentDetails : {"fatherFirstName":null,"fatherLastName":null,"fatherOccupation":null,"motherFirstName":null,"motherLastName":null,"motherEmailId":null,"fatherPhoneNo":null,"motherPhoneNo":null,"motherProfilePic":null,"fatherEmailId":null,"motherOccupation":null,"fatherProfilePic":null}
         * guardianDetails : {"guardianLastName":null,"guardianFirstName":null,"education":null,"occupation":null,"address":null,"dob":null,"emailId":null,"phoneNo":null,"relation":null}
         * pastSchoolDetails : {"gradeId":null,"classId":null,"address":null,"yearOfPassing":null,"schoolName":null}
         * religion : null
         * studentId : 1295
         * firstName : D test
         * birthPlace : Surat
         * nationality : 
         * dob : 2018-03-08
         * schoolId : 129
         * id : 1291295
         * dateofJoining : 2018-03-08
         * motherTounge : null
         */

        private String lastName;
        private String address;
        private String gender;
        private boolean academicInfo;
        private String profilePic;
        private String geopoint;
        private ParentDetailsBean parentDetails;
        private GuardianDetailsBean guardianDetails;
        private PastSchoolDetailsBean pastSchoolDetails;
        private PhysicianDetailsBean physicianDetails;
        private NotesBean notes;
        private String religion;
        private String studentId;
        private String firstName;
        private String birthPlace;
        private String nationality;
        private String dob;
        private String schoolId;
        private String id;
        private String dateofJoining;
        private String motherTounge;
        private String requiresTransportation;
        private List<FilesBean> files;

        public List<FilesBean> getFiles() {
            return files;
        }

        public void setFiles(List<FilesBean> files) {
            this.files = files;
        }

        public NotesBean getNotes() {
            return notes;
        }

        public void setNotes(NotesBean notes) {
            this.notes = notes;
        }

        public PhysicianDetailsBean getPhysicianDetails() {
            return physicianDetails;
        }

        public void setPhysicianDetails(PhysicianDetailsBean physicianDetails) {
            this.physicianDetails = physicianDetails;
        }

        public String getRequiresTransportation() {
            return requiresTransportation;
        }

        public void setRequiresTransportation(String requiresTransportation) {
            this.requiresTransportation = requiresTransportation;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public boolean isAcademicInfo() {
            return academicInfo;
        }

        public void setAcademicInfo(boolean academicInfo) {
            this.academicInfo = academicInfo;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        public String getGeopoint() {
            return geopoint;
        }

        public void setGeopoint(String geopoint) {
            this.geopoint = geopoint;
        }

        public ParentDetailsBean getParentDetails() {
            return parentDetails;
        }

        public void setParentDetails(ParentDetailsBean parentDetails) {
            this.parentDetails = parentDetails;
        }

        public GuardianDetailsBean getGuardianDetails() {
            return guardianDetails;
        }

        public void setGuardianDetails(GuardianDetailsBean guardianDetails) {
            this.guardianDetails = guardianDetails;
        }

        public PastSchoolDetailsBean getPastSchoolDetails() {
            return pastSchoolDetails;
        }

        public void setPastSchoolDetails(PastSchoolDetailsBean pastSchoolDetails) {
            this.pastSchoolDetails = pastSchoolDetails;
        }

        public String getReligion() {
            return religion;
        }

        public void setReligion(String religion) {
            this.religion = religion;
        }

        public String getStudentId() {
            return studentId;
        }

        public void setStudentId(String studentId) {
            this.studentId = studentId;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getBirthPlace() {
            return birthPlace;
        }

        public void setBirthPlace(String birthPlace) {
            this.birthPlace = birthPlace;
        }

        public String getNationality() {
            return nationality;
        }

        public void setNationality(String nationality) {
            this.nationality = nationality;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDateofJoining() {
            return dateofJoining;
        }

        public void setDateofJoining(String dateofJoining) {
            this.dateofJoining = dateofJoining;
        }

        public String getMotherTounge() {
            return motherTounge;
        }

        public void setMotherTounge(String motherTounge) {
            this.motherTounge = motherTounge;
        }

        public class FilesBean  {
            /**
             * fileName : Apple vs Android.pdf
             * filePath : /schoolapp/images/45e75072-cc59-428c-9feb-b584d10f3e6b.pdf
             */

            private String fileName;
            private String filePath;

            public String getFileName() {
                return fileName;
            }

            public void setFileName(String fileName) {
                this.fileName = fileName;
            }

            public String getFilePath() {
                return filePath;
            }

            public void setFilePath(String filePath) {
                this.filePath = filePath;
            }


        }

        public static class ParentDetailsBean {
            /**
             * fatherFirstName : null
             * fatherLastName : null
             * fatherOccupation : null
             * motherFirstName : null
             * motherLastName : null
             * motherEmailId : null
             * fatherPhoneNo : null
             * motherPhoneNo : null
             * motherProfilePic : null
             * fatherEmailId : null
             * motherOccupation : null
             * fatherProfilePic : null
             */

            private String fatherFirstName;
            private String fatherLastName;
            private String fatherOccupation;
            private String motherFirstName;
            private String motherLastName;
            private String motherEmailId;
            private String fatherPhoneNo;
            private String motherPhoneNo;
            private String motherProfilePic;
            private String fatherEmailId;
            private String motherOccupation;
            private String fatherProfilePic;
            private List<FilesBean> files;

            public List<FilesBean> getFiles() {
                return files;
            }

            public void setFiles(List<FilesBean> files) {
                this.files = files;
            }

            public String getFatherFirstName() {
                return fatherFirstName;
            }

            public void setFatherFirstName(String fatherFirstName) {
                this.fatherFirstName = fatherFirstName;
            }

            public String getFatherLastName() {
                return fatherLastName;
            }

            public void setFatherLastName(String fatherLastName) {
                this.fatherLastName = fatherLastName;
            }

            public String getFatherOccupation() {
                return fatherOccupation;
            }

            public void setFatherOccupation(String fatherOccupation) {
                this.fatherOccupation = fatherOccupation;
            }

            public String getMotherFirstName() {
                return motherFirstName;
            }

            public void setMotherFirstName(String motherFirstName) {
                this.motherFirstName = motherFirstName;
            }

            public String getMotherLastName() {
                return motherLastName;
            }

            public void setMotherLastName(String motherLastName) {
                this.motherLastName = motherLastName;
            }

            public String getMotherEmailId() {
                return motherEmailId;
            }

            public void setMotherEmailId(String motherEmailId) {
                this.motherEmailId = motherEmailId;
            }

            public String getFatherPhoneNo() {
                return fatherPhoneNo;
            }

            public void setFatherPhoneNo(String fatherPhoneNo) {
                this.fatherPhoneNo = fatherPhoneNo;
            }

            public String getMotherPhoneNo() {
                return motherPhoneNo;
            }

            public void setMotherPhoneNo(String motherPhoneNo) {
                this.motherPhoneNo = motherPhoneNo;
            }

            public String getMotherProfilePic() {
                return motherProfilePic;
            }

            public void setMotherProfilePic(String motherProfilePic) {
                this.motherProfilePic = motherProfilePic;
            }

            public String getFatherEmailId() {
                return fatherEmailId;
            }

            public void setFatherEmailId(String fatherEmailId) {
                this.fatherEmailId = fatherEmailId;
            }

            public String getMotherOccupation() {
                return motherOccupation;
            }

            public void setMotherOccupation(String motherOccupation) {
                this.motherOccupation = motherOccupation;
            }

            public String getFatherProfilePic() {
                return fatherProfilePic;
            }

            public void setFatherProfilePic(String fatherProfilePic) {
                this.fatherProfilePic = fatherProfilePic;
            }

            public class FilesBean  {
                /**
                 * fileName : Apple vs Android.pdf
                 * filePath : /schoolapp/images/45e75072-cc59-428c-9feb-b584d10f3e6b.pdf
                 */

                private String fileName;
                private String filePath;

                public String getFileName() {
                    return fileName;
                }

                public void setFileName(String fileName) {
                    this.fileName = fileName;
                }

                public String getFilePath() {
                    return filePath;
                }

                public void setFilePath(String filePath) {
                    this.filePath = filePath;
                }


            }
        }

        public static class GuardianDetailsBean {
            /**
             * guardianLastName : null
             * guardianFirstName : null
             * education : null
             * occupation : null
             * address : null
             * dob : null
             * emailId : null
             * phoneNo : null
             * relation : null
             */

            private String guardianLastName;
            private String guardianFirstName;
            private String education;
            private String occupation;
            private String address;
            private String dob;
            private String emailId;
            private String phoneNo;
            private String relation;
            private List<FilesBean> files;

            public List<FilesBean> getFiles() {
                return files;
            }

            public void setFiles(List<FilesBean> files) {
                this.files = files;
            }

            public String getGuardianLastName() {
                return guardianLastName;
            }

            public void setGuardianLastName(String guardianLastName) {
                this.guardianLastName = guardianLastName;
            }

            public String getGuardianFirstName() {
                return guardianFirstName;
            }

            public void setGuardianFirstName(String guardianFirstName) {
                this.guardianFirstName = guardianFirstName;
            }

            public String getEducation() {
                return education;
            }

            public void setEducation(String education) {
                this.education = education;
            }

            public String getOccupation() {
                return occupation;
            }

            public void setOccupation(String occupation) {
                this.occupation = occupation;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getDob() {
                return dob;
            }

            public void setDob(String dob) {
                this.dob = dob;
            }

            public String getEmailId() {
                return emailId;
            }

            public void setEmailId(String emailId) {
                this.emailId = emailId;
            }

            public String getPhoneNo() {
                return phoneNo;
            }

            public void setPhoneNo(String phoneNo) {
                this.phoneNo = phoneNo;
            }

            public String getRelation() {
                return relation;
            }

            public void setRelation(String relation) {
                this.relation = relation;
            }

            public class FilesBean  {
                /**
                 * fileName : Apple vs Android.pdf
                 * filePath : /schoolapp/images/45e75072-cc59-428c-9feb-b584d10f3e6b.pdf
                 */

                private String fileName;
                private String filePath;

                public String getFileName() {
                    return fileName;
                }

                public void setFileName(String fileName) {
                    this.fileName = fileName;
                }

                public String getFilePath() {
                    return filePath;
                }

                public void setFilePath(String filePath) {
                    this.filePath = filePath;
                }


            }
        }

        public static class PastSchoolDetailsBean {
            /**
             * gradeId : null
             * classId : null
             * address : null
             * yearOfPassing : null
             * schoolName : null
             */

            private String gradeId;
            private String classId;
            private String address;
            private String yearOfPassing;
            private String schoolName;
            private List<FilesBean> files;

            public List<FilesBean> getFiles() {
                return files;
            }

            public void setFiles(List<FilesBean> files) {
                this.files = files;
            }

            public String getGradeId() {
                return gradeId;
            }

            public void setGradeId(String gradeId) {
                this.gradeId = gradeId;
            }

            public String getClassId() {
                return classId;
            }

            public void setClassId(String classId) {
                this.classId = classId;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getYearOfPassing() {
                return yearOfPassing;
            }

            public void setYearOfPassing(String yearOfPassing) {
                this.yearOfPassing = yearOfPassing;
            }

            public String getSchoolName() {
                return schoolName;
            }

            public void setSchoolName(String schoolName) {
                this.schoolName = schoolName;
            }

            public class FilesBean  {
                /**
                 * fileName : Apple vs Android.pdf
                 * filePath : /schoolapp/images/45e75072-cc59-428c-9feb-b584d10f3e6b.pdf
                 */

                private String fileName;
                private String filePath;

                public String getFileName() {
                    return fileName;
                }

                public void setFileName(String fileName) {
                    this.fileName = fileName;
                }

                public String getFilePath() {
                    return filePath;
                }

                public void setFilePath(String filePath) {
                    this.filePath = filePath;
                }


            }
        }

        public static class PhysicianDetailsBean {
            /**
             * address : string
             * city : string
             * country : string
             * email : string
             * fax : string
             * firstName : string
             * lastName : string
             * phone : string
             * state : string
             */

            private String address;
            private String city;
            private String country;
            private String email;
            private String fax;
            private String firstName;
            private String lastName;
            private String phone;
            private String state;

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getFax() {
                return fax;
            }

            public void setFax(String fax) {
                this.fax = fax;
            }

            public String getFirstName() {
                return firstName;
            }

            public void setFirstName(String firstName) {
                this.firstName = firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public void setLastName(String lastName) {
                this.lastName = lastName;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }
        }

        public static class NotesBean {
            /**
             * schoolCounselor : string
             * transportationCounselor : string
             */

            private String schoolCounselor;
            private String transportationCounselor;

            public String getSchoolCounselor() {
                return schoolCounselor;
            }

            public void setSchoolCounselor(String schoolCounselor) {
                this.schoolCounselor = schoolCounselor;
            }

            public String getTransportationCounselor() {
                return transportationCounselor;
            }

            public void setTransportationCounselor(String transportationCounselor) {
                this.transportationCounselor = transportationCounselor;
            }
        }

    }
}
