/*
 * Copyright (c) 2017.                                                       
 *                                                                           
 * ***********************************************************************  
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                           
 * ________________________________________________________________________  
 *                                                                           
 *    [2010] - [2018] SripathiSolutions Corp.                                
 *    [2010] - [2018] DataReady Technology Corp.                             
 *   ________________________________________________________________________
 *                                                                           
 *    All Rights Reserved.                                                   
 *                                                                           
 *   NOTICE:  All information contained herein is, and remains               
 *   the property of DataReady Technology  and its suppliers,                
 *   if any.  The intellectual and technical concepts contained              
 *   herein are proprietary to DataReady Technology Incorporated             
 *   and its suppliers and may be covered by U.S. and Foreign Patents,       
 *   patents in process, and are protected by trade secret or copyright law. 
 *   Dissemination of this information or reproduction of this material      
 *   is strictly forbidden unless prior written permission is obtained       
 *   from DataReady Technology Incorporated.                                 
 *                                                                           
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                            
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class TripInfoResponse {

    /**
     * message : Details are below
     * status : true
     * authtoken : null
     * result : {"endTripTime":"","stopage":[{"address":"Sector 37","latitude":"28.5670","stopId":"1","sequenceId":"1","longitude":"77.3418"},{"address":"Sector 49","latitude":"28.5622","stopId":"2","sequenceId":"2","longitude":"77.3741"},{"address":"Sector 82","latitude":"28.5294","stopId":"3","sequenceId":"3","longitude":"77.3913"}],"routeId":"11","driverId":"pankajcpmishra@gmail.com","startTripTime":"2017-07-05 11:32:00 UTC","currentPosition":{"currentTime":"2017-07-05 01:52:50 UTC","routeId":"11","driverId":"pankajcpmishra@gmail.com","latitude":"28.5670","name":"11","currentDate":"2017-07-05","stopId":"","serverTime":"2017-07-05 11:50:09 UTC","uniqueId":"694094","speed":"","direction":"","longitude":"77.3418"},"name":"SP","uniqueId":"694094","onboardCount":"0","status":"active"}
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private ResultBean result;
    private String authtokenexpires;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public static class ResultBean implements Parcelable {
        /**
         * endTripTime :
         * stopage : [{"address":"Sector 37","latitude":"28.5670","stopId":"1","sequenceId":"1","longitude":"77.3418"},{"address":"Sector 49","latitude":"28.5622","stopId":"2","sequenceId":"2","longitude":"77.3741"},{"address":"Sector 82","latitude":"28.5294","stopId":"3","sequenceId":"3","longitude":"77.3913"}]
         * routeId : 11
         * driverId : pankajcpmishra@gmail.com
         * startTripTime : 2017-07-05 11:32:00 UTC
         * currentPosition : {"currentTime":"2017-07-05 01:52:50 UTC","routeId":"11","driverId":"pankajcpmishra@gmail.com","latitude":"28.5670","name":"11","currentDate":"2017-07-05","stopId":"","serverTime":"2017-07-05 11:50:09 UTC","uniqueId":"694094","speed":"","direction":"","longitude":"77.3418"}
         * name : SP
         * uniqueId : 694094
         * onboardCount : 0
         * status : active
         */

        private String endTripTime;
        private String routeId;
        private String driverId;
        private String startTripTime;
        private CurrentPositionBean currentPosition;
        private String name;
        private String uniqueId;
        private String onboardCount;
        private String status;
        private String tripId;
        private String tripType;
        private ArrayList<StopageBean> stopage;

        public String getTripType() {
            return tripType;
        }

        public void setTripType(String tripType) {
            this.tripType = tripType;
        }

        public static Creator<ResultBean> getCREATOR() {
            return CREATOR;
        }

        public String getTripId() {
            return tripId;
        }

        public void setTripId(String tripId) {
            this.tripId = tripId;
        }

        public String getEndTripTime() {
            return endTripTime;
        }

        public void setEndTripTime(String endTripTime) {
            this.endTripTime = endTripTime;
        }

        public String getRouteId() {
            return routeId;
        }

        public void setRouteId(String routeId) {
            this.routeId = routeId;
        }

        public String getDriverId() {
            return driverId;
        }

        public void setDriverId(String driverId) {
            this.driverId = driverId;
        }

        public String getStartTripTime() {
            return startTripTime;
        }

        public void setStartTripTime(String startTripTime) {
            this.startTripTime = startTripTime;
        }

        public CurrentPositionBean getCurrentPosition() {
            return currentPosition;
        }

        public void setCurrentPosition(CurrentPositionBean currentPosition) {
            this.currentPosition = currentPosition;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUniqueId() {
            return uniqueId;
        }

        public void setUniqueId(String uniqueId) {
            this.uniqueId = uniqueId;
        }

        public String getOnboardCount() {
            return onboardCount;
        }

        public void setOnboardCount(String onboardCount) {
            this.onboardCount = onboardCount;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public ArrayList<StopageBean> getStopage() {
            return stopage;
        }

        public void setStopage(ArrayList<StopageBean> stopage) {
            this.stopage = stopage;
        }

        public static class CurrentPositionBean implements Parcelable {
            /**
             * currentTime : 2017-07-05 01:52:50 UTC
             * routeId : 11
             * driverId : pankajcpmishra@gmail.com
             * latitude : 28.5670
             * name : 11
             * currentDate : 2017-07-05
             * stopId :
             * serverTime : 2017-07-05 11:50:09 UTC
             * uniqueId : 694094
             * speed :
             * direction :
             * longitude : 77.3418
             */

            private String currentTime;
            private String routeId;
            private String driverId;
            private String latitude;
            private String name;
            private String currentDate;
            private String stopId;
            private String serverTime;
            private String uniqueId;
            private String speed;
            private String direction;
            private String longitude;

            public String getCurrentTime() {
                return currentTime;
            }

            public void setCurrentTime(String currentTime) {
                this.currentTime = currentTime;
            }

            public String getRouteId() {
                return routeId;
            }

            public void setRouteId(String routeId) {
                this.routeId = routeId;
            }

            public String getDriverId() {
                return driverId;
            }

            public void setDriverId(String driverId) {
                this.driverId = driverId;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getCurrentDate() {
                return currentDate;
            }

            public void setCurrentDate(String currentDate) {
                this.currentDate = currentDate;
            }

            public String getStopId() {
                return stopId;
            }

            public void setStopId(String stopId) {
                this.stopId = stopId;
            }

            public String getServerTime() {
                return serverTime;
            }

            public void setServerTime(String serverTime) {
                this.serverTime = serverTime;
            }

            public String getUniqueId() {
                return uniqueId;
            }

            public void setUniqueId(String uniqueId) {
                this.uniqueId = uniqueId;
            }

            public String getSpeed() {
                return speed;
            }

            public void setSpeed(String speed) {
                this.speed = speed;
            }

            public String getDirection() {
                return direction;
            }

            public void setDirection(String direction) {
                this.direction = direction;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.currentTime);
                dest.writeString(this.routeId);
                dest.writeString(this.driverId);
                dest.writeString(this.latitude);
                dest.writeString(this.name);
                dest.writeString(this.currentDate);
                dest.writeString(this.stopId);
                dest.writeString(this.serverTime);
                dest.writeString(this.uniqueId);
                dest.writeString(this.speed);
                dest.writeString(this.direction);
                dest.writeString(this.longitude);
            }

            public CurrentPositionBean() {
            }

            protected CurrentPositionBean(Parcel in) {
                this.currentTime = in.readString();
                this.routeId = in.readString();
                this.driverId = in.readString();
                this.latitude = in.readString();
                this.name = in.readString();
                this.currentDate = in.readString();
                this.stopId = in.readString();
                this.serverTime = in.readString();
                this.uniqueId = in.readString();
                this.speed = in.readString();
                this.direction = in.readString();
                this.longitude = in.readString();
            }

            public static final Parcelable.Creator<CurrentPositionBean> CREATOR = new Parcelable.Creator<CurrentPositionBean>() {
                @Override
                public CurrentPositionBean createFromParcel(Parcel source) {
                    return new CurrentPositionBean(source);
                }

                @Override
                public CurrentPositionBean[] newArray(int size) {
                    return new CurrentPositionBean[size];
                }
            };
        }

        public static class StopageBean implements Parcelable {
            /**
             * address : Sector 37
             * latitude : 28.5670
             * stopId : 1
             * sequenceId : 1
             * longitude : 77.3418
             */

            private String address;
            private String latitude;
            private String stopId;
            boolean isSchool;
            private String sequenceId;
            private String longitude;

            public boolean isSchool() {
                return isSchool;
            }

            public void setSchool(boolean school) {
                isSchool = school;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getStopId() {
                return stopId;
            }

            public void setStopId(String stopId) {
                this.stopId = stopId;
            }

            public String getSequenceId() {
                return sequenceId;
            }

            public void setSequenceId(String sequenceId) {
                this.sequenceId = sequenceId;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.address);
                dest.writeString(this.latitude);
                dest.writeString(this.stopId);

                dest.writeString(this.sequenceId);
                dest.writeString(this.longitude);
            }

            public StopageBean() {
            }

            protected StopageBean(Parcel in) {
                this.address = in.readString();
                this.latitude = in.readString();
                this.stopId = in.readString();
                this.sequenceId = in.readString();
                this.longitude = in.readString();
            }

            public static final Creator<StopageBean> CREATOR = new Creator<StopageBean>() {
                @Override
                public StopageBean createFromParcel(Parcel source) {
                    return new StopageBean(source);
                }

                @Override
                public StopageBean[] newArray(int size) {
                    return new StopageBean[size];
                }
            };
        }

        public ResultBean() {
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.endTripTime);
            dest.writeString(this.routeId);
            dest.writeString(this.driverId);
            dest.writeString(this.startTripTime);
            dest.writeParcelable(this.currentPosition, flags);
            dest.writeString(this.name);
            dest.writeString(this.uniqueId);
            dest.writeString(this.onboardCount);
            dest.writeString(this.status);
            dest.writeString(this.tripId);
            dest.writeString(this.tripType);
            dest.writeTypedList(this.stopage);
        }

        protected ResultBean(Parcel in) {
            this.endTripTime = in.readString();
            this.routeId = in.readString();
            this.driverId = in.readString();
            this.startTripTime = in.readString();
            this.currentPosition = in.readParcelable(CurrentPositionBean.class.getClassLoader());
            this.name = in.readString();
            this.uniqueId = in.readString();
            this.onboardCount = in.readString();
            this.status = in.readString();
            this.tripId = in.readString();
            this.tripType = in.readString();
            this.stopage = in.createTypedArrayList(StopageBean.CREATOR);
        }

        public static final Creator<ResultBean> CREATOR = new Creator<ResultBean>() {
            @Override
            public ResultBean createFromParcel(Parcel source) {
                return new ResultBean(source);
            }

            @Override
            public ResultBean[] newArray(int size) {
                return new ResultBean[size];
            }
        };
    }

}



