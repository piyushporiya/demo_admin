package ca.dataready.SmartSchoolAdmin.server;

public class TimeTableModel {

    private String timePeriod;
    private SlotBean slotBean;

    public String getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(String timePeriod) {
        this.timePeriod = timePeriod;
    }

    public SlotBean getSlotBean() {
        return slotBean;
    }

    public void setSlotBean(SlotBean slotBean) {
        this.slotBean = slotBean;
    }

    public static class SlotBean{

        private String slotOnMon;
        private String slotOnTue;
        private String slotOnWed;
        private String slotOnThu;
        private String slotOnFri;
        private String slotOnSat;

        public String getSlotOnMon() {
            return slotOnMon;
        }

        public void setSlotOnMon(String slotOnMon) {
            this.slotOnMon = slotOnMon;
        }

        public String getSlotOnTue() {
            return slotOnTue;
        }

        public void setSlotOnTue(String slotOnTue) {
            this.slotOnTue = slotOnTue;
        }

        public String getSlotOnWed() {
            return slotOnWed;
        }

        public void setSlotOnWed(String slotOnWed) {
            this.slotOnWed = slotOnWed;
        }

        public String getSlotOnThu() {
            return slotOnThu;
        }

        public void setSlotOnThu(String slotOnThu) {
            this.slotOnThu = slotOnThu;
        }

        public String getSlotOnFri() {
            return slotOnFri;
        }

        public void setSlotOnFri(String slotOnFri) {
            this.slotOnFri = slotOnFri;
        }

        public String getSlotOnSat() {
            return slotOnSat;
        }

        public void setSlotOnSat(String slotOnSat) {
            this.slotOnSat = slotOnSat;
        }
    }

}
