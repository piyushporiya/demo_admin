/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class GetStudentsList implements Parcelable {


    /**
     * message : Student informations are below
     * status : true
     * authtoken : null
     * result : [{"lastName":"okiiiiiiiii","address":"India","gender":"male","academicInfo":false,"profilePic":"/schoolapp/images/a91abbde-d06f-49ef-a814-734757dc63e0.png","geopoint":"20.5937,78.9629","parentDetails":{},"guardianDetails":{},"pastSchoolDetails":{},"religion":null,"studentId":"1278","firstName":"D testrrrrr","birthPlace":null,"nationality":null,"dob":"2018-02-08","schoolId":"129","id":"1291278","dateofJoining":null,"motherTounge":null},{"lastName":"yadava","address":"102 sec noida ","gender":"male","academicInfo":false,"profilePic":"/schoolapp/images/5335667b-c436-4cde-8e36-7b58997bd081.png","geopoint":"40.6064,-74.0595","parentDetails":{"fatherFirstName":null,"fatherLastName":null,"fatherOccupation":null,"motherFirstName":null,"motherLastName":null,"motherEmailId":null,"fatherPhoneNo":null,"motherPhoneNo":null,"motherProfilePic":null,"fatherEmailId":null,"motherOccupation":null,"fatherProfilePic":null},"guardianDetails":{"guardianLastName":null,"guardianFirstName":null,"education":null,"occupation":null,"address":null,"dob":null,"emailId":null,"phoneNo":null,"relation":null},"pastSchoolDetails":{"gradeId":null,"classId":null,"address":null,"yearOfPassing":null,"schoolName":null},"religion":null,"studentId":"1288","firstName":"Jitendra jitendra","birthPlace":null,"nationality":null,"dob":"2018-02-09","schoolId":"129","id":"1291288","dateofJoining":"","motherTounge":null},{"lastName":"new","address":"indecisive ","gender":"male","academicInfo":false,"profilePic":"/schoolapp/images/24844e42-755f-4550-bfcd-f2d80114d2f9.png","geopoint":"35.7866,139.712","parentDetails":{"fatherFirstName":null,"fatherLastName":null,"fatherOccupation":null,"motherFirstName":null,"motherLastName":null,"motherEmailId":null,"fatherPhoneNo":null,"motherPhoneNo":null,"motherProfilePic":null,"fatherEmailId":null,"motherOccupation":null,"fatherProfilePic":null},"guardianDetails":{"guardianLastName":null,"guardianFirstName":null,"education":null,"occupation":null,"address":null,"dob":null,"emailId":null,"phoneNo":null,"relation":null},"pastSchoolDetails":{"gradeId":null,"classId":null,"address":null,"yearOfPassing":null,"schoolName":null},"religion":null,"studentId":"1295","firstName":"D test","birthPlace":"Surat","nationality":"","dob":"2018-03-08","schoolId":"129","id":"1291295","dateofJoining":"2018-03-08","motherTounge":null},{"lastName":"yadav2","address":"102 sec noida ","gender":"male","academicInfo":false,"profilePic":"/schoolapp/images/d26cb59d-ae09-4b96-a33f-cfb3f96fa220.png","geopoint":"40.6064,-74.0595","parentDetails":{"fatherFirstName":null,"fatherLastName":null,"fatherOccupation":null,"motherFirstName":null,"motherLastName":null,"motherEmailId":null,"fatherPhoneNo":null,"motherPhoneNo":null,"motherProfilePic":null,"fatherEmailId":null,"motherOccupation":null,"fatherProfilePic":null},"guardianDetails":{"guardianLastName":null,"guardianFirstName":null,"education":null,"occupation":null,"address":null,"dob":null,"emailId":null,"phoneNo":null,"relation":null},"pastSchoolDetails":{"gradeId":null,"classId":null,"address":null,"yearOfPassing":null,"schoolName":null},"religion":null,"studentId":"1289","firstName":"Jitendra jitendra","birthPlace":null,"nationality":null,"dob":"2018-02-09","schoolId":"129","id":"1291289","dateofJoining":"","motherTounge":null},{"lastName":"s","address":"hanuman mandir sec 49 noida up","gender":"male","academicInfo":false,"profilePic":"/schoolapp/images/b24da5d1-199a-4bc4-b4db-40c8a7090cef.png","geopoint":"28.5575,77.3784","parentDetails":{"fatherFirstName":null,"fatherLastName":null,"fatherOccupation":null,"motherFirstName":null,"motherLastName":null,"motherEmailId":null,"fatherPhoneNo":null,"motherPhoneNo":null,"motherProfilePic":null,"fatherEmailId":null,"motherOccupation":null,"fatherProfilePic":null},"guardianDetails":{"guardianLastName":null,"guardianFirstName":null,"education":null,"occupation":null,"address":null,"dob":null,"emailId":null,"phoneNo":null,"relation":null},"pastSchoolDetails":{"gradeId":null,"classId":null,"address":null,"yearOfPassing":null,"schoolName":null},"religion":null,"studentId":"1285","firstName":"umesh ","birthPlace":null,"nationality":null,"dob":"1999-02-09","schoolId":"129","id":"1291285","dateofJoining":null,"motherTounge":null},{"lastName":"gah","address":"Surat ","gender":"male","academicInfo":false,"profilePic":"/schoolapp/images/5051b1be-f527-428d-bca3-8e4c0f0845a7.png","geopoint":"21.1702,72.8311","parentDetails":{"fatherFirstName":null,"fatherLastName":null,"fatherOccupation":null,"motherFirstName":null,"motherLastName":null,"motherEmailId":null,"fatherPhoneNo":null,"motherPhoneNo":null,"motherProfilePic":null,"fatherEmailId":null,"motherOccupation":null,"fatherProfilePic":null},"guardianDetails":{"guardianLastName":null,"guardianFirstName":null,"education":null,"occupation":null,"address":null,"dob":null,"emailId":null,"phoneNo":null,"relation":null},"pastSchoolDetails":{"gradeId":null,"classId":null,"address":null,"yearOfPassing":null,"schoolName":null},"religion":null,"studentId":"1292","firstName":"GUI","birthPlace":"","nationality":null,"dob":"2018-02-09","schoolId":"129","id":"1291292","dateofJoining":null,"motherTounge":null},{"lastName":"sassy","address":"Surat","gender":"male","academicInfo":false,"profilePic":"/schoolapp/images/fe059367-ca1e-49fe-9fe2-0e6567c5a0b8.png","geopoint":"21.1702,72.8311","parentDetails":{"fatherFirstName":null,"fatherLastName":null,"fatherOccupation":null,"motherFirstName":null,"motherLastName":null,"motherEmailId":null,"fatherPhoneNo":null,"motherPhoneNo":null,"motherProfilePic":null,"fatherEmailId":null,"motherOccupation":null,"fatherProfilePic":null},"guardianDetails":{"guardianLastName":null,"guardianFirstName":null,"education":null,"occupation":null,"address":null,"dob":null,"emailId":null,"phoneNo":null,"relation":null},"pastSchoolDetails":{"gradeId":null,"classId":null,"address":null,"yearOfPassing":null,"schoolName":null},"religion":null,"studentId":"1296","firstName":"assa","birthPlace":"","nationality":null,"dob":"2018-03-20","schoolId":"129","id":"1291296","dateofJoining":"2018-03-20","motherTounge":null},{"lastName":"ok","address":"India","gender":"male","academicInfo":false,"profilePic":"/schoolapp/images/a91abbde-d06f-49ef-a814-734757dc63e0.png","geopoint":"20.5937,78.9629","parentDetails":{"fatherFirstName":null,"fatherLastName":null,"fatherOccupation":null,"motherFirstName":null,"motherLastName":null,"motherEmailId":null,"fatherPhoneNo":null,"motherPhoneNo":null,"motherProfilePic":null,"fatherEmailId":null,"motherOccupation":null,"fatherProfilePic":null},"guardianDetails":{"guardianLastName":null,"guardianFirstName":null,"education":null,"occupation":null,"address":null,"dob":null,"emailId":null,"phoneNo":null,"relation":null},"pastSchoolDetails":{"gradeId":null,"classId":null,"address":null,"yearOfPassing":null,"schoolName":null},"religion":null,"studentId":"1279","firstName":"D test","birthPlace":null,"nationality":null,"dob":"2018-02-08","schoolId":"129","id":"1291279","dateofJoining":null,"motherTounge":null},{"lastName":"ok","address":"India","gender":"male","academicInfo":false,"profilePic":"/schoolapp/images/a91abbde-d06f-49ef-a814-734757dc63e0.png","geopoint":"20.5937,78.9629","parentDetails":{"fatherFirstName":null,"fatherLastName":null,"fatherOccupation":null,"motherFirstName":null,"motherLastName":null,"motherEmailId":null,"fatherPhoneNo":null,"motherPhoneNo":null,"motherProfilePic":null,"fatherEmailId":null,"motherOccupation":null,"fatherProfilePic":null},"guardianDetails":{"guardianLastName":null,"guardianFirstName":null,"education":null,"occupation":null,"address":null,"dob":null,"emailId":null,"phoneNo":null,"relation":null},"pastSchoolDetails":{"gradeId":null,"classId":null,"address":null,"yearOfPassing":null,"schoolName":null},"religion":null,"studentId":"1277","firstName":"D test","birthPlace":null,"nationality":null,"dob":"2018-02-08","schoolId":"129","id":"1291277","dateofJoining":null,"motherTounge":null},{"lastName":"jky ret nmk","address":"typo ","gender":"male","academicInfo":false,"profilePic":"/schoolapp/images/53b9ce79-d873-4867-af66-353898e63cd6.png","geopoint":"37.2779,-83.2549","parentDetails":{"fatherFirstName":null,"fatherLastName":null,"fatherOccupation":null,"motherFirstName":null,"motherLastName":null,"motherEmailId":null,"fatherPhoneNo":null,"motherPhoneNo":null,"motherProfilePic":null,"fatherEmailId":null,"motherOccupation":null,"fatherProfilePic":null},"guardianDetails":{"guardianLastName":null,"guardianFirstName":null,"education":null,"occupation":null,"address":null,"dob":null,"emailId":null,"phoneNo":null,"relation":null},"pastSchoolDetails":{"gradeId":null,"classId":null,"address":null,"yearOfPassing":null,"schoolName":null},"religion":null,"studentId":"1291","firstName":"jitendra ","birthPlace":"","nationality":null,"dob":"2018-02-09","schoolId":"129","id":"1291291","dateofJoining":"2018-02-09","motherTounge":null},{"lastName":"Haro","address":"Surat","gender":"female","academicInfo":false,"profilePic":"/schoolapp/images/14629528-dfbd-48c2-997f-55ca72e50035.png","geopoint":"21.1702,72.8311","parentDetails":{"fatherLastName":"hari","fatherOccupation":"","fatherPhoneNo":null,"motherPhoneNo":null,"fatherEmailId":null,"motherOccupation":null,"fatherFirstName":"Ram","motherFirstName":null,"motherLastName":null,"motherEmailId":null,"files":[{"fileName":"asset.PNG","filePath":"/schoolapp/images/1bdedae0-a1b9-4ace-a9ee-f68da75fab6e.PNG"}],"motherProfilePic":null,"fatherProfilePic":"/schoolapp/images/831d53f5-25dd-4416-9a4d-e3dd52382018.png"},"guardianDetails":{"guardianLastName":null,"guardianFirstName":"hare had","education":null,"occupation":null,"address":null,"dob":null,"files":[{"fileName":"Screen 26.odt","filePath":"/schoolapp/images/69c7b206-649f-4a8a-8608-022cfd399e3e.odt"}],"emailId":null,"phoneNo":null,"relation":null},"pastSchoolDetails":{"gradeId":null,"classId":null,"address":null,"yearOfPassing":null,"files":[{"fileName":"asset.PNG","filePath":"/schoolapp/images/4e638f86-ec6e-4375-acd9-af1bf9c052f1.PNG"}],"schoolName":"School"},"religion":"Hindu","studentId":"1286","firstName":"Shree","birthPlace":"Surat","nationality":"Indian","dob":"2018-02-09","schoolId":"129","files":[{"fileName":"asset.PNG","filePath":"/schoolapp/images/641f03d7-07ae-4024-9a69-8fa2027be8e9.PNG"}],"id":"1291286","dateofJoining":"2018-02-09","motherTounge":"Gujarati"},{"lastName":"jk","address":"typo ","gender":"male","academicInfo":false,"profilePic":"/schoolapp/images/53b9ce79-d873-4867-af66-353898e63cd6.png","geopoint":"37.2779,-83.2549","parentDetails":{"fatherFirstName":null,"fatherLastName":null,"fatherOccupation":null,"motherFirstName":null,"motherLastName":null,"motherEmailId":null,"fatherPhoneNo":null,"motherPhoneNo":null,"motherProfilePic":null,"fatherEmailId":null,"motherOccupation":null,"fatherProfilePic":null},"guardianDetails":{"guardianLastName":null,"guardianFirstName":null,"education":null,"occupation":null,"address":null,"dob":null,"emailId":null,"phoneNo":null,"relation":null},"pastSchoolDetails":{"gradeId":null,"classId":null,"address":null,"yearOfPassing":null,"schoolName":null},"religion":null,"studentId":"1290","firstName":"jitendra ","birthPlace":"","nationality":null,"dob":"2018-02-09","schoolId":"129","id":"1291290","dateofJoining":"2018-02-09","motherTounge":null},{"lastName":"ggg","address":"India","gender":"male","academicInfo":false,"profilePic":"/schoolapp/images/9733d0fd-6be6-45ff-b7fd-5238ad299a30.png","geopoint":"20.5937,78.9629","parentDetails":{"fatherFirstName":null,"fatherLastName":null,"fatherOccupation":null,"motherFirstName":null,"motherLastName":null,"motherEmailId":null,"fatherPhoneNo":null,"motherPhoneNo":null,"motherProfilePic":null,"fatherEmailId":null,"motherOccupation":null,"fatherProfilePic":null},"guardianDetails":{"guardianLastName":null,"guardianFirstName":null,"education":null,"occupation":null,"address":null,"dob":null,"emailId":null,"phoneNo":null,"relation":null},"pastSchoolDetails":{"gradeId":null,"classId":null,"address":null,"yearOfPassing":null,"schoolName":null},"religion":null,"studentId":"1293","firstName":"he","birthPlace":"","nationality":null,"dob":"2018-02-09","schoolId":"129","id":"1291293","dateofJoining":null,"motherTounge":null}]
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private String authtokenexpires;
    private ArrayList<ResultBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public ArrayList<ResultBean> getResult() {
        return result;
    }

    public void setResult(ArrayList<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean implements Parcelable {
        /**
         * lastName : okiiiiiiiii
         * address : India
         * gender : male
         * academicInfo : false
         * profilePic : /schoolapp/images/a91abbde-d06f-49ef-a814-734757dc63e0.png
         * geopoint : 20.5937,78.9629
         * parentDetails : {}
         * guardianDetails : {}
         * pastSchoolDetails : {}
         * religion : null
         * studentId : 1278
         * firstName : D testrrrrr
         * birthPlace : null
         * nationality : null
         * dob : 2018-02-08
         * schoolId : 129
         * id : 1291278
         * dateofJoining : null
         * motherTounge : null
         * files : [{"fileName":"asset.PNG","filePath":"/schoolapp/images/641f03d7-07ae-4024-9a69-8fa2027be8e9.PNG"}]
         */

        private String lastName;
        private String address;
        private String gender;
        private boolean academicInfo;
        private String profilePic;
        private String geopoint;
        private String requiresTransportation;
        private ParentDetailsBean parentDetails;
        private GuardianDetailsBean guardianDetails;
        private PastSchoolDetailsBean pastSchoolDetails;
        private String religion;
        private String studentId;
        private String emailId;
        private String firstName;
        private String birthPlace;
        private String nationality;
        private String dob;
        private String schoolId;
        private String id;
        private String dateofJoining;
        private String motherTounge;
        private String schoolYear;
        private String phoneNo;
        private List<FilesBean> files;
        private boolean isPending = true;
        private String gradeId;
        private String classId;
        private boolean isSelected = false;

        public String getRequiresTransportation() {
            return requiresTransportation;
        }

        public void setRequiresTransportation(String requiresTransportation) {
            this.requiresTransportation = requiresTransportation;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        public String getGradeId() {
            return gradeId;
        }

        public void setGradeId(String gradeId) {
            this.gradeId = gradeId;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }


        public String getSchoolYear() {
            return schoolYear;
        }

        public void setSchoolYear(String schoolYear) {
            this.schoolYear = schoolYear;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public String getEmailId() {
            return emailId;
        }

        public void setEmailId(String emailId) {
            this.emailId = emailId;
        }

        public boolean isPending() {
            return isPending;
        }

        public void setPending(boolean pending) {
            isPending = pending;
        }

        public static Creator<ResultBean> getCREATOR() {
            return CREATOR;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public boolean isAcademicInfo() {
            return academicInfo;
        }

        public void setAcademicInfo(boolean academicInfo) {
            this.academicInfo = academicInfo;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        public String getGeopoint() {
            return geopoint;
        }

        public void setGeopoint(String geopoint) {
            this.geopoint = geopoint;
        }

        public ParentDetailsBean getParentDetails() {
            return parentDetails;
        }

        public void setParentDetails(ParentDetailsBean parentDetails) {
            this.parentDetails = parentDetails;
        }

        public GuardianDetailsBean getGuardianDetails() {
            return guardianDetails;
        }

        public void setGuardianDetails(GuardianDetailsBean guardianDetails) {
            this.guardianDetails = guardianDetails;
        }

        public PastSchoolDetailsBean getPastSchoolDetails() {
            return pastSchoolDetails;
        }

        public void setPastSchoolDetails(PastSchoolDetailsBean pastSchoolDetails) {
            this.pastSchoolDetails = pastSchoolDetails;
        }

        public String getReligion() {
            return religion;
        }

        public void setReligion(String religion) {
            this.religion = religion;
        }

        public String getStudentId() {
            return studentId;
        }

        public void setStudentId(String studentId) {
            this.studentId = studentId;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getBirthPlace() {
            return birthPlace;
        }

        public void setBirthPlace(String birthPlace) {
            this.birthPlace = birthPlace;
        }

        public String getNationality() {
            return nationality;
        }

        public void setNationality(String nationality) {
            this.nationality = nationality;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDateofJoining() {
            return dateofJoining;
        }

        public void setDateofJoining(String dateofJoining) {
            this.dateofJoining = dateofJoining;
        }

        public String getMotherTounge() {
            return motherTounge;
        }

        public void setMotherTounge(String motherTounge) {
            this.motherTounge = motherTounge;
        }

        public List<FilesBean> getFiles() {
            return files;
        }

        public void setFiles(List<FilesBean> files) {
            this.files = files;
        }

        public static class ParentDetailsBean implements Parcelable {


            /**
             * fatherFirstName : null
             * fatherLastName : null
             * fatherOccupation : null
             * motherFirstName : null
             * motherLastName : null
             * motherEmailId : null
             * fatherPhoneNo : null
             * motherPhoneNo : null
             * motherProfilePic : null
             * fatherEmailId : null
             * motherOccupation : null
             * fatherProfilePic : null
             */

            private String fatherFirstName;
            private String fatherLastName;
            private String fatherOccupation;
            private String motherFirstName;
            private String motherLastName;
            private String motherEmailId;
            private String fatherPhoneNo;
            private String motherPhoneNo;
            private String motherProfilePic;
            private String fatherEmailId;
            private String motherOccupation;
            private String fatherProfilePic;

            public ParentDetailsBean() {
            }

            public String getFatherFirstName() {
                return fatherFirstName;
            }

            public void setFatherFirstName(String fatherFirstName) {
                this.fatherFirstName = fatherFirstName;
            }

            public String getFatherLastName() {
                return fatherLastName;
            }

            public void setFatherLastName(String fatherLastName) {
                this.fatherLastName = fatherLastName;
            }

            public String getFatherOccupation() {
                return fatherOccupation;
            }

            public void setFatherOccupation(String fatherOccupation) {
                this.fatherOccupation = fatherOccupation;
            }

            public String getMotherFirstName() {
                return motherFirstName;
            }

            public void setMotherFirstName(String motherFirstName) {
                this.motherFirstName = motherFirstName;
            }

            public String getMotherLastName() {
                return motherLastName;
            }

            public void setMotherLastName(String motherLastName) {
                this.motherLastName = motherLastName;
            }

            public String getMotherEmailId() {
                return motherEmailId;
            }

            public void setMotherEmailId(String motherEmailId) {
                this.motherEmailId = motherEmailId;
            }

            public String getFatherPhoneNo() {
                return fatherPhoneNo;
            }

            public void setFatherPhoneNo(String fatherPhoneNo) {
                this.fatherPhoneNo = fatherPhoneNo;
            }

            public String getMotherPhoneNo() {
                return motherPhoneNo;
            }

            public void setMotherPhoneNo(String motherPhoneNo) {
                this.motherPhoneNo = motherPhoneNo;
            }

            public String getMotherProfilePic() {
                return motherProfilePic;
            }

            public void setMotherProfilePic(String motherProfilePic) {
                this.motherProfilePic = motherProfilePic;
            }

            public String getFatherEmailId() {
                return fatherEmailId;
            }

            public void setFatherEmailId(String fatherEmailId) {
                this.fatherEmailId = fatherEmailId;
            }

            public String getMotherOccupation() {
                return motherOccupation;
            }

            public void setMotherOccupation(String motherOccupation) {
                this.motherOccupation = motherOccupation;
            }

            public String getFatherProfilePic() {
                return fatherProfilePic;
            }

            public void setFatherProfilePic(String fatherProfilePic) {
                this.fatherProfilePic = fatherProfilePic;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.fatherFirstName);
                dest.writeString(this.fatherLastName);
                dest.writeString(this.fatherOccupation);
                dest.writeString(this.motherFirstName);
                dest.writeString(this.motherLastName);
                dest.writeString(this.motherEmailId);
                dest.writeString(this.fatherPhoneNo);
                dest.writeString(this.motherPhoneNo);
                dest.writeString(this.motherProfilePic);
                dest.writeString(this.fatherEmailId);
                dest.writeString(this.motherOccupation);
                dest.writeString(this.fatherProfilePic);
            }

            protected ParentDetailsBean(Parcel in) {
                this.fatherFirstName = in.readString();
                this.fatherLastName = in.readString();
                this.fatherOccupation = in.readString();
                this.motherFirstName = in.readString();
                this.motherLastName = in.readString();
                this.motherEmailId = in.readString();
                this.fatherPhoneNo = in.readString();
                this.motherPhoneNo = in.readString();
                this.motherProfilePic = in.readString();
                this.fatherEmailId = in.readString();
                this.motherOccupation = in.readString();
                this.fatherProfilePic = in.readString();
            }

            public static final Creator<ParentDetailsBean> CREATOR = new Creator<ParentDetailsBean>() {
                @Override
                public ParentDetailsBean createFromParcel(Parcel source) {
                    return new ParentDetailsBean(source);
                }

                @Override
                public ParentDetailsBean[] newArray(int size) {
                    return new ParentDetailsBean[size];
                }
            };
        }

        public static class GuardianDetailsBean implements Parcelable {


            /**
             * guardianLastName : null
             * guardianFirstName : null
             * education : null
             * occupation : null
             * address : null
             * dob : null
             * emailId : null
             * phoneNo : null
             * relation : null
             */

            private String guardianLastName;
            private String guardianFirstName;
            private String education;
            private String occupation;
            private String address;
            private String dob;
            private String emailId;
            private String phoneNo;
            private String relation;

            public GuardianDetailsBean() {
            }

            public String getGuardianLastName() {
                return guardianLastName;
            }

            public void setGuardianLastName(String guardianLastName) {
                this.guardianLastName = guardianLastName;
            }

            public String getGuardianFirstName() {
                return guardianFirstName;
            }

            public void setGuardianFirstName(String guardianFirstName) {
                this.guardianFirstName = guardianFirstName;
            }

            public String getEducation() {
                return education;
            }

            public void setEducation(String education) {
                this.education = education;
            }

            public String getOccupation() {
                return occupation;
            }

            public void setOccupation(String occupation) {
                this.occupation = occupation;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getDob() {
                return dob;
            }

            public void setDob(String dob) {
                this.dob = dob;
            }

            public String getEmailId() {
                return emailId;
            }

            public void setEmailId(String emailId) {
                this.emailId = emailId;
            }

            public String getPhoneNo() {
                return phoneNo;
            }

            public void setPhoneNo(String phoneNo) {
                this.phoneNo = phoneNo;
            }

            public String getRelation() {
                return relation;
            }

            public void setRelation(String relation) {
                this.relation = relation;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.guardianLastName);
                dest.writeString(this.guardianFirstName);
                dest.writeString(this.education);
                dest.writeString(this.occupation);
                dest.writeString(this.address);
                dest.writeString(this.dob);
                dest.writeString(this.emailId);
                dest.writeString(this.phoneNo);
                dest.writeString(this.relation);
            }

            protected GuardianDetailsBean(Parcel in) {
                this.guardianLastName = in.readString();
                this.guardianFirstName = in.readString();
                this.education = in.readString();
                this.occupation = in.readString();
                this.address = in.readString();
                this.dob = in.readString();
                this.emailId = in.readString();
                this.phoneNo = in.readString();
                this.relation = in.readString();
            }

            public static final Creator<GuardianDetailsBean> CREATOR = new Creator<GuardianDetailsBean>() {
                @Override
                public GuardianDetailsBean createFromParcel(Parcel source) {
                    return new GuardianDetailsBean(source);
                }

                @Override
                public GuardianDetailsBean[] newArray(int size) {
                    return new GuardianDetailsBean[size];
                }
            };
        }

        public static class PastSchoolDetailsBean implements Parcelable {


            /**
             * gradeId : null
             * classId : null
             * address : null
             * yearOfPassing : null
             * schoolName : null
             */

            private String gradeId;
            private String classId;
            private String address;
            private String yearOfPassing;
            private String schoolName;

            public PastSchoolDetailsBean() {
            }

            public String getGradeId() {
                return gradeId;
            }

            public void setGradeId(String gradeId) {
                this.gradeId = gradeId;
            }

            public String getClassId() {
                return classId;
            }

            public void setClassId(String classId) {
                this.classId = classId;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getYearOfPassing() {
                return yearOfPassing;
            }

            public void setYearOfPassing(String yearOfPassing) {
                this.yearOfPassing = yearOfPassing;
            }

            public String getSchoolName() {
                return schoolName;
            }

            public void setSchoolName(String schoolName) {
                this.schoolName = schoolName;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.gradeId);
                dest.writeString(this.classId);
                dest.writeString(this.address);
                dest.writeString(this.yearOfPassing);
                dest.writeString(this.schoolName);
            }

            protected PastSchoolDetailsBean(Parcel in) {
                this.gradeId = in.readString();
                this.classId = in.readString();
                this.address = in.readString();
                this.yearOfPassing = in.readString();
                this.schoolName = in.readString();
            }

            public static final Creator<PastSchoolDetailsBean> CREATOR = new Creator<PastSchoolDetailsBean>() {
                @Override
                public PastSchoolDetailsBean createFromParcel(Parcel source) {
                    return new PastSchoolDetailsBean(source);
                }

                @Override
                public PastSchoolDetailsBean[] newArray(int size) {
                    return new PastSchoolDetailsBean[size];
                }
            };
        }

        public static class FilesBean implements Parcelable {
            /**
             * fileName : asset.PNG
             * filePath : /schoolapp/images/641f03d7-07ae-4024-9a69-8fa2027be8e9.PNG
             */

            private String fileName;
            private String filePath;

            public String getFileName() {
                return fileName;
            }

            public void setFileName(String fileName) {
                this.fileName = fileName;
            }

            public String getFilePath() {
                return filePath;
            }

            public void setFilePath(String filePath) {
                this.filePath = filePath;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.fileName);
                dest.writeString(this.filePath);
            }

            public FilesBean() {
            }

            protected FilesBean(Parcel in) {
                this.fileName = in.readString();
                this.filePath = in.readString();
            }

            public static final Creator<FilesBean> CREATOR = new Creator<FilesBean>() {
                @Override
                public FilesBean createFromParcel(Parcel source) {
                    return new FilesBean(source);
                }

                @Override
                public FilesBean[] newArray(int size) {
                    return new FilesBean[size];
                }
            };
        }

        public ResultBean() {
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.lastName);
            dest.writeString(this.address);
            dest.writeString(this.gender);
            dest.writeByte(this.academicInfo ? (byte) 1 : (byte) 0);
            dest.writeString(this.profilePic);
            dest.writeString(this.geopoint);
            dest.writeString(this.requiresTransportation);
            dest.writeParcelable(this.parentDetails, flags);
            dest.writeParcelable(this.guardianDetails, flags);
            dest.writeParcelable(this.pastSchoolDetails, flags);
            dest.writeString(this.religion);
            dest.writeString(this.studentId);
            dest.writeString(this.emailId);
            dest.writeString(this.firstName);
            dest.writeString(this.birthPlace);
            dest.writeString(this.nationality);
            dest.writeString(this.dob);
            dest.writeString(this.schoolId);
            dest.writeString(this.id);
            dest.writeString(this.dateofJoining);
            dest.writeString(this.motherTounge);
            dest.writeString(this.schoolYear);
            dest.writeString(this.phoneNo);
            dest.writeTypedList(this.files);
            dest.writeByte(this.isPending ? (byte) 1 : (byte) 0);
            dest.writeString(this.gradeId);
            dest.writeString(this.classId);
            dest.writeByte(this.isSelected ? (byte) 1 : (byte) 0);
        }

        protected ResultBean(Parcel in) {
            this.lastName = in.readString();
            this.address = in.readString();
            this.gender = in.readString();
            this.academicInfo = in.readByte() != 0;
            this.profilePic = in.readString();
            this.geopoint = in.readString();
            this.requiresTransportation = in.readString();
            this.parentDetails = in.readParcelable(ParentDetailsBean.class.getClassLoader());
            this.guardianDetails = in.readParcelable(GuardianDetailsBean.class.getClassLoader());
            this.pastSchoolDetails = in.readParcelable(PastSchoolDetailsBean.class.getClassLoader());
            this.religion = in.readString();
            this.studentId = in.readString();
            this.emailId = in.readString();
            this.firstName = in.readString();
            this.birthPlace = in.readString();
            this.nationality = in.readString();
            this.dob = in.readString();
            this.schoolId = in.readString();
            this.id = in.readString();
            this.dateofJoining = in.readString();
            this.motherTounge = in.readString();
            this.schoolYear = in.readString();
            this.phoneNo = in.readString();
            this.files = in.createTypedArrayList(FilesBean.CREATOR);
            this.isPending = in.readByte() != 0;
            this.gradeId = in.readString();
            this.classId = in.readString();
            this.isSelected = in.readByte() != 0;
        }

        public static final Creator<ResultBean> CREATOR = new Creator<ResultBean>() {
            @Override
            public ResultBean createFromParcel(Parcel source) {
                return new ResultBean(source);
            }

            @Override
            public ResultBean[] newArray(int size) {
                return new ResultBean[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.message);
        dest.writeByte(this.status ? (byte) 1 : (byte) 0);
        dest.writeString(this.authtoken);
        dest.writeString(this.authtokenexpires);
        dest.writeList(this.result);
    }

    public GetStudentsList() {
    }

    protected GetStudentsList(Parcel in) {
        this.message = in.readString();
        this.status = in.readByte() != 0;
        this.authtoken = in.readString();
        this.authtokenexpires = in.readString();
        this.result = new ArrayList<ResultBean>();
        in.readList(this.result, ResultBean.class.getClassLoader());
    }

    public static final Parcelable.Creator<GetStudentsList> CREATOR = new Parcelable.Creator<GetStudentsList>() {
        @Override
        public GetStudentsList createFromParcel(Parcel source) {
            return new GetStudentsList(source);
        }

        @Override
        public GetStudentsList[] newArray(int size) {
            return new GetStudentsList[size];
        }
    };
}
