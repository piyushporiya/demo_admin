/*
 * Copyright (c) 2017.                                                       
 *                                                                           
 * ***********************************************************************  
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                           
 * ________________________________________________________________________  
 *                                                                           
 *    [2010] - [2018] SripathiSolutions Corp.                                
 *    [2010] - [2018] DataReady Technology Corp.                             
 *   ________________________________________________________________________
 *                                                                           
 *    All Rights Reserved.                                                   
 *                                                                           
 *   NOTICE:  All information contained herein is, and remains               
 *   the property of DataReady Technology  and its suppliers,                
 *   if any.  The intellectual and technical concepts contained              
 *   herein are proprietary to DataReady Technology Incorporated             
 *   and its suppliers and may be covered by U.S. and Foreign Patents,       
 *   patents in process, and are protected by trade secret or copyright law. 
 *   Dissemination of this information or reproduction of this material      
 *   is strictly forbidden unless prior written permission is obtained       
 *   from DataReady Technology Incorporated.                                 
 *                                                                           
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                            
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class GetAssignedTeacherList {


    /**
     * message : Class room schedule details are below
     * status : true
     * authtoken : null
     * result : [{"teacherMobileNo":"2147483647","gradeId":"3","teacherName":"Teacher Two","subjectEndTime":"13:00","subjectStartTime":"12:00","classId":"A","teacherId":"teachertwo@dataready.in","teacherEmailId":"teachertwo@dataready.in","schoolId":"129","weekDay":"2","schoolYear":"2017-2018","id":"1292017-2018A32Science12:00","subjectName":"Science"},{"teacherMobileNo":"2147483647","gradeId":"3","teacherName":"Teacher Three","subjectEndTime":"12:00","subjectStartTime":"11:00","classId":"A","teacherId":"teacherthree@dataready.in","teacherEmailId":"teacherthree@dataready.in","schoolId":"129","weekDay":"2","schoolYear":"2017-2018","id":"1292017-2018A32History11:00","subjectName":"History"}]
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private String authtokenexpires;
    private List<ResultBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean implements Parcelable {
        /**
         * teacherMobileNo : 2147483647
         * gradeId : 3
         * teacherName : Teacher Two
         * subjectEndTime : 13:00
         * subjectStartTime : 12:00
         * classId : A
         * teacherId : teachertwo@dataready.in
         * teacherEmailId : teachertwo@dataready.in
         * schoolId : 129
         * weekDay : 2
         * schoolYear : 2017-2018
         * id : 1292017-2018A32Science12:00
         * subjectName : Science
         */

        private String teacherMobileNo;
        private String gradeId;
        private String teacherName;
        private String subjectEndTime;
        private String subjectStartTime;
        private String classId;
        private String teacherId;
        private String teacherEmailId;
        private String schoolId;
        private String weekDay;
        private String schoolYear;
        private String id;
        private String subjectName;

        public String getTeacherMobileNo() {
            return teacherMobileNo;
        }

        public void setTeacherMobileNo(String teacherMobileNo) {
            this.teacherMobileNo = teacherMobileNo;
        }

        public String getGradeId() {
            return gradeId;
        }

        public void setGradeId(String gradeId) {
            this.gradeId = gradeId;
        }

        public String getTeacherName() {
            return teacherName;
        }

        public void setTeacherName(String teacherName) {
            this.teacherName = teacherName;
        }

        public String getSubjectEndTime() {
            return subjectEndTime;
        }

        public void setSubjectEndTime(String subjectEndTime) {
            this.subjectEndTime = subjectEndTime;
        }

        public String getSubjectStartTime() {
            return subjectStartTime;
        }

        public void setSubjectStartTime(String subjectStartTime) {
            this.subjectStartTime = subjectStartTime;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getTeacherId() {
            return teacherId;
        }

        public void setTeacherId(String teacherId) {
            this.teacherId = teacherId;
        }

        public String getTeacherEmailId() {
            return teacherEmailId;
        }

        public void setTeacherEmailId(String teacherEmailId) {
            this.teacherEmailId = teacherEmailId;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getWeekDay() {
            return weekDay;
        }

        public void setWeekDay(String weekDay) {
            this.weekDay = weekDay;
        }

        public String getSchoolYear() {
            return schoolYear;
        }

        public void setSchoolYear(String schoolYear) {
            this.schoolYear = schoolYear;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.teacherMobileNo);
            dest.writeString(this.gradeId);
            dest.writeString(this.teacherName);
            dest.writeString(this.subjectEndTime);
            dest.writeString(this.subjectStartTime);
            dest.writeString(this.classId);
            dest.writeString(this.teacherId);
            dest.writeString(this.teacherEmailId);
            dest.writeString(this.schoolId);
            dest.writeString(this.weekDay);
            dest.writeString(this.schoolYear);
            dest.writeString(this.id);
            dest.writeString(this.subjectName);
        }

        public ResultBean() {
        }

        protected ResultBean(Parcel in) {
            this.teacherMobileNo = in.readString();
            this.gradeId = in.readString();
            this.teacherName = in.readString();
            this.subjectEndTime = in.readString();
            this.subjectStartTime = in.readString();
            this.classId = in.readString();
            this.teacherId = in.readString();
            this.teacherEmailId = in.readString();
            this.schoolId = in.readString();
            this.weekDay = in.readString();
            this.schoolYear = in.readString();
            this.id = in.readString();
            this.subjectName = in.readString();
        }

        public static final Parcelable.Creator<ResultBean> CREATOR = new Parcelable.Creator<ResultBean>() {
            @Override
            public ResultBean createFromParcel(Parcel source) {
                return new ResultBean(source);
            }

            @Override
            public ResultBean[] newArray(int size) {
                return new ResultBean[size];
            }
        };
    }
}
