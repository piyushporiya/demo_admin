/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;

public class GetStudentInfo {


    /**
     * message : Student informations are below
     * status : true
     * authtoken : null
     * result : {"lastName":"Mendapara","address":"rander surat","gender":"male","academicInfo":true,"profilePic":"/schoolapp/images/1291234.jpg","geopoint":null,"parentDetails":{"fatherFirstName":"","fatherLastName":"","fatherOccupation":"","motherFirstName":"","motherLastName":"","motherEmailId":"","fatherPhoneNo":"","motherPhoneNo":"","motherProfilePic":null,"fatherEmailId":"","motherOccupation":"","fatherProfilePic":null},"guardianDetails":{"guardianLastName":"","guardianFirstName":"","education":"","occupation":"","address":"","dob":"","emailId":"","phoneNo":"","relation":""},"pastSchoolDetails":{"gradeId":"","classId":"","address":"","yearOfPassing":"","schoolName":""},"religion":"","studentId":"1234","firstName":"Darshit","birthPlace":"","nationality":"","dob":"1987-01-05","schoolId":"129","id":"1291234","dateofJoining":"","motherTounge":""}
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private ResultBean result;
    private String authtokenexpires;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public static class ResultBean {
        /**
         * lastName : Mendapara
         * address : rander surat
         * gender : male
         * academicInfo : true
         * profilePic : /schoolapp/images/1291234.jpg
         * geopoint : null
         * parentDetails : {"fatherFirstName":"","fatherLastName":"","fatherOccupation":"","motherFirstName":"","motherLastName":"","motherEmailId":"","fatherPhoneNo":"","motherPhoneNo":"","motherProfilePic":null,"fatherEmailId":"","motherOccupation":"","fatherProfilePic":null}
         * guardianDetails : {"guardianLastName":"","guardianFirstName":"","education":"","occupation":"","address":"","dob":"","emailId":"","phoneNo":"","relation":""}
         * pastSchoolDetails : {"gradeId":"","classId":"","address":"","yearOfPassing":"","schoolName":""}
         * religion : 
         * studentId : 1234
         * firstName : Darshit
         * birthPlace : 
         * nationality : 
         * dob : 1987-01-05
         * schoolId : 129
         * id : 1291234
         * dateofJoining : 
         * motherTounge : 
         */

        private String lastName;
        private String address;
        private String gender;
        private boolean academicInfo;
        private String profilePic;
        private String geopoint;
        private ParentDetailsBean parentDetails;
        private GuardianDetailsBean guardianDetails;
        private PastSchoolDetailsBean pastSchoolDetails;
        private String religion;
        private String studentId;
        private String firstName;
        private String birthPlace;
        private String nationality;
        private String dob;
        private String schoolId;
        private String id;
        private String dateofJoining;
        private String motherTounge;

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public boolean isAcademicInfo() {
            return academicInfo;
        }

        public void setAcademicInfo(boolean academicInfo) {
            this.academicInfo = academicInfo;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        public String getGeopoint() {
            return geopoint;
        }

        public void setGeopoint(String geopoint) {
            this.geopoint = geopoint;
        }

        public ParentDetailsBean getParentDetails() {
            return parentDetails;
        }

        public void setParentDetails(ParentDetailsBean parentDetails) {
            this.parentDetails = parentDetails;
        }

        public GuardianDetailsBean getGuardianDetails() {
            return guardianDetails;
        }

        public void setGuardianDetails(GuardianDetailsBean guardianDetails) {
            this.guardianDetails = guardianDetails;
        }

        public PastSchoolDetailsBean getPastSchoolDetails() {
            return pastSchoolDetails;
        }

        public void setPastSchoolDetails(PastSchoolDetailsBean pastSchoolDetails) {
            this.pastSchoolDetails = pastSchoolDetails;
        }

        public String getReligion() {
            return religion;
        }

        public void setReligion(String religion) {
            this.religion = religion;
        }

        public String getStudentId() {
            return studentId;
        }

        public void setStudentId(String studentId) {
            this.studentId = studentId;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getBirthPlace() {
            return birthPlace;
        }

        public void setBirthPlace(String birthPlace) {
            this.birthPlace = birthPlace;
        }

        public String getNationality() {
            return nationality;
        }

        public void setNationality(String nationality) {
            this.nationality = nationality;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDateofJoining() {
            return dateofJoining;
        }

        public void setDateofJoining(String dateofJoining) {
            this.dateofJoining = dateofJoining;
        }

        public String getMotherTounge() {
            return motherTounge;
        }

        public void setMotherTounge(String motherTounge) {
            this.motherTounge = motherTounge;
        }

        public static class ParentDetailsBean {
            /**
             * fatherFirstName : 
             * fatherLastName : 
             * fatherOccupation : 
             * motherFirstName : 
             * motherLastName : 
             * motherEmailId : 
             * fatherPhoneNo : 
             * motherPhoneNo : 
             * motherProfilePic : null
             * fatherEmailId : 
             * motherOccupation : 
             * fatherProfilePic : null
             */

            private String fatherFirstName;
            private String fatherLastName;
            private String fatherOccupation;
            private String motherFirstName;
            private String motherLastName;
            private String motherEmailId;
            private String fatherPhoneNo;
            private String motherPhoneNo;
            private String motherProfilePic;
            private String fatherEmailId;
            private String motherOccupation;
            private String fatherProfilePic;

            public String getFatherFirstName() {
                return fatherFirstName;
            }

            public void setFatherFirstName(String fatherFirstName) {
                this.fatherFirstName = fatherFirstName;
            }

            public String getFatherLastName() {
                return fatherLastName;
            }

            public void setFatherLastName(String fatherLastName) {
                this.fatherLastName = fatherLastName;
            }

            public String getFatherOccupation() {
                return fatherOccupation;
            }

            public void setFatherOccupation(String fatherOccupation) {
                this.fatherOccupation = fatherOccupation;
            }

            public String getMotherFirstName() {
                return motherFirstName;
            }

            public void setMotherFirstName(String motherFirstName) {
                this.motherFirstName = motherFirstName;
            }

            public String getMotherLastName() {
                return motherLastName;
            }

            public void setMotherLastName(String motherLastName) {
                this.motherLastName = motherLastName;
            }

            public String getMotherEmailId() {
                return motherEmailId;
            }

            public void setMotherEmailId(String motherEmailId) {
                this.motherEmailId = motherEmailId;
            }

            public String getFatherPhoneNo() {
                return fatherPhoneNo;
            }

            public void setFatherPhoneNo(String fatherPhoneNo) {
                this.fatherPhoneNo = fatherPhoneNo;
            }

            public String getMotherPhoneNo() {
                return motherPhoneNo;
            }

            public void setMotherPhoneNo(String motherPhoneNo) {
                this.motherPhoneNo = motherPhoneNo;
            }

            public String getMotherProfilePic() {
                return motherProfilePic;
            }

            public void setMotherProfilePic(String motherProfilePic) {
                this.motherProfilePic = motherProfilePic;
            }

            public String getFatherEmailId() {
                return fatherEmailId;
            }

            public void setFatherEmailId(String fatherEmailId) {
                this.fatherEmailId = fatherEmailId;
            }

            public String getMotherOccupation() {
                return motherOccupation;
            }

            public void setMotherOccupation(String motherOccupation) {
                this.motherOccupation = motherOccupation;
            }

            public String getFatherProfilePic() {
                return fatherProfilePic;
            }

            public void setFatherProfilePic(String fatherProfilePic) {
                this.fatherProfilePic = fatherProfilePic;
            }
        }

        public static class GuardianDetailsBean {
            /**
             * guardianLastName : 
             * guardianFirstName : 
             * education : 
             * occupation : 
             * address : 
             * dob : 
             * emailId : 
             * phoneNo : 
             * relation : 
             */

            private String guardianLastName;
            private String guardianFirstName;
            private String education;
            private String occupation;
            private String address;
            private String dob;
            private String emailId;
            private String phoneNo;
            private String relation;

            public String getGuardianLastName() {
                return guardianLastName;
            }

            public void setGuardianLastName(String guardianLastName) {
                this.guardianLastName = guardianLastName;
            }

            public String getGuardianFirstName() {
                return guardianFirstName;
            }

            public void setGuardianFirstName(String guardianFirstName) {
                this.guardianFirstName = guardianFirstName;
            }

            public String getEducation() {
                return education;
            }

            public void setEducation(String education) {
                this.education = education;
            }

            public String getOccupation() {
                return occupation;
            }

            public void setOccupation(String occupation) {
                this.occupation = occupation;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getDob() {
                return dob;
            }

            public void setDob(String dob) {
                this.dob = dob;
            }

            public String getEmailId() {
                return emailId;
            }

            public void setEmailId(String emailId) {
                this.emailId = emailId;
            }

            public String getPhoneNo() {
                return phoneNo;
            }

            public void setPhoneNo(String phoneNo) {
                this.phoneNo = phoneNo;
            }

            public String getRelation() {
                return relation;
            }

            public void setRelation(String relation) {
                this.relation = relation;
            }
        }

        public static class PastSchoolDetailsBean {
            /**
             * gradeId : 
             * classId : 
             * address : 
             * yearOfPassing : 
             * schoolName : 
             */

            private String gradeId;
            private String classId;
            private String address;
            private String yearOfPassing;
            private String schoolName;

            public String getGradeId() {
                return gradeId;
            }

            public void setGradeId(String gradeId) {
                this.gradeId = gradeId;
            }

            public String getClassId() {
                return classId;
            }

            public void setClassId(String classId) {
                this.classId = classId;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getYearOfPassing() {
                return yearOfPassing;
            }

            public void setYearOfPassing(String yearOfPassing) {
                this.yearOfPassing = yearOfPassing;
            }

            public String getSchoolName() {
                return schoolName;
            }

            public void setSchoolName(String schoolName) {
                this.schoolName = schoolName;
            }
        }
    }
}
