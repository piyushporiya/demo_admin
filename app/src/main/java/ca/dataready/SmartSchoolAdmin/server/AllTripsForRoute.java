/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;

import java.util.ArrayList;
import java.util.List;

public class AllTripsForRoute {


    /**
     * message : Details are below
     * status : true
     * authtoken : null
     * result : [{"endTripTime":"2017-09-11 13:32:10 UTC","tripType":"null","routeId":"10","driverId":"jitendra@techsoftlabs.com","startTripTime":"2017-09-08 11:27:58 UTC","schoolId":"129","name":"SP10-B","tripId":"AV5hPxePeCSjgpUCcNSs","uniqueId":"6940932","status":"completed"},{"endTripTime":"2017-09-08 12:04:55 UTC","tripType":"null","routeId":"9","driverId":"piyushporiya@techsoftlabs.com","startTripTime":"2017-09-08 12:01:24 UTC","schoolId":"129","name":"SP9","tripId":"AV5hXbJceCSjgpUCcNV8","uniqueId":"694092","status":"completed"},{"endTripTime":"2017-09-08 12:15:33 UTC","tripType":"null","routeId":"9","driverId":"piyushporiya@techsoftlabs.com","startTripTime":"2017-09-08 12:13:55 UTC","schoolId":"129","name":"SP9","tripId":"AV5haSp7eCSjgpUCcNXr","uniqueId":"694092","status":"completed"},{"endTripTime":"2017-09-08 12:25:47 UTC","tripType":"null","routeId":"9","driverId":"piyushporiya@techsoftlabs.com","startTripTime":"2017-09-08 12:24:45 UTC","schoolId":"129","name":"SP9","tripId":"AV5hcxR8eCSjgpUCcNYN","uniqueId":"694092","status":"completed"},{"endTripTime":"2017-09-08 11:23:26 UTC","tripType":"null","routeId":"10","driverId":"jitendra@techsoftlabs.com","startTripTime":"2017-09-08 11:07:07 UTC","schoolId":"129","name":"SP10-B","tripId":"AV5hLAJFeCSjgpUCcNQx","uniqueId":"6940932","status":"completed"},{"endTripTime":"2017-09-08 11:06:35 UTC","tripType":"null","routeId":"10","driverId":"jitendra@techsoftlabs.com","startTripTime":"2017-09-08 10:48:58 UTC","schoolId":"129","name":"SP10","tripId":"AV5hG2M3eCSjgpUCcNOk","uniqueId":"694093","status":"completed"}]
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private String authtokenexpires;
    private ArrayList<ResultBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public ArrayList<ResultBean> getResult() {
        return result;
    }

    public void setResult(ArrayList<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * endTripTime : 2017-09-11 13:32:10 UTC
         * tripType : null
         * routeId : 10
         * driverId : jitendra@techsoftlabs.com
         * startTripTime : 2017-09-08 11:27:58 UTC
         * schoolId : 129
         * name : SP10-B
         * tripId : AV5hPxePeCSjgpUCcNSs
         * uniqueId : 6940932
         * status : completed
         */

        private String endTripTime;
        private String tripType;
        private String routeId;
        private String driverId;
        private String startTripTime;
        private String schoolId;
        private String name;
        private String tripId;
        private String uniqueId;
        private String status;
        private boolean isSelected = false;

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        public String getEndTripTime() {
            return endTripTime;
        }

        public void setEndTripTime(String endTripTime) {
            this.endTripTime = endTripTime;
        }

        public String getTripType() {
            return tripType;
        }

        public void setTripType(String tripType) {
            this.tripType = tripType;
        }

        public String getRouteId() {
            return routeId;
        }

        public void setRouteId(String routeId) {
            this.routeId = routeId;
        }

        public String getDriverId() {
            return driverId;
        }

        public void setDriverId(String driverId) {
            this.driverId = driverId;
        }

        public String getStartTripTime() {
            return startTripTime;
        }

        public void setStartTripTime(String startTripTime) {
            this.startTripTime = startTripTime;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTripId() {
            return tripId;
        }

        public void setTripId(String tripId) {
            this.tripId = tripId;
        }

        public String getUniqueId() {
            return uniqueId;
        }

        public void setUniqueId(String uniqueId) {
            this.uniqueId = uniqueId;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
