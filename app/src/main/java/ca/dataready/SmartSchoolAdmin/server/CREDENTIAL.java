/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;

import java.util.List;

/**
 * Created by social_jaydeep on 27/09/17.
 */

public class CREDENTIAL {


    /**
     * message : User is authorized
     * status : true
     * authtoken : 127439
     * result : [{"googleId":"teacherone@dataready.in","lastName":"One","role":"staff","gender":"Female","emailId":"teacherone@dataready.in","phoneNo":"98765432101","schoolProfilePic":"/schoolapp/images/modernschool.jpg","password":"testing","isDeleted":"","countryCode":"IN","schoolId":"129","staffPin":172639,"usingFaceBookId":"true","schoolYear":"2017-2018","deviceTokenId":"egaSLv3Ep68:APA91bHpIulCfErSUXpt-foj4CKRgDe_zq_e4Si5ml1g6qT_xvZghH_jTCtGSxbaAng5WPqvDj5vUHwJQZTPaCnCJM7cMlmbGE9UM5syhi69CiazKg8sAPg0Wihw7LL-g4GXyA14zvNc","id":"teacherone@dataready.in","schoolName":"Pankaj Modern School","profilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolAddress":"Barakhamba Road New Delhi New Delhi Delhi Postal code - 110001","faceBookId":"teacherone@dataready.in","firstName":"Teacher","createdDate":"2017-06-14","modifiedDate":"2017-12-26","routeDetails":{"dropoffRouteId":"10","dropoffStopId":"1","pickupRouteId":"9","dropoffRouteName":"SP10","name":"","stopId":"","pickupStopId":"1","id":"","uniqueId":"","pickupRouteName":"SP9"},"usingGoogleId":"true","userType":"teacher"}]
     * authtokenexpires : 2017-12-27 04:56 UTC
     */

    private String message;
    private boolean status;
    private String authtoken;
    private String authtokenexpires;
    private List<ResultBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * googleId : teacherone@dataready.in
         * lastName : One
         * role : staff
         * gender : Female
         * emailId : teacherone@dataready.in
         * phoneNo : 98765432101
         * schoolProfilePic : /schoolapp/images/modernschool.jpg
         * password : testing
         * isDeleted :
         * countryCode : IN
         * schoolId : 129
         * staffPin : 172639
         * usingFaceBookId : true
         * schoolYear : 2017-2018
         * deviceTokenId : egaSLv3Ep68:APA91bHpIulCfErSUXpt-foj4CKRgDe_zq_e4Si5ml1g6qT_xvZghH_jTCtGSxbaAng5WPqvDj5vUHwJQZTPaCnCJM7cMlmbGE9UM5syhi69CiazKg8sAPg0Wihw7LL-g4GXyA14zvNc
         * id : teacherone@dataready.in
         * schoolName : Pankaj Modern School
         * profilePic : /schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png
         * schoolAddress : Barakhamba Road New Delhi New Delhi Delhi Postal code - 110001
         * faceBookId : teacherone@dataready.in
         * firstName : Teacher
         * createdDate : 2017-06-14
         * modifiedDate : 2017-12-26
         * routeDetails : {"dropoffRouteId":"10","dropoffStopId":"1","pickupRouteId":"9","dropoffRouteName":"SP10","name":"","stopId":"","pickupStopId":"1","id":"","uniqueId":"","pickupRouteName":"SP9"}
         * usingGoogleId : true
         * userType : teacher
         * schoolpermissions : {"parentmodules":{"route":true,"homework":true,"fee":true,"channel":true,"appointment":true,"communication":true,"version":"string","events":true,"attendance":true,"p2p":true},"teachermodules":{"homework":true,"t2t":true,"appointment":true,"communication":true,"version":"string","attendance":true},"adminmodules":{"route":true,"driver":true,"student":true,"fee":true,"channel":true,"appointment":true,"staff":true,"communication":true,"version":"string","events":true},"apps":{"parent":true,"teacher":true,"driver":true,"admin":true},"drivermodules":{"pickupdropoff":true,"qrscan":false,"fileupload":true,"video":false,"version":"basic"}}
         * userpermissions : {"teachermodules":{"homework":true,"t2t":true,"appointment":true,"login":true,"communication":true,"attendance":true},"adminmodules":{"route":true,"driver":true,"student":true,"fee":true,"channel":true,"appointment":true,"staff":true,"login":true,"communication":true,"events":true},"drivermodules":{"login":true}}
         */

        private String googleId;
        private String lastName;
        private String role;
        private String gender;
        private String emailId;
        private String phoneNo;
        private String schoolProfilePic;
        private String password;
        private String isDeleted;
        private String countryCode;
        private String schoolId;
        private int staffPin;
        private String usingFaceBookId;
        private String schoolYear;
        private String deviceTokenId;
        private String id;
        private String schoolName;
        private String profilePic;
        private String schoolAddress;
        private String faceBookId;
        private String firstName;
        private String createdDate;
        private String modifiedDate;
        private RouteDetailsBean routeDetails;
        private String usingGoogleId;
        private String userType;
        private String geopoint;
        private SchoolpermissionsBean schoolpermissions;
        private UserpermissionsBean userpermissions;


        public String getGeopoint() {
            return geopoint;
        }

        public void setGeopoint(String geopoint) {
            this.geopoint = geopoint;
        }

        public String getGoogleId() {
            return googleId;
        }

        public void setGoogleId(String googleId) {
            this.googleId = googleId;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getEmailId() {
            return emailId;
        }

        public void setEmailId(String emailId) {
            this.emailId = emailId;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public String getSchoolProfilePic() {
            return schoolProfilePic;
        }

        public void setSchoolProfilePic(String schoolProfilePic) {
            this.schoolProfilePic = schoolProfilePic;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(String isDeleted) {
            this.isDeleted = isDeleted;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public int getStaffPin() {
            return staffPin;
        }

        public void setStaffPin(int staffPin) {
            this.staffPin = staffPin;
        }

        public String getUsingFaceBookId() {
            return usingFaceBookId;
        }

        public void setUsingFaceBookId(String usingFaceBookId) {
            this.usingFaceBookId = usingFaceBookId;
        }

        public String getSchoolYear() {
            return schoolYear;
        }

        public void setSchoolYear(String schoolYear) {
            this.schoolYear = schoolYear;
        }

        public String getDeviceTokenId() {
            return deviceTokenId;
        }

        public void setDeviceTokenId(String deviceTokenId) {
            this.deviceTokenId = deviceTokenId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSchoolName() {
            return schoolName;
        }

        public void setSchoolName(String schoolName) {
            this.schoolName = schoolName;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        public String getSchoolAddress() {
            return schoolAddress;
        }

        public void setSchoolAddress(String schoolAddress) {
            this.schoolAddress = schoolAddress;
        }

        public String getFaceBookId() {
            return faceBookId;
        }

        public void setFaceBookId(String faceBookId) {
            this.faceBookId = faceBookId;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

        public String getModifiedDate() {
            return modifiedDate;
        }

        public void setModifiedDate(String modifiedDate) {
            this.modifiedDate = modifiedDate;
        }

        public RouteDetailsBean getRouteDetails() {
            return routeDetails;
        }

        public void setRouteDetails(RouteDetailsBean routeDetails) {
            this.routeDetails = routeDetails;
        }

        public String getUsingGoogleId() {
            return usingGoogleId;
        }

        public void setUsingGoogleId(String usingGoogleId) {
            this.usingGoogleId = usingGoogleId;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

        public SchoolpermissionsBean getSchoolpermissions() {
            return schoolpermissions;
        }

        public void setSchoolpermissions(SchoolpermissionsBean schoolpermissions) {
            this.schoolpermissions = schoolpermissions;
        }

        public UserpermissionsBean getUserpermissions() {
            return userpermissions;
        }

        public void setUserpermissions(UserpermissionsBean userpermissions) {
            this.userpermissions = userpermissions;
        }

        public static class RouteDetailsBean {
            /**
             * dropoffRouteId : 10
             * dropoffStopId : 1
             * pickupRouteId : 9
             * dropoffRouteName : SP10
             * name :
             * stopId :
             * pickupStopId : 1
             * id :
             * uniqueId :
             * pickupRouteName : SP9
             */

            private String dropoffRouteId;
            private String dropoffStopId;
            private String pickupRouteId;
            private String dropoffRouteName;
            private String name;
            private String stopId;
            private String pickupStopId;
            private String id;
            private String uniqueId;
            private String pickupRouteName;

            public String getDropoffRouteId() {
                return dropoffRouteId;
            }

            public void setDropoffRouteId(String dropoffRouteId) {
                this.dropoffRouteId = dropoffRouteId;
            }

            public String getDropoffStopId() {
                return dropoffStopId;
            }

            public void setDropoffStopId(String dropoffStopId) {
                this.dropoffStopId = dropoffStopId;
            }

            public String getPickupRouteId() {
                return pickupRouteId;
            }

            public void setPickupRouteId(String pickupRouteId) {
                this.pickupRouteId = pickupRouteId;
            }

            public String getDropoffRouteName() {
                return dropoffRouteName;
            }

            public void setDropoffRouteName(String dropoffRouteName) {
                this.dropoffRouteName = dropoffRouteName;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getStopId() {
                return stopId;
            }

            public void setStopId(String stopId) {
                this.stopId = stopId;
            }

            public String getPickupStopId() {
                return pickupStopId;
            }

            public void setPickupStopId(String pickupStopId) {
                this.pickupStopId = pickupStopId;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getUniqueId() {
                return uniqueId;
            }

            public void setUniqueId(String uniqueId) {
                this.uniqueId = uniqueId;
            }

            public String getPickupRouteName() {
                return pickupRouteName;
            }

            public void setPickupRouteName(String pickupRouteName) {
                this.pickupRouteName = pickupRouteName;
            }
        }

        public static class SchoolpermissionsBean {
            /**
             * parentmodules : {"route":true,"homework":true,"fee":true,"channel":true,"appointment":true,"communication":true,"version":"string","events":true,"attendance":true,"p2p":true}
             * teachermodules : {"homework":true,"t2t":true,"appointment":true,"communication":true,"version":"string","attendance":true}
             * adminmodules : {"route":true,"driver":true,"student":true,"fee":true,"channel":true,"appointment":true,"staff":true,"communication":true,"version":"string","events":true}
             * apps : {"parent":true,"teacher":true,"driver":true,"admin":true}
             * drivermodules : {"pickupdropoff":true,"qrscan":false,"fileupload":true,"video":false,"version":"basic"}
             */

            private ParentmodulesBean parentmodules;
            private TeachermodulesBean teachermodules;
            private AdminmodulesBean adminmodules;
            private AppsBean apps;
            private DrivermodulesBean drivermodules;

            public ParentmodulesBean getParentmodules() {
                return parentmodules;
            }

            public void setParentmodules(ParentmodulesBean parentmodules) {
                this.parentmodules = parentmodules;
            }

            public TeachermodulesBean getTeachermodules() {
                return teachermodules;
            }

            public void setTeachermodules(TeachermodulesBean teachermodules) {
                this.teachermodules = teachermodules;
            }

            public AdminmodulesBean getAdminmodules() {
                return adminmodules;
            }

            public void setAdminmodules(AdminmodulesBean adminmodules) {
                this.adminmodules = adminmodules;
            }

            public AppsBean getApps() {
                return apps;
            }

            public void setApps(AppsBean apps) {
                this.apps = apps;
            }

            public DrivermodulesBean getDrivermodules() {
                return drivermodules;
            }

            public void setDrivermodules(DrivermodulesBean drivermodules) {
                this.drivermodules = drivermodules;
            }

            public static class ParentmodulesBean {
                /**
                 * route : true
                 * homework : true
                 * fee : true
                 * channel : true
                 * appointment : true
                 * communication : true
                 * version : string
                 * events : true
                 * attendance : true
                 * p2p : true
                 */

                private boolean route;
                private boolean homework;
                private boolean fee;
                private boolean channel;
                private boolean appointment;
                private boolean communication;
                private String version;
                private boolean events;
                private boolean attendance;
                private boolean p2p;
                private String refreshFrequency;

                public String getRefreshFrequency() {
                    return refreshFrequency;
                }

                public void setRefreshFrequency(String refreshFrequency) {
                    this.refreshFrequency = refreshFrequency;
                }

                public boolean isRoute() {
                    return route;
                }

                public void setRoute(boolean route) {
                    this.route = route;
                }

                public boolean isHomework() {
                    return homework;
                }

                public void setHomework(boolean homework) {
                    this.homework = homework;
                }

                public boolean isFee() {
                    return fee;
                }

                public void setFee(boolean fee) {
                    this.fee = fee;
                }

                public boolean isChannel() {
                    return channel;
                }

                public void setChannel(boolean channel) {
                    this.channel = channel;
                }

                public boolean isAppointment() {
                    return appointment;
                }

                public void setAppointment(boolean appointment) {
                    this.appointment = appointment;
                }

                public boolean isCommunication() {
                    return communication;
                }

                public void setCommunication(boolean communication) {
                    this.communication = communication;
                }

                public String getVersion() {
                    return version;
                }

                public void setVersion(String version) {
                    this.version = version;
                }

                public boolean isEvents() {
                    return events;
                }

                public void setEvents(boolean events) {
                    this.events = events;
                }

                public boolean isAttendance() {
                    return attendance;
                }

                public void setAttendance(boolean attendance) {
                    this.attendance = attendance;
                }

                public boolean isP2p() {
                    return p2p;
                }

                public void setP2p(boolean p2p) {
                    this.p2p = p2p;
                }
            }

            public static class TeachermodulesBean {
                /**
                 * homework : true
                 * t2t : true
                 * appointment : true
                 * communication : true
                 * version : string
                 * attendance : true
                 */

                private boolean homework;
                private boolean t2t;
                private boolean appointment;
                private boolean communication;
                private String version;
                private boolean attendance;

                public boolean isHomework() {
                    return homework;
                }

                public void setHomework(boolean homework) {
                    this.homework = homework;
                }

                public boolean isT2t() {
                    return t2t;
                }

                public void setT2t(boolean t2t) {
                    this.t2t = t2t;
                }

                public boolean isAppointment() {
                    return appointment;
                }

                public void setAppointment(boolean appointment) {
                    this.appointment = appointment;
                }

                public boolean isCommunication() {
                    return communication;
                }

                public void setCommunication(boolean communication) {
                    this.communication = communication;
                }

                public String getVersion() {
                    return version;
                }

                public void setVersion(String version) {
                    this.version = version;
                }

                public boolean isAttendance() {
                    return attendance;
                }

                public void setAttendance(boolean attendance) {
                    this.attendance = attendance;
                }
            }

            public static class AdminmodulesBean {
                /**
                 * route : true
                 * driver : true
                 * student : true
                 * fee : true
                 * channel : true
                 * appointment : true
                 * staff : true
                 * communication : true
                 * version : string
                 * events : true
                 */

                private boolean route;
                private boolean driver;
                private boolean student;
                private boolean fee;
                private boolean channel;
                private boolean appointment;
                private boolean staff;
                private boolean communication;
                private String version;
                private boolean events;

                public boolean isRoute() {
                    return route;
                }

                public void setRoute(boolean route) {
                    this.route = route;
                }

                public boolean isDriver() {
                    return driver;
                }

                public void setDriver(boolean driver) {
                    this.driver = driver;
                }

                public boolean isStudent() {
                    return student;
                }

                public void setStudent(boolean student) {
                    this.student = student;
                }

                public boolean isFee() {
                    return fee;
                }

                public void setFee(boolean fee) {
                    this.fee = fee;
                }

                public boolean isChannel() {
                    return channel;
                }

                public void setChannel(boolean channel) {
                    this.channel = channel;
                }

                public boolean isAppointment() {
                    return appointment;
                }

                public void setAppointment(boolean appointment) {
                    this.appointment = appointment;
                }

                public boolean isStaff() {
                    return staff;
                }

                public void setStaff(boolean staff) {
                    this.staff = staff;
                }

                public boolean isCommunication() {
                    return communication;
                }

                public void setCommunication(boolean communication) {
                    this.communication = communication;
                }

                public String getVersion() {
                    return version;
                }

                public void setVersion(String version) {
                    this.version = version;
                }

                public boolean isEvents() {
                    return events;
                }

                public void setEvents(boolean events) {
                    this.events = events;
                }
            }

            public static class AppsBean {
                /**
                 * parent : true
                 * teacher : true
                 * driver : true
                 * admin : true
                 */

                private boolean parent;
                private boolean teacher;
                private boolean driver;
                private boolean admin;

                public boolean isParent() {
                    return parent;
                }

                public void setParent(boolean parent) {
                    this.parent = parent;
                }

                public boolean isTeacher() {
                    return teacher;
                }

                public void setTeacher(boolean teacher) {
                    this.teacher = teacher;
                }

                public boolean isDriver() {
                    return driver;
                }

                public void setDriver(boolean driver) {
                    this.driver = driver;
                }

                public boolean isAdmin() {
                    return admin;
                }

                public void setAdmin(boolean admin) {
                    this.admin = admin;
                }
            }

            public static class DrivermodulesBean {
                /**
                 * pickupdropoff : true
                 * qrscan : false
                 * fileupload : true
                 * video : false
                 * version : basic
                 */

                private boolean pickupdropoff;
                private boolean qrscan;
                private boolean fileupload;
                private boolean video;
                private String version;
                private String refreshFrequency;

                public String getRefreshFrequency() {
                    return refreshFrequency;
                }

                public void setRefreshFrequency(String refreshFrequency) {
                    this.refreshFrequency = refreshFrequency;
                }

                public boolean isPickupdropoff() {
                    return pickupdropoff;
                }

                public void setPickupdropoff(boolean pickupdropoff) {
                    this.pickupdropoff = pickupdropoff;
                }

                public boolean isQrscan() {
                    return qrscan;
                }

                public void setQrscan(boolean qrscan) {
                    this.qrscan = qrscan;
                }

                public boolean isFileupload() {
                    return fileupload;
                }

                public void setFileupload(boolean fileupload) {
                    this.fileupload = fileupload;
                }

                public boolean isVideo() {
                    return video;
                }

                public void setVideo(boolean video) {
                    this.video = video;
                }

                public String getVersion() {
                    return version;
                }

                public void setVersion(String version) {
                    this.version = version;
                }
            }
        }

        public static class UserpermissionsBean {
            /**
             * teachermodules : {"homework":true,"t2t":true,"appointment":true,"login":true,"communication":true,"attendance":true}
             * adminmodules : {"route":true,"driver":true,"student":true,"fee":true,"channel":true,"appointment":true,"staff":true,"login":true,"communication":true,"events":true}
             * drivermodules : {"login":true}
             */

            private TeachermodulesBean teachermodules;
            private AdminmodulesBean adminmodules;
            private DrivermodulesBean drivermodules;

            public TeachermodulesBean getTeachermodules() {
                return teachermodules;
            }

            public void setTeachermodules(TeachermodulesBean teachermodules) {
                this.teachermodules = teachermodules;
            }

            public AdminmodulesBean getAdminmodules() {
                return adminmodules;
            }

            public void setAdminmodules(AdminmodulesBean adminmodules) {
                this.adminmodules = adminmodules;
            }

            public DrivermodulesBean getDrivermodules() {
                return drivermodules;
            }

            public void setDrivermodules(DrivermodulesBean drivermodules) {
                this.drivermodules = drivermodules;
            }

            public static class TeachermodulesBean {
                /**
                 * homework : true
                 * t2t : true
                 * appointment : true
                 * login : true
                 * communication : true
                 * attendance : true
                 */

                private boolean homework;
                private boolean t2t;
                private boolean appointment;
                private boolean login;
                private boolean communication;
                private boolean attendance;

                public boolean isHomework() {
                    return homework;
                }

                public void setHomework(boolean homework) {
                    this.homework = homework;
                }

                public boolean isT2t() {
                    return t2t;
                }

                public void setT2t(boolean t2t) {
                    this.t2t = t2t;
                }

                public boolean isAppointment() {
                    return appointment;
                }

                public void setAppointment(boolean appointment) {
                    this.appointment = appointment;
                }

                public boolean isLogin() {
                    return login;
                }

                public void setLogin(boolean login) {
                    this.login = login;
                }

                public boolean isCommunication() {
                    return communication;
                }

                public void setCommunication(boolean communication) {
                    this.communication = communication;
                }

                public boolean isAttendance() {
                    return attendance;
                }

                public void setAttendance(boolean attendance) {
                    this.attendance = attendance;
                }
            }

            public static class AdminmodulesBean {
                /**
                 * route : true
                 * driver : true
                 * student : true
                 * fee : true
                 * channel : true
                 * appointment : true
                 * staff : true
                 * login : true
                 * communication : true
                 * events : true
                 */

                private boolean route;
                private boolean driver;
                private boolean student;
                private boolean fee;
                private boolean channel;
                private boolean appointment;
                private boolean staff;
                private boolean login;
                private boolean communication;
                private boolean events;

                public boolean isRoute() {
                    return route;
                }

                public void setRoute(boolean route) {
                    this.route = route;
                }

                public boolean isDriver() {
                    return driver;
                }

                public void setDriver(boolean driver) {
                    this.driver = driver;
                }

                public boolean isStudent() {
                    return student;
                }

                public void setStudent(boolean student) {
                    this.student = student;
                }

                public boolean isFee() {
                    return fee;
                }

                public void setFee(boolean fee) {
                    this.fee = fee;
                }

                public boolean isChannel() {
                    return channel;
                }

                public void setChannel(boolean channel) {
                    this.channel = channel;
                }

                public boolean isAppointment() {
                    return appointment;
                }

                public void setAppointment(boolean appointment) {
                    this.appointment = appointment;
                }

                public boolean isStaff() {
                    return staff;
                }

                public void setStaff(boolean staff) {
                    this.staff = staff;
                }

                public boolean isLogin() {
                    return login;
                }

                public void setLogin(boolean login) {
                    this.login = login;
                }

                public boolean isCommunication() {
                    return communication;
                }

                public void setCommunication(boolean communication) {
                    this.communication = communication;
                }

                public boolean isEvents() {
                    return events;
                }

                public void setEvents(boolean events) {
                    this.events = events;
                }
            }

            public static class DrivermodulesBean {
                /**
                 * login : true
                 */

                private boolean login;

                public boolean isLogin() {
                    return login;
                }

                public void setLogin(boolean login) {
                    this.login = login;
                }
            }
        }
    }
}
