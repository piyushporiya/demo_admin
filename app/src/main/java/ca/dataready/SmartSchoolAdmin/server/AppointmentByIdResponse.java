/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;


import java.util.List;

public class AppointmentByIdResponse {


    /**
     * message : Appointment details are below
     * status : true
     * authtoken : null
     * result : {"teacherMobileNo":"98765432101","gradeId":"3","teacherName":"Teacher One","studentList":["1214"],"creationDate":"2017-11-21 10:21:03","classId":"A","teacherId":"teacherone@dataready.in","appointmentTime":"10:00 - 11:00","appointmentId":"1291511259663929","teacherEmailId":"teacherone@dataready.in","appointmentTitle":"meet","schoolId":"129","schoolYear":"2017","from":"teacherone@dataready.in","appointmentDate":"2017-11-21","subjectName":"math","status":"initiated"}
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private Object authtoken;
    private ResultBean result;
    private Object authtokenexpires;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Object getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(Object authtoken) {
        this.authtoken = authtoken;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public Object getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(Object authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public static class ResultBean {
        /**
         * teacherMobileNo : 98765432101
         * gradeId : 3
         * teacherName : Teacher One
         * studentList : ["1214"]
         * creationDate : 2017-11-21 10:21:03
         * classId : A
         * teacherId : teacherone@dataready.in
         * appointmentTime : 10:00 - 11:00
         * appointmentId : 1291511259663929
         * teacherEmailId : teacherone@dataready.in
         * appointmentTitle : meet
         * schoolId : 129
         * schoolYear : 2017
         * from : teacherone@dataready.in
         * appointmentDate : 2017-11-21
         * subjectName : math
         * status : initiated
         */

        private String teacherMobileNo;
        private String gradeId;
        private String teacherName;
        private String creationDate;
        private String classId;
        private String teacherId;
        private String appointmentTime;
        private String appointmentId;
        private String teacherEmailId;
        private String appointmentTitle;
        private String schoolId;
        private String schoolYear;
        private String from;
        private String appointmentDate;
        private String subjectName;
        private String status;
        private List<String> studentList;
        private List<FilesBean> files;

        public List<FilesBean> getFiles() {
            return files;
        }

        public void setFiles(List<FilesBean> files) {
            this.files = files;
        }

        public String getTeacherMobileNo() {
            return teacherMobileNo;
        }

        public void setTeacherMobileNo(String teacherMobileNo) {
            this.teacherMobileNo = teacherMobileNo;
        }

        public String getGradeId() {
            return gradeId;
        }

        public void setGradeId(String gradeId) {
            this.gradeId = gradeId;
        }

        public String getTeacherName() {
            return teacherName;
        }

        public void setTeacherName(String teacherName) {
            this.teacherName = teacherName;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getTeacherId() {
            return teacherId;
        }

        public void setTeacherId(String teacherId) {
            this.teacherId = teacherId;
        }

        public String getAppointmentTime() {
            return appointmentTime;
        }

        public void setAppointmentTime(String appointmentTime) {
            this.appointmentTime = appointmentTime;
        }

        public String getAppointmentId() {
            return appointmentId;
        }

        public void setAppointmentId(String appointmentId) {
            this.appointmentId = appointmentId;
        }

        public String getTeacherEmailId() {
            return teacherEmailId;
        }

        public void setTeacherEmailId(String teacherEmailId) {
            this.teacherEmailId = teacherEmailId;
        }

        public String getAppointmentTitle() {
            return appointmentTitle;
        }

        public void setAppointmentTitle(String appointmentTitle) {
            this.appointmentTitle = appointmentTitle;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getSchoolYear() {
            return schoolYear;
        }

        public void setSchoolYear(String schoolYear) {
            this.schoolYear = schoolYear;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getAppointmentDate() {
            return appointmentDate;
        }

        public void setAppointmentDate(String appointmentDate) {
            this.appointmentDate = appointmentDate;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<String> getStudentList() {
            return studentList;
        }

        public void setStudentList(List<String> studentList) {
            this.studentList = studentList;
        }


        public static class FilesBean {
            /**
             * filePath : /schoolapp/images/d2f24343-306d-4e41-b7f8-91224bf27619.png
             */

            private String filePath;

            public String getFilePath() {
                return filePath;
            }

            public void setFilePath(String filePath) {
                this.filePath = filePath;
            }
        }
    }
}
