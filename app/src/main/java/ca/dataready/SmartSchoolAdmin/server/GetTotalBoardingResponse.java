/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by social_jaydeep on 13/09/17.
 */

public class GetTotalBoardingResponse implements Parcelable {


    /**
     * message : Details are below
     * status : true
     * authtoken : null
     * result : [{"schoolId":"129","cardId":"1290000006","studentId":"1219","firstName":"Vijay","lastName":"Tiwari","profilePic":"/schoolapp/images/1291219.jpg","gradeId":"3","classId":"A","routeId":"9","stopId":"2","intime":null,"outtime":null},{"schoolId":"129","cardId":"1290000007","studentId":"1220","firstName":"Rahul","lastName":"Singh","profilePic":"/schoolapp/images/1291220.jpg","gradeId":"3","classId":"A","routeId":"9","stopId":"2","intime":null,"outtime":null},{"schoolId":"129","cardId":"1290000004","studentId":"1217","firstName":"Ajay","lastName":"Dargan","profilePic":"/schoolapp/images/1291217.jpg","gradeId":"3","classId":"A","routeId":"9","stopId":"2","intime":null,"outtime":null},{"schoolId":"129","cardId":"1290000005","studentId":"1218","firstName":"Rajesh","lastName":"Agarwal","profilePic":"/schoolapp/images/1291218.jpg","gradeId":"3","classId":"A","routeId":"9","stopId":"2","intime":null,"outtime":null},{"schoolId":"129","cardId":"1290000002","studentId":"1215","firstName":"Himanshu","lastName":"Badola","profilePic":"/schoolapp/images/1291215.jpg","gradeId":"3","classId":"A","routeId":"9","stopId":"2","intime":null,"outtime":null},{"schoolId":"129","cardId":"1290000003","studentId":"1216","firstName":"David","lastName":"srdrpk","profilePic":"/schoolapp/images/1291216.jpg","gradeId":"3","classId":"A","routeId":"9","stopId":"2","intime":null,"outtime":null},{"schoolId":"129","cardId":"1290000001","studentId":"1214","firstName":"Ajeet","lastName":"Choubey","profilePic":"/schoolapp/images/1291214.jpg","gradeId":"3","classId":"A","routeId":"9","stopId":"2","intime":null,"outtime":null},{"schoolId":"129","cardId":"1290000010","studentId":"1223","firstName":"Brijesh","lastName":"Verma","profilePic":"/schoolapp/images/1291223.jpg","gradeId":"3","classId":"A","routeId":"9","stopId":"2","intime":null,"outtime":null},{"schoolId":"129","cardId":"1290000008","studentId":"1221","firstName":"Sahil","lastName":"Singh","profilePic":"/schoolapp/images/1291221.jpg","gradeId":"3","classId":"A","routeId":"9","stopId":"2","intime":null,"outtime":null},{"schoolId":"129","cardId":"1290000009","studentId":"1222","firstName":"Anoop","lastName":"Depuria","profilePic":"/schoolapp/images/1291222.jpg","gradeId":"3","classId":"A","routeId":"9","stopId":"2","intime":null,"outtime":null}]
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private String authtokenexpires;
    private int onBoardStudentCount = 0;
    private int remainingStudentCount;
    private ArrayList<ResultBean> result;

    public int getRemainingStudentCount() {
        return remainingStudentCount;
    }

    public void setRemainingStudentCount(int remainingStudentCount) {
        this.remainingStudentCount = remainingStudentCount;
    }

    public static Creator<GetTotalBoardingResponse> getCREATOR() {
        return CREATOR;
    }

    public int getOnBoardStudentCount() {
        return onBoardStudentCount;
    }

    public void setOnBoardStudentCount(int onBoardStudentCount) {
        this.onBoardStudentCount = onBoardStudentCount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public ArrayList<ResultBean> getResult() {
        return result;
    }

    public void setResult(ArrayList<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean implements Parcelable {
        /**
         * schoolId : 129
         * cardId : 1290000006
         * profileId: "1230",
         * profileType: "student"
         * firstName : Vijay
         * lastName : Tiwari
         * profilePic : /schoolapp/images/1291219.jpg
         * gradeId : 3
         * classId : A
         * routeId : 9
         * stopId : 2
         * intime : null
         * outtime : null
         */

        private String schoolId;
        private String cardId;
        private String profileId;
        private String firstName;
        private String lastName;
        private String profileType;
        private String profilePic;
        private String gradeId;
        private String classId;
        private String routeId;
        private String stopId;
        private String intime;
        private String outtime;
        boolean isPickedUp = false;
        boolean isDroppedOff = false;
        boolean isSelected = false;

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        public String getProfileType() {
            return profileType;
        }

        public void setProfileType(String profileType) {
            this.profileType = profileType;
        }

        public boolean isPickedUp() {
            return isPickedUp;
        }

        public void setIsPickedUp(boolean pickedUp) {
            isPickedUp = pickedUp;
        }

        public boolean isDroppedOff() {
            return isDroppedOff;
        }

        public void setIsDroppedOff(boolean droppedOff) {
            isDroppedOff = droppedOff;
        }

        public static Creator<ResultBean> getCREATOR() {
            return CREATOR;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getCardId() {
            return cardId;
        }

        public void setCardId(String cardId) {
            this.cardId = cardId;
        }

        public String getProfileId() {
            return profileId;
        }

        public void setProfileId(String studentId) {
            this.profileId = studentId;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        public String getGradeId() {
            return gradeId;
        }

        public void setGradeId(String gradeId) {
            this.gradeId = gradeId;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getRouteId() {
            return routeId;
        }

        public void setRouteId(String routeId) {
            this.routeId = routeId;
        }

        public String getStopId() {
            return stopId;
        }

        public void setStopId(String stopId) {
            this.stopId = stopId;
        }

        public String getIntime() {
            return intime;
        }

        public void setIntime(String intime) {
            this.intime = intime;
        }

        public String getOuttime() {
            return outtime;
        }

        public void setOuttime(String outtime) {
            this.outtime = outtime;
        }

        public ResultBean() {
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.schoolId);
            dest.writeString(this.cardId);
            dest.writeString(this.profileId);
            dest.writeString(this.firstName);
            dest.writeString(this.lastName);
            dest.writeString(this.profileType);
            dest.writeString(this.profilePic);
            dest.writeString(this.gradeId);
            dest.writeString(this.classId);
            dest.writeString(this.routeId);
            dest.writeString(this.stopId);
            dest.writeString(this.intime);
            dest.writeString(this.outtime);
            dest.writeByte(this.isPickedUp ? (byte) 1 : (byte) 0);
            dest.writeByte(this.isDroppedOff ? (byte) 1 : (byte) 0);
            dest.writeByte(this.isSelected ? (byte) 1 : (byte) 0);
        }

        protected ResultBean(Parcel in) {
            this.schoolId = in.readString();
            this.cardId = in.readString();
            this.profileId = in.readString();
            this.firstName = in.readString();
            this.lastName = in.readString();
            this.profileType = in.readString();
            this.profilePic = in.readString();
            this.gradeId = in.readString();
            this.classId = in.readString();
            this.routeId = in.readString();
            this.stopId = in.readString();
            this.intime = in.readString();
            this.outtime = in.readString();
            this.isPickedUp = in.readByte() != 0;
            this.isDroppedOff = in.readByte() != 0;
            this.isSelected = in.readByte() != 0;
        }

        public static final Creator<ResultBean> CREATOR = new Creator<ResultBean>() {
            @Override
            public ResultBean createFromParcel(Parcel source) {
                return new ResultBean(source);
            }

            @Override
            public ResultBean[] newArray(int size) {
                return new ResultBean[size];
            }
        };
    }

    public GetTotalBoardingResponse() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.message);
        dest.writeByte(this.status ? (byte) 1 : (byte) 0);
        dest.writeString(this.authtoken);
        dest.writeString(this.authtokenexpires);
        dest.writeInt(this.onBoardStudentCount);
        dest.writeInt(this.remainingStudentCount);
        dest.writeTypedList(this.result);
    }

    protected GetTotalBoardingResponse(Parcel in) {
        this.message = in.readString();
        this.status = in.readByte() != 0;
        this.authtoken = in.readString();
        this.authtokenexpires = in.readString();
        this.onBoardStudentCount = in.readInt();
        this.remainingStudentCount = in.readInt();
        this.result = in.createTypedArrayList(ResultBean.CREATOR);
    }

    public static final Creator<GetTotalBoardingResponse> CREATOR = new Creator<GetTotalBoardingResponse>() {
        @Override
        public GetTotalBoardingResponse createFromParcel(Parcel source) {
            return new GetTotalBoardingResponse(source);
        }

        @Override
        public GetTotalBoardingResponse[] newArray(int size) {
            return new GetTotalBoardingResponse[size];
        }
    };
}
