/*
 * Copyright (c) 2017.                                                       
 *                                                                           
 * ***********************************************************************  
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                           
 * ________________________________________________________________________  
 *                                                                           
 *    [2010] - [2018] SripathiSolutions Corp.                                
 *    [2010] - [2018] DataReady Technology Corp.                             
 *   ________________________________________________________________________
 *                                                                           
 *    All Rights Reserved.                                                   
 *                                                                           
 *   NOTICE:  All information contained herein is, and remains               
 *   the property of DataReady Technology  and its suppliers,                
 *   if any.  The intellectual and technical concepts contained              
 *   herein are proprietary to DataReady Technology Incorporated             
 *   and its suppliers and may be covered by U.S. and Foreign Patents,       
 *   patents in process, and are protected by trade secret or copyright law. 
 *   Dissemination of this information or reproduction of this material      
 *   is strictly forbidden unless prior written permission is obtained       
 *   from DataReady Technology Incorporated.                                 
 *                                                                           
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                            
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class ActiveRouteInfoResponse {


    /**
     * message : Details are below
     * status : true
     * authtoken : null
     * result : [{"endTripTime":"","tripType":"PICKUP","routeId":"9","driverId":"piyushporiya@techsoftlabs.com","startTripTime":"2018-04-04 10:20:45 UTC","schoolId":"129","name":"SP9-HOME","positionDetails":{"latitude":"21.2333083","currentDate":"2018-04-04","stopId":null,"tripId":"AWKQLE8AITmueQOZEGNx","speed":"0.00","currentTime":"2018-04-04 15:50:57","routeId":"9","driverId":"piyushporiya@techsoftlabs.com","name":"9","serverTime":"2018-04-04 10:20:57","uniqueId":"694092","direction":null,"longitude":"72.8643551"},"tripId":"AWKQLE8AITmueQOZEGNx","uniqueId":"694092","status":"active"}]
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private String authtokenexpires;
    private List<ResultBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean implements Parcelable {
        /**
         * endTripTime : 
         * tripType : PICKUP
         * routeId : 9
         * driverId : piyushporiya@techsoftlabs.com
         * startTripTime : 2018-04-04 10:20:45 UTC
         * schoolId : 129
         * name : SP9-HOME
         * positionDetails : {"latitude":"21.2333083","currentDate":"2018-04-04","stopId":null,"tripId":"AWKQLE8AITmueQOZEGNx","speed":"0.00","currentTime":"2018-04-04 15:50:57","routeId":"9","driverId":"piyushporiya@techsoftlabs.com","name":"9","serverTime":"2018-04-04 10:20:57","uniqueId":"694092","direction":null,"longitude":"72.8643551"}
         * tripId : AWKQLE8AITmueQOZEGNx
         * uniqueId : 694092
         * status : active
         */

        private String endTripTime;
        private String tripType;
        private String routeId;
        private String driverId;
        private String startTripTime;
        private String schoolId;
        private String name;
        private PositionDetailsBean positionDetails;
        private String tripId;
        private String uniqueId;
        private String status;

        public String getEndTripTime() {
            return endTripTime;
        }

        public void setEndTripTime(String endTripTime) {
            this.endTripTime = endTripTime;
        }

        public String getTripType() {
            return tripType;
        }

        public void setTripType(String tripType) {
            this.tripType = tripType;
        }

        public String getRouteId() {
            return routeId;
        }

        public void setRouteId(String routeId) {
            this.routeId = routeId;
        }

        public String getDriverId() {
            return driverId;
        }

        public void setDriverId(String driverId) {
            this.driverId = driverId;
        }

        public String getStartTripTime() {
            return startTripTime;
        }

        public void setStartTripTime(String startTripTime) {
            this.startTripTime = startTripTime;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public PositionDetailsBean getPositionDetails() {
            return positionDetails;
        }

        public void setPositionDetails(PositionDetailsBean positionDetails) {
            this.positionDetails = positionDetails;
        }

        public String getTripId() {
            return tripId;
        }

        public void setTripId(String tripId) {
            this.tripId = tripId;
        }

        public String getUniqueId() {
            return uniqueId;
        }

        public void setUniqueId(String uniqueId) {
            this.uniqueId = uniqueId;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public static class PositionDetailsBean implements Parcelable {
            /**
             * latitude : 21.2333083
             * currentDate : 2018-04-04
             * stopId : null
             * tripId : AWKQLE8AITmueQOZEGNx
             * speed : 0.00
             * currentTime : 2018-04-04 15:50:57
             * routeId : 9
             * driverId : piyushporiya@techsoftlabs.com
             * name : 9
             * serverTime : 2018-04-04 10:20:57
             * uniqueId : 694092
             * direction : null
             * longitude : 72.8643551
             */

            private String latitude;
            private String currentDate;
            private String stopId;
            private String tripId;
            private String speed;
            private String currentTime;
            private String routeId;
            private String driverId;
            private String name;
            private String serverTime;
            private String uniqueId;
            private String direction;
            private String longitude;

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getCurrentDate() {
                return currentDate;
            }

            public void setCurrentDate(String currentDate) {
                this.currentDate = currentDate;
            }

            public String getStopId() {
                return stopId;
            }

            public void setStopId(String stopId) {
                this.stopId = stopId;
            }

            public String getTripId() {
                return tripId;
            }

            public void setTripId(String tripId) {
                this.tripId = tripId;
            }

            public String getSpeed() {
                return speed;
            }

            public void setSpeed(String speed) {
                this.speed = speed;
            }

            public String getCurrentTime() {
                return currentTime;
            }

            public void setCurrentTime(String currentTime) {
                this.currentTime = currentTime;
            }

            public String getRouteId() {
                return routeId;
            }

            public void setRouteId(String routeId) {
                this.routeId = routeId;
            }

            public String getDriverId() {
                return driverId;
            }

            public void setDriverId(String driverId) {
                this.driverId = driverId;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getServerTime() {
                return serverTime;
            }

            public void setServerTime(String serverTime) {
                this.serverTime = serverTime;
            }

            public String getUniqueId() {
                return uniqueId;
            }

            public void setUniqueId(String uniqueId) {
                this.uniqueId = uniqueId;
            }

            public String getDirection() {
                return direction;
            }

            public void setDirection(String direction) {
                this.direction = direction;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.latitude);
                dest.writeString(this.currentDate);
                dest.writeString(this.stopId);
                dest.writeString(this.tripId);
                dest.writeString(this.speed);
                dest.writeString(this.currentTime);
                dest.writeString(this.routeId);
                dest.writeString(this.driverId);
                dest.writeString(this.name);
                dest.writeString(this.serverTime);
                dest.writeString(this.uniqueId);
                dest.writeString(this.direction);
                dest.writeString(this.longitude);
            }

            public PositionDetailsBean() {
            }

            protected PositionDetailsBean(Parcel in) {
                this.latitude = in.readString();
                this.currentDate = in.readString();
                this.stopId = in.readString();
                this.tripId = in.readString();
                this.speed = in.readString();
                this.currentTime = in.readString();
                this.routeId = in.readString();
                this.driverId = in.readString();
                this.name = in.readString();
                this.serverTime = in.readString();
                this.uniqueId = in.readString();
                this.direction = in.readString();
                this.longitude = in.readString();
            }

            public static final Creator<PositionDetailsBean> CREATOR = new Creator<PositionDetailsBean>() {
                @Override
                public PositionDetailsBean createFromParcel(Parcel source) {
                    return new PositionDetailsBean(source);
                }

                @Override
                public PositionDetailsBean[] newArray(int size) {
                    return new PositionDetailsBean[size];
                }
            };
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.endTripTime);
            dest.writeString(this.tripType);
            dest.writeString(this.routeId);
            dest.writeString(this.driverId);
            dest.writeString(this.startTripTime);
            dest.writeString(this.schoolId);
            dest.writeString(this.name);
            dest.writeParcelable(this.positionDetails, flags);
            dest.writeString(this.tripId);
            dest.writeString(this.uniqueId);
            dest.writeString(this.status);
        }

        public ResultBean() {
        }

        protected ResultBean(Parcel in) {
            this.endTripTime = in.readString();
            this.tripType = in.readString();
            this.routeId = in.readString();
            this.driverId = in.readString();
            this.startTripTime = in.readString();
            this.schoolId = in.readString();
            this.name = in.readString();
            this.positionDetails = in.readParcelable(PositionDetailsBean.class.getClassLoader());
            this.tripId = in.readString();
            this.uniqueId = in.readString();
            this.status = in.readString();
        }

        public static final Parcelable.Creator<ResultBean> CREATOR = new Parcelable.Creator<ResultBean>() {
            @Override
            public ResultBean createFromParcel(Parcel source) {
                return new ResultBean(source);
            }

            @Override
            public ResultBean[] newArray(int size) {
                return new ResultBean[size];
            }
        };
    }
}
