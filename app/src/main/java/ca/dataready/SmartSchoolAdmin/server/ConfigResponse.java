/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;

public class ConfigResponse {


    /**
     * message : Configuration information saved successfully
     * status : true
     * authtoken : null
     * result : {"id":"AWKEuDYaITmueQOZEGHs","appversion":"4","appbuildnumber":"1.3","ostype":"Android","osversion":"5.1","apptype":"admin","apiurl":"dev.api.dataready.us","apiversion":"1.2.0","region":"IN","env":"dev","deviceid":"87654421","devicevendor":"LG","language":"en","creationDate":null,"lastUpdatedDate":null}
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private ResultBean result;
    private String authtokenexpires;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public static class ResultBean {
        /**
         * id : AWKEuDYaITmueQOZEGHs
         * appversion : 4
         * appbuildnumber : 1.3
         * ostype : Android
         * osversion : 5.1
         * apptype : admin
         * apiurl : dev.api.dataready.us
         * apiversion : 1.2.0
         * region : IN
         * env : dev
         * deviceid : 87654421
         * devicevendor : LG
         * language : en
         * creationDate : null
         * lastUpdatedDate : null
         */

        private String id;
        private String appversion;
        private String appbuildnumber;
        private String ostype;
        private String osversion;
        private String apptype;
        private String apiurl;
        private String apiversion;
        private String region;
        private String env;
        private String deviceid;
        private String devicevendor;
        private String language;
        private String creationDate;
        private String lastUpdatedDate;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getAppversion() {
            return appversion;
        }

        public void setAppversion(String appversion) {
            this.appversion = appversion;
        }

        public String getAppbuildnumber() {
            return appbuildnumber;
        }

        public void setAppbuildnumber(String appbuildnumber) {
            this.appbuildnumber = appbuildnumber;
        }

        public String getOstype() {
            return ostype;
        }

        public void setOstype(String ostype) {
            this.ostype = ostype;
        }

        public String getOsversion() {
            return osversion;
        }

        public void setOsversion(String osversion) {
            this.osversion = osversion;
        }

        public String getApptype() {
            return apptype;
        }

        public void setApptype(String apptype) {
            this.apptype = apptype;
        }

        public String getApiurl() {
            return apiurl;
        }

        public void setApiurl(String apiurl) {
            this.apiurl = apiurl;
        }

        public String getApiversion() {
            return apiversion;
        }

        public void setApiversion(String apiversion) {
            this.apiversion = apiversion;
        }

        public String getRegion() {
            return region;
        }

        public void setRegion(String region) {
            this.region = region;
        }

        public String getEnv() {
            return env;
        }

        public void setEnv(String env) {
            this.env = env;
        }

        public String getDeviceid() {
            return deviceid;
        }

        public void setDeviceid(String deviceid) {
            this.deviceid = deviceid;
        }

        public String getDevicevendor() {
            return devicevendor;
        }

        public void setDevicevendor(String devicevendor) {
            this.devicevendor = devicevendor;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public String getLastUpdatedDate() {
            return lastUpdatedDate;
        }

        public void setLastUpdatedDate(String lastUpdatedDate) {
            this.lastUpdatedDate = lastUpdatedDate;
        }
    }
}
