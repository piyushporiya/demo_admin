/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;




import android.support.v4.app.Fragment;

import java.util.List;

public class DrawerModel {

    private String groupTitle;
    private List<SubItem> subList;
    private boolean isExpanded = false;
    private boolean hasMoreItems = true;

    public DrawerModel(){

    }

    public DrawerModel(String groupTitle, List<SubItem> subList) {
        this.groupTitle = groupTitle;
        this.subList = subList;
    }

    public boolean hasMoreItems() {
        return hasMoreItems;
    }

    public void setHasMoreItems(boolean hasMoreItems) {
        this.hasMoreItems = hasMoreItems;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }

    public List<SubItem> getSubList() {
        return subList;
    }

    public void setSubList(List<SubItem> subList) {
        this.subList = subList;
    }

    public String getGroupTitle() {

        return groupTitle;
    }

    public void setGroupTitle(String groupTitle) {
        this.groupTitle = groupTitle;
    }

    public static class SubItem {

        private String subItemTitle;
        private int subItemIcon;
        private Fragment fragment;
        private boolean isEnabled;

        public SubItem(String subItemTitle, int subItemIcon, Fragment fragment, boolean isEnabled) {
            this.subItemTitle = subItemTitle;
            this.subItemIcon = subItemIcon;
            this.fragment = fragment;
            this.isEnabled = isEnabled;
        }

        public boolean isEnabled() {
            return isEnabled;
        }

        public void setEnabled(boolean enabled) {
            isEnabled = enabled;
        }

        public Fragment getFragment() {
            return fragment;
        }

        public void setFragment(Fragment fragment) {
            this.fragment = fragment;
        }

        public String getSubItemTitle() {
            return subItemTitle;
        }

        public void setSubItemTitle(String subItemTitle) {
            this.subItemTitle = subItemTitle;
        }

        public int getSubItemIcon() {
            return subItemIcon;
        }

        public void setSubItemIcon(int subItemIcon) {
            this.subItemIcon = subItemIcon;
        }
    }
}
