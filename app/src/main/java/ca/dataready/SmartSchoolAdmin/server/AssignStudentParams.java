/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;

public class AssignStudentParams {


    /**
     * classId : string
     * dob : YYYY-MM-dd
     * emailId : string
     * firstName : string
     * gradeId : string
     * id : string
     * lastName : string
     * parentId : string
     * phoneNo : string
     * profilePic : string
     * routeDetails : {"dropoffRouteId":"string","dropoffRouteName":"string","dropoffStopId":"string","id":"string","name":"string","pickupRouteId":"string","pickupRouteName":"string","pickupStopId":"string","stopId":"string","uniqueId":"string"}
     * schoolId : string
     * schoolYear : YYYY-YYYY
     * studentId : string
     */

    private String classId;
    private String dob;
    private String emailId;
    private String firstName;
    private String gradeId;
    private String id;
    private String lastName;
    private String parentId;
    private String phoneNo;
    private String profilePic;
    private RouteDetailsBean routeDetails;
    private String schoolId;
    private String schoolYear;
    private String studentId;

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGradeId() {
        return gradeId;
    }

    public void setGradeId(String gradeId) {
        this.gradeId = gradeId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public RouteDetailsBean getRouteDetails() {
        return routeDetails;
    }

    public void setRouteDetails(RouteDetailsBean routeDetails) {
        this.routeDetails = routeDetails;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getSchoolYear() {
        return schoolYear;
    }

    public void setSchoolYear(String schoolYear) {
        this.schoolYear = schoolYear;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public static class RouteDetailsBean {
        /**
         * dropoffRouteId : string
         * dropoffRouteName : string
         * dropoffStopId : string
         * id : string
         * name : string
         * pickupRouteId : string
         * pickupRouteName : string
         * pickupStopId : string
         * stopId : string
         * uniqueId : string
         */

        private String dropoffRouteId;
        private String dropoffRouteName;
        private String dropoffStopId;
        private String id;
        private String name;
        private String pickupRouteId;
        private String pickupRouteName;
        private String pickupStopId;
        private String stopId;
        private String uniqueId;

        public String getDropoffRouteId() {
            return dropoffRouteId;
        }

        public void setDropoffRouteId(String dropoffRouteId) {
            this.dropoffRouteId = dropoffRouteId;
        }

        public String getDropoffRouteName() {
            return dropoffRouteName;
        }

        public void setDropoffRouteName(String dropoffRouteName) {
            this.dropoffRouteName = dropoffRouteName;
        }

        public String getDropoffStopId() {
            return dropoffStopId;
        }

        public void setDropoffStopId(String dropoffStopId) {
            this.dropoffStopId = dropoffStopId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPickupRouteId() {
            return pickupRouteId;
        }

        public void setPickupRouteId(String pickupRouteId) {
            this.pickupRouteId = pickupRouteId;
        }

        public String getPickupRouteName() {
            return pickupRouteName;
        }

        public void setPickupRouteName(String pickupRouteName) {
            this.pickupRouteName = pickupRouteName;
        }

        public String getPickupStopId() {
            return pickupStopId;
        }

        public void setPickupStopId(String pickupStopId) {
            this.pickupStopId = pickupStopId;
        }

        public String getStopId() {
            return stopId;
        }

        public void setStopId(String stopId) {
            this.stopId = stopId;
        }

        public String getUniqueId() {
            return uniqueId;
        }

        public void setUniqueId(String uniqueId) {
            this.uniqueId = uniqueId;
        }
    }
}
