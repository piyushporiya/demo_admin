/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;

import java.util.List;

public class UpdateSchoolInfoParams {


    /**
     * busStandInfo : {"address":"string","geopoint":"string"}
     * cityId : string
     * countryId : string
     * emailId : string
     * geopoint : string
     * phoneNo : string
     * profilePic : string
     * regionId : string
     * schoolAddress : {"addrLine1":"string","addrLine2":"string","addrLine3":"string","city":"string","pin":"string","state":"string"}
     * schoolId : string
     * schoolName : string
     * schoolTiming : {"endTime":"string","periodFrequency":"string","startTime":"string"}
     * stateId : string
     * termDetails : [{"termDesc":"string","termId":"string","termName":"string"}]
     */

    private BusStandInfoBean busStandInfo;
    private String cityId;
    private String countryId;
    private String emailId;
    private String geopoint;
    private String phoneNo;
    private String profilePic;
    private String regionId;
    private SchoolAddressBean schoolAddress;
    private String schoolId;
    private String schoolName;
    private SchoolTimingBean schoolTiming;
    private String stateId;
    private List<TermDetailsBean> termDetails;

    public BusStandInfoBean getBusStandInfo() {
        return busStandInfo;
    }

    public void setBusStandInfo(BusStandInfoBean busStandInfo) {
        this.busStandInfo = busStandInfo;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getGeopoint() {
        return geopoint;
    }

    public void setGeopoint(String geopoint) {
        this.geopoint = geopoint;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public SchoolAddressBean getSchoolAddress() {
        return schoolAddress;
    }

    public void setSchoolAddress(SchoolAddressBean schoolAddress) {
        this.schoolAddress = schoolAddress;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public SchoolTimingBean getSchoolTiming() {
        return schoolTiming;
    }

    public void setSchoolTiming(SchoolTimingBean schoolTiming) {
        this.schoolTiming = schoolTiming;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public List<TermDetailsBean> getTermDetails() {
        return termDetails;
    }

    public void setTermDetails(List<TermDetailsBean> termDetails) {
        this.termDetails = termDetails;
    }

    public static class BusStandInfoBean {
        /**
         * address : string
         * geopoint : string
         */

        private String address;
        private String geopoint;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getGeopoint() {
            return geopoint;
        }

        public void setGeopoint(String geopoint) {
            this.geopoint = geopoint;
        }
    }

    public static class SchoolAddressBean {
        /**
         * addrLine1 : string
         * addrLine2 : string
         * addrLine3 : string
         * city : string
         * pin : string
         * state : string
         */

        private String addrLine1;
        private String addrLine2;
        private String addrLine3;
        private String city;
        private String pin;
        private String state;

        public String getAddrLine1() {
            return addrLine1;
        }

        public void setAddrLine1(String addrLine1) {
            this.addrLine1 = addrLine1;
        }

        public String getAddrLine2() {
            return addrLine2;
        }

        public void setAddrLine2(String addrLine2) {
            this.addrLine2 = addrLine2;
        }

        public String getAddrLine3() {
            return addrLine3;
        }

        public void setAddrLine3(String addrLine3) {
            this.addrLine3 = addrLine3;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getPin() {
            return pin;
        }

        public void setPin(String pin) {
            this.pin = pin;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }
    }

    public static class SchoolTimingBean {
        /**
         * endTime : string
         * periodFrequency : string
         * startTime : string
         */

        private String endTime;
        private String periodFrequency;
        private String startTime;

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public String getPeriodFrequency() {
            return periodFrequency;
        }

        public void setPeriodFrequency(String periodFrequency) {
            this.periodFrequency = periodFrequency;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }
    }

    public static class TermDetailsBean {
        /**
         * termDesc : string
         * termId : string
         * termName : string
         */

        private String termDesc;
        private String termId;
        private String termName;

        public String getTermDesc() {
            return termDesc;
        }

        public void setTermDesc(String termDesc) {
            this.termDesc = termDesc;
        }

        public String getTermId() {
            return termId;
        }

        public void setTermId(String termId) {
            this.termId = termId;
        }

        public String getTermName() {
            return termName;
        }

        public void setTermName(String termName) {
            this.termName = termName;
        }
    }
}
