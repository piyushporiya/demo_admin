/*
 * Copyright (c) 2017.                                                       
 *                                                                           
 * ***********************************************************************  
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                           
 * ________________________________________________________________________  
 *                                                                           
 *    [2010] - [2018] SripathiSolutions Corp.                                
 *    [2010] - [2018] DataReady Technology Corp.                             
 *   ________________________________________________________________________
 *                                                                           
 *    All Rights Reserved.                                                   
 *                                                                           
 *   NOTICE:  All information contained herein is, and remains               
 *   the property of DataReady Technology  and its suppliers,                
 *   if any.  The intellectual and technical concepts contained              
 *   herein are proprietary to DataReady Technology Incorporated             
 *   and its suppliers and may be covered by U.S. and Foreign Patents,       
 *   patents in process, and are protected by trade secret or copyright law. 
 *   Dissemination of this information or reproduction of this material      
 *   is strictly forbidden unless prior written permission is obtained       
 *   from DataReady Technology Incorporated.                                 
 *                                                                           
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                            
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;

import java.util.List;

public class DeleteSubjectResponse {


    /**
     * message : Informations are below
     * status : true
     * authtoken : null
     * result : [{"gradeId":"1","classId":null,"isDeleted":false,"schoolId":"129","prerequisite":"This is subject biology.","id":"AWJrUlLTN0p0Sxykcixz","subjectType":"Mandatory","subjectCode":"BO","subjectName":"Biology"},{"gradeId":"1","classId":null,"isDeleted":false,"schoolId":"129","prerequisite":"This is subject history.","id":"AWJrfIJrN0p0Sxykcix-","subjectType":"Optional","subjectCode":"HO","subjectName":"History"},{"gradeId":"1","classId":"A","isDeleted":false,"schoolId":"129","prerequisite":"nothing","id":"AWJq7Kj6N0p0Sxykciw8","subjectType":"Mandatory","subjectCode":"Math","subjectName":"Math"},{"gradeId":"1","classId":null,"isDeleted":false,"schoolId":"129","prerequisite":"Science subject","id":"AWJrY0f7N0p0Sxykcix1","subjectType":"Optional","subjectCode":"SCI","subjectName":"Science"}]
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private String authtokenexpires;
    private List<ResultBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * gradeId : 1
         * classId : null
         * isDeleted : false
         * schoolId : 129
         * prerequisite : This is subject biology.
         * id : AWJrUlLTN0p0Sxykcixz
         * subjectType : Mandatory
         * subjectCode : BO
         * subjectName : Biology
         */

        private String gradeId;
        private String classId;
        private boolean isDeleted;
        private String schoolId;
        private String prerequisite;
        private String id;
        private String subjectType;
        private String subjectCode;
        private String subjectName;

        public String getGradeId() {
            return gradeId;
        }

        public void setGradeId(String gradeId) {
            this.gradeId = gradeId;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public boolean isIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(boolean isDeleted) {
            this.isDeleted = isDeleted;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getPrerequisite() {
            return prerequisite;
        }

        public void setPrerequisite(String prerequisite) {
            this.prerequisite = prerequisite;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSubjectType() {
            return subjectType;
        }

        public void setSubjectType(String subjectType) {
            this.subjectType = subjectType;
        }

        public String getSubjectCode() {
            return subjectCode;
        }

        public void setSubjectCode(String subjectCode) {
            this.subjectCode = subjectCode;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }
    }
}
