/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;

import java.util.List;

import ca.dataready.SmartSchoolAdmin.schoolevents.create.CreateSchoolEventActivity;

public class AddEventParams {


    /**
     * channelType : class
     * classId : A
     * files : [{"filePath":"/schoolapp/images/df2e59c68be6-4c38-9e4c-e885b98ba6a1.PNG"},{"filePath":"/schoolapp/images/7ca71a5b-a05a-4d8f-9256-675f91228cb8.png"}]
     * gradeId : 3
     * notificationDate : 2018-01-24
     * notificationEndDate : 2018-01-30
     * notificationEndTime : 04:30 PM
     * notificationId :
     * notificationLongMessage : Fouth Event
     * notificationShortMessage : With attachments
     * notificationStartTime : 03:30 PM
     * schoolId : 129
     * schoolYear : 2017-2018
     * staffEmailId : teachertwo@dataready.in
     * staffId : teachertwo@dataready.in
     * staffName : Teacher Two
     * staffProfilePic :
     * timePeriod : Every Day
     * toId : []
     */

    private String channelType;
    private String classId;
    private String gradeId;
    private String notificationDate;
    private String notificationEndDate;
    private String notificationEndTime;
    private String notificationId;
    private String notificationLongMessage;
    private String notificationShortMessage;
    private String notificationStartTime;
    private String schoolId;
    private String schoolYear;
    private String staffEmailId;
    private String staffId;
    private String staffName;
    private String staffProfilePic;
    private String timePeriod;
    private List<FilesBean> files;
    private List<?> toId;

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getGradeId() {
        return gradeId;
    }

    public void setGradeId(String gradeId) {
        this.gradeId = gradeId;
    }

    public String getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(String notificationDate) {
        this.notificationDate = notificationDate;
    }

    public String getNotificationEndDate() {
        return notificationEndDate;
    }

    public void setNotificationEndDate(String notificationEndDate) {
        this.notificationEndDate = notificationEndDate;
    }

    public String getNotificationEndTime() {
        return notificationEndTime;
    }

    public void setNotificationEndTime(String notificationEndTime) {
        this.notificationEndTime = notificationEndTime;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public String getNotificationLongMessage() {
        return notificationLongMessage;
    }

    public void setNotificationLongMessage(String notificationLongMessage) {
        this.notificationLongMessage = notificationLongMessage;
    }

    public String getNotificationShortMessage() {
        return notificationShortMessage;
    }

    public void setNotificationShortMessage(String notificationShortMessage) {
        this.notificationShortMessage = notificationShortMessage;
    }

    public String getNotificationStartTime() {
        return notificationStartTime;
    }

    public void setNotificationStartTime(String notificationStartTime) {
        this.notificationStartTime = notificationStartTime;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getSchoolYear() {
        return schoolYear;
    }

    public void setSchoolYear(String schoolYear) {
        this.schoolYear = schoolYear;
    }

    public String getStaffEmailId() {
        return staffEmailId;
    }

    public void setStaffEmailId(String staffEmailId) {
        this.staffEmailId = staffEmailId;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getStaffProfilePic() {
        return staffProfilePic;
    }

    public void setStaffProfilePic(String staffProfilePic) {
        this.staffProfilePic = staffProfilePic;
    }

    public String getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(String timePeriod) {
        this.timePeriod = timePeriod;
    }

    public List<FilesBean> getFiles() {
        return files;
    }

    public void setFiles(List<FilesBean> files) {
        this.files = files;
    }

    public List<?> getToId() {
        return toId;
    }

    public void setToId(List<?> toId) {
        this.toId = toId;
    }

    public static class FilesBean {
        /**
         * filePath : /schoolapp/images/df2e59c68be6-4c38-9e4c-e885b98ba6a1.PNG
         */

        private String filePath;

        public String getFilePath() {
            return filePath;
        }

        public void setFilePath(String filePath) {
            this.filePath = filePath;
        }
    }
}

