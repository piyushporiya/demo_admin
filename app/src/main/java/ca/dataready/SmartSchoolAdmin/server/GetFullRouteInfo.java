/*
 * Copyright (c) 2017.                                                       
 *                                                                           
 * ***********************************************************************  
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                           
 * ________________________________________________________________________  
 *                                                                           
 *    [2010] - [2018] SripathiSolutions Corp.                                
 *    [2010] - [2018] DataReady Technology Corp.                             
 *   ________________________________________________________________________
 *                                                                           
 *    All Rights Reserved.                                                   
 *                                                                           
 *   NOTICE:  All information contained herein is, and remains               
 *   the property of DataReady Technology  and its suppliers,                
 *   if any.  The intellectual and technical concepts contained              
 *   herein are proprietary to DataReady Technology Incorporated             
 *   and its suppliers and may be covered by U.S. and Foreign Patents,       
 *   patents in process, and are protected by trade secret or copyright law. 
 *   Dissemination of this information or reproduction of this material      
 *   is strictly forbidden unless prior written permission is obtained       
 *   from DataReady Technology Incorporated.                                 
 *                                                                           
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                            
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;

import java.util.List;

public class GetFullRouteInfo {


    /**
     * message : Route informations are below
     * status : true
     * authtoken : null
     * result : {"stopage":[{"isSchool":true,"address":"udhana","latitude":"21.1431","stopId":"1","sequenceId":"1","longitude":"72.8431"},{"isSchool":false,"address":"GIDC, 137, Dr KV Hagrenad Road, Beside C.N.G. Pump, Alfa Industry, Unit Estate, Pandesara, Udhna, Surat, Gujarat 394221, India","latitude":"21.1448","stopId":"2","sequenceId":"2","longitude":"72.8372"},{"isSchool":false,"address":"Ashoka Shopping Centre, Plot No. 197, Udhana - Sachin Hwy, Near S B I, Pandesara GIDC, Udhna, Surat, Gujarat 394221, India","latitude":"21.1424","stopId":"3","sequenceId":"3","longitude":"72.8489"}],"driverDetails":{"firstName":"Piyush","lastName":"Poriya","driverId":"piyushporiya@techsoftlabs.com","profilePic":"/schoolapp/images/43b6206f-3715-42cf-b3de-27a6b75bd3c4.jpg","emailId":"piyushporiya@techsoftlabs.com"},"routeId":"13568","studentDetails":[{"lastName":"mendapara","gradeId":"4","profilePic":"/schoolapp/images/81ad8e12-4b48-4a98-8825-09b72d6d96ef.png","emailId":"hereedd@gmail.com","parentId":"1","phoneNo":"9933445566","studentId":"201801","firstName":"milan","classId":"A","dob":"1998-05-31","schoolId":"129","schoolYear":"2017-2018","routeDetails":{"dropoffRouteId":"13568","dropoffStopId":"2","pickupRouteId":"12021612531","dropoffRouteName":"Udhana","pickupStopId":"2","pickupRouteName":"Pool 13"},"id":"2018011294A2017-2018"}],"schoolId":"129","name":"Udhana","id":"129135681","routeType":"Dropoff","uniqueId":"1","teacherDetails":[{"lastName":"asdf","role":null,"address":"asadsf","gender":"female","profilePic":"/schoolapp/images/5b88e499-066f-441f-ae15-fec31213cd3c.png","geopoint":"42.221,-73.8732","emailId":"asdf","phoneNo":"asdf","firstName":"jky","schoolId":"129","routeDetails":{"dropoffRouteId":"13568","dropoffStopId":"3","pickupRouteId":"12921","dropoffRouteName":"Udhana","pickupStopId":"2","pickupRouteName":"noida dataready"},"userType":"teacher","id":"asdf"}]}
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private ResultBean result;
    private String authtokenexpires;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public static class ResultBean {
        /**
         * stopage : [{"isSchool":true,"address":"udhana","latitude":"21.1431","stopId":"1","sequenceId":"1","longitude":"72.8431"},{"isSchool":false,"address":"GIDC, 137, Dr KV Hagrenad Road, Beside C.N.G. Pump, Alfa Industry, Unit Estate, Pandesara, Udhna, Surat, Gujarat 394221, India","latitude":"21.1448","stopId":"2","sequenceId":"2","longitude":"72.8372"},{"isSchool":false,"address":"Ashoka Shopping Centre, Plot No. 197, Udhana - Sachin Hwy, Near S B I, Pandesara GIDC, Udhna, Surat, Gujarat 394221, India","latitude":"21.1424","stopId":"3","sequenceId":"3","longitude":"72.8489"}]
         * driverDetails : {"firstName":"Piyush","lastName":"Poriya","driverId":"piyushporiya@techsoftlabs.com","profilePic":"/schoolapp/images/43b6206f-3715-42cf-b3de-27a6b75bd3c4.jpg","emailId":"piyushporiya@techsoftlabs.com"}
         * routeId : 13568
         * studentDetails : [{"lastName":"mendapara","gradeId":"4","profilePic":"/schoolapp/images/81ad8e12-4b48-4a98-8825-09b72d6d96ef.png","emailId":"hereedd@gmail.com","parentId":"1","phoneNo":"9933445566","studentId":"201801","firstName":"milan","classId":"A","dob":"1998-05-31","schoolId":"129","schoolYear":"2017-2018","routeDetails":{"dropoffRouteId":"13568","dropoffStopId":"2","pickupRouteId":"12021612531","dropoffRouteName":"Udhana","pickupStopId":"2","pickupRouteName":"Pool 13"},"id":"2018011294A2017-2018"}]
         * schoolId : 129
         * name : Udhana
         * id : 129135681
         * routeType : Dropoff
         * uniqueId : 1
         * teacherDetails : [{"lastName":"asdf","role":null,"address":"asadsf","gender":"female","profilePic":"/schoolapp/images/5b88e499-066f-441f-ae15-fec31213cd3c.png","geopoint":"42.221,-73.8732","emailId":"asdf","phoneNo":"asdf","firstName":"jky","schoolId":"129","routeDetails":{"dropoffRouteId":"13568","dropoffStopId":"3","pickupRouteId":"12921","dropoffRouteName":"Udhana","pickupStopId":"2","pickupRouteName":"noida dataready"},"userType":"teacher","id":"asdf"}]
         */

        private DriverDetailsBean driverDetails;
        private String routeId;
        private String schoolId;
        private String name;
        private String id;
        private String routeType;
        private String uniqueId;
        private List<StopageBean> stopage;
        private List<StudentDetailsBean> studentDetails;
        private List<TeacherDetailsBean> teacherDetails;

        public DriverDetailsBean getDriverDetails() {
            return driverDetails;
        }

        public void setDriverDetails(DriverDetailsBean driverDetails) {
            this.driverDetails = driverDetails;
        }

        public String getRouteId() {
            return routeId;
        }

        public void setRouteId(String routeId) {
            this.routeId = routeId;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getRouteType() {
            return routeType;
        }

        public void setRouteType(String routeType) {
            this.routeType = routeType;
        }

        public String getUniqueId() {
            return uniqueId;
        }

        public void setUniqueId(String uniqueId) {
            this.uniqueId = uniqueId;
        }

        public List<StopageBean> getStopage() {
            return stopage;
        }

        public void setStopage(List<StopageBean> stopage) {
            this.stopage = stopage;
        }

        public List<StudentDetailsBean> getStudentDetails() {
            return studentDetails;
        }

        public void setStudentDetails(List<StudentDetailsBean> studentDetails) {
            this.studentDetails = studentDetails;
        }

        public List<TeacherDetailsBean> getTeacherDetails() {
            return teacherDetails;
        }

        public void setTeacherDetails(List<TeacherDetailsBean> teacherDetails) {
            this.teacherDetails = teacherDetails;
        }

        public static class DriverDetailsBean {
            /**
             * firstName : Piyush
             * lastName : Poriya
             * driverId : piyushporiya@techsoftlabs.com
             * profilePic : /schoolapp/images/43b6206f-3715-42cf-b3de-27a6b75bd3c4.jpg
             * emailId : piyushporiya@techsoftlabs.com
             */

            private String firstName;
            private String lastName;
            private String driverId;
            private String profilePic;
            private String emailId;

            public String getFirstName() {
                return firstName;
            }

            public void setFirstName(String firstName) {
                this.firstName = firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public void setLastName(String lastName) {
                this.lastName = lastName;
            }

            public String getDriverId() {
                return driverId;
            }

            public void setDriverId(String driverId) {
                this.driverId = driverId;
            }

            public String getProfilePic() {
                return profilePic;
            }

            public void setProfilePic(String profilePic) {
                this.profilePic = profilePic;
            }

            public String getEmailId() {
                return emailId;
            }

            public void setEmailId(String emailId) {
                this.emailId = emailId;
            }
        }

        public static class StopageBean {
            /**
             * isSchool : true
             * address : udhana
             * latitude : 21.1431
             * stopId : 1
             * sequenceId : 1
             * longitude : 72.8431
             */

            private boolean isSchool;
            private String address;
            private String latitude;
            private String stopId;
            private String sequenceId;
            private String longitude;

            public boolean isIsSchool() {
                return isSchool;
            }

            public void setIsSchool(boolean isSchool) {
                this.isSchool = isSchool;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getStopId() {
                return stopId;
            }

            public void setStopId(String stopId) {
                this.stopId = stopId;
            }

            public String getSequenceId() {
                return sequenceId;
            }

            public void setSequenceId(String sequenceId) {
                this.sequenceId = sequenceId;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }
        }

        public static class StudentDetailsBean {
            /**
             * lastName : mendapara
             * gradeId : 4
             * profilePic : /schoolapp/images/81ad8e12-4b48-4a98-8825-09b72d6d96ef.png
             * emailId : hereedd@gmail.com
             * parentId : 1
             * phoneNo : 9933445566
             * studentId : 201801
             * firstName : milan
             * classId : A
             * dob : 1998-05-31
             * schoolId : 129
             * schoolYear : 2017-2018
             * routeDetails : {"dropoffRouteId":"13568","dropoffStopId":"2","pickupRouteId":"12021612531","dropoffRouteName":"Udhana","pickupStopId":"2","pickupRouteName":"Pool 13"}
             * id : 2018011294A2017-2018
             */

            private String lastName;
            private String gradeId;
            private String profilePic;
            private String emailId;
            private String parentId;
            private String phoneNo;
            private String studentId;
            private String firstName;
            private String classId;
            private String dob;
            private String schoolId;
            private String schoolYear;
            private RouteDetailsBean routeDetails;
            private String id;

            public String getLastName() {
                return lastName;
            }

            public void setLastName(String lastName) {
                this.lastName = lastName;
            }

            public String getGradeId() {
                return gradeId;
            }

            public void setGradeId(String gradeId) {
                this.gradeId = gradeId;
            }

            public String getProfilePic() {
                return profilePic;
            }

            public void setProfilePic(String profilePic) {
                this.profilePic = profilePic;
            }

            public String getEmailId() {
                return emailId;
            }

            public void setEmailId(String emailId) {
                this.emailId = emailId;
            }

            public String getParentId() {
                return parentId;
            }

            public void setParentId(String parentId) {
                this.parentId = parentId;
            }

            public String getPhoneNo() {
                return phoneNo;
            }

            public void setPhoneNo(String phoneNo) {
                this.phoneNo = phoneNo;
            }

            public String getStudentId() {
                return studentId;
            }

            public void setStudentId(String studentId) {
                this.studentId = studentId;
            }

            public String getFirstName() {
                return firstName;
            }

            public void setFirstName(String firstName) {
                this.firstName = firstName;
            }

            public String getClassId() {
                return classId;
            }

            public void setClassId(String classId) {
                this.classId = classId;
            }

            public String getDob() {
                return dob;
            }

            public void setDob(String dob) {
                this.dob = dob;
            }

            public String getSchoolId() {
                return schoolId;
            }

            public void setSchoolId(String schoolId) {
                this.schoolId = schoolId;
            }

            public String getSchoolYear() {
                return schoolYear;
            }

            public void setSchoolYear(String schoolYear) {
                this.schoolYear = schoolYear;
            }

            public RouteDetailsBean getRouteDetails() {
                return routeDetails;
            }

            public void setRouteDetails(RouteDetailsBean routeDetails) {
                this.routeDetails = routeDetails;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public static class RouteDetailsBean {
                /**
                 * dropoffRouteId : 13568
                 * dropoffStopId : 2
                 * pickupRouteId : 12021612531
                 * dropoffRouteName : Udhana
                 * pickupStopId : 2
                 * pickupRouteName : Pool 13
                 */

                private String dropoffRouteId;
                private String dropoffStopId;
                private String pickupRouteId;
                private String dropoffRouteName;
                private String pickupStopId;
                private String pickupRouteName;

                public String getDropoffRouteId() {
                    return dropoffRouteId;
                }

                public void setDropoffRouteId(String dropoffRouteId) {
                    this.dropoffRouteId = dropoffRouteId;
                }

                public String getDropoffStopId() {
                    return dropoffStopId;
                }

                public void setDropoffStopId(String dropoffStopId) {
                    this.dropoffStopId = dropoffStopId;
                }

                public String getPickupRouteId() {
                    return pickupRouteId;
                }

                public void setPickupRouteId(String pickupRouteId) {
                    this.pickupRouteId = pickupRouteId;
                }

                public String getDropoffRouteName() {
                    return dropoffRouteName;
                }

                public void setDropoffRouteName(String dropoffRouteName) {
                    this.dropoffRouteName = dropoffRouteName;
                }

                public String getPickupStopId() {
                    return pickupStopId;
                }

                public void setPickupStopId(String pickupStopId) {
                    this.pickupStopId = pickupStopId;
                }

                public String getPickupRouteName() {
                    return pickupRouteName;
                }

                public void setPickupRouteName(String pickupRouteName) {
                    this.pickupRouteName = pickupRouteName;
                }
            }
        }

        public static class TeacherDetailsBean {
            /**
             * lastName : asdf
             * role : null
             * address : asadsf
             * gender : female
             * profilePic : /schoolapp/images/5b88e499-066f-441f-ae15-fec31213cd3c.png
             * geopoint : 42.221,-73.8732
             * emailId : asdf
             * phoneNo : asdf
             * firstName : jky
             * schoolId : 129
             * routeDetails : {"dropoffRouteId":"13568","dropoffStopId":"3","pickupRouteId":"12921","dropoffRouteName":"Udhana","pickupStopId":"2","pickupRouteName":"noida dataready"}
             * userType : teacher
             * id : asdf
             */

            private String lastName;
            private String role;
            private String address;
            private String gender;
            private String profilePic;
            private String geopoint;
            private String emailId;
            private String phoneNo;
            private String firstName;
            private String schoolId;
            private RouteDetailsBeanX routeDetails;
            private String userType;
            private String id;

            public String getLastName() {
                return lastName;
            }

            public void setLastName(String lastName) {
                this.lastName = lastName;
            }

            public String getRole() {
                return role;
            }

            public void setRole(String role) {
                this.role = role;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getProfilePic() {
                return profilePic;
            }

            public void setProfilePic(String profilePic) {
                this.profilePic = profilePic;
            }

            public String getGeopoint() {
                return geopoint;
            }

            public void setGeopoint(String geopoint) {
                this.geopoint = geopoint;
            }

            public String getEmailId() {
                return emailId;
            }

            public void setEmailId(String emailId) {
                this.emailId = emailId;
            }

            public String getPhoneNo() {
                return phoneNo;
            }

            public void setPhoneNo(String phoneNo) {
                this.phoneNo = phoneNo;
            }

            public String getFirstName() {
                return firstName;
            }

            public void setFirstName(String firstName) {
                this.firstName = firstName;
            }

            public String getSchoolId() {
                return schoolId;
            }

            public void setSchoolId(String schoolId) {
                this.schoolId = schoolId;
            }

            public RouteDetailsBeanX getRouteDetails() {
                return routeDetails;
            }

            public void setRouteDetails(RouteDetailsBeanX routeDetails) {
                this.routeDetails = routeDetails;
            }

            public String getUserType() {
                return userType;
            }

            public void setUserType(String userType) {
                this.userType = userType;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public static class RouteDetailsBeanX {
                /**
                 * dropoffRouteId : 13568
                 * dropoffStopId : 3
                 * pickupRouteId : 12921
                 * dropoffRouteName : Udhana
                 * pickupStopId : 2
                 * pickupRouteName : noida dataready
                 */

                private String dropoffRouteId;
                private String dropoffStopId;
                private String pickupRouteId;
                private String dropoffRouteName;
                private String pickupStopId;
                private String pickupRouteName;

                public String getDropoffRouteId() {
                    return dropoffRouteId;
                }

                public void setDropoffRouteId(String dropoffRouteId) {
                    this.dropoffRouteId = dropoffRouteId;
                }

                public String getDropoffStopId() {
                    return dropoffStopId;
                }

                public void setDropoffStopId(String dropoffStopId) {
                    this.dropoffStopId = dropoffStopId;
                }

                public String getPickupRouteId() {
                    return pickupRouteId;
                }

                public void setPickupRouteId(String pickupRouteId) {
                    this.pickupRouteId = pickupRouteId;
                }

                public String getDropoffRouteName() {
                    return dropoffRouteName;
                }

                public void setDropoffRouteName(String dropoffRouteName) {
                    this.dropoffRouteName = dropoffRouteName;
                }

                public String getPickupStopId() {
                    return pickupStopId;
                }

                public void setPickupStopId(String pickupStopId) {
                    this.pickupStopId = pickupStopId;
                }

                public String getPickupRouteName() {
                    return pickupRouteName;
                }

                public void setPickupRouteName(String pickupRouteName) {
                    this.pickupRouteName = pickupRouteName;
                }
            }
        }
    }
}
