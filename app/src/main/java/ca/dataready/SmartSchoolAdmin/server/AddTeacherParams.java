/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;


import java.util.List;

public class AddTeacherParams {

    /**
     * address : string
     * countryCode : string
     * department : string
     * dob : string
     * emailId : string
     * emergencyDetails : {"address":"string","dob":"string","education":"string","emailId":"string","files":[{"fileName":"string","filePath":"string"}],"firstName":"string","lastName":"string","occupation":"string","phoneNo":"string","relation":"string"}
     * experienceDetails : {"experiences":[{"address":"string","fromYear":"string","organizationName":"string","toYear":"string"}],"files":[{"fileName":"string","filePath":"string"}]}
     * files : [{"fileName":"string","filePath":"string"}]
     * firstName : string
     * gender : string
     * geopoint : string
     * id : string
     * joiningDate : string
     * lastName : string
     * maritalStatus : string
     * phoneNo : string
     * position : string
     * profilePic : string
     * qualificationDetails : {"files":[{"fileName":"string","filePath":"string"}],"qualifications":[{"passedYear":"string","qualificationName":"string","university":"string"}]}
     * role : string
     * routeDetails : {"dropoffRouteId":"string","dropoffRouteName":"string","dropoffStopId":"string","id":"string","name":"string","pickupRouteId":"string","pickupRouteName":"string","pickupStopId":"string","stopId":"string","uniqueId":"string"}
     * schoolId : string
     * staffPin : string
     * userType : string
     */

    private String address;
    private String countryCode;
    private String department;
    private String dob;
    private String emailId;
    private EmergencyDetailsBean emergencyDetails;
    private ExperienceDetailsBean experienceDetails;
    private String firstName;
    private String gender;
    private String geopoint;
    private String id;
    private String joiningDate;
    private String lastName;
    private String maritalStatus;
    private String phoneNo;
    private String position;
    private String profilePic;
    private String pickPoint;
    private QualificationDetailsBean qualificationDetails;
    private String role;
    private RouteDetailsBean routeDetails;
    private String schoolId;
    private String staffPin;
    private String userType;
    private List<FilesBeanXXX> files;
    private NotesBean notes;
    private PhysicianDetailsBean physicianDetails;
    private String requiresTransportation;

    public String getPickPoint() {
        return pickPoint;
    }

    public void setPickPoint(String pickPoint) {
        this.pickPoint = pickPoint;
    }

    public NotesBean getNotes() {
        return notes;
    }

    public void setNotes(NotesBean notes) {
        this.notes = notes;
    }

    public PhysicianDetailsBean getPhysicianDetails() {
        return physicianDetails;
    }

    public void setPhysicianDetails(PhysicianDetailsBean physicianDetails) {
        this.physicianDetails = physicianDetails;
    }

    public String getRequiresTransportation() {
        return requiresTransportation;
    }

    public void setRequiresTransportation(String requiresTransportation) {
        this.requiresTransportation = requiresTransportation;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public EmergencyDetailsBean getEmergencyDetails() {
        return emergencyDetails;
    }

    public void setEmergencyDetails(EmergencyDetailsBean emergencyDetails) {
        this.emergencyDetails = emergencyDetails;
    }

    public ExperienceDetailsBean getExperienceDetails() {
        return experienceDetails;
    }

    public void setExperienceDetails(ExperienceDetailsBean experienceDetails) {
        this.experienceDetails = experienceDetails;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGeopoint() {
        return geopoint;
    }

    public void setGeopoint(String geopoint) {
        this.geopoint = geopoint;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(String joiningDate) {
        this.joiningDate = joiningDate;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public QualificationDetailsBean getQualificationDetails() {
        return qualificationDetails;
    }

    public void setQualificationDetails(QualificationDetailsBean qualificationDetails) {
        this.qualificationDetails = qualificationDetails;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public RouteDetailsBean getRouteDetails() {
        return routeDetails;
    }

    public void setRouteDetails(RouteDetailsBean routeDetails) {
        this.routeDetails = routeDetails;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getStaffPin() {
        return staffPin;
    }

    public void setStaffPin(String staffPin) {
        this.staffPin = staffPin;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public List<FilesBeanXXX> getFiles() {
        return files;
    }

    public void setFiles(List<FilesBeanXXX> files) {
        this.files = files;
    }

    public static class EmergencyDetailsBean {
        /**
         * address : string
         * dob : string
         * education : string
         * emailId : string
         * files : [{"fileName":"string","filePath":"string"}]
         * firstName : string
         * lastName : string
         * occupation : string
         * phoneNo : string
         * relation : string
         */

        private String address;
        private String dob;
        private String education;
        private String emailId;
        private String firstName;
        private String lastName;
        private String occupation;
        private String phoneNo;
        private String relation;
        private List<FilesBean> files;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getEducation() {
            return education;
        }

        public void setEducation(String education) {
            this.education = education;
        }

        public String getEmailId() {
            return emailId;
        }

        public void setEmailId(String emailId) {
            this.emailId = emailId;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getOccupation() {
            return occupation;
        }

        public void setOccupation(String occupation) {
            this.occupation = occupation;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public String getRelation() {
            return relation;
        }

        public void setRelation(String relation) {
            this.relation = relation;
        }

        public List<FilesBean> getFiles() {
            return files;
        }

        public void setFiles(List<FilesBean> files) {
            this.files = files;
        }

        public static class FilesBean {
            /**
             * fileName : string
             * filePath : string
             */

            private String fileName;
            private String filePath;

            public String getFileName() {
                return fileName;
            }

            public void setFileName(String fileName) {
                this.fileName = fileName;
            }

            public String getFilePath() {
                return filePath;
            }

            public void setFilePath(String filePath) {
                this.filePath = filePath;
            }
        }
    }

    public static class ExperienceDetailsBean {
        private List<ExperiencesBean> experiences;
        private List<FilesBeanX> files;

        public List<ExperiencesBean> getExperiences() {
            return experiences;
        }

        public void setExperiences(List<ExperiencesBean> experiences) {
            this.experiences = experiences;
        }

        public List<FilesBeanX> getFiles() {
            return files;
        }

        public void setFiles(List<FilesBeanX> files) {
            this.files = files;
        }

        public static class ExperiencesBean {
            /**
             * address : string
             * fromYear : string
             * organizationName : string
             * toYear : string
             */

            private String address;
            private String fromYear;
            private String organizationName;
            private String toYear;

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getFromYear() {
                return fromYear;
            }

            public void setFromYear(String fromYear) {
                this.fromYear = fromYear;
            }

            public String getOrganizationName() {
                return organizationName;
            }

            public void setOrganizationName(String organizationName) {
                this.organizationName = organizationName;
            }

            public String getToYear() {
                return toYear;
            }

            public void setToYear(String toYear) {
                this.toYear = toYear;
            }
        }

        public static class FilesBeanX {
            /**
             * fileName : string
             * filePath : string
             */

            private String fileName;
            private String filePath;

            public String getFileName() {
                return fileName;
            }

            public void setFileName(String fileName) {
                this.fileName = fileName;
            }

            public String getFilePath() {
                return filePath;
            }

            public void setFilePath(String filePath) {
                this.filePath = filePath;
            }
        }
    }

    public static class QualificationDetailsBean {
        private List<FilesBeanXX> files;
        private List<QualificationsBean> qualifications;

        public List<FilesBeanXX> getFiles() {
            return files;
        }

        public void setFiles(List<FilesBeanXX> files) {
            this.files = files;
        }

        public List<QualificationsBean> getQualifications() {
            return qualifications;
        }

        public void setQualifications(List<QualificationsBean> qualifications) {
            this.qualifications = qualifications;
        }

        public static class FilesBeanXX {
            /**
             * fileName : string
             * filePath : string
             */

            private String fileName;
            private String filePath;

            public String getFileName() {
                return fileName;
            }

            public void setFileName(String fileName) {
                this.fileName = fileName;
            }

            public String getFilePath() {
                return filePath;
            }

            public void setFilePath(String filePath) {
                this.filePath = filePath;
            }
        }

        public static class QualificationsBean {
            /**
             * passedYear : string
             * qualificationName : string
             * university : string
             */

            private String passedYear;
            private String qualificationName;
            private String university;

            public String getPassedYear() {
                return passedYear;
            }

            public void setPassedYear(String passedYear) {
                this.passedYear = passedYear;
            }

            public String getQualificationName() {
                return qualificationName;
            }

            public void setQualificationName(String qualificationName) {
                this.qualificationName = qualificationName;
            }

            public String getUniversity() {
                return university;
            }

            public void setUniversity(String university) {
                this.university = university;
            }
        }
    }

    public static class RouteDetailsBean {
        /**
         * dropoffRouteId : string
         * dropoffRouteName : string
         * dropoffStopId : string
         * id : string
         * name : string
         * pickupRouteId : string
         * pickupRouteName : string
         * pickupStopId : string
         * stopId : string
         * uniqueId : string
         */

        private String dropoffRouteId;
        private String dropoffRouteName;
        private String dropoffStopId;
        private String id;
        private String name;
        private String pickupRouteId;
        private String pickupRouteName;
        private String pickupStopId;
        private String stopId;
        private String uniqueId;

        public String getDropoffRouteId() {
            return dropoffRouteId;
        }

        public void setDropoffRouteId(String dropoffRouteId) {
            this.dropoffRouteId = dropoffRouteId;
        }

        public String getDropoffRouteName() {
            return dropoffRouteName;
        }

        public void setDropoffRouteName(String dropoffRouteName) {
            this.dropoffRouteName = dropoffRouteName;
        }

        public String getDropoffStopId() {
            return dropoffStopId;
        }

        public void setDropoffStopId(String dropoffStopId) {
            this.dropoffStopId = dropoffStopId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPickupRouteId() {
            return pickupRouteId;
        }

        public void setPickupRouteId(String pickupRouteId) {
            this.pickupRouteId = pickupRouteId;
        }

        public String getPickupRouteName() {
            return pickupRouteName;
        }

        public void setPickupRouteName(String pickupRouteName) {
            this.pickupRouteName = pickupRouteName;
        }

        public String getPickupStopId() {
            return pickupStopId;
        }

        public void setPickupStopId(String pickupStopId) {
            this.pickupStopId = pickupStopId;
        }

        public String getStopId() {
            return stopId;
        }

        public void setStopId(String stopId) {
            this.stopId = stopId;
        }

        public String getUniqueId() {
            return uniqueId;
        }

        public void setUniqueId(String uniqueId) {
            this.uniqueId = uniqueId;
        }
    }

    public static class FilesBeanXXX {
        /**
         * fileName : string
         * filePath : string
         */

        private String fileName;
        private String filePath;

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public String getFilePath() {
            return filePath;
        }

        public void setFilePath(String filePath) {
            this.filePath = filePath;
        }
    }

    public static class NotesBean {
        /**
         * schoolCounselor : string
         * transportationCounselor : string
         */

        private String schoolCounselor;
        private String transportationCounselor;

        public String getSchoolCounselor() {
            return schoolCounselor;
        }

        public void setSchoolCounselor(String schoolCounselor) {
            this.schoolCounselor = schoolCounselor;
        }

        public String getTransportationCounselor() {
            return transportationCounselor;
        }

        public void setTransportationCounselor(String transportationCounselor) {
            this.transportationCounselor = transportationCounselor;
        }
    }

    public static class PhysicianDetailsBean {
        /**
         * address : string
         * city : string
         * country : string
         * email : string
         * fax : string
         * firstName : string
         * lastName : string
         * phone : string
         * state : string
         */

        private String address;
        private String city;
        private String country;
        private String email;
        private String fax;
        private String firstName;
        private String lastName;
        private String phone;
        private String state;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getFax() {
            return fax;
        }

        public void setFax(String fax) {
            this.fax = fax;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }
    }

}
