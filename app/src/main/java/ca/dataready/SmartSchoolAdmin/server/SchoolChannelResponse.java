/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class SchoolChannelResponse {


    /**
     * message : Message informations are below
     * status : true
     * authtoken : null
     * result : [{"toId":[],"gradeId":"3","notificationShortMessage":"Hello Ajeet","staffEmailId":"teacherone@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-02-20 09:06:33","classId":"B","notificationLongMessage":"How doing","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1519117593507","staffId":"teacherone@dataready.in","notificationDate":"2018-02-20"},{"toId":[],"gradeId":"3","notificationShortMessage":"https://dataready.atlassian.net/projects/SMARTEACHE/issues/SMARTEACHE-103?filter=myopenissueshttps://dataready.atlassian.net/projects/SMARTEACHE/issues/SMARTEACHE-103?filter=myopenissueshttps://dataready.atlassian.net/projects/SMARTEACHE/issues/SMARTEACHE-103?filter=myopenissueshttps://dataready.atlassian.net/projects/SMARTEACHE/issues/SMARTEACHE-103?filter=myopenissues","staffEmailId":"teachertwo@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-02-20 09:34:40","classId":"A","notificationLongMessage":"ll","staffProfilePic":"/schoolapp/images/a0737f29-8d59-4f1d-bc62-67952ce341a2.png","schoolId":"129","staffName":"Teacher two","schoolYear":"2017-2018","notificationId":"1519119280891","staffId":"teachertwo@dataready.in","notificationDate":"2018-02-20"},{"toId":[],"gradeId":"","notificationShortMessage":"android","staffEmailId":"teacherone@dataready.in","channelType":"school","notificationType":"notification","creationDate":"2018-02-20 06:32:53","classId":"","notificationLongMessage":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1519108373144","staffId":"teacherone@dataready.in","notificationDate":"2018-02-20"},{"toId":[],"gradeId":"3","notificationShortMessage":"Dargen is it you","staffEmailId":"teacherone@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-02-20 09:07:35","classId":"A","notificationLongMessage":"Ok ADargrn","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1519117655615","staffId":"teacherone@dataready.in","notificationDate":"2018-02-20"},{"toId":[],"gradeId":"3","notificationShortMessage":"Ajeet you on","staffEmailId":"teacherone@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-02-20 09:11:04","classId":"B","notificationLongMessage":"Ajeet","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1519117864314","staffId":"teacherone@dataready.in","notificationDate":"2018-02-20"},{"toId":[],"gradeId":"3","notificationShortMessage":"Hodor. Hodor hodor, hodor. Hodor hodor hodor hodor hodor. Hodor. Hodor! Hodor hodor, hodor; hodor hodor hodor. Hodor. Hodor hodor; hodor hodor - hodor, hodor, hodor hodor. Hodor, hodor. H Hodor. Hodor hodor, hodor. Hodor hodor hodor hodor hodor. Hodor. Hodor! Hodor hodor, hodor; hodor hodor hodor. Hodor. Hodor hodor; hodor hodor - hodor, hodor, hodor hodor. Hodor, hodor. Hodor. Hodor, hodor hodor hodor; hodor hodor; hodor hodor hodor! Hodor hodor HODOR! Hodor hodor... Hodor hodor hodor...k odor. Hodor, hodor hodor hodor; hodor hodor; hodor hodor hodor! Hodor hodor HODOR! Hodor hodor... Hodor hodor hodor...k","staffEmailId":"teachertwo@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-02-18 20:35:12","classId":"B","notificationLongMessage":"Hodor. Hodor hodor, hodor. Hodor hodor hodor hodor hodor. Hodor. Hodor! Hodor hodor, hodor; hodor hodor hodor. Hodor. Hodor hodor; hodor hodor - hodor, hodor, hodor hodor. Hodor, hodor. Hodor. Hodor, hodor hodor hodor; hodor hodor; hodor hodor hodor! Hodor hodor HODOR! Hodor hodor... Hodor hodor hodor...k Hodor. Hodor hodor, hodor. Hodor hodor hodor hodor hodor. Hodor. Hodor! Hodor hodor, hodor; hodor hodor hodor. Hodor. Hodor hodor; hodor hodor - hodor, hodor, hodor hodor. Hodor, hodor. Hodor. Hodor, hodor hodor hodor; hodor hodor; hodor hodor hodor! Hodor hodor HODOR! Hodor hodor... Hodor hodor hodor...k","staffProfilePic":"/schoolapp/images/a0737f29-8d59-4f1d-bc62-67952ce341a2.png","schoolId":"129","staffName":"Teacher two","schoolYear":"2017-2018","notificationId":"1518986112219","staffId":"teachertwo@dataready.in","notificationDate":"2018-02-19"},{"toId":[],"gradeId":"3","notificationShortMessage":"dads as","staffEmailId":"teacherone@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-02-19 10:23:05","classId":"A","notificationLongMessage":"Fdsgt","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1519035785798","staffId":"teacherone@dataready.in","notificationDate":"2018-02-19"},{"toId":[],"gradeId":"","notificationShortMessage":"ssddvm,,nk","staffEmailId":"teacherone@dataready.in","channelType":"school","notificationType":"notification","creationDate":"2018-02-18 20:08:29","classId":"","notificationLongMessage":"Sfjkl","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1518984509421","staffId":"teacherone@dataready.in","notificationDate":"2018-02-19"},{"toId":[],"gradeId":"3","notificationShortMessage":"hhhjhhhh","staffEmailId":"teachertwo@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-02-19 11:38:10","classId":"A","notificationLongMessage":"Ghujk,l","staffProfilePic":"/schoolapp/images/a0737f29-8d59-4f1d-bc62-67952ce341a2.png","schoolId":"129","staffName":"Teacher two","schoolYear":"2017-2018","notificationId":"1519040290737","staffId":"teachertwo@dataready.in","notificationDate":"2018-02-19"},{"toId":[],"gradeId":"3","notificationShortMessage":"jk","staffEmailId":"teachertwo@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-02-19 11:38:55","classId":"C","notificationLongMessage":"Jk","staffProfilePic":"/schoolapp/images/a0737f29-8d59-4f1d-bc62-67952ce341a2.png","schoolId":"129","staffName":"Teacher two","schoolYear":"2017-2018","notificationId":"1519040335659","staffId":"teachertwo@dataready.in","notificationDate":"2018-02-19"},{"toId":[],"gradeId":"","notificationShortMessage":",dd,d,,dd,k","staffEmailId":"teacherone@dataready.in","channelType":"school","notificationType":"notification","creationDate":"2018-02-18 20:07:39","classId":"","notificationLongMessage":"Hello","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1518984459141","staffId":"teacherone@dataready.in","notificationDate":"2018-02-19"},{"toId":[],"gradeId":"","notificationShortMessage":"Hodor. Hodor hodor, hodor. Hodor hodor hodor hodor hodor. Hodor. Hodor! Hodor hodor, hodor; hodor hodor hodor. Hodor. Hodor hodor; hodor hodor - hodor, hodor, hodor hodor. Hodor, hodor. Hodor. Hodor, hodor hodor hodor; hodor hodor; hodor hodor hodor! Hodor hodor HODOR! Hodor hodor... Hodor hodor hodor...k","staffEmailId":"teacherone@dataready.in","channelType":"school","notificationType":"notification","creationDate":"2018-02-18 20:37:16","classId":"","notificationLongMessage":"Hodor. Hodor hodor, hodor. Hodor hodor hodor hodor hodor. Hodor. Hodor! Hodor hodor, hodor; hodor hodor hodor. Hodor. Hodor hodor; hodor hodor - hodor, hodor, hodor hodor. Hodor, hodor. Hodor. Hodor, hodor hodor hodor; hodor hodor; hodor hodor hodor! Hodor hodor HODOR! Hodor hodor... Hodor hodor hodor...k Hodor. Hodor hodor, hodor. Hodor hodor hodor hodor hodor. Hodor. Hodor! Hodor hodor, hodor; hodor hodor hodor. Hodor. Hodor hodor; hodor hodor - hodor, hodor, hodor hodor. Hodor, hodor. Hodor. Hodor, hodor hodor hodor; hodor hodor; hodor hodor hodor! Hodor hodor HODOR! Hodor hodor... Hodor hodor hodor...k","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1518986236549","staffId":"teacherone@dataready.in","notificationDate":"2018-02-19"},{"toId":[],"gradeId":"3","notificationShortMessage":"hhhjhhhh","staffEmailId":"teachertwo@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-02-19 11:38:14","classId":"A","notificationLongMessage":"Ghujk,l","staffProfilePic":"/schoolapp/images/a0737f29-8d59-4f1d-bc62-67952ce341a2.png","schoolId":"129","staffName":"Teacher two","schoolYear":"2017-2018","notificationId":"1519040294098","staffId":"teachertwo@dataready.in","notificationDate":"2018-02-19"},{"toId":[],"gradeId":"3","notificationShortMessage":"njkikjhh","staffEmailId":"teachertwo@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-02-19 11:38:29","classId":"B","notificationLongMessage":"Hhikjhjil","staffProfilePic":"/schoolapp/images/a0737f29-8d59-4f1d-bc62-67952ce341a2.png","schoolId":"129","staffName":"Teacher two","schoolYear":"2017-2018","notificationId":"1519040309161","staffId":"teachertwo@dataready.in","notificationDate":"2018-02-19"},{"toId":[],"gradeId":"3","notificationShortMessage":"Hodor. Hodor hodor, hodor. Hodor hodor hodor hodor hodor. Hodor. Hodor! Hodor hodor, hodor; hodor hodor hodor. Hodor. Hodor hodor; hodor hodor - hodor, hodor, hodor hodor. Hodor, hodor. Hodor. Hodor, hodor hodor hodor; hodor hodor; hodor hodor hodor! Hodor hodor HODOR! Hodor hodor... Hodor hodor hodor...k","staffEmailId":"teacherthree@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-02-18 06:08:56","classId":"A","notificationLongMessage":"Hodor. Hodor hodor, hodor. Hodor hodor hodor hodor hodor. Hodor. Hodor! Hodor hodor, hodor; hodor hodor hodor. Hodor. Hodor hodor; hodor hodor - hodor, hodor, hodor hodor. Hodor, hodor. Hodor. Hodor, hodor hodor hodor; hodor hodor; hodor hodor hodor! Hodor hodor HODOR! Hodor hodor... Hodor hodor hodor...k\nHodor. Hodor hodor, hodor. Hodor hodor hodor hodor hodor. Hodor. Hodor! Hodor hodor, hodor; hodor hodor hodor. Hodor. Hodor hodor; hodor hodor - hodor, hodor, hodor hodor. Hodor, hodor. Hodor. Hodor, hodor hodor hodor; hodor hodor; hodor hodor hodor! Hodor hodor HODOR! Hodor hodor... Hodor hodor hodor...k","staffProfilePic":"/schoolapp/images/de6e3b40-492f-42f5-9850-19573bb71a52.png","schoolId":"129","staffName":"Teacher three","schoolYear":"2017-2018","files":[{"filePath":"/schoolapp/images/077b106b-6331-4bf8-8e8f-f497043f17bb.jpg"}],"notificationId":"1518934136974","staffId":"teacherthree@dataready.in","notificationDate":"2018-02-18"},{"toId":[],"gradeId":"3","notificationShortMessage":"12 was a great night out and then we got a lot to say to him so we had a great day at the end and he said that we had to go back and he was going through a ","staffEmailId":"teacherone@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-02-13 05:09:00","classId":"A","notificationLongMessage":"I\u2019m just going home now I\u2019ll see if I can come get it and then go to my car then go get my truck I will get you some money and I can pick you guys around if I get a ride to work I can ","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1518498540699","staffId":"teacherone@dataready.in","notificationDate":"2018-02-13"},{"toId":[],"gradeId":"3","notificationShortMessage":"Entire Class Have a Picknic","staffEmailId":"teacherone@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-02-10 06:09:34","classId":"B","notificationLongMessage":"You all are come in this trip","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1518242974377","staffId":"teacherone@dataready.in","notificationDate":"2018-02-10"},{"toId":[],"gradeId":"","notificationShortMessage":"This is for whole school","staffEmailId":"teacherone@dataready.in","channelType":"school","notificationType":"notification","creationDate":"2018-02-10 06:07:35","classId":"","notificationLongMessage":"Hello School we have holiday tomorrow","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1518242855909","staffId":"teacherone@dataready.in","notificationDate":"2018-02-10"},{"toId":[],"gradeId":"","notificationShortMessage":"hi school ","staffEmailId":"teacherone@dataready.in","channelType":"school","notificationType":"notification","creationDate":"2018-02-09 12:47:17","classId":"","notificationLongMessage":"Hi school","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1518180437723","staffId":"teacherone@dataready.in","notificationDate":"2018-02-09"},{"toId":[],"gradeId":"3","notificationShortMessage":"gg","staffEmailId":"teacherone@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-01-29 07:37:43","classId":"B","notificationLongMessage":"gty","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1517211463746","staffId":"teacherone@dataready.in","notificationDate":"2018-01-29"},{"toId":[],"gradeId":"3","notificationShortMessage":"gg","staffEmailId":"teacherone@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-01-29 07:37:43","classId":"B","notificationLongMessage":"gty","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1517211463375","staffId":"teacherone@dataready.in","notificationDate":"2018-01-29"},{"toId":[],"gradeId":"3","notificationShortMessage":"hello","staffEmailId":"teacherone@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-01-29 07:51:18","classId":"B","notificationLongMessage":"I have the first to comment","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1517212278701","staffId":"teacherone@dataready.in","notificationDate":"2018-01-29"},{"toId":[],"gradeId":"3","notificationShortMessage":"rr","staffEmailId":"teacherone@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-01-29 07:43:38","classId":"B","notificationLongMessage":"the needful","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1517211818823","staffId":"teacherone@dataready.in","notificationDate":"2018-01-29"},{"toId":[],"gradeId":"3","notificationShortMessage":"dmskks","staffEmailId":"teacherthree@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-01-27 11:47:20","classId":"B","notificationLongMessage":"ksksksksk","staffProfilePic":"/schoolapp/images/de6e3b40-492f-42f5-9850-19573bb71a52.png","schoolId":"129","staffName":"Teacher three","schoolYear":"2017-2018","notificationId":"1517053640658","staffId":"teacherthree@dataready.in","notificationDate":"2018-01-27"},{"toId":[],"gradeId":"3","notificationShortMessage":"hi","staffEmailId":"teacherone@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-01-27 09:29:48","classId":"A","notificationLongMessage":"1","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1517045388338","staffId":"teacherone@dataready.in","notificationDate":"2018-01-27"},{"toId":[],"gradeId":"","notificationShortMessage":"hhhhh","staffEmailId":"teacherone@dataready.in","channelType":"school","notificationType":"notification","creationDate":"2018-01-27 09:36:09","classId":"","notificationLongMessage":"Rrrr\n","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1517045769700","staffId":"teacherone@dataready.in","notificationDate":"2018-01-27"},{"toId":[],"gradeId":"","notificationShortMessage":"hi","staffEmailId":"teacherone@dataready.in","channelType":"school","notificationType":"notification","creationDate":"2018-01-27 09:35:44","classId":"","notificationLongMessage":"A","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1517045744212","staffId":"teacherone@dataready.in","notificationDate":"2018-01-27"},{"toId":[],"gradeId":"3","notificationShortMessage":"sad","staffEmailId":"teacherone@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-01-26 12:07:03","classId":"B","notificationLongMessage":"Reeeeee","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1516968423720","staffId":"teacherone@dataready.in","notificationDate":"2018-01-26"},{"toId":[],"gradeId":"3","notificationShortMessage":"hi t0","staffEmailId":"teachertwo@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-01-25 11:10:36","classId":"C","notificationLongMessage":"teacher two","staffProfilePic":"/schoolapp/images/a0737f29-8d59-4f1d-bc62-67952ce341a2.png","schoolId":"129","staffName":"Teacher two","schoolYear":"2017-2018","files":[{"filePath":"/schoolapp/images/8c8f2e39-0c45-48ac-8cb0-38eaf370f72d.png"}],"notificationId":"1516854235272","staffId":"teachertwo@dataready.in","notificationDate":"2018-01-25"},{"toId":[],"gradeId":"3","notificationShortMessage":"hey class.    ====. Is all home work done ,,,c  IIIIIIIIIIIIIan we go for further assignment,lets check tomorrow for all class then only i will assign homework further IIIIIIIIII","staffEmailId":"teacherthree@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-01-27 11:05:20","classId":"B","notificationLongMessage":"Hows things going dndjddkkdkdmdmdjdjdkdd djdjdkdkdkdjd djdjdkdkdkd djdjdkdkjdkd djdjdkdkd djdjdkdkd djdjdkkdd djdkdkdkd djdkdkkd djdkdkddk djdkdkdd djdjkdd djdkdkdk","staffProfilePic":"/schoolapp/images/de6e3b40-492f-42f5-9850-19573bb71a52.png","schoolId":"129","staffName":"Teacher three","schoolYear":"2017-2018","files":[],"notificationId":"1516862027782","staffId":"teacherthree@dataready.in","notificationDate":"2018-01-25"},{"toId":[],"gradeId":"3","notificationShortMessage":"tttttt tt t tttgggg","staffEmailId":"teacherthree@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-01-25 08:23:51","classId":"B","notificationLongMessage":"Nvhnhvnhvn hhhhhh h h\nMgmg\nmmmmmmm\nnnnnnnnnnn\njjjjjjjjjjj","staffProfilePic":"/schoolapp/images/de6e3b40-492f-42f5-9850-19573bb71a52.png","schoolId":"129","staffName":"Teacher three","schoolYear":"2017-2018","files":null,"notificationId":"1516862571960","staffId":"teacherthree@dataready.in","notificationDate":"2018-01-25"},{"toId":[],"gradeId":"3","notificationShortMessage":"dggdtgrhtr","staffEmailId":"teacherthree@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-01-25 06:40:29","classId":"A","notificationLongMessage":"Hikngh","staffProfilePic":"/schoolapp/images/de6e3b40-492f-42f5-9850-19573bb71a52.png","schoolId":"129","staffName":"Teacher three","schoolYear":"2017-2018","notificationId":"1516862429592","staffId":"teacherthree@dataready.in","notificationDate":"2018-01-25"},{"toId":[],"gradeId":"3","notificationShortMessage":"hhhhhh hhhhhhh hhhhhhh hhhhhhhhh","staffEmailId":"teacherthree@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-01-25 08:29:43","classId":"C","notificationLongMessage":"rrrrrrr tttttttt \nhhhhhh \n\njjjjjjjjjj bbbbbb bbbbb\n","staffProfilePic":"/schoolapp/images/de6e3b40-492f-42f5-9850-19573bb71a52.png","schoolId":"129","staffName":"Teacher three","schoolYear":"2017-2018","files":[{"filePath":"/schoolapp/images/9a346d8f-1308-4741-8b5d-c42a2510ddab.jpg"},{"filePath":"/schoolapp/images/d1a189ea-9f35-4947-b7e5-31a090660a69.jpg"},{"filePath":"/schoolapp/images/43f3ce09-63f2-40da-934d-6396208ac0b5.jpg"},{"filePath":"/schoolapp/images/6228058b-aaa7-48f9-bb08-c0e1a6411a57.docx"},{"filePath":"/schoolapp/images/423e1e8c-0b44-4d4b-812d-85803aeb11b6.jpg"}],"notificationId":"1516868699188","staffId":"teacherthree@dataready.in","notificationDate":"2018-01-25"},{"toId":[],"gradeId":"3","notificationShortMessage":"fffffffffffff ffv","staffEmailId":"teacherthree@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-01-25 08:22:29","classId":"A","notificationLongMessage":"gffgffg\nrfbfjfyng\n","staffProfilePic":"/schoolapp/images/de6e3b40-492f-42f5-9850-19573bb71a52.png","schoolId":"129","staffName":"Teacher three","schoolYear":"2017-2018","files":[{"filePath":"/schoolapp/images/1683b009-1572-403b-abe4-9f344427b76f.jpg"}],"notificationId":"1516868549171","staffId":"teacherthree@dataready.in","notificationDate":"2018-01-25"},{"toId":[],"gradeId":"","notificationShortMessage":"new","staffEmailId":"teacherone@dataready.in","channelType":"school","notificationType":"notification","creationDate":"2018-01-24 12:52:32","classId":"","notificationLongMessage":"Hi","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1516798352662","staffId":"teacherone@dataready.in","notificationDate":"2018-01-24"},{"toId":[],"gradeId":"3","notificationShortMessage":"I","staffEmailId":"teachertwo@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-01-22 06:12:48","classId":"B","notificationLongMessage":"the the the needful and send it for you to do the needful as soon","staffProfilePic":"/schoolapp/images/a0737f29-8d59-4f1d-bc62-67952ce341a2.png","schoolId":"129","staffName":"Teacher two","schoolYear":"2017-2018","notificationId":"1516601568879","staffId":"teachertwo@dataready.in","notificationDate":"2018-01-22"},{"toId":[],"gradeId":"","notificationShortMessage":"hi","staffEmailId":"teacherone@dataready.in","channelType":"school","notificationType":"notification","creationDate":"2018-01-22 11:13:21","classId":"","notificationLongMessage":"Ok","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1516619601770","staffId":"teacherone@dataready.in","notificationDate":"2018-01-22"},{"toId":[],"gradeId":"3","notificationShortMessage":"the","staffEmailId":"teachertwo@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-01-22 06:13:23","classId":"B","notificationLongMessage":"the the needful as early","staffProfilePic":"/schoolapp/images/a0737f29-8d59-4f1d-bc62-67952ce341a2.png","schoolId":"129","staffName":"Teacher two","schoolYear":"2017-2018","notificationId":"1516601603221","staffId":"teachertwo@dataready.in","notificationDate":"2018-01-22"},{"toId":[],"gradeId":"3","notificationShortMessage":"hello","staffEmailId":"teachertwo@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-01-22 06:12:16","classId":"B","notificationLongMessage":"hello math and and and at the needful and send it to you have any","staffProfilePic":"/schoolapp/images/a0737f29-8d59-4f1d-bc62-67952ce341a2.png","schoolId":"129","staffName":"Teacher two","schoolYear":"2017-2018","notificationId":"1516601536142","staffId":"teachertwo@dataready.in","notificationDate":"2018-01-22"},{"toId":[],"gradeId":"3","notificationShortMessage":"uoi","staffEmailId":"teacherone@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-01-21 17:08:40","classId":"A","notificationLongMessage":"Uoi","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1516554520136","staffId":"teacherone@dataready.in","notificationDate":"2018-01-21"},{"toId":[],"gradeId":"","notificationShortMessage":"hey ","staffEmailId":"teacherone@dataready.in","channelType":"school","notificationType":"notification","creationDate":"2018-01-21 17:07:28","classId":"","notificationLongMessage":"Hello","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1516554448819","staffId":"teacherone@dataready.in","notificationDate":"2018-01-21"},{"toId":[],"gradeId":"","notificationShortMessage":"hi use ","staffEmailId":"teacherone@dataready.in","channelType":"school","notificationType":"notification","creationDate":"2018-01-20 06:41:44","classId":"","notificationLongMessage":"Hi use ","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1516430504631","staffId":"teacherone@dataready.in","notificationDate":"2018-01-20"},{"toId":[],"gradeId":"3","notificationShortMessage":"this is start","staffEmailId":"teacherone@dataready.in","channelType":"class","notificationType":"notification","creationDate":"2018-01-20 06:53:40","classId":"C","notificationLongMessage":"Lets msg","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1516431220312","staffId":"teacherone@dataready.in","notificationDate":"2018-01-20"},{"toId":[],"gradeId":"","notificationShortMessage":"hjj","staffEmailId":"teacherone@dataready.in","channelType":"school","notificationType":"notification","creationDate":"2018-01-20 06:42:45","classId":"","notificationLongMessage":"Hi that's it","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1516430565707","staffId":"teacherone@dataready.in","notificationDate":"2018-01-20"},{"toId":[],"gradeId":"","notificationShortMessage":"hi","staffEmailId":"teacherone@dataready.in","channelType":"school","notificationType":"notification","creationDate":"2018-01-20 06:44:01","classId":"","notificationLongMessage":"Hi \nD\nHey d\nD\nD\nD\nD\nD\nD\nD\nD\n","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","files":[{"filePath":"/schoolapp/images/1c0b1663-7fc8-40fa-aef3-2ba6dbf01a48.PNG"},{"filePath":"/schoolapp/images/b894b663-c62f-4784-b38c-3cbec63c49cd.PNG"},{"filePath":"/schoolapp/images/23f02554-f76c-447e-ae51-09c8919f1613.PNG"},{"filePath":"/schoolapp/images/71b0751a-4525-423e-9b43-3c96df64dc39.PNG"}],"notificationId":"1516430641948","staffId":"teacherone@dataready.in","notificationDate":"2018-01-20"},{"toId":[],"gradeId":"","notificationShortMessage":"gsjsfsnmsks","staffEmailId":"teacherone@dataready.in","channelType":"school","notificationType":"notification","creationDate":"2018-01-20 06:43:10","classId":"","notificationLongMessage":"Hsjssgsh","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","files":[{"filePath":"/schoolapp/images/4a43e403-f00b-4a24-986c-9f49043d05a3.PNG"}],"notificationId":"1516430590060","staffId":"teacherone@dataready.in","notificationDate":"2018-01-20"},{"toId":[],"gradeId":"","notificationShortMessage":"testing of notification","staffEmailId":"teacherone@dataready.in","channelType":"school","notificationType":"notification","creationDate":"2018-01-19 13:05:28","classId":"","notificationLongMessage":"Validation","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017-2018","notificationId":"1516367128571","staffId":"teacherone@dataready.in","notificationDate":"2018-01-19"}]
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private String authtokenexpires;
    private ArrayList<ResultBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public ArrayList<ResultBean> getResult() {
        return result;
    }

    public void setResult(ArrayList<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean implements Parcelable {
        /**
         * toId : []
         * gradeId : 3
         * notificationShortMessage : Hello Ajeet
         * staffEmailId : teacherone@dataready.in
         * channelType : class
         * notificationType : notification
         * creationDate : 2018-02-20 09:06:33
         * classId : B
         * notificationLongMessage : How doing
         * staffProfilePic : /schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png
         * schoolId : 129
         * staffName : Teacher One
         * schoolYear : 2017-2018
         * notificationId : 1519117593507
         * staffId : teacherone@dataready.in
         * notificationDate : 2018-02-20
         * files : [{"filePath":"/schoolapp/images/077b106b-6331-4bf8-8e8f-f497043f17bb.jpg"}]
         */

        private String gradeId;
        private String notificationShortMessage;
        private String staffEmailId;
        private String channelType;
        private String notificationType;
        private String creationDate;
        private String classId;
        private String notificationLongMessage;
        private String staffProfilePic;
        private String schoolId;
        private String staffName;
        private String schoolYear;
        private String notificationId;
        private String staffId;
        private String notificationDate;
        private List<String> toId;
        private List<FilesBean> files;
        private boolean isExpanded = false;
        private int type = 0;
        private String month;

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public boolean isExpanded() {
            return isExpanded;
        }

        public void setExpanded(boolean expanded) {
            isExpanded = expanded;
        }

        public String getGradeId() {
            return gradeId;
        }

        public void setGradeId(String gradeId) {
            this.gradeId = gradeId;
        }

        public String getNotificationShortMessage() {
            return notificationShortMessage;
        }

        public void setNotificationShortMessage(String notificationShortMessage) {
            this.notificationShortMessage = notificationShortMessage;
        }

        public String getStaffEmailId() {
            return staffEmailId;
        }

        public void setStaffEmailId(String staffEmailId) {
            this.staffEmailId = staffEmailId;
        }

        public String getChannelType() {
            return channelType;
        }

        public void setChannelType(String channelType) {
            this.channelType = channelType;
        }

        public String getNotificationType() {
            return notificationType;
        }

        public void setNotificationType(String notificationType) {
            this.notificationType = notificationType;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getNotificationLongMessage() {
            return notificationLongMessage;
        }

        public void setNotificationLongMessage(String notificationLongMessage) {
            this.notificationLongMessage = notificationLongMessage;
        }

        public String getStaffProfilePic() {
            return staffProfilePic;
        }

        public void setStaffProfilePic(String staffProfilePic) {
            this.staffProfilePic = staffProfilePic;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getStaffName() {
            return staffName;
        }

        public void setStaffName(String staffName) {
            this.staffName = staffName;
        }

        public String getSchoolYear() {
            return schoolYear;
        }

        public void setSchoolYear(String schoolYear) {
            this.schoolYear = schoolYear;
        }

        public String getNotificationId() {
            return notificationId;
        }

        public void setNotificationId(String notificationId) {
            this.notificationId = notificationId;
        }

        public String getStaffId() {
            return staffId;
        }

        public void setStaffId(String staffId) {
            this.staffId = staffId;
        }

        public String getNotificationDate() {
            return notificationDate;
        }

        public void setNotificationDate(String notificationDate) {
            this.notificationDate = notificationDate;
        }

        public List<String> getToId() {
            return toId;
        }

        public void setToId(List<String> toId) {
            this.toId = toId;
        }

        public List<FilesBean> getFiles() {
            return files;
        }

        public void setFiles(List<FilesBean> files) {
            this.files = files;
        }

        public static class FilesBean implements Parcelable {
            /**
             * filePath : /schoolapp/images/077b106b-6331-4bf8-8e8f-f497043f17bb.jpg
             */

            private String filePath;

            public String getFilePath() {
                return filePath;
            }

            public void setFilePath(String filePath) {
                this.filePath = filePath;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.filePath);
            }

            public FilesBean() {
            }

            protected FilesBean(Parcel in) {
                this.filePath = in.readString();
            }

            public static final Creator<FilesBean> CREATOR = new Creator<FilesBean>() {
                @Override
                public FilesBean createFromParcel(Parcel source) {
                    return new FilesBean(source);
                }

                @Override
                public FilesBean[] newArray(int size) {
                    return new FilesBean[size];
                }
            };
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.gradeId);
            dest.writeString(this.notificationShortMessage);
            dest.writeString(this.staffEmailId);
            dest.writeString(this.channelType);
            dest.writeString(this.notificationType);
            dest.writeString(this.creationDate);
            dest.writeString(this.classId);
            dest.writeString(this.notificationLongMessage);
            dest.writeString(this.staffProfilePic);
            dest.writeString(this.schoolId);
            dest.writeString(this.staffName);
            dest.writeString(this.schoolYear);
            dest.writeString(this.notificationId);
            dest.writeString(this.staffId);
            dest.writeString(this.notificationDate);
            dest.writeStringList(this.toId);
            dest.writeList(this.files);
            dest.writeByte(this.isExpanded ? (byte) 1 : (byte) 0);
            dest.writeInt(this.type);
            dest.writeString(this.month);
        }

        public ResultBean() {
        }

        protected ResultBean(Parcel in) {
            this.gradeId = in.readString();
            this.notificationShortMessage = in.readString();
            this.staffEmailId = in.readString();
            this.channelType = in.readString();
            this.notificationType = in.readString();
            this.creationDate = in.readString();
            this.classId = in.readString();
            this.notificationLongMessage = in.readString();
            this.staffProfilePic = in.readString();
            this.schoolId = in.readString();
            this.staffName = in.readString();
            this.schoolYear = in.readString();
            this.notificationId = in.readString();
            this.staffId = in.readString();
            this.notificationDate = in.readString();
            this.toId = in.createStringArrayList();
            this.files = new ArrayList<FilesBean>();
            in.readList(this.files, FilesBean.class.getClassLoader());
            this.isExpanded = in.readByte() != 0;
            this.type = in.readInt();
            this.month = in.readString();
        }

        public static final Parcelable.Creator<ResultBean> CREATOR = new Parcelable.Creator<ResultBean>() {
            @Override
            public ResultBean createFromParcel(Parcel source) {
                return new ResultBean(source);
            }

            @Override
            public ResultBean[] newArray(int size) {
                return new ResultBean[size];
            }
        };
    }
}
