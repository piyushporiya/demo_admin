/*
 * Copyright (c) 2017.                                                       
 *                                                                           
 * ***********************************************************************  
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                           
 * ________________________________________________________________________  
 *                                                                           
 *    [2010] - [2018] SripathiSolutions Corp.                                
 *    [2010] - [2018] DataReady Technology Corp.                             
 *   ________________________________________________________________________
 *                                                                           
 *    All Rights Reserved.                                                   
 *                                                                           
 *   NOTICE:  All information contained herein is, and remains               
 *   the property of DataReady Technology  and its suppliers,                
 *   if any.  The intellectual and technical concepts contained              
 *   herein are proprietary to DataReady Technology Incorporated             
 *   and its suppliers and may be covered by U.S. and Foreign Patents,       
 *   patents in process, and are protected by trade secret or copyright law. 
 *   Dissemination of this information or reproduction of this material      
 *   is strictly forbidden unless prior written permission is obtained       
 *   from DataReady Technology Incorporated.                                 
 *                                                                           
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                            
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;


import java.util.List;

public class GetYearListResponse {


    /**
     * message : Informations are below
     * status : true
     * authtoken : null
     * result : [{"yearStartDate":"2017-12-31","yearEndDate":"2017-01-01","lastUpdateDate":"2017-12-23 05:32:10","schoolId":"129","schoolYear":"2017","id":"AWBpTmkRN0p0Sxykca7x","status":"completed"},{"yearStartDate":"2018-06-30","yearEndDate":"2017-12-20","lastUpdateDate":"2017-12-23 05:33:51","schoolId":"129","schoolYear":"2017-2018","id":"AWCB3Pt-N0p0SxykcbL7","status":"active"}]
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private String authtokenexpires;
    private List<ResultBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * yearStartDate : 2017-12-31
         * yearEndDate : 2017-01-01
         * lastUpdateDate : 2017-12-23 05:32:10
         * schoolId : 129
         * schoolYear : 2017
         * id : AWBpTmkRN0p0Sxykca7x
         * status : completed
         */

        private String yearStartDate;
        private String yearEndDate;
        private String lastUpdateDate;
        private String schoolId;
        private String schoolYear;
        private String id;
        private String status;
        private boolean isActive = false;

        public boolean isActive() {
            return isActive;
        }

        public void setActive(boolean active) {
            isActive = active;
        }

        public String getYearStartDate() {
            return yearStartDate;
        }

        public void setYearStartDate(String yearStartDate) {
            this.yearStartDate = yearStartDate;
        }

        public String getYearEndDate() {
            return yearEndDate;
        }

        public void setYearEndDate(String yearEndDate) {
            this.yearEndDate = yearEndDate;
        }

        public String getLastUpdateDate() {
            return lastUpdateDate;
        }

        public void setLastUpdateDate(String lastUpdateDate) {
            this.lastUpdateDate = lastUpdateDate;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getSchoolYear() {
            return schoolYear;
        }

        public void setSchoolYear(String schoolYear) {
            this.schoolYear = schoolYear;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
