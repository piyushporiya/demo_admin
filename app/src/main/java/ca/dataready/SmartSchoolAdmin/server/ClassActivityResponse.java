/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class ClassActivityResponse {


    /**
     * message : Message informations are below
     * status : true
     * authtoken : null
     * result : [{"toId":[],"notificationEndTime":"07:55 PM","gradeId":"3","notificationShortMessage":"trying ","staffEmailId":"teacherone@dataready.in","channelType":"class","notificationType":"event","notificationEndDate":"2018-01-25","creationDate":"2018-01-23 14:16:53","classId":"A","notificationStartTime":"07:45 PM","notificationLongMessage":"The first ","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","timePeriod":"Every Day","schoolYear":"2017-2018","notificationId":"1516717013437","staffId":"teacherone@dataready.in","notificationDate":"2018-03-21"},{"toId":[],"notificationEndTime":"12:00 PM","gradeId":"3","notificationShortMessage":"Raja1","staffEmailId":"teacherone@dataready.in","channelType":"class","notificationType":"event","notificationEndDate":"2018-02-23","creationDate":"2018-01-22 06:11:24","classId":"A","notificationStartTime":"11:00 AM","notificationLongMessage":"holidays","staffProfilePic":"","schoolId":"129","staffName":"Teacher One","timePeriod":"1 hour","schoolYear":"2017-2018","files":[{"filePath":"/schoolapp/images/df2e59c6-8be6-4c38-9e4c-e885b98ba6a1.PNG"}],"notificationId":"1516601484714","staffId":"teacherone@dataready.in","notificationDate":"2018-02-22"},{"toId":[],"notificationEndTime":"12:00 PM","gradeId":"3","notificationShortMessage":"school Event","staffEmailId":"teacherone@dataready.in","channelType":"class","notificationType":"event","notificationEndDate":"2018-02-23","creationDate":"2018-01-22 10:52:24","classId":"A","notificationStartTime":"11:00 AM","notificationLongMessage":"holidays","staffProfilePic":"","schoolId":"129","staffName":"Teacher One","timePeriod":"Every day","schoolYear":"2017-2018","files":[{"filePath":"/schoolapp/images/df2e59c6-8be6-4c38-9e4c-e885b98ba6a1.PNG"}],"notificationId":"1516618344528","staffId":"teacherone@dataready.in","notificationDate":"2018-02-22"},{"toId":[],"notificationEndTime":"08:05 PM","gradeId":"3","notificationShortMessage":"the only way I could ever find out ","staffEmailId":"teacherone@dataready.in","channelType":"class","notificationType":"event","notificationEndDate":"","creationDate":"2018-01-23 14:31:27","classId":"A","notificationStartTime":"08:00 PM","notificationLongMessage":"I just got to the point where I\u2019m going to go to the movies and I will be there in a few minutes I will be there in about an hour or so I\u2019ll text him and see how you\u2019re going I know that I will see him and see if you guys want me to come get him to get come over for a little while and see how he does he know how much I appreciate him I love know him I so I will be happy there ","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","timePeriod":"","schoolYear":"2017-2018","notificationId":"1516717887232","staffId":"teacherone@dataready.in","notificationDate":"2018-01-25"},{"toId":["string"],"notificationEndTime":"12:00:00","gradeId":"3","notificationShortMessage":"hai how are you??","staffEmailId":"teacherone@dataready.in","channelType":"class","notificationType":"event","notificationEndDate":"2018-01-24","creationDate":"2018-01-23 05:49:01","classId":"B","notificationStartTime":"10:00:00","notificationLongMessage":"helooo","staffProfilePic":"/schoolapp/images/modernschool.jpg","schoolId":"129","staffName":"Teacher One","timePeriod":"string","schoolYear":"2017-2018","files":[{"filePath":"/schoolapp/images/df2e59c6-8be6-4c38-9e4c-e885b98ba6a1.PNG"}],"notificationId":"1516686541592","staffId":"teacherone@dataready.in","notificationDate":"2018-01-23"},{"toId":[],"notificationEndTime":"08:05 PM","gradeId":"3","notificationShortMessage":"I err","staffEmailId":"teacherone@dataready.in","channelType":"class","notificationType":"event","notificationEndDate":"","creationDate":"2018-01-23 14:28:57","classId":"A","notificationStartTime":"08:00 PM","notificationLongMessage":"Egg\n","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","timePeriod":"","schoolYear":"2017-2018","files":[{"filePath":"/schoolapp/images/86e8e7b5-49ad-4bcc-a851-a348dffe4a4c.png"}],"notificationId":"1516717737975","staffId":"teacherone@dataready.in","notificationDate":"2018-01-23"},{"toId":[],"notificationEndTime":"07:40 PM","gradeId":"3","notificationShortMessage":"typical ","staffEmailId":"teacherone@dataready.in","channelType":"class","notificationType":"event","notificationEndDate":"","creationDate":"2018-01-23 14:04:39","classId":"A","notificationStartTime":"07:35 PM","notificationLongMessage":"Lo","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","timePeriod":"","schoolYear":"2017-2018","notificationId":"1516716279744","staffId":"teacherone@dataready.in","notificationDate":"2018-01-23"},{"toId":[],"notificationEndTime":"08:05 PM","gradeId":"3","notificationShortMessage":"the only new ","staffEmailId":"teacherone@dataready.in","channelType":"class","notificationType":"event","notificationEndDate":"2018-01-24","creationDate":"2018-01-23 14:30:23","classId":"B","notificationStartTime":"08:00 PM","notificationLongMessage":"You have ","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","timePeriod":"Every Week","schoolYear":"2017-2018","files":[{"filePath":"/schoolapp/images/d249c31c-2deb-4829-8767-9fdc312a6559.png"},{"filePath":"/schoolapp/images/7ca71a5b-a05a-4d8f-9256-675f91228cb8.png"},{"filePath":"/schoolapp/images/8a13fc80-2a13-4071-9045-8969234a5b66.pdf"}],"notificationId":"1516717823818","staffId":"teacherone@dataready.in","notificationDate":"2018-01-23"},{"toId":[],"notificationEndTime":"12:00 PM","gradeId":"3","notificationShortMessage":"Raja","staffEmailId":"teacherone@dataready.in","channelType":"class","notificationType":"event","notificationEndDate":"2018-01-23","creationDate":"2018-01-22 05:44:20","classId":"A","notificationStartTime":"11:00 AM","notificationLongMessage":"holidays","staffProfilePic":"","schoolId":"129","staffName":"Teacher One","timePeriod":"1 hour","schoolYear":"2017-2018","files":[{"filePath":"/schoolapp/images/df2e59c6-8be6-4c38-9e4c-e885b98ba6a1.PNG"}],"notificationId":"1516599860300","staffId":"teacherone@dataready.in","notificationDate":"2018-01-22"},{"toId":[],"notificationEndTime":"12:00 PM","gradeId":"3","notificationShortMessage":"Raja","staffEmailId":"teacherone@dataready.in","channelType":"class","notificationType":"event","notificationEndDate":"2018-01-23","creationDate":"2018-01-22 05:05:42","classId":"A","notificationStartTime":"11:00 AM","notificationLongMessage":"holidays","staffProfilePic":"","schoolId":"129","staffName":"Teacher One","timePeriod":"1 hour","schoolYear":"2017-2018","notificationId":"1516597542263","staffId":"teacherone@dataready.in","notificationDate":"2018-01-22"}]
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private String authtokenexpires;
    private List<ResultBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean implements Parcelable {
        /**
         * toId : []
         * notificationEndTime : 07:55 PM
         * gradeId : 3
         * notificationShortMessage : trying
         * staffEmailId : teacherone@dataready.in
         * channelType : class
         * notificationType : event
         * notificationEndDate : 2018-01-25
         * creationDate : 2018-01-23 14:16:53
         * classId : A
         * notificationStartTime : 07:45 PM
         * notificationLongMessage : The first
         * staffProfilePic : /schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png
         * schoolId : 129
         * staffName : Teacher One
         * timePeriod : Every Day
         * schoolYear : 2017-2018
         * notificationId : 1516717013437
         * staffId : teacherone@dataready.in
         * notificationDate : 2018-03-21
         * files : [{"filePath":"/schoolapp/images/df2e59c6-8be6-4c38-9e4c-e885b98ba6a1.PNG"}]
         */

        private String notificationEndTime;
        private String repeatEventId;
        private String gradeId;
        private String notificationShortMessage;
        private String staffEmailId;
        private String channelType;
        private String notificationType;
        private String notificationEndDate;
        private String creationDate;
        private String classId;
        private String notificationStartTime;
        private String notificationLongMessage;
        private String staffProfilePic;
        private String schoolId;
        private String staffName;
        private String timePeriod;
        private String schoolYear;
        private String notificationId;
        private String staffId;
        private String notificationDate;
        private List<String> toId;
        private List<FilesBean> files;
        private boolean isTodaysEvent = false;
        private boolean isUpcomingEvent = false;
        private int type = 0;
        private String month;
        private  boolean isExpanded = false;

        public String getRepeatEventId() {
            return repeatEventId;
        }

        public void setRepeatEventId(String repeatEventId) {
            this.repeatEventId = repeatEventId;
        }

        public static Creator<ResultBean> getCREATOR() {
            return CREATOR;
        }

        public boolean isExpanded() {
            return isExpanded;
        }

        public void setExpanded(boolean expanded) {
            isExpanded = expanded;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public boolean isTodaysEvent() {
            return isTodaysEvent;
        }

        public void setTodaysEvent(boolean todaysEvent) {
            isTodaysEvent = todaysEvent;
        }

        public boolean isUpcomingEvent() {
            return isUpcomingEvent;
        }

        public void setUpcomingEvent(boolean upcomingEvent) {
            isUpcomingEvent = upcomingEvent;
        }

        public String getNotificationEndTime() {
            return notificationEndTime;
        }

        public void setNotificationEndTime(String notificationEndTime) {
            this.notificationEndTime = notificationEndTime;
        }

        public String getGradeId() {
            return gradeId;
        }

        public void setGradeId(String gradeId) {
            this.gradeId = gradeId;
        }

        public String getNotificationShortMessage() {
            return notificationShortMessage;
        }

        public void setNotificationShortMessage(String notificationShortMessage) {
            this.notificationShortMessage = notificationShortMessage;
        }

        public String getStaffEmailId() {
            return staffEmailId;
        }

        public void setStaffEmailId(String staffEmailId) {
            this.staffEmailId = staffEmailId;
        }

        public String getChannelType() {
            return channelType;
        }

        public void setChannelType(String channelType) {
            this.channelType = channelType;
        }

        public String getNotificationType() {
            return notificationType;
        }

        public void setNotificationType(String notificationType) {
            this.notificationType = notificationType;
        }

        public String getNotificationEndDate() {
            return notificationEndDate;
        }

        public void setNotificationEndDate(String notificationEndDate) {
            this.notificationEndDate = notificationEndDate;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getNotificationStartTime() {
            return notificationStartTime;
        }

        public void setNotificationStartTime(String notificationStartTime) {
            this.notificationStartTime = notificationStartTime;
        }

        public String getNotificationLongMessage() {
            return notificationLongMessage;
        }

        public void setNotificationLongMessage(String notificationLongMessage) {
            this.notificationLongMessage = notificationLongMessage;
        }

        public String getStaffProfilePic() {
            return staffProfilePic;
        }

        public void setStaffProfilePic(String staffProfilePic) {
            this.staffProfilePic = staffProfilePic;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getStaffName() {
            return staffName;
        }

        public void setStaffName(String staffName) {
            this.staffName = staffName;
        }

        public String getTimePeriod() {
            return timePeriod;
        }

        public void setTimePeriod(String timePeriod) {
            this.timePeriod = timePeriod;
        }

        public String getSchoolYear() {
            return schoolYear;
        }

        public void setSchoolYear(String schoolYear) {
            this.schoolYear = schoolYear;
        }

        public String getNotificationId() {
            return notificationId;
        }

        public void setNotificationId(String notificationId) {
            this.notificationId = notificationId;
        }

        public String getStaffId() {
            return staffId;
        }

        public void setStaffId(String staffId) {
            this.staffId = staffId;
        }

        public String getNotificationDate() {
            return notificationDate;
        }

        public void setNotificationDate(String notificationDate) {
            this.notificationDate = notificationDate;
        }

        public List<String> getToId() {
            return toId;
        }

        public void setToId(List<String> toId) {
            this.toId = toId;
        }

        public List<FilesBean> getFiles() {
            return files;
        }

        public void setFiles(List<FilesBean> files) {
            this.files = files;
        }

        public static class FilesBean implements Parcelable {
            /**
             * filePath : /schoolapp/images/df2e59c6-8be6-4c38-9e4c-e885b98ba6a1.PNG
             */

            private String filePath;

            public String getFilePath() {
                return filePath;
            }

            public void setFilePath(String filePath) {
                this.filePath = filePath;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.filePath);
            }

            public FilesBean() {
            }

            protected FilesBean(Parcel in) {
                this.filePath = in.readString();
            }

            public static final Creator<FilesBean> CREATOR = new Creator<FilesBean>() {
                @Override
                public FilesBean createFromParcel(Parcel source) {
                    return new FilesBean(source);
                }

                @Override
                public FilesBean[] newArray(int size) {
                    return new FilesBean[size];
                }
            };
        }

        public ResultBean() {
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.notificationEndTime);
            dest.writeString(this.repeatEventId);
            dest.writeString(this.gradeId);
            dest.writeString(this.notificationShortMessage);
            dest.writeString(this.staffEmailId);
            dest.writeString(this.channelType);
            dest.writeString(this.notificationType);
            dest.writeString(this.notificationEndDate);
            dest.writeString(this.creationDate);
            dest.writeString(this.classId);
            dest.writeString(this.notificationStartTime);
            dest.writeString(this.notificationLongMessage);
            dest.writeString(this.staffProfilePic);
            dest.writeString(this.schoolId);
            dest.writeString(this.staffName);
            dest.writeString(this.timePeriod);
            dest.writeString(this.schoolYear);
            dest.writeString(this.notificationId);
            dest.writeString(this.staffId);
            dest.writeString(this.notificationDate);
            dest.writeStringList(this.toId);
            dest.writeTypedList(this.files);
            dest.writeByte(this.isTodaysEvent ? (byte) 1 : (byte) 0);
            dest.writeByte(this.isUpcomingEvent ? (byte) 1 : (byte) 0);
            dest.writeInt(this.type);
            dest.writeString(this.month);
            dest.writeByte(this.isExpanded ? (byte) 1 : (byte) 0);
        }

        protected ResultBean(Parcel in) {
            this.notificationEndTime = in.readString();
            this.repeatEventId = in.readString();
            this.gradeId = in.readString();
            this.notificationShortMessage = in.readString();
            this.staffEmailId = in.readString();
            this.channelType = in.readString();
            this.notificationType = in.readString();
            this.notificationEndDate = in.readString();
            this.creationDate = in.readString();
            this.classId = in.readString();
            this.notificationStartTime = in.readString();
            this.notificationLongMessage = in.readString();
            this.staffProfilePic = in.readString();
            this.schoolId = in.readString();
            this.staffName = in.readString();
            this.timePeriod = in.readString();
            this.schoolYear = in.readString();
            this.notificationId = in.readString();
            this.staffId = in.readString();
            this.notificationDate = in.readString();
            this.toId = in.createStringArrayList();
            this.files = in.createTypedArrayList(FilesBean.CREATOR);
            this.isTodaysEvent = in.readByte() != 0;
            this.isUpcomingEvent = in.readByte() != 0;
            this.type = in.readInt();
            this.month = in.readString();
            this.isExpanded = in.readByte() != 0;
        }

        public static final Creator<ResultBean> CREATOR = new Creator<ResultBean>() {
            @Override
            public ResultBean createFromParcel(Parcel source) {
                return new ResultBean(source);
            }

            @Override
            public ResultBean[] newArray(int size) {
                return new ResultBean[size];
            }
        };
    }
}
