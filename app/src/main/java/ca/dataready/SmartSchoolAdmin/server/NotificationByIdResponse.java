/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;


import java.util.List;

public class NotificationByIdResponse {


    /**
     * message : Message informations are below
     * status : true
     * authtoken : null
     * result : {"toId":[],"gradeId":"3","notificationShortMessage":"das","staffEmailId":"teacherone@dataready.in","channelType":"class","creationDate":"2017-12-22 11:41:44","classId":"A","notificationLongMessage":"ee","staffProfilePic":"/schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png","schoolId":"129","staffName":"Teacher One","schoolYear":"2017","files":[{"filePath":"/schoolapp/images/d2f24343-306d-4e41-b7f8-91224bf27619.png"}],"notificationId":"1513942904817","staffId":"teacherone@dataready.in","notificationDate":"2017-12-22"}
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private Object authtoken;
    private ResultBean result;
    private Object authtokenexpires;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Object getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(Object authtoken) {
        this.authtoken = authtoken;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public Object getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(Object authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public static class ResultBean {
        /**
         * toId : []
         * gradeId : 3
         * notificationShortMessage : das
         * staffEmailId : teacherone@dataready.in
         * channelType : class
         * creationDate : 2017-12-22 11:41:44
         * classId : A
         * notificationLongMessage : ee
         * staffProfilePic : /schoolapp/images/66462177-e663-4d0f-9a03-e790aa4e48bf.png
         * schoolId : 129
         * staffName : Teacher One
         * schoolYear : 2017
         * files : [{"filePath":"/schoolapp/images/d2f24343-306d-4e41-b7f8-91224bf27619.png"}]
         * notificationId : 1513942904817
         * staffId : teacherone@dataready.in
         * notificationDate : 2017-12-22
         */

        private String gradeId;
        private String notificationShortMessage;
        private String staffEmailId;
        private String channelType;
        private String creationDate;
        private String classId;
        private String notificationLongMessage;
        private String staffProfilePic;
        private String schoolId;
        private String staffName;
        private String schoolYear;
        private String notificationId;
        private String staffId;
        private String notificationDate;
        private List<?> toId;
        private List<FilesBean> files;

        public String getGradeId() {
            return gradeId;
        }

        public void setGradeId(String gradeId) {
            this.gradeId = gradeId;
        }

        public String getNotificationShortMessage() {
            return notificationShortMessage;
        }

        public void setNotificationShortMessage(String notificationShortMessage) {
            this.notificationShortMessage = notificationShortMessage;
        }

        public String getStaffEmailId() {
            return staffEmailId;
        }

        public void setStaffEmailId(String staffEmailId) {
            this.staffEmailId = staffEmailId;
        }

        public String getChannelType() {
            return channelType;
        }

        public void setChannelType(String channelType) {
            this.channelType = channelType;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getNotificationLongMessage() {
            return notificationLongMessage;
        }

        public void setNotificationLongMessage(String notificationLongMessage) {
            this.notificationLongMessage = notificationLongMessage;
        }

        public String getStaffProfilePic() {
            return staffProfilePic;
        }

        public void setStaffProfilePic(String staffProfilePic) {
            this.staffProfilePic = staffProfilePic;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getStaffName() {
            return staffName;
        }

        public void setStaffName(String staffName) {
            this.staffName = staffName;
        }

        public String getSchoolYear() {
            return schoolYear;
        }

        public void setSchoolYear(String schoolYear) {
            this.schoolYear = schoolYear;
        }

        public String getNotificationId() {
            return notificationId;
        }

        public void setNotificationId(String notificationId) {
            this.notificationId = notificationId;
        }

        public String getStaffId() {
            return staffId;
        }

        public void setStaffId(String staffId) {
            this.staffId = staffId;
        }

        public String getNotificationDate() {
            return notificationDate;
        }

        public void setNotificationDate(String notificationDate) {
            this.notificationDate = notificationDate;
        }

        public List<?> getToId() {
            return toId;
        }

        public void setToId(List<?> toId) {
            this.toId = toId;
        }

        public List<FilesBean> getFiles() {
            return files;
        }

        public void setFiles(List<FilesBean> files) {
            this.files = files;
        }

        public static class FilesBean {
            /**
             * filePath : /schoolapp/images/d2f24343-306d-4e41-b7f8-91224bf27619.png
             */

            private String filePath;

            public String getFilePath() {
                return filePath;
            }

            public void setFilePath(String filePath) {
                this.filePath = filePath;
            }
        }
    }
}
