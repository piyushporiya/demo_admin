/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GetAllTeacherResponse implements Parcelable {


    /**
     * message : Driver details are below
     * status : true
     * authtoken : null
     * result : [{"lastName":"two","firstName":"Teacher","role":"staff","gender":"Female","profilePic":"/schoolapp/images/1e947096-1b1e-4487-b5fd-8097171801f9.jpg","routeDetails":{"pickupRouteId":"102011","name":"SP","pickupStopId":"3","id":"11","uniqueId":"694094","pickupRouteName":"Social Route"},"emailId":"teachertwo@dataready.in","deviceTokenId":"c4B11psCSrA:APA91bFLffQa7ERIYYZ_KUDSdytQWNW1cp7aeMOhNPPDMJaVw502nWolhu-wWl3o5De0FaPx1eWdUxL4wm-RiBySDEisVo-NY52V4m5QDlGWpH8fP9xfS6a3PC4jYB9fCB6FjQaws2ph","userType":"teacher","id":"teachertwo@dataready.in","phoneNo":"9898989898"},{"lastName":"One","firstName":"Teacher","role":"staff","gender":"female","profilePic":"/schoolapp/images/67e878b1-026b-4244-85e4-7b7113a527d3.png","routeDetails":{"dropoffRouteId":"10","dropoffStopId":"1","pickupRouteId":"102011","dropoffRouteName":"SP10","name":"","stopId":"","pickupStopId":"2","id":"","uniqueId":"","pickupRouteName":"Social Route"},"emailId":"teacherone@dataready.in","deviceTokenId":"c4B11psCSrA:APA91bFLffQa7ERIYYZ_KUDSdytQWNW1cp7aeMOhNPPDMJaVw502nWolhu-wWl3o5De0FaPx1eWdUxL4wm-RiBySDEisVo-NY52V4m5QDlGWpH8fP9xfS6a3PC4jYB9fCB6FjQaws2ph","userType":"teacher","id":"teacherone@dataready.in","phoneNo":"98765432101"},{"lastName":"sadf","firstName":"piter","role":null,"gender":"male","profilePic":"/schoolapp/images/66def9ba-a263-4a66-9d63-7d6306d494cb.png","routeDetails":{"dropoffRouteId":"0102201800","dropoffStopId":"3","pickupRouteId":"12021612531","dropoffRouteName":" Pool 8","pickupStopId":"2","pickupRouteName":"Pool 13"},"emailId":"piter@gmail.com","deviceTokenId":"","userType":"teacher","id":"piter@gmail.com","phoneNo":"12345678"},{"lastName":"three","firstName":"Teacher","role":"staff","gender":"female","profilePic":"/schoolapp/images/de6e3b40-492f-42f5-9850-19573bb71a52.png","routeDetails":{"pickupRouteId":"12021612531","name":"SP","pickupStopId":"2","id":"11","uniqueId":"694094","pickupRouteName":"Pool 13"},"emailId":"teacherthree@dataready.in","deviceTokenId":"fpVU0bacUZU:APA91bEaO7emu4Y7BhZ9esc00wI_wlaj7OzNn6yk3Z78fzlIX6nNHJFYsPUkUfcoRyYy-O1_SBp_cfRb90pMGgna_4AFFTc6-WNRvC45IC63eaFv1NXsl5wKO7sVrVott78G7fDnn3Lw","userType":"teacher","id":"teacherthree@dataready.in","phoneNo":"9898989898"},{"lastName":"asdfasdf","firstName":"asdf","role":null,"gender":"male","profilePic":"/schoolapp/images/4b04aa96-81e6-401a-878f-5bd9c8bc3e01.png","routeDetails":{"pickupRouteId":"12021612531","pickupStopId":"2","pickupRouteName":"Pool 13"},"emailId":"asdfasdf@gmail.com","deviceTokenId":"","userType":"teacher","id":"asdfasdf@gmail.com","phoneNo":"13623"},{"lastName":"teacher new","firstName":"test","role":"teacher","gender":"male","profilePic":"/schoolapp/images/c2f5cc95-53ab-4c21-ac37-26bf7f698a21.png","emailId":"dteacher@gmail.com","deviceTokenId":"","userType":"teacher","id":"dteacher@gmail.com","phoneNo":"78945412312"},{"lastName":"yadav","firstName":"jitendra","role":"teacher","gender":"male","profilePic":"/schoolapp/images/23cec65a-ed34-46db-8d22-e285dd9baba0.png","routeDetails":{"pickupRouteId":"12021612531","pickupStopId":"3","pickupRouteName":"Pool 13"},"emailId":"ddddddd","deviceTokenId":"","userType":"teacher","id":"ddddddd","phoneNo":"\u20b9&&&\u20b9\u20b9"},{"lastName":"Ykghhhhn","firstName":"Jitendra ","role":"teacher","gender":"male","profilePic":"/schoolapp/images/4eaee0b2-db20-4fbf-979e-e8ef0834b693.png","routeDetails":{"pickupRouteId":"12911","pickupStopId":"3","pickupRouteName":"noida greater noida"},"emailId":"herejitu4u@gmail.com","deviceTokenId":"","userType":"teacher","id":"herejitu4u@gmail.com","phoneNo":"48844884"},{"lastName":"Patel","firstName":"Robert","role":null,"gender":"male","profilePic":"/schoolapp/images/184ac2a5-d94b-42a9-b8a5-deaaf6744694.png","routeDetails":{"pickupRouteId":"12021612531","pickupStopId":"3","pickupRouteName":"Pool 13"},"emailId":"robert@gmail.com","deviceTokenId":"","userType":"teacher","id":"robert@gmail.com","phoneNo":"12356"},{"lastName":"T","firstName":"Vijay ","role":"staff","gender":"male","profilePic":"/schoolapp/images/eb38f9d9-506b-48a8-ac00-28dcebf8813f.png","emailId":"hdirnfnrr@hdhdjdid.com","deviceTokenId":"","userType":"staff","id":"hdirnfnrr@hdhdjdid.com","phoneNo":"747484848488"},{"lastName":"team","firstName":"Sales","role":"teacher","gender":"male","profilePic":"/schoolapp/images/782c0ce1-351f-434c-aba4-2e9904e85124.png","routeDetails":{"dropoffRouteId":"12914","dropoffStopId":"3","dropoffRouteName":"Delhi New Route With Sales Team"},"emailId":"salesteam@gmail.com","deviceTokenId":"","userType":"teacher","id":"salesteam@gmail.com","phoneNo":"789745612"},{"lastName":"Test1","firstName":"Teacher","role":"teacher","gender":"male","profilePic":"/schoolapp/images/a9b3c503-04d9-4c9c-9fa3-95500651ecb5.png","routeDetails":{"pickupRouteId":"12921","pickupStopId":"2","pickupRouteName":"noida dataready"},"emailId":"test@gmail.com","deviceTokenId":"","userType":"teacher","id":"test@gmail.com","phoneNo":"9873221"},{"lastName":"Test","firstName":"Teacher","role":"teacher","gender":"male","profilePic":"/schoolapp/images/fdbf217f-e5c5-48ef-b945-568d9fa1325e.png","routeDetails":{"pickupRouteId":"12921","pickupStopId":"2","pickupRouteName":"noida dataready"},"emailId":"teachertest@gmail.com","deviceTokenId":"","userType":"teacher","id":"teachertest@gmail.com","phoneNo":"987654322"},{"lastName":"asdf","firstName":"jky","role":null,"gender":"female","profilePic":"/schoolapp/images/5b88e499-066f-441f-ae15-fec31213cd3c.png","routeDetails":{"dropoffRouteId":"13568","dropoffStopId":"3","pickupRouteId":"12921","dropoffRouteName":"Udhana","pickupStopId":"2","pickupRouteName":"noida dataready"},"emailId":"asdf","deviceTokenId":"","userType":"teacher","id":"asdf","phoneNo":"asdf"},{"lastName":"Yadav","firstName":"Jitendra  Kr","role":"staff","gender":"male","profilePic":"/schoolapp/images/a56e2227-20ea-4cbb-a755-fdc301a3c105.png","emailId":"herejitu4u@yahoo.co.in","deviceTokenId":"eoFD0xvL-Aw:APA91bFgddcuXNEUCDTNqdo6bd5_EfN3_7H4vVKBvS4QwubrOUbDhzYSTkKwJiVd-gwBzT3lKnljnrO9Hmjav1eEJR_e4JaCsJDn3tHrXFFBiiiidmLz5ieX_ds6Fq4hTr3_Zw9nY_ST","userType":"staff","id":"herejitu4u@yahoo.co.in","phoneNo":"88383838388383333"},{"lastName":"Teacher","firstName":"D","role":null,"gender":"female","profilePic":"/schoolapp/images/ad39d36a-c9c4-422c-a160-abaa8d3a5ff9.png","routeDetails":{"dropoffRouteId":"13568","dropoffStopId":"3","pickupRouteId":"12021612531","dropoffRouteName":"Udhana","pickupStopId":"3","pickupRouteName":"Pool 13"},"emailId":"dt@gmail.com","deviceTokenId":"","userType":"teacher","id":"dt@gmail.com","phoneNo":"098765432"},{"lastName":"Mishra","firstName":"pankaj","role":"teacher","gender":"male","profilePic":"/schoolapp/images/4aa06e47-8d81-4561-be4c-6f06e19123fe.png","routeDetails":{"dropoffRouteId":"12345","dropoffStopId":"3","pickupRouteId":"01022018001","dropoffRouteName":"varacha","pickupStopId":"3","pickupRouteName":"pool 7"},"emailId":"pankajcpmishra@gmail.com","deviceTokenId":"eRyTIBPbwoY:APA91bE9yAvO84bpfqtZWjO6Z2ZpbCNEQpvXW7-6Ev2Y5y8qggcU_aq3qTBpy8QL-A6xBvYScyEijR6F0iNAknYuXoDRTlQ0owfQ8Ld-Yb9gQNQyIusDC4ZAvlcvAvhJVrIek2qcAgTe","userType":"staff","id":"pankajcpmishra@gmail.com","phoneNo":"38388333883"},{"lastName":"Four","firstName":"Teacher","role":null,"gender":"female","profilePic":"/schoolapp/images/6fad61c4-e9cc-436c-ab96-1cea523984ed.png","routeDetails":{"dropoffRouteId":"12914","dropoffStopId":"2","pickupRouteId":"12920","dropoffRouteName":"Delhi New Route With Sales Team","pickupStopId":"2","pickupRouteName":"data rady delhi"},"emailId":"teacher@gmail.com","deviceTokenId":"","userType":"teacher","id":"teacher@gmail.com","phoneNo":"123456"}]
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private String authtokenexpires;
    private ArrayList<ResultBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public ArrayList<ResultBean> getResult() {
        return result;
    }

    public void setResult(ArrayList<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean implements Parcelable {
        /**
         * lastName : two
         * firstName : Teacher
         * role : staff
         * gender : Female
         * profilePic : /schoolapp/images/1e947096-1b1e-4487-b5fd-8097171801f9.jpg
         * routeDetails : {"pickupRouteId":"102011","name":"SP","pickupStopId":"3","id":"11","uniqueId":"694094","pickupRouteName":"Social Route"}
         * emailId : teachertwo@dataready.in
         * deviceTokenId : c4B11psCSrA:APA91bFLffQa7ERIYYZ_KUDSdytQWNW1cp7aeMOhNPPDMJaVw502nWolhu-wWl3o5De0FaPx1eWdUxL4wm-RiBySDEisVo-NY52V4m5QDlGWpH8fP9xfS6a3PC4jYB9fCB6FjQaws2ph
         * userType : teacher
         * id : teachertwo@dataready.in
         * phoneNo : 9898989898
         */

        private String lastName;
        private String firstName;
        private String role;
        private String gender;
        private String profilePic;
        private RouteDetailsBean routeDetails;
        private EmergencyDetailsBean emergencyDetails;
        private String emailId;
        private String deviceTokenId;
        private String userType;
        private String id;
        private String phoneNo;
        private String department;
        private String joiningDate;
        private String dob;
        private String address;
        private String maritalStatus;

        public String getMaritalStatus() {
            return maritalStatus;
        }

        public void setMaritalStatus(String maritalStatus) {
            this.maritalStatus = maritalStatus;
        }

        /**
         * emergencyDetails : {"address":"string","dob":"string","education":"string","emailId":"string","files":[{"fileName":"string","filePath":"string"}],"firstName":"string","lastName":"string","occupation":"string","phoneNo":"string","relation":"string"}
         */



        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getDepartment() {
            return department;
        }

        public void setDepartment(String department) {
            this.department = department;
        }

        public String getJoiningDate() {
            return joiningDate;
        }

        public void setJoiningDate(String joiningDate) {
            this.joiningDate = joiningDate;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public static Creator<ResultBean> getCREATOR() {
            return CREATOR;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        public RouteDetailsBean getRouteDetails() {
            return routeDetails;
        }

        public void setRouteDetails(RouteDetailsBean routeDetails) {
            this.routeDetails = routeDetails;
        }

        public String getEmailId() {
            return emailId;
        }

        public void setEmailId(String emailId) {
            this.emailId = emailId;
        }

        public String getDeviceTokenId() {
            return deviceTokenId;
        }

        public void setDeviceTokenId(String deviceTokenId) {
            this.deviceTokenId = deviceTokenId;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public EmergencyDetailsBean getEmergencyDetails() {
            return emergencyDetails;
        }

        public void setEmergencyDetails(EmergencyDetailsBean emergencyDetails) {
            this.emergencyDetails = emergencyDetails;
        }


        public static class RouteDetailsBean implements Parcelable {
            /**
             * pickupRouteId : 102011
             * name : SP
             * pickupStopId : 3
             * id : 11
             * uniqueId : 694094
             * pickupRouteName : Social Route
             */

            private String pickupRouteId;
            private String name;
            private String pickupStopId;
            private String id;
            private String uniqueId;
            private String pickupRouteName;

            public String getPickupRouteId() {
                return pickupRouteId;
            }

            public void setPickupRouteId(String pickupRouteId) {
                this.pickupRouteId = pickupRouteId;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPickupStopId() {
                return pickupStopId;
            }

            public void setPickupStopId(String pickupStopId) {
                this.pickupStopId = pickupStopId;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getUniqueId() {
                return uniqueId;
            }

            public void setUniqueId(String uniqueId) {
                this.uniqueId = uniqueId;
            }

            public String getPickupRouteName() {
                return pickupRouteName;
            }

            public void setPickupRouteName(String pickupRouteName) {
                this.pickupRouteName = pickupRouteName;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.pickupRouteId);
                dest.writeString(this.name);
                dest.writeString(this.pickupStopId);
                dest.writeString(this.id);
                dest.writeString(this.uniqueId);
                dest.writeString(this.pickupRouteName);
            }

            public RouteDetailsBean() {
            }

            protected RouteDetailsBean(Parcel in) {
                this.pickupRouteId = in.readString();
                this.name = in.readString();
                this.pickupStopId = in.readString();
                this.id = in.readString();
                this.uniqueId = in.readString();
                this.pickupRouteName = in.readString();
            }

            public static final Creator<RouteDetailsBean> CREATOR = new Creator<RouteDetailsBean>() {
                @Override
                public RouteDetailsBean createFromParcel(Parcel source) {
                    return new RouteDetailsBean(source);
                }

                @Override
                public RouteDetailsBean[] newArray(int size) {
                    return new RouteDetailsBean[size];
                }
            };
        }

        public ResultBean() {
        }

        public static class EmergencyDetailsBean implements Parcelable {
            /**
             * address : string
             * dob : string
             * education : string
             * emailId : string
             * files : [{"fileName":"string","filePath":"string"}]
             * firstName : string
             * lastName : string
             * occupation : string
             * phoneNo : string
             * relation : string
             */

            @SerializedName("address")
            private String addressX;
            @SerializedName("dob")
            private String dobX;
            private String education;
            @SerializedName("emailId")
            private String emailIdX;
            @SerializedName("firstName")
            private String firstNameX;
            @SerializedName("lastName")
            private String lastNameX;
            private String occupation;
            @SerializedName("phoneNo")
            private String phoneNoX;
            private String relation;
            private List<FilesBean> files;

            public String getAddressX() {
                return addressX;
            }

            public void setAddressX(String addressX) {
                this.addressX = addressX;
            }

            public String getDobX() {
                return dobX;
            }

            public void setDobX(String dobX) {
                this.dobX = dobX;
            }

            public String getEducation() {
                return education;
            }

            public void setEducation(String education) {
                this.education = education;
            }

            public String getEmailIdX() {
                return emailIdX;
            }

            public void setEmailIdX(String emailIdX) {
                this.emailIdX = emailIdX;
            }

            public String getFirstNameX() {
                return firstNameX;
            }

            public void setFirstNameX(String firstNameX) {
                this.firstNameX = firstNameX;
            }

            public String getLastNameX() {
                return lastNameX;
            }

            public void setLastNameX(String lastNameX) {
                this.lastNameX = lastNameX;
            }

            public String getOccupation() {
                return occupation;
            }

            public void setOccupation(String occupation) {
                this.occupation = occupation;
            }

            public String getPhoneNoX() {
                return phoneNoX;
            }

            public void setPhoneNoX(String phoneNoX) {
                this.phoneNoX = phoneNoX;
            }

            public String getRelation() {
                return relation;
            }

            public void setRelation(String relation) {
                this.relation = relation;
            }

            public List<FilesBean> getFiles() {
                return files;
            }

            public void setFiles(List<FilesBean> files) {
                this.files = files;
            }

            public static class FilesBean {
                /**
                 * fileName : string
                 * filePath : string
                 */

                private String fileName;
                private String filePath;

                public String getFileName() {
                    return fileName;
                }

                public void setFileName(String fileName) {
                    this.fileName = fileName;
                }

                public String getFilePath() {
                    return filePath;
                }

                public void setFilePath(String filePath) {
                    this.filePath = filePath;
                }
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.addressX);
                dest.writeString(this.dobX);
                dest.writeString(this.education);
                dest.writeString(this.emailIdX);
                dest.writeString(this.firstNameX);
                dest.writeString(this.lastNameX);
                dest.writeString(this.occupation);
                dest.writeString(this.phoneNoX);
                dest.writeString(this.relation);
                dest.writeList(this.files);
            }

            public EmergencyDetailsBean() {
            }

            protected EmergencyDetailsBean(Parcel in) {
                this.addressX = in.readString();
                this.dobX = in.readString();
                this.education = in.readString();
                this.emailIdX = in.readString();
                this.firstNameX = in.readString();
                this.lastNameX = in.readString();
                this.occupation = in.readString();
                this.phoneNoX = in.readString();
                this.relation = in.readString();
                this.files = new ArrayList<FilesBean>();
                in.readList(this.files, FilesBean.class.getClassLoader());
            }

            public static final Creator<EmergencyDetailsBean> CREATOR = new Creator<EmergencyDetailsBean>() {
                @Override
                public EmergencyDetailsBean createFromParcel(Parcel source) {
                    return new EmergencyDetailsBean(source);
                }

                @Override
                public EmergencyDetailsBean[] newArray(int size) {
                    return new EmergencyDetailsBean[size];
                }
            };
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.lastName);
            dest.writeString(this.firstName);
            dest.writeString(this.role);
            dest.writeString(this.gender);
            dest.writeString(this.profilePic);
            dest.writeParcelable(this.routeDetails, flags);
            dest.writeParcelable(this.emergencyDetails, flags);
            dest.writeString(this.emailId);
            dest.writeString(this.deviceTokenId);
            dest.writeString(this.userType);
            dest.writeString(this.id);
            dest.writeString(this.phoneNo);
            dest.writeString(this.department);
            dest.writeString(this.joiningDate);
            dest.writeString(this.dob);
            dest.writeString(this.address);
            dest.writeString(this.maritalStatus);
        }

        protected ResultBean(Parcel in) {
            this.lastName = in.readString();
            this.firstName = in.readString();
            this.role = in.readString();
            this.gender = in.readString();
            this.profilePic = in.readString();
            this.routeDetails = in.readParcelable(RouteDetailsBean.class.getClassLoader());
            this.emergencyDetails = in.readParcelable(EmergencyDetailsBean.class.getClassLoader());
            this.emailId = in.readString();
            this.deviceTokenId = in.readString();
            this.userType = in.readString();
            this.id = in.readString();
            this.phoneNo = in.readString();
            this.department = in.readString();
            this.joiningDate = in.readString();
            this.dob = in.readString();
            this.address = in.readString();
            this.maritalStatus = in.readString();
        }

        public static final Creator<ResultBean> CREATOR = new Creator<ResultBean>() {
            @Override
            public ResultBean createFromParcel(Parcel source) {
                return new ResultBean(source);
            }

            @Override
            public ResultBean[] newArray(int size) {
                return new ResultBean[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.message);
        dest.writeByte(this.status ? (byte) 1 : (byte) 0);
        dest.writeString(this.authtoken);
        dest.writeString(this.authtokenexpires);
        dest.writeTypedList(this.result);
    }

    public GetAllTeacherResponse() {
    }

    protected GetAllTeacherResponse(Parcel in) {
        this.message = in.readString();
        this.status = in.readByte() != 0;
        this.authtoken = in.readString();
        this.authtokenexpires = in.readString();
        this.result = in.createTypedArrayList(ResultBean.CREATOR);
    }

    public static final Parcelable.Creator<GetAllTeacherResponse> CREATOR = new Parcelable.Creator<GetAllTeacherResponse>() {
        @Override
        public GetAllTeacherResponse createFromParcel(Parcel source) {
            return new GetAllTeacherResponse(source);
        }

        @Override
        public GetAllTeacherResponse[] newArray(int size) {
            return new GetAllTeacherResponse[size];
        }
    };
}
