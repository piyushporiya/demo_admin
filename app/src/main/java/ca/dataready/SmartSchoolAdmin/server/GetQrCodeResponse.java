/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;

public class GetQrCodeResponse {


    /**
     * message : Details are below
     * status : true
     * authtoken : null
     * result : {"id":null,"cardId":"1290000079","firstName":null,"lastName":null,"profilePic":null,"schoolId":"129","schoolName":"Pankaj Modern School","profileType":"student","profileId":"1304","activationDate":"2018-03-22","deactivationDate":null,"expiryDate":null,"routeDetails":null,"schoolProfilePic":null,"schoolAddress":null,"schoolYear":null,"driverPin":null,"qrcode":"/schoolapp/images/129/1290000079.jpg","userpermissions":null,"additionalInfo":null,"schoolpermissions":null}
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private Object authtoken;
    private ResultBean result;
    private Object authtokenexpires;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Object getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(Object authtoken) {
        this.authtoken = authtoken;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public Object getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(Object authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public static class ResultBean {
        /**
         * id : null
         * cardId : 1290000079
         * firstName : null
         * lastName : null
         * profilePic : null
         * schoolId : 129
         * schoolName : Pankaj Modern School
         * profileType : student
         * profileId : 1304
         * activationDate : 2018-03-22
         * deactivationDate : null
         * expiryDate : null
         * routeDetails : null
         * schoolProfilePic : null
         * schoolAddress : null
         * schoolYear : null
         * driverPin : null
         * qrcode : /schoolapp/images/129/1290000079.jpg
         * userpermissions : null
         * additionalInfo : null
         * schoolpermissions : null
         */

        private Object id;
        private String cardId;
        private Object firstName;
        private Object lastName;
        private Object profilePic;
        private String schoolId;
        private String schoolName;
        private String profileType;
        private String profileId;
        private String activationDate;
        private Object deactivationDate;
        private Object expiryDate;
        private Object routeDetails;
        private Object schoolProfilePic;
        private Object schoolAddress;
        private Object schoolYear;
        private Object driverPin;
        private String qrcode;
        private Object userpermissions;
        private Object additionalInfo;
        private Object schoolpermissions;

        public Object getId() {
            return id;
        }

        public void setId(Object id) {
            this.id = id;
        }

        public String getCardId() {
            return cardId;
        }

        public void setCardId(String cardId) {
            this.cardId = cardId;
        }

        public Object getFirstName() {
            return firstName;
        }

        public void setFirstName(Object firstName) {
            this.firstName = firstName;
        }

        public Object getLastName() {
            return lastName;
        }

        public void setLastName(Object lastName) {
            this.lastName = lastName;
        }

        public Object getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(Object profilePic) {
            this.profilePic = profilePic;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getSchoolName() {
            return schoolName;
        }

        public void setSchoolName(String schoolName) {
            this.schoolName = schoolName;
        }

        public String getProfileType() {
            return profileType;
        }

        public void setProfileType(String profileType) {
            this.profileType = profileType;
        }

        public String getProfileId() {
            return profileId;
        }

        public void setProfileId(String profileId) {
            this.profileId = profileId;
        }

        public String getActivationDate() {
            return activationDate;
        }

        public void setActivationDate(String activationDate) {
            this.activationDate = activationDate;
        }

        public Object getDeactivationDate() {
            return deactivationDate;
        }

        public void setDeactivationDate(Object deactivationDate) {
            this.deactivationDate = deactivationDate;
        }

        public Object getExpiryDate() {
            return expiryDate;
        }

        public void setExpiryDate(Object expiryDate) {
            this.expiryDate = expiryDate;
        }

        public Object getRouteDetails() {
            return routeDetails;
        }

        public void setRouteDetails(Object routeDetails) {
            this.routeDetails = routeDetails;
        }

        public Object getSchoolProfilePic() {
            return schoolProfilePic;
        }

        public void setSchoolProfilePic(Object schoolProfilePic) {
            this.schoolProfilePic = schoolProfilePic;
        }

        public Object getSchoolAddress() {
            return schoolAddress;
        }

        public void setSchoolAddress(Object schoolAddress) {
            this.schoolAddress = schoolAddress;
        }

        public Object getSchoolYear() {
            return schoolYear;
        }

        public void setSchoolYear(Object schoolYear) {
            this.schoolYear = schoolYear;
        }

        public Object getDriverPin() {
            return driverPin;
        }

        public void setDriverPin(Object driverPin) {
            this.driverPin = driverPin;
        }

        public String getQrcode() {
            return qrcode;
        }

        public void setQrcode(String qrcode) {
            this.qrcode = qrcode;
        }

        public Object getUserpermissions() {
            return userpermissions;
        }

        public void setUserpermissions(Object userpermissions) {
            this.userpermissions = userpermissions;
        }

        public Object getAdditionalInfo() {
            return additionalInfo;
        }

        public void setAdditionalInfo(Object additionalInfo) {
            this.additionalInfo = additionalInfo;
        }

        public Object getSchoolpermissions() {
            return schoolpermissions;
        }

        public void setSchoolpermissions(Object schoolpermissions) {
            this.schoolpermissions = schoolpermissions;
        }
    }
}
