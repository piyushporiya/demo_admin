/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;


import java.util.List;

import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.appointment.AppointmentFragment;
import ca.dataready.SmartSchoolAdmin.classchannel.create.CreateClassChannelActivity;
import ca.dataready.SmartSchoolAdmin.login.LoginActivity;
import ca.dataready.SmartSchoolAdmin.chat.ChatActivity;
import ca.dataready.SmartSchoolAdmin.schoolchannel.create.CreateSchoolChannelActivity;
import ca.dataready.SmartSchoolAdmin.userinfo.driver.add.AddDriverActivity;
import ca.dataready.SmartSchoolAdmin.viewresponse.create.PostMessageActivity;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.ImageUpload;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.ReplyCommunicationModel;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.SchoolCommunication;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.SendCommunicationMessage;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.StudentListModel;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.GradeList;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.ThreadCommunicationModel;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by social_jaydeep on 27/09/17.
 */

public interface AppApi {


    public static final String BASE_URL = "http://" + AdminApp.getInstance().getApiUrl() + "/";
    public static final String GEOCODING_URL = " https://maps.googleapis.com/";
    String apiVersion = AdminApp.getInstance().getApiVersion();
    String VERSION_NO = "1.2.0";

    @POST("/SchoolRegistrationServices/" + VERSION_NO + "/rest/login/")
    Call<CREDENTIAL> login(@Body LoginActivity.LoginParams params);

    @POST("/SchoolRegistrationServices/" + VERSION_NO + "/rest/forgotpassword/")
    Call<ForgotPassResponse> forgotPassword(@Body LoginActivity.ForgotPassParams params);

    @GET("/CommonServices/" + VERSION_NO + "/rest/updatetoken/")
    Call<UpdateTokenResponse> updatetoken(@Query("emailId") String emailId, @Query("authToken") String authToken, @Query("authTokenExpires") String authTokenExpires);

    @GET("/SchoolServices/" + VERSION_NO + "/rest/gradelist/")
    Call<GradeList> getClassSchedule(@Query("schoolId") String schoolId, @Query("schoolYear") String schoolYear);

    @Multipart
    @POST("/CommonServices/" + VERSION_NO + "/rest/fileupload/")
    Call<ImageUpload> upload(@Part MultipartBody.Part file);

    @Multipart
    @POST("/CommonServices/" + VERSION_NO + "/rest/multiplefileupload/")
    Call<MultipleImageUpload> uploadMultipleFile(@Part List<MultipartBody.Part> files);

    @GET("/StudentServices/" + VERSION_NO + "/rest/studentlist/")
    Call<StudentListModel> getStudentList(@Query("schoolId") String schoolId, @Query("gradeId") String gradeId, @Query("classId") String classId, @Query("schoolYear") String schoolYear);

    @POST("/SchoolCommunicationServices/" + VERSION_NO + "/rest/saveteachercommunication/")
    Call<SendCommunicationMessage> sendMessage(@Body PostMessageActivity.SendMessageParams params);

    @GET("/SchoolNotificationsServices/" + VERSION_NO + "/rest/getschoolnotifications/")
    Call<SchoolChannelResponse> getSchoolChannelNotifications(@Query("schoolId") String schoolId, @Query("schoolYear") String schoolYear);

    @GET("/SchoolNotificationsServices/" + VERSION_NO + "/rest/getclassnotifications/")
    Call<ClassChannelResponse> getClassChannelNotifications(@Query("schoolId") String schoolId, @Query("schoolYear") String schoolYear
            , @Query("gradeId") String gradeId, @Query("classId") String classId);

    @GET("/SchoolCommunicationServices/" + VERSION_NO + "/rest/getthreadcommunications/")
    Call<ThreadCommunicationModel> getThreadCommunications(@Query("schoolId") String schoolId, @Query("messageId") String messageId, @Query("studentId") String studentId);

    @POST("/SchoolCommunicationServices/" + VERSION_NO + "/rest/replyteachercommunication/")
    Call<ReplyCommunicationModel> replyCommunication(@Body ChatActivity.ReplyCommunicationParams params);


    @GET("/SchoolCommunicationServices/" + VERSION_NO + "/rest/getschoolcommunications/")
    Call<SchoolCommunication> getSchoolCommunications(@Query("schoolId") String schoolId, @Query("schoolYear") String schoolYear);

    @GET("/SchoolNotificationsServices/" + VERSION_NO + "/rest/getnotificationbyid/")
    Call<NotificationByIdResponse> getNotificationById(@Query("schoolId") String schoolId, @Query("schoolYear") String schoolYear
            , @Query("notificationId") String notificationId);

    @POST("/SchoolNotificationsServices/" + VERSION_NO + "/rest/savenotification/")
    Call<CreateSchoolChannelResponse> sendMessage(@Body CreateSchoolChannelActivity.SendChannelParams params);

    @POST("/SchoolNotificationsServices/" + VERSION_NO + "/rest/savenotification/")
    Call<CreateSchoolChannelResponse> sendMessageToClass(@Body CreateClassChannelActivity.SendChannelParams params);

    @POST("/AppointmentServices/" + VERSION_NO + "/rest/createteacherappointment/")
    Call<AddEventModel> postEvent(@Body AppointmentFragment.AddEventParams params);

    @GET("/AppointmentServices/" + VERSION_NO + "/rest/getteacherappointment/")
    Call<TeacherAppointmentModel> getTeacherAppoinments(@Query("schoolId") String schoolId, @Query("schoolYear") String schoolYear, @Query("teacherId") String teacherId);

    @GET("/AppointmentServices/" + VERSION_NO + "/rest/teacherappointmentbymonth/")
    Call<TeacherAppointmentModel> getTeacherAppoinmentsByMonth(@Query("schoolId") String schoolId, @Query("schoolYear") String schoolYear, @Query("teacherId") String teacherId
            , @Query("month") int month, @Query("year") int year);

    @GET("/AppointmentServices/" + VERSION_NO + "/rest/confirmteacherappointment/")
    Call<AppointmentConfirmResponse> confirmAppointment(@Query("appointmentId") String appointmentId);

    @GET("/AppointmentServices/" + VERSION_NO + "/rest/cancelteacherappointment/")
    Call<AppointmentConfirmResponse> cancelAppointment(@Query("appointmentId") String appointmentId);

    @GET("/AppointmentServices/" + VERSION_NO + "/rest/getappointmentbyid/")
    Call<AppointmentByIdResponse> getAppointmentById(@Query("appointmentId") String appointmentId);

    @POST("/AppointmentServices/" + VERSION_NO + "/rest/updateteacherappointment/")
    Call<AddEventModel> updateEvent(@Body AppointmentFragment.AddEventParams params);

    @GET("/DriverServices/" + VERSION_NO + "/rest/getalldriver/")
    Call<GetAllDriverResponse> getAllDrivers(@Query("schoolId") String schoolId);

    @GET("/DriverServices/" + VERSION_NO + "/rest/searchdriver/")
    Call<GetAllDriverResponse> searchDriver(@Query("schoolId") String schoolId, @Query("driverId") String driverId, @Query("name") String name);

    @POST("/DriverServices/" + VERSION_NO + "/rest/registerdriver/")
    Call<AddDriverResponse> registerDriver(@Body AddDriverActivity.AddDriverParams params);

    @POST("/DriverServices/" + VERSION_NO + "/rest/updatedriver/")
    Call<AddDriverResponse> updateDriver(@Body AddDriverActivity.AddDriverParams params);

    @GET("/maps/api/geocode/json")
    Call<GetLatLngFromAddressResponse> getLatLongFromAddress(@Query("address") String address, @Query("key") String key);

    @GET("/SchoolRegistrationServices/" + VERSION_NO + "/rest/teacherlist/")
    Call<GetAllTeacherResponse> getAllTeachers(@Query("schoolId") String schoolId);

    @GET("/SchoolRegistrationServices/" + VERSION_NO + "/rest/searchteacher/")
    Call<GetAllTeacherResponse> searchTeacher(@Query("schoolId") String schoolId, @Query("emailId") String driverId, @Query("name") String name);

    @POST("/SchoolRegistrationServices/" + VERSION_NO + "/rest/registeruser/")
    Call<AddTeacherResponse> registerTeacher(@Body AddTeacherParams params);

    @POST("/SchoolRegistrationServices/" + VERSION_NO + "/rest/updateuser/")
    Call<AddTeacherResponse> updateTeacher(@Body AddTeacherParams params);

    @GET("/SchoolRegistrationServices/" + VERSION_NO + "/rest/getuserinfo/")
    Call<GetTeacherInfo> getTeacherInfo(@Query("emailId") String emailId);

    @GET("/StudentServices/" + VERSION_NO + "/rest/getpendingstudentprofile/")
    Call<GetStudentsList> getPendingStudents(@Query("schoolId") String schoolId);

    @GET("/StudentServices/" + VERSION_NO + "/rest/studentlist/")
    Call<GetStudentsList> getConfirmStudents(@Query("schoolId") String schoolId, @Query("gradeId") String gradeId
            , @Query("classId") String classId, @Query("schoolYear") String schoolYear);

    @GET("/StudentServices/" + VERSION_NO + "/rest/searchstudent/")
    Call<GetStudentsList> searchStudents(@Query("schoolId") String schoolId, @Query("schoolYear") String schoolYear
            , @Query("studentId") String studentId, @Query("name") String name);


    @POST("/StudentServices/" + VERSION_NO + "/rest/addstudentprofile/")
    Call<AddStudentResponse> registerStudent(@Body AddStudentParams params);

    @POST("/StudentServices/" + VERSION_NO + "/rest/updatestudentprofile/")
    Call<AddStudentResponse> updateStudent(@Body AddStudentParams params);

    @GET("/StudentServices/" + VERSION_NO + "/rest/getstudentprofile/")
    Call<GetStudentProfileResponse> getStudentsInfo(@Query("schoolId") String schoolId, @Query("studentId") String studentId);

    @POST("/StudentServices/" + VERSION_NO + "/rest/registerstudent/")
    Call<RegisterStudentAdminProcessResponse> registerStudentAdminProcess(@Body StudentAdminProcessParams params);

    @POST("/StudentServices/" + VERSION_NO + "/rest/updatestudent/")
    Call<RegisterStudentAdminProcessResponse> updateStudentAdminProcess(@Body StudentAdminProcessParams params);

    @GET("/SchoolServices/" + VERSION_NO + "/rest/getyearlist/")
    Call<GetYearListResponse> getYearList(@Query("schoolId") String schoolId);

    @GET("/SchoolServices/" + VERSION_NO + "/rest/getattendancedetails/")
    Call<GetAttendanceDetails> getAttendanceDetails(@Query("schoolId") String schoolId, @Query("schoolYear") String schoolYear
            , @Query("gradeId") String gradeId, @Query("classId") String classId
            , @Query("studentId") String studentId, @Query("attendanceDate") String attendanceDate);

    @GET("/SchoolBusServices/" + VERSION_NO + "/rest/getstudentboardinghistory/")
    Call<StudentBordingDetails> getStudentTransportationDetails(@Query("schoolId") String schoolId
            , @Query("schoolYear") String schoolYear, @Query("studentId") String studentId
            , @Query("fromDate") String fromDate, @Query("toDate") String toDate);

    @GET("/SchoolBusServices/" + VERSION_NO + "/rest/getstaffboardinghistory/")
    Call<StudentBordingDetails> getStaffTransportationDetails(@Query("schoolId") String schoolId, @Query("schoolYear") String schoolYear
            , @Query("staffId") String staffId, @Query("fromDate") String fromDate, @Query("toDate") String toDate);

    @POST("/SchoolServices/" + VERSION_NO + "/rest/updategrade/")
    Call<AddGradeResponse> addGrade(@Body AddGrade params);

    @POST("/SchoolServices/" + VERSION_NO + "/rest/deletegrade/")
    Call<AddGradeResponse> deleteGradeORClass(@Body DeleteGrade params);

    @GET("/SchoolServices/" + VERSION_NO + "/rest/gradelist/")
    Call<GradeListResponse> getGradeList(@Query("schoolId") String schoolId, @Query("schoolYear") String schoolYear);

    @GET("/SchoolServices/" + VERSION_NO + "/rest/subjectlist/")
    Call<SubjectListResponse> getSubjectList(@Query("schoolId") String schoolId, @Query("gradeId") String gradeId);

    @POST("/SchoolServices/" + VERSION_NO + "/rest/addsubject/")
    Call<AddUpdateSubjectResponse> addSubject(@Body AddSubjectParams params);

    @POST("/SchoolServices/" + VERSION_NO + "/rest/updatesubject/")
    Call<AddUpdateSubjectResponse> updateSubject(@Body AddSubjectParams params);

    @GET("/SchoolServices/" + VERSION_NO + "/rest/deletesubject/")
    Call<DeleteSubjectResponse> deleteSubject(@Query("schoolId") String schoolId, @Query("id") String id);

    @POST("/SchoolServices/" + VERSION_NO + "/rest/setyearactive/")
    Call<CommonAcadamicYearResponse> setActiveYear(@Body SetActiveYearParams params);

    @POST("/SchoolServices/" + VERSION_NO + "/rest/endschoolyear/")
    Call<CommonAcadamicYearResponse> endSchoolYear(@Body EndSchoolYearParams params);

    @POST("/SchoolServices/" + VERSION_NO + "/rest/addschoolyear/")
    Call<CommonAcadamicYearResponse> addSchoolYear(@Body AddUpdateAcademicYearParams params);

    @POST("/SchoolServices/" + VERSION_NO + "/rest/updateschoolyear/")
    Call<CommonAcadamicYearResponse> updateSchoolYear(@Body AddUpdateAcademicYearParams params);

    @GET("/ClassRoomScheduleServices/" + VERSION_NO + "/rest/getclassschedule/")
    Call<GetAssignedTeacherList> getAssignedTeacherList(@Query("schoolId") String schoolId, @Query("schoolYear") String schoolYear
            , @Query("gradeId") String gradeId, @Query("classId") String classId
            , @Query("weekDay") String weekDay);

    @POST("/ClassRoomScheduleServices/" + VERSION_NO + "/rest/postclassschedule/")
    Call<CommonResponse> assignTeacher(@Body AssignUpdateTeacherParams params);

    @POST("/ClassRoomScheduleServices/" + VERSION_NO + "/rest/updateclassschedule/")
    Call<CommonResponse> updateAssignedTeacher(@Body AssignUpdateTeacherParams params);

    @GET("/ClassRoomScheduleServices/" + VERSION_NO + "/rest/deleteclassschedule/")
    Call<CommonResponse> deleteAssignedTeacher(@Query("id") String id);

    @POST("/ConfigurationServices/rest/addconfiguration/")
    Call<ConfigResponse> getConfig(@Body ConfigParams params);

    @GET("/RouteServices/" + VERSION_NO + "/rest/getallschoolroute/")
    Call<GetAllRoutesResponse> getAllRoutes(@Query("schoolId") String schoolId);

    @GET("/RouteServices/" + VERSION_NO + "/rest/searchroute/")
    Call<GetAllRoutesResponse> searchRoute(@Query("schoolId") String schoolId, @Query("routeId") String routeId, @Query("name") String name);

    @POST("/StudentServices/" + VERSION_NO + "/rest/registerstudentlist/")
    Call<AssignStudentResponse> assignStudents(@Body List<AssignStudentParams> params);

    @GET("/SchoolBusServices/" + VERSION_NO + "/rest/gettripinfo/")
    Call<TripInfoResponse> getTripInfo(@Query("routeId") String routeId, @Query("uniqueId") String uniqueId, @Query("recordDate") String recordDate);

    @GET("/SchoolBusServices/" + VERSION_NO + "/rest/gettotalboarding/")
    Call<GetTotalBoardingResponse> getTotalBoarding(@Query("schoolId") String schoolId, @Query("schoolYear") String schoolYear
            , @Query("routeId") String routeId, @Query("tripId") String tripId, @Query("tripType") String tripType);

    @GET("/SchoolBusServices/" + VERSION_NO + "/rest/activeroutewithpositioninfo/")
    Call<ActiveRouteInfoResponse> getActiveRoutes(@Query("schoolId") String schoolId, @Query("recordDate") String recordDate);

    @GET("/CommonServices/" + VERSION_NO + "/rest/questionlistbyapptype/")
    Call<HelpResponse> getQuestions(@Query("appType") String appType);

    @GET("/SchoolNotificationsServices/" + VERSION_NO + "/rest/getclassevents/")
    Call<ClassActivityResponse> getClassEvents(@Query("schoolId") String schoolId, @Query("schoolYear") String schoolYear);

    @GET("/SchoolNotificationsServices/" + VERSION_NO + "/rest/getschoolevents/")
    Call<SchoolActivityResponse> getSchoolActivities(@Query("schoolId") String schoolId, @Query("schoolYear") String schoolYear);

    @POST("/SchoolNotificationsServices/" + VERSION_NO + "/rest/postevent/")
    Call<CreateClassActivityResponse> createClassActivities(@Body AddEventParams params);

    @POST("/SchoolNotificationsServices/" + VERSION_NO + "/rest/updateevent/")
    Call<CreateClassActivityResponse> updateClassActivities(@Body AddEventParams params);

    @GET("/SchoolBusServices/" + VERSION_NO + "/rest/getprofilebytype/")
    Call<GetQrCodeResponse> getQrCode(@Query("schoolId") String schoolId, @Query("profileId") String profileId, @Query("profileType") String profileType);

    @POST("/RouteServices/" + VERSION_NO + "/rest/createroute/")
    Call<CreateRouteResponse> createRoute(@Body CreateRouteParams params);

    @POST("/RouteServices/" + VERSION_NO + "/rest/updateroute/")
    Call<CommonResponse> updateRoute(@Body CreateRouteParams params);

    @GET("/SchoolBusServices/" + VERSION_NO + "/rest/getallrouteinfo/")
    Call<AllTripsForRoute> getAllTripsForRoute(@Query("schoolId") String schoolId, @Query("routeId") String routeId, @Query("recordDate") String recordDate);

    @GET("/SchoolBusServices/" + VERSION_NO + "/rest/gettriphistory/")
    Call<GetTripHistory> getTripHistory(@Query("schoolId") String schoolId, @Query("schoolYear") String schoolYear, @Query("tripId") String tripId);

    @GET("/SchoolServices/" + VERSION_NO + "/rest/getschoolbyid/")
    Call<GetSchoolInfo> getSchoolInfo(@Query("schoolId") String schoolId);

    @GET("/RouteServices/" + VERSION_NO + "/rest/passengersbytransport/")
    Call<GetPassangersResponse> getPassangers(@Query("schoolId") String schoolId, @Query("schoolYear") String schoolYear
            ,@Query("routeId") String routeId, @Query("routeType") String routeType);

    @POST("/RouteServices/" + VERSION_NO + "/rest/addpassengerstoroute/")
    Call<CommonResponse> assignPassanger(@Body AssignPassangerParams params);

    @POST("/DriverServices/" + VERSION_NO + "/rest/addroutetodriver/")
    Call<CommonResponse> assignDriverToRoute(@Body AssignDriverToRouteParams params);

    @GET("/RouteServices/" + VERSION_NO + "/rest/getfullrouteinfo/")
    Call<GetFullRouteInfo> getFullRouteInfo(@Query("schoolId") String schoolId, @Query("schoolYear") String schoolYear, @Query("routeId") String routeId, @Query("uniqueId") String uniqueId);

    @POST("/SchoolBusServices/" + VERSION_NO + "/rest/sendemergencymessage/")
    Call<CommonResponse> sendEmergencyCommunication(@Body SendEmergencyCommunicationParams params);

    @GET("/SchoolNotificationsServices/" + VERSION_NO + "/rest/deleteeventsseries/")
    Call<CommonResponse> deletEvents(@Query("schoolId") String schoolId, @Query("schoolYear") String schoolYear, @Query("notificationId") String notificationId
            , @Query("repeatEventId") String repeatEventId , @Query("notificationDate") String notificationDate , @Query("notificationEndDate") String notificationEndDate
            , @Query("deleteSeries") boolean deleteSeries);


    @POST("/SchoolServices/" + VERSION_NO + "/rest/updateschoolinfo/")
    Call<CommonResponse> updateSchoolInfo(@Body UpdateSchoolInfoParams params);

    @POST("/SchoolServices/" + VERSION_NO + "/rest/updateschoolpermissions/")
    Call<CommonResponse> updateSchoolPermissions(@Body UpdateSchoolLevelPermissionsParams params);
}
