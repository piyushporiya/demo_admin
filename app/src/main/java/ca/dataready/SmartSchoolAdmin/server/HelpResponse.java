/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;

import java.util.List;

public class HelpResponse {


    /**
     * message : Details are below
     * status : true
     * authtoken : null
     * result : [{"qestionText":"How to Register Student ?","videoUrl":"/schoolapp/images/129/c49ee175-bf5d-47b0-b65a-a1b18ddd6933.MP4","appType":"parent","videoName":"Register Student","module":"student","modifiedDate":"2018-04-03 06:17:21","comment":"string","language":"en","id":"AWKKJxyDITmueQOZEGLR","creationDate":"2018-04-03 06:17:21"}]
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private String authtokenexpires;
    private List<ResultBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * qestionText : How to Register Student ?
         * videoUrl : /schoolapp/images/129/c49ee175-bf5d-47b0-b65a-a1b18ddd6933.MP4
         * appType : parent
         * videoName : Register Student
         * module : student
         * modifiedDate : 2018-04-03 06:17:21
         * comment : string
         * language : en
         * id : AWKKJxyDITmueQOZEGLR
         * creationDate : 2018-04-03 06:17:21
         */

        private String qestionText;
        private String videoUrl;
        private String appType;
        private String videoName;
        private String module;
        private String modifiedDate;
        private String comment;
        private String language;
        private String id;
        private String creationDate;

        public String getQestionText() {
            return qestionText;
        }

        public void setQestionText(String qestionText) {
            this.qestionText = qestionText;
        }

        public String getVideoUrl() {
            return videoUrl;
        }

        public void setVideoUrl(String videoUrl) {
            this.videoUrl = videoUrl;
        }

        public String getAppType() {
            return appType;
        }

        public void setAppType(String appType) {
            this.appType = appType;
        }

        public String getVideoName() {
            return videoName;
        }

        public void setVideoName(String videoName) {
            this.videoName = videoName;
        }

        public String getModule() {
            return module;
        }

        public void setModule(String module) {
            this.module = module;
        }

        public String getModifiedDate() {
            return modifiedDate;
        }

        public void setModifiedDate(String modifiedDate) {
            this.modifiedDate = modifiedDate;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }
    }
}
