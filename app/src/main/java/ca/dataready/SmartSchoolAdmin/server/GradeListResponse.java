/*
 * Copyright (c) 2017.                                                       
 *                                                                           
 * ***********************************************************************  
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                           
 * ________________________________________________________________________  
 *                                                                           
 *    [2010] - [2018] SripathiSolutions Corp.                                
 *    [2010] - [2018] DataReady Technology Corp.                             
 *   ________________________________________________________________________
 *                                                                           
 *    All Rights Reserved.                                                   
 *                                                                           
 *   NOTICE:  All information contained herein is, and remains               
 *   the property of DataReady Technology  and its suppliers,                
 *   if any.  The intellectual and technical concepts contained              
 *   herein are proprietary to DataReady Technology Incorporated             
 *   and its suppliers and may be covered by U.S. and Foreign Patents,       
 *   patents in process, and are protected by trade secret or copyright law. 
 *   Dissemination of this information or reproduction of this material      
 *   is strictly forbidden unless prior written permission is obtained       
 *   from DataReady Technology Incorporated.                                 
 *                                                                           
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                            
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GradeListResponse {


    /**
     * message : Informations are below
     * status : true
     * authtoken : null
     * result : {"schoolId":"129","grade":[{"gradeName":"Grade 1","gradeId":"1","class":[{"classId":"1","className":"Class 1"}]}],"schoolYear":"2017-2018","id":"1292017-2018"}
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private ResultBean result;
    private String authtokenexpires;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public static class ResultBean {
        /**
         * schoolId : 129
         * grade : [{"gradeName":"Grade 1","gradeId":"1","class":[{"classId":"1","className":"Class 1"}]}]
         * schoolYear : 2017-2018
         * id : 1292017-2018
         */

        private String schoolId;
        private String schoolYear;
        private String id;
        private List<GradeBean> grade;

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getSchoolYear() {
            return schoolYear;
        }

        public void setSchoolYear(String schoolYear) {
            this.schoolYear = schoolYear;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public List<GradeBean> getGrade() {
            return grade;
        }

        public void setGrade(List<GradeBean> grade) {
            this.grade = grade;
        }

        public static class GradeBean {
            /**
             * gradeName : Grade 1
             * gradeId : 1
             * class : [{"classId":"1","className":"Class 1"}]
             */

            private String gradeName;
            private String gradeId;
            @SerializedName("class")
            private List<ClassBean> classX;
            private boolean isExpanded = false;

            public boolean isExpanded() {
                return isExpanded;
            }

            public void setExpanded(boolean expanded) {
                isExpanded = expanded;
            }

            public String getGradeName() {
                return gradeName;
            }

            public void setGradeName(String gradeName) {
                this.gradeName = gradeName;
            }

            public String getGradeId() {
                return gradeId;
            }

            public void setGradeId(String gradeId) {
                this.gradeId = gradeId;
            }

            public List<ClassBean> getClassX() {
                return classX;
            }

            public void setClassX(List<ClassBean> classX) {
                this.classX = classX;
            }

            public static class ClassBean {
                /**
                 * classId : 1
                 * className : Class 1
                 */

                private String classId;
                private String className;
                private boolean isAddMore = false;

                public boolean isAddMore() {
                    return isAddMore;
                }

                public void setAddMore(boolean addMore) {
                    isAddMore = addMore;
                }

                public String getClassId() {
                    return classId;
                }

                public void setClassId(String classId) {
                    this.classId = classId;
                }

                public String getClassName() {
                    return className;
                }

                public void setClassName(String className) {
                    this.className = className;
                }
            }
        }
    }
}
