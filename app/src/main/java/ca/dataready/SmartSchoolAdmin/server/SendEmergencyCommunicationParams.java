/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;

import java.util.List;

public class SendEmergencyCommunicationParams {


    /**
     * message : string
     * routeId : string
     * routeType : string
     * schoolId : string
     * schoolYear : string
     * staffEmailId : string
     * staffId : string
     * staffName : string
     * staffProfilePic : string
     * stopId : ["string"]
     * toId : [{"cardId":"string","profileId":"string","profileType":"string"}]
     * uniqueId : string
     */

    private String message;
    private String routeId;
    private String routeType;
    private String schoolId;
    private String schoolYear;
    private String staffEmailId;
    private String staffId;
    private String staffName;
    private String staffProfilePic;
    private String uniqueId;
    private List<String> stopId;
    private List<ToIdBean> toId;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getRouteType() {
        return routeType;
    }

    public void setRouteType(String routeType) {
        this.routeType = routeType;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getSchoolYear() {
        return schoolYear;
    }

    public void setSchoolYear(String schoolYear) {
        this.schoolYear = schoolYear;
    }

    public String getStaffEmailId() {
        return staffEmailId;
    }

    public void setStaffEmailId(String staffEmailId) {
        this.staffEmailId = staffEmailId;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getStaffProfilePic() {
        return staffProfilePic;
    }

    public void setStaffProfilePic(String staffProfilePic) {
        this.staffProfilePic = staffProfilePic;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public List<String> getStopId() {
        return stopId;
    }

    public void setStopId(List<String> stopId) {
        this.stopId = stopId;
    }

    public List<ToIdBean> getToId() {
        return toId;
    }

    public void setToId(List<ToIdBean> toId) {
        this.toId = toId;
    }

    public static class ToIdBean {
        /**
         * cardId : string
         * profileId : string
         * profileType : string
         */

        private String cardId;
        private String profileId;
        private String profileType;

        public String getCardId() {
            return cardId;
        }

        public void setCardId(String cardId) {
            this.cardId = cardId;
        }

        public String getProfileId() {
            return profileId;
        }

        public void setProfileId(String profileId) {
            this.profileId = profileId;
        }

        public String getProfileType() {
            return profileType;
        }

        public void setProfileType(String profileType) {
            this.profileType = profileType;
        }
    }
}
