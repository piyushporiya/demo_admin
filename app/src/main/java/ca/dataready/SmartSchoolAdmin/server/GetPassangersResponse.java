/*
 * Copyright (c) 2017.                                                       
 *                                                                           
 * ***********************************************************************  
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                           
 * ________________________________________________________________________  
 *                                                                           
 *    [2010] - [2018] SripathiSolutions Corp.                                
 *    [2010] - [2018] DataReady Technology Corp.                             
 *   ________________________________________________________________________
 *                                                                           
 *    All Rights Reserved.                                                   
 *                                                                           
 *   NOTICE:  All information contained herein is, and remains               
 *   the property of DataReady Technology  and its suppliers,                
 *   if any.  The intellectual and technical concepts contained              
 *   herein are proprietary to DataReady Technology Incorporated             
 *   and its suppliers and may be covered by U.S. and Foreign Patents,       
 *   patents in process, and are protected by trade secret or copyright law. 
 *   Dissemination of this information or reproduction of this material      
 *   is strictly forbidden unless prior written permission is obtained       
 *   from DataReady Technology Incorporated.                                 
 *                                                                           
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL                            
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;

import java.util.List;

public class GetPassangersResponse {


    /**
     * message : Data are below
     * status : true
     * authtoken : null
     * result : {"studentDetails":[{"lastName":"Singh","gradeId":"4","profilePic":"/schoolapp/images/1291252.jpg","geopoint":"","pickPoint":"","emailId":"narendrasingh@hotmail.com","requiresTransportation":"","parentId":null,"phoneNo":"2147483647","studentId":"1252","firstName":"Narendra","classId":"A","dob":"1987-01-02","schoolId":"129","schoolYear":"2017-2018","routeDetails":{"dropoffRouteId":"13568","dropoffStopId":"3","pickupRouteId":"12021612531","dropoffRouteName":"Udhana","name":null,"stopId":null,"pickupStopId":"2","id":null,"uniqueId":null,"pickupRouteName":"Pool 13"},"id":"12521294A2017-2018"},{"lastName":"mendapara","gradeId":"4","profilePic":"/schoolapp/images/81ad8e12-4b48-4a98-8825-09b72d6d96ef.png","geopoint":"","pickPoint":"","emailId":"hereedd@gmail.com","requiresTransportation":"","parentId":"1","phoneNo":"9933445566","studentId":"201801","firstName":"milan","classId":"A","dob":"1998-05-31","schoolId":"129","schoolYear":"2017-2018","routeDetails":{"dropoffRouteId":"13568","dropoffStopId":"2","pickupRouteId":"12021612531","dropoffRouteName":"Udhana","pickupStopId":"2","pickupRouteName":"Pool 13"},"id":"2018011294A2017-2018"}],"staffDetails":[{"lastName":"asdf","role":null,"address":"asadsf","gender":"female","profilePic":"/schoolapp/images/5b88e499-066f-441f-ae15-fec31213cd3c.png","geopoint":"42.221,-73.8732","emailId":"asdf","phoneNo":"asdf","firstName":"jky","schoolId":"129","routeDetails":{"dropoffRouteId":"13568","dropoffStopId":"3","pickupRouteId":"12921","dropoffRouteName":"Udhana","pickupStopId":"2","pickupRouteName":"noida dataready"},"userType":"teacher","id":"asdf"},{"lastName":"Teacher","role":null,"address":"nana varachha, surat","gender":"female","profilePic":"/schoolapp/images/ad39d36a-c9c4-422c-a160-abaa8d3a5ff9.png","geopoint":"21.23,72.9009","emailId":"dt@gmail.com","phoneNo":"098765432","firstName":"D","schoolId":"129","routeDetails":{"dropoffRouteId":"13568","dropoffStopId":"3","pickupRouteId":"12021612531","dropoffRouteName":"Udhana","pickupStopId":"3","pickupRouteName":"Pool 13"},"userType":"teacher","id":"dt@gmail.com"}]}
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private ResultBean result;
    private String authtokenexpires;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public static class ResultBean {
        private List<StudentDetailsBean> studentDetails;
        private List<StaffDetailsBean> staffDetails;

        public List<StudentDetailsBean> getStudentDetails() {
            return studentDetails;
        }

        public void setStudentDetails(List<StudentDetailsBean> studentDetails) {
            this.studentDetails = studentDetails;
        }

        public List<StaffDetailsBean> getStaffDetails() {
            return staffDetails;
        }

        public void setStaffDetails(List<StaffDetailsBean> staffDetails) {
            this.staffDetails = staffDetails;
        }

        public static class StudentDetailsBean {
            /**
             * lastName : Singh
             * gradeId : 4
             * profilePic : /schoolapp/images/1291252.jpg
             * geopoint : 
             * pickPoint : 
             * emailId : narendrasingh@hotmail.com
             * requiresTransportation : 
             * parentId : null
             * phoneNo : 2147483647
             * studentId : 1252
             * firstName : Narendra
             * classId : A
             * dob : 1987-01-02
             * schoolId : 129
             * schoolYear : 2017-2018
             * routeDetails : {"dropoffRouteId":"13568","dropoffStopId":"3","pickupRouteId":"12021612531","dropoffRouteName":"Udhana","name":null,"stopId":null,"pickupStopId":"2","id":null,"uniqueId":null,"pickupRouteName":"Pool 13"}
             * id : 12521294A2017-2018
             */

            private String lastName;
            private String gradeId;
            private String profilePic;
            private String geopoint;
            private String pickPoint;
            private String emailId;
            private String requiresTransportation;
            private String parentId;
            private String phoneNo;
            private String studentId;
            private String firstName;
            private String classId;
            private String dob;
            private String schoolId;
            private String schoolYear;
            private RouteDetailsBean routeDetails;
            private String id;

            public String getLastName() {
                return lastName;
            }

            public void setLastName(String lastName) {
                this.lastName = lastName;
            }

            public String getGradeId() {
                return gradeId;
            }

            public void setGradeId(String gradeId) {
                this.gradeId = gradeId;
            }

            public String getProfilePic() {
                return profilePic;
            }

            public void setProfilePic(String profilePic) {
                this.profilePic = profilePic;
            }

            public String getGeopoint() {
                return geopoint;
            }

            public void setGeopoint(String geopoint) {
                this.geopoint = geopoint;
            }

            public String getPickPoint() {
                return pickPoint;
            }

            public void setPickPoint(String pickPoint) {
                this.pickPoint = pickPoint;
            }

            public String getEmailId() {
                return emailId;
            }

            public void setEmailId(String emailId) {
                this.emailId = emailId;
            }

            public String getRequiresTransportation() {
                return requiresTransportation;
            }

            public void setRequiresTransportation(String requiresTransportation) {
                this.requiresTransportation = requiresTransportation;
            }

            public String getParentId() {
                return parentId;
            }

            public void setParentId(String parentId) {
                this.parentId = parentId;
            }

            public String getPhoneNo() {
                return phoneNo;
            }

            public void setPhoneNo(String phoneNo) {
                this.phoneNo = phoneNo;
            }

            public String getStudentId() {
                return studentId;
            }

            public void setStudentId(String studentId) {
                this.studentId = studentId;
            }

            public String getFirstName() {
                return firstName;
            }

            public void setFirstName(String firstName) {
                this.firstName = firstName;
            }

            public String getClassId() {
                return classId;
            }

            public void setClassId(String classId) {
                this.classId = classId;
            }

            public String getDob() {
                return dob;
            }

            public void setDob(String dob) {
                this.dob = dob;
            }

            public String getSchoolId() {
                return schoolId;
            }

            public void setSchoolId(String schoolId) {
                this.schoolId = schoolId;
            }

            public String getSchoolYear() {
                return schoolYear;
            }

            public void setSchoolYear(String schoolYear) {
                this.schoolYear = schoolYear;
            }

            public RouteDetailsBean getRouteDetails() {
                return routeDetails;
            }

            public void setRouteDetails(RouteDetailsBean routeDetails) {
                this.routeDetails = routeDetails;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public static class RouteDetailsBean {
                /**
                 * dropoffRouteId : 13568
                 * dropoffStopId : 3
                 * pickupRouteId : 12021612531
                 * dropoffRouteName : Udhana
                 * name : null
                 * stopId : null
                 * pickupStopId : 2
                 * id : null
                 * uniqueId : null
                 * pickupRouteName : Pool 13
                 */

                private String dropoffRouteId;
                private String dropoffStopId;
                private String pickupRouteId;
                private String dropoffRouteName;
                private String name;
                private String stopId;
                private String pickupStopId;
                private String id;
                private String uniqueId;
                private String pickupRouteName;

                public String getDropoffRouteId() {
                    return dropoffRouteId;
                }

                public void setDropoffRouteId(String dropoffRouteId) {
                    this.dropoffRouteId = dropoffRouteId;
                }

                public String getDropoffStopId() {
                    return dropoffStopId;
                }

                public void setDropoffStopId(String dropoffStopId) {
                    this.dropoffStopId = dropoffStopId;
                }

                public String getPickupRouteId() {
                    return pickupRouteId;
                }

                public void setPickupRouteId(String pickupRouteId) {
                    this.pickupRouteId = pickupRouteId;
                }

                public String getDropoffRouteName() {
                    return dropoffRouteName;
                }

                public void setDropoffRouteName(String dropoffRouteName) {
                    this.dropoffRouteName = dropoffRouteName;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getStopId() {
                    return stopId;
                }

                public void setStopId(String stopId) {
                    this.stopId = stopId;
                }

                public String getPickupStopId() {
                    return pickupStopId;
                }

                public void setPickupStopId(String pickupStopId) {
                    this.pickupStopId = pickupStopId;
                }

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getUniqueId() {
                    return uniqueId;
                }

                public void setUniqueId(String uniqueId) {
                    this.uniqueId = uniqueId;
                }

                public String getPickupRouteName() {
                    return pickupRouteName;
                }

                public void setPickupRouteName(String pickupRouteName) {
                    this.pickupRouteName = pickupRouteName;
                }
            }
        }

        public static class StaffDetailsBean {
            /**
             * lastName : asdf
             * role : null
             * address : asadsf
             * gender : female
             * profilePic : /schoolapp/images/5b88e499-066f-441f-ae15-fec31213cd3c.png
             * geopoint : 42.221,-73.8732
             * emailId : asdf
             * phoneNo : asdf
             * firstName : jky
             * schoolId : 129
             * routeDetails : {"dropoffRouteId":"13568","dropoffStopId":"3","pickupRouteId":"12921","dropoffRouteName":"Udhana","pickupStopId":"2","pickupRouteName":"noida dataready"}
             * userType : teacher
             * id : asdf
             */

            private String lastName;
            private String role;
            private String address;
            private String gender;
            private String profilePic;
            private String geopoint;
            private String emailId;
            private String phoneNo;
            private String firstName;
            private String schoolId;
            private RouteDetailsBeanX routeDetails;
            private String userType;
            private String id;

            public String getLastName() {
                return lastName;
            }

            public void setLastName(String lastName) {
                this.lastName = lastName;
            }

            public String getRole() {
                return role;
            }

            public void setRole(String role) {
                this.role = role;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getProfilePic() {
                return profilePic;
            }

            public void setProfilePic(String profilePic) {
                this.profilePic = profilePic;
            }

            public String getGeopoint() {
                return geopoint;
            }

            public void setGeopoint(String geopoint) {
                this.geopoint = geopoint;
            }

            public String getEmailId() {
                return emailId;
            }

            public void setEmailId(String emailId) {
                this.emailId = emailId;
            }

            public String getPhoneNo() {
                return phoneNo;
            }

            public void setPhoneNo(String phoneNo) {
                this.phoneNo = phoneNo;
            }

            public String getFirstName() {
                return firstName;
            }

            public void setFirstName(String firstName) {
                this.firstName = firstName;
            }

            public String getSchoolId() {
                return schoolId;
            }

            public void setSchoolId(String schoolId) {
                this.schoolId = schoolId;
            }

            public RouteDetailsBeanX getRouteDetails() {
                return routeDetails;
            }

            public void setRouteDetails(RouteDetailsBeanX routeDetails) {
                this.routeDetails = routeDetails;
            }

            public String getUserType() {
                return userType;
            }

            public void setUserType(String userType) {
                this.userType = userType;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public static class RouteDetailsBeanX {
                /**
                 * dropoffRouteId : 13568
                 * dropoffStopId : 3
                 * pickupRouteId : 12921
                 * dropoffRouteName : Udhana
                 * pickupStopId : 2
                 * pickupRouteName : noida dataready
                 */

                private String dropoffRouteId;
                private String dropoffStopId;
                private String pickupRouteId;
                private String dropoffRouteName;
                private String pickupStopId;
                private String pickupRouteName;

                public String getDropoffRouteId() {
                    return dropoffRouteId;
                }

                public void setDropoffRouteId(String dropoffRouteId) {
                    this.dropoffRouteId = dropoffRouteId;
                }

                public String getDropoffStopId() {
                    return dropoffStopId;
                }

                public void setDropoffStopId(String dropoffStopId) {
                    this.dropoffStopId = dropoffStopId;
                }

                public String getPickupRouteId() {
                    return pickupRouteId;
                }

                public void setPickupRouteId(String pickupRouteId) {
                    this.pickupRouteId = pickupRouteId;
                }

                public String getDropoffRouteName() {
                    return dropoffRouteName;
                }

                public void setDropoffRouteName(String dropoffRouteName) {
                    this.dropoffRouteName = dropoffRouteName;
                }

                public String getPickupStopId() {
                    return pickupStopId;
                }

                public void setPickupStopId(String pickupStopId) {
                    this.pickupStopId = pickupStopId;
                }

                public String getPickupRouteName() {
                    return pickupRouteName;
                }

                public void setPickupRouteName(String pickupRouteName) {
                    this.pickupRouteName = pickupRouteName;
                }
            }
        }
    }
}
