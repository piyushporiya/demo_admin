/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;


import java.util.List;

public class AddStudentParams {


    /**
     * academicInfo : true
     * address : string
     * birthPlace : string
     * dateofJoining : string
     * dob : string
     * files : [{"fileName":"string","filePath":"string"}]
     * firstName : string
     * gender : string
     * geopoint : string
     * guardianDetails : {"address":"string","dob":"string","education":"string","emailId":"string","files":[{"fileName":"string","filePath":"string"}],"guardianFirstName":"string","guardianLastName":"string","occupation":"string","phoneNo":"string","relation":"string"}
     * id : string
     * lastName : string
     * motherTounge : string
     * nationality : string
     * notes : {"schoolCounselor":"string","transportationCounselor":"string"}
     * parentDetails : {"fatherEmailId":"string","fatherFirstName":"string","fatherLastName":"string","fatherOccupation":"string","fatherPhoneNo":"string","fatherProfilePic":"string","files":[{"fileName":"string","filePath":"string"}],"motherEmailId":"string","motherFirstName":"string","motherLastName":"string","motherOccupation":"string","motherPhoneNo":"string","motherProfilePic":"string"}
     * pastSchoolDetails : {"address":"string","classId":"string","files":[{"fileName":"string","filePath":"string"}],"gradeId":"string","schoolName":"string","yearOfPassing":"string"}
     * physicianDetails : {"address":"string","city":"string","country":"string","email":"string","fax":"string","firstName":"string","lastName":"string","phone":"string","state":"string"}
     * pickPoint : string
     * profilePic : string
     * religion : string
     * requiresTransportation : string
     * schoolId : string
     * studentId : string
     */

    private boolean academicInfo;
    private String address;
    private String birthPlace;
    private String dateofJoining;
    private String dob;
    private String firstName;
    private String gender;
    private String geopoint;
    private GuardianDetailsBean guardianDetails;
    private String id;
    private String lastName;
    private String motherTounge;
    private String nationality;
    private NotesBean notes;
    private PhysicianDetailsBean physicianDetails;
    private String requiresTransportation;
    private ParentDetailsBean parentDetails;
    private PastSchoolDetailsBean pastSchoolDetails;
    private String pickPoint;
    private String profilePic;
    private String religion;
    private String schoolId;
    private String studentId;
    private List<FilesBeanXXX> files;

    public boolean isAcademicInfo() {
        return academicInfo;
    }

    public void setAcademicInfo(boolean academicInfo) {
        this.academicInfo = academicInfo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getDateofJoining() {
        return dateofJoining;
    }

    public void setDateofJoining(String dateofJoining) {
        this.dateofJoining = dateofJoining;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGeopoint() {
        return geopoint;
    }

    public void setGeopoint(String geopoint) {
        this.geopoint = geopoint;
    }

    public GuardianDetailsBean getGuardianDetails() {
        return guardianDetails;
    }

    public void setGuardianDetails(GuardianDetailsBean guardianDetails) {
        this.guardianDetails = guardianDetails;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMotherTounge() {
        return motherTounge;
    }

    public void setMotherTounge(String motherTounge) {
        this.motherTounge = motherTounge;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public NotesBean getNotes() {
        return notes;
    }

    public void setNotes(NotesBean notes) {
        this.notes = notes;
    }

    public ParentDetailsBean getParentDetails() {
        return parentDetails;
    }

    public void setParentDetails(ParentDetailsBean parentDetails) {
        this.parentDetails = parentDetails;
    }

    public PastSchoolDetailsBean getPastSchoolDetails() {
        return pastSchoolDetails;
    }

    public void setPastSchoolDetails(PastSchoolDetailsBean pastSchoolDetails) {
        this.pastSchoolDetails = pastSchoolDetails;
    }

    public PhysicianDetailsBean getPhysicianDetails() {
        return physicianDetails;
    }

    public void setPhysicianDetails(PhysicianDetailsBean physicianDetails) {
        this.physicianDetails = physicianDetails;
    }

    public String getPickPoint() {
        return pickPoint;
    }

    public void setPickPoint(String pickPoint) {
        this.pickPoint = pickPoint;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getRequiresTransportation() {
        return requiresTransportation;
    }

    public void setRequiresTransportation(String requiresTransportation) {
        this.requiresTransportation = requiresTransportation;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public List<FilesBeanXXX> getFiles() {
        return files;
    }

    public void setFiles(List<FilesBeanXXX> files) {
        this.files = files;
    }

    public static class GuardianDetailsBean {
        /**
         * address : string
         * dob : string
         * education : string
         * emailId : string
         * files : [{"fileName":"string","filePath":"string"}]
         * guardianFirstName : string
         * guardianLastName : string
         * occupation : string
         * phoneNo : string
         * relation : string
         */

        private String address;
        private String dob;
        private String education;
        private String emailId;
        private String guardianFirstName;
        private String guardianLastName;
        private String occupation;
        private String phoneNo;
        private String relation;
        private List<FilesBean> files;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getEducation() {
            return education;
        }

        public void setEducation(String education) {
            this.education = education;
        }

        public String getEmailId() {
            return emailId;
        }

        public void setEmailId(String emailId) {
            this.emailId = emailId;
        }

        public String getGuardianFirstName() {
            return guardianFirstName;
        }

        public void setGuardianFirstName(String guardianFirstName) {
            this.guardianFirstName = guardianFirstName;
        }

        public String getGuardianLastName() {
            return guardianLastName;
        }

        public void setGuardianLastName(String guardianLastName) {
            this.guardianLastName = guardianLastName;
        }

        public String getOccupation() {
            return occupation;
        }

        public void setOccupation(String occupation) {
            this.occupation = occupation;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public String getRelation() {
            return relation;
        }

        public void setRelation(String relation) {
            this.relation = relation;
        }

        public List<FilesBean> getFiles() {
            return files;
        }

        public void setFiles(List<FilesBean> files) {
            this.files = files;
        }

        public static class FilesBean {
            /**
             * fileName : string
             * filePath : string
             */

            private String fileName;
            private String filePath;

            public String getFileName() {
                return fileName;
            }

            public void setFileName(String fileName) {
                this.fileName = fileName;
            }

            public String getFilePath() {
                return filePath;
            }

            public void setFilePath(String filePath) {
                this.filePath = filePath;
            }
        }
    }

    public static class NotesBean {
        /**
         * schoolCounselor : string
         * transportationCounselor : string
         */

        private String schoolCounselor;
        private String transportationCounselor;

        public String getSchoolCounselor() {
            return schoolCounselor;
        }

        public void setSchoolCounselor(String schoolCounselor) {
            this.schoolCounselor = schoolCounselor;
        }

        public String getTransportationCounselor() {
            return transportationCounselor;
        }

        public void setTransportationCounselor(String transportationCounselor) {
            this.transportationCounselor = transportationCounselor;
        }
    }

    public static class ParentDetailsBean {
        /**
         * fatherEmailId : string
         * fatherFirstName : string
         * fatherLastName : string
         * fatherOccupation : string
         * fatherPhoneNo : string
         * fatherProfilePic : string
         * files : [{"fileName":"string","filePath":"string"}]
         * motherEmailId : string
         * motherFirstName : string
         * motherLastName : string
         * motherOccupation : string
         * motherPhoneNo : string
         * motherProfilePic : string
         */

        private String fatherEmailId;
        private String fatherFirstName;
        private String fatherLastName;
        private String fatherOccupation;
        private String fatherPhoneNo;
        private String fatherProfilePic;
        private String motherEmailId;
        private String motherFirstName;
        private String motherLastName;
        private String motherOccupation;
        private String motherPhoneNo;
        private String motherProfilePic;
        private List<FilesBeanX> files;

        public String getFatherEmailId() {
            return fatherEmailId;
        }

        public void setFatherEmailId(String fatherEmailId) {
            this.fatherEmailId = fatherEmailId;
        }

        public String getFatherFirstName() {
            return fatherFirstName;
        }

        public void setFatherFirstName(String fatherFirstName) {
            this.fatherFirstName = fatherFirstName;
        }

        public String getFatherLastName() {
            return fatherLastName;
        }

        public void setFatherLastName(String fatherLastName) {
            this.fatherLastName = fatherLastName;
        }

        public String getFatherOccupation() {
            return fatherOccupation;
        }

        public void setFatherOccupation(String fatherOccupation) {
            this.fatherOccupation = fatherOccupation;
        }

        public String getFatherPhoneNo() {
            return fatherPhoneNo;
        }

        public void setFatherPhoneNo(String fatherPhoneNo) {
            this.fatherPhoneNo = fatherPhoneNo;
        }

        public String getFatherProfilePic() {
            return fatherProfilePic;
        }

        public void setFatherProfilePic(String fatherProfilePic) {
            this.fatherProfilePic = fatherProfilePic;
        }

        public String getMotherEmailId() {
            return motherEmailId;
        }

        public void setMotherEmailId(String motherEmailId) {
            this.motherEmailId = motherEmailId;
        }

        public String getMotherFirstName() {
            return motherFirstName;
        }

        public void setMotherFirstName(String motherFirstName) {
            this.motherFirstName = motherFirstName;
        }

        public String getMotherLastName() {
            return motherLastName;
        }

        public void setMotherLastName(String motherLastName) {
            this.motherLastName = motherLastName;
        }

        public String getMotherOccupation() {
            return motherOccupation;
        }

        public void setMotherOccupation(String motherOccupation) {
            this.motherOccupation = motherOccupation;
        }

        public String getMotherPhoneNo() {
            return motherPhoneNo;
        }

        public void setMotherPhoneNo(String motherPhoneNo) {
            this.motherPhoneNo = motherPhoneNo;
        }

        public String getMotherProfilePic() {
            return motherProfilePic;
        }

        public void setMotherProfilePic(String motherProfilePic) {
            this.motherProfilePic = motherProfilePic;
        }

        public List<FilesBeanX> getFiles() {
            return files;
        }

        public void setFiles(List<FilesBeanX> files) {
            this.files = files;
        }

        public static class FilesBeanX {
            /**
             * fileName : string
             * filePath : string
             */

            private String fileName;
            private String filePath;

            public String getFileName() {
                return fileName;
            }

            public void setFileName(String fileName) {
                this.fileName = fileName;
            }

            public String getFilePath() {
                return filePath;
            }

            public void setFilePath(String filePath) {
                this.filePath = filePath;
            }
        }
    }

    public static class PastSchoolDetailsBean {
        /**
         * address : string
         * classId : string
         * files : [{"fileName":"string","filePath":"string"}]
         * gradeId : string
         * schoolName : string
         * yearOfPassing : string
         */

        private String address;
        private String classId;
        private String gradeId;
        private String schoolName;
        private String yearOfPassing;
        private List<FilesBeanXX> files;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getGradeId() {
            return gradeId;
        }

        public void setGradeId(String gradeId) {
            this.gradeId = gradeId;
        }

        public String getSchoolName() {
            return schoolName;
        }

        public void setSchoolName(String schoolName) {
            this.schoolName = schoolName;
        }

        public String getYearOfPassing() {
            return yearOfPassing;
        }

        public void setYearOfPassing(String yearOfPassing) {
            this.yearOfPassing = yearOfPassing;
        }

        public List<FilesBeanXX> getFiles() {
            return files;
        }

        public void setFiles(List<FilesBeanXX> files) {
            this.files = files;
        }

        public static class FilesBeanXX {
            /**
             * fileName : string
             * filePath : string
             */

            private String fileName;
            private String filePath;

            public String getFileName() {
                return fileName;
            }

            public void setFileName(String fileName) {
                this.fileName = fileName;
            }

            public String getFilePath() {
                return filePath;
            }

            public void setFilePath(String filePath) {
                this.filePath = filePath;
            }
        }
    }

    public static class PhysicianDetailsBean {
        /**
         * address : string
         * city : string
         * country : string
         * email : string
         * fax : string
         * firstName : string
         * lastName : string
         * phone : string
         * state : string
         */

        private String address;
        private String city;
        private String country;
        private String email;
        private String fax;
        private String firstName;
        private String lastName;
        private String phone;
        private String state;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getFax() {
            return fax;
        }

        public void setFax(String fax) {
            this.fax = fax;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }
    }

    public static class FilesBeanXXX {
        /**
         * fileName : string
         * filePath : string
         */

        private String fileName;
        private String filePath;

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public String getFilePath() {
            return filePath;
        }

        public void setFilePath(String filePath) {
            this.filePath = filePath;
        }
    }
}
