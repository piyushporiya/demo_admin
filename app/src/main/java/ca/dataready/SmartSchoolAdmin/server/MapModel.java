/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by social_jaydeep on 03/07/17.
 */

public class MapModel implements Parcelable {

    private double lat;
    private double lon;
    private String title;
    private String stopId;
    private boolean isSchool;
    private boolean isOrigin;
    private boolean isStartOrEnd;

    public MapModel(double lat, double lon, String title, String stopId, boolean isschool) {
        this.lat = lat;
        this.isSchool = isschool;
        this.lon = lon;
        this.title = title;
        this.stopId = stopId;
    }

    public MapModel(double lat, double lon, String title, String stopId) {
        this.lat = lat;
        this.lon = lon;
        this.title = title;
        this.stopId = stopId;
    }

    public MapModel(double lat, double lon, String title, boolean isSchool) {
        this.lat = lat;
        this.lon = lon;
        this.title = title;
        this.isSchool = isSchool;
    }

    public MapModel(double lat, double lon, String title, boolean isSchool, boolean isStartOrEnd) {
        this.lat = lat;
        this.lon = lon;
        this.title = title;
        this.isSchool = isSchool;
        this.isStartOrEnd = isStartOrEnd;
    }

    public boolean isOrigin() {
        return isOrigin;
    }

    public void setOrigin(boolean origin) {
        isOrigin = origin;
    }

    public boolean isSchool() {
        return isSchool;
    }

    public void setSchool(boolean school) {
        isSchool = school;
    }

    public String getStopId() {

        return stopId;
    }

    public void setStopId(String stopId) {
        this.stopId = stopId;
    }

    public static Creator<MapModel> getCREATOR() {
        return CREATOR;
    }

    public MapModel(double lat, double lon, String title) {
        this.lat = lat;
        this.lon = lon;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public boolean isStartOrEnd() {
        return isStartOrEnd;
    }

    public void setStartOrEnd(boolean startOrEnd) {
        isStartOrEnd = startOrEnd;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.lat);
        dest.writeDouble(this.lon);
        dest.writeString(this.title);
        dest.writeString(this.stopId);
    }

    protected MapModel(Parcel in) {
        this.lat = in.readDouble();
        this.lon = in.readDouble();
        this.title = in.readString();
        this.stopId = in.readString();
    }

    public static final Creator<MapModel> CREATOR = new Creator<MapModel>() {
        @Override
        public MapModel createFromParcel(Parcel source) {
            return new MapModel(source);
        }

        @Override
        public MapModel[] newArray(int size) {
            return new MapModel[size];
        }
    };
}
