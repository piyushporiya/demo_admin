/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;

public class AssignUpdateTeacherParams {

    /**
     * classId : string
     * gradeId : string
     * id : string
     * schoolId : string
     * schoolYear : YYYY-YYYY
     * subjectEndTime : HH:mm
     * subjectName : string
     * subjectStartTime : HH:mm
     * teacherEmailId : string
     * teacherId : string
     * teacherMobileNo : string
     * teacherName : string
     * weekDay : string
     */

    private String classId;
    private String gradeId;
    private String id;
    private String schoolId;
    private String schoolYear;
    private String subjectEndTime;
    private String subjectName;
    private String subjectStartTime;
    private String teacherEmailId;
    private String teacherId;
    private String teacherMobileNo;
    private String teacherName;
    private String weekDay;

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getGradeId() {
        return gradeId;
    }

    public void setGradeId(String gradeId) {
        this.gradeId = gradeId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getSchoolYear() {
        return schoolYear;
    }

    public void setSchoolYear(String schoolYear) {
        this.schoolYear = schoolYear;
    }

    public String getSubjectEndTime() {
        return subjectEndTime;
    }

    public void setSubjectEndTime(String subjectEndTime) {
        this.subjectEndTime = subjectEndTime;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectStartTime() {
        return subjectStartTime;
    }

    public void setSubjectStartTime(String subjectStartTime) {
        this.subjectStartTime = subjectStartTime;
    }

    public String getTeacherEmailId() {
        return teacherEmailId;
    }

    public void setTeacherEmailId(String teacherEmailId) {
        this.teacherEmailId = teacherEmailId;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getTeacherMobileNo() {
        return teacherMobileNo;
    }

    public void setTeacherMobileNo(String teacherMobileNo) {
        this.teacherMobileNo = teacherMobileNo;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(String weekDay) {
        this.weekDay = weekDay;
    }
}
