/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class GetAllDriverResponse {


    /**
     * message : Driver details are below
     * status : true
     * authtoken : null
     * result : [{"lastName":"Test1","country":"yuuu","deactivationDate":null,"issuedDate":"08/02/2018","driverPin":"5444","gender":null,"city":null,"issuedBy":null,"emailId":"drivertest@gmail.com","isActive":true,"phoneNo":"95333","expiryDate":"08/02/2018","presentAddress":"Surat ","schoolId":"129","state":null,"department":null,"profilePic":"/schoolapp/images/6206972f-bf6c-4fda-87da-50d72963c85c.png","geopoint":"21.1702,72.8311","certificateNo":"855678","firstName":"Driver","parmanentAddress":"Raipur ","driverId":"drivertest@gmail.com","dob":"08/02/2018","routeDetails":[{"name":"dabholi","id":"8790","routeType":"Dropoff","uniqueId":"1"}],"emergencyDetails":{"firstName":null,"lastName":null,"address":null,"emailId":null,"phoneNo":null,"relation":null},"activationDate":"2018-02-08 14:34:08 UTC"},{"lastName":"Y.   K","country":"India","deactivationDate":null,"issuedDate":"09/02/2018","driverPin":"5187","gender":null,"city":null,"issuedBy":null,"emailId":"ndndndnd@dhdndjjdd.com","isActive":true,"phoneNo":"7438383838383","expiryDate":"09/02/2018","presentAddress":"noida up","schoolId":"129","state":null,"department":null,"profilePic":"/schoolapp/images/db5982a8-31fb-4cc3-a91f-bc16f926252e.png","geopoint":"28.5355,77.391","certificateNo":"83838eejdjdnjd","firstName":"J","parmanentAddress":"greater noida up","driverId":"ndndndnd@dhdndjjdd.com","dob":"09/02/2018","routeDetails":[{"name":"D Route 1","id":"30012018","routeType":"Pickup","uniqueId":"636528900590937344"}],"emergencyDetails":{"firstName":null,"lastName":null,"address":null,"emailId":null,"phoneNo":null,"relation":null},"activationDate":"2018-02-09 11:56:11 UTC"},{"lastName":"Yadav","country":"India","deactivationDate":null,"issuedDate":"09/02/2018","driverPin":"1661","gender":null,"city":null,"issuedBy":null,"emailId":"umesh@techsoftlabs.com","isActive":true,"phoneNo":"38388383838993","expiryDate":"09/02/2027","presentAddress":"ashok nagar ,new delhi","schoolId":"129","state":null,"department":null,"profilePic":"/schoolapp/images/8bb2e518-d7df-4706-86c2-c9c47c4e8d01.png","geopoint":"28.6374,77.1014","certificateNo":"gdgdgdggd636637373&","firstName":"Jitu","parmanentAddress":"Dabauli,kanpur up","driverId":"umesh@techsoftlabs.com","dob":"09/02/1991","routeDetails":[{"name":"maheshbhai","id":"12916","routeType":"pickup","uniqueId":"1"},{"name":"30012035","id":"pool 6","routeType":"Pickup","uniqueId":"636530011818453760"},{"name":"noida dataready","id":"12921","routeType":"pickup","uniqueId":"1"}],"emergencyDetails":{"firstName":null,"lastName":null,"address":"gujaini kanpur up","emailId":null,"phoneNo":null,"relation":null},"activationDate":"2018-02-09 07:09:51 UTC"},{"lastName":"Poriya","country":"IN","deactivationDate":"2017-12-31 23:59:59 UTC","issuedDate":null,"driverPin":"1234","gender":null,"city":"Gujrat","postalCode":"","issuedBy":null,"emailId":"piyushporiya@techsoftlabs.com","isActive":"true","phoneNo":"123456","expiryDate":null,"presentAddress":null,"schoolId":"129","state":"Gujrat","department":null,"address":"Surat","profilePic":"/schoolapp/images/43b6206f-3715-42cf-b3de-27a6b75bd3c4.jpg","geopoint":null,"certificateNo":null,"firstName":"Piyush","parmanentAddress":null,"driverId":"piyushporiya@techsoftlabs.com","routeDetails":[{"name":"SP9-HOME","id":"9","routeType":"PICKUP","uniqueId":"694092"},{"name":"SP9-OFFICE","id":"9","routeType":"DROPOFF","uniqueId":"6940921"},{"name":"SITR","id":"12901","routeType":"PICKUP","uniqueId":"1"},{"name":"SP9-HOME","id":"9694092SP9-HOME","routeType":"pickup","uniqueId":"694092"},{"name":"varacha surat","id":"129130020181636529897323589376","routeType":"Pickup","uniqueId":"636529897323589376"},{"name":"pool 2","id":"12930012031636529990577977600","routeType":"Pickup","uniqueId":"636529990577977600"},{"name":"varacha","id":"129123451","routeType":"Dropoff","uniqueId":"1"},{"name":"pool4","id":"129300120330636530009027735936","routeType":"Pickup","uniqueId":"636530009027735936"},{"name":"30012034","id":"129pool 5636530010659056000","routeType":"Pickup","uniqueId":"636530010659056000"},{"name":"30012035","id":"129pool 6636530011818453760","routeType":"Pickup","uniqueId":"636530011818453760"},{"name":"udhana","id":"129135691","routeType":"Pickup","uniqueId":"1"},{"name":"kapodara","id":"1293001201800636529009144680192","routeType":"Pickup","uniqueId":"636529009144680192"},{"name":"surat","id":"1298789636528887714325376","routeType":"Dropoff","uniqueId":"636528887714325376"},{"name":"pool 7","id":"1290201020636530647509843328","routeType":"Pickup","uniqueId":"636530647509843328"},{"name":"Udhana","id":"129135681","routeType":"Dropoff","uniqueId":"1"},{"name":"katargam","id":"12912341","routeType":"Pickup","uniqueId":"1"},{"name":"Pool 11","id":"129200100636530799030966400","routeType":"Pickup","uniqueId":"636530799030966400"},{"name":"Pool 13","id":"12912021612531636530841174379008","routeType":"Pickup","uniqueId":"636530841174379008"},{"name":"Social Route","id":"129102011636530878117669248","routeType":"Pickup","uniqueId":"636530878117669248"},{"name":"Social Route","id":"102011","routeType":"Pickup","uniqueId":"636530878117669248"},{"name":"surat","id":"8789","routeType":"Dropoff","uniqueId":"636528887714325376"},{"name":"DL route","id":"12907","routeType":"pickup","uniqueId":"1"},{"name":"Udhana","id":"13568","routeType":"Dropoff","uniqueId":"1"},{"name":"katargam","id":"1234","routeType":"Pickup","uniqueId":"1"},{"name":"Ram Drop","id":"12908","routeType":"dropoff","uniqueId":"1"},{"name":"dabholi","id":"879","routeType":"Pickup","uniqueId":"1"},{"name":"asdf","id":"123","routeType":"Pickup","uniqueId":"1"},{"name":"Pickup Darshit Test route","id":"12922","routeType":"pickup","uniqueId":"1"},{"name":"Dropoff Darshit test route","id":"12923","routeType":"dropoff","uniqueId":"1"}],"position":null,"activationDate":"2017-08-14 08:34:12 UTC"},{"lastName":"patel","country":"india","deactivationDate":null,"issuedDate":"26/01/2018","driverPin":"5374","gender":null,"city":null,"issuedBy":null,"emailId":"mukesh@gmail.com","isActive":true,"phoneNo":"456478941536","expiryDate":"26/01/2022","presentAddress":"katargam surat","schoolId":"129","state":null,"department":null,"profilePic":"/schoolapp/images/5e38b284-0d8c-447a-8b26-ca94b03c9dbd.png","geopoint":null,"certificateNo":"456132","firstName":"mukesh","parmanentAddress":"varcha surat","driverId":"mukesh@gmail.com","dob":"26/01/1995","files":[{"fileName":"Apple vs Android.pdf","filePath":"/schoolapp/images/45e75072-cc59-428c-9feb-b584d10f3e6b.pdf"}],"routeDetails":[{"name":"jakatnaka, surat","id":"12987651","routeType":"Pickup","uniqueId":"1"},{"name":"noida ","id":"1291232123636531454242735488","routeType":"Pickup","uniqueId":"636531454242735488"}],"emergencyDetails":{"firstName":"maheshbhai","lastName":"patel","address":"surat","files":[{"fileName":"asset.PNG","filePath":"/schoolapp/images/fabb662f-9fef-4b5d-b7dc-24b943d3e812.PNG"}],"emailId":"maheshbhail@gmail.com","phoneNo":"4564768913","relation":"uncle"},"activationDate":"2018-01-26 07:29:26 UTC"},{"lastName":"Patel","country":"Uk","deactivationDate":null,"issuedDate":"26/01/2018","driverPin":"5646","gender":null,"city":null,"issuedBy":null,"emailId":"jay@gmail.com","isActive":true,"phoneNo":"987654325","expiryDate":"26/01/2035","presentAddress":"katargam s","schoolId":"129","state":null,"department":null,"profilePic":"/schoolapp/images/99d02469-a88c-45b7-8d1b-2dfa50aa3341.png","geopoint":"21.2281,72.8338","certificateNo":"5687889","firstName":"Jay","parmanentAddress":"surat","driverId":"jay@gmail.com","dob":"26/01/1991","files":[{"fileName":"asset.PNG","filePath":"/schoolapp/images/cd8fa8f6-7100-4936-83dd-5b951c38df84.PNG"},{"fileName":"Apple vs Android.pdf","filePath":"/schoolapp/images/8ce9a4d6-b49c-4471-882b-19013776a37b.pdf"}],"routeDetails":[{"name":"jakatnaka, surat","id":"12987651","routeType":"Pickup","uniqueId":"1"},{"name":"jakatnaka, surat","id":"8765","routeType":"Dropoff","uniqueId":"1"}],"emergencyDetails":{"firstName":"maheshbhai","lastName":"patel","address":"surat","files":[{"fileName":"asset.PNG","filePath":"/schoolapp/images/42fe532c-f4f4-41fb-b544-99885a0e17bf.PNG"},{"fileName":"Apple vs Android.pdf","filePath":"/schoolapp/images/22afe096-ac53-49df-9dcf-d73a4d962bd7.pdf"}],"emailId":"maheshbhai@gmail.com","phoneNo":"98654232","relation":"uncle"},"activationDate":"2018-01-26 07:47:45 UTC"},{"lastName":"patel","country":"India","deactivationDate":null,"issuedDate":"24/10/2014","driverPin":"9111","gender":null,"city":null,"issuedBy":null,"emailId":"mahesh@gmail.com","isActive":true,"phoneNo":"984562123","expiryDate":"14/12/2024","presentAddress":"katargam surat","schoolId":"129","state":null,"department":null,"profilePic":"/schoolapp/images/d69ba7b8-e9bf-47fd-a6a6-5ed72af81acb.png","geopoint":null,"certificateNo":"5689745","firstName":"mahesh","parmanentAddress":"varacha surat","driverId":"mahesh@gmail.com","dob":"26/01/1994","files":[{"fileName":"asset.PNG","filePath":"/schoolapp/images/d9f7ba99-af26-4907-ae75-5dfcfd99947d.PNG"}],"routeDetails":[{"name":"Pool 11","id":"129200100636530799030966400","routeType":"Pickup","uniqueId":"636530799030966400"},{"name":"noida greater noida","id":"12911","routeType":"pickup","uniqueId":"1"}],"emergencyDetails":{"firstName":"mukeshbhai","lastName":"patel","address":"surat","files":[{"fileName":"asset.PNG","filePath":"/schoolapp/images/2c5027c7-1797-4785-81ab-92bb05afd071.PNG"}],"emailId":"mukesh@gmail.com","phoneNo":"87561235","relation":"uncle"},"activationDate":"2018-01-26 07:38:00 UTC"},{"lastName":"Mr","country":"IN","deactivationDate":null,"issuedDate":null,"driverPin":"6557","gender":null,"city":"New Delhi","postalCode":"110096","issuedBy":null,"emailId":"pankajcpmishra@gmail.com","isActive":"true","phoneNo":"989889898","expiryDate":null,"presentAddress":"katargam surat","schoolId":"129","state":"Delhi","department":null,"address":"New Ashok Nagar","profilePic":"/schoolapp/images/688f9945-7736-4d25-820a-6d346177b668.png","geopoint":"21.2281,72.8338","certificateNo":null,"firstName":"Pankaj","parmanentAddress":null,"driverId":"pankajcpmishra@gmail.com","routeDetails":[{"name":"SP","id":"11","uniqueId":"694094"},{"name":"katargam","id":"12912341","routeType":"Pickup","uniqueId":"1"},{"name":"Udhana","id":"129135681","routeType":"Dropoff","uniqueId":"1"},{"name":"kanpur lucknow","id":"12919","routeType":"pickup","uniqueId":"1"}],"emergencyDetails":{},"position":null,"activationDate":"2017-07-19 11:35:37 UTC"},{"lastName":"Kumar","country":"IN","deactivationDate":"2017-12-31 23:59:59 UTC","issuedDate":null,"driverPin":"1234","gender":null,"city":"New Delhi","postalCode":"110096","issuedBy":null,"emailId":"jitendra@techsoftlabs.com","isActive":"true","phoneNo":"989889898","expiryDate":null,"presentAddress":"varacha surat","schoolId":"129","state":"Delhi","department":null,"address":"New Ashok Nagar","profilePic":"/schoolapp/images/93b820c6-96aa-4c20-b934-195743b1c66b.png","geopoint":"21.2437,72.8873","certificateNo":null,"firstName":"Jitendra K","parmanentAddress":null,"driverId":"jitendra@techsoftlabs.com","routeDetails":[{"name":"SP10","id":"10","routeType":"PICKUP","uniqueId":"694093"},{"name":"SP12","id":"12","routeType":"DROPOFF","uniqueId":"69409321"},{"name":"DS-office-to-hanumantemple-pickup","id":"12902","routeType":"pickup","uniqueId":"1"},{"name":"DS-hanumantemple-to-office-dropoff","id":"12903","routeType":"dropoff","uniqueId":"1"},{"name":"SP10","id":"10694093SP10","routeType":"pickup","uniqueId":"694093"},{"name":"surat","id":"1298789636528887714325376","routeType":"Dropoff","uniqueId":"636528887714325376"},{"name":"noida ","id":"1291232123636531454242735488","routeType":"Pickup","uniqueId":"636531454242735488"},{"name":"30012034","id":"pool 5","routeType":"Pickup","uniqueId":"636530010617777024"},{"name":"Delhi New Route With Sales Team","id":"12914","routeType":"dropoff","uniqueId":"1"},{"name":"data rady delhi","id":"12920","routeType":"pickup","uniqueId":"1"}],"emergencyDetails":{},"position":null,"activationDate":"2017-08-14 08:36:14 UTC"}]
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private String authtokenexpires;
    private ArrayList<ResultBean> result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public ArrayList<ResultBean> getResult() {
        return result;
    }

    public void setResult(ArrayList<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean implements Parcelable {
        /**
         * lastName : Test1
         * country : yuuu
         * deactivationDate : null
         * issuedDate : 08/02/2018
         * driverPin : 5444
         * gender : null
         * city : null
         * issuedBy : null
         * emailId : drivertest@gmail.com
         * isActive : true
         * phoneNo : 95333
         * expiryDate : 08/02/2018
         * presentAddress : Surat
         * schoolId : 129
         * state : null
         * department : null
         * profilePic : /schoolapp/images/6206972f-bf6c-4fda-87da-50d72963c85c.png
         * geopoint : 21.1702,72.8311
         * certificateNo : 855678
         * firstName : Driver
         * parmanentAddress : Raipur
         * driverId : drivertest@gmail.com
         * dob : 08/02/2018
         * routeDetails : [{"name":"dabholi","id":"8790","routeType":"Dropoff","uniqueId":"1"}]
         * emergencyDetails : {"firstName":null,"lastName":null,"address":null,"emailId":null,"phoneNo":null,"relation":null}
         * activationDate : 2018-02-08 14:34:08 UTC
         * postalCode :
         * address : Surat
         * position : null
         * files : [{"fileName":"Apple vs Android.pdf","filePath":"/schoolapp/images/45e75072-cc59-428c-9feb-b584d10f3e6b.pdf"}]
         */

        private String lastName;
        private String country;
        private String deactivationDate;
        private String issuedDate;
        private String driverPin;
        private String gender;
        private String city;
        private String issuedBy;
        private String emailId;
        private boolean isActive;
        private String phoneNo;
        private String expiryDate;
        private String presentAddress;
        private String schoolId;
        private String state;
        private String department;
        private String profilePic;
        private String geopoint;
        private String certificateNo;
        private String firstName;
        private String parmanentAddress;
        private String driverId;
        private String dob;
        private EmergencyDetailsBean emergencyDetails;
        private String activationDate;
        private String postalCode;
        private String address;
        private String position;
        private List<RouteDetailsBean> routeDetails;
        private List<FilesBean> files;
        private NotesBean notes;
        private PhysicianDetailsBean physicianDetails;


        public boolean isActive() {
            return isActive;
        }

        public void setActive(boolean active) {
            isActive = active;
        }

        public NotesBean getNotes() {
            return notes;
        }

        public void setNotes(NotesBean notes) {
            this.notes = notes;
        }

        public PhysicianDetailsBean getPhysicianDetails() {
            return physicianDetails;
        }

        public void setPhysicianDetails(PhysicianDetailsBean physicianDetails) {
            this.physicianDetails = physicianDetails;
        }

        public static Creator<ResultBean> getCREATOR() {
            return CREATOR;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getDeactivationDate() {
            return deactivationDate;
        }

        public void setDeactivationDate(String deactivationDate) {
            this.deactivationDate = deactivationDate;
        }

        public String getIssuedDate() {
            return issuedDate;
        }

        public void setIssuedDate(String issuedDate) {
            this.issuedDate = issuedDate;
        }

        public String getDriverPin() {
            return driverPin;
        }

        public void setDriverPin(String driverPin) {
            this.driverPin = driverPin;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getIssuedBy() {
            return issuedBy;
        }

        public void setIssuedBy(String issuedBy) {
            this.issuedBy = issuedBy;
        }

        public String getEmailId() {
            return emailId;
        }

        public void setEmailId(String emailId) {
            this.emailId = emailId;
        }

        public boolean isIsActive() {
            return isActive;
        }

        public void setIsActive(boolean isActive) {
            this.isActive = isActive;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public String getExpiryDate() {
            return expiryDate;
        }

        public void setExpiryDate(String expiryDate) {
            this.expiryDate = expiryDate;
        }

        public String getPresentAddress() {
            return presentAddress;
        }

        public void setPresentAddress(String presentAddress) {
            this.presentAddress = presentAddress;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getDepartment() {
            return department;
        }

        public void setDepartment(String department) {
            this.department = department;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        public String getGeopoint() {
            return geopoint;
        }

        public void setGeopoint(String geopoint) {
            this.geopoint = geopoint;
        }

        public String getCertificateNo() {
            return certificateNo;
        }

        public void setCertificateNo(String certificateNo) {
            this.certificateNo = certificateNo;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getParmanentAddress() {
            return parmanentAddress;
        }

        public void setParmanentAddress(String parmanentAddress) {
            this.parmanentAddress = parmanentAddress;
        }

        public String getDriverId() {
            return driverId;
        }

        public void setDriverId(String driverId) {
            this.driverId = driverId;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public EmergencyDetailsBean getEmergencyDetails() {
            return emergencyDetails;
        }

        public void setEmergencyDetails(EmergencyDetailsBean emergencyDetails) {
            this.emergencyDetails = emergencyDetails;
        }

        public String getActivationDate() {
            return activationDate;
        }

        public void setActivationDate(String activationDate) {
            this.activationDate = activationDate;
        }

        public String getPostalCode() {
            return postalCode;
        }

        public void setPostalCode(String postalCode) {
            this.postalCode = postalCode;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }

        public List<RouteDetailsBean> getRouteDetails() {
            return routeDetails;
        }

        public void setRouteDetails(List<RouteDetailsBean> routeDetails) {
            this.routeDetails = routeDetails;
        }

        public List<FilesBean> getFiles() {
            return files;
        }

        public void setFiles(List<FilesBean> files) {
            this.files = files;
        }

        public static class EmergencyDetailsBean implements Parcelable {
            /**
             * firstName : null
             * lastName : null
             * address : null
             * emailId : null
             * phoneNo : null
             * relation : null
             */

            private String firstName;
            private String lastName;
            private String address;
            private String emailId;
            private String phoneNo;
            private String relation;
            private List<FilesBean> files;


            public String getFirstName() {
                return firstName;
            }

            public void setFirstName(String firstName) {
                this.firstName = firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public void setLastName(String lastName) {
                this.lastName = lastName;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getEmailId() {
                return emailId;
            }

            public void setEmailId(String emailId) {
                this.emailId = emailId;
            }

            public String getPhoneNo() {
                return phoneNo;
            }

            public void setPhoneNo(String phoneNo) {
                this.phoneNo = phoneNo;
            }

            public String getRelation() {
                return relation;
            }

            public void setRelation(String relation) {
                this.relation = relation;
            }

            public EmergencyDetailsBean() {
            }

            public List<FilesBean> getFiles() {
                return files;
            }

            public void setFiles(List<FilesBean> files) {
                this.files = files;
            }

            public static class FilesBean implements Parcelable {
                /**
                 * fileName : asset.PNG
                 * filePath : /schoolapp/images/fabb662f-9fef-4b5d-b7dc-24b943d3e812.PNG
                 */

                private String fileName;
                private String filePath;

                public String getFileName() {
                    return fileName;
                }

                public void setFileName(String fileName) {
                    this.fileName = fileName;
                }

                public String getFilePath() {
                    return filePath;
                }

                public void setFilePath(String filePath) {
                    this.filePath = filePath;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(this.fileName);
                    dest.writeString(this.filePath);
                }

                public FilesBean() {
                }

                protected FilesBean(Parcel in) {
                    this.fileName = in.readString();
                    this.filePath = in.readString();
                }

                public static final Creator<FilesBean> CREATOR = new Creator<FilesBean>() {
                    @Override
                    public FilesBean createFromParcel(Parcel source) {
                        return new FilesBean(source);
                    }

                    @Override
                    public FilesBean[] newArray(int size) {
                        return new FilesBean[size];
                    }
                };
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.firstName);
                dest.writeString(this.lastName);
                dest.writeString(this.address);
                dest.writeString(this.emailId);
                dest.writeString(this.phoneNo);
                dest.writeString(this.relation);
                dest.writeTypedList(this.files);
            }

            protected EmergencyDetailsBean(Parcel in) {
                this.firstName = in.readString();
                this.lastName = in.readString();
                this.address = in.readString();
                this.emailId = in.readString();
                this.phoneNo = in.readString();
                this.relation = in.readString();
                this.files = in.createTypedArrayList(FilesBean.CREATOR);
            }

            public static final Creator<EmergencyDetailsBean> CREATOR = new Creator<EmergencyDetailsBean>() {
                @Override
                public EmergencyDetailsBean createFromParcel(Parcel source) {
                    return new EmergencyDetailsBean(source);
                }

                @Override
                public EmergencyDetailsBean[] newArray(int size) {
                    return new EmergencyDetailsBean[size];
                }
            };
        }

        public static class RouteDetailsBean implements Parcelable {
            /**
             * name : dabholi
             * id : 8790
             * routeType : Dropoff
             * uniqueId : 1
             */

            private String name;
            private String id;
            private String routeType;
            private String uniqueId;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getRouteType() {
                return routeType;
            }

            public void setRouteType(String routeType) {
                this.routeType = routeType;
            }

            public String getUniqueId() {
                return uniqueId;
            }

            public void setUniqueId(String uniqueId) {
                this.uniqueId = uniqueId;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.name);
                dest.writeString(this.id);
                dest.writeString(this.routeType);
                dest.writeString(this.uniqueId);
            }

            public RouteDetailsBean() {
            }

            protected RouteDetailsBean(Parcel in) {
                this.name = in.readString();
                this.id = in.readString();
                this.routeType = in.readString();
                this.uniqueId = in.readString();
            }

            public static final Creator<RouteDetailsBean> CREATOR = new Creator<RouteDetailsBean>() {
                @Override
                public RouteDetailsBean createFromParcel(Parcel source) {
                    return new RouteDetailsBean(source);
                }

                @Override
                public RouteDetailsBean[] newArray(int size) {
                    return new RouteDetailsBean[size];
                }
            };
        }


        public static class NotesBean implements Parcelable {
            /**
             * schoolCounselor : string
             * transportationCounselor : string
             */

            private String schoolCounselor;
            private String transportationCounselor;

            public String getSchoolCounselor() {
                return schoolCounselor;
            }

            public void setSchoolCounselor(String schoolCounselor) {
                this.schoolCounselor = schoolCounselor;
            }

            public String getTransportationCounselor() {
                return transportationCounselor;
            }

            public void setTransportationCounselor(String transportationCounselor) {
                this.transportationCounselor = transportationCounselor;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.schoolCounselor);
                dest.writeString(this.transportationCounselor);
            }

            public NotesBean() {
            }

            protected NotesBean(Parcel in) {
                this.schoolCounselor = in.readString();
                this.transportationCounselor = in.readString();
            }

            public static final Creator<NotesBean> CREATOR = new Creator<NotesBean>() {
                @Override
                public NotesBean createFromParcel(Parcel source) {
                    return new NotesBean(source);
                }

                @Override
                public NotesBean[] newArray(int size) {
                    return new NotesBean[size];
                }
            };
        }

        public static class PhysicianDetailsBean implements Parcelable {
            /**
             * address : string
             * city : string
             * country : string
             * email : string
             * fax : string
             * firstName : string
             * lastName : string
             * phone : string
             * state : string
             */

            private String address;
            private String city;
            private String country;
            private String email;
            private String fax;
            private String firstName;
            private String lastName;
            private String phone;
            private String state;

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getFax() {
                return fax;
            }

            public void setFax(String fax) {
                this.fax = fax;
            }

            public String getFirstName() {
                return firstName;
            }

            public void setFirstName(String firstName) {
                this.firstName = firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public void setLastName(String lastName) {
                this.lastName = lastName;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.address);
                dest.writeString(this.city);
                dest.writeString(this.country);
                dest.writeString(this.email);
                dest.writeString(this.fax);
                dest.writeString(this.firstName);
                dest.writeString(this.lastName);
                dest.writeString(this.phone);
                dest.writeString(this.state);
            }

            public PhysicianDetailsBean() {
            }

            protected PhysicianDetailsBean(Parcel in) {
                this.address = in.readString();
                this.city = in.readString();
                this.country = in.readString();
                this.email = in.readString();
                this.fax = in.readString();
                this.firstName = in.readString();
                this.lastName = in.readString();
                this.phone = in.readString();
                this.state = in.readString();
            }

            public static final Creator<PhysicianDetailsBean> CREATOR = new Creator<PhysicianDetailsBean>() {
                @Override
                public PhysicianDetailsBean createFromParcel(Parcel source) {
                    return new PhysicianDetailsBean(source);
                }

                @Override
                public PhysicianDetailsBean[] newArray(int size) {
                    return new PhysicianDetailsBean[size];
                }
            };
        }

        public static class FilesBean implements Parcelable {
            /**
             * fileName : Apple vs Android.pdf
             * filePath : /schoolapp/images/45e75072-cc59-428c-9feb-b584d10f3e6b.pdf
             */

            private String fileName;
            private String filePath;

            public String getFileName() {
                return fileName;
            }

            public void setFileName(String fileName) {
                this.fileName = fileName;
            }

            public String getFilePath() {
                return filePath;
            }

            public void setFilePath(String filePath) {
                this.filePath = filePath;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.fileName);
                dest.writeString(this.filePath);
            }

            public FilesBean() {
            }

            protected FilesBean(Parcel in) {
                this.fileName = in.readString();
                this.filePath = in.readString();
            }

            public static final Creator<FilesBean> CREATOR = new Creator<FilesBean>() {
                @Override
                public FilesBean createFromParcel(Parcel source) {
                    return new FilesBean(source);
                }

                @Override
                public FilesBean[] newArray(int size) {
                    return new FilesBean[size];
                }
            };
        }

        public ResultBean() {
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.lastName);
            dest.writeString(this.country);
            dest.writeString(this.deactivationDate);
            dest.writeString(this.issuedDate);
            dest.writeString(this.driverPin);
            dest.writeString(this.gender);
            dest.writeString(this.city);
            dest.writeString(this.issuedBy);
            dest.writeString(this.emailId);
            dest.writeByte(this.isActive ? (byte) 1 : (byte) 0);
            dest.writeString(this.phoneNo);
            dest.writeString(this.expiryDate);
            dest.writeString(this.presentAddress);
            dest.writeString(this.schoolId);
            dest.writeString(this.state);
            dest.writeString(this.department);
            dest.writeString(this.profilePic);
            dest.writeString(this.geopoint);
            dest.writeString(this.certificateNo);
            dest.writeString(this.firstName);
            dest.writeString(this.parmanentAddress);
            dest.writeString(this.driverId);
            dest.writeString(this.dob);
            dest.writeParcelable(this.emergencyDetails, flags);
            dest.writeString(this.activationDate);
            dest.writeString(this.postalCode);
            dest.writeString(this.address);
            dest.writeString(this.position);
            dest.writeTypedList(this.routeDetails);
            dest.writeTypedList(this.files);
            dest.writeParcelable(this.notes, flags);
            dest.writeParcelable(this.physicianDetails, flags);
        }

        protected ResultBean(Parcel in) {
            this.lastName = in.readString();
            this.country = in.readString();
            this.deactivationDate = in.readString();
            this.issuedDate = in.readString();
            this.driverPin = in.readString();
            this.gender = in.readString();
            this.city = in.readString();
            this.issuedBy = in.readString();
            this.emailId = in.readString();
            this.isActive = in.readByte() != 0;
            this.phoneNo = in.readString();
            this.expiryDate = in.readString();
            this.presentAddress = in.readString();
            this.schoolId = in.readString();
            this.state = in.readString();
            this.department = in.readString();
            this.profilePic = in.readString();
            this.geopoint = in.readString();
            this.certificateNo = in.readString();
            this.firstName = in.readString();
            this.parmanentAddress = in.readString();
            this.driverId = in.readString();
            this.dob = in.readString();
            this.emergencyDetails = in.readParcelable(EmergencyDetailsBean.class.getClassLoader());
            this.activationDate = in.readString();
            this.postalCode = in.readString();
            this.address = in.readString();
            this.position = in.readString();
            this.routeDetails = in.createTypedArrayList(RouteDetailsBean.CREATOR);
            this.files = in.createTypedArrayList(FilesBean.CREATOR);
            this.notes = in.readParcelable(NotesBean.class.getClassLoader());
            this.physicianDetails = in.readParcelable(PhysicianDetailsBean.class.getClassLoader());
        }

        public static final Creator<ResultBean> CREATOR = new Creator<ResultBean>() {
            @Override
            public ResultBean createFromParcel(Parcel source) {
                return new ResultBean(source);
            }

            @Override
            public ResultBean[] newArray(int size) {
                return new ResultBean[size];
            }
        };
    }
}
