/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class AddStudentResponse implements Parcelable {


    /**
     * message : Student information saved successfully
     * status : true
     * authtoken : null
     * result : {"id":"1291299","studentId":"1299","schoolId":"129","firstName":"Jayesh","lastName":"Bhavani","dob":"21/03/2018","birthPlace":"","nationality":"","motherTounge":"","religion":"","profilePic":"/schoolapp/images/a44da88e-ad64-4ede-bb72-c30fccb65b4a.jpg","gender":"male","dateofJoining":"","academicInfo":false,"address":"Adajan Surat","geopoint":null,"parentDetails":{"fatherFirstName":"","fatherLastName":"","fatherOccupation":"","fatherPhoneNo":"","fatherEmailId":"","fatherProfilePic":null,"motherFirstName":"","motherLastName":"","motherOccupation":"","motherPhoneNo":"","motherEmailId":"","motherProfilePic":null,"files":null},"guardianDetails":{"guardianFirstName":"","guardianLastName":"","relation":"","dob":"","education":"","occupation":"","phoneNo":"","emailId":"","address":"","files":null},"pastSchoolDetails":{"schoolName":"","yearOfPassing":"","gradeId":"","classId":"","address":"","files":null},"files":null}
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private ResultBean result;
    private String authtokenexpires;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public static class ResultBean implements Parcelable {
        /**
         * id : 1291299
         * studentId : 1299
         * schoolId : 129
         * firstName : Jayesh
         * lastName : Bhavani
         * dob : 21/03/2018
         * birthPlace : 
         * nationality : 
         * motherTounge : 
         * religion : 
         * profilePic : /schoolapp/images/a44da88e-ad64-4ede-bb72-c30fccb65b4a.jpg
         * gender : male
         * dateofJoining : 
         * academicInfo : false
         * address : Adajan Surat
         * geopoint : null
         * parentDetails : {"fatherFirstName":"","fatherLastName":"","fatherOccupation":"","fatherPhoneNo":"","fatherEmailId":"","fatherProfilePic":null,"motherFirstName":"","motherLastName":"","motherOccupation":"","motherPhoneNo":"","motherEmailId":"","motherProfilePic":null,"files":null}
         * guardianDetails : {"guardianFirstName":"","guardianLastName":"","relation":"","dob":"","education":"","occupation":"","phoneNo":"","emailId":"","address":"","files":null}
         * pastSchoolDetails : {"schoolName":"","yearOfPassing":"","gradeId":"","classId":"","address":"","files":null}
         * files : null
         */

        private String id;
        private String studentId;
        private String schoolId;
        private String firstName;
        private String lastName;
        private String dob;
        private String birthPlace;
        private String nationality;
        private String motherTounge;
        private String religion;
        private String profilePic;
        private String gender;
        private String dateofJoining;
        private boolean academicInfo;
        private String address;
        private String geopoint;
        private ParentDetailsBean parentDetails;
        private GuardianDetailsBean guardianDetails;
        private PastSchoolDetailsBean pastSchoolDetails;
        private List<FilesBeanXXX> files;

        public List<FilesBeanXXX> getFiles() {
            return files;
        }

        public void setFiles(List<FilesBeanXXX> files) {
            this.files = files;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getStudentId() {
            return studentId;
        }

        public void setStudentId(String studentId) {
            this.studentId = studentId;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getBirthPlace() {
            return birthPlace;
        }

        public void setBirthPlace(String birthPlace) {
            this.birthPlace = birthPlace;
        }

        public String getNationality() {
            return nationality;
        }

        public void setNationality(String nationality) {
            this.nationality = nationality;
        }

        public String getMotherTounge() {
            return motherTounge;
        }

        public void setMotherTounge(String motherTounge) {
            this.motherTounge = motherTounge;
        }

        public String getReligion() {
            return religion;
        }

        public void setReligion(String religion) {
            this.religion = religion;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getDateofJoining() {
            return dateofJoining;
        }

        public void setDateofJoining(String dateofJoining) {
            this.dateofJoining = dateofJoining;
        }

        public boolean isAcademicInfo() {
            return academicInfo;
        }

        public void setAcademicInfo(boolean academicInfo) {
            this.academicInfo = academicInfo;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getGeopoint() {
            return geopoint;
        }

        public void setGeopoint(String geopoint) {
            this.geopoint = geopoint;
        }

        public ParentDetailsBean getParentDetails() {
            return parentDetails;
        }

        public void setParentDetails(ParentDetailsBean parentDetails) {
            this.parentDetails = parentDetails;
        }

        public GuardianDetailsBean getGuardianDetails() {
            return guardianDetails;
        }

        public void setGuardianDetails(GuardianDetailsBean guardianDetails) {
            this.guardianDetails = guardianDetails;
        }

        public PastSchoolDetailsBean getPastSchoolDetails() {
            return pastSchoolDetails;
        }

        public void setPastSchoolDetails(PastSchoolDetailsBean pastSchoolDetails) {
            this.pastSchoolDetails = pastSchoolDetails;
        }


        public static class ParentDetailsBean implements Parcelable {
            /**
             * fatherFirstName : 
             * fatherLastName : 
             * fatherOccupation : 
             * fatherPhoneNo : 
             * fatherEmailId : 
             * fatherProfilePic : null
             * motherFirstName : 
             * motherLastName : 
             * motherOccupation : 
             * motherPhoneNo : 
             * motherEmailId : 
             * motherProfilePic : null
             * files : null
             */

            private String fatherFirstName;
            private String fatherLastName;
            private String fatherOccupation;
            private String fatherPhoneNo;
            private String fatherEmailId;
            private String fatherProfilePic;
            private String motherFirstName;
            private String motherLastName;
            private String motherOccupation;
            private String motherPhoneNo;
            private String motherEmailId;
            private String motherProfilePic;
            private List<FilesBeanX> files;

            public String getFatherFirstName() {
                return fatherFirstName;
            }

            public void setFatherFirstName(String fatherFirstName) {
                this.fatherFirstName = fatherFirstName;
            }

            public String getFatherLastName() {
                return fatherLastName;
            }

            public void setFatherLastName(String fatherLastName) {
                this.fatherLastName = fatherLastName;
            }

            public String getFatherOccupation() {
                return fatherOccupation;
            }

            public void setFatherOccupation(String fatherOccupation) {
                this.fatherOccupation = fatherOccupation;
            }

            public String getFatherPhoneNo() {
                return fatherPhoneNo;
            }

            public void setFatherPhoneNo(String fatherPhoneNo) {
                this.fatherPhoneNo = fatherPhoneNo;
            }

            public String getFatherEmailId() {
                return fatherEmailId;
            }

            public void setFatherEmailId(String fatherEmailId) {
                this.fatherEmailId = fatherEmailId;
            }

            public String getFatherProfilePic() {
                return fatherProfilePic;
            }

            public void setFatherProfilePic(String fatherProfilePic) {
                this.fatherProfilePic = fatherProfilePic;
            }

            public String getMotherFirstName() {
                return motherFirstName;
            }

            public void setMotherFirstName(String motherFirstName) {
                this.motherFirstName = motherFirstName;
            }

            public String getMotherLastName() {
                return motherLastName;
            }

            public void setMotherLastName(String motherLastName) {
                this.motherLastName = motherLastName;
            }

            public String getMotherOccupation() {
                return motherOccupation;
            }

            public void setMotherOccupation(String motherOccupation) {
                this.motherOccupation = motherOccupation;
            }

            public String getMotherPhoneNo() {
                return motherPhoneNo;
            }

            public void setMotherPhoneNo(String motherPhoneNo) {
                this.motherPhoneNo = motherPhoneNo;
            }

            public String getMotherEmailId() {
                return motherEmailId;
            }

            public void setMotherEmailId(String motherEmailId) {
                this.motherEmailId = motherEmailId;
            }

            public String getMotherProfilePic() {
                return motherProfilePic;
            }

            public void setMotherProfilePic(String motherProfilePic) {
                this.motherProfilePic = motherProfilePic;
            }

            public List<FilesBeanX> getFiles() {
                return files;
            }

            public void setFiles(List<FilesBeanX> files) {
                this.files = files;
            }

            public static class FilesBeanX implements Parcelable {
                /**
                 * fileName : string
                 * filePath : string
                 */

                private String fileName;
                private String filePath;

                public String getFileName() {
                    return fileName;
                }

                public void setFileName(String fileName) {
                    this.fileName = fileName;
                }

                public String getFilePath() {
                    return filePath;
                }

                public void setFilePath(String filePath) {
                    this.filePath = filePath;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(this.fileName);
                    dest.writeString(this.filePath);
                }

                public FilesBeanX() {
                }

                protected FilesBeanX(Parcel in) {
                    this.fileName = in.readString();
                    this.filePath = in.readString();
                }

                public static final Creator<FilesBeanX> CREATOR = new Creator<FilesBeanX>() {
                    @Override
                    public FilesBeanX createFromParcel(Parcel source) {
                        return new FilesBeanX(source);
                    }

                    @Override
                    public FilesBeanX[] newArray(int size) {
                        return new FilesBeanX[size];
                    }
                };
            }

            public ParentDetailsBean() {
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.fatherFirstName);
                dest.writeString(this.fatherLastName);
                dest.writeString(this.fatherOccupation);
                dest.writeString(this.fatherPhoneNo);
                dest.writeString(this.fatherEmailId);
                dest.writeString(this.fatherProfilePic);
                dest.writeString(this.motherFirstName);
                dest.writeString(this.motherLastName);
                dest.writeString(this.motherOccupation);
                dest.writeString(this.motherPhoneNo);
                dest.writeString(this.motherEmailId);
                dest.writeString(this.motherProfilePic);
                dest.writeTypedList(this.files);
            }

            protected ParentDetailsBean(Parcel in) {
                this.fatherFirstName = in.readString();
                this.fatherLastName = in.readString();
                this.fatherOccupation = in.readString();
                this.fatherPhoneNo = in.readString();
                this.fatherEmailId = in.readString();
                this.fatherProfilePic = in.readString();
                this.motherFirstName = in.readString();
                this.motherLastName = in.readString();
                this.motherOccupation = in.readString();
                this.motherPhoneNo = in.readString();
                this.motherEmailId = in.readString();
                this.motherProfilePic = in.readString();
                this.files = in.createTypedArrayList(FilesBeanX.CREATOR);
            }

            public static final Creator<ParentDetailsBean> CREATOR = new Creator<ParentDetailsBean>() {
                @Override
                public ParentDetailsBean createFromParcel(Parcel source) {
                    return new ParentDetailsBean(source);
                }

                @Override
                public ParentDetailsBean[] newArray(int size) {
                    return new ParentDetailsBean[size];
                }
            };
        }

        public static class GuardianDetailsBean implements Parcelable {
            /**
             * guardianFirstName : 
             * guardianLastName : 
             * relation : 
             * dob : 
             * education : 
             * occupation : 
             * phoneNo : 
             * emailId : 
             * address : 
             * files : null
             */

            private String guardianFirstName;
            private String guardianLastName;
            private String relation;
            private String dob;
            private String education;
            private String occupation;
            private String phoneNo;
            private String emailId;
            private String address;
            private List<FilesBean> files;

            public String getGuardianFirstName() {
                return guardianFirstName;
            }

            public void setGuardianFirstName(String guardianFirstName) {
                this.guardianFirstName = guardianFirstName;
            }

            public String getGuardianLastName() {
                return guardianLastName;
            }

            public void setGuardianLastName(String guardianLastName) {
                this.guardianLastName = guardianLastName;
            }

            public String getRelation() {
                return relation;
            }

            public void setRelation(String relation) {
                this.relation = relation;
            }

            public String getDob() {
                return dob;
            }

            public void setDob(String dob) {
                this.dob = dob;
            }

            public String getEducation() {
                return education;
            }

            public void setEducation(String education) {
                this.education = education;
            }

            public String getOccupation() {
                return occupation;
            }

            public void setOccupation(String occupation) {
                this.occupation = occupation;
            }

            public String getPhoneNo() {
                return phoneNo;
            }

            public void setPhoneNo(String phoneNo) {
                this.phoneNo = phoneNo;
            }

            public String getEmailId() {
                return emailId;
            }

            public void setEmailId(String emailId) {
                this.emailId = emailId;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public List<FilesBean> getFiles() {
                return files;
            }

            public void setFiles(List<FilesBean> files) {
                this.files = files;
            }

            public static class FilesBean implements Parcelable {
                /**
                 * fileName : string
                 * filePath : string
                 */

                private String fileName;
                private String filePath;

                public String getFileName() {
                    return fileName;
                }

                public void setFileName(String fileName) {
                    this.fileName = fileName;
                }

                public String getFilePath() {
                    return filePath;
                }

                public void setFilePath(String filePath) {
                    this.filePath = filePath;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(this.fileName);
                    dest.writeString(this.filePath);
                }

                public FilesBean() {
                }

                protected FilesBean(Parcel in) {
                    this.fileName = in.readString();
                    this.filePath = in.readString();
                }

                public static final Creator<FilesBean> CREATOR = new Creator<FilesBean>() {
                    @Override
                    public FilesBean createFromParcel(Parcel source) {
                        return new FilesBean(source);
                    }

                    @Override
                    public FilesBean[] newArray(int size) {
                        return new FilesBean[size];
                    }
                };
            }

            public GuardianDetailsBean() {
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.guardianFirstName);
                dest.writeString(this.guardianLastName);
                dest.writeString(this.relation);
                dest.writeString(this.dob);
                dest.writeString(this.education);
                dest.writeString(this.occupation);
                dest.writeString(this.phoneNo);
                dest.writeString(this.emailId);
                dest.writeString(this.address);
                dest.writeList(this.files);
            }

            protected GuardianDetailsBean(Parcel in) {
                this.guardianFirstName = in.readString();
                this.guardianLastName = in.readString();
                this.relation = in.readString();
                this.dob = in.readString();
                this.education = in.readString();
                this.occupation = in.readString();
                this.phoneNo = in.readString();
                this.emailId = in.readString();
                this.address = in.readString();
                this.files = new ArrayList<FilesBean>();
                in.readList(this.files, FilesBean.class.getClassLoader());
            }

            public static final Creator<GuardianDetailsBean> CREATOR = new Creator<GuardianDetailsBean>() {
                @Override
                public GuardianDetailsBean createFromParcel(Parcel source) {
                    return new GuardianDetailsBean(source);
                }

                @Override
                public GuardianDetailsBean[] newArray(int size) {
                    return new GuardianDetailsBean[size];
                }
            };
        }

        public static class PastSchoolDetailsBean implements Parcelable {
            /**
             * schoolName : 
             * yearOfPassing : 
             * gradeId : 
             * classId : 
             * address : 
             * files : null
             */

            private String schoolName;
            private String yearOfPassing;
            private String gradeId;
            private String classId;
            private String address;
            private List<FilesBeanXX> files;

            public String getSchoolName() {
                return schoolName;
            }

            public void setSchoolName(String schoolName) {
                this.schoolName = schoolName;
            }

            public String getYearOfPassing() {
                return yearOfPassing;
            }

            public void setYearOfPassing(String yearOfPassing) {
                this.yearOfPassing = yearOfPassing;
            }

            public String getGradeId() {
                return gradeId;
            }

            public void setGradeId(String gradeId) {
                this.gradeId = gradeId;
            }

            public String getClassId() {
                return classId;
            }

            public void setClassId(String classId) {
                this.classId = classId;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public List<FilesBeanXX> getFiles() {
                return files;
            }

            public void setFiles(List<FilesBeanXX> files) {
                this.files = files;
            }

            public static class FilesBeanXX implements Parcelable {
                /**
                 * fileName : string
                 * filePath : string
                 */

                private String fileName;
                private String filePath;

                public String getFileName() {
                    return fileName;
                }

                public void setFileName(String fileName) {
                    this.fileName = fileName;
                }

                public String getFilePath() {
                    return filePath;
                }

                public void setFilePath(String filePath) {
                    this.filePath = filePath;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(this.fileName);
                    dest.writeString(this.filePath);
                }

                public FilesBeanXX() {
                }

                protected FilesBeanXX(Parcel in) {
                    this.fileName = in.readString();
                    this.filePath = in.readString();
                }

                public static final Creator<FilesBeanXX> CREATOR = new Creator<FilesBeanXX>() {
                    @Override
                    public FilesBeanXX createFromParcel(Parcel source) {
                        return new FilesBeanXX(source);
                    }

                    @Override
                    public FilesBeanXX[] newArray(int size) {
                        return new FilesBeanXX[size];
                    }
                };
            }

            public PastSchoolDetailsBean() {
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.schoolName);
                dest.writeString(this.yearOfPassing);
                dest.writeString(this.gradeId);
                dest.writeString(this.classId);
                dest.writeString(this.address);
                dest.writeList(this.files);
            }

            protected PastSchoolDetailsBean(Parcel in) {
                this.schoolName = in.readString();
                this.yearOfPassing = in.readString();
                this.gradeId = in.readString();
                this.classId = in.readString();
                this.address = in.readString();
                this.files = new ArrayList<FilesBeanXX>();
                in.readList(this.files, FilesBeanXX.class.getClassLoader());
            }

            public static final Creator<PastSchoolDetailsBean> CREATOR = new Creator<PastSchoolDetailsBean>() {
                @Override
                public PastSchoolDetailsBean createFromParcel(Parcel source) {
                    return new PastSchoolDetailsBean(source);
                }

                @Override
                public PastSchoolDetailsBean[] newArray(int size) {
                    return new PastSchoolDetailsBean[size];
                }
            };
        }

        public ResultBean() {
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.id);
            dest.writeString(this.studentId);
            dest.writeString(this.schoolId);
            dest.writeString(this.firstName);
            dest.writeString(this.lastName);
            dest.writeString(this.dob);
            dest.writeString(this.birthPlace);
            dest.writeString(this.nationality);
            dest.writeString(this.motherTounge);
            dest.writeString(this.religion);
            dest.writeString(this.profilePic);
            dest.writeString(this.gender);
            dest.writeString(this.dateofJoining);
            dest.writeByte(this.academicInfo ? (byte) 1 : (byte) 0);
            dest.writeString(this.address);
            dest.writeString(this.geopoint);
            dest.writeParcelable(this.parentDetails, flags);
            dest.writeParcelable(this.guardianDetails, flags);
            dest.writeParcelable(this.pastSchoolDetails, flags);
            dest.writeTypedList(this.files);
        }

        protected ResultBean(Parcel in) {
            this.id = in.readString();
            this.studentId = in.readString();
            this.schoolId = in.readString();
            this.firstName = in.readString();
            this.lastName = in.readString();
            this.dob = in.readString();
            this.birthPlace = in.readString();
            this.nationality = in.readString();
            this.motherTounge = in.readString();
            this.religion = in.readString();
            this.profilePic = in.readString();
            this.gender = in.readString();
            this.dateofJoining = in.readString();
            this.academicInfo = in.readByte() != 0;
            this.address = in.readString();
            this.geopoint = in.readString();
            this.parentDetails = in.readParcelable(ParentDetailsBean.class.getClassLoader());
            this.guardianDetails = in.readParcelable(GuardianDetailsBean.class.getClassLoader());
            this.pastSchoolDetails = in.readParcelable(PastSchoolDetailsBean.class.getClassLoader());
            this.files = in.createTypedArrayList(FilesBeanXXX.CREATOR);
        }

        public static final Creator<ResultBean> CREATOR = new Creator<ResultBean>() {
            @Override
            public ResultBean createFromParcel(Parcel source) {
                return new ResultBean(source);
            }

            @Override
            public ResultBean[] newArray(int size) {
                return new ResultBean[size];
            }
        };
    }

    public AddStudentResponse() {
    }

    public static class FilesBeanXXX implements Parcelable {
        /**
         * fileName : string
         * filePath : string
         */

        private String fileName;
        private String filePath;

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public String getFilePath() {
            return filePath;
        }

        public void setFilePath(String filePath) {
            this.filePath = filePath;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.fileName);
            dest.writeString(this.filePath);
        }

        public FilesBeanXXX() {
        }

        protected FilesBeanXXX(Parcel in) {
            this.fileName = in.readString();
            this.filePath = in.readString();
        }

        public static final Creator<FilesBeanXXX> CREATOR = new Creator<FilesBeanXXX>() {
            @Override
            public FilesBeanXXX createFromParcel(Parcel source) {
                return new FilesBeanXXX(source);
            }

            @Override
            public FilesBeanXXX[] newArray(int size) {
                return new FilesBeanXXX[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.message);
        dest.writeByte(this.status ? (byte) 1 : (byte) 0);
        dest.writeString(this.authtoken);
        dest.writeParcelable(this.result, flags);
        dest.writeString(this.authtokenexpires);
    }

    protected AddStudentResponse(Parcel in) {
        this.message = in.readString();
        this.status = in.readByte() != 0;
        this.authtoken = in.readString();
        this.result = in.readParcelable(ResultBean.class.getClassLoader());
        this.authtokenexpires = in.readString();
    }

    public static final Creator<AddStudentResponse> CREATOR = new Creator<AddStudentResponse>() {
        @Override
        public AddStudentResponse createFromParcel(Parcel source) {
            return new AddStudentResponse(source);
        }

        @Override
        public AddStudentResponse[] newArray(int size) {
            return new AddStudentResponse[size];
        }
    };
}
