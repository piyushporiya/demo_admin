/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;

import java.util.List;

public class GetSchoolInfo {


    /**
     * message : Informations are below
     * status : true
     * authtoken : null
     * result : {"city":"New Delhi","stateId":"Delhi","profilePic":"/schoolapp/images/modernschool.jpg","geopoint":"28.628275,77.229675","schoolpermissions":{"parentmodules":{"route":true,"homework":true,"fee":true,"channel":true,"appointment":true,"refreshFrequency":"10","communication":true,"version":"string","events":true,"attendance":true,"p2p":true},"teachermodules":{"homework":true,"t2t":true,"appointment":true,"communication":true,"version":"premium","attendance":true},"adminmodules":{"route":true,"driver":true,"student":true,"fee":true,"channel":true,"appointment":true,"staff":true,"communication":true,"version":"premium","events":false},"apps":{"parent":true,"teacher":true,"driver":true,"admin":true},"drivermodules":{"pickupdropoff":true,"qrscan":true,"fileupload":true,"refreshFrequency":"10","video":true,"version":"premium"}},"emailID":"info@modernschool.com","cityId":"987","schoolTiming":{"startTime":"07:00 AM","endTime":"02:00 PM","periodFrequency":"60"},"countryId":"IN","phoneNo":"011 2688 3336","termDetails":[{"termId":"01","termDesc":"July Month","termName":"July"},{"termId":"02","termDesc":"August Month","termName":"August"},{"termId":"03","termDesc":"September Month","termName":"September"},{"termId":"04","termDesc":"October Month","termName":"October"},{"termId":"05","termDesc":"November Month","termName":"November"},{"termId":"06","termDesc":"December Month","termName":"December"},{"termId":"07","termDesc":"January Month","termName":"January"},{"termId":"08","termDesc":"Febrary Month","termName":"Febrary"},{"termId":"09","termDesc":"March Month","termName":"March"},{"termId":"10","termDesc":"April Month","termName":"April"},{"termId":"11","termDesc":"May Month","termName":"May"},{"termId":"12","termDesc":"June Month","termName":"June"}],"pin":"110001","regionId":"","addrLine3":"","addrLine2":"New Delhi","busStandInfo":{"address":"Barakhamba Road, New Delhi, Delhi","geopoint":"28.628275,77.229675"},"schoolId":"129","addrLine1":"Barakhamba Road","state":"Delhi","schoolName":"Pankaj Modern School"}
     * authtokenexpires : null
     */

    private String message;
    private boolean status;
    private String authtoken;
    private ResultBean result;
    private String authtokenexpires;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public String getAuthtokenexpires() {
        return authtokenexpires;
    }

    public void setAuthtokenexpires(String authtokenexpires) {
        this.authtokenexpires = authtokenexpires;
    }

    public static class ResultBean {
        /**
         * city : New Delhi
         * stateId : Delhi
         * profilePic : /schoolapp/images/modernschool.jpg
         * geopoint : 28.628275,77.229675
         * schoolpermissions : {"parentmodules":{"route":true,"homework":true,"fee":true,"channel":true,"appointment":true,"refreshFrequency":"10","communication":true,"version":"string","events":true,"attendance":true,"p2p":true},"teachermodules":{"homework":true,"t2t":true,"appointment":true,"communication":true,"version":"premium","attendance":true},"adminmodules":{"route":true,"driver":true,"student":true,"fee":true,"channel":true,"appointment":true,"staff":true,"communication":true,"version":"premium","events":false},"apps":{"parent":true,"teacher":true,"driver":true,"admin":true},"drivermodules":{"pickupdropoff":true,"qrscan":true,"fileupload":true,"refreshFrequency":"10","video":true,"version":"premium"}}
         * emailID : info@modernschool.com
         * cityId : 987
         * schoolTiming : {"startTime":"07:00 AM","endTime":"02:00 PM","periodFrequency":"60"}
         * countryId : IN
         * phoneNo : 011 2688 3336
         * termDetails : [{"termId":"01","termDesc":"July Month","termName":"July"},{"termId":"02","termDesc":"August Month","termName":"August"},{"termId":"03","termDesc":"September Month","termName":"September"},{"termId":"04","termDesc":"October Month","termName":"October"},{"termId":"05","termDesc":"November Month","termName":"November"},{"termId":"06","termDesc":"December Month","termName":"December"},{"termId":"07","termDesc":"January Month","termName":"January"},{"termId":"08","termDesc":"Febrary Month","termName":"Febrary"},{"termId":"09","termDesc":"March Month","termName":"March"},{"termId":"10","termDesc":"April Month","termName":"April"},{"termId":"11","termDesc":"May Month","termName":"May"},{"termId":"12","termDesc":"June Month","termName":"June"}]
         * pin : 110001
         * regionId : 
         * addrLine3 : 
         * addrLine2 : New Delhi
         * busStandInfo : {"address":"Barakhamba Road, New Delhi, Delhi","geopoint":"28.628275,77.229675"}
         * schoolId : 129
         * addrLine1 : Barakhamba Road
         * state : Delhi
         * schoolName : Pankaj Modern School
         */

        private String city;
        private String stateId;
        private String profilePic;
        private String geopoint;
        private SchoolpermissionsBean schoolpermissions;
        private String emailID;
        private String cityId;
        private SchoolTimingBean schoolTiming;
        private String countryId;
        private String phoneNo;
        private String pin;
        private String regionId;
        private String addrLine3;
        private String addrLine2;
        private BusStandInfoBean busStandInfo;
        private String schoolId;
        private String addrLine1;
        private String state;
        private String schoolName;
        private List<TermDetailsBean> termDetails;

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getStateId() {
            return stateId;
        }

        public void setStateId(String stateId) {
            this.stateId = stateId;
        }

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        public String getGeopoint() {
            return geopoint;
        }

        public void setGeopoint(String geopoint) {
            this.geopoint = geopoint;
        }

        public SchoolpermissionsBean getSchoolpermissions() {
            return schoolpermissions;
        }

        public void setSchoolpermissions(SchoolpermissionsBean schoolpermissions) {
            this.schoolpermissions = schoolpermissions;
        }

        public String getEmailID() {
            return emailID;
        }

        public void setEmailID(String emailID) {
            this.emailID = emailID;
        }

        public String getCityId() {
            return cityId;
        }

        public void setCityId(String cityId) {
            this.cityId = cityId;
        }

        public SchoolTimingBean getSchoolTiming() {
            return schoolTiming;
        }

        public void setSchoolTiming(SchoolTimingBean schoolTiming) {
            this.schoolTiming = schoolTiming;
        }

        public String getCountryId() {
            return countryId;
        }

        public void setCountryId(String countryId) {
            this.countryId = countryId;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public String getPin() {
            return pin;
        }

        public void setPin(String pin) {
            this.pin = pin;
        }

        public String getRegionId() {
            return regionId;
        }

        public void setRegionId(String regionId) {
            this.regionId = regionId;
        }

        public String getAddrLine3() {
            return addrLine3;
        }

        public void setAddrLine3(String addrLine3) {
            this.addrLine3 = addrLine3;
        }

        public String getAddrLine2() {
            return addrLine2;
        }

        public void setAddrLine2(String addrLine2) {
            this.addrLine2 = addrLine2;
        }

        public BusStandInfoBean getBusStandInfo() {
            return busStandInfo;
        }

        public void setBusStandInfo(BusStandInfoBean busStandInfo) {
            this.busStandInfo = busStandInfo;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getAddrLine1() {
            return addrLine1;
        }

        public void setAddrLine1(String addrLine1) {
            this.addrLine1 = addrLine1;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getSchoolName() {
            return schoolName;
        }

        public void setSchoolName(String schoolName) {
            this.schoolName = schoolName;
        }

        public List<TermDetailsBean> getTermDetails() {
            return termDetails;
        }

        public void setTermDetails(List<TermDetailsBean> termDetails) {
            this.termDetails = termDetails;
        }

        public static class SchoolpermissionsBean {
            /**
             * parentmodules : {"route":true,"homework":true,"fee":true,"channel":true,"appointment":true,"refreshFrequency":"10","communication":true,"version":"string","events":true,"attendance":true,"p2p":true}
             * teachermodules : {"homework":true,"t2t":true,"appointment":true,"communication":true,"version":"premium","attendance":true}
             * adminmodules : {"route":true,"driver":true,"student":true,"fee":true,"channel":true,"appointment":true,"staff":true,"communication":true,"version":"premium","events":false}
             * apps : {"parent":true,"teacher":true,"driver":true,"admin":true}
             * drivermodules : {"pickupdropoff":true,"qrscan":true,"fileupload":true,"refreshFrequency":"10","video":true,"version":"premium"}
             */

            private ParentmodulesBean parentmodules;
            private TeachermodulesBean teachermodules;
            private AdminmodulesBean adminmodules;
            private AppsBean apps;
            private DrivermodulesBean drivermodules;

            public ParentmodulesBean getParentmodules() {
                return parentmodules;
            }

            public void setParentmodules(ParentmodulesBean parentmodules) {
                this.parentmodules = parentmodules;
            }

            public TeachermodulesBean getTeachermodules() {
                return teachermodules;
            }

            public void setTeachermodules(TeachermodulesBean teachermodules) {
                this.teachermodules = teachermodules;
            }

            public AdminmodulesBean getAdminmodules() {
                return adminmodules;
            }

            public void setAdminmodules(AdminmodulesBean adminmodules) {
                this.adminmodules = adminmodules;
            }

            public AppsBean getApps() {
                return apps;
            }

            public void setApps(AppsBean apps) {
                this.apps = apps;
            }

            public DrivermodulesBean getDrivermodules() {
                return drivermodules;
            }

            public void setDrivermodules(DrivermodulesBean drivermodules) {
                this.drivermodules = drivermodules;
            }

            public static class ParentmodulesBean {
                /**
                 * route : true
                 * homework : true
                 * fee : true
                 * channel : true
                 * appointment : true
                 * refreshFrequency : 10
                 * communication : true
                 * version : string
                 * events : true
                 * attendance : true
                 * p2p : true
                 */

                private boolean route;
                private boolean homework;
                private boolean fee;
                private boolean channel;
                private boolean appointment;
                private String refreshFrequency;
                private boolean communication;
                private String version;
                private boolean events;
                private boolean attendance;
                private boolean p2p;

                public boolean isRoute() {
                    return route;
                }

                public void setRoute(boolean route) {
                    this.route = route;
                }

                public boolean isHomework() {
                    return homework;
                }

                public void setHomework(boolean homework) {
                    this.homework = homework;
                }

                public boolean isFee() {
                    return fee;
                }

                public void setFee(boolean fee) {
                    this.fee = fee;
                }

                public boolean isChannel() {
                    return channel;
                }

                public void setChannel(boolean channel) {
                    this.channel = channel;
                }

                public boolean isAppointment() {
                    return appointment;
                }

                public void setAppointment(boolean appointment) {
                    this.appointment = appointment;
                }

                public String getRefreshFrequency() {
                    return refreshFrequency;
                }

                public void setRefreshFrequency(String refreshFrequency) {
                    this.refreshFrequency = refreshFrequency;
                }

                public boolean isCommunication() {
                    return communication;
                }

                public void setCommunication(boolean communication) {
                    this.communication = communication;
                }

                public String getVersion() {
                    return version;
                }

                public void setVersion(String version) {
                    this.version = version;
                }

                public boolean isEvents() {
                    return events;
                }

                public void setEvents(boolean events) {
                    this.events = events;
                }

                public boolean isAttendance() {
                    return attendance;
                }

                public void setAttendance(boolean attendance) {
                    this.attendance = attendance;
                }

                public boolean isP2p() {
                    return p2p;
                }

                public void setP2p(boolean p2p) {
                    this.p2p = p2p;
                }
            }

            public static class TeachermodulesBean {
                /**
                 * homework : true
                 * t2t : true
                 * appointment : true
                 * communication : true
                 * version : premium
                 * attendance : true
                 */

                private boolean homework;
                private boolean t2t;
                private boolean appointment;
                private boolean communication;
                private String version;
                private boolean attendance;

                public boolean isHomework() {
                    return homework;
                }

                public void setHomework(boolean homework) {
                    this.homework = homework;
                }

                public boolean isT2t() {
                    return t2t;
                }

                public void setT2t(boolean t2t) {
                    this.t2t = t2t;
                }

                public boolean isAppointment() {
                    return appointment;
                }

                public void setAppointment(boolean appointment) {
                    this.appointment = appointment;
                }

                public boolean isCommunication() {
                    return communication;
                }

                public void setCommunication(boolean communication) {
                    this.communication = communication;
                }

                public String getVersion() {
                    return version;
                }

                public void setVersion(String version) {
                    this.version = version;
                }

                public boolean isAttendance() {
                    return attendance;
                }

                public void setAttendance(boolean attendance) {
                    this.attendance = attendance;
                }
            }

            public static class AdminmodulesBean {
                /**
                 * route : true
                 * driver : true
                 * student : true
                 * fee : true
                 * channel : true
                 * appointment : true
                 * staff : true
                 * communication : true
                 * version : premium
                 * events : false
                 */

                private boolean route;
                private boolean driver;
                private boolean student;
                private boolean fee;
                private boolean channel;
                private boolean appointment;
                private boolean staff;
                private boolean communication;
                private String version;
                private boolean events;

                public boolean isRoute() {
                    return route;
                }

                public void setRoute(boolean route) {
                    this.route = route;
                }

                public boolean isDriver() {
                    return driver;
                }

                public void setDriver(boolean driver) {
                    this.driver = driver;
                }

                public boolean isStudent() {
                    return student;
                }

                public void setStudent(boolean student) {
                    this.student = student;
                }

                public boolean isFee() {
                    return fee;
                }

                public void setFee(boolean fee) {
                    this.fee = fee;
                }

                public boolean isChannel() {
                    return channel;
                }

                public void setChannel(boolean channel) {
                    this.channel = channel;
                }

                public boolean isAppointment() {
                    return appointment;
                }

                public void setAppointment(boolean appointment) {
                    this.appointment = appointment;
                }

                public boolean isStaff() {
                    return staff;
                }

                public void setStaff(boolean staff) {
                    this.staff = staff;
                }

                public boolean isCommunication() {
                    return communication;
                }

                public void setCommunication(boolean communication) {
                    this.communication = communication;
                }

                public String getVersion() {
                    return version;
                }

                public void setVersion(String version) {
                    this.version = version;
                }

                public boolean isEvents() {
                    return events;
                }

                public void setEvents(boolean events) {
                    this.events = events;
                }
            }

            public static class AppsBean {
                /**
                 * parent : true
                 * teacher : true
                 * driver : true
                 * admin : true
                 */

                private boolean parent;
                private boolean teacher;
                private boolean driver;
                private boolean admin;

                public boolean isParent() {
                    return parent;
                }

                public void setParent(boolean parent) {
                    this.parent = parent;
                }

                public boolean isTeacher() {
                    return teacher;
                }

                public void setTeacher(boolean teacher) {
                    this.teacher = teacher;
                }

                public boolean isDriver() {
                    return driver;
                }

                public void setDriver(boolean driver) {
                    this.driver = driver;
                }

                public boolean isAdmin() {
                    return admin;
                }

                public void setAdmin(boolean admin) {
                    this.admin = admin;
                }
            }

            public static class DrivermodulesBean {
                /**
                 * pickupdropoff : true
                 * qrscan : true
                 * fileupload : true
                 * refreshFrequency : 10
                 * video : true
                 * version : premium
                 */

                private boolean pickupdropoff;
                private boolean qrscan;
                private boolean fileupload;
                private String refreshFrequency;
                private boolean video;
                private String version;

                public boolean isPickupdropoff() {
                    return pickupdropoff;
                }

                public void setPickupdropoff(boolean pickupdropoff) {
                    this.pickupdropoff = pickupdropoff;
                }

                public boolean isQrscan() {
                    return qrscan;
                }

                public void setQrscan(boolean qrscan) {
                    this.qrscan = qrscan;
                }

                public boolean isFileupload() {
                    return fileupload;
                }

                public void setFileupload(boolean fileupload) {
                    this.fileupload = fileupload;
                }

                public String getRefreshFrequency() {
                    return refreshFrequency;
                }

                public void setRefreshFrequency(String refreshFrequency) {
                    this.refreshFrequency = refreshFrequency;
                }

                public boolean isVideo() {
                    return video;
                }

                public void setVideo(boolean video) {
                    this.video = video;
                }

                public String getVersion() {
                    return version;
                }

                public void setVersion(String version) {
                    this.version = version;
                }
            }
        }

        public static class SchoolTimingBean {
            /**
             * startTime : 07:00 AM
             * endTime : 02:00 PM
             * periodFrequency : 60
             */

            private String startTime;
            private String endTime;
            private String periodFrequency;

            public String getStartTime() {
                return startTime;
            }

            public void setStartTime(String startTime) {
                this.startTime = startTime;
            }

            public String getEndTime() {
                return endTime;
            }

            public void setEndTime(String endTime) {
                this.endTime = endTime;
            }

            public String getPeriodFrequency() {
                return periodFrequency;
            }

            public void setPeriodFrequency(String periodFrequency) {
                this.periodFrequency = periodFrequency;
            }
        }

        public static class BusStandInfoBean {
            /**
             * address : Barakhamba Road, New Delhi, Delhi
             * geopoint : 28.628275,77.229675
             */

            private String address;
            private String geopoint;

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getGeopoint() {
                return geopoint;
            }

            public void setGeopoint(String geopoint) {
                this.geopoint = geopoint;
            }
        }

        public static class TermDetailsBean {
            /**
             * termId : 01
             * termDesc : July Month
             * termName : July
             */

            private String termId;
            private String termDesc;
            private String termName;

            public String getTermId() {
                return termId;
            }

            public void setTermId(String termId) {
                this.termId = termId;
            }

            public String getTermDesc() {
                return termDesc;
            }

            public void setTermDesc(String termDesc) {
                this.termDesc = termDesc;
            }

            public String getTermName() {
                return termName;
            }

            public void setTermName(String termName) {
                this.termName = termName;
            }
        }
    }
}
