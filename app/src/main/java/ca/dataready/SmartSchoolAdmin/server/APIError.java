/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.server;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import java.lang.annotation.Annotation;


import ca.dataready.SmartSchoolAdmin.login.LoginActivity;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.expired.ExpiredActivity;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;


/**
 * Created by pankaj on 03/03/17.
 */

public class APIError {

    private int statusCode;
    private String message;
    public static boolean status = false;
    public static final int UPDATE_TOKEN = 285;

    public APIError() {
    }

    public int status() {
        return statusCode;
    }

    public String message() {
        return message;
    }

    public static APIError parseError(Response<?> response, Activity activity, String apiName) {
        APIError error;
        try {
            Converter<ResponseBody, APIError> converter =
                AdminApp.getInstance().getRetrofit()
                            .responseBodyConverter(APIError.class, new Annotation[0]);


            error = converter.convert(response.errorBody());
            Log.e("error", "zero");
            Context context = AdminApp.getInstance().getApplicationContext();
            if (context != null) {
                Log.e("error", "one");
                if (AdminApp.getInstance().getCredential() == null) {
                    Log.e("error", "two");
                    intent(context);
                } else {
                    Log.e("error", "three");
                    alerDialog(activity,apiName);
                }
            }
        } catch (Exception e) {
            return new APIError();
        }

        return error;
    }

    private static void intent(Context context) {

        Intent intent = new Intent(context, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }


    public static void alerDialog(Activity context, String apiName) {

        if (!status) {
            status = true;
            Intent intent = new Intent(context, ExpiredActivity.class);
            intent.putExtra("api",apiName);
            context.startActivityForResult(intent, UPDATE_TOKEN);
        }
    }

    private static void alerDialog(Context context, APIError error) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(error.message());
        builder.setPositiveButton(context.getString(R.string.update), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.setNegativeButton(context.getString(R.string.dismiss), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.create().show();
    }



}
