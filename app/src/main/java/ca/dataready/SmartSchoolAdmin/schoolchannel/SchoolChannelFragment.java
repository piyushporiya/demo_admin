/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.schoolchannel;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.Listeners.OnReloadListener;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.DateFunction;
import ca.dataready.SmartSchoolAdmin.Utilities.LastItemDecoration;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.schoolchannel.adapter.SchoolChannelAdapter;
import ca.dataready.SmartSchoolAdmin.schoolchannel.create.CreateSchoolChannelActivity;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.SchoolChannelResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SchoolChannelFragment extends Fragment implements OnReloadListener, SwipeRefreshLayout.OnRefreshListener {

    private static final String OBJECT = "object";
    @BindView(R.id.recyclerView)
    SuperRecyclerView recyclerView;
    Unbinder unbinder;
    private static boolean isSchoolChannelVisible;
    @BindView(R.id.fab_create)
    FloatingActionButton fabCreate;
    private Call<SchoolChannelResponse> appApi;
    private SchoolChannelAdapter adapter;
    private ArrayList<SchoolChannelResponse.ResultBean> results;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_school_channel, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) getActivity()).setSubTitle(getString(R.string.school_channel));
        ((HomeActivity) getActivity()).setOnReloadListener(this);
      /*  BadgeCountTracker.saveSchoolChannelBadgeCounter(getActivity(), 0);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mSchoolChannelReceiver,
                new IntentFilter(SCHOOL_PUSH_NOTIFICATION_BROADCAST));*/

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new LastItemDecoration());
        recyclerView.setRefreshListener(SchoolChannelFragment.this);

        if (savedInstanceState != null) {

            results = savedInstanceState.getParcelableArrayList(App_Constants.OBJECT);

            if (results != null && results.size() > 0) {

                setData();

            } else {

                adapter = new SchoolChannelAdapter(getActivity());
                recyclerView.setAdapter(adapter);
                TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                emptyView.setText(getString(R.string.no_data));
            }

        } else {

            Init();
        }

    }


    private void Init() {

        ApiCall();
    }

    private void ApiCall() {

        CREDENTIAL.ResultBean entity = AdminApp.getInstance().getAdmin();
        appApi = AdminApp.getInstance().getApi().getSchoolChannelNotifications(entity.getSchoolId(), AdminApp.getInstance().getAdmin().getSchoolYear());
        appApi.enqueue(new Callback<SchoolChannelResponse>() {
            @Override
            public void onResponse(Call<SchoolChannelResponse> call, Response<SchoolChannelResponse> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            results = response.body().getResult();
                            if (results != null && results.size() > 0) {

                                setData();

                            } else {
                                adapter = new SchoolChannelAdapter(getActivity());
                                recyclerView.setAdapter(adapter);
                                TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                                emptyView.setText(getString(R.string.no_data));
                            }

                        } else {
                            adapter = new SchoolChannelAdapter(getActivity());
                            recyclerView.setAdapter(adapter);
                            TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                            emptyView.setText(getString(R.string.no_data));
                        }
                    } else {

                        APIError error = APIError.parseError(response, getActivity(), App_Constants.API_SCHOOL_CHANNEL_DATA);
                        adapter = new SchoolChannelAdapter(getActivity());
                        recyclerView.setAdapter(adapter);
                        TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                        emptyView.setText(error.message());

                    }
                } catch (Exception e) {

                    adapter = new SchoolChannelAdapter(getActivity());
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(getString(R.string.somethingwrong));
                }
            }

            @Override
            public void onFailure(Call<SchoolChannelResponse> call, Throwable t) {
                if (!call.isCanceled()) {
                    adapter = new SchoolChannelAdapter(getActivity());
                    recyclerView.setAdapter(adapter);
                    TextView emptyView = (TextView) recyclerView.getEmptyView().findViewById(R.id.empty);
                    emptyView.setText(App_Constants.NO_INTERNET);
                }
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();

        if(appApi!=null)
            appApi.cancel();
    }

    private void setData() {

        adapter = new SchoolChannelAdapter(getActivity());
        recyclerView.setAdapter(adapter);

        List<String> headers = new ArrayList<>();
        List<SchoolChannelResponse.ResultBean> schoolChannelBeans = new ArrayList<>();

        for (int i = 0; i < results.size(); i++) {

            if (results.get(i).getCreationDate() != null && results.get(i).getCreationDate().contains(" ")) {

                String formattedDate = DateFunction.ConvertDate(results.get(i).getCreationDate(), "yyyy-MM-dd hh:mm:ss", "dd MMMM yyyy");

                String[] pDates = formattedDate.split(" ");
                if (pDates.length > 2) {
                    String month = pDates[1];
                    String year = pDates[2];
                    if (!headers.contains(month)) {
                        headers.add(month);
                        SchoolChannelResponse.ResultBean bean = new SchoolChannelResponse.ResultBean();
                        bean.setCreationDate(results.get(i).getCreationDate());
                        bean.setType(1);
                        bean.setMonth(month + " " + year);
                        schoolChannelBeans.add(bean);
                        schoolChannelBeans.add(results.get(i));
                    } else {
                        schoolChannelBeans.add(results.get(i));
                    }
                } else {
                    schoolChannelBeans.add(results.get(i));
                }
            }
        }

        adapter.addItem(schoolChannelBeans);
    }


    @Override
    public void onRefresh() {

        if (isVisible())
            ApiCall();
    }

    @Override
    public void onReload(String apiName) {

        if (apiName != null) {

            if (apiName.equals(App_Constants.API_SCHOOL_CHANNEL_DATA)) {
                if (isVisible())
                    ApiCall();
            }
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        isSchoolChannelVisible = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        isSchoolChannelVisible = true;
    }

    private BroadcastReceiver mSchoolChannelReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (isVisible())
                onRefresh();

        }
    };

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(App_Constants.OBJECT, results);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.fab_create)
    public void onViewClicked() {

        startActivityForResult(new Intent(getActivity(), CreateSchoolChannelActivity.class), App_Constants.UPDATE_LISTING);
        getActivity().overridePendingTransition(R.anim.enter, R.anim.leave);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == App_Constants.UPDATE_LISTING && resultCode == Activity.RESULT_OK) {

            if (isVisible()) {
                ApiCall();
            }
        }
    }
}
