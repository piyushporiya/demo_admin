/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.schoolchannel.unread.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.server.SchoolChannelResponse;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.FileModel;

public class UnReadSchoolChannelNotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mcontex;
    private List<SchoolChannelResponse.ResultBean> list;
    private List<FileModel> selectedFiles = new ArrayList<>();

    public UnReadSchoolChannelNotificationAdapter(Context mcontex) {
        this.mcontex = mcontex;
        this.list = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new NotificationHolder(LayoutInflater.from(mcontex).inflate(R.layout.raw_reader_list, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

//        final SchoolChannelResponse.ResultBean bean = list.get(position);


        if (holder instanceof NotificationHolder) {


        }

    }

    private void makeOtherCollapsed(int position) {

        for (int i = 0; i < list.size(); i++) {

            if (i != position) {
                list.get(i).setExpanded(false);
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return  2;
    }

    public void addItem(List<SchoolChannelResponse.ResultBean> result) {

        list.addAll(result);
        notifyDataSetChanged();
    }

    public void clear() {

        list.clear();
        notifyDataSetChanged();
    }



    class NotificationHolder extends RecyclerView.ViewHolder {

        View mView;

        //R.layout.raw_school_channel
        NotificationHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mView = itemView;
        }
    }

}
