/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.schoolchannel.details;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.DateFunction;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.server.AppApi;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.server.NotificationByIdResponse;
import ca.dataready.SmartSchoolAdmin.server.SchoolChannelResponse;
import ca.dataready.SmartSchoolAdmin.viewresponse.adapter.FilesAdapter;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.FileModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SchoolChannelDetailsActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txt_date_value)
    TextView txtDateValue;
    @BindView(R.id.txt_title_value)
    TextView txtTitleValue;
    @BindView(R.id.my_image_view)
    SimpleDraweeView myImageView;
    @BindView(R.id.txt_name)
    TextView txtName;
    @BindView(R.id.txt_email)
    TextView txtEmail;
    @BindView(R.id.rlMainBox)
    RelativeLayout rlMainBox;
    @BindView(R.id.txt_message_value)
    TextView txtMessageValue;
    @BindView(R.id.rvAttachments)
    RecyclerView rvAttachments;
    @BindView(R.id.llAttachments)
    LinearLayout llAttachments;
    @BindView(R.id.txt_empty)
    TextView txtEmpty;
    @BindView(R.id.viewAnimator)
    ViewAnimator viewAnimator;
    private SchoolChannelResponse.ResultBean entity;
    private String notificationId;
    private CREDENTIAL.ResultBean adminEntity;
    private Call<NotificationByIdResponse> call;
    private NotificationByIdResponse.ResultBean model;
    private FilesAdapter filesAdapter;
    private List<FileModel> selectedFiles = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_channel_details);
        ButterKnife.bind(this);
        setHeaderInfo();

        if (getIntent().getExtras() != null) {
            entity = getIntent().getParcelableExtra(App_Constants.OBJECT);
            notificationId = getIntent().getStringExtra(App_Constants.NOTIFICATION_ID);
            if (entity != null) {
                setValues();
            } else if (notificationId != null) {
                getNotificationById();
            }
        }
    }

    private void setHeaderInfo() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(AdminApp.getInstance().getAdmin().getSchoolName()+ " " + AdminApp.getInstance().getAdmin().getSchoolYear());
        getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + getString(R.string.school_channel_details) + "</small>"));

    }

    private void getNotificationById() {

        adminEntity = AdminApp.getInstance().getAdmin();
        if (adminEntity == null) {
            viewAnimator.setDisplayedChild(2);
            return;
        }

        call = AdminApp.getInstance().getApi().getNotificationById(adminEntity.getSchoolId()
                , adminEntity.getSchoolYear(), notificationId);

        call.enqueue(new Callback<NotificationByIdResponse>() {
            @Override
            public void onResponse(Call<NotificationByIdResponse> call, Response<NotificationByIdResponse> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {
                            model = response.body().getResult();
                            setValues();
                        } else {
                            viewAnimator.setDisplayedChild(2);
                            txtEmpty.setText(response.body().getMessage());
                        }
                    }
                } catch (Exception e) {
                    viewAnimator.setDisplayedChild(2);
                    txtEmpty.setText(getString(R.string.somethingwrong));
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<NotificationByIdResponse> call, Throwable t) {

                if (!call.isCanceled()) {

                    viewAnimator.setDisplayedChild(2);
                    txtEmpty.setText(App_Constants.NO_INTERNET);
                }
            }
        });

    }


    private void setValues() {

        if (entity != null) {

            txtTitleValue.setText(entity.getNotificationShortMessage());

            txtMessageValue.setText(entity.getNotificationLongMessage());
            String formattedDate = DateFunction.ConvertDate(entity.getNotificationDate(), "yyyy-MM-dd", "dd MMM yyyy");
            txtDateValue.setText(formattedDate);

            txtName.setText(entity.getStaffName());
            txtEmail.setText(entity.getStaffId());
            myImageView.setImageURI(Uri.parse(AppApi.BASE_URL + entity.getStaffProfilePic()));


            if (entity.getFiles() != null && entity.getFiles().size() > 0) {

                rvAttachments.setLayoutManager(new LinearLayoutManager(SchoolChannelDetailsActivity.this));
                filesAdapter = new FilesAdapter(SchoolChannelDetailsActivity.this, true);
                rvAttachments.setAdapter(filesAdapter);
                selectedFiles.clear();
                if (entity.getFiles() != null && entity.getFiles().size() > 0) {
                    for (SchoolChannelResponse.ResultBean.FilesBean model : entity.getFiles()) {
                        if (model.getFilePath().contains("/")) {
                            selectedFiles.add(new FileModel(model.getFilePath().substring(model.getFilePath().lastIndexOf("/") + 1), model.getFilePath()));
                        }
                    }
                }
                filesAdapter.addItem(selectedFiles);

            } else {

                llAttachments.setVisibility(View.GONE);
            }
            viewAnimator.setDisplayedChild(1);

        } else if (model != null) {

            txtTitleValue.setText(model.getNotificationShortMessage());

            txtMessageValue.setText(model.getNotificationLongMessage());
            String formattedDate = DateFunction.ConvertDate(model.getNotificationDate(), "yyyy-MM-dd", "dd MMM yyyy");
            txtDateValue.setText(formattedDate);

            txtName.setText(model.getStaffName());
            txtEmail.setText(model.getStaffId());
            myImageView.setImageURI(Uri.parse(AppApi.BASE_URL + model.getStaffProfilePic()));


            if (model.getFiles() != null && model.getFiles().size() > 0) {

                rvAttachments.setLayoutManager(new LinearLayoutManager(SchoolChannelDetailsActivity.this));
                filesAdapter = new FilesAdapter(SchoolChannelDetailsActivity.this, true);
                rvAttachments.setAdapter(filesAdapter);
                selectedFiles.clear();
                if (model.getFiles() != null && model.getFiles().size() > 0) {
                    for (NotificationByIdResponse.ResultBean.FilesBean model : model.getFiles()) {
                        if (model.getFilePath().contains("/")) {
                            selectedFiles.add(new FileModel(model.getFilePath().substring(model.getFilePath().lastIndexOf("/") + 1), model.getFilePath()));
                        }
                    }
                }
                filesAdapter.addItem(selectedFiles);

            } else {

                llAttachments.setVisibility(View.GONE);
            }
            viewAnimator.setDisplayedChild(1);

        } else {

            viewAnimator.setDisplayedChild(2);
        }


    }


    @Override
    protected void onStop() {
        super.onStop();
        if (call != null)
            call.cancel();
    }

    @Override
    public void onBackPressed() {

        if (notificationId != null) {
            Intent intent = new Intent(App_Constants.UPDATE_FRAGEMENT_BROADCAST);
            intent.putExtra(App_Constants.TYPE, App_Constants.SCHOOL_CHANNEL);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        super.onBackPressed();

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
