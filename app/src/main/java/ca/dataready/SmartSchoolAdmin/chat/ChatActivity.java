/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.chat;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.Utility;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.app.App_Constants;
import ca.dataready.SmartSchoolAdmin.app.BaseActivity;
import ca.dataready.SmartSchoolAdmin.chat.adapter.ChatAdapter;
import ca.dataready.SmartSchoolAdmin.counter.BadgeCountTracker;
import ca.dataready.SmartSchoolAdmin.fileselectoer.FileSelectionActivity;
import ca.dataready.SmartSchoolAdmin.HomeActivity;
import ca.dataready.SmartSchoolAdmin.offline.chat.ChatDataHolder;
import ca.dataready.SmartSchoolAdmin.server.APIError;
import ca.dataready.SmartSchoolAdmin.server.CREDENTIAL;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.FileModel;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.ImageUpload;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.ReplyCommunicationModel;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.SchoolCommunication;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.ThreadCommunicationModel;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.MESSAGE_ID;
import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.STUDENT_ID;
import static ca.dataready.SmartSchoolAdmin.fcm.FirebaseMessagingService.VIEW_RESPOND_THREAD_PUSH_NOTIFICATION_BROADCAST;


public class ChatActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    private static final int REQUEST_CAMERA = 111;
    private static final int CAMERA_REQUEST_CODE = 501;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.txt_no_data)
    TextView txtNoData;
    CREDENTIAL.ResultBean entity;
    @BindView(R.id.parent_viewAnimator)
    ViewAnimator parentViewAnimator;
    ChatAdapter chatAdapter;
    public static final String OBJECT = "object";
    SchoolCommunication.ResultBean bean;

    Call<ThreadCommunicationModel> call;
    Call<ReplyCommunicationModel> replyCall;

    @BindView(R.id.editText)
    EditText editText;
    @BindView(R.id.send_btn)
    ImageButton sendBtn;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.retry_btn)
    ImageButton retryBtn;
    @BindView(R.id.childviewAnimator)
    ViewAnimator childviewAnimator;
    boolean isVisible = false;
    List<ThreadCommunicationModel.ResultBean> threadCommBeans;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.bottom)
    RelativeLayout bottom;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.send_attach)
    ImageButton sendAttach;
    public static final int FILE_SELECT = 85;
    List<FileModel> selectedFiles = new ArrayList<>();
    @BindView(R.id.camera_attach)
    ImageButton cameraAttach;
    private String attachmentToUpload;
    List<ThreadCommunicationModel.ResultBean.FilesBean> files = new ArrayList<>();
    private Timer t;
    private boolean isMessageSend = true;
    Handler sendMsgHandler = new Handler();
    public static boolean isChatScreenVisible = false;

    private String studentId, messageId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReloadReceiver,
                new IntentFilter(HomeActivity.RELOAD));
        LinearLayoutManager linearLayout = new LinearLayoutManager(ChatActivity.this);
        linearLayout.setStackFromEnd(true);
        recyclerview.setLayoutManager(linearLayout);

        setHeaderInfo();
        Init();
    }

    private void setHeaderInfo() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    private void Init() {

        entity = AdminApp.getInstance().getAdmin();
        if (getIntent().getExtras() != null) {
            bean = getIntent().getParcelableExtra(OBJECT);
            getSupportActionBar().setTitle(AdminApp.getInstance().getAdmin().getSchoolName());
            getSupportActionBar().setSubtitle(Html.fromHtml("<small>" + bean.getStudentFirstName() + " " + bean.getStudentLastName() + "</small>"));
            studentId = bean.getStudentId();
            messageId = bean.getMessageId();
            BadgeCountTracker.saveViewandResposndSingleThreadBadgeCounter(ChatActivity.this, 0, messageId);
        }

        if (entity != null && bean != null) {
            parentViewAnimator.setDisplayedChild(0);
            ApiCall();
        } else {
            parentViewAnimator.setDisplayedChild(2);
        }
    }

    private void ApiCall() {


        call = AdminApp.getInstance().getApi().getThreadCommunications(entity.getSchoolId(), messageId, studentId);
        call.enqueue(new Callback<ThreadCommunicationModel>() {
            @Override
            public void onResponse(Call<ThreadCommunicationModel> call, Response<ThreadCommunicationModel> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            chatAdapter = new ChatAdapter(ChatActivity.this);
                            recyclerview.setAdapter(chatAdapter);

                            threadCommBeans = response.body().getResult();

                            if (threadCommBeans != null && threadCommBeans.size() > 0) {

                                for (ThreadCommunicationModel.ResultBean bean : threadCommBeans) {

                                    if (bean.getFiles() != null && bean.getFiles().size() > 0) {
                                        bean.setIsAttachment(true);
                                    }
                                }

                                Collections.reverse(threadCommBeans);
                                chatAdapter.addItem(threadCommBeans);
                                parentViewAnimator.setDisplayedChild(1);
                                addDataForOfflineUse(threadCommBeans, bean.getMessageId());
                                // recyclerview.smoothScrollToPosition(0);

                            } else {

                                parentViewAnimator.setDisplayedChild(2);
                                txtNoData.setText(getString(R.string.no_data));
                            }

                        } else {

                            parentViewAnimator.setDisplayedChild(2);
                            txtNoData.setText(response.body().getMessage());
                        }

                    } else {

                        APIError error = APIError.parseError(response, ChatActivity.this, App_Constants.API_COMMUNICATION_THREAD);
                        parentViewAnimator.setDisplayedChild(2);
                        txtNoData.setText(error.message());

                    }
                } catch (Exception e) {

                    parentViewAnimator.setDisplayedChild(2);
                    txtNoData.setText(getString(R.string.somethingwrong));
                    e.printStackTrace();
                }

                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<ThreadCommunicationModel> call, Throwable t) {
                if (!call.isCanceled()) {
                    swipeRefreshLayout.setRefreshing(false);
                    parentViewAnimator.setDisplayedChild(2);
                    txtNoData.setText(App_Constants.NO_INTERNET);
                    System.out.println(t.getMessage());
                    getDataForOfflineUse(bean.getMessageId());
                }
            }
        });
    }

    private void addDataForOfflineUse(List<ThreadCommunicationModel.ResultBean> results, String messageId) {

        ChatDataHolder.saveChat(ChatActivity.this, results, messageId);
    }

    private void getDataForOfflineUse(String messageId) {

        List<ThreadCommunicationModel.ResultBean> model = ChatDataHolder.getChat(ChatActivity.this, messageId);

        if (model != null && model.size() > 0) {

            chatAdapter = new ChatAdapter(ChatActivity.this);
            recyclerview.setAdapter(chatAdapter);
            chatAdapter.addItem(model);
            parentViewAnimator.setDisplayedChild(1);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        super.onBackPressed();
    }

    @OnClick({R.id.send_btn, R.id.retry_btn, R.id.btn_refresh_chat, R.id.send_attach, R.id.camera_attach})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.send_btn:

                if (!editText.getText().toString().isEmpty()) {
                    files.clear();
                    replyCommunication(false);
                }

                break;
            case R.id.retry_btn:

                break;

            case R.id.btn_refresh_chat:

                onRefresh();

                break;

            case R.id.send_attach:

                Intent intent = new Intent(ChatActivity.this, FileSelectionActivity.class);
                startActivityForResult(intent, FILE_SELECT);

                break;

            case R.id.camera_attach:

               /* if (ContextCompat.checkSelfPermission(ChatActivity.this, Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA},
                            CAMERA_REQUEST_CODE);
                } else {

                    cameraIntent();
                }*/

                break;
        }
    }

    private void cameraIntent() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);

    }


    private void replyCommunication(final boolean isAttachment) {

        childviewAnimator.setDisplayedChild(1);

        ReplyCommunicationParams params = new ReplyCommunicationParams();
        params.setClassId(bean.getClassId());
        params.setGradeId(bean.getGradeId());
        params.setMessage(editText.getText().toString());
        params.setMessageId(bean.getMessageId());
        params.setSchoolId(bean.getSchoolId());
        params.setSchoolYear(bean.getSchoolYear());
        params.setStudentId(bean.getStudentId());
        params.setCommType("teacher");
        params.setTeacherId(AdminApp.getInstance().getAdmin().getId());
        params.setTitle(bean.getTitle());
        if (files != null && files.size() > 0) {

            List<ReplyCommunicationParams.FilesBean> filesBeans = new ArrayList<>();
            for (ThreadCommunicationModel.ResultBean.FilesBean bean : files) {
                ReplyCommunicationParams.FilesBean beans = new ReplyCommunicationParams.FilesBean();
                beans.setFilePath(bean.getFilePath());
                filesBeans.add(beans);
            }

            params.setFiles(filesBeans);
        }


        replyCall = AdminApp.getInstance().getApi().replyCommunication(params);
        replyCall.enqueue(new Callback<ReplyCommunicationModel>() {
            @Override
            public void onResponse(Call<ReplyCommunicationModel> call, Response<ReplyCommunicationModel> response) {

                try {
                    if (response.isSuccessful()) {
                        if (response.body().isStatus()) {

                            ThreadCommunicationModel.ResultBean bean = new ThreadCommunicationModel.ResultBean();
                            bean.setMessage(editText.getText().toString());
                            bean.setFrom(AdminApp.getInstance().getAdmin().getId());
                            bean.setCreatedDate(AdminApp.getInstance().currentDate());
                            bean.setIsAttachment(isAttachment);
                            if (files != null && files.size() > 0)
                                bean.setFiles(files);

                            if (chatAdapter != null) {
                                chatAdapter.addSingleItem(bean);
                                recyclerview.smoothScrollToPosition(chatAdapter.getItemCount());
                            }

                            editText.getText().clear();

                        } else {

                            Utility.showSnackBar(recyclerview, response.body().getMessage());
                        }

                    } else {

                        APIError error = APIError.parseError(response, ChatActivity.this, App_Constants.API_REPLY_COMMUNICATION);
                        Utility.showSnackBar(recyclerview, error.message());

                    }
                } catch (Exception e) {

                    Utility.showSnackBar(recyclerview, getString(R.string.somethingwrong));
                    e.printStackTrace();
                }
                childviewAnimator.setDisplayedChild(0);
            }

            @Override
            public void onFailure(Call<ReplyCommunicationModel> call, Throwable t) {
                if (!call.isCanceled()) {
                    childviewAnimator.setDisplayedChild(0);
                    Utility.showSnackBar(recyclerview, App_Constants.NO_INTERNET);
                    System.out.println(t.getMessage());
                }
            }
        });
    }


    @Override
    protected void onStop() {
        if (call != null)
            call.cancel();

        if (replyCall != null)
            replyCall.cancel();

        super.onStop();
    }

    private BroadcastReceiver mReloadReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

        }
    };


    @Override
    protected void onResume() {
        isVisible = true;
        isChatScreenVisible = true;
        LocalBroadcastManager.getInstance(ChatActivity.this).registerReceiver(mViewnRespondThreadPushNotificationReceiver,
                new IntentFilter(VIEW_RESPOND_THREAD_PUSH_NOTIFICATION_BROADCAST));
        super.onResume();
    }


    @Override
    protected void onPause() {
        isVisible = false;
        isChatScreenVisible = false;
        super.onPause();
    }


    private BroadcastReceiver mViewnRespondThreadPushNotificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent != null) {
                studentId = intent.getStringExtra(STUDENT_ID);
                messageId = intent.getStringExtra(MESSAGE_ID);

                ApiCall();
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == APIError.UPDATE_TOKEN && resultCode == RESULT_OK) {
            if (data != null) {
                String apiName = data.getStringExtra("api");
                onReload(apiName);
            }
        } else if (requestCode == FILE_SELECT && resultCode == Activity.RESULT_OK) {

            ArrayList<File> Files = (ArrayList<File>) data.getSerializableExtra(FileSelectionActivity.FILES_TO_UPLOAD);
            selectedFiles.clear();
            files.clear();
            if (Files != null && Files.size() > 0) {

                for (File bean : Files) {
                    selectedFiles.add(new FileModel(bean.toString().substring(bean.toString().lastIndexOf("/") + 1)
                            , bean.getAbsolutePath()));
                }

                sendMsgHandler.postDelayed(run, 100);

            }

        } else if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {

            Bitmap imageBitmap = null;
            Bundle extras = data.getExtras();
            if (extras != null) {
                imageBitmap = (Bitmap) extras.get("data");

            }

            Uri tempUri = getImageUri(ChatActivity.this, imageBitmap);
            File finalFile = new File(getRealPathFromURI(tempUri));
            String picturePath = finalFile.toString();
            System.out.println(finalFile);

            selectedFiles.clear();
            selectedFiles.add(new FileModel(picturePath.substring(picturePath.lastIndexOf("/") + 1), picturePath));


            sendMsgHandler.postDelayed(run, 100);


        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }


    Runnable run = new Runnable() {
        @Override
        public void run() {

            if (selectedFiles != null && selectedFiles.size() > 0) {
                if (isMessageSend) {
                    isMessageSend = false;
                    UploadFile(selectedFiles.get(0).getPath());
                    sendMsgHandler.postDelayed(this, 1000);
                } else {
                    sendMsgHandler.postDelayed(run, 1000);
                }
            } else {
                sendAttach.setEnabled(true);
                cameraAttach.setEnabled(true);
                sendMsgHandler.removeCallbacks(this);
                replyCommunication(true);

            }
        }
    };

    private void UploadFile(String attachmentName) {

        sendAttach.setEnabled(false);
        cameraAttach.setEnabled(false);
        childviewAnimator.setDisplayedChild(1);

        final File file = new File(attachmentName);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        Call<ImageUpload> call = AdminApp.getInstance().getApi().upload(body);

        call.enqueue(new Callback<ImageUpload>() {
            @Override
            public void onResponse(Call<ImageUpload> call, Response<ImageUpload> response) {

                try {
                    if (response.isSuccessful()) {

                        if (response.body().isStatus()) {
                            if (response.body().getResult() != null) {

                                attachmentToUpload = response.body().getResult().getFilepath();
                                System.out.println(attachmentToUpload);

                                ThreadCommunicationModel.ResultBean.FilesBean model = new ThreadCommunicationModel.ResultBean.FilesBean();
                                model.setFilePath(attachmentToUpload);
                                files.add(model);

                                if (selectedFiles != null && selectedFiles.size() > 0)
                                    selectedFiles.remove(0);

                                isMessageSend = true;

                            } else {

                                sendAttach.setEnabled(true);
                                cameraAttach.setEnabled(true);
                                childviewAnimator.setDisplayedChild(0);
                                Toast.makeText(ChatActivity.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();

                            }
                        } else {
                            sendAttach.setEnabled(true);
                            cameraAttach.setEnabled(true);
                            childviewAnimator.setDisplayedChild(0);
                            Toast.makeText(ChatActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
                    sendAttach.setEnabled(true);
                    cameraAttach.setEnabled(true);
                    childviewAnimator.setDisplayedChild(0);
                    e.printStackTrace();
                    Toast.makeText(ChatActivity.this, getString(R.string.somethingwrong), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ImageUpload> call, Throwable t) {
                sendAttach.setEnabled(true);
                cameraAttach.setEnabled(true);
                childviewAnimator.setDisplayedChild(0);
                Log.e("ERROR UPLOAD", "" + t.getMessage());
                Toast.makeText(ChatActivity.this, App_Constants.NO_INTERNET, Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void onReload(String apiName) {

        Log.e("OnReload", "Chat Screen");

        if (apiName.equals(App_Constants.API_COMMUNICATION_THREAD)) {
            swipeRefreshLayout.setRefreshing(true);
            ApiCall();
        } else if (apiName.equals(App_Constants.API_REPLY_COMMUNICATION)) {
            sendBtn.performClick();
        }


    }

    @Override
    public void onRefresh() {

        swipeRefreshLayout.setRefreshing(true);
        Init();
    }


    public static class ReplyCommunicationParams {


        /**
         * classId : string
         * gradeId : string
         * message : string
         * messageId : string
         * schoolId : string
         * schoolYear : YYYY-YYYY
         * studentId : string
         * teacherId : string
         * title : string
         */

        private String classId;
        private String gradeId;
        private String message;
        private String messageId;
        private String schoolId;
        private String schoolYear;
        private String studentId;
        private String teacherId;
        private String title;
        private String commType;
        private List<FilesBean> files;

        public String getCommType() {
            return commType;
        }

        public void setCommType(String commType) {
            this.commType = commType;
        }

        public List<FilesBean> getFiles() {
            return files;
        }

        public void setFiles(List<FilesBean> files) {
            this.files = files;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getGradeId() {
            return gradeId;
        }

        public void setGradeId(String gradeId) {
            this.gradeId = gradeId;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getMessageId() {
            return messageId;
        }

        public void setMessageId(String messageId) {
            this.messageId = messageId;
        }

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getSchoolYear() {
            return schoolYear;
        }

        public void setSchoolYear(String schoolYear) {
            this.schoolYear = schoolYear;
        }

        public String getStudentId() {
            return studentId;
        }

        public void setStudentId(String studentId) {
            this.studentId = studentId;
        }

        public String getTeacherId() {
            return teacherId;
        }

        public void setTeacherId(String teacherId) {
            this.teacherId = teacherId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public static class FilesBean {
            /**
             * filePath : /schoolapp/images/354fa4c9-129e-4720-b29c-cee8ffb410a6.pdf
             */

            private String filePath;

            public String getFilePath() {
                return filePath;
            }

            public void setFilePath(String filePath) {
                this.filePath = filePath;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == CAMERA_REQUEST_CODE) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(ChatActivity.this, "camera permission granted", Toast.LENGTH_LONG).show();
                cameraIntent();

            } else {
                Toast.makeText(ChatActivity.this, "Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();

            }

        }
    }

}
