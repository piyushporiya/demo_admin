/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.chat.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.Utilities.DateFunction;
import ca.dataready.SmartSchoolAdmin.app.AdminApp;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.ThreadCommunicationModel;

/**
 * Created by social_jaydeep on 03/10/17.
 */

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<ThreadCommunicationModel.ResultBean> beans;
    private static final int ITEM_SENT = 0;
    private static final int ITEM_RECEIVE = 1;
    private static final int ITEM_SENT_ATTACHMENT = 2;
    private static final int ITEM_RECEIVE_ATTACHMENT = 3;
    AttachmentAdapter attachmentAdapter;

    public ChatAdapter(Context context) {
        this.context = context;
        beans = new ArrayList<>();
    }

    @Override
    public int getItemViewType(int position) {

        if (beans.get(position).getFrom() != null && beans.get(position).getFrom().equals(AdminApp.getInstance().getAdmin().getId())) {
            if (beans.get(position).isAttachment())
                return ITEM_SENT_ATTACHMENT;

            return ITEM_SENT;
        } else {
            if (beans.get(position).isAttachment())
                return ITEM_RECEIVE_ATTACHMENT;
            return ITEM_RECEIVE;
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == ITEM_SENT) {
            return new SentMessageViewHolder(LayoutInflater.from(context).inflate(R.layout.chat_item_sent, parent, false));
        } else if (viewType == ITEM_RECEIVE) {
            return new ReceiveMessageViewHolder(LayoutInflater.from(context).inflate(R.layout.chat_item_rcv, parent, false));
        } else if (viewType == ITEM_SENT_ATTACHMENT) {
            return new SentAttachmentViewHolder(LayoutInflater.from(context).inflate(R.layout.chat_item_sent_attachment, parent, false));
        } else if (viewType == ITEM_RECEIVE_ATTACHMENT) {
            return new ReceiveAttachmentViewHolder(LayoutInflater.from(context).inflate(R.layout.chat_item_rcv_attachment, parent, false));
        } else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        final ThreadCommunicationModel.ResultBean bean = beans.get(position);

        if (holder instanceof SentMessageViewHolder) {

            ((SentMessageViewHolder) holder).messageTextView.setText(bean.getMessage());

            String formattedDate = DateFunction.ConvertDate(bean.getCreatedDate(), "yyyy-MM-dd", "dd MMM yyyy");
            ((SentMessageViewHolder) holder).timestampTextView.setText(formattedDate);


        } else if (holder instanceof ReceiveMessageViewHolder) {


            ((ReceiveMessageViewHolder) holder).messageTextView.setText(bean.getMessage());
            String formattedDate = DateFunction.ConvertDate(bean.getCreatedDate(), "yyyy-MM-dd", "dd MMM yyyy");
            ((ReceiveMessageViewHolder) holder).timestampTextView.setText(formattedDate);

        } else if (holder instanceof SentAttachmentViewHolder) {

            if (bean.isAttachment() && bean.getFiles() != null) {

                if (bean.getMessage() != null && bean.getMessage().isEmpty())
                    ((SentAttachmentViewHolder) holder).messageTextView.setVisibility(View.GONE);
                else
                    ((SentAttachmentViewHolder) holder).messageTextView.setText(bean.getMessage());

                attachmentAdapter = new AttachmentAdapter(context);
                ((SentAttachmentViewHolder) holder).recyclerview.setAdapter(attachmentAdapter);
                attachmentAdapter.addItem(bean.getFiles());

                String formattedDate = DateFunction.ConvertDate(bean.getCreatedDate(), "yyyy-MM-dd", "dd MMM yyyy");
                ((SentAttachmentViewHolder) holder).timestampTextView.setText(formattedDate);
            }
        } else if (holder instanceof ReceiveAttachmentViewHolder) {

            if (bean.isAttachment() && bean.getFiles() != null) {

                attachmentAdapter = new AttachmentAdapter(context);
                ((ReceiveAttachmentViewHolder) holder).recyclerview.setAdapter(attachmentAdapter);
                attachmentAdapter.addItem(bean.getFiles());

                String formattedDate = DateFunction.ConvertDate(bean.getCreatedDate(), "yyyy-MM-dd", "dd MMM yyyy");
                ((ReceiveAttachmentViewHolder) holder).timestampTextView.setText(formattedDate);
            }
        }
    }

    public void addItem(List<ThreadCommunicationModel.ResultBean> results) {

        beans.addAll(results);
        notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }

    public void addSingleItem(ThreadCommunicationModel.ResultBean bean) {

        beans.add(bean);
        notifyItemInserted(beans.size() - 1);
    }


    class SentMessageViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.message_text_view)
        TextView messageTextView;
        @BindView(R.id.timestamp_text_view)
        TextView timestampTextView;
        View view;
        //  R.layout.chat_item_sent

        SentMessageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;

        }
    }


    class ReceiveMessageViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.message_text_view)
        TextView messageTextView;
        @BindView(R.id.timestamp_text_view)
        TextView timestampTextView;
        View view;
        //  R.layout.chat_item_rcv

        ReceiveMessageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }


    class SentAttachmentViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.recyclerview)
        RecyclerView recyclerview;
        @BindView(R.id.message_text_view)
        TextView messageTextView;
        @BindView(R.id.timestamp_text_view)
        TextView timestampTextView;
        View view;
        //  R.layout.chat_item_sent

        SentAttachmentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
            recyclerview.setLayoutManager(new LinearLayoutManager(context));
            recyclerview.setNestedScrollingEnabled(false);
        }
    }


    class ReceiveAttachmentViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.recyclerview)
        RecyclerView recyclerview;
        @BindView(R.id.timestamp_text_view)
        TextView timestampTextView;
        View view;
        //  R.layout.chat_item_rcv

        ReceiveAttachmentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
            recyclerview.setLayoutManager(new LinearLayoutManager(context));
            recyclerview.setNestedScrollingEnabled(false);

        }
    }
}
