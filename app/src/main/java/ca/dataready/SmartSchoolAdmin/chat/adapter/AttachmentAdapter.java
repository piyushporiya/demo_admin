/*
 * Copyright (c) 2017.
 *
 * ***********************************************************************
 *   DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 * ________________________________________________________________________
 *
 *    [2010] - [2018] SripathiSolutions Corp.
 *    [2010] - [2018] DataReady Technology Corp.
 *   ________________________________________________________________________
 *
 *    All Rights Reserved.
 *
 *   NOTICE:  All information contained herein is, and remains
 *   the property of DataReady Technology  and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to DataReady Technology Incorporated
 *   and its suppliers and may be covered by U.S. and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from DataReady Technology Incorporated.
 *
 *  *************************************************************************
 *  DATAREADY TECHNOLOGY CORPORATION CONFIDENTIAL
 *  *************************************************************************
 */

package ca.dataready.SmartSchoolAdmin.chat.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.dataready.SmartSchoolAdmin.R;
import ca.dataready.SmartSchoolAdmin.server.AppApi;
import ca.dataready.SmartSchoolAdmin.viewresponse.model.ThreadCommunicationModel;
import ca.dataready.SmartSchoolAdmin.web.WebActivity;


public class AttachmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<ThreadCommunicationModel.ResultBean.FilesBean> beans;

    public AttachmentAdapter(Context context) {
        this.context = context;
        beans = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(context).inflate(R.layout.raw_attachment, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final ThreadCommunicationModel.ResultBean.FilesBean bean = beans.get(position);
        if (holder instanceof ItemViewHolder) {

            ((ItemViewHolder) holder).messageTextView.setText(bean.getFilePath().substring(bean.getFilePath().lastIndexOf("/") + 1));

            ((ItemViewHolder) holder).messageTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    context.startActivity(new Intent(context, WebActivity.class)
                            .putExtra(WebActivity.URL, AppApi.BASE_URL + bean.getFilePath()));
                }
            });

        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return beans == null ? 0 : beans.size();
    }

    public void addItem(List<ThreadCommunicationModel.ResultBean.FilesBean> files) {

       beans.addAll(files);
        notifyDataSetChanged();
    }



    class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.message_text_view)
        TextView messageTextView;
        //R.layout.raw_attachment

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
